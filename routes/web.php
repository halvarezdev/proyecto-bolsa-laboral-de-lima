<?php
if ( ! session_id() ) @ session_start();
 
$codigoAcceso=isset($_POST['codigoAcceso'])?'1':'2';
$codigoAcceso=isset($_GET['code'])?'1':(isset($_POST['codigoAcceso'])?'1':'2');
 
if($codigoAcceso=='2'){
   
    if(isset($_SESSION['USUARIO_SESSION'])){
         
    }/*else{
        $url_sis=env('APP_URL'); 
         header('Location: '.$url_sis.'AdBlock?code');
        die();
        exit();
    }*/
}
 
 
Auth::routes(['verify' => true]);

/******************* */
Route::get('/clear', function() {
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');

 });


 /*
 php artisan cache:clear
 php artisan view:clear
 php artisan route:clear
 php artisan clear-compiled
 php artisan config:cache
 php artisan config:clear
 php artisan cache:clear
*/

//Route::get("AdBlock", 'AdBlockController@AdBlock');
Route::get("email", 'PHPMailerController@email')->name("email");
 
Route::get("send-email", 'PHPMailerController@verifyEmail')->name("send-email");

# Login
Route::get('/nuevoprueba', 'InicioController@loginPrueba')->name('nuevo.login');

# Rutas de Excel
Route::get('excel/empresa','ExcelController@listadoEmpresaExcel')->name('exportar.listado.empresa');
Route::get('excel/persona','ExcelController@listadoPersonaExcel')->name('exportar.listado.persona');

# Ruta para compartir via administrador
/*
Route::get('compartir','MarquetingFacebook@index')->name('compartir');
Route::post('compartir/guardar','MarquetingFacebook@guardaDatos')->name('marqueting.save');
Route::get('listacommpartir/{compartir}','MarquetingFacebook@compartirFacebook');
Route::get('vistaPrevia/{compartir}','MarquetingFacebook@vistaPrevia');
*/
// Password Reset Routes...

Route::get('users/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('users.password.reset');

Route::get('users/password/token/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('users/password/update', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::get('/', 'InicioController@index')->name('welcome');
Route::get('/buscador/{entidad}', 'InicioController@busqueda')->name('users.busqueda.inicio');
Route::get('/admin/home', 'AdminController@index')->name('admin.home');
Route::get('/admin/publicar/{id}', 'AdminController@publiarVacante')->name('contacto.publicar');
Route::get('/admin/logout', 'Auth\AdminLoginController@logoutAdmin')->name('logout.admin');

Route::get('/home', 'HomeController@index')->name('home');//->middleware('verified');
Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');
Route::get('/users/buscar', 'HomeController@BuscarPedido')->name('user.buscar');
Route::post('/users/busqueda', 'HomeController@busquedaSubmmit')->name('users.busqueda.submit');
Route::get('/users/contenido/{ficha}/{persona}', 'HomeController@getDataEmpresa');

Route::get('/users/editar', 'HomeController@editarDatos')->name('users.editar');
Route::post('/users/guardar', 'HomeController@guardarDatos')->name('users.guardar');
Route::get('/users/file', 'HomeController@listfile')->name('users.file');
Route::post('/users/save/file', 'HomeController@saveFile')->name('users.save.file');
Route::get('/users/logout/{entidad}', 'Auth\LoginController@userLogout')->name('logout.users');

Route::post('/users/ajaxFotoPostulante','HomeController@ajaxFotoPostulante')->name('users.ajax.foto');
Route::post('/users/basica','HomeController@formacionBasica')->name('users.basica');
Route::post('/users/superior','HomeController@formacionSuperior')->name('users.superior');
Route::post('/users/posgrado','HomeController@formacionPosgrado')->name('users.posgrado');
Route::post('/users/idioma','HomeController@idioma')->name('users.idioma');
Route::post('/users/compu','HomeController@comptacion')->name('users.comptacion');
Route::post('/users/estudios','HomeController@estudiosAdicionales')->name('users.estudios');
Route::post('/users/experiencia','HomeController@experienciaLaboral')->name('users.experiencia');
Route::get('/postulaciones','HomeController@postulaciones')->name('postulaciones');
Route::get('/convocatoria','HomeController@convocatoria')->name('convocatoria');
Route::get('/configuracion','HomeController@configuracion')->name('configuracion');
Route::get('/notificacion','HomeController@notificacion')->name('notificacion');
Route::get('/favoritas','HomeController@favoritas')->name('favoritas');

Route::get('/elimina/basico/{id}','HomeController@eliminaBasico')->name('elimina.basico');
Route::get('/elimina/superior/{id}','HomeController@eliminaSuperior')->name('elimina.superior');
Route::get('/elimina/posgrado/{id}','HomeController@eliminaPosgrado')->name('elimina.posgrado');
Route::get('/elimina/idioma/{id}','HomeController@eliminaIdioma')->name('elimina.idioma');
Route::get('/elimina/ofimatica/{id}','HomeController@eliminaOfimatica')->name('elimina.ofimatica');
Route::get('/elimina/adicionales/{id}','HomeController@eliminaAdcionales')->name('elimina.adicionales');
Route::get('/elimina/experiencia/{id}','HomeController@eliminaExperiencia')->name('elimina.experiencia');
// Rutas Para Contactos

Route::prefix('contacto')->group(function() {
    Route::get('/', 'ContactoController@index')->name('contacto.dashboard');//->middleware('contacto.verified');
    Route::post('/logout', 'Auth\ContactoLoginController@logoutContacto')->name('contacto.logout');
	Route::get('/login', 'Auth\ContactoLoginController@showLoginForm')->name('contacto.login');
	Route::post('/login', 'Auth\ContactoLoginController@login')->name('contacto.login.submit');
	Route::get('logout', 'Auth\ContactoLoginController@logout')->name('contacto.logout');
	Route::get('/register', 'Auth\ContactoRegisterController@registroFormulario')->name('contacto.register');
    Route::get('/termines-condiciones', 'Auth\ContactoRegisterController@CondicionesFormulario')->name('contacto.terminos');
    Route::post('/create', 'Auth\ContactoRegisterController@register')->name('contacto.create');
    Route::get('/verification/resend', 'Auth\ContactoVerificationController@resend')->name('contacto.verification.resend');
    Route::get('/email/verify', 'Auth\ContactoVerificationController@show')->name('contacto.verification.notice');
    Route::get('/email/verify/{id}', 'Auth\ContactoVerificationController@verify')->name('contacto.verification.verify');

});

Route::prefix('admin')->group(function() {
  Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/inicio', 'AdminController@index')->name('admin.dashboard');
 /* Route::get('/login', 'Auth\AdminController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminController@login')->name('admin.login.submit');
    */
});

Route::prefix('entidad')->group(function() {
    Route::get('/', 'Auth\EntidadUserLoginController@showLoginForm')->name('entidad.login');
    Route::post('/login', 'Auth\EntidadUserLoginController@login')->name('entidad.login.submit');
    Route::get('/inicio', 'EntidadController@index')->name('entidad.dashboard');
    Route::get('/vacante', 'EntidadController@vacante')->name('entidad.vacante');
    Route::get('/publicar/{id}', 'EntidadController@publiarVacante')->name('entidad.publicar');
    Route::get('/lista', 'EntidadController@listEntity')->name('entity.list');
    Route::get('/delete/{id}/{flag}', 'EntidadController@deleteEntity')->name('entity.delete');
    Route::post('/save', 'EntidadController@saveEntity')->name('entity.save');
    Route::post('/update', 'EntidadController@updateEntity')->name('entity.update');
    Route::get('/e-1', 'EntidadController@ReporteEUno')->name('reporte.entidad.euno');
    Route::get('/e-3', 'EntidadController@ReporteETres')->name('reporte.entidad.etres');
});


Route::post('/contacto/subirFoto','ContactoController@SubirForo')->name('contacto.ajax.foto');
Route::post('/contacto/subirBanner','ContactoController@SubirBanner')->name('contacto.ajax.banner');
Route::post('/contacto/editar', 'ContactoController@editarInforamcion')->name('editar.contacto');
Route::get('/contacto/lista', 'ContactoController@listaContacto')->name('lista.contacto');
Route::post('/contacto/nuevo', 'ContactoController@nuevoContacto')->name('contacto.nuevo');
Route::post('/contacto/nuevo/json','ContactoController@nuevoContactoJson')->name('contacto.nuevo.json');
Route::get('/vacante/editar/{id}', 'ContactoController@editarVacante')->name('contacto.editar');
Route::get('actualiza/contacto/','ContactoController@listaNuevoContactos')->name('lista.nuevos.contacto');
Route::post('/vacante/editarForm', 'ContactoController@editaVacanteForm')->name('vacante.editarForm');

Route::post('/vacante/cerrarpedido', 'ContactoController@cerrarPedido')->name('vacante.cierre');
Route::post('/vacante/vambiostatus', 'seguimiento\MatchingController@cambioEstatus')->name('cambiar.estadoestatus');

//Rutas para empresa
Route::prefix('empresa')->group(function(){
	Route::get('/vacante', 'ContactoController@vacante')->name('empresa.vacante');
	Route::post('/create', 'ContactoController@guardarEmpresa')->name('empresa.create');
    Route::post('/buscar', 'ContactoController@BuscaPostulante')->name('empresa.buscar');
});
Route::get('/users/download/{id}' , 'ContactoController@downloadFile')->name('download.hojavida');
Route::get('/servicios/{entidad}', 'ContactoController@empresaServicios')->name('empresa.servicios');
// reseto contraseña
//ESTA RUTA HA SIDO CAMBIADA POR LA ORIGINAL
//Route::post('/password/email', 'Auth\ContactoForgotPasswordController@sendResetLinkEmail')->name('contacto.password.email');
Route::post('users/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('contacto.password.email');//nueva ruta
Route::get('/password/reset', 'Auth\ContactoForgotPasswordController@showLinkRequestForm')->name('contacto.password.request');
Route::post('/password/reset', 'Auth\ContactoResetPasswordController@reset');
Route::get('/password/reset/{token}', 'Auth\ContactoResetPasswordController@showResetForm')->name('contacto.password.reset');

// Socialite

Route::get('login/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
//Route::get('login/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
//Route::get('/user', 'GraphController@publishToProfile')->name('publicar');

//RUTA PARA CV
Route::get('cv','cv\Crear_cv@Inicio');
Route::post('process-json','cv\Crear_cv@accion');
Route::post('view-cv','cv\Crear_cv@view_cv');

//RUTA PARA MACHIN
Route::group([
    'prefix' => 'matching'
],function () {
   Route::get('seguimiento/{id}','seguimiento\MatchingController@Inicio')->name('matching.seguimiento');
  Route::get('search/{pedido}','seguimiento\MatchingController@search_postulante')->name('buscar.postulante');
   Route::get('postulantes','seguimiento\MatchingController@getPostulante');
   Route::post('save/{id}','seguimiento\MatchingController@savePedidoPedido');
   Route::post('delete/','seguimiento\MatchingController@deletePedidoPedido');

   Route::post('updatePedido','seguimiento\MatchingController@updatePedido');
   Route::post('editar/{id}','seguimiento\MatchingController@editar');
   Route::post('delete/{id}','seguimiento\MatchingController@deletePedidPostu');
   Route::post('video/{id}','seguimiento\MatchingController@video');
   Route::post('actualizarColocacion/{id}','seguimiento\MatchingController@actualizarColocacion');
   Route::get('agregarpost/{ficha}/{persona}/{pedido}','seguimiento\MatchingController@agregarpost');
   Route::get('deletepost/{ficha}/{persona}/{pedido}','seguimiento\MatchingController@deletepost');
   Route::get('agregarPostAjax','seguimiento\MatchingController@agregarPostAjax');
});

Route::group([
    'prefix' => 'ubigeo'
],function () {
   Route::get('provincia/{depa}','control\GlobalJSON@getProviJson');
   Route::get('distrito/{depa}/{priv}','control\GlobalJSON@getDistritoJson');
});
Route::group([
    'prefix' => 'pdf'
],function () {
   Route::get('downloadcv/{PP_id}/{ficha_id}/{id_persona}/{idPedido}/{fin}','pdf\Hoja_cv@download_cv');

});

Route::group([
    'prefix' => 'autocomplete'
],function () {
   Route::get('puesto','control\GlobalJSON@automplitPuesto');
   Route::get('especialidad','control\GlobalJSON@automplitPuesto');
});

Route::get('asesoramiento/descubriendo','abe\Asesoramiento@descubriendo');
Route::get('asesoramiento/herramientas','abe\Asesoramiento@herramientas');
Route::get('asesoramiento/afrontar','abe\Asesoramiento@afrontar');


//vacante

Route::get('vacante/detalle/{id}', 'InicioController@detalleVacante')->name('vacante.detalle');
Route::post('vacante/guardar/{id}', 'InicioController@guardarVacante');

Route::get('vacante/guardar/{id}', 'InicioController@guardarVacante');

Route::get('integracionMML', 'IntegracionMMLController@integracionMML');
Route::post('integracionIntranetMML', 'IntegracionIntranetMMLController@integracionIntranetMML');
// configuracion

Route::post('cofiguracion/update', 'HomeController@updateConfiguracion')->name('update.configuracion');
Route::post('cofiguracion/updateC', 'HomeController@updateConfiguracionCurriculum')->name('update.curri');
Route::post('cofiguracion/eliminar', 'HomeController@updateConfiguracionEliminar')->name('update.elimina');


// editar persona

Route::get('edit/basico/{id}', 'HomeController@editBasico');
Route::post('edit/basicoFormulario', 'HomeController@editarFormularioDatos')->name('form.basico.update');
Route::get('edit/superior/{id}', 'HomeController@editSuperior');
Route::post('edit/superiorFormulario', 'HomeController@editarFormularioSuperior')->name('form.superior.update');
Route::get('edit/posgrado/{id}', 'HomeController@editPosgrado');
Route::post('edit/posgradoFormulario', 'HomeController@editarFormularioPosgrado')->name('form.posgrado.update');
Route::get('edit/idioma/{id}', 'HomeController@editIdioma');
Route::post('edit/idiomaFormulario', 'HomeController@editarFormularioIdioma')->name('form.idioma.update');
Route::get('edit/compu/{id}', 'HomeController@editCompu');
Route::post('edit/compuFormulario', 'HomeController@editarFormularioCompu')->name('form.compu.update');
Route::get('edit/estudios/{id}', 'HomeController@editEstudios');
Route::post('edit/estudiosFormulario', 'HomeController@editarFormularioEstudios')->name('form.estudios.update');
Route::get('edit/experiencia/{id}', 'HomeController@editExperiencia');
Route::post('edit/experienciaFormulario', 'HomeController@editarFormularioExperiencia')->name('form.experiencia.update');

// edit postulantes refile

Route::get('matching/citado/{ficha}/{persona}/{pedido}/{fecha}/{hora}', 'seguimiento\MatchingController@editCitado');
Route::get('matching/apto/{ficha}/{persona}/{pedido}', 'seguimiento\MatchingController@editApto');
Route::get('matching/contratado/{ficha}/{persona}/{pedido}', 'seguimiento\MatchingController@editContratado');

// menu maestros

Route::get('/nosotros/{entidad}', 'InicioController@indexNosotros')->name('nosotros');
Route::get('/consulta/{entidad}', 'InicioController@indexConsulta')->name('consulta');
Route::get('/contactenos/{entidad}', 'InicioController@indexContactenos')->name('contactenos');
Route::get('/orientador/{entidad}', 'InicioController@indexOrientador')->name('orientador');

Route::get('cambiar/visto/{id}', 'InicioController@cambiarVisto');


# Url para entidades
Route::get('/{name_entidad}', 'InicioController@index')->name('welcome.entidad');
Route::get('/entidad/logout', 'Auth\EntidadUserLoginController@entidadLogout')->name('entidad.logout');
//Route::get('/entidad/register', 'InicioController@index')->name('welcome.entidad');
Route::get('entidad/persona/nuevo', 'EntidadController@registroPersonas')->name('register.entipersona');
Route::post('entidad/persona/submit', 'EntidadController@registroPersonaSutmib')->name('register.submit');
Route::get('entidad/persona/listado', 'EntidadController@listadoPeronas')->name('persona.listado');
Route::get('entidad/empresa/nuevo', 'EntidadController@registroEnpresa')->name('register.entiempresa');
Route::post('entidad/empresa/submit', 'EntidadController@registroEmpresaSutmib')->name('entidad.empresa.submit');
Route::get('empresa/listado', 'EntidadController@listadoEmpresa')->name('empresa.listado');
Route::get('empresa/datos/{cod_empresa}', 'EntidadController@DatosEmpresa')->name('empresa.descripcion');
Route::get('empresa/pedido/{cod_empresa}', 'EntidadController@entidadNuevoPedido')->name('enti.nuevo.pedido');
Route::post('entidad/pedido/submit', 'EntidadController@registroPedidoSubmit')->name('entidad.empresa.create');
Route::get('entidad/pedido/detalle/{empresa}/{pedido}', 'EntidadController@detallePedido')->name('detalle.pedido');
Route::get('entidad/buscar/{pedido}/{empresa}', 'EntidadController@buscarPostulantes')->name('pedido.buscar');
Route::get('entidad/agregarPostAjax','EntidadController@agregarPostAjax');
Route::get('entidad/agregarpost/{ficha}/{persona}/{pedido}','EntidadController@agregarpost');
Route::get('entidad/deletepost/{ficha}/{persona}/{pedido}','EntidadController@deletepost');

# Url Para Entidades Reportes

Route::get('reporte/persona','EntidadController@personaGraficos')->name('reporte.presona.grafico');
Route::get('reporte/empresa','EntidadController@empresaGraficos')->name('reporte.empresa.grafico');
Route::get('filtro/ocupacion','EntidadController@filtroOcuapcion')->name('filtro.ocupacion');
Route::post('ocupacion/save','EntidadController@ocupacionSave')->name('guarda.ocupacion.parametrica');
Route::get('reporte/profesion','EntidadController@reporteGraficosOcupacion')->name('reporte.presona.ocupacion.grafico');

# Politicas

Route::get('/politica-privacidad/{entidad}', 'InicioController@politicaProvacidad')->name('politica.privacidad');

# valida contraseña numero de documento

Route::get('valida/numdoc/{id}', 'ContactoController@validaNumeroDocumento');
Route::get('valida/correo/{id}', 'ContactoController@validaCorreoElectronico');

# Elimina, Edita contactos
Route::get('/elimina/contacto/{id}','ContactoController@eliminaContacto')->name('elimina.contacto');
Route::get('edit/contacto/{id}', 'ContactoController@editContacto');
Route::post('edit/contactoFormulario', 'ContactoController@editarFormularioContacto')->name('form.contacto.update');

# Opciones de login persona/empresa

Route::get('valida/correo/users/{id}', 'InicioController@validaCorreoElectronicoUsers');
Route::post('valida/existe/users', 'Auth\LoginController@loginJson')->name('login.users.ajax');
Route::get('valida/correo/empresa/{id}', 'InicioController@validaCorreoElectronicoEmpresa');
Route::post('valida/existe/empresa', 'Auth\ContactoLoginController@loginJsonEmpresa')->name('login.empresa.ajax');
Route::get('valida/ruc/empresa/{id}', 'InicioController@validaRucEmpresa');

# Ruta para vacantes

Route::get('vacante/registro', 'VacanteController@index')->name('vacante.index');
Route::post('vacante/save', 'VacanteController@saveVacante')->name('save.vacante');
Route::get('valida/ruc/vacante/{id}','VacanteController@validaRucInstitucion');
Route::get('registro/institucion','VacanteController@registroInstitucion')->name('registro.institucion');
Route::post('institucion/save', 'VacanteController@sabeInstitucion')->name('save.institucion');
Route::get('vacante/lista','VacanteController@listaInstitucion');
Route::get('vacante/listavacante/{vacante}','VacanteController@listaVacante')->name('lista.vacantes');
Route::get('detalle/vacante/publica/{id}','VacanteController@detalleVacantePublica')->name('vacante.detalle.publica');
Route::get('formato/facebook/{codigo}','VacanteController@formatoFacebook')->name('formato.fb');
Route::get('vacante/editar', 'VacanteController@editarVacante')->name('editar.vacante');
Route::get('vacante/elimina', 'VacanteController@eliminaVacante')->name('elimina.vacante');
Route::post('vacante/update', 'VacanteController@updateVacante')->name('update.vacante');
Route::get('institucion/update/{institucion}', 'VacanteController@updateInstitucion')->name('update.institucion');
Route::post('institucion/editar', 'VacanteController@editarInstitucion')->name('editar.institucion');
Route::get('vacante/listatotal', 'VacanteController@vacanteListaTotal')->name('vacante.listatotal');


# Ruta para favoritos

Route::get('favorite/guardar/{postulante}/{pedido}', 'HomeController@guardaFavorito')->name('guarda.favorite');
Route::get('favorite/cancelar/{postulante}/{pedido}', 'HomeController@CancelarFavorito')->name('cancelar.favorite');
Route::get('favorite/guardarVacante/{postulante}/{pedido}', 'HomeController@guardaFavoritoVacante')->name('guarda.favorite.vacante');
Route::get('favorite/cancelarVacante/{postulante}/{pedido}', 'HomeController@CancelarFavoritoVacante')->name('cancelar.favorite.vacante');

# Correo de Envio Masivos

Route::get('envio/masivo','MailsController@index')->name('envio.masivo');
Route::get('ejemplo/mails','InicioController@ejemplo');

# Panel de Administrador

Route::get('listado/empresas','AdminController@listadoEmpresa')->name('empresa.listado.accede');
Route::get('listado/personas','AdminController@listadoPersona')->name('persona.listado.accede');

Route::get('extranet/web/citas','citas\CitasController@index')->name('citas');
Route::post('citas/traerServicios','citas\CitasController@traerServicios');
Route::post('citas/filtraHorarios','citas\CitasController@filtraHorarios');
Route::post('citas/validarDNI','citas\CitasController@validarDNI');
Route::post('cita/registraCita','citas\CitasController@registraCita');
Route::get('cita/confirmacion','citas\CitaConfirController@index');
Route::post('citas/enviarCorreo','citas\CitaConfirController@enviarCorreo');

Route::post('citas/dashboard/lista','citas\DashboardCitaController@index');
Route::post('citas/dashboard/verHorariosCitas','citas\DashboardCitaController@verHorariosCitas');

# Panel de Ferias

Route::get('evento/registro', 'EventoController@registroEvento')->name('register.eventos');
Route::post('evento/registro', 'EventoController@eventoSubmit')->name('eventos.submit');
Route::post('evento/update', 'EventoController@updateEvento')->name('update.submit');
Route::get('evento/listado', 'EventoController@listaEvento')->name('eventos.listado');

Route::post('citas/dashboard/buscarData','citas\DashboardCitaController@buscarDataFill');
Route::post('citas/dashboard/actualizarData','citas\DashboardCitaController@actualizarData');
Route::post('citas/dashboard/sede','citas\DashboardSedeController@index');
Route::post('citas/dashboard/servicio','citas\DashboardServicioController@index');
Route::post('citas/dashboard/reporte','citas\DashboardReporteController@index');

Route::post('citas/dashboard/rep1','citas\DashboardReporteController@rep1');
Route::post('citas/dashboard/rep2','citas\DashboardReporteController@rep2');
Route::post('citas/dashboard/rep3','citas\DashboardReporteController@rep3');
Route::post('citas/dashboard/rep4','citas\DashboardReporteController@rep4');
Route::get('citas/dashboard/gruporeporte','citas\DashboardReporteController@gruporeporte');

Route::post('citas/dashboard/listaprecen','citas\DashboardCitaPrecenciaController@index');
Route::post('citas/grupoprece','citas\DashboardCitaPrecenciaController@grupoprece');
Route::post('citas/gruposede','citas\DashboardSedeController@gruposede');
Route::post('citas/gruposervicio','citas\DashboardServicioController@gruposervicio');
Route::post('citas/dashboard/progrcalen','citas\DashboardProgHora@index');
Route::post('citas/grupopPrcale','citas\DashboardProgHora@grupopPrcale');
Route::post('citas/filtraHorariosv2','citas\DashboardProgHora@filtraHorarios');


Route::post('citas/dashboard/user','citas\DashboardUserController@index');
Route::post('citas/grupoUser','citas\DashboardUserController@grupoUser');
