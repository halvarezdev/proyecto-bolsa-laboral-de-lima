function nuevoSede(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposede",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value='';
        get('txtNombreSede').value='';
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        alert("En este momento no se puede registrar una sede. Por favor intente nuevamente más tarde.");      
    });
}
function editarSede(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposede",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value=response['obj']['sde_id'];
        get('txtNombreSede').value=response['obj']['sde_name'];
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        alert("En este momento no se puede registrar una sede. Por favor intente nuevamente más tarde.");      
    });
}
function anular_sede(btn,idsede,estado) {
    if(idsede!=''){
        swal({
            title: "¿Desea anular?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            cancelButtonClass: "btn-danger",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true
        },
        function(inputValue){
            if (inputValue === true){
                var base_url=get('base_url').value;
                var token=get('_token').value;
                $.ajax({
                    type: "POST",
                    url: base_url+"citas/gruposede",
                    data: {idsede:idsede,estado:estado,_token:token,method:'anular_sede'},
                    dataType: 'JSON',
                    beforeSend: function(){
                        $(".animationload").css({display:'block'});
                    },
                }).done(function( response, textStatus, jqXHR ){   
                    $(".animationload").css({display:'none'});
                    btns=btn.parentNode.parentNode.cells;
                    btns[2].innerHTML='<b style="color:#f2dede">Anulado</b>';
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    $(".animationload").css({display:'none'});
                    alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
                });
            }
        });
    }
}
function sedeguardar() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var nombre=get('txtNombreSede').value;   
    var idsede=get('hidValidSaveItem').value;
    if(nombre!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/gruposede",
            data: {idsede:idsede,nombre:nombre,_token:token,method:'save_sede'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
                $("#myModal").modal('hide');
                $("#bodySede").html(response['data']['table']);
                $("#table-paginaicionsede").html(response['data']['theadPagin']);
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
        }); 
    }
}


function editarSedeServcio(sede){
    $("#hidValidSaveItem_sede").val(sede)
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/traerServicios",
        data: {sede: sede,_token:token},
        dataType: 'json',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'});
            html=''; 
            if (response.status !== "0"){
                $.each( response.listaConfiguracionServicio,function (ind,valor) {
                    html+='<tr>'
                    html+='<td>'+ind+'</td>'
                    html+='<td>'+valor.names+'</td>'
                    html+='</tr>'
                });
            }
            $("#bodySedeser").html(html)
           
             
            $("#agregarpaginacionClients").tableHeadFixer({'foot' : true,'head' : true}); 
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, inténtelo más tarde.");
        });
        
}

function sedeServguardar2() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var nombre=get('serviocsesde').value;   
    var idsede=get('hidValidSaveItem_sede').value;
    if(nombre!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/gruposede",
            data: {idsede:idsede,nombre:nombre,_token:token,method:'save_sedeservi'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
               // $("#myModal").modal('hide');
                editarSedeServcio(idsede);
               
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
        }); 
    }
}