function nuevoServicio(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposervicio",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value='';
        get('txtNombre').value='';
        get('txtNombredetall').value=''
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        alert("En este momento no se puede registrar una sede. Por favor intente nuevamente más tarde.");      
    });
}
function editarServicio(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposervicio",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value=response['obj']['srv_id'];
        get('txtNombre').value=response['obj']['srv_name'];
        get('txtNombredetall').value=response['obj']['detalle'];
        
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});
        alert("En este momento no se puede registrar una sede. Por favor intente nuevamente más tarde.");      
    });
}
function anular_servicio(btn,idsede,estado) {
    if(idsede!=''){
        swal({
            title: "¿Desea anular?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            cancelButtonClass: "btn-danger",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true
        },
        function(inputValue){
            if (inputValue === true){
                var base_url=get('base_url').value;
                var token=get('_token').value;
                $.ajax({
                    type: "POST",
                    url: base_url+"citas/gruposervicio",
                    data: {idsede:idsede,estado:estado,_token:token,method:'anular_servicio'},
                    dataType: 'JSON',
                    beforeSend: function(){
                        $(".animationload").css({display:'block'});
                    },
                }).done(function( response, textStatus, jqXHR ){   
                    $(".animationload").css({display:'none'});
                    btns=btn.parentNode.parentNode.cells;
                    btns[2].innerHTML='<b style="color:#f2dede">Anulado</b>';
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    $(".animationload").css({display:'none'});
                    alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
                });
            }
        });
    }
}
function servicioguardar() {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    var nombre=get('txtNombre').value;  
    var detalle=get('txtNombredetall').value;  
    var idsede=get('hidValidSaveItem').value;
    if(nombre!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/gruposervicio",
            data: {idsede:idsede,detalle:detalle,nombre:nombre,_token:token,method:'save_servicio'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
                $("#myModal").modal('hide');
                $("#bodyServicio").html(response['data']['table']);
                $("#table-paginaicionservicio").html(response['data']['theadPagin']);
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
        }); 
    }
}
