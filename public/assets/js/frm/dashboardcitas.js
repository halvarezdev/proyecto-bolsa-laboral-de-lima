var DiasPermitido = null;
var HorarioPermitido = null;
var PrimerDiaSemana = null;
var UltimoDiaSemana = null;
var SelAnterior = null;
var conta = 0;

var gListaServicio = [];
$(document).ready(function(){
    DiasPermitido = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sabado"];
    var iniciarEn = 6;
    var finalizarEn = 19;
    HorarioPermitido = [];
    
    

    for (var i = iniciarEn; i <= finalizarEn; i++) {
        HorarioPermitido[i - iniciarEn] = pad(i) + ":00";
    }
    actualizarExtremos(new Date());
  //  actualizarComboServ();
  mueveReloj()
});
function mueveReloj(){
    momentoActual = new Date()
    hora = momentoActual.getHours()
    minuto = momentoActual.getMinutes()
    segundo = momentoActual.getSeconds()

    str_segundo = new String (segundo)
    if (str_segundo.length == 1)
       segundo = "0" + segundo

    str_minuto = new String (minuto)
    if (str_minuto.length == 1)
       minuto = "0" + minuto

    str_hora = new String (hora)
    if (str_hora.length == 1)
       hora = "0" + hora

    horaImprimible = hora + " : " + minuto + " : " + segundo

    document.form_reloj.reloj.value = horaImprimible

    setTimeout("mueveReloj()",1000)
}
Date.prototype.getWeek = function() {
    var dt = new Date(this.getFullYear(),0,1);

    return Math.ceil((((this - dt) / 86400000) + dt.getDay()+1)/7);
};

 
function actualizarExtremos(date) {
    var dt = new Date(date.getFullYear(),0,1);
   
    getWeek= Math.ceil((((date - dt) / 86400000) + dt.getDay()+1)/7);
   
    PrimerDiaSemana = getDateOfISOWeek(getWeek, date.getFullYear());
    UltimoDiaSemana = new Date(PrimerDiaSemana);
    UltimoDiaSemana.setDate(PrimerDiaSemana.getDate() + DiasPermitido.length - 1);
}
function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}

function siguienteFecha() {
    var value = setDate(document.getElementById("fecha").value);
    var lista = value.split('-');  
    var date = new Date(lista[0], lista[1] - 1, lista[2]);
    date.setDate(date.getDate() + 1);
 
    document.getElementById("fecha").value = convertDateISO(date);
    actualizarHorarioFecha();
}
function convertDateISO(date) {
    var d = new Date(date);
    return [ pad(d.getDate()),pad(d.getMonth() + 1),d.getFullYear()].join('/');
}
function pad(s) {
    return (s < 10) ? '0' + s : s;
}
function anteriorFecha() {
    var value = setDate(document.getElementById("fecha").value);
    var lista =value.split('-');
    var date = new Date(lista[0], lista[1] - 1, lista[2]);
    date.setDate(date.getDate() - 1);

    document.getElementById("fecha").value = convertDateISO(date);
    actualizarHorarioFecha();
}
function reiniciarMotivoCese(){	
	reiniciarInformacionLiquidacion();
	$("select#motivoCese").val("0");
	$("div#divMotivoCese").hide();	
}
function reiniciarInformacionLiquidacion(){

	var $opcionMotivoCese = $("div#opcionMotivoCese");
	$opcionMotivoCese.find("div#listaOpcionMotivoCese").html("");
	$opcionMotivoCese.find("button#btnMostrarHorario").prop("disabled",false);
			
	var $nota = $opcionMotivoCese.find("p#nota");
	$nota.find("span#descripcionNota").html("");
	
	$nota.hide();
	$opcionMotivoCese.hide();
	
}

function actualizarComboServ() {
	reiniciarMotivoCese();
    var sede = document.getElementById("sede").value;
    if (sede !== '0') {
        $("#divLoader").show();
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
        console.log(token)
        $.ajax({
            type: "POST",
            url: base_url+"citas/traerServicios",
            data: {sede: sede,_token:token},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0")
                    poblarComboServ(response.listaConfiguracionServicio);
                else
                    document.getElementById("servicio").innerHTML = "<option value='0'>-- NINGUN SERVICIO PARA MOSTRAR --</option>";
                
                gListaServicio = response.listaConfiguracionServicio;
                
                $("#divLoader").hide();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
            
    }
}
function poblarComboServ(rpta) {
    var contenido = "<option value='0'>-- SELECCIONE --</option>";
    $( rpta ).each(function( index ,fila) {
        contenido += "<option value='" + fila.codigo + "'>" + fila.names + "</option>";
    }); 
    document.getElementById("servicio").innerHTML = contenido;

    poblarTablaVacia();
}
function poblarTablaVacia() {
    var contenido = "<thead style='background: #f5f5f5'><th class='text-center'>Horario</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></thead>";
    contenido += "<tbody>";
    contenido += "<tr><td colspan='6' class='text-center'><span id='spanTextoTabla'>Seleccione un servicio.</span></td></tr>";
    contenido += "</tbody>";
    document.getElementById("tblHorario").innerHTML = contenido;
}
function seleccionarNuevoServicio(){
    reiniciarMotivoCese();
	poblarTablaVacia();
    actualizarHorario();
}
function actualizarHorario() {
    var servicio = document.getElementById("servicio").value;
    var sede = document.getElementById("sede").value;

    if (servicio === "0" || sede === "0") {
        poblarTablaVacia();
        return;
    }
    
    filtrarHorario(servicio, sede);
}
function obtenerConfiguracionServicio(lIdServicio){
	var objConfServicio = null;
	$.each(gListaServicio, function(index, item){
		if(item.codigo == lIdServicio){
			objConfServicio = item;
			return false;
		}
	});
	
	return objConfServicio;
}
function filtrarHorario(servicio, sede){
	$("#divLoader").show();
    var $btnMostrarHorario = $("button#btnMostrarHorario");
    var $btnSemanaAnterior = $("button#btnSemanaAnterior");
    var $btnSemananaSiguiente =$("button#btnSemananaSiguiente");
    
    $btnMostrarHorario.prop('disabled', true);
    $btnSemanaAnterior.prop('disabled', true);
    $btnSemananaSiguiente.prop('disabled', true);
    var base_url=get('base_url').value;
    var token=get('_token').value;
    var inicio=setDate(convertDateStr(PrimerDiaSemana))
    var fin=setDate(convertDateStr(UltimoDiaSemana))
    $.ajax({
        type: "POST",
        url: base_url+"citas/filtraHorarios",
        data: {_token:token,inicio: inicio, fin:fin, servicio: servicio, sede: sede},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
        	$btnMostrarHorario.prop('disabled', false);
            $btnSemanaAnterior.prop('disabled', false);
            $btnSemananaSiguiente.prop('disabled', false);
            
            if (response.status ==1) {
                // var matrizObjs = generarMatriz(response);
                // matrizObjs = refinarMatriz(matrizObjs);
                poblarTablaSemana(response.dataHora);
                console.log('')
            } else {
                poblarTablaVacia();
            }
            $("#divLoader").hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            
         
        	$btnMostrarHorario.prop('disabled', false);
            $btnSemanaAnterior.prop('disabled', false);
            $btnSemananaSiguiente.prop('disabled', false);
            
            alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");
       
    });
}

function convertDateStr(date) {
    var d = new Date(date);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}
function poblarTablaSemana(matrizObjs) {
    var contenido = "";
    // poblar tabla semana
    var diaSgte = new Date(PrimerDiaSemana);
    contenido += "<thead style='background: #f5f5f5'><tr>";
    contenido += "<th class='text-center'>Horario</th>";
    for (var i = 0; i < DiasPermitido.length; i++) {
        contenido += "<th class='text-center'>" + DiasPermitido[i] + "  ";
        contenido += pad(diaSgte.getDate()) + "/" + pad(diaSgte.getMonth() + 1) + "</th>";
        diaSgte.setDate(diaSgte.getDate() + 1);
    }
    contenido += "</tr></thead><tbody >";
 
    contenido+=matrizObjs;
    contenido += "</tbody>";
    document.getElementById("tblHorario").innerHTML = contenido;
}


function getWeeksNum(year, month) {
    var daysNum = 32 - new Date(year, month, 32).getDate(),
        fDayO = new Date(year, month, 1).getDay(),
        fDay = fDayO ? (fDayO - 1) : 6,
        weeksNum = Math.ceil((daysNum + fDay) / 7);
        console.log(weeksNum)
    return weeksNum;
}
function anteriorSemana() {
    var anteDiaSem = new Date(PrimerDiaSemana);
    anteDiaSem.setDate(anteDiaSem.getDate() - 2); // jum sat, sund

    actualizarExtremos(anteDiaSem);
    actualizarSemana();
}
function siguienteSemana() {
    var sgtDiaSem = new Date(UltimoDiaSemana);
    sgtDiaSem.setDate(sgtDiaSem.getDate() + 1); // jum sund

    actualizarExtremos(sgtDiaSem);
    actualizarSemana();
}
function actualizarSemana(){
	var servicio = document.getElementById("servicio").value;
    var sede = document.getElementById("sede").value;
    
    if (servicio === "0" || sede === "0") {
        poblarTablaVacia();
        return;
    }
    
    var lIdServicio = document.getElementById("servicio").value;
	var objConfServicio = obtenerConfiguracionServicio(lIdServicio);
	
	if(objConfServicio != null && objConfServicio.flagVerCese){
		
		var idMotivoCese = $("select#motivoCese").val();
		if(idMotivoCese == "0"){
			return;
		}
		
	}
    
    filtrarHorario(servicio, sede);
}
function seleccionarHora(idHorario, indexDia, hrStr, elem) {
    if(hrStr=='2'){
        fecha=get('fechaaa').value
    }else{
        var diaSgte = new Date();
        diaSgte.setDate(PrimerDiaSemana.getDate() + indexDia);
        
        var m = pad(diaSgte.getMonth()+1);
        if(m == "00"){
            m = "01";
        }
    
        var fecha = setDate(pad(diaSgte.getDate()) + "/" + m + "/" + diaSgte.getFullYear());
        get('fechaaa').value=fecha
        
    }
    

    get('idHorario').value=idHorario;
    var base_url=get('base_url').value;
    var sede=get('sede').value;
    var servicio=get('servicio').value;
    var _token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/dashboard/verHorariosCitas",
        data: {_token:_token,servicio:servicio,idHorario:idHorario,sede:sede,fecha: fecha},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
       
            table='<thead>';
            table+='<tr>';
            table+='<th>Hora Atendido</th>';
            table+='<th>Documento</th>';
            table+='<th>Nombres Apellidos</th>';
            table+='<th>Telefono</th>';
            table+='<th>Correo</th>';
            table+='<th>Estado</th>';
            table+='<th>Accion</th>';
            table+='</tr>';
            table+='</thead>';
            table+='<tbody>';
            table+=response['table'];
            table+='</tbody>';
            table+='<tfoot>';
            table+=response['theadPagin']
            table+='</tfoot>';
            $("#tblHorario").html(table)
            
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, reporte al administrador."); 
        });
        
   
}
function marcarInicioAsis(tipo) {
    var hoy = new Date();
    var horaini=get('iniciofecha').value;
    var finfecha=get('finfecha').value;
    var AbandonoAsis=get('AbandonoAsis').value;
    var texto=get('txtObservaciones').value;
    var tipofon=get('tipofon').value;
    var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
    
    if(tipo==1){
        $("#divInicioAsis").html(hora);  
        get('iniciofecha').value=hora
        horaini=hora;
        tipofon=5;
        get('tipofon').value=5
    }else if(tipo==2){
        $("#divFinAsis").html(hora); 
        get('finfecha').value=hora
        finfecha=hora;
        tipofon=4
        get('tipofon').value=4

    }else if(tipo==3){
        $("#divAbandonoAsis").html(hora);  
        get('AbandonoAsis').value=hora
        AbandonoAsis=hora;
        get('txtObservaciones').value='Abandonado'
        tipofon=3
        get('tipofon').value=3
    }

    var base_url=get('base_url').value;
    var sede=get('sede').value;
    var servicio=get('servicio').value;
    var _token=get('_token').value;
    var idcita=get('idcita').value;
    var idHorario=get('idHorario').value;
    
    $.ajax({
        type: "POST",
        url: base_url+"citas/dashboard/actualizarData",
        data: {idcita:idcita,texto:texto,finfecha:finfecha,AbandonoAsis:AbandonoAsis,tipofon:tipofon,horaini:horaini,tipo:tipo,_token:_token,servicio:servicio,idHorario:idHorario,sede:sede},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
            seleccionarHora(idHorario, 1, '2', '')
            if(tipo=='100'){
$("#myModal").modal('hide')
            }
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, reporte al administrador."); 
        });
}
function atende_cita(btn,id) {
    var base_url=get('base_url').value;
    var sede=get('sede').value;
    var servicio=get('servicio').value;
    var _token=get('_token').value;
    get('idcita').value=id
    $.ajax({
        type: "POST",
        url: base_url+"citas/dashboard/buscarData",
        data: {servicio:servicio,sede:sede,_token:_token,id:id},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
           
            if(response['response']['cta_horaini']==''){
              
                $("#divInicioAsis").html('<button onclick="marcarInicioAsis(1)" class="btn btn-danger bigIcon"><i class="far fa-clock"></i> </button>');
            }else{
                $("#divInicioAsis").html(response['response']['cta_horaini']);  
               
            }
            if(response['response']['cta_horafin']==''){
                $("#divFinAsis").html('<button onclick="marcarInicioAsis(2)" class="btn btn-danger bigIcon"><i class="far fa-clock"></i> </button>');
                  
            }else{
                $("#divFinAsis").html(response['response']['cta_horafin']);
               
            }

            if( response['response']['cta_abandona']==''){
               
                $("#divAbandonoAsis").html('<button onclick="marcarInicioAsis(3)" class="btn btn-warning bigIcon"><i class="fa fa-power-off"></i> </button>');  
            }else{
                $("#divAbandonoAsis").html(response['response']['cta_abandona']); 
            }
            get('iniciofecha').value=response['response']['cta_horaini'];
            get('finfecha').value=response['response']['cta_horafin'];
            get('AbandonoAsis').value=response['response']['cta_abandona'];
            get('txtObservaciones').value=response['response']['cta_descripcion'];
            get('tipofon').value=response['response']['cta_estado'];
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, reporte al administrador."); 
        });
}