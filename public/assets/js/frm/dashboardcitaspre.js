$(document).ready(function(){
    $(document).on('click', '#tblReporte .pagination a',function(event){
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var page=$(this).attr('href').split('page=')[1];
        filter_Data(page,'S');
    });
    $("#fechaInicia").datepicker({changeMonth: true,changeYear: true})
    $("#fechaFin").datepicker({changeMonth: true,changeYear: true});
    //$("#agregarpaginacionClients").tableHeadFixer({'foot' : true,'head' : true}); 
   
});

function mostrarAtencion(id) {
     
    var base_url=get('base_url').value;
    var token=get('_token').value;
    
    $.ajax({
        type: "POST",
        url: base_url+"citas/grupoprece",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
        	
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
    });
}
function marcarguardar() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var tipdoc=get('txtTipoDocAten').value;   
    var dni=get('txtDocIdentidadAten').value;   
    var nombre=get('txtNombreAten').value;   
    var appat=get('txtApPaternoAten').value;   
    var apmat=get('txtApMaternoAten').value;   
    var tefon=get('txtTelefonoAten').value;   
    var correo=get('txtCorreoAten').value;   
    var sede=get('sede').value;   
    var sexo=get('txtSexoAten').value;   
    var fechna=setDate(get('txtFecNacimientoAten').value);   
    var serv=get('txtServicioAten').value;   
    var horaini=get('txtInicioAten').value;   
    var horafin=get('txtFinAten').value;   
    var idcita=get('hidValidSaveItem').value;   
    var fecha=setDate(get('txtFinAten').value);   

    $.ajax({
        type: "POST",
        url: base_url+"citas/grupoprece",
        data: {fecha:fecha,sexo:sexo,fechna:fechna,idcita:idcita,horafin:horafin,horaini:horaini,serv:serv,sede:sede,correo:correo,tefon:tefon,apmat:apmat,appat:appat,nombre:nombre,dni:dni,tipdoc:tipdoc,_token:token,method:'save_pres'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
            $("#hidValidSaveItem").val(response['datacita']['idcita'])
            filter_Data(1)
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
    });    
}

function actualizarComboServPRE() {
	 
    var sede = document.getElementById("sede").value;
    if (sede !== '0') {
        $("#divLoader").show();
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
         
        $.ajax({
            type: "POST",
            url: base_url+"citas/traerServicios",
            data: {sede: sede,_token:token},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0")
                poblarComboServPRE(response.listaConfiguracionServicio);
                else
                    document.getElementById("txtServicioAten").innerHTML = "<option value='0'>-- NINGUN SERVICIO PARA MOSTRAR --</option>";
                
              
                 
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
            
    }
}
function poblarComboServPRE(rpta) {
    var contenido = "<option value='0'>-- SELECCIONE --</option>";
    $( rpta ).each(function( index ,fila) {
        contenido += "<option value='" + fila.codigo + "'>" + fila.names + "</option>";
    }); 
    document.getElementById("txtServicioAten").innerHTML = contenido;
 
}

function atende_cita(tbn,id) {
    var base_url=$("#base_url").val();
        var token=$("#_token").val();
     
        $.ajax({
            type: "POST",
            url: base_url+"citas/grupoprece",
            data: {id:id,_token:token,method:'nuevo'},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0"){
                    actualizarComboServPREve(response['obj']['sde_id'],response['obj']['srv_id'])
                     get('txtTipoDocAten').value=response['obj']['tipo_documento'];   
                     get('txtDocIdentidadAten').value=response['obj']['numero_documento'];  
                     get('txtNombreAten').value=response['obj']['name'];  
                    get('txtApPaternoAten').value=response['obj']['ape_pat'];     
                    get('txtApMaternoAten').value=response['obj']['ape_mat'];     
                    get('txtTelefonoAten').value=response['obj']['telefono'];     
                    get('txtCorreoAten').value=response['obj']['email'];     
                    get('sede').value=response['obj']['sde_id'];     
                    get('txtSexoAten').value=response['obj']['sexo'];     
                    get('txtFecNacimientoAten').value=response['obj']['fechana'];    
                     get('txtServicioAten').value=response['obj']['srv_id'];   
                     get('txtInicioAten').value=response['obj']['cta_horaini'];   
                     get('txtFinAten').value=response['obj']['cta_horafin'];  
                    get('hidValidSaveItem').value=response['obj']['cta_id'];    
                     get('txtFinAten').value=response['obj']['fecha']; 
                }
                
              
                 
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                
            });
}
function actualizarComboServPREve(sede,ser) {
	  
    if (sede !== '0') {
        $("#divLoader").show();
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
         
        $.ajax({
            type: "POST",
            url: base_url+"citas/traerServicios",
            data: {sede: sede,_token:token},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0")
                poblarComboServPRE(response.listaConfiguracionServicio);
                else
                    document.getElementById("txtServicioAten").innerHTML = "<option value='0'>-- NINGUN SERVICIO PARA MOSTRAR --</option>";
                
              
                $("#txtServicioAten").val(ser)    
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
            
    }
}
function filter_Data(page) {
	
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
      var daini=setDate(get('fechaInicia').value);
      var dafin=setDate(get('fechaFin').value);
        $.ajax({
            type: "POST",
            url: base_url+"citas/grupoprece",
            data: {hasta:dafin,desde:daini,page:page,_token:token,method:'filter'},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0"){
$("#bodyReporte").html(response['response']['table'])
$("#table-paginaicionorden_pago").html(response['response']['theadPagin'])
                }
                
              
                 
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
   
}