var DiasPermitido = null;
var HorarioPermitido = null;
var PrimerDiaSemana = null;
var UltimoDiaSemana = null;
var SelAnterior = null;
var conta = 0;

var gListaServicio = [];

window.onload = function () {
    DiasPermitido = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sabado"];
    var iniciarEn = 6;
    var finalizarEn = 19;
    HorarioPermitido = [];
    
    

    for (var i = iniciarEn; i <= finalizarEn; i++) {
        HorarioPermitido[i - iniciarEn] = pad(i) + ":00";
    }
    actualizarExtremos(new Date());
    actualizarComboServ();
};

Date.prototype.getWeek = function () {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
};

function actualizarComboServ() {
	reiniciarMotivoCese();
    var sede = document.getElementById("sede").value;
    if (sede !== '0') {
        $("#divLoader").show();
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
        console.log(token)
        $.ajax({
            type: "POST",
            url: base_url+"citas/traerServicios",
            data: {sede: sede,_token:token},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0")
                    poblarComboServ(response.listaConfiguracionServicio);
                else
                    document.getElementById("servicio").innerHTML = "<option value='0'>-- NINGUN SERVICIO PARA MOSTRAR --</option>";
                
                gListaServicio = response.listaConfiguracionServicio;
                
                $("#divLoader").hide();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                 
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            
        });
    }
}

function poblarComboServ(rpta) {
    var contenido = "<option value='0'>-- SELECCIONE --</option>";
    $( rpta ).each(function( index ,fila) {
        contenido += "<option value='" + fila.codigo + "'>" + fila.names + "</option>";
    }); 
    document.getElementById("servicio").innerHTML = contenido;

    poblarTablaVacia();
}

 

function actualizarHorario() {
    var servicio = document.getElementById("servicio").value;
    var sede = document.getElementById("sede").value;

    if (servicio === "0" || sede === "0") {
        poblarTablaVacia();
        return;
    }
    
    filtrarHorario(servicio, sede);
}

function filtrarHorario(servicio, sede){
	$("#divLoader").show();
    var $btnMostrarHorario = $("button#btnMostrarHorario");
    var $btnSemanaAnterior = $("button#btnSemanaAnterior");
    var $btnSemananaSiguiente =$("button#btnSemananaSiguiente");
    
    $btnMostrarHorario.prop('disabled', true);
    $btnSemanaAnterior.prop('disabled', true);
    $btnSemananaSiguiente.prop('disabled', true);
    var base_url=get('base_url').value;
    var token=get('_token').value;
    var inicio=setDate(convertDateStr(PrimerDiaSemana))
    var fin=setDate(convertDateStr(UltimoDiaSemana))
    $.ajax({
        type: "POST",
        url: base_url+"citas/filtraHorarios",
        data: {_token:token,inicio: inicio, fin:fin, servicio: servicio, sede: sede},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
        	$btnMostrarHorario.prop('disabled', false);
            $btnSemanaAnterior.prop('disabled', false);
            $btnSemananaSiguiente.prop('disabled', false);
            
            if (response.status ==1) {
                // var matrizObjs = generarMatriz(response);
                // matrizObjs = refinarMatriz(matrizObjs);
                poblarTablaSemana(response.dataHora);
                console.log('')
            } else {
                poblarTablaVacia();
            }
            $("#divLoader").hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
             
        	$btnMostrarHorario.prop('disabled', false);
            $btnSemanaAnterior.prop('disabled', false);
            $btnSemananaSiguiente.prop('disabled', false);
            
            alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");
       
    });
}

function actualizarSemana(){
	var servicio = document.getElementById("servicio").value;
    var sede = document.getElementById("sede").value;
    
    if (servicio === "0" || sede === "0") {
        poblarTablaVacia();
        return;
    }
    
    var lIdServicio = document.getElementById("servicio").value;
	var objConfServicio = obtenerConfiguracionServicio(lIdServicio);
	
	if(objConfServicio != null && objConfServicio.flagVerCese){
		
		var idMotivoCese = $("select#motivoCese").val();
		if(idMotivoCese == "0"){
			return;
		}
		
	}
    
    filtrarHorario(servicio, sede);
}

function generarMatriz(rpta) {
    var data = crearArray(rpta, "¬");
    var matrizObjs = new Array(DiasPermitido.length);
    var indexDay = -1;
    var indexHor = -1;
    var date = null;
    var diaAnte = "";
    // inicializar matriz
    for (var i = 0; i < DiasPermitido.length; i++) {
        matrizObjs[i] = new Array(HorarioPermitido.length);
        for (var j = 0; j < HorarioPermitido.length; j++) {
            matrizObjs[i][j] = new Array();
        }
    }
    for (var i = 0; i < data.length; i++) {
        // 0=idHorario, 1=dia, 2=horaInicio, 3=fin, 4=ventanilla
        var fila = data[i].split("|");
        if (fila.length < 5)
            continue;
        if (fila[1] !== diaAnte) {
            date = crearArray(fila[1], "-");
            date = new Date(date[0], date[1] - 1, date[2]);
            indexDay = date.getDay() - 1;
            diaAnte = fila[1];
        }
        indexHor = buscarHorarioIndex(fila[2]);
        matrizObjs[indexDay][indexHor].push([fila[0], fila[2], fila[3], fila[4], fila[5]]);
    }

    return matrizObjs;
}

function refinarMatriz(matrizObjs) {
    for (var i = 0; i < DiasPermitido.length; i++) {
        for (var j = 0; j < HorarioPermitido.length; j++) {
            matrizObjs[i][j] = refinarBloque(matrizObjs[i][j]);
        }
    }
    return matrizObjs;
}

function refinarBloque(bloque) {
    var result = [];
    var last, elem;
    for (var i = 0; i < bloque.length; i++) {
        // 0=idHorario, 1=horaInicio, 2=fin, 3=disponible, 4= citas asignadas a esta ventanilla en la semana
        elem = bloque[i];
        if (elem[3] === '0')
            continue;// obviar si no esta disponible
        if (result.length === 0) {
            result.push(bloque[i]);
        } else {
            last = result[result.length - 1];
            if (elem[1] === last[1]) {
	            if (elem[4] < last[4]) {
                    result[result.length - 1] = elem;
	            } else if (elem[4] === last[4]){
	                if (random(1, 10) % 2 === 0) {
    	                result[result.length - 1] = elem;
            	    }
	            } 
            } else {
                result.push(elem);
            }
        }
    }
    return result;
}

function random(min, max) {
    return Math.floor((Math.random() * max) + min);
}

function buscarHorarioIndex(hor) {
    for (var i = 0; i < HorarioPermitido.length; i++) {
        if (hor < HorarioPermitido[i])
            return i - 1;
    }
    return -1;
}
function poblarTablaVacia() {
    var contenido = "<thead style='background: #f5f5f5'><th class='text-center'>Horario</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></thead>";
    contenido += "<tbody>";
    contenido += "<tr><td colspan='6' class='text-center'><span id='spanTextoTabla'>Seleccione un servicio.</span></td></tr>";
    contenido += "</tbody>";
    document.getElementById("tblHorario").innerHTML = contenido;
}

function poblarTablaVacia2() {
    var contenido = "<thead style='background: #f5f5f5'><th class='text-center'>Horario</th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th></thead>";
    contenido += "<tbody>";
    for (var i = 0; i < HorarioPermitido.length; i++) {
        contenido += "<tr>";
        contenido += "<td class='text-center'><b>" + HorarioPermitido[i] + "</b></td>";
        contenido += "</td><td></td><td></td><td></td><td></td><td></td></tr>";
    }
    contenido += "</tbody>";
    document.getElementById("tblHorario").innerHTML = contenido;
}

function poblarTablaSemana(matrizObjs) {
    var contenido = "";
    // poblar tabla semana
    var diaSgte = new Date(PrimerDiaSemana);
    contenido += "<thead style='background: #f5f5f5'><tr>";
    contenido += "<th class='text-center'>Horario</th>";
    for (var i = 0; i < DiasPermitido.length; i++) {
        contenido += "<th class='text-center'>" + DiasPermitido[i] + "  ";
        contenido += pad(diaSgte.getDate()) + "/" + pad(diaSgte.getMonth() + 1) + "</th>";
        diaSgte.setDate(diaSgte.getDate() + 1);
    }
    contenido += "</tr></thead><tbody >";
 
    contenido+=matrizObjs;
    contenido += "</tbody>";
    document.getElementById("tblHorario").innerHTML = contenido;
}

function seleccionarHora(idHorario, indexDia, hrStr, elem) {
    var diaSgte = new Date();
     
    diaSgte.setDate(PrimerDiaSemana.getDate() + indexDia);
    
    var m = pad(diaSgte.getMonth()+1);
    if(m == "00"){
    	m = "01";
    }

//    var fecha = pad(diaSgte.getDate()) + "/" + pad(diaSgte.getMonth() + 1) + "/" + diaSgte.getFullYear();
    var fecha = pad(diaSgte.getDate()) + "/" + m + "/" + diaSgte.getFullYear();
    document.getElementById("txtfecha").value = DiasPermitido[indexDia] + "  " + fecha + ",    " + hrStr;
    document.getElementById("horario").value = idHorario;
    document.getElementById("fecha").value = fecha;

    $(SelAnterior).removeClass("hrcitado");
    $(SelAnterior).addClass("hrlibre");

    $(elem).removeClass("hrlibre");
    $(elem).addClass("hrcitado");

    SelAnterior = elem;

    $('html,body').animate({
        scrollTop: $("#abajoFrm").offset().top
    }, 1000);
}

function siguienteSemana() {
    var sgtDiaSem = new Date(UltimoDiaSemana);
    sgtDiaSem.setDate(sgtDiaSem.getDate() + 1); // jum sund

    actualizarExtremos(sgtDiaSem);
    actualizarSemana();
}

function anteriorSemana() {
    var anteDiaSem = new Date(PrimerDiaSemana);
    anteDiaSem.setDate(anteDiaSem.getDate() - 2); // jum sat, sund

    actualizarExtremos(anteDiaSem);
    actualizarSemana();
}

function actualizarExtremos(date) {
    console.log(date.getWeek())
    PrimerDiaSemana = getDateOfISOWeek(date.getWeek(), date.getFullYear());
    UltimoDiaSemana = new Date(PrimerDiaSemana);
    UltimoDiaSemana.setDate(PrimerDiaSemana.getDate() + DiasPermitido.length - 1);
}

// ///////// funciones de apoyo ///////////
function crearArray(lista, separador) {
    var data = lista.split(separador);
    if (data.length === 1 && data[0] === "")
        data = [];
    return data;
}

function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    console.log(simple)
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}

function getDayName(y, m, d) {
    var date = new Date(y, m - 1, d);
    var weekday = new Array(7);
    weekday[0] = "Domingo";
    weekday[1] = "Lunes";
    weekday[2] = "Martes";
    weekday[3] = "Miércoles";
    weekday[4] = "Jueves";
    weekday[5] = "Viernes";
    weekday[6] = "Sábado";
    return weekday[date.getDay()];
}

function convertDateStr(date) {
    var d = new Date(date);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

function pad(s) {
    return (s < 10) ? '0' + s : s;
}

function formatTime12(time24) {
    var lista = crearArray(time24, ":");
    var hours = parseInt(lista[0]);
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours > 12 ? hours - 12 : hours;
    return pad(hours) + ":" + lista[1] + " " + ampm;
}

function validarSoloNumero(texto){
    return /^\d+$/.test(texto); 
 }

function habilitarLecturaDatoPersonal(flagLectura){
	document.getElementById("apePat").readOnly = flagLectura;
    document.getElementById("apeMat").readOnly = flagLectura;
    document.getElementById("nombre").readOnly = flagLectura;
}
function mostrarEnlaceServicio() {
      $('#myModal_detalle').modal('show');
    var base_url=get('base_url').value;
    var _token=get('_token').value;
    var servicio=get('servicio').value;
    if(servicio==''){
        return false
    }
    if(servicio=='0'){
        return false
    }
    // $.ajax({
    //     type: "POST",
    //     url: base_url+"citas/BuscarDettaleServi",
    //     data: {servicio:servicio,_token:_token},
    //     dataType: 'JSON',
    //     beforeSend: function(){
    //         $(".animationload").css({display:'block'});
    //        },
    //     }).done(function( response, textStatus, jqXHR ){   
    //         $(".animationload").css({display:'none'}); 
    //         $("#myModal .modal-title").html(response['datais']['names'])
    //         $("#myModal .modal-body").html(response['datais']['detalle'])
    //     }).fail(function(jqXHR, ajaxOptions, thrownError){
    //         $(".animationload").css({display:'none'});  
    // }); 
}
// ///////////// FORMULARIO /////////////////////////
function buscarDNI() {
    var dni = document.getElementById("numDoc").value;
    if (dni === '' || dni.length < 8 || !validarSoloNumero(dni)) {
        swal("Por favor ingrese los 8 dígitos de su DNI.","", "info");
    } else {
    	limpiarDatosPersonales();
    	habilitarLecturaDatoPersonal(true);
        $("#divLoader2").show();
        var $btnBuscar = $("#btnBuscar");
        $btnBuscar.prop('disabled', true);
        var base_url=get('base_url').value;
        var _token=get('_token').value;
        $.ajax({
            type: "POST",
            url: base_url+"citas/validarDNI",
            data: {_token:_token,dni: dni.trim()},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
            	$btnBuscar.prop('disabled', false);
                if (response.status == "0") {
                    swal("En este momento no se pudo validar el DNI. Por favor ingrese sus datos personales.", "", "info")
                    
                    habilitarLecturaDatoPersonal(false);
                } else if (response.status === "2") {
                swal("El número de DNI es incorrecto, por favor asegurese de registrar correctamente el DNI.", "", "info")

                                     
                } else {
                    poblarCampos(response.row_data);
                }
                $("#divLoader2").hide();
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                 
                $btnBuscar.prop('disabled', false);
                swal("En este momento no se pudo validar el DNI. Por favor intente nuevamente más tarde.", "", "info")
                
                
                $("#divLoader2").hide();
          
        });
    }
}

function poblarCampos(lista) {
  
    document.getElementById("apePat").value = lista.ape_pat;
    document.getElementById("apeMat").value = lista.ape_mat;
    document.getElementById("nombre").value = lista.name;
    document.getElementById("sexo").value = '';
    document.getElementById("sexo").value = '';
    document.getElementById("correo").value = lista.email;
    document.getElementById("fecNacimiento").value = '';
    
    // var flagActualizado = lista[6];
    // if(flagActualizado == "false"){
    // 	document.getElementById("apePat").readOnly = false;
    //     document.getElementById("apeMat").readOnly = false;
    //     document.getElementById("nombre").readOnly = false;
    // }
}

function tipoDocumento() {
    var selectedValue = document.getElementById("tipoDoc").value;
    var num = document.getElementById("numDoc");
    num.value = "";
    if (selectedValue === "1")
    {
        num.maxLength = 8;
        document.getElementById("apePat").readOnly = true;
        document.getElementById("apeMat").readOnly = true;
        document.getElementById("nombre").readOnly = true;
        document.getElementById("btnBuscar").disabled = false;

    } else {
        if (selectedValue === "3" || selectedValue === "4") {
            num.maxLength = 15;
        } else {
            num.maxLength = 12;
        }
        document.getElementById("apePat").readOnly = false;
        document.getElementById("apeMat").readOnly = false;
        document.getElementById("nombre").readOnly = false;
        document.getElementById("btnBuscar").disabled = true;
    }

    limpiarDatosPersonales();
}

function limpiarDatosPersonales(){
	document.getElementById("apePat").value = "";
    document.getElementById("apeMat").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("sexo").value = ""
    document.getElementById("fecNacimiento").value = ""
}

function registrarCita() {
	// if(conta % 2 === 0){
	//     mostarMensajeClicEnReservar();
	// 	conta = conta +1	;
	// 	return conta;
	// }			

    var servSel = document.getElementById("servicio");
    var servicio = servSel.options[servSel.selectedIndex].value;
    var horario = document.getElementById("horario").value;
    var dia = document.getElementById("fecha").value;
    
    var d = dia.substring(0,2);
    var m = dia.substring(3,5);
    var a = dia.substring(6,10);
    if(m == "00"){
    	m = "01";
    }
    
    var servTipoDoc = document.getElementById("tipoDoc");
    var tipoDoc = servTipoDoc.options[servTipoDoc.selectedIndex].value;
    var numDoc = document.getElementById("numDoc").value.trim();
    var nombre = document.getElementById("nombre").value.trim();
    var apePat = document.getElementById("apePat").value.trim();
    var apeMat = document.getElementById("apeMat").value.trim();
    var correo = document.getElementById("correo").value.trim();
    var telefono = document.getElementById("telefono").value.trim();
    var sexo = document.getElementById("sexo").value.trim();
    var fecNac = document.getElementById("fecNacimiento").value.trim();
    var mailing = document.getElementById("chkMailing").checked;
	var dom = correo.split('@')[1];
	
    if (horario < 1) {
        swal("Por favor seleccione un horario de atención", "", "info")
    } else if (tipoDoc === "0" || numDoc === "" || nombre === "") {
        swal("Por favor termine de llenar los datos personales", "", "info")
    } else if (tipoDoc === "1" && numDoc.length < 8) {
        swal("Por favor ingrese los 8 dígitos de su DNI", "", "info")
    } else if (tipoDoc === "1" && apeMat === "") {
        alert("Por favor ingrese el apellido materno", "", "info")
    } else if (correo === ''){
    	swal("Por favor ingrese su correo electrónico, es necesario para poder realizar su atención virtual.", "", "info")
    } else if (correo.length > 0 && !validateEmail(correo)) {
        swal("Por favor ingrese un correo electrónico válido", "", "info")
/*    } else if (dom != 'gmail.com'){
    	swal("Por favor ingrese un correo electrónico GMAIL", "", "info")*/
    } else if (telefono.length === 0) {
        swal("Por favor ingrese un telefono de contacto ", "", "info")
    } else if (telefono.length < 7 || telefono.length > 15) {
        swal("Por favor ingrese un número de telefono válido", "", "info")
    } else {

       
        
        var $btnGuardar = $("#btnGuardar");
        var $imgLoaderReservar = $("#imgLoaderReservar");
                
        $btnGuardar.prop('disabled', true);
        $imgLoaderReservar.show();
        var base_url=get('base_url').value
        var _token=get('_token').value
        var txtfecha=get('txtfecha').value;
        var sede=get('sede').value;
        $.ajax({
            type: "POST",
            url: base_url+"cita/registraCita",
            data: {_token:_token,txtfecha:txtfecha,sede:sede,fecha:a+'-'+m+'-'+d,servicio:servicio,horario:horario,mailing:mailing,fecNac:fecNac,sexo:sexo,telefono:telefono,apeMat:apeMat,correo:correo,tipoDoc: tipoDoc, numDoc: numDoc,nombre:nombre,apePat:apePat},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
            
                if (response.status == 1) {
                    //swal("En este momento no se puede registrar una cita. Intente nuevamente más tarde", "", "info")
                    if (response.datacita.result == 7) {
                        swal("Este horario ya fue ocupado, seleccione otro horario disponible.", "", "info")
                    } else if (response.datacita.result ==9) {
                        swal("Esta persona con identificación " + numDoc + " ya tiene reservada una cita para ésta fecha. Seleccione otro día.", "", "info")
                    } else {
                       window.location.href = base_url+"cita/confirmacion?id=" + response.datacita.idcita;
                    }
                    console.log(response.datacita.result)
                }
                $imgLoaderReservar.hide();
                $btnGuardar.prop('disabled', false);
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                
                swal("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.", "", "info")
                $imgLoaderReservar.hide();
                $btnGuardar.prop('disabled', false);
             
        });
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function habilitarBotonGuardar(elem) {
    var but = document.getElementById("btnGuardar");
    but.disabled = !elem.checked;
}

 
 

function mostrarHorarioDisponible(){	
	actualizarExtremos(new Date());
	actualizarHorario();
}

function reiniciarMotivoCese(){	
	reiniciarInformacionLiquidacion();
	$("select#motivoCese").val("0");
	$("div#divMotivoCese").hide();	
}

function reiniciarInformacionLiquidacion(){

	var $opcionMotivoCese = $("div#opcionMotivoCese");
	$opcionMotivoCese.find("div#listaOpcionMotivoCese").html("");
	$opcionMotivoCese.find("button#btnMostrarHorario").prop("disabled",false);
			
	var $nota = $opcionMotivoCese.find("p#nota");
	$nota.find("span#descripcionNota").html("");
	
	$nota.hide();
	$opcionMotivoCese.hide();
	
}



function obtenerConfiguracionServicio(lIdServicio){
	var objConfServicio = null;
	$.each(gListaServicio, function(index, item){
		if(item.codigo == lIdServicio){
			objConfServicio = item;
			return false;
		}
	});
	
	return objConfServicio;
}

function seleccionarNuevoServicio(){
	reiniciarMotivoCese();
	poblarTablaVacia();
	
	 
		actualizarHorario();
	 
	
}

 


