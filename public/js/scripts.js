$(document).ready(function(){
	pasarAVista();

	$(document).on('click', '#btnContinuar1', function(e){

		ValidarForm1();
	  	return false;
	});
	 $(document).on('keypress', 'input', function(e) {
      // && e.target.type !== 'submit'
      if(e.keyCode == 13) {
        e.preventDefault();
        var inputs = $(this).closest('form').find(':input:visible');         
          console.log(e.target.type);
        inputs.eq( inputs.index(this)+ 1 ).focus();

      }

    });
	$(document).on('click', '#btnContinuar2', function (e) {
	  ValidarForm2();
	  return false;
	});
	$(document).on('click', '#btnRegresar2', function (e) {
	 // $('#nav-tab a[href="#nav-personales"]').tab('show');
	   $('#nav-laboral').removeClass('show');
		$('#nav-laboral').removeClass('active');
		$('#nav-personales').addClass('show active');
	  return false;
	});

	$(document).on('click', '#btnFinal', function (e) {
		dataInfomacion=JSON.stringify(comvertirJson());
		showCodeReport('frmPreview',dataInfomacion,'POST','view-cv');
	 	 $.ajax({
            type: 'POST',
            url: "process-json",
            data:  dataInfomacion,
            contentType: "application/json",
            dataType: 'JSON',
            success: function(json) { 
            	console.log(json);
				$("_id").val(json['_id'])
            }           
        });
	  return false;
	});

	$(document).on('click', '#btnContinuar3', function (e) {
	  ValidarForm3();
	  return false;
	});
	$(document).on('click', '#btnRegresar3', function (e) {
	  //$('#nav-tab a[href="#nav-laboral"]').tab('show');
	    $('#nav-academica').removeClass('show');
		$('#nav-academica').removeClass('active');
		$('#nav-laboral').addClass('show active');
	  return false;
	});
	$(document).on('click', '#btnRegresar4', function (e) {
		$('#nav-referencias').removeClass('show');
		$('#nav-referencias').removeClass('active');
		$('#nav-academica').addClass('show active');
	  //$('#nav-tab a[href="#nav-academica"]').tab('show');
	  return false;
	});
	$(document).on('submit', '#frmPreview', function (e) {
		ValidarForm4();
		$('input[type=text]').val (function () {
		    return this.value.toUpperCase();
		})
		$("#frmPreview").submit();
  		return false;
	});
	// $(document).on('change', '#txtDNI', function (e) {
	// 	if($(this).val().length==8){
	// 		getDNI($(this).val());
	// 	}
 //  		return false;
	// });
	$(document).on('change', '#txtEmail', function (e) {
		if(!isEmail($(this).val())){
			$(this).val("");
			$(".mje").show();
		}
	});
	$(document).on('click', '#btnImprimir', function (e) {
		$("#printzone").printThis();
		return false;
	});
	$(document).on('click', '#btnDescargar', function (e) {
		$("#frmCV").submit();
  		return false;
	});
	$(document).on('click', '#btnEmail', function (e) {
		//$("#frmCVemail").submit();
		$(".loading").show();
		var form = $('#frmCVemail');
		$.ajax( {
	      type: "POST",
	      url: form.attr( 'action' ),
	      data: form.serialize(),
	      success: function( response ) {
	      	$(".loading").hide();
	      	alert("Se envió el correo correctamente.");
	      }
	    } );
  		return false;
	});
	$(document).on('change', '#chkFoto', function (e) {
		ChkFoto();
	});
	$(document).on('change', '#chkExp1', function (e) {
		Exp1();
	});
	$(document).on('change', '#chkExp2', function (e) {
		Exp2();
	});
	$(document).on('change', '#chkExp3', function (e) {
		Exp3();
	});
	$(document).on('change', '#chkSup', function (e) {
		valSup();
	});
	$(document).on('change', '#chkOtros', function (e) {
		valOtros();
	});
	ChkFoto();
	Exp1();
	Exp2();
	valSup();
	valOtros();

	$('.date-pickerf').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        maxDate: '0',
        currentText: 'Hoy',
        closeText: 'Aceptar',
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dateFormat: 'mm/yy',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
    $('.date-pickerf2').datepicker( {
    	changeMonth: false,
        changeYear: true,
        showButtonPanel: true,
        maxDate: '0',
        closeText: 'Aceptar',
        dateFormat: 'yy',
        onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, 1));
        }
    });
    $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
   };
   $.datepicker.setDefaults($.datepicker.regional['es']);

    $(document).on("keypress", ":input:not(textarea):not([type=submit])", function(event) {
	    if (event.keyCode == 13) {
	        event.preventDefault();
	    }
	});
	$("#txtDNI, #txtEdad").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
  	$(document).on("focus","#txtFunciones1x",function() {
	    if(document.getElementById('txtFunciones1x').value === ''){
	        document.getElementById('txtFunciones1x').value +='• ';
		}
	});
	$(document).on("keyup","#txtFunciones1x",function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	        document.getElementById('txtFunciones1x').value +='• ';
		}
		var txtval = document.getElementById('txtFunciones1x').value;
		if(txtval.substr(txtval.length - 1) == '\n'){
			document.getElementById('txtFunciones1x').value = txtval.substring(0,txtval.length - 1);
		}
		$("#txtFunciones1").val($("#txtFunciones1x").val().replace(/•/g,"<br>•"));
	});
  	$("#txtFunciones2x").focus(function() {
	    if(document.getElementById('txtFunciones2x').value === ''){
	        document.getElementById('txtFunciones2x').value +='• ';
		}
	});
	$("#txtFunciones2x").keyup(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13'){
	        document.getElementById('txtFunciones2x').value +='• ';
		}
		var txtval = document.getElementById('txtFunciones2x').value;
		if(txtval.substr(txtval.length - 1) == '\n'){
			document.getElementById('txtFunciones2x').value = txtval.substring(0,txtval.length - 1);
		}
		$("#txtFunciones2").val($("#txtFunciones2x").val().replace(/•/g,"<br>•"));
	});
});
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function getDNI(dni){

}
function getBuscarDni(dni) {
	_token=$("#_token").val();
		$.ajax( {
	      type: "POST",
	      url: 'buscar_dni',
	      data: {dni:dni,_token:_token},
	      dataType:'JSON',
	      success: function( response ) {
	      	if (response['s']=='true' || response['s']==true) {
	      		localStorage.setItem('accede_informaicon',JSON.stringify(response))
	    		pasarAVista()
	    	}else{
	    		$(".limpiar").val('')
	    	}
	      }
	    } );
}
function encodeImagetoBase64(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    //$(".link").attr("href",reader.result);
    $("#txtFoto").val(reader.result);
    $("#fotoCV").attr("src",reader.result).show();//.css("height","150px");
    $(".removerText").remove()
  }
  reader.readAsDataURL(file);
}
function encodeImagetoBase64_2() {
	element=$()
	encodeImagetoBase64(element)
}
function ValidarForm1(){
	if($("#chkFoto").is(":checked")){
		if (!validacionFRM1()){
			return false;
		} 
		getFormularioUno();
	}else{
		
		if(validacionFRM1()==true){
			if ($("#txtFoto").val()=="") {
				console.log('echo')
			$(".mje").html('Falta Subir Foto').show();
			}else{
				getFormularioUno();
				var json=comvertirJson();
				localStorage.setItem('accede_informaicon',JSON.stringify(json))
				return false;
			}
		}else{
			return false;
		}
		getFormularioUno();
	}
	var json=comvertirJson();
	localStorage.setItem('accede_informaicon',JSON.stringify(json))
}

function validacionFRM1() {
	if ($("#txtDNI").val()=="") {
			$(".mje").html('Tiene que Ingresar DNI').show();
			$("#txtDNI").focus();
			return false;
	}
	if ($("#txtNombres").val()=="") {
		$(".mje").html('Tiene que Ingresar Nombre').show();
			$("#txtNombres").focus();

		return false;
	}
	if ($("#txtEdad").val()=="") {
		$(".mje").html('Tiene que Ingresar Edad').show();
		$("#txtEdad").focus();
		return false;
	}
	if ($("#txtEdad").val()=="") {
		$(".mje").html('Tiene que Ingresar Dirección').show();
		$("#txtEdad").focus();
		return false;
	}
	if ($("#txtTelf").val()=="") {
		$(".mje").html('Tiene que Ingresar Telefono').show();
		$("#txtTelf").focus();
		return false;
	}

	if($("#txtDNI").val().length!==8){
		$(".mje").html('DNI Errada').show();
		$("#txtDNI").focus();
			return false;	
	}
	return true;
}
function getFormularioUno() {
	$(".mje").html('* Debe ingresar los datos correctamente.').hide();
	$('#nav-personales').removeClass('show');
	$('#nav-personales').removeClass('active');
	$('#nav-laboral').addClass('show active');
	$("#btn2").addClass('btn_color').removeClass('btn-default');
	return true;
}
function ValidarForm2(){
	if($("#chkExp1").is(":checked")){
		$(".mje2").hide();
		$("#btn3").addClass('btn_color').removeClass('btn-default')
					$('#nav-academica').addClass('show active');

					$('#nav-laboral').removeClass('show');
					$('#nav-laboral').removeClass('active');
					var json=comvertirJson();
	localStorage.setItem('accede_informaicon',JSON.stringify(json))
	}else{
		if(!validate2empresa1()){
			$(".mje2").show();
		}else{
			if($("#chkExp2").is(":checked")){
				$(".mje2").hide();
				$("#btn3").addClass('btn_color').removeClass('btn-default')
				$('#nav-academica').addClass('show active');
				$('#nav-laboral').removeClass('show');
				$('#nav-laboral').removeClass('active');
				var json=comvertirJson();
	localStorage.setItem('accede_informaicon',JSON.stringify(json))
			}else{
				if(!validate2empresa2()){
					$(".mje2").show();
				}else{
					if($("#chkExp2").is(":checked")){
						$(".mje2").hide();
						$("#btn3").addClass('btn_color').removeClass('btn-default')
						$('#nav-academica').addClass('show active');
						$('#nav-laboral').removeClass('show');
						$('#nav-laboral').removeClass('active');
					}else{
			
						if($("#chkExp3").is(":checked")){

						$(".mje2").hide();
							$("#btn3").addClass('btn_color').removeClass('btn-default')
							$('#nav-academica').addClass('show active');

							$('#nav-laboral').removeClass('show');
							$('#nav-laboral').removeClass('active');
							var json=comvertirJson();
	localStorage.setItem('accede_informaicon',JSON.stringify(json))
					}else{
						if(!validate2empresa3()){
							$(".mje2").show();
						}else{
							$(".mje2").hide();
							$("#btn3").addClass('btn_color').removeClass('btn-default')
							$('#nav-academica').addClass('show active');

							$('#nav-laboral').removeClass('show');
							$('#nav-laboral').removeClass('active');
							var json=comvertirJson();
	localStorage.setItem('accede_informaicon',JSON.stringify(json))
						}	
					}
					}
				}
			}
		}
	}
	
}
function validate2empresa1(argument) {
	if($("#txtEmpresa1").val()==""){
		$("#txtEmpresa1").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtCargo1").val()==""){
		$("#txtCargo1").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtFunciones1x").val()==""){
		$("#txtFunciones1x").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtDesde1").val()==""){
		$("#txtDesde1").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtHasta1").val()==""){
		$("#txtHasta1").focus();
		$(".mje2").show();
		return false
	}
	return true;
}
function validate2empresa2() {
	if($("#txtEmpresa2").val()==""){
		$("#txtEmpresa2").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtCargo2").val()==""){
		$("#txtCargo2").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtFunciones2x").val()==""){
		$("#txtFunciones2x").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtDesde2").val()==""){
		$("#txtDesde2").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtHasta2").val()==""){
		$("#txtHasta2").focus();
		$(".mje2").show();
		return false
	}
	return true;
}
function validate2empresa3() {

	if($("#txtEmpresa3").val()==""){
		$("#txtEmpresa3").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtCargo3").val()==""){
		$("#txtCargo3").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtFunciones3x").val()==""){
		$("#txtFunciones3x").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtDesde3").val()==""){
		$("#txtDesde3").focus();
		$(".mje2").show();
		return false;
	}
	if($("#txtHasta3").val()==""){
		$("#txtHasta3").focus();
		$(".mje2").show();
		return false
	}
	return true;
}
function ValidarForm3(){
	if(!validatePrima()){
		$(".mje3").show();
	}else{
		if(!validateSegun()){
			$(".mje3").show();
		}else{
			if(!$("#chkSup").is(":checked")){
				if(!carreraVlida()){
					$(".mje3").show();
				}else{
					if($("#chkOtros").is(":checked")){
					pasarA4() 
					}else{
						if($("#txtECAP").val()=="" || $("#txtCurso").val()=="" || $("#txtDesdeOtros").val()=="" || $("#txtHastaOtros").val()==""){
							$(".mje3").show();
						}else{
							pasarA4() 
						}
					}
				}
			}else{
				if($("#chkOtros").is(":checked")){
					pasarA4() 
				}else{
					if($("#txtECAP").val()=="" || $("#txtCurso").val()=="" || $("#txtDesdeOtros").val()=="" || $("#txtHastaOtros").val()==""){
						$(".mje3").show();
					}else{
						pasarA4() 
					}
				}
			}
		}
	}		
}
function pasarA4() {
	$(".mje3").hide();
	$('#nav-academica').removeClass('show');
	$('#nav-academica').removeClass('active');
	$('#nav-referencias').addClass('show active');
	$("#btn4").addClass('btn_color').removeClass('btn-default');
	var json=comvertirJson();

	localStorage.setItem('accede_informaicon',JSON.stringify(json))
}
function carreraVlida() {
	if($("#txtCarrera").val()==""){
		$("#txtCarrera").focus();
		$(".mje3").show();
		return false;
	} 
	if($("#txtInsTec").val()==""){
		$("#txtInsTec").focus();

		$(".mje3").show();
		return false;
	}
	if($("#txtDesdeTec1").val()==""){
		$("#txtDesdeTec1").focus();
		$(".mje3").show();
		return false;
	}
	if($("#txtHastaTec1").val()==""){
		$("#txtHastaTec1").focus();
		$(".mje3").show();
		return false;
	}
	return true;
}
function validatePrima() {
	if($("#txtInsPrim").val()==""){$(".mje3").show();
		$("#txtInsPrim").focus();
		return false;
	} 
	if($("#txtDesdePrim").val()==""){$(".mje3").show();
		$("#txtDesdePrim").focus();
		return false;
	} 
	if($("#txtHastaPrim").val()==""){$(".mje3").show();
		$("#txtHastaPrim").focus();
		return false;
	}
	return true;
}

function validateSegun() {
	if($("#txtInsSec").val()==""){$(".mje3").show();
		$("#txtInsSec").focus();
		return false;
	}
	if($("#txtDesdeSec").val()==""){$(".mje3").show();
		$("#txtDesdeSec").focus();
		return false;
	}
	if($("#txtHastaSec").val()==""){$(".mje3").show();
		$("#txtHastaSec").focus();
		return false;
	}
		return true;
}
function showCodeReport (pForm, params, method2,controlador) {

    if (typeof method2 == 'undefined') {
        method2 = 'GET';
    }
    var form = document.getElementById(pForm);
    form.method = method2;
    if(method2=='POST'){
        form.target="_blank";
        form.action = controlador;
        form.submit();
    } else{             
        window.open(bas+controlador+'?'+ params);
    }      
}
function ValidarForm4(){
	if($("#txtRefPers").val()=="" || $("#txtRefOrg").val()=="" || $("#txtRefTelf").val()==""){
		$(".mje4").show();
	}else{
		$(".mje4").hide();
		$("#frmPreview").submit();
	}
}
function ChkFoto(){
	if($("#chkFoto").is(":checked")){
		$("input[name=image]").val("").prop("disabled",true);
		$("#txtFoto").val("");
		$("#fotoCV").hide();
	}else{
		$("input[name=image]").prop("disabled",false);
	}
}
function Exp1(){
	if($("#chkExp1").is(":checked")){
		$("#txtEmpresa1").val('').prop("disabled",true);
		$("#txtCargo1").val('').prop("disabled",true);
		$("#txtFunciones1x").val('').prop("disabled",true);
		$("#txtDesde1").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtHasta1").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtEmpresa2").val('').prop("disabled",true);
		$("#txtCargo2").val('').prop("disabled",true);
		$("#txtFunciones2x").val('').prop("disabled",true);
		$("#txtDesde2").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtHasta2").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#chkExp2").val('').prop("disabled",true);
		$("#chkExp3").val('').prop("disabled",true);
		$("#chkExp2").prop("checked",true);
		$("#chkExp3").prop("checked",true);
		$("#txtEmpresa3").val('').prop("disabled",true);
		$("#txtCargo3").val('').prop("disabled",true);
		$("#txtFunciones3x").val('').prop("disabled",true);
		$("#txtDesde3").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtHasta3").val('').prop("disabled",true).css("background","rgb(233,236,239)");
	}else{
		$("#txtEmpresa1").prop("disabled",false);
		$("#txtCargo1").prop("disabled",false);
		$("#txtFunciones1x").prop("disabled",false);
		$("#txtDesde1").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtHasta1").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtEmpresa2").prop("disabled",false);
		$("#txtCargo2").prop("disabled",false);
		$("#txtFunciones2x").prop("disabled",false);
		$("#txtDesde2").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtHasta2").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#chkExp2").prop("disabled",false);
		$("#chkExp3").prop("disabled",false);
		$("#chkExp2").prop("checked",false);
		$("#chkExp3").prop("checked",false);
		$("#txtEmpresa3").prop("disabled",false);
		$("#txtCargo3").prop("disabled",false);
		$("#txtFunciones3x").prop("disabled",false);
		$("#txtDesde3").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtHasta3").prop("disabled",false).css("background","rgb(255,255,255)");
	}
}
function Exp2(){
	if($("#chkExp2").is(":checked")){
		$("#txtEmpresa2").val('').prop("disabled",true);
		$("#txtCargo2").val('').prop("disabled",true);
		$("#txtFunciones2x").val('').prop("disabled",true);
		$("#txtDesde2").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtHasta2").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtEmpresa3").val('').prop("disabled",true);
		$("#txtCargo3").val('').prop("disabled",true);
		$("#txtFunciones3x").val('').prop("disabled",true);
		$("#chkExp3").prop("disabled",true);
			$("#chkExp3").prop("checked",true);
		$("#txtDesde3").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtHasta3").val('').prop("disabled",true).css("background","rgb(233,236,239)");
	}else{
		$("#txtEmpresa2").prop("disabled",false);
		$("#txtCargo2").prop("disabled",false);
		$("#txtFunciones2x").prop("disabled",false);
		$("#txtDesde2").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtHasta2").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtEmpresa3").prop("disabled",false);
		$("#txtCargo3").prop("disabled",false);
		$("#txtFunciones3x").prop("disabled",false);
		$("#chkExp3").prop("disabled",false);
		$("#chkExp3").prop("checked",false);
		$("#txtDesde3").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtHasta3").prop("disabled",false).css("background","rgb(255,255,255)");
	}
}
function Exp3(){
	if($("#chkExp3").is(":checked")){
		$("#txtEmpresa3").val('').prop("disabled",true);
		$("#txtCargo3").val('').prop("disabled",true);
		$("#txtFunciones3x").val('').prop("disabled",true);
		$("#txtDesde3").val('').prop("disabled",true).css("background","rgb(233,236,239)");
		$("#txtHasta3").val('').prop("disabled",true).css("background","rgb(233,236,239)");
	}else{
		$("#txtEmpresa3").prop("disabled",false);
		$("#txtCargo3").prop("disabled",false);
		$("#txtFunciones3x").prop("disabled",false);
		$("#txtDesde3").prop("disabled",false).css("background","rgb(255,255,255)");
		$("#txtHasta3").prop("disabled",false).css("background","rgb(255,255,255)");
	}
}
function valSup(){
	if($("#chkSup").is(":checked")){
		$("#txtCarrera").val('').prop("disabled",true);
		$("#txtInsTec").val('').prop("disabled",true);
		$("#txtDesdeTec1").val('').prop("disabled",true);
		$("#txtHastaTec1").val('').prop("disabled",true);
	}else{
		$("#txtCarrera").prop("disabled",false);
		$("#txtInsTec").prop("disabled",false);
		$("#txtDesdeTec1").prop("disabled",false);
		$("#txtHastaTec1").prop("disabled",false);
	}
}
function valOtros(){
	if($("#chkOtros").is(":checked")){
		$("#txtECAP").val('').prop("disabled",true);
		$("#txtCurso").val('').prop("disabled",true);
		$("#txtDesdeOtros").val('').prop("disabled",true);
		$("#txtHastaOtros").val('').prop("disabled",true);
	}else{
		$("#txtECAP").prop("disabled",false);
		$("#txtCurso").prop("disabled",false);
		$("#txtDesdeOtros").prop("disabled",false);
		$("#txtHastaOtros").prop("disabled",false);
	}
}

//btnFinal
function comvertirJson() {
	var dataInformacion = {}
	dataPersonales = [];
	dataExtraNoDB = [];
	dataFormacionAcademica=[];
	dataReferencia=[];

	var txtDNI=$("#txtDNI").val();
	var txtNombres=$("#txtNombres").val();
	var txtEdad=$("#txtEdad").val();
	var txtDireccion=$("#txtDireccion").val();
	var txtTelf=$("#txtTelf").val();
	var txtEmail=$("#txtEmail").val();
	var txtResumen=$("#txtResumen").val();
	var chkFoto=$("#chkFoto").val();


	var txtEmpresa1=$("#txtEmpresa1").val();
	var txtCargo1=$("#txtCargo1").val();
	var txtFunciones1x=$("#txtFunciones1x").val();
	var txtDesde1=$("#txtDesde1").val();
	var txtHasta1=$("#txtHasta1").val();
	var chkExp1=$("#chkExp1").val();
	var txtEmpresa2=$("#txtEmpresa2").val();
	var txtCargo2=$("#txtCargo2").val();
	var txtFunciones2x=$("#txtFunciones2x").val();
	var txtDesde2=$("#txtDesde2").val();
	var txtHasta2=$("#txtHasta2").val();
	var chkExp2=$("#chkExp2").val();

	var txtEmpresa3=$("#txtEmpresa3").val();
	var txtCargo3=$("#txtCargo3").val();
	var txtFunciones3x=$("#txtFunciones3x").val();
	var txtDesde3=$("#txtDesde3").val();
	var txtHasta3=$("#txtHasta3").val();
	var chkExp3=$("#chkExp3").val();

	var txtCarrera=$("#txtCarrera").val();
	var txtInsTec=$("#txtInsTec").val();
	var txtDesdeTec1=$("#txtDesdeTec1").val();
	var txtHastaTec1=$("#txtHastaTec1").val();
	var chkSup=$("#chkSup").val();
	var txtInsSec=$("#txtInsSec").val();
	var txtDesdeSec=$("#txtDesdeSec").val();
	var txtHastaSec=$("#txtHastaSec").val();
	var txtInsPrim=$("#txtInsPrim").val();
	var txtDesdePrim=$("#txtDesdePrim").val();
	var txtHastaPrim=$("#txtHastaPrim").val();
	var txtECAP=$("#txtECAP").val();
	var txtCurso=$("#txtCurso").val();
	var txtDesdeOtros=$("#txtDesdeOtros").val();
	var txtHastaOtros=$("#txtHastaOtros").val();
	var chkOtros=$("#chkOtros").val();

	var txtRefPers=$("#txtRefPers").val();
	var txtRefOrg=$("#txtRefOrg").val();
	var txtRefTelf=$("#txtRefTelf").val();
	var txtFoto=$("#txtFoto").val();
	var _ide=$("#_id").val();
	//img=fotoCV[0].src;
	dataExtraNoDB.push({"imagen":txtFoto,'_id':_ide});

	dataFormacionAcademica.push({
		'CV_REF_NOMBRE':txtRefPers,
		'CV_REF_CARGO':txtRefOrg,
		'CV_REF_TELEFONO':txtRefTelf,
		'CV_FOR_CARRERA1':txtCarrera,
		'CV_FOR_INSTI1':txtInsTec,
		'CV_FOR_DEL_ANIO1':txtDesdeTec1,
		'CV_FOR_AL_ANIO1':txtHastaTec1,
		'CV_FOR_CHECK1':chkSup,
		'CV_FOR_SEGUN':txtInsSec,
		'CV_FOR_DEL_SEGANIO':txtDesdeSec,
		'CV_FOR_AL_SEGANIO':txtHastaSec,
		'CV_FOR_PRIMARY':txtInsPrim,
		'CV_FOR_DEL_PRIM_ANIO':txtDesdePrim,
		'CV_FOR_AL_PRIM_ANIO':txtHastaPrim,
		'CV_CAPACITACION':txtECAP,
		'CV_CAP_CURSO':txtCurso,
		'CV_CAP_DEL_ANIO':txtDesdeOtros,
		'CV_CAP_AL_ANIO':txtHastaOtros,
		'CV_CAP_CHEK':chkOtros,
    		'CV_DNI':txtDNI,
    		'CV_NOMBRE':txtNombres,
    		'CV_EDAD':txtEdad,
    		'CV_DIRECCION':txtDireccion,
    		'CV_TELEFONO':txtTelf,
    		'CV_CORREO':txtEmail,
    		'CV_RESUMEN_PERSONAL':txtResumen,
    		'CV_EXP_NOM_EMPR1':txtEmpresa1,
    		'CV_EXP_CARGO1':txtCargo1,
    		'CV_EXP_FUNCIO1':txtFunciones1x,
    		'CV_EXP_DEL_ANIO1':txtDesde1,
    		'CV_EXP_AL_ANIO1':txtHasta1,
    		'CV_EXP_CHEK1':chkExp1,

    		'CV_EXP_NOM_EMPR2':txtEmpresa2,
    		'CV_EXP_CARGO2':txtCargo2,
    		'CV_EXP_FUNCIO2':txtFunciones2x,
    		'CV_EXP_DEL_ANIO2':txtDesde2,
    		'CV_EXP_AL_ANIO2':txtHasta2,
    		'CV_EXP_CHECK2':chkExp2,
    		'CV_EXP_NOM_EMPR3':txtEmpresa3,
    		'CV_EXP_CARGO3':txtCargo3,
    		'CV_EXP_FUNCIO3':txtFunciones3x,
    		'CV_EXP_DEL_ANIO3':txtDesde3,
    		'CV_EXP_AL_ANIO3':txtHasta3,
    		'CV_EXP_CHECK3':chkExp3
    	});
    dataInformacion = {'dataExtraNoDB':dataExtraNoDB,'dataInformacion':dataFormacionAcademica} ;
    return   dataInformacion;
}
function getTokenPersona(){
  var json=JSON.parse(localStorage.getItem('accede_informaicon'));
  if (json ==null) {
    return true;
  }
  if (!localStorage.getItem('accede_informaicon')) {
  	return false;
  }
  return false;
}
function pasarAVista() {
	if(getTokenPersona()==false){
	var json=JSON.parse(localStorage.getItem('accede_informaicon'));

	info=json.dataInformacion[0];
	infoI=json.dataExtraNoDB[0];
	$("#txtDNI").val(info.CV_DNI);
	$("#txtNombres").val(info.CV_NOMBRE);
	$("#txtEdad").val(info.CV_EDAD);
	$("#txtDireccion").val(info.CV_DIRECCION);
	$("#txtTelf").val(info.CV_TELEFONO);
	$("#txtEmail").val(info.CV_CORREO);
	$("#txtResumen").val(info.CV_RESUMEN_PERSONAL);
	//$("#chkFoto").val(info.CV_DNI);
	$("#txtEmpresa1").val(info.CV_EXP_NOM_EMPR1);
	$("#txtCargo1").val(info.CV_EXP_CARGO1);
	$("#txtFunciones1x").val(info.CV_EXP_FUNCIO1);
	$("#txtDesde1").val(info.CV_EXP_DEL_ANIO1);
	$("#txtHasta1").val(info.CV_EXP_AL_ANIO1);
	$("#chkExp1").val();
	$("#txtEmpresa2").val(info.CV_EXP_NOM_EMPR2);
	$("#txtCargo2").val(info.CV_EXP_CARGO2);
	$("#txtFunciones2x").val(info.CV_EXP_FUNCIO2);
	$("#txtDesde2").val(info.CV_EXP_DEL_ANIO2);
	$("#txtHasta2").val(info.CV_EXP_AL_ANIO2);
	$("#chkExp2").val();
	$("#txtEmpresa3").val(info.CV_EXP_NOM_EMPR3);
	$("#txtCargo3").val(info.CV_EXP_CARGO3);
	$("#txtFunciones3x").val(info.CV_EXP_FUNCIO3);
	$("#txtDesde3").val(info.CV_EXP_DEL_ANIO3);
	$("#txtHasta3").val(info.CV_EXP_AL_ANIO3);

	$("#txtCarrera").val(info.CV_FOR_CARRERA1);
	$("#txtInsTec").val(info.CV_FOR_INSTI1);
	$("#txtDesdeTec1").val(info.CV_FOR_DEL_ANIO1);
	$("#txtHastaTec1").val(info.CV_FOR_AL_ANIO1);
	$("#chkSup").val();

	$("#txtInsSec").val(info.CV_FOR_SEGUN);
	$("#txtDesdeSec").val(info.CV_FOR_DEL_SEGANIO);
	$("#txtHastaSec").val(info.CV_FOR_AL_SEGANIO);

	$("#txtInsPrim").val(info.CV_FOR_PRIMARY);
	$("#txtDesdePrim").val(info.CV_FOR_DEL_PRIM_ANIO);
	$("#txtHastaPrim").val(info.CV_FOR_AL_PRIM_ANIO);

	$("#txtECAP").val(info.CV_CAPACITACION);
	$("#txtCurso").val(info.CV_CAP_CURSO);
	$("#txtDesdeOtros").val(info.CV_CAP_DEL_ANIO);
	$("#txtHastaOtros").val(info.CV_CAP_AL_ANIO);
	$("#chkOtros").val();

	$("#txtRefPers").val();
	$("#txtRefOrg").val();
	$("#txtRefTelf").val();
	$("#txtFoto").val();
	$("#_id").val(infoI._id);	
	}
}