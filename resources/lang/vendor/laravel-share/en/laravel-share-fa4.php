<?php

return [
    'facebook' => '<li><a href=":url" target="_blank" class="social-button :class" id=":id"><span class="fab fa-facebook fa-2x"></span></a></li>',
    'twitter' => '<li><a href=":url" target="_blank" class="social-button :class" id=":id"><span class="fab fa-twitter-square fa-2x"></span></a></li>',
    'gplus' => '<li><a href=":url" target="_blank" class="social-button :class" id=":id"><span class="fab fa-google-plus-square fa-2x"></span></a></li>',
    'linkedin' => '<li><a href=":url" target="_blank" class="social-button :class" id=":id"><span class="fab fa-linkedin fa-2x"></span></a></li>',
    'whatsapp' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id"><span class="fab fa-whatsapp-square fa-2x"></span></a></li>',
];
 
