<form action="{{ route('editar.contacto') }}" method="post">
	 @csrf
    <div class="form-row">
	    <div class="form-group col-md-6">
	      <label>Ruc</label>
	      <input type="text" class="form-control" id="ruc" name="ruc" placeholder="Ruc" value="@foreach($empresas as $empresa){{ ucwords($empresa->numero_documento) }}@endforeach" disabled>
	    </div>
	    <div class="form-group col-md-6">
	      <label>Telefono</label>
	      <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value="@foreach($personas as $persona){{ ucwords($persona->telefono) }}@endforeach">
	    </div>
	</div>

	<div class="form-row">

	    <div class="form-group col-md-6">
	      <label>Direccion</label>
	      <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direccion" value="@foreach($empresas as $empresa){{ ucwords($empresa->direccion) }}@endforeach">
	    </div>
	     <div class="form-group col-md-6">
	      <label>Sitio Web</label>
	      <input type="text" class="form-control" id="sitioweb" name="sitioweb" placeholder="Sitio Web" value="@foreach($empresas as $empresa){{ ucwords($empresa->sitio_web) }}@endforeach">
	    </div>
	</div>


	<button type="submit" class="btn btn-primary">Editar Inforamción</button>
</form>