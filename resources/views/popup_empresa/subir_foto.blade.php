<div class="card">
  	<div class="card-body">
	  
	    <div class="alert alert-danger" role="alert" style="display:none" id="error">
	        Archivo seleccionado no  permitido. Tipo de archivos permitidos <b>(.pdf .doc .docx)</b>
	    </div>
	    <form action="{{ route('contacto.ajax.foto') }}" method="post" enctype="multipart/form-data" id="form_foto">
	        @csrf
		    <div class="custom-file">
		    	<input onchange="return validarExt()" type="file" class="custom-file-input" id="customFileLang" lang="es" name="file">
		    	<label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
		    </div>
		    <div id="visorArchivo"></div>
	    	<br>
		    <spam style="color:red">Formato png/jpg - Tamallo Max:1000Kb</spam>
			<br><br>
		<!--	<a href="#" class="btn btn-primary" onclick="return guardarFoto()">Adjuntar Archivo</a> -->
				<input class="btn btn-primary" type="submit" value="Adjuntar Archivo"> 
		</form>
      
  	</div>
</div>

<script>
    function validarExt(){
        var archivoInput = document.getElementById("customFileLang");
        var archivoRuta = archivoInput.value;
        var extPermitidas = /(.png|.jpg|.jpge)$/i;

        if(!extPermitidas.exec(archivoRuta)){
            document.getElementById("sucess").style = "display:none";
            document.getElementById("error").style = "display:block";
            archivoInput.value = "";
            return false;
        }else{
            if(archivoInput.files && archivoInput.files[0]){
                var visor = new FileReader();
                visor.onload = function(e){
                    document.getElementById("visorArchivo").innerHTML = "<embed src='"+e.target.result+"' width='100' height='100' />" ;     
                };
                visor.readAsDataURL(archivoInput.files[0]);
            }
        }
    }

    function guardarFoto(){
    	var form = $("#form_foto");
    	$.ajax( {
	      type: "POST",
	      url: form.attr( 'action' ),
	      data: form.serialize(),
	      success: function( response ) {
	      	$(".loading").hide();
	      	alert("Se envió el correo correctamente.");
	      }
	    } );
    }
</script>