<form method="post" action="{{ route('contacto.nuevo.json') }}" id="form_contacto" >
<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
<input type="hidden" id="base_url" value="{{ asset('') }}">
<input type="hidden" id="codigo_contacto" name="codigo_contacto" value="">
	<div class="form-row">
    <div class="form-group col-md-6">
      	<label>Tipo Documento</label>
      	<select class="form-control{{ $errors->has('documento_contacto') ? ' is-invalid' : '' }} form-control-sm" name="documento_contacto" id="documento_contacto" > 
           <option value="">Seleccione</option>
            @foreach($documentos as $documento)
                <option value="{{ $documento->id }}">{{ $documento->tipdoc_descripion }}</option>
            @endforeach
      	</select>
    </div>
    <div class="form-group col-md-6 ">
      <label>Numero Documento</label>
      <input type="text" class="form-control form-control-sm " id="numero_contacto" name="numero_contacto" placeholder="Numero Documento"  onblur="validarExiste(this)">
          <span class="invalid-feedback" role="alert">
              <strong>Este numero ya existe en nuestras base de datos</strong>
          </span>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-12">
      <label>Nombre</label>
      <input type="text" class="form-control form-control-sm" id="nombre_contacto" name="nombre_contacto" placeholder="nombre_contacto" >
    </div>
    
  </div>

<div class="form-row">
	<div class="form-group col-md-6">
      <label>Cargo</label>
      <input type="text" class="form-control form-control-sm" id="cargo_contacto" name="cargo_contacto" placeholder="Cargo" >
    </div>
    <div class="form-group col-md-6">
      <label>Telefono</label>
      <input type="text" class="form-control form-control-sm" id="telefono_contacto" name="telefono_contacto" placeholder="Telefono">
    </div>
  </div>

  <div class="form-row" id="ocultar">
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" class="form-control form-control-sm" id="email_contacto" name="email_contacto" placeholder="Email"  onblur="validarExisteCorreo(this)">
        <span class="invalid-feedback" role="alert">
          <strong>Este email ya existe en nuestra base de datoss</strong>
        </span>
    </div>
    <div class="form-group col-md-6">
      <label>Password</label>
      <input type="password" class="form-control form-control-sm" id="password_contacto" name="password_contacto" placeholder="Password" >
    </div>
  </div>
 
</form>
<a type="btn" class="btn btn-primary" onclick="enviarformulario()">Agregar Contacto</a>
<script>
function enviarformulario(){

  if($("#documento_contacto").val() == ""){
  //  alert("sdfadf");
    $("#documento_contacto").attr("required", "true");
    $("#documento_contacto").addClass('is-invalid');
    return false;
  }

  if($("#numero_contacto").val() == ""){
    $("#numero_contacto").attr("required", "true");
    $("#numero_contacto").addClass('is-invalid');
    return false;
  }

  if($("#nombre_contacto").val() == ""){
    $("#nombre_contacto").attr("required", "true");
    $("#nombre_contacto").addClass('is-invalid');
    return false;
  }

  if($("#cargo_contacto").val() == ""){
    $("#cargo_contacto").attr("required", "true");
    $("#cargo_contacto").addClass('is-invalid');
    return false;
  }
/*
  if($("#telefono").val() == ""){
    $("#telefono").attr("required", "true");
    $("#telefono").addClass('is-invalid');
    return false;
  }
*/
  if($("#email_contacto").val() == ""){
    $("#email_contacto").attr("required", "true");
    $("#email_contacto").addClass('is-invalid');
    return false;
  }
  if($("#password_contacto").val() == ""){
    $("#password_contacto").attr("required", "true");
    $("#password_contacto").addClass('is-invalid');
    return false;
  }


  var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlajax = base_url + "contacto/nuevo/json";

    $.ajax({
        type: 'post',
        url: urlajax,
        dataType: 'json',
        data: { 
            _token:_token,
            documento_contacto:$("#documento_contacto").val(),
            numero_contacto:$("#numero_contacto").val(),
            nombre_contacto:$("#nombre_contacto").val(),
            cargo_contacto:$("#cargo_contacto").val(),
            telefono_contacto:$("#telefono_contacto").val(),
            email_contacto:$("#email_contacto").val(),
            password_contacto:$("#password_contacto").val(),
        },
      //  data:$("#form_contacto").serialize(),
        success: function(data) {
              if(data == "1"){
                actualizarListaContacto();
              }else{
                alert("no regstrado");
              }
        }
    });

    
 // $("#form_contacto").submit();

}


function validarExiste(codigo){

  var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlajax = base_url + "valida/numdoc/" + codigo.value;

    $.ajax({
        type: 'GET',
        url: urlajax,
        dataType: 'json',
        success: function(data) {
              if(data == "1"){
                $("#numero_contacto").addClass('is-invalid');
              }else{
                $("#numero_contacto").removeClass('is-invalid');
              }
        }
    });


}

function validarExisteCorreo(codigo){

var _token = $("#csrf-token").val();
  base_url = $("#base_url").val();
  var urlajax = base_url + "valida/correo/" + codigo.value;

  $.ajax({
      type: 'GET',
      url: urlajax,
      dataType: 'json',
      success: function(data) {
            if(data == "1"){
              $("#email_contacto").addClass('is-invalid');
            }else{
              $("#email_contacto").removeClass('is-invalid');
            }
      }
  });

}

</script>