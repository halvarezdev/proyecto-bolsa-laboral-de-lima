<form method="post" action="{{ route('contacto.nuevo') }}" id="form_contacto" >
<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
<input type="hidden" id="base_url" value="{{ asset('') }}">
<input type="hidden" id="codigo_contacto" name="codigo_contacto" value="">
	<div class="form-row">
    <div class="form-group col-md-6">
      	<label>Tipo Documento</label>
      	<select class="form-control{{ $errors->has('tipo_documento_contacto') ? ' is-invalid' : '' }} form-control-sm"
        onchange="cambiar_ruc_contacto()"
         name="tipo_documento_contacto" value="{{ old('tipo_documento_contacto') }}" id="tipo_documento_contacto" >
           <option value="">Seleccione</option>
            @foreach($documentos as $documento)
                <option value="{{ $documento->id }}">{{ $documento->tipdoc_descripion }}</option>
            @endforeach
      	</select>
    </div>
    <div class="form-group col-md-6 ">
      <label>Numero Documento</label>
      <input type="text" class="form-control form-control-sm " onkeypress="return valideKey(event);" id="numero_documento" name="numero_documento" placeholder="Numero Documento"  onblur="validarExiste(this)">
          <span class="invalid-feedback" role="alert">
              <strong>Este numero ya existe en nuestras base de datos</strong>
          </span>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-12">
      <label>Nombre</label>
      <input onpaste="return false"  onkeypress="return soloLetras(event);" type="text" class="form-control form-control-sm" id="nombre" name="nombre" placeholder="Nombre" >
    </div>

  </div>

<div class="form-row">
	<div class="form-group col-md-6">
      <label>Cargo</label>
      <input type="text"  onkeypress="return soloLetras(event);" onpaste="return false"  class="form-control form-control-sm" id="cargo" name="cargo" placeholder="Cargo" onkeypress="return soloLetras(event)">
    </div>
    <div class="form-group col-md-6">
      <label>Telefono</label>
      <input type="text"  onpaste="return false"  class="form-control form-control-sm" id="telefono" name="telefono" placeholder="Telefono" onkeypress="return telefonoNumero(event);">
    </div>
  </div>

  <div class="form-row" >
    <div class="form-group col-md-6">
      <label>Email</label>
      <input type="email" onpaste="return false"  class="form-control form-control-sm" id="email" name="email" placeholder="Email"  onblur="validarExisteCorreo(this)">
        <span class="invalid-feedback" role="alert">
          <strong>Este email ya existe en nuestra base de datoss</strong>
        </span>
    </div>
    <div class="form-group col-md-6" id="ocultar">
      <label>Password</label>
      <input type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Password" onkeypress="return space(event);">
    </div>
  </div>

</form>
<button type="btn" class="btn btn-primary" onclick="enviarformulario()">Agregar Contacto</button>
<script>
function cambiar_ruc_contacto(){
  $("#numero_documento").focus().val('')
}
function valideKey(evt){

    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    var digitos = document.querySelector('#numero_documento').value.length;
    let tipo_documento = document.getElementById('tipo_documento_contacto').value;
   if(tipo_documento == '2'){
      if(digitos >= 8){
         return false;
      }
   } else {
    if(digitos >= 10){
         return false;
      }
   }
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}

function telefonoNumero(evt){

    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    var digitos = evt.target.value.length;

   if(digitos >= 9){
      return false;
   }
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}

function space(evt){

   var code = (evt.which) ? evt.which : evt.keyCode;
   if(code==32) { // backspace.
      return false;
    } else{ // other keys.
      return true;
   }
}

function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

         if(key==32) { // backspace.
            return false;
         }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
function enviarformulario(){

  if($("#tipo_documento_contacto").val() == ""){
  //  alert("sdfadf");
    $("#tipo_documento_contacto").attr("required", "true");
    $("#tipo_documento_contacto").addClass('is-invalid');
    return false;
  }

  if($("#numero_documento").val() == ""){
    $("#numero_documento").attr("required", "true");
    $("#numero_documento").addClass('is-invalid');
    return false;
  }

  if($("#nombre").val() == ""){
    $("#nombre").attr("required", "true");
    $("#nombre").addClass('is-invalid');
    return false;
  }

  if($("#cargo").val() == ""){
    $("#cargo").attr("required", "true");
    $("#cargo").addClass('is-invalid');
    return false;
  }
/*
  if($("#telefono").val() == ""){
    $("#telefono").attr("required", "true");
    $("#telefono").addClass('is-invalid');
    return false;
  }
*/
$("#password").val($("#numero_documento").val())

  if($("#email").val() == ""){
    $("#email").attr("required", "true");
    $("#email").addClass('is-invalid');
    return false;
  }
  if($("#password").val() == ""){
    $("#password").attr("required", "true");
    $("#password").addClass('is-invalid');
    return false;
  }
  $("#tipo_documento_contacto option").removeAttr("disabled");

  $("#form_contacto").submit();

}


function validarExiste(codigo){

  var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlajax = base_url + "valida/numdoc/" + codigo.value;

    $.ajax({
        type: 'GET',
        url: urlajax,
        dataType: 'json',
        success: function(data) {
              if(data == "1"){
                $("#numero_documento").addClass('is-invalid');
              }else{
                $("#numero_documento").removeClass('is-invalid');
              }
        }
    });


}

function validarExisteCorreo(codigo){

var _token = $("#csrf-token").val();
  base_url = $("#base_url").val();
  var urlajax = base_url + "valida/correo/" + codigo.value;

  $.ajax({
      type: 'GET',
      url: urlajax,
      dataType: 'json',
      success: function(data) {
            if(data == "1"){
              $("#email").addClass('is-invalid');
            }else{
              $("#email").removeClass('is-invalid');
            }
      }
  });

}

</script>
