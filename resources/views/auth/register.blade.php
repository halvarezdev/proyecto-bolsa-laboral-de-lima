@extends('layouts.app')
@section('content')
<style>
.fondo_body h5{font-family:Galano Grotesque Black;    color: #ffffff;}
.fondo_body label{ font-family: Galano Grotesque Medium; color: #3850A0;    font-size: 1rem;}
.fondo_body input{   font-family: Galano-Grotesque Light; color: #575756;}
.fondo_body .card-header{background-color:#575756;}
.fondo_body .border-color{border-color:#575756;}
.fondo_body .regit_button{background-color: #b00002;color: #ffffff;  font-family: Galano Grotesque Medium;}
</style>



<div class="container fondo_body" style="padding-top:40px;padding-bottom: 40px;">
@if(isset($tipo) && $tipo == '2')
<form method="POST" action="{{ route('register.submit') }}" id="formulario_persona">
   @else
   <form method="POST" action="{{ route('register') }}" id="formulario_persona">
      @endif
      <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
      <div class="row justify-content-md-center" style="background-color: #d8d8d8;">
         <div class="col-xl-7 col-md-12 col-sm-12 mt-4 mb-4">
            <div class="card border-color mb-3">
               <div class="card-header"><h5><b>REGISTRO DE USUARIO</b></h5></div>
               <div class="card-body ">
                  <input type="hidden" id="base_url" value="{{ asset('') }}">
                  <input type="hidden" id="entidad" name="entidad" value="{{ $entidad }}">
                  <!-- TIPO DOCUMENTO -->
                  <div class="form-row">
                     <div class="form-group col-md-4">
                        <label>Tipo Documento <span>(*)</span></label>
                        <select class="form-control form-control-sm {{ $errors->has('tipo_documento') ? ' is-invalid' : '' }} " onChange="buscarSelect()" data-style="select-style" name="tipo_documento" value="{{ old('tipo_documento') }}" id="tipo_documento" required autofocus>
                           @foreach($documentos as $documento)
                           @if($documento->id != "6")
                           <option value="{{ ($documento->id != '1' ? $documento->id : '') }}">{{ $documento->tipdoc_descripion }}</option>
                           @endif
                           @endforeach
                        </select>
                        <span class="tipo_documento" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('tipo_documento'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('tipo_documento') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label>Número de Documento <span>(*)</span></label>
                        <input id="numero_documento"
                           type="text"
                           class="form-control form-control-sm {{ $errors->has('numero_documento') ? ' is-invalid' : '' }}"
                           name="numero_documento"
                           value="{{ old('numero_documento') }}"
                           required
                           autofocus
                           placeholder="Ingrese Numero de Documento"
                           onkeypress="return valideKey(event);">
                        <span class="numero_documento" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('numero_documento'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('numero_documento') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-2"><br>
                        <button type="button" class="btn btn-primary" onclick="BuscarDNI(this)"  id="buscar_doc">Buscar</button>
                     </div>
                     <div class="form-group col-md-6">
                        <label>Primer Apellido <span>(*)</span> <br><span style="color:#fff"> .</span> </label>
                        <input
                           id="ape_pat"
                           onkeypress="return soloLetras(event);"
                           type="text" class="form-control form-control-sm {{ $errors->has('ape_pat') ? ' is-invalid' : '' }}"
                           name="ape_pat" value="{{ old('ape_pat') }}"
                           required
                           autofocus
                           placeholder="Ingrese Primer Apellido"
                           style="text-transform:uppercase;"
                           pattern="^[A-Za-z]+$">
                        <span class="ape_pat" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('ape_pat'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ape_pat') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label>Segundo Apellido<br><span style="color:gray; font-size:12px">(dejar en blanco si no tiene)</span></label>
                        <input id="ape_mat" onkeypress="return soloLetras(event);" type="text" class="form-control form-control-sm {{ $errors->has('ape_mat') ? ' is-invalid' : '' }}" required name="ape_mat" value="{{ old('ape_mat') }}" autofocus placeholder="Ingrese Segundo Apellido" style="text-transform:uppercase;">
                        <span class="ape_mat" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('ape_mat'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ape_mat') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-12">
                        <label>Nombres <span>(*)</span></label>
                        <input id="name" onkeypress="return soloLetras(event);" type="text" class="form-control form-control-sm {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Ingrese Nombres" style="text-transform:uppercase;">
                        <span class="name" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-12">
                        <label>Correo Electrónico <span>(*)</span></label>
                        <input id="email" onkeypress="return space(event);" type="email" class="form-control form-control-sm {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Ingrese Correo Electronico" style="text-transform:uppercase;">
                        <span class="email" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label>Contraseña <span>Minimo 6 Caracteres (*)</span></label>
                        <input id="password" onkeypress="return space(event);" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" name="password" required placeholder="Contraseña">
                        <span class="password" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label>Confirmar Contraseña <span>(*)</span></label>
                        <input id="password-confirm" onkeypress="return space(event);" type="password" class="form-control form-control-sm" name="password_confirmation" required placeholder="Contraseña">
                        <span class="password-confirm" role="alert" style="font-size:12px; color:red;"></span>
                     </div>
                     <div class="form-group col-md-6">
                        <label>¿Qué tipo de trabajo buscas? <span>(*)</span></label>
                        <select name="ocu_trabajo" id="ocu_trabajo" class="selectpicker show-tick form-control form-control-sm" data-width="100%" data-style="select-style" data-live-search="true" required>
                           <option value="">Seleccione</option>
                           @foreach($ocupaciones as $value)
                           <option value="{{ $value->ocup_codigo }}">{{ $value->nombre }}</option>
                           @endforeach
                        </select>
                        <span class="ocu_trabajo" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('ocu_trabajo'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ocu_trabajo') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label>¿Dónde lo buscas? <span>(*)</span></label>
                        <select name="ubi_lugar" id="ubi_lugar" class="form-control form-control-sm selectpicker show-menu-arrow"  data-style="select-style" required>
                           <option value="">Seleccione</option>
                           @foreach($ubigeo as $ubi)
                           <option value="{{ $ubi->ubi_codigo }}">{{ $ubi->ubi_descripcion }}</option>
                           @endforeach
                        </select>
                        <span class="ubi_lugar" role="alert" style="font-size:12px; color:red;"></span>
                        @if ($errors->has('ubi_lugar'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('ubi_lugar') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-12">
                     <label><input type="checkbox" required name="leertInvor" id="leertInvor" > He leído y acepto todo los <a href="{{asset('contacto/termines-condiciones')}}" target="_blank"  >Términos y Condiciones</a> </label>
                     </div>
                  </div>
                  <div class="form-row justify-content-end">
                     <button type="submit" class="btn regit_button">REGISTRAR</button>
                  </div>
               </div>
            </div>
         </div>
   </form>
</div>
<script >

function valideKey(evt){

    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    var digitos = document.querySelector('#numero_documento').value.length;
    let tipo_documento = document.getElementById('tipo_documento').value;
   if(tipo_documento == '2'){
      if(digitos >= 8){
         return false;
      }
   }else {
      if(digitos >= 10){
         return false;
      }
   }
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}

function space(evt){

   var code = (evt.which) ? evt.which : evt.keyCode;
   if(code==32) { // backspace.
      return false;
    } else{ // other keys.
      return true;
   }
}

function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(e.target.name != 'name'){
            if(key==32) { // backspace.
               return false;
            }
        } 

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
}

   function buscarSelect(){
      document.querySelector('#numero_documento').value = '';
      let tipo_documento = document.getElementById('tipo_documento').value;
      if(tipo_documento == '2' || tipo_documento == ''){
         document.getElementById('buscar_doc').style.display = 'block';
      }else{
         document.getElementById('buscar_doc').style.display = 'none';
      }
   }
</script>

@endsection
