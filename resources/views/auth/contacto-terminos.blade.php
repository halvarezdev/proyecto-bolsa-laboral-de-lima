
@extends('layouts.app')
@section('content')
<style>
.text_empre h4{font-family:Galano Grotesque Black;background: #575756;
    color: #ffffff; }
.text_empre label{ font-family: Galano Grotesque Medium; color: #3850A0;    font-size: 0.9rem;}
.text_empre input{   font-family: Galano-Grotesque Light; color: #575756;}
.text_empre .card-header{background-color:#575756;}
.text_empre .regit_button{background-color: #b00002;color: #ffffff;  font-family: Galano Grotesque Medium;}
.text_empre .titulo_formulario{font-family: Galano Grotesque Medium;}
.text_empre select,textarea{font-family: Galano-Grotesque Light;}
</style>


<div class="container text_empre" style="padding-top: 20px;">
   
      <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
      <input type="hidden" id="base_url" value="{{ asset('') }}">
      <input type="hidden" id="entidad" name="entidad" value="{{ $entidad }}">
      <div class="row justify-content-center" style="background-color: #d8d8d8;">
         <div class="col-md-10 mt-4 mb-4">
            <div class="card">
            <div class="card-header text-center"><h4>Términos y condiciones de uso</h4></div>
            <div class="card-body">
            
            <p>Usted está visitando la plataforma para la intermediación laboral <a href="{{ asset('/') }}" target="_blank">{{ asset('/') }}</a> de propiedad de la Municipalidad distrital de Lima, con domicilio en la Calle 9 de Junio N° 100, distrito de Lima, provincia y departamento de Lima. Esta plataforma brinda a la población, especialmente Lima, un servicio gratuito para ayudarlos a realizar una búsqueda de empleo rápido y eficiente.</p>


<p>El usuario se compromete a utilizar la plataforma de conformidad con la Ley y el orden público. En este sentido, hará un uso adecuado de los contenidos que se ofrecen en la plataforma y a no emplearlos para incurrir en actividades ilícitas, así como a no publicar ningún tipo de contenido ilícito, y no utilizará un lenguaje ilícito, amenazante, obsceno, vulgar, racista, ni cualquier otro que se considere inapropiado, ni anunciar o proporcionar enlaces a sitios que contengan materia ilegal. Queda prohibida la utilización de los servicios ofrecidos a través de esta plataforma para publicar ofertas laborales con contenidos discriminatorios o que contengan indicios de ser ofertas laborales falsas o que promueven la trata de personas. Asimismo, queda prohibida la utilización de los servicios ofrecidos a través de esta plataforma por personas que carezcan de capacidad legal para contratar o menores de edad según la legislación vigente. Ante la gravedad o recurrencia de estos hechos, la Municipalidad de Lima procederá a suspender temporalmente y/o cancelar definitivamente la cuenta del usuario; sin perjuicio, de iniciar acciones legales si se ha infringido la legislación vigente. En ese sentido, el usuario puede también reportar a la Municipalidad de Lima sobre cualquier incidente que ocurriese en el manejo o la interacción con la plataforma a fin de tomar las medidas que correspondan.</p>

<p>Los datos personales que registren las personas y las empresas que utilicen esta plataforma serán recopilados y almacenados únicamente para fines de vinculación laboral y reportes de este proceso. La Municipalidad de Lima garantiza que el tratamiento de los datos se limite a las finalidades antes señalada y que se mantengan de forma confidencial. También, se compromete a no transferir o compartir la información del usuario sin el debido consentimiento para ello, con excepción de solicitud de información de autoridades públicas en ejercicio de sus funciones y en el ámbito de sus competencias, y en virtud de órdenes judiciales y de disposiciones legales.</p>

<p>A través de la aceptación de los presentes términos y condiciones, el usuario autoriza y otorga su consentimiento previo, libre, expreso e inequívoco, para el tratamiento de la información del usuario, de conformidad con las finalidades antes descritas. Asimismo, el usuario puede solicitar a la Municipalidad de Lima la eliminación de su registro en cualquier momento y sin necesidad de señalar motivación alguna.</p>

<p>Los términos y condiciones generales de uso y las demás de condiciones legales, así como la política de privacidad y protección de datos personales de la plataforma, se rigen en todos y cada uno de sus extremos por la ley peruana.</p>


            </div>
            </div>
            <br>
            
            
         </div>
   </form>
   </div>
   <br><br>
</div>
 

@endsection