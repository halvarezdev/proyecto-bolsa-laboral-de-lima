@extends('layouts.app-login')

@section('content')

<style>
.titulo_login_segundo{
    text-align:center;
}
</style>
<br>

<div class="container">
    <form method="POST" action="{{ route('login') }}">
<br>
    <div class="row justify-content-center">
        <div class="col-10">
            @csrf
            <h5 class="titulo_login_segundo"><b>INGRESA A TU CUENTA</b></h5>
            <br>
            <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-nuevo" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Contraseña</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-nuevo" name="password" required placeholder="Contraseña">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
            
        </div> 
        <div class="col-10">
        
        </div>
    </div>

 
  <div class="form-group row justify-content-center">
        <div class="col-6">
            <button type="submit" class="btn btn_portal"  style="font-size: 14px;color:#fff; background:#00A4D3;width:100%;" >
                Ingresar
            </button>
        </div>
    </div>
  

    <div class="form-group row mb-0 justify-content-center">
        <div class="col-9">
            <a class="btn" href="{{ route('social.auth', 'facebook') }}">
                <img src="{{ asset('images/facebook.png') }}" alt="Facebook">
            </a>
        </div>
    </div>

    <div class="form-group row mb-0 justify-content-center">
        <div class="col-10">
            <div class="form-check">
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('¿Olvidó la contraseña?') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
<br>
    <div class="form-group row justify-content-center">
        <div class="col-10">
           <font>¿Nuevo Postulante? </font> <a href="{{ route('register', ['e' => $entidad]) }}"><font>Registrate</font></a>
        </div>
    </div>

</form>  

</div>
<!--
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h6 class="titulo_login_segundo"><b>INGRESA A TU CUENTA</b></h6>
                <h6 class="titulo_login">&nbsp;&nbsp;&nbsp;Persona</h6>
                <br>
                <div class="form-group row">
                    <label for="email" class="col-md-2 col-form-label text-md-right">
                        <i class="fas fa-user fa-2x icon-login"></i>
                    </label>

                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-nuevo" name="email" value="{{ old('email') }}" required autofocus placeholder="E-MAIL">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">
                            <i class="fas fa-unlock-alt fa-2x icon-login"></i>
                            </label>

                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-nuevo" name="password" required placeholder="CONTRASEÑA">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col-md-7">

                            <button type="submit" class="btn btn_portal" style="font-size: 14px;">
                                Ingresar
                            </button>
                            
                            </div>
                        </div>

                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col-md-6">
                                <div class="form-check">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidó la contraseña?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                        <div class="col-md-2">
                        </div>
                            <div class="col-md-6">
                                <a class="btn" href="{{ route('social.auth', 'facebook') }}">
                                    <img src="{{ asset('images/facebook.png') }}" alt="Facebook">
                                </a>
                            </div>
                        </div>
                        
                            
                        <div class="form-group row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember">

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuérdame') }}
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                            <br>
                                <a href="{{ route('register') }}"><font>¿Nuevo Postulante? Registrate Ahora</font></a>
                            </div>
                        </div>
                    </form>
                </div>
          
        </div>
    </div>
</div>

-->
<script src="https://www.google.com/recaptcha/api.js"></script>
@endsection
