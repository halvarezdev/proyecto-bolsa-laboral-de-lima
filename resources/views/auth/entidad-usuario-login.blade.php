   <!DOCTYPE html>
   <html lang="en">
      <head>
         <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <meta http-equiv="X-UA-Compatible" content="ie=edge">
         <title>Municipalidad de Lima</title>
         @include('maestro.bootstrap')
         <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
         <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/login.css') }}">
         <link rel="stylesheet" type="text/css" href="{{ asset('fonts/style.css') }}"   />
         @if(isset($entidad) && $entidad == '2')
         <link rel="shortcut icon" href="{{ asset('entidad_logo/cen_logo55.png') }}" />
         @else
         <link rel="shortcut icon" href="{{ asset('images/logo_menu.png') }}" />
         @endif
      </head>
      <body class="fondo_body">
         <div class="container-fluid" id="fondo">
            <div class="row">
               <div class="col d-md-flex align-items-center justify-content-center main-wrapper">
                  <div class="content" style="background-image: url('{{ asset('fondo/image-login.png') }}');background-repeat: no-repeat;
                  background-attachment: fixed;
                  background-size: cover;">
                     <div class="opacity"></div>
                     <div class="triangle-red"></div>
                     <div class="wraper-content">

                        <div class="row d-flex flex-column flex-md-row justify-content-center align-items-center">
                           <div class="col-sm-6 login-title ">
                              <div class="color-b">
                                 <h2 class=" ml-md-4 mb-0 " style="color:#ffffff;font-family: Galano Grotesque Bold;">Plataforma de </h2>
                                 <div class="TextoAqui ">
                                    <ul>
                                       <li>Servicio al Ciudadano</li>
                                       <li>Atención en Línea</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="card mx-md-5 mt-3">
                                 <div class="card-body ">
                                    <div class="form-group row justify-content-center">
                                       <div class="col-md-8">
                                          <center>
                                             <img src="{{ asset('entidad_logo/logo_menu.svg') }}" style="height: 50px;">
                                             <br><p class="mb-1" style="margin-top: 20px;">Intranet - Municipalidad de Lima</p>
                                             <br><br><br><span class="mb-1 mt-1">Atención en Línea</span>
                                          </center>
                                       </div>
                                    </div>
                                    <form method="POST" action="{{ route('entidad.login.submit') }}">
                                       @csrf
                                       <div class="form-group row">
                                          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>
                                          <div class="col-md-6">
                                             <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                             @if ($errors->has('email'))
                                             <span class="invalid-feedback" role="alert">
                                             <strong>{{ $errors->first('email') }}</strong>
                                             </span>
                                             @endif
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>
                                          <div class="col-md-6">
                                             <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                             @if ($errors->has('password'))
                                             <span class="invalid-feedback" role="alert">
                                             <strong>{{ $errors->first('password') }}</strong>
                                             </span>
                                             @endif
                                          </div>
                                       </div>
                                       <div class="form-group row mb-0">
                                          <div class="col-md-12">
                                             <button type="submit" class="login loginmodal-submit" style="width:100%;">
                                             {{ __('Ingresar') }}
                                             </button>
                                          </div>
                                       </div>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex">
                        <div class="col d-flex justify-content-center justify-content-md-end">
                           <p class="mt-1  mt-md-5  mr-1 ml-4 login-text text-center">
                              <!-- En el caso de no poder ingresar, envíe un correo a <a href="mailto:svisitas@trabajo.gob.pe" class="link-correo">notificaciones@trabajo.gob.pe</a>
                                 <br>
                                 © 2017 Ministerio de Trabajo y Promoción del Empleo -->
                           </p>
                        </div>
                     </div>
                  </div>
                  <!-- <img src="./Extranet - Ministerio de Trabajo y Promoción del Empleo_files/image-login.png" class="m-auto" alt="Que necesitas"> -->
               </div>
            </div>
         </div>
         <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
         <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
         <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
         <script defer="" src="js/fontawesome-all.js"></script>
         <script type="text/javascript" src="js/persona.js"></script>
         <script type="text/javascript" src="js/scripts.js"></script>
      </body>
   </html>