@extends('layouts.app')

@section('content')
<style>
.titulo_formulario{
    color:black;
}
</style>
<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-10">
            <div class="card" style="border:1px solid black">
                <div class="card-body">
                 
<table class="table table-sm">
    <thead>
        <tr>
            <td>CODIGO</td>
            <td>EMPRESA</td>
            <td>FECHA</td>
            
            <td>OCUPACIÓN</td>
            <td>SUELDO</td>
            <td>N° VAC.</td>
            <td>DET</td>
        </tr>
    </thead>
       <tbody>
           <?php
    echo $data['response_db']['table']
    ?> 
     </tbody>
    <tfoot>
           <?php
    echo $data['response_db']['theadPagin']
    ?> 
    </tfoot>
    </table>
                </div>
            </div>
        </div>   
    </div>
</div>        

@endsection

