@extends('layouts.app')

@section('content')
<style>
.titulo_formulario{
    color:black;
}
</style>
<br>
<div class="container">
    <div class="row justify-content-center">
    <div class="col-md-10">
            <div class="card" style="border:1px solid black">
                <div class="card-body">
                    <form method="POST" action="{{ route('users.busqueda.submit') }}">
                        <input type="hidden" id="base_url" value="{{ asset('') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="titulo_formulario" style="color:black;">
                                    <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;">
                                    Búsqueda de Vacante de Empleo 
                                </h5>
                            </div>
                        </div>

                                             <!-- DESCRIPCION PEDIDO -->

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Descripcion de Pedido') }}</label>

                            <div class="col-md-6">
                                <input id="descripcion" type="text" class="form-control form-control-sm" name="descripcion" value="{{ old('descripcion') }}" placeholder="Ingrese Descripcion">
                            </div>
                        </div>

                    <!-- EXPERIENCIA  -->
                     <div class="form-group row">
                         <label for="experiencia" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Área') }}</label>
                         <div class="col-sm-6">
                             <select id="area" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Area" name="area">
                                <option value="">Seleccione</option>
                                @foreach($areas as $area)
                                <option value="{{ $area->id }}">{{ $area->area_descripcion }}</option>
                                @endforeach
                            </select>
                         </div>
                     </div>   


             <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">Idioma</label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                    <div class="control-group">
                            <select id="idioma2" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Idioma" name="idioma2">
                                <option value="">Seleccione</option>
                                @foreach($idiomas as $idioma)
                                <option value="{{ $idioma->MAESP_CODIGO }}">{{ $idioma->MAESC_DESCRIPCION }}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                            <div class="col">
                                <select class="form-control form-control-sm" name="nivel_idioma_2" value="{{ old('nivel_idioma_2') }}" id="nivel_idioma_2"> 
                                    <option value="">Seleccione</option>
                                    <option value="Basico">Basico</option>
                                    <option value="Medio">Medio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                </div>
            </div>
        </div>


                <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Licencia de Conducir') }}</label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                    <select class="form-control{{ $errors->has('licencia') ? ' is-invalid' : '' }} form-control-sm" name="licencia" value="{{ old('licencia') }}" id="licencia"> 
                        <option value="">Seleccione</option>
                        <option value="2">No</option>
                        <option value="1">Si</option>
                    </select>
                </div>

                <div class="col">
                    <select class="form-control{{ $errors->has('categoria') ? ' is-invalid' : '' }} form-control-sm" name="categoria" value="{{ old('categoria') }}" id="categoria"> 
                        <option value="">Seleccione</option>
                        <option value="a1">a1</option>
                        <option value="a2">a2</option>
                        <option value="b2">b2</option>
                    </select>
                </div>
                </div>
            </div>
        </div>

                    <div class="form-group row">
                            <label for="experiencia" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Tiempo Experiencia') }}</label>

                            <div class="col-md-2">
                                <input id="experiencia" type="text" class="form-control form-control-sm" name="experiencia" value="{{ old('experiencia') }}" placeholder="Ingrese Experiencia">
                            </div>

                            <div class="col-sm-4">
                                <select class="form-control form-control-sm" name="tipo_experiencia" value="{{ old('tipo_experiencia') }}" id="tipo_experiencia"> 
                                    <option value="">Seleccione</option>
                                    <option value="1">Años</option>
                                    <option value="2">Meses</option>
                                    <option value="3">Horas</option>
                                </select>
                            </div>


                        </div>

                    <!-- POR ZONA DE TRABAJO  -->

                        <div class="form-group row">
                            <label for="especialidad" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Zona de Trabajo') }}</label>

                            <div class="col-md-2">
                                <?php echo \Form::select('departamento',$lista_depa,'0',array('Class'=>'form-control form-control-sm  ','id'=>'departamento','onchange'=>'ComboboxProvincia(this)')) ?>
                            </div>

                            <div class="col-md-2">
                                
                                 <?php echo \Form::select('provincia',$lista_prov,'0',array('Class'=>'form-control form-control-sm','id'=>'provincia','onchange'=>'ComboboxDistrito(this)')) ?>
                            </div>


                            <div class="col-md-2">
                              
                                <?php echo \Form::select('distrito',$lista_dist,'0',array('Class'=>'form-control form-control-sm','id'=>'distrito')) ?>
                            </div>
                        </div>

                    <!-- BUSCAR POR PRINCIPALES TAREAS  -->

                    <div class="form-group row">
                            <label for="tareas" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Funciones del puesto') }}</label>

                            <div class="col-md-6">
                                <input id="tareas" type="text" class="form-control form-control-sm" name="tareas" value="{{ old('tareas') }}" placeholder="Ingrese las principales funciones">
                            </div>
                        </div>

                

               <!--   MODALIDADES DE TRABAJO  -->

                                <div class="form-group row">
                            
                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Modalidades') }}</label>
                            
                            <div class="col-md-6">
                                <select class="form-control form-control-sm" name="modalidades" value="{{ old('modalidades') }}" id="modalidades"> 
                                    <option value="">Seleccione</option>
                                    <option value="1">Permanente</option>
                                    <option value="2">Eventual</option>
                                    <option value="3">Bajo Cualquier Modalidad</option>
                                </select>
                                <span class="modalidades" style="color:red; font-size:12px;"></span>
                            </div>
                        </div>

                         <!--   HORARIO DE TRABAJO  -->

                         <div class="form-group row">
                            
                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Horario de Trabajo') }}</label>
                            
                            <div class="col-md-6">
                                <select class="form-control form-control-sm" name="horario_trabajo" value="{{ old('horario_trabajo') }}" id="horario_trabajo"> 
                                    <option value="">Seleccione</option>
                                    <option value="1">A Tiempo Completo</option>
                                    <option value="2">A Tiempo Parcial</option>
                                    <option value="3">tarde</option>
                                    <option value="4">Bajo Cualquier Modalidad</option>
                                </select>
                                <span class="horario_trabajo" style="color:red; font-size:12px;"></span>
                            </div>
                        </div>

       <!--   TURNO DE TRABAJO -->

       <div class="form-group row">
                            
                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Turno Trabajo') }}</label>
                            
                            <div class="col-md-6">
                                <select class="form-control form-control-sm" name="turno_trabajo" value="{{ old('turno_trabajo') }}" id="turno_trabajo" > 
                                    <option value="">Seleccione</option>
                                    <option value="1">Fijo</option>
                                    <option value="2">Rotativo</option>
                                </select>
                                <span class="turno_trabajo" style="color:red; font-size:12px;"></span>
                            </div>
                        </div>


                    <!--CREAR CUENTA -->
                    <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Buscar Pedido') }}
                                </button>
                            </div>
                        </div>
<script type="text/javascript">
    function ComboboxProvincia(idc){
        var base_url=$("#base_url").val();
        var comxDeparment=$("#"+idc.id).val();
        var urlData=base_url+"ubigeo/provincia/"+comxDeparment;
        $.ajax({
                type:'GET',
                url:urlData,
                dataType:'JSON',  
                 beforeSend: function(){
               $(".loader").css({display:'block'});
              }      
        }).done(function( data, textStatus, jqXHR ){

                var texto='';
                texto+='<option value="00">*SELECCIÓNE*</option>';     
                text='<option value="00">*SELECCIÓNE*</option>';     
                if(data['success']!="false" && data['success']!=false){
                    if(typeof(data['ResulProvincia'])!='undefined'){
                      $.each(data['ResulProvincia'], function(i, field){
                        texto+='<option value="'+field.ubi_codprov+'">'+field.ubi_descripcion+'</option>';     
                      });
                      $('#provincia').html(texto); 
                       $('#distrito').html(text);  
                    }else{
                      $('#provincia').html(texto);
                      $('#distrito').html(text);
                    }
                }else{
                  $('#provincia').html(text);
                  $('#distrito').html(text);
                }
            $(".loader").css({display:'none'});
          }).fail(function( jqXHR, textStatus, errorThrown ){
            mensaje='Solicitud fallida: ' + textStatus;
            $(".mensajeGlobal").SendMessajeAlert({
                messaje:mensaje+textStatus,
                dataType:'Error',
                type:'danger',
                time:6000
            });
             $(".loader").css({display:'none'});
          }); 
    }
    function ComboboxDistrito(){
           var base_url=$("#base_url").val();
          var comxDeparment=$("#departamento").val();
          var comxProvin = $("#provincia").val();
          var urlData=base_url+"ubigeo/distrito/"+comxDeparment+'/'+comxProvin;
          $.ajax({
                type:'GET',
                url:urlData,
                dataType:'JSON', 
                 beforeSend: function(){
               $(".loader").css({display:'block'});
              }      
            }).done(function( data, textStatus, jqXHR ){
                var texto='';
                texto+='<option value="00">*SELECCIÓNE*</option>';     
                if(data['success']!="false" && data['success']!=false){
                    if(typeof(data['ResulDistrito'])!='undefined'){
                      $.each(data['ResulDistrito'], function(i, field){
                        texto+='<option value="'+field.ubi_coddist+'">'+field.ubi_descripcion+'</option>';     
                      });
                      $('#distrito').html(texto);
                  }else{
                      $('#distrito').html(texto);
                  }
                }else{
                  $('#distrito').html(texto);
                }
            $(".loader").css({display:'none'});
          }).fail(function( jqXHR, textStatus, errorThrown ){
           mensaje='Solicitud fallida: ' + textStatus;
            $(".mensajeGlobal").SendMessajeAlert({
                messaje:mensaje+textStatus,
                dataType:'Error',
                type:'danger',
                time:6000
            });
             $(".loader").css({display:'none'});
          }); 
    }
</script>
                    </form>

                </diV>
            </div>
        </div>   
    </div>
</div>        
@include('maestro/footer_usuario')
@endsection