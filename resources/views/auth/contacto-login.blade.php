@extends('layouts.app-new')

@section('content')
<style>
.titulo_login_segundo{
    text-align:center;
}
</style>
<br>

<div class="container">
    <form method="POST" action="{{ route('contacto.login.submit') }}">
<br>
    <div class="row justify-content-center">
        <div class="col-10">
            
            @csrf
            <h5 class="titulo_login_segundo"><b>INGRESA A TU CUENTA</b></h5>
            <br>
            <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-nuevo" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Contraseña</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-nuevo" name="password" required placeholder="Contraseña">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
            
        </div> 
    </div>

 
  <div class="form-group row justify-content-center">
        <div class="col-6">
            <button type="submit" class="btn btn_portal"  style="font-size: 14px;color:#fff; background:#00A4D3;width:100%;" >
                Ingresar
            </button>
        </div>
    </div>
  

    <div class="form-group row mb-0 justify-content-center">
        <div class="col-10">
            <div class="form-check">
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('contacto.password.request') }}">
                        {{ __('¿Olvidó la contraseña?') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
<br>
    <div class="form-group row justify-content-center">
        <div class="col-10">
           <font>¿No tiene una cuenta? </font> <a href="{{ route('contacto.register', ['e' => $entidad]) }}"><font>Registrate</font></a>
        </div>
    </div>

</form>  

</div>
<!--

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
              <h2>Login Empresa</h2>
                <div class="card-body">
                <div>
							<img class="d-block w-100" src="{{ asset('images/icon-logo.jpg') }}" alt="Second slide">
                    </div>
                    <form method="POST" action="{{ route('admin.login.submit') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Usuario') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="login loginmodal-submit" style="width:100%;">
                                    {{ __('Ingresar') }}
                                </button>

                                
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuérdame') }}
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-check">
                               
                                        <a class="btn btn-link" href="{{ route('contacto.password.request') }}">
                                            {{ __('¿Olvidaste tu contraseña?') }}
                                        </a>
                            
                                </div>
                            </div>

                        <a href="{{ route('contacto.register') }}">¿No tiene una cuenta? Registrese</a>

                        </div>

                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection
