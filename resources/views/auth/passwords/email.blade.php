@extends('layouts.app')

@section('content')
<body style="background-repeat: no-repeat;
   background-position: 100% 100%;background-attachment: fixed;
   background-image:url({{ asset('fondo/fondo-persona.png') }});">
<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-6"> <br><br><br><br>
            <div class="card">
                <div class="card-header" style="font-family: Galano Grotesque Bold;    font-size: 19px;">{{ __('Recuperar Contraseña') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row mt-5 mb-5">
                            <label for="email" class="col-md-4 col-form-label text-md-right" style="font-family: Galano Grotesque Medium;">{{ __('Correo Electronico') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert" style="font-family: Galano Grotesque Bold;background-color:#3850A0;color:#ffffff">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0 mt-4">
                            <div class="col-md-6 offset-md-4">
                                <button  type="submit" class="btn " style="font-family: Galano Grotesque Bold;background-color:#3850A0;color:#ffffff">
                                    {{ __('Enviar Correo') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br>
@include('maestro.footer_portal')
</body>
@endsection
