
@extends('layouts.app')
@section('content')
<style>
.text_empre h4{font-family:Galano Grotesque Black;background: #575756;
    color: #ffffff; }
.text_empre label{ font-family: Galano Grotesque Medium; color: #3850A0;    font-size: 0.9rem;}
.text_empre input{   font-family: Galano-Grotesque Light; color: #575756;}
.text_empre .card-header{background-color:#575756;}
.text_empre .regit_button{background-color: #b00002;color: #ffffff;  font-family: Galano Grotesque Medium;}
.text_empre .titulo_formulario{font-family: Galano Grotesque Medium;}
.text_empre select,textarea{font-family: Galano-Grotesque Light;}
</style>


<div class="container text_empre" style="padding-top: 20px;">
   @if(isset($tipo) && $tipo == '2')

   <form method="POST" action="{{ route('entidad.empresa.submit') }}" id="formulario_persona">
      @else
   <form method="POST" action="{{ route('contacto.create') }}" class="registroformulario">
      @endif
      <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
      <input type="hidden" id="base_url" value="{{ asset('') }}">
      <input type="hidden" id="entidad" name="entidad" value="{{ $entidad }}">
      <div class="row justify-content-center" style="background-color: #d8d8d8;">
         <div class="col-md-10 mt-4 mb-4">
            <div class="card">
            <div class="card-header text-center"><h4>REGISTRO DE EMPRESA</h4></div>
               <div class="card-body">
                  @csrf
                  <div class="form-group row">
                     <div class="col-sm-12">
                        <h5 class="titulo_formulario " style="color:black;">
                           <!-- <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;"> -->
                           Datos de Acceso
                        </h5>
                     </div>
                  </div>
                  <!-- CORREO ELECTRONICO -->
                  <div class="form-group row">
                     <label for="email" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Correo Electronico') }}</label>
                     <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-sm" name="email" value="{{ old('email') }}" required placeholder="Ingrese Correo Electronico">
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- CONTRASEÑA -->
                  <div class="form-group row">
                     <label for="password" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Contraseña') }}</label>
                     <div class="col-md-6">
                        <input onkeypress="return space(event);" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" name="password" required placeholder="Ingrese Contraseña">
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- COMFIRMAR CONTRASEÑA -->
                  <div class="form-group row">
                     <label for="password-confirm" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Confirmar Contraseña') }}</label>
                     <div class="col-md-6">
                        <input onkeypress="return space(event);" id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" required placeholder="Confirmar la Contraseña">
                     </div>
                  </div>
               </div>
            </div>
            <br>
            <!-- DATOS DE EMPRESA -->
            <div class="card">
               <div class="card-body">
                  <div class="form-group row">
                     <div class="col-sm-12">
                        <h5 class="titulo_formulario " style="color:black;">
                           <!-- <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;"> -->
                           Datos de la Empresa
                        </h5>
                     </div>
                  </div>
                  <!-- TIPO DOCUMENTO -->
                  <div class="form-group row">
                     <label for="tipo_documento" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Tipo documento') }}</label>
                     <div class="col-sm-6">
                        <select class="form-control form-control-sm" name="tipo_documento" value="{{ old('tipo_documento') }}" id="tipo_documento" required autofocus>
                           @foreach($documentos as $documento)
                           <option value="{{ $documento->id }}">{{ $documento->tipdoc_descripion }}</option>
                           @endforeach
                        </select>
                        @if ($errors->has('tipo_documento'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('tipo_documento') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- NUMERO DOCUMENTO -->
                  <div class="form-group row">
                     <label for="numero_documento" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Numero de documento') }}</label>
                     <div class="col-md-6">

                        <input pattern="[0-9]+"  id="numero_documento"  onblur="validaRuc(this)" maxlength="11" type="text" class="form-control{{ $errors->has('numero_documento') ? ' is-invalid' : '' }} form-control-sm" name="numero_documento" value="{{ old('numero_documento') }}" required autofocus placeholder="Ingrese Numero de Documento" onkeypress="return checkTypeDocument(event)">
                        <span class="invalid-feedback" role="alert">
                        <strong id="msj_numero_documento"></strong>
                        </span>
                        @if ($errors->has('empresas.numero_documento'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('Numero Documento') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="col-md-2">

                   <button type="button" onclick="buscarNombre(this)">Buscar</button>
                </div>
                  </div>
                  <!--   RAZION SOCIAL  -->
                  <div class="form-group row">
                     <label for="razon_social" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Razon Social') }}</label>
                     <div class="col-md-6">
                        <input id="razon_social" type="text" class="form-control{{ $errors->has('razon_social') ? ' is-invalid' : '' }} form-control-sm" name="razon_social" value="{{ old('razon_social') }}" required autofocus placeholder="Ingrese Razon Social" onkeypress="return validateSpecialCharacters(event)">
                        @if ($errors->has('razon_social'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('razon_social') }}</strong>
                        </span>
                        @endif
                     </div>

                  </div>
                  <!--   DESCRIPCION  -->
                  <div class="form-group row">
                     <label for="descripcion" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Descripción') }}</label>
                     <div class="col-md-6">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="descripcion" placeholder="Ingrese Una Descripcion" required autofocus></textarea>
                        @if ($errors->has('descripcion'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!--   RUBRO  -->
                  <div class="form-group row">
                     <label for="rubro" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Rubro de la Empresa') }}</label>
                     <div class="col-md-6">
                        <div class="control-group">
                           <select id="rubro" class="demo-default form-control form-control-sm buscar" data-placeholder="Seleccione Rubro" name="rubro" onfocus="limiartodo(this)" required>
                              <option value="">Buscar</option>
                              @foreach($sector as $sec)
                              <option value="{{ $sec->id }}" >{{ $sec->sec_descripcion }}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <!--   SITIO WEB  -->
                  <div class="form-group row">
                     <label for="sitio_web" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Sitio Web') }}</label>
                     <div class="col-md-6">
                        <input id="sitio_web" type="text" class="form-control form-control-sm" name="sitio_web" value="{{ old('sitio_web') }}" placeholder="Ingrese El Sitio Web">
                        @if ($errors->has('sitio_web'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('sitio_web') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!--   DIRECCION   -->
                  <div class="form-group row">
                     <label for="direccion" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Direccion') }}</label>
                     <div class="col-md-6">
                        <input id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }} form-control-sm" name="direccion" value="{{ old('direccion') }}" required autofocus placeholder="Ingrese la Direccion">
                        @if ($errors->has('direccion'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('direccion') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!--   UBIGEO  -->
                  <div class="form-group row">
                     <div class="col-12 col-sm-12 col-md-4 mb-2">
                        <select class="form-control form-control-sm" name="departamento" value="{{ old('departamento') }}" id="departamento" required autofocus onchange="ComboboxProvincia(this)">
                           <option value="">Seleccione Departamento</option>
                           @foreach($ubigeo as $ubi)
                           <option value="{{ $ubi->ubi_coddpto }}">{{ $ubi->ubi_descripcion }}</option>
                           @endforeach
                        </select>
                     </div>
                     <br>
                     <div class="col-12 col-sm-12 col-md-4 mb-2">
                        <select class="form-control{{ $errors->has('provincia') ? ' is-invalid' : '' }} form-control-sm" name="provincia" value="{{ old('provincia') }}" id="provincia" required autofocus onchange="ComboboxDistrito(this)">
                           <option value="0">Seleccione Provincia</option>
                        </select>
                     </div>
                     <br>
                     <div class="col-12 col-sm-12 col-md-4 mb-2">
                        <select class="form-control{{ $errors->has('distrito') ? ' is-invalid' : '' }} form-control-sm" name="distrito" value="{{ old('distrito') }}" id="distrito" required autofocus >
                           <option value="0">Seleccione Distrito</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <br>
            <!-- DATOS DE CONTACTO -->
            <div class="card">
               <div class="card-body">
                  <div class="form-group row">
                     <div class="col-sm-12">
                        <h5 class="titulo_formulario " style="color:black;">
                           <!-- <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;"> -->
                           Datos del Contacto de la Empresa
                        </h5>
                     </div>
                  </div>
                  <!-- NOMBRE -->
                  <div class="form-group row">
                     <label for="name" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Nombres') }}</label>
                     <div class="col-md-6">
                        <input
                           id="name"
                           type="text"
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-sm"
                           name="name"
                           value="{{ old('name') }}"
                           required
                           autofocus
                           placeholder="Ingrese Nombre"
                           onkeypress="return validateSpecialCharacters(event);">
                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- TIPO DOCUMENTO -->
                  <div class="form-group row">
                     <label for="tipo_documento_contacto" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Tipo documento') }}</label>
                     <div class="col-sm-6">
                        <select class="form-control{{ $errors->has('tipo_documento_contacto') ? ' is-invalid' : '' }} form-control-sm" name="tipo_documento_contacto" value="{{ old('tipo_documento_contacto') }}" id="tipo_documento_contacto" required autofocus>
                           @foreach($documentos as $documento)
                           @if($documento->id != '6')
                           <option value="{{ $documento->id }}">{{ $documento->tipdoc_descripion }}</option>
                           @endif
                           @endforeach
                        </select>
                        @if ($errors->has('tipo_documento_contacto'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('tipo_documento_contacto') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- NUMERO DOCUMENTO -->
                  <div class="form-group row">
                     <label for="numero_documento_contacto" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Numero de documento') }}</label>
                     <div class="col-md-6">
                        <input
                           id="numero_documento_contacto"
                           type="text"
                           class="form-control{{ $errors->has('numero_documento_contacto') ? ' is-invalid' : '' }} form-control-sm"
                           name="numero_documento_contacto"
                           value="{{ old('numero_documento_contacto') }}"
                           required
                           autofocus
                           placeholder="Ingrese Numero de Documento"
                           onkeypress="return valideKey(event);">
                        @if ($errors->has('numero_documento_contacto'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('numero_documento_contacto Documento') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- CARGO -->
                  <div class="form-group row">
                     <label for="cargo" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Cargo') }}</label>
                     <div class="col-md-6">
                        <input id="cargo" onkeypress="return validateSpecialCharacters(event)" type="text" class="form-control{{ $errors->has('cargo') ? ' is-invalid' : '' }} form-control-sm" name="cargo" value="{{ old('cargo') }}" required autofocus placeholder="Ingrese Cargo">
                        @if ($errors->has('cargo'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cargo') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- CELULAR -->
                  <div class="form-group row">
                     <label for="telefono" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Telefono') }}</label>
                     <div class="col-md-6">
                        <input
                           id="telefono"
                           type="text"
                           class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }} form-control-sm"
                           name="telefono"
                           value="{{ old('telefono') }}"
                           required
                           autofocus
                           placeholder="Ingrese Telefono"
                           onkeypress="return telefonoNumero(event);"
                           max="9">
                        @if ($errors->has('telefono'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                        @endif
                     </div>
                  </div>
                  <!-- CORREO ELECTRONICO -->
                  <div class="form-group row">
                     <label for="email_contacto" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Correo Electronico') }}</label>
                     <div class="col-md-6">
                        <input id="email_contacto" type="email" class="form-control{{ $errors->has('email_contacto') ? ' is-invalid' : '' }} form-control-sm" name="email_contacto" value="{{ old('email_contacto') }}" required autofocus placeholder="Ingrese Correo Electronico">
                        @if ($errors->has('email_contacto'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email_contacto') }}</strong>
                        </span>
                        @endif
                     </div>

                  </div>

               </div>
               <div class="form-group row ">
               <div class="form-group col-md-3"></div>
                  <div class="form-group col-md-9">
                     <label><input type="checkbox" required name="leertInvor" id="leertInvor"> He leído y acepto todo los <a href="{{asset('contacto/termines-condiciones')}}" target="_blank">Términos y Condiciones</a> </label>
                     </div>
                     </div>
               <br>
               <div class="form-group row mb-5">
                  <div class="col-md-6 offset-md-4 text-center">
                     <button type="submit" class="btn regit_button">
                     {{ __('Crear Cuenta') }}
                     </button><br>
                  </div>
               </div>
            </div>
         </div>
   </form>
   </div>
   <br><br>
</div>
<script>
   function valideKey(evt){

    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    var digitos = document.querySelector('#numero_documento_contacto').value.length;
    let tipo_documento = document.getElementById('tipo_documento_contacto').value;
   if(tipo_documento == '2'){
      if(digitos >= 8){
         return false;
      }
   }else{
      if(digitos >= 10){
         return false;
      }
   }
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}

function telefonoNumero(evt){

    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    var digitos = evt.target.value.length;

   if(digitos >= 9){
      return false;
   }
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}

function space(evt){

   var code = (evt.which) ? evt.which : evt.keyCode;
   if(code==32) { // backspace.
      return false;
    } else{ // other keys.
      return true;
   }
}

function soloLetras(e){

       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }

   function validaRuc(input){
       var _token = $("#csrf-token").val();
       base_url = $("#base_url").val();
       var urlajax = base_url + "valida/ruc/empresa/" + input.value;

       $.ajax({
           type: 'GET',
           url: urlajax,
           dataType: 'json',
           success: function(data) {
                   if(data == "1"){
                   $("#numero_documento").addClass('is-invalid');
                   $("#msj_numero_documento").html('Este RUC ya se encuentra registrado');
                   }else{
                   $("#numero_documento").removeClass('is-invalid');
                   }
           }
       });
   }
   function buscarNombre(btn){
      var tipo=$("#numero_documento").val();
     var base_url=$("#base_url").val();
    $.ajax({
      url:base_url+'recsunat/sunat?ruc='+tipo,
      type: 'GET',
      dataType:'JSON',
      beforeSend: function(){
      $(btn).html('Buscando..');
      },
  }).done(function(data , textStatus, jqXHR ){
    $(btn).html('Buscar');
   //var cod_prov=data['getDatosPrincipales']['cod_prov'];
        $("#razon_social").val(data['getDatosPrincipales']['ddp_nombre']);
        $("#direccion").val(data['getDomicilioLegal']);
        $("#departamento").val(data['getDatosPrincipales']['cod_dep']);
        //ComboboxProvincia({value:data['getDatosPrincipales']['cod_dep']})
        //ComboboxDistrito({value:data['getDatosPrincipales']['cod_prov']})

  }).fail(function(jqXHR, ajaxOptions, thrownError){
    $(btn).html('Buscar');
  });
   }


   // CCAPA - Luis Jauregui

   function convertKeyToLowerCase(key) {
       return String.fromCharCode(key).toLowerCase();
   }

   function checkCharacter(allowedCharacters, key) {
       return allowedCharacters.indexOf(key) != -1;
   }

   function checkSpecialCharacters(allowedUnicodeCharacters, key) {
      for(let i in allowedUnicodeCharacters){
         if(key === allowedUnicodeCharacters[i]){
               return true;
         }
      }
      return false;
   }

   function validateSpecialCharacters(event) {

      let key = (event.which) ? event.which : event.keyCode;
      let keyLowerCase = convertKeyToLowerCase(key);

      const allowedUnicodeCharacters = [ 8, 32 ];
      let allowedCharacters = "áéíóúabcdefghijklmnñopqrstuvwxyz";

      isActiveSpecialCharacter = false
      hasCharacter = false

      if (checkSpecialCharacters(allowedUnicodeCharacters, key)) {
          isActiveSpecialCharacter = true;
      }

      if (checkCharacter(allowedCharacters, keyLowerCase)) {
          hasCharacter = true;
      }

      if(hasCharacter || isActiveSpecialCharacter){
         return true;
       } else {
         return false;
       }
   }

   function checkTypeDocument(event) {

    let key = (event.which) ? event.which : event.keyCode;

    // DNI = 2, RUC = 6, CARNET = 5
    let totalLengthNumeroDocumento = document.querySelector('#numero_documento').value.length;
    let typeDocument = document.getElementById('tipo_documento').value;

    if(typeDocument == '2'){
      if(totalLengthNumeroDocumento >= 8){
         return false;
      }
    }

    if(typeDocument == '5'){
      if(totalLengthNumeroDocumento >= 10){
         return false;
      }
    }

    if(typeDocument == '6'){
      if(totalLengthNumeroDocumento >= 11){
         return false;
      }
    }

    if(key == 8) {
      return true;
    } else if (key >= 48 && key <= 57) {
      return true;
    } else {
      return false;
    }
   }

</script>

@endsection
