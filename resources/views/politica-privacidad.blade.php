@extends('layouts.app')
@section('content')
@include('maestro.cabezera')
@if($inicio == "1")
<style>
.container{
    max-width: 800px;
}
</style>
@endif
<div class="container">
<br>
<h3>Politica de Privacidad</h3>
    <b>1. ¿Qué información recolectamos?</b>
    <p align="justify">Al momento de registrarse en el Portal, recopilamos la siguiente información:  </p>
    <p align="justify">
    En el caso de postulantes: nombres y apellidos, tipo y número de documento de identidad, correo electrónico, fecha de nacimiento, contraseña, ocupación de preferencia.<br>
En el caso de empresas: datos de acceso (correo electrónico y contraseña), datos de la empresa (RUC, razón social, rubro de negocio, ubicación), datos de la persona de contacto (nombre y apellidos, teléfonos, puesto y área).
</p>
<p>El Usuario también puede registrarse en el Portal utilizando servicios de terceros, en particular, redes sociales. Para ello, requerirá ingresar el nombre de Usuario y clave (“Credenciales de Acceso”) utilizadas en dichas redes sociales. Al ingresar estas Credenciales de Acceso el Usuario acepta que se regirá por los términos y condiciones de dichos servicios de terceros en lo que resulte aplicable, y autoriza a Accede a acceder a la información de la cuenta registrada en dicho servicio.</p>
<br>
<p>Asimismo, se requiere almacenar información relativa al comportamiento del Usuario dentro del Portal, entre la que se incluye:</p>
<p>
    La URL de la que proviene el Usuario (incluyendo las externas al Portal).<br>
    URLs más visitadas por el Usuario (incluyendo las externas al Portal).<br>
    Direcciones de IP.<br>
    Navegador que utiliza el Usuario.<br>
    Todas las actividades realizadas dentro del Portal.<br>
    Información sobre la operativa del portal, tráfico, promociones, campañas de venta, estadísticas de navegación, historial de pago, entre otros.<br>
</p><br>
<p>Finalmente, el Portal también recoge la información necesaria para que el Usuario pueda postular y/o ser contactado por las empresas interesadas. Para este fin, el Usuario completa su perfil con datos académicos y profesionales, generando su Currículum Vitae. A manera enunciativa más no limitativa se recoge información relativa al grado de estudios, universidades o instituciones, experiencia profesional, empresas en las que el postulante ha trabajado, puesto y área, conocimientos de informática e idiomas, entre otros.</p>
<br>


<b>2.Sobre la veracidad de la información que recolectamos</b>
<p align="justify">El Usuario, al registrarse y utilizar el Portal, declara que toda la información proporcionada es verdadera, completa y exacta. Cada Usuario es responsable por la veracidad, exactitud, vigencia y autenticidad de la información suministrada, y se compromete a mantenerla debidamente actualizada.</p>

<p align="justify">Sin perjuicio de lo anterior, el Usuario autoriza a Accede a verificar la veracidad de los datos personales facilitados por el Usuario a través de información obtenida de fuentes de acceso público o entidades especializadas en la provisión de dicha información..</p>


<p align="justify">Accede no se hace responsable de la veracidad de la información que no sea de elaboración propia, por lo que tampoco asume responsabilidad alguna por posibles daños o perjuicios que pudieran originarse por el uso de dicha información.</p>


<b>3.¿Cómo conservamos su información personal?</b>

<p align="justify">De acuerdo a lo establecido en la Ley N° 29733, Ley de Protección de Datos Personales, y el Decreto Supremo N° 003-2013-JUS, por el que se aprueba el Reglamento de la Ley de Protección de Datos Personales, Accede informa a los Usuarios del Portal que todos los datos de carácter personal que nos faciliten serán incorporados a un banco de datos, debidamente inscrito en la Dirección de Registro Nacional de Protección de Datos Personales</p>

<p align="justify">A través de la presente Política de Privacidad el Usuario da su consentimiento expreso para la inclusión de sus datos personales en el mencionado banco de datos.</p>

<b>4.¿Para qué utilizamos su información personal?</b>


<p align="justify">Los datos personales de los Usuarios del Portal son tratados con las siguientes finalidades:</p>

<p align="justify">
    Atender y procesar solicitudes de registro de Usuarios en el Portal, brindar soporte al Usuario y validar la veracidad de la información de registro.  <br>
    Gestionar y administrar las cuentas personales de los Usuarios, así como supervisar el comportamiento y la actividad del Usuario dentro del Portal<br>
    Formar parte del banco de datos de postulantes del Portal, a fin de facilitar el contacto con la empresa gestora de una convocatoria u oferta de empleo, así como ser contactado por empresas interesadas en cubrir algún puesto de trabajo.<br>
    Contactar al Usuario interesado en utilizar algún servicio ofrecido por el Portal o recibir información sobre los servicios del Portal.<br>
    Realizar estudios internos sobre los intereses, comportamientos y hábitos de conducta de los Usuarios a fin poder ofrecerles un mejor servicio de acuerdo a sus necesidades, con información que pueda ser de su interés.<br>
    Brindar información a los Usuarios sobre los nuevos servicios y promociones ofrecidos por el Portal y/o por las empresas anunciantes.<br>
    Gestionar los concursos, y promociones que se realicen con los Usuarios registrados en el Portal.<br>
    Informar sobre los ganadores de premios, promociones, concursos y/ o sorteos realizados por el Portal. Los Usuarios que participen en las promociones, concursos o sorteos mencionados, autorizan expresamente a que el Portal difunda por los medios que estime convenientes los datos personales e imágenes de los ganadores, sin derecho a compensación alguna.<br>
    Compartir los datos personales de los Usuarios con terceros tales como empresas que contribuyan a mejorar o facilitar la operativa del Portal, servicios de transporte, medios de pago, proveedores de seguros, gestores, entre otros.  <br>
</p>

<p align="justify">Adicionalmente, los datos del Usuario serán utilizados con la finalidad de enviarle noticias, promociones, publicidad y novedades del Portal a través de comunicaciones periódicas que serán remitidas a la dirección de correo electrónico que el Usuario facilitó al momento de realizar el registro. Dichas comunicaciones serán consideradas solicitadas y no califican como “spam” bajo la normativa vigente.  </p>


<p align="justify">Accede informa al Usuario que una vez autorizado el envío de las mencionadas comunicaciones, tendrá la facultad de revocar el consentimiento prestado en cada una de las comunicaciones que reciba de Accede.com a través de un hipervínculo que se incorpora en todas las comunicaciones, o enviando una solicitud a la siguiente dirección de correo electrónico: soporte@accede.com.pe.   </p>

<p align="justify">El Usuario del Portal manifiesta expresamente que ha sido informado de todas las finalidades antes mencionadas y autoriza el tratamiento de sus datos personales  con dichas finalidades.</p>

<p align="justify">Accede le recuerda al Usuario que las finalidades de tratamiento de datos necesarias para la ejecución de la relación contractual que vincula al Usuario registrado y a Accede no requieren del consentimiento del mismo. </p>

<b>5.¿Cómo resguardamos su información personal?</b>

<p align="justify">Accede, a través del Portal, adopta las medidas técnicas y organizativas necesarias para garantizar la protección de los datos de carácter personal y evitar su alteración, pérdida, tratamiento y/o acceso no autorizado, habida cuenta del estado de la técnica, la naturaleza de los datos almacenados y los riesgos a que están expuestos, todo ello, conforme a lo establecido por la Ley N° 29733, Ley de Protección de Datos Personales, su Reglamento y la Directiva de Seguridad.</p>


<p align="justify">En este sentido, Accede usará los estándares de la industria en materia de protección de la confidencialidad de la información personal de los Usuarios del Portal..</p>


<p align="justify">El Portal facilita a los Usuarios procedimientos físicos de control de datos como cambios de contraseña, datos de cuenta y similares con el fin de salvaguardar su privacidad dentro del portal.</p>

<p>Accede emplea diversas técnicas de seguridad para proteger tales datos de accesos no autorizados. Sin perjuicio de ello, Accede no se hace responsable por interceptaciones ilegales o violación de sus sistemas o bases de datos por parte de personas no autorizadas, así como la indebida utilización de la información obtenida por esos medios, o de cualquier intromisión ilegítima que escape al control de Accede y que no le sea imputable.</p>

<p align="justify">Accede tampoco se hace responsable de posibles daños o perjuicios que se pudieran derivar de interferencias, omisiones, interrupciones, virus informáticos, averías telefónicas o desconexiones en el funcionamiento operativo de este sistema electrónico, motivadas por causas ajenas a Accede; de retrasos o bloqueos en el uso de la plataforma informática causados por deficiencias o sobrecargas en el Centro de Procesos de Datos, en el sistema de Internet o en otros sistemas electrónicos.</p>

<b>6.¿Con quiénes compartimos información?</b>

<p align="justify">Forma parte de la actividad propia del Portal, dar a conocer los datos del Usuario postulante a las empresas anunciantes que publican convocatorias u ofertas de trabajo, a fin de que éstas puedan contactar con el o los postulantes de su interés. Al marcar el botón “Postula” que figura en cada convocatoria el postulante enviará información de su perfil a la empresa gestora de la publicación. Asimismo, los datos del Usuario serán visualizados en la base de datos de CV del Portal siempre que haya escogido la opción de “Perfil público”.</p>

<p>Accede se compromete a no divulgar o compartir con terceros la información personal recabada de los Usuarios sin que se haya prestado el debido consentimiento para ello, con excepción de los siguientes casos:</p>

<p align="justify">
    En aquellos casos en que el uso de los datos personales sea necesario para la prestación de los servicios brindados a través del Portal.<br>
    Solicitud de información de autoridades públicas en ejercicio de sus funciones y el ámbito de sus competencias.<br>
    Solicitud de información en virtud de órdenes judiciales.<br>
    Solicitud de información en virtud de disposiciones legales<br>
</p>

<b>7.Cookies </b>

<p align="justify">El Portal utiliza cookies. Una “Cookie” es un pequeño archivo de texto que un sitio web almacena en el navegador del Usuario. Las cookies facilitan el uso y la navegación por una página web y son esenciales para el funcionamiento de Internet, aportando innumerables ventajas en la prestación de servicios interactivos.</p>
<p align="justify">El Portal podrá utilizar información de las visitas de los Usuarios para realizar evaluaciones y cálculos estadísticos sobre datos anónimos, así como para garantizar la continuidad del servicio o para realizar mejoras en el portal</p>

<p align="justify">El Portal también utilizará la información obtenida a través las cookies para analizar los hábitos de navegación del Usuario y las búsquedas realizadas por éste, a fin de mejorar sus iniciativas comerciales y promocionales, mostrando publicidad que pueda ser de su interés, y personalizando los contenidos del Portal.</p>

<p align="justify">Las cookies pueden borrarse del disco duro si el Usuario así lo desea. La mayoría de los navegadores aceptan las cookies de forma automática, pero le permiten al Usuario cambiar la configuración de su navegador para que rechace la instalación de cookies, sin que ello perjudique su acceso y navegación por el Portal.</p>

<p align="justify">En el supuesto de que en el presente Portal se dispusieran enlaces o hipervínculos hacia otros lugares de Internet propiedad de terceros que utilicen cookies, Accede no se hace responsable ni controla el uso de cookies por parte de dichos terceros.</p>

<b>8.Derechos de acceso, rectificación, cancelación y oposición de datos personales</b>

<p align="justify">Accede pone en conocimiento del Usuario su derecho de acceso, actualización y cancelación de su información personal, los mismos que podrá hacerlo desde el mismo portal.</p>
<p align="justify">Del mismo modo, el Usuario puede oponerse al uso o tratamiento de sus datos personales y puede solicitar ser informado sobre todas las finalidades con que se tratan los datos personales.</p>
<p align="justify">Sin perjuicio de lo anterior, Accede podrá conservar determinada información personal del Usuario que solicita la baja, a fin de que sirva de prueba ante una eventual reclamación contra Accede por responsabilidades derivadas del tratamiento de dicha información. Dicha conservación deberá realizarse previo bloqueo de los datos, de manera que se impida su habitual tratamiento y su duración no podrá ser superior al plazo de prescripción legal de dichas responsabilidades.</p>
<b>Modificaciones de la Política</b>
<p align="justify">Accede se reserva expresamente el derecho a modificar, actualizar o completar en cualquier momento la presente Política de Privacidad</p>
<p align="justify">Cualquier modificación, actualización o ampliación producida en la presente Política será inmediatamente publicada en el Portal, por lo que se recomienda al Usuario revisarla periódicamente, especialmente antes de proporcionar información personal.</p>

</div>

@include('maestro.footer_portal')
@endsection
