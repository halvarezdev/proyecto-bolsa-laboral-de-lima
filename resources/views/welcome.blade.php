@extends('layouts.app-header')

@section('content')
<br>
<br>
<br>
<br>



<div class="avisosfondo_aviso" style="background-image: url('{{ asset('fondo/fondobanneranuncio.png') }}');background-repeat: no-repeat;background-size:100% 100%">
<div class="container" >
    <div class="card" style="border:none; background-color: transparent;">
      <div class="card-body">
        <center>
        <img src="{{ asset('images/bussinesicon.png') }}" alt=""  style="width: 100%;">
          <div class="d-none d-sm-none d-md-block mt-5 mb-5">
            <h2 style="color:#00A39C;font-size: 36px;"><b>Puestos destacados</b></h2>
          </div>
          <div class="d-block d-sm-block d-md-none">
            <br><br>
            <h5 style="color:#00A39C;font-size: 36px;"><b>Puestos destacados</b></h5>
          </div>
        </center>
      </div>
    </div>
  </div>  

<div class="container ">  <!-- INICIO DE CONTAINER -->
    <!-- INICIO LISTA DE AVISOS -->
    <div class="card" style="border:none;background-color: transparent;">
      <div class="card-body mt-2">


        <?php $i = 1; ?>
        @foreach ($pedidos as $pedido)
          @if ($i == 1 || $i == 5 || $i == 9)
          <div class="row" >
          @endif
            <div class="col-md-3" style="margin-bottom: 3%;">
              <div class="card" style="background-color:#ffffff; border:none;border-radius: 12px;">
                <a href="{{ route('vacante.detalle',['id' => $pedido->id_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" style="text-decoration: none;">
                  <div class="card-body text-center" style="padding-top: 10px;">
                    <img src="{{ asset('public/storage/'.$pedido->perfil_foto) }}" class="img-thumbnail rounded" alt="logo" style="height:150px; width:150px;" >

                    <center >
                    <span style="color:#000000;font-size:13px;">
                      {{ strtoupper(str_limit($pedido->razon_social,25)) }}
                    </span>
                    </center>
                    <center style="height: 48px;">
                      <b style="color:#C73C63;font-size:13px;">{{ strtoupper(str_limit($pedido->descripcion,30)) }}</b>
                    </center>
                    <span style="color:#000000;font-size:13px;">
                      Publicado&nbsp;&nbsp;{{ $pedido->created_at->format('d/m/Y')  }}
                      <br>
                      Área: {{ strtolower(str_limit($pedido->area_descripcion,18)) }}
                      <br>
                      {{ $pedido->ubi_descripcion }}
                    </span>
                  </div>
                </a>
                <div class="d-none d-sm-none d-md-block">
                  <a class="btn btn-primary nav-sub" href="{{ route('vacante.detalle',['id' => $pedido->id_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" role="button" style="font-size: 13px;width: 50%;margin-top: -4%;margin-left: 25%;padding:0px;position: absolute;background-color:#00A39C;border-color:#00A39C;font-family: Galano Grotesque Bold;">
                  VER AVISO
                  </a>
                </div>
              </div>
            </div>
          @if ($i == 4 || $i == 8 || $i == 12)
          </div>
          @endif
          <?php $i++; ?>
        @endforeach
        
        <a  href="#blogCarousel" role="button" data-slide="prev" style="left: -2%;position: absolute;top: 50%;font-size: 55px;color:#504f4f;">
              <i class="fas fa-angle-left"></i>
          <span class="sr-only">Previous</span>
        </a>
        <a href="#blogCarousel" role="button" data-slide="next" style="right: -2%;position: absolute;top: 50%;font-size: 55px;color:#504f4f;">
            <i class="fas fa-angle-right"></i>
            <span class="sr-only">Next</span>
        </a>
        <script>
        	
            $(document).ready(function(){
              $("#modal-banner").modal('show');
              $('#blogCarousel').carousel({
        				interval: 1000
              });

              function enviarBusquedaResponsive(){
                  $("#form_banner_responsive").submit();
              }
            });
        </script>
        <!-- Fin Vacantes Publicas -->
        <!--
          <div>
          <a class="btn btn-primary nav-sub" href="{{ route('users.busqueda.inicio',['e' => (isset($entidad) ? $entidad : '1')]) }}" role="button" style="width: 50%;margin-top: 3%;margin-left: 25%;position: absolute;background-color:#00A39C;border-color:#00A39C;padding: 10px;font-family: Galano Grotesque Bold;">VER TODOS LOS AVISOS</a>
        </div>
        -->
      </div>
    </div>
  </div>
  <br><br>
</div>

<!-- FIN CONTAINER -->
  <!-- <br> -->
  @include('maestro/footer_portal')
  <!--  ***********************  -->
  
        @include('maestro.login-modal')
       
  <script type="text/javascript">
    base_url = $("#base_url").val();
    function tabs_seguros(key){
      for (var i = 1; i <= 4; i++) {
        if(key==i){
          $("#tabst"+i).css("display", "block");
          $("#tabs_c"+i).addClass("activets");
          $("#tabs_c"+i+" img").attr('src',$("#tab_img"+i).val());
          $("#tabs_c"+i+" .feature-inner h2").css("color", "#ffffff");
          if(i<4){
            $("#tabs_c"+i).css("border-right", "1px solid #00A39C");
          }
        }else{
          $("#tabst"+i).css("display", "none");
          $("#tabs_c"+i).removeClass("activets");
          $("#tabs_c"+i+" img").attr('src',$("#tab_imga"+i).val());
          $("#tabs_c"+i+" .feature-inner h2").css("color", "#504f4f");
          if(i<4){
            $("#tabs_c"+i).css("border-right", "1px solid #504f4f");
          }
        }
      }
    }
  </script>
  {!! RecaptchaV3::initJs() !!}

<script>
   function onSubmit(token) {
     document.getElementById("demo-form").submit();
   }
 </script>

@endsection
