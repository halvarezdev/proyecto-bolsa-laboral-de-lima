<div class="card">
  <div class="card-body">
    <form action="{{ route('users.idioma') }}" method="post" id="form_idioma" onsubmit="validateIdi(event)">
    	@csrf
			<input name="codigo_idioma" id="codigo_idioma" value="" type="hidden">
	  <div class="hidden-mobile form-row">
	    <div class="form-group col-md-6">
	      <label>Curso</label>
	    </div>


	    <div class="form-group col-md-6">
	      <label>Nivel</label>
	    </div>
	  </div>

	  <div class="form-row">
	  <div hidden-form class="form-group col-md-6">
	      		<label>Curso</label>
	    	  </div>
	  		<div class="form-group col-md-6">
		    	<select class="selectpicker show-tick form-control form-control-sm" data-style="select-style" data-live-search="true" name="idioma" id="idioma_nombre">
						<option value="">Seleccione</option>   
						@foreach($idioma_lista as $idioma)
	            <option value="{{ $idioma->MAESP_CODIGO }}">{{ $idioma->MAESC_DESCRIPCION }}</option>
	          @endforeach
	        </select>
	        </div>
		<div hidden-form class="form-group col-md-6">
	      <label>Nivel</label>
	    </div>
			<div class="col-md-4">
	            <select class="form-control form-control-sm" name="nivel_idioma" id="nivel_idioma">
	                <option value="">Seleccione</option> 
	                <option value="1">Básico</option>
	                <option value="2">Intermedio</option>
	                <option value="3">Avanzado</option>
	            </select>
	        </div>
	  </div>
	 <br>
	  <button type="submit" class="btn btn-primary">Agregar Idioma</button>
	</form>	
  </div>
</div>

<?php 
	echo "<script>
		var idiomas = ".$idiomas.";
	</script>"
?>

<script>
	function validateIdi(e) {

		var idiomaDuplicado = false;

		var codigo_idioma = document.getElementById('idioma_nombre').value;
		var texto_idioma = document.getElementById('idioma_nombre').options[document.getElementById('idioma_nombre').selectedIndex].text;
		var nivel_idioma = document.getElementById('nivel_idioma').value;
		
		if(codigo_idioma == ''){
			alert('Seleccione un idioma');
			e.preventDefault();
			return;
		}

		if(nivel_idioma == ''){
			alert('Seleccione un nivel');
			e.preventDefault();
			return;
		}

		idiomas.forEach(function(idioma){
			if(texto_idioma == idioma.MAESC_DESCRIPCION){
				idiomaDuplicado = true;
			}
		});

		if(idiomaDuplicado){
			alert('El idioma ya se encuentra registrado');
			e.preventDefault();
			return;
		}
		
	}
</script>