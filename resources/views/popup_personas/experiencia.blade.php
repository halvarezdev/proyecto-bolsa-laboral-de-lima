<div class="card">
  <div class="card-body">
    <form action="{{ route('users.experiencia') }}" method="post" id="form_experiencia" onsubmit="validateEmpresa(event)">
    	@csrf
			<input name="codigo_experiencia" id="codigo_experiencia" type="hidden">
	  <div class="form-row">
	    <div class="form-group col-md-12">
	      <label>Empresa</label>
	      <input id="empresa" onpaste="return false"  onkeypress="return soloLetrasHoja(event);return soloLetras(event);"  type="text" class="form-control form-control-sm" name="empresa" value="{{ old('empresa') }}" placeholder="Ingrese Empresa" required
		  
		  >
	    </div>

	  </div>

	  <div class="form-row">
	  	<div class="form-group col-md-12">
	      <label>Cargo</label>
	       	<select class="selectpicker show-tick form-control form-control-sm" data-style="select-style" data-live-search="true" id="ocupacion" name="ocupacion" onchange="return ocultarOtro(this)">
								<option value="">Seleccione</option>
								<option value="000000" >OTRO</option>
                    @foreach($ocupaciones as $ocupacion)
                    <option value="{{ $ocupacion->ocup_codigo }}" >{{ $ocupacion->nombre }}</option>
                    @endforeach
            </select>
             <p style="color:red; text-aling:rigth; font-size:11px;">De no encontrar su cargo, escoger la opci&oacute;n OTRO ubicada al inicio del listado y digitarlo en la opci&oacute;n OTRO CARGO</p>
	    </div>
	  </div>
	  
	  
	  <div class="form-row" style="display:none" id="otro_experiencia">
	  	<div class="form-group col-md-12">
	      <label>Otro Cargo</label>
	        <input id="otro_cargo" type="text" class="form-control form-control-sm" name="otro_cargo" value="{{ old('otro_cargo') }}" placeholder="Ingrese Otro Cargo">
	    </div>
	  </div>
	  

	  <div class="form-row">
	  		<div class="form-group col-md-12">
	  			<label>Funciones a Cargo</label>
		    	<textarea name="funciones_cargo" id="funciones_cargo" class="form-control form-control-sm" required></textarea>
	        </div>
	  </div>
	 
	 <div class="form-row">
	 	<div class="form-group col-md-6">
	  			<label>Fecha Inicio </label>
		    	<input id="fecha_ini_empresa" type="date" class="form-control form-control-sm" name="fecha_ini_empresa" value="{{ old('fecha_ini_empresa') }}" placeholder="Ingrese Fecha" required>
	        </div>

	        <div class="form-group col-md-6">
	  			<label>Fecha Fin </label>
		    	<input id="fecha_fin_empresa" type="date" class="form-control form-control-sm" name="fecha_fin_empresa" value="{{ old('fecha_fin_empresa') }}" placeholder="Ingrese Fecha" max="{{ date('Y-m-d') }}" required> 
	        </div>
	 </div>
	  <button type="submit" class="btn btn-primary">Agregar Experiencia</button>
	</form>	
  </div>
</div>

<script>
	function ocultarOtro(input){
			if(input.value == "000000"){
				document.getElementById("otro_experiencia").style = "display:block";
			}else{
				document.getElementById("otro_experiencia").style = "display:none";
			}
	}

	function validateEmpresa(e) {
		var empresa = document.getElementById("empresa").value;
		var ocupacion = document.getElementById("ocupacion").value;
		var funciones_cargo = document.getElementById("funciones_cargo").value;
		var fecha_ini_empresa = document.getElementById("fecha_ini_empresa").value;
		var fecha_fin_empresa = document.getElementById("fecha_fin_empresa").value;

		if(empresa == ""){
			alert("Ingrese Empresa");
			e.preventDefault();
			return;
		}

		if(ocupacion == ""){
			alert("Seleccione Cargo");
			e.preventDefault();
			return;
		}


		if(ocupacion == "000000"){
			var otro_cargo = document.getElementById("otro_cargo").value;
			if(otro_cargo == ""){
				alert("Ingrese Otro Cargo");
				e.preventDefault();
				return;
			}
		}

		if(funciones_cargo == ""){
			alert("Ingrese Funciones a Cargo");
			e.preventDefault();
			return;
		}

		if(fecha_ini_empresa == ""){
			alert("Ingrese Fecha Inicio");
			e.preventDefault();
			return;
		}

		if(fecha_fin_empresa == ""){
			alert("Ingrese Fecha Fin");
			e.preventDefault();
			return;
		}

		if(fecha_ini_empresa > fecha_fin_empresa){
			alert("Fecha Inicio no puede ser mayor a Fecha Fin");
			e.preventDefault();
			return;
		}
			
	}

	function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        // if(e.target.name != 'name'){
        //     if(key==32) { // backspace.
        //        return false;
        //     }
        // } 

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
	}
</script>