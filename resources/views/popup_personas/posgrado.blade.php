<div class="card">
  <div class="card-body">
    <form 
      action="{{ route('users.posgrado') }}" 
      method="post" id="form_posgrado" 
      onsubmit="validatePos(event)">
    	@csrf
    	<div class="form-row">
      <input name="codigo_posgrado" id="codigo_posgrado" value="" type="hidden">
	    <div class="form-group col-md-12">
            <label>Especialidad</label>
            <input onpaste="return false"  onkeypress="return soloLetrasHoja(event);"  type="text" name="profesion" id="profesion_posgrado" class="form-control form-control-sm" required>
         </div>
        
	  </div>
	  <div class="form-row">

         <div class="form-group col-md-6">
            <label>Nivel Alcanzado</label>
            <select class="form-control form-control-sm" name="nivel_estudio" id="nivel_estudio_posgrado" onchange="return variarpsotd(this)">
            <option value="1">Seleccione</option>
                @foreach($grado_intruccion_posgrado as $grado)
                <option value="{{ $grado->id }}">{{ $grado->grad_descripcion }}</option>
                @endforeach
            </select>
        </div>


        <div class="form-group col-md-6">
	         <label>Estado</label>
            <select class="form-control form-control-sm" name="estado_post" id="estado_postgrado">
              <option value="">Seleccione</option>
            </select>
        </div>

	   
	  </div>


      <div class="form-row">
          <div class="form-group col-md-12">
            <label>Centro de Estudios</label>
         <input onpaste="return false"  onkeypress="return soloLetrasHoja(event);"  id="otro_centro_estudio3_posgrado" type="text" class="form-control form-control-sm" name="otro_centro_estudio3" value="{{ old('otro_centro_estudio3') }}" placeholder="Ingrese Centro de Estudio" required>   
        </div>
      </div>

	  <div class="form-row">
	  		<div class="form-group col-md-6">
			    <label>Año Inicio <span style="color:#585858">(Ejemplo 2002)</span></label>
			    <input id="fecha_incio1_posgrado" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm date-pickerf" name="fecha_incio1" value="" placeholder="2002" required>
			  </div>
			  <div class="form-group col-md-6">
			    <label>Año Término <span style="color:#585858">(Ejemplo 2015)</span></label>
			    <input id="fecha_fin1_posgrado" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm date-pickerf" name="fecha_fin1" value="" placeholder="2015" required >
			  </div>
	  </div>

	  <button type="submit" class="btn btn-primary">Agregar Formación</button>
	</form>	
  </div>
</div>

<script>

  function validatePos(e){
    var profesion_posgrado = document.getElementById('profesion_posgrado').value;
    var nivel_estudio_posgrado = document.getElementById('nivel_estudio_posgrado').value;
    var estado_postgrado = document.getElementById('estado_postgrado').value;
    var centro_estudio_posgrado = document.getElementById('otro_centro_estudio3_posgrado').value;
    var fecha_incio1 = document.getElementById("fecha_incio1_posgrado").value;
    var fecha_fin1 = document.getElementById("fecha_fin1_posgrado").value;

    if(profesion_posgrado == ''){
      alert('Ingrese Especialidad');
      e.preventDefault();
      return;
    }

    if(nivel_estudio_posgrado == '1'){
      alert('Seleccione Nivel de Estudio');
      e.preventDefault();
      return;
    }

    if(estado_postgrado == ''){
      alert('Seleccione Estado');
      e.preventDefault();
      return;
    }

    if(centro_estudio_posgrado == ''){
      alert('Ingrese Centro de Estudio');
      e.preventDefault();
      return;
    }

    if(fecha_incio1 > fecha_fin1){
      e.preventDefault();
      alert("La fecha de inicio no puede ser mayor a la fecha final");
      return;
    }
  }

  function variarpsotd(tipo){
    
    response = "<option value=''>Seleccione</option>";
    if(tipo.value == "26"){
     // alert(tipo.value);
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='39'>MAESTRO</option>";

    }
    if(tipo.value == "27"){
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='38'>DOCTOR</option>";
    }
    if(tipo.value == "28"){
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='13'>EGRESADO</option>";
    }
    if(tipo.value == "29"){
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='13'>EGRESADO</option>";
    }
    $('#estado_postgrado').html(response).fadeIn();
  }
</script>