<div class="card">
  <div class="card-body">
    <form action="{{ route('users.comptacion') }}" method="post" id="form_compu" onsubmit="validateComp(event)">
    	@csrf
			<input name="codigo_compu" id="codigo_compu" value="" type="hidden">
	  <div class="hidden-mobile form-row">
	    <div class="form-group col-md-6">
	      <label>Curso</label>
	    </div>
	    <div class="form-group col-md-6">
	      <label>Nivel</label>
	    </div>
	  </div>

	  <div class="form-row">
	  <div hidden-form class="form-group col-md-6">
	      <label>Curso</label>
	    </div>
	  		<div class="form-group col-md-6">
		    	<select class="selectpicker show-tick form-control form-control-sm" data-style="select-style" data-live-search="true" name="compu" id="compu" onchange="return otroCompu(this)">
	                <option value="">Seleccione</option>
									@foreach($compu_lista as $compu)
	                <option value="{{ $compu->MAESP_CODIGO }}">{{ $compu->MAESC_DESCRIPCION }}</option>
	                @endforeach
	            </select>
	        </div>
			<div hidden-form class="form-group col-md-6">
	      <label>Nivel</label>
	    </div>
			<div class="col-md-6">
	            <select class="form-control form-control-sm" name="nivel_compu" id="nivel_compu">
	                <option value="">Seleccione</option> 
	                <option value="1">Básico</option>
	                <option value="2">Intermedio</option>
	                <option value="3">Avanzado</option>
	            </select>
	    </div>
				<div class="col-md-12" style="display:none;" id="otro_computacion">
					<input id="otro_compu" onpaste="return false"  onkeypress="return soloLetrasHoja(event);"  type="text" class="form-control form-control-sm" name="otro_compu" value="{{ old('otro_compu') }}" placeholder="Ingrese otro curso" style="text-transform:uppercase;">
				</div>
		
	  </div>
		<br>
	  <button type="submit" class="btn btn-primary">Agregar Computacion</button>
	</form>	
  </div>
</div>

<?php 
	echo "<script>
		var computaciones = ".$computaciones."
	</script>"
?>

<script>
	function otroCompu(input){
		
		if(input.value == "30142"){
			document.getElementById('otro_computacion').style = "display:block";
		}else{
			document.getElementById('otro_computacion').style = "display:none";
		}
		
	}

	function validateComp(e) {

		var cursoDuplicado = false;

		var curso = document.getElementById('compu').value;
		var textoCurso = document.getElementById('compu').options[document.getElementById('compu').selectedIndex].text;
		var nivel = document.getElementById('nivel_compu').value;

		if(curso == ""){
			alert('Seleccione un curso');
			e.preventDefault();
			return;
		}

		if(nivel == ""){
			alert('Seleccione un nivel');
			e.preventDefault();
			return;
		}

		computaciones.forEach(function(compu){
			if(compu.MAESC_DESCRIPCION == textoCurso){
				cursoDuplicado = true;
			}
		});

		if(cursoDuplicado){
			alert('El curso ya esta agregado');
			e.preventDefault();
			return;
		}
	}
</script>