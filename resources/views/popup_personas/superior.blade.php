<div class="card">
  <div class="card-body">
    <form 
      action="{{ route('users.superior') }}" 
      method="post" 
      id="form_superior" 
      onsubmit="validateSu(event)">
    	@csrf
    	<div class="form-row">
      <input name="codigo_superior" id="codigo_superior" value="" type="hidden">
	    <div class="form-group col-md-12">
            <label>Carrera</label>
            <select class="selectpicker show-tick form-control form-control-sm" data-style="select-style" data-live-search="true" id="profesion" name="profesion" required>
                <option value="">Seleccione</option>
                @foreach($profesiones as $profesion)
                    <option value="{{ $profesion->profp_codigo }}">{{ $profesion->nombre }}</option>
                @endforeach
            </select>
               
         </div>
        
	  </div>
	  <div class="form-row">

         <div class="form-group col-md-6">
            <label>Nivel Alcanzado</label>
            <select class="form-control form-control-sm" name="nivel_estudio" id="nivel_estudio_superior" onchange="return variarEstadoSuperior(this)">
                <option value="1">Seleccione</option>
                @foreach($grado_intruccion_superior as $grado)
                <option value="{{ $grado->id }}">{{ $grado->grad_descripcion }}</option>
                @endforeach
            </select>
        </div>


	    <div class="form-group col-md-6">
	         <label>Estado</label>
            <select class="form-control form-control-sm" name="estado" id="estado_superior">
              <option value="">Seleccione</option>
            </select>
        </div>

	   
	  </div>


      <div class="form-row">
          <div class="form-group col-md-12">
            <label>Centro de Estudios</label>
         <input id="otro_centro_estudio3" onpaste="return false"  onkeypress="return soloLetrasHoja(event);"  type="text" class="form-control form-control-sm" name="otro_centro_estudio3" value="{{ old('otro_centro_estudio3') }}" placeholder="Ingrese Centro de Estudio" required>   
        </div>
      </div>

	  <div class="form-row">
	  		<div class="form-group col-md-6">
			    <label>Año Inicio <span style="color:#585858">(Ejemplo 2002)</span></label>
			    <input id="fecha_incio1_superior" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm date-pickerf" name="fecha_incio1" value="" placeholder="2002" required>
			  </div>
			  <div class="form-group col-md-6">
			    <label>Año Término <span style="color:#585858">(Ejemplo 2015)</span></label>
			    <input id="fecha_fin1_superior" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm date-pickerf" name="fecha_fin1" value="" placeholder="2015" required>
			  </div>
	  </div>
	  <button type="submit" class="btn btn-primary">Agregar Formación</button>
	</form>	
  </div>
</div>

<?php 
	echo "<script>
			var formacion_basica = ".$formacion_basica.";
		</script>";
?>

<script>
  function validateSu(e){

    var estudiosIncompletos = false;

    var nivel_alcanzado = document.getElementById('nivel_estudio_superior').value;
    var centro_estudio = document.getElementById('otro_centro_estudio3').value;
    var estado = document.getElementById('estado_superior').value;
    var fecha_incio1 = document.getElementById("fecha_incio1_superior").value;
    var fecha_fin1 = document.getElementById("fecha_fin1_superior").value;

    if(nivel_alcanzado == 1){
      alert('Seleccione Nivel de Estudio');
      e.preventDefault();
      return;
    }

    if(estado == 1 || estado == '' ) {
      alert('Seleccione Estado');
      e.preventDefault();
      return;
    }

    if(centro_estudio == ''){
      alert('Ingrese Centro de Estudio');
      e.preventDefault();
      return;
    }

    if(fecha_incio1 > fecha_fin1){
      e.preventDefault();
      alert("La fecha de inicio no puede ser mayor a la fecha final");
      return;
    }

    if(formacion_basica.length > 0){
      formacion_basica.forEach(function(element){
        if(element.grad_descripcion == 'PRIMARIA INCOMPLETA' || element.grad_descripcion == 'SECUNDARIA  INCOMPLETA' ){
          estudiosIncompletos = true;
        }
      });
    }

    if(estudiosIncompletos) {
      e.preventDefault();
      alert('Usted no cuenta con estudios completos, por favor complete estudios primarios y secundarios');
      return;
    }

  }
  function variarEstadoSuperior(tipo){
    
    response = "<option value='1'>Seleccione</option>";
    if(tipo.value == "7"){
      
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='16'>COLEGIADO TITULADO</option>";
      response += "<option value='14'>BACHILLER</option>";
      response += "<option value='34'>CICLOS FINALES</option>";
      response += "<option value='35'>CICLOS INTERMEDIOS</option>";
      response += "<option value='36'>CICLOS INICIALES</option>";
      
    }
    if(tipo.value == "6"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='34'>CICLOS FINALES</option>";
      response += "<option value='35'>CICLOS INTERMEDIOS</option>";
      response += "<option value='36'>CICLOS INICIALES</option>";
    }
    $('#estado_superior').html(response).fadeIn();
  }
</script>