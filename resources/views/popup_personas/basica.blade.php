
<div class="card">
  <div class="card-body">
    <form 
		action="{{ route('users.basica') }}"
		method="post" id="form_basico" 
		onsubmit="validateBas(event)">
    	@csrf
			<input value="0" type="hidden" name="codigo_basico" id="codigo_basico">
	  <div class="form-row">

	    <div class="form-group col-md-6">
	      <label>Centro de Estudios</label>
	      <input id="centro_estudio" onpaste="return false"  onkeypress="return soloLetrasHoja(event);"  type="text" class="form-control form-control-sm" name="centro_estudio" value="{{ old('centro_estudio') }}" placeholder="Ingrese Centro de Estudio" required  
		  >
	    </div>

	    <div class="form-group col-md-6">
	    	<label>Nivel Alcanzado</label>
            <select class="form-control form-control-sm" name="nivel_estudio" id="nivel_estudio" onchange="return nivelEstudio(this)" required > 
						<option value="1">Seleccione</option>
								@foreach($grado_intruccion_basico as $grado)
                <option value="{{ $grado->id }}">{{ $grado->grad_descripcion }}</option>
                @endforeach
            </select>
        </div>


	  </div>

	  <div class="form-row">
	  		<div class="form-group col-md-6">
			    <label>Año Inicio <span style="color:#585858">(Ejemplo 2002)</span></label>
			    <input id="fecha_incio1" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm " name="fecha_incio1" value="" placeholder="2002"  required>
			  </div>
			  <div class="form-group col-md-6">
			    <label>Año Término <span style="color:#585858">(Ejemplo 2015)</span></label>
			    <input id="fecha_fin1" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm " name="fecha_fin1" value="" placeholder="2015"  	required>
			  </div>
	  </div>
		<button  type="submit" class="btn btn-primary">Agregar Formación</button>
	</form>	

  </div>
</div>
<?php 
	echo "<script>
			var formacion_basica = ".$formacion_basica.";
			
		</script>";
?>
<script>

function validateBas(e){

	var primariaIncompleta = false;

	var centro_estudio = document.getElementById('centro_estudio').value;
	var fecha_incio1 = document.getElementById("fecha_incio1").value;
	var fecha_fin1 = document.getElementById("fecha_fin1").value;
	var nivel_alcanzado = document.getElementById("nivel_estudio").value;
	if(fecha_incio1 > fecha_fin1){
		e.preventDefault();
		alert("La fecha de inicio no puede ser mayor a la fecha final");
		return;
	}

	if(centro_estudio == ""){
		e.preventDefault();
		alert("El campo centro de estudio no puede estar vacio");
		return;
	}

	if(nivel_alcanzado == 1){
		e.preventDefault();
		alert("Debe seleccionar un nivel de estudio");
		return;
	}

	if(formacion_basica.length > 0){
		formacion_basica.forEach(function(element) {
			if(element.grad_descripcion == 'PRIMARIA INCOMPLETA'){
				primariaIncompleta = true;
			}
		});
	}

	if((nivel_alcanzado == 4 || nivel_alcanzado == 5) && primariaIncompleta == true){
		e.preventDefault();
		alert("No puede agregar una formación de nivel secundario sin haber completado la formación primaria");
		return;
	}
}
	
/*
function validaForm(){
	if(document.getElemenetById("centro_estudio") == ""){
		alert("Centro de estudio es requirido");
	}

	if(document.getElemenetById("nivel_estudio") == "1"){
		alert("nivel de estudio es requirido")
	}

	if(document.getElemenetById("fecha_incio1") == ""){
		alert("Fecha de Inicio es requirido");
	}

	if(document.getElemenetById("fecha_fin1") == ""){
		
	}
}
*/
</script>
