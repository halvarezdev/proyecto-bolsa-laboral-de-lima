<div class="card">
  <div class="card-body">
    <form action="{{ route('users.estudios') }}" method="post" id="form_adicional" onsubmit="validateAd(event)">
    	@csrf
			<input name="codigo_adicional" id="codigo_adicional" value="" type="hidden">
	  <div class="form-row">
	    <div class="form-group col-md-6">
	      <label>Tipo</label>
	      <select name="estudio_tipo" id="estudio_tipo_estudios" class="form-control form-control-sm">
            <option value="">SELECCIONE</option>
            <option value="1">CURSO</option>
            <option value="2">SEMINARIO</option>
            <option value="3">TALLER</option>
            <option value="4">CONFERENCIA</option>
            <option value="5">CONGRESO</option>
            <option value="6">CHARLA</option>           
         </select>
	    </div>


	    <div class="form-group col-md-6">
	      <label>Tema</label>
	       <input id="nombre_curso_estuios" onpaste="return false"  onkeypress="return soloLetrasHoja(event);"  type="text" class="form-control form-control-sm" name="nombre_curso" value="{{ old('nombre_curso') }}" placeholder="Ingrese Nombre del curso" required>
	    </div>
	  </div>

	  <div class="form-row">
	  		<div class="form-group col-md-8">
	  			<label>Centro de Estudio</label>
		    	 <input  onpaste="return false"  onkeypress="return soloLetrasHoja(event);" id="centro_curso_estuios" type="text" class="form-control form-control-sm" name="centro_curso" value="{{ old('centro_curso') }}" placeholder="Ingrese Centro de Estudio" onkeypress="return soloLetras(event)" required>
	        </div>

					<div class="form-group col-md-4">
	  			<label>Año Término <span style="color:#585858">(Ejemplo 2015)</span></label>
		    	<input id="fecha_fin_estudioadi" type="number" min="1900" max="{{date('Y')}" step="1" class="form-control form-control-sm" name="fecha_fin_estudioadi" value="" placeholder="2015" required>
	        </div>
	  </div>
	 
	  <button type="submit" class="btn btn-primary">Agregar Estudios</button>
	</form>	
  </div>
</div>

<script>
	function validateAd(e) {
		var estudio_tipo = document.getElementById("estudio_tipo_estudios").value;
		var nombre_curso = document.getElementById("nombre_curso_estuios").value;
		var centro_curso = document.getElementById("centro_curso_estuios").value;
		var fecha_fin_estudioadi = document.getElementById("fecha_fin_estudioadi").value;

		if (estudio_tipo == "") {
			alert("Seleccione el tipo de estudio");
			e.preventDefault();
			return;
		}

		if (nombre_curso == "") {
			alert("Ingrese el nombre del curso");
			e.preventDefault();
			return;
		}

		if (centro_curso == "") {
			alert("Ingrese el centro de estudio");
			e.preventDefault();
			return;
		}

		if (fecha_fin_estudioadi == "") {
			alert("Ingrese el año de termino");
			e.preventDefault();
			return;
		}

	}

	function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
	}
</script>