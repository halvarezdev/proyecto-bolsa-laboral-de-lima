@extends('layouts.app-entidad')

@section('content')

<div class="container" style="background: #fff;">
<nav class="navbar navbar-light bg-light">
  <span class="navbar-brand mb-0 h1"><h3>Listado de Ferias</h3></span>
</nav>
<br>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('eventos.listado') }}" method="GET">
            <div class="form-group row">
                <label for="fec_inicio" class="col-sm-1 col-form-label">Desde</label>
                <div class="col-sm-3">
                <input type="date" class="form-control" id="fec_inicio" name="fec_inicio" value="{{ $fec_inicio }}">
                </div>
                <label for="fec_fin" class="col-sm-1 col-form-label" >Hasta</label>
                <div class="col-sm-3">
                <input type="date" class="form-control" id="fec_fin" name="fec_fin" value="{{ $fec_fin }}">
                </div>
                <div class="col-sm-3">
                <button type="submit" class="btn btn-success">Buscar</button>
                </div>
            </div>
            <div class="form-group row">
                <label for="nombre" class="col-sm-1 col-form-label" >Nombre</label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $nombre }}">
                </div>
                <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" title="Agregar Feria" onClick="limpiar()">
                    Nuevo <i class="fas fa-plus"></i>
                </button>
                </div>
            </div>
            </form>
        <br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover">
                    <thead class="thead-dark ">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th colspan="2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($lista_eventos as $i => $value)
                            <tr>
                                <td>
                                {{ $i+1 }}
                                <input type="hidden" id="nombre_h{{$i}}" value="{{ $value->nombre }}">
                                <input type="hidden" id="descripcion_h{{$i}}" value="{{ $value->descripcion }}">
                                <input type="hidden" id="fecha_inicio_h{{$i}}" value="{{ $value->fecha_inicio }}">
                                <input type="hidden" id="fecha_fin_h{{$i}}" value="{{ $value->fecha_fin }}">
                                <input type="hidden" id="id_evento_h{{$i}}" value="{{ $value->id_evento }}">
                                <input type="hidden" id="ruta_imagen{{$i}}" value="{{ $value->ruta_imagen }}">
                                <input type="hidden" id="ruta_imagen_2{{$i}}" value="{{ $value->ruta_imagen_2 }}">
                                <input type="hidden" id="ruta_imagen_3{{$i}}" value="{{ $value->ruta_imagen_3 }}">
                                <input type="hidden" id="estado1{{$i}}" value="{{ $value->estado1 }}">
                                <input type="hidden" id="estado2{{$i}}" value="{{ $value->estado2 }}">
                                <input type="hidden" id="estado3{{$i}}" value="{{ $value->estado3 }}">
                                <input type="hidden" id="tipo1{{$i}}" value="{{ $value->estado3 }}">
                                <input type="hidden" id="tipo2{{$i}}" value="{{ $value->estado3 }}">
                                <input type="hidden" id="tipo3{{$i}}" value="{{ $value->estado3 }}">
                                <input type="hidden" id="ruta_1{{$i}}" value="{{ $value->ruta_1 }}">
                                <input type="hidden" id="ruta_2{{$i}}" value="{{ $value->ruta_2 }}">
                                <input type="hidden" id="ruta_3{{$i}}" value="{{ $value->ruta_3 }}">
                                </td>
                                <td>{{ $value->nombre }}</td>
                                <td>{{ $value->descripcion }}</td>
                                <td>{{ date('d/m/Y', strtotime($value->fecha_inicio)) }}</td>
                                <td>{{ date('d/m/Y', strtotime($value->fecha_fin)) }}</td>
                                <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                                 title="Editar Feria" onclick="editaEvento('{{ $i }}')">
                                <i class="fas fa-pencil-alt"></i>
                                </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-sm-6"> 
                {{ $lista_eventos->links() }}
            </div>
        </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eventos Laborales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @include('entidad.registro-evento')
      </div>
     <!--
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
         -->
    </div>
  </div>
</div>


@endsection
