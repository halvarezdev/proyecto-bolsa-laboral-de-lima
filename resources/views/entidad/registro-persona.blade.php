@extends('layouts.app-entidad')
@section('content')
<style type="text/css"> 
    .fondo_body{

        background-image:url({{ asset('fondo/fondo-persona.png') }});
    }
</style>
<div class="container " style="margin-top: 40px;">
@if(isset($tipo) && $tipo == '2')
<style>
  /*  body{
    background-image: url("{{ asset('fondo/cenfotur-fondo.png') }}" );
    background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
        
  }*/
</style>
<form method="POST" action="{{ route('register.submit') }}" id="formulario_persona">
@else
<form method="POST" action="{{ route('register') }}" id="formulario_persona">
@endif
   
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
    <div class="row justify-content-md-center">
        <div class="col-sm-7">
        <div class="card border-primary mb-3">
    <div class="card-header"><b>REGISTRO DE USUARIO</b></div>
    <div class="card-body text-primary">
    <input type="hidden" id="base_url" value="{{ asset('') }}">
    <input type="hidden" id="entidad" name="entidad" value="{{ $entidad }}">
    
    <!-- TIPO DOCUMENTO -->
    <div class="form-row">
        <div class="form-group col-md-6">
          <label>Tipo Documento <span>(*)</span></label>
              <select class="form-control form-control-sm {{ $errors->has('tipo_documento') ? ' is-invalid' : '' }} 
                  "  data-style="select-style" name="tipo_documento" value="{{ old('tipo_documento') }}" id="tipo_documento" required autofocus> 
                    @foreach($documentos as $documento)
                    @if($documento->id != "6")
                        <option value="{{ ($documento->id != '1' ? $documento->id : '') }}">{{ $documento->tipdoc_descripion }}</option>
                    @endif
                    @endforeach
                </select>
                <span class="tipo_documento" role="alert" style="font-size:12px; color:red;"></span>
                @if ($errors->has('tipo_documento'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('tipo_documento') }}</strong>
                    </span>
                @endif
        </div>
        <div class="form-group col-md-6">
          <label>Número de Documento <span>(*)</span></label>
            <input id="numero_documento" type="text" class="form-control form-control-sm {{ $errors->has('numero_documento') ? ' is-invalid' : '' }}" name="numero_documento" value="{{ old('numero_documento') }}" required autofocus placeholder="Ingrese Numero de Documento">
            <span class="numero_documento" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('numero_documento'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('numero_documento') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group col-md-6">
            <label>Apellido Paterno <span>(*)</span></label>
            <input id="ape_pat" type="text" class="form-control form-control-sm {{ $errors->has('ape_pat') ? ' is-invalid' : '' }}" name="ape_pat" value="{{ old('ape_pat') }}"  required autofocus placeholder="Ingrese Apellido Paterno" style="text-transform:uppercase;">
            <span class="ape_pat" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('ape_pat'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('ape_pat') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group col-md-6">
          <label>Apellido Materno <span>(*)</span></label>
            <input id="ape_mat" type="text" class="form-control form-control-sm {{ $errors->has('ape_mat') ? ' is-invalid' : '' }}" name="ape_mat" value="{{ old('ape_mat') }}" required autofocus placeholder="Ingrese Apellido Materno" style="text-transform:uppercase;">
            <span class="ape_mat" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('ape_mat'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('ape_mat') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group col-md-12">
          <label>Nombre <span>(*)</span></label>
            <input id="name" type="text" class="form-control form-control-sm {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Ingrese Nombre" style="text-transform:uppercase;">
            <span class="name" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group col-md-12">
          <label>Correo Electrónico <span>(*)</span></label>
            <input id="email" type="email" class="form-control form-control-sm {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Ingrese Correo Electronico" style="text-transform:uppercase;">
            <span class="email" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>


        <div class="form-group col-md-6">
          <label>Contraseña <span>Minimo 6 Caracteres (*)</span></label>
            <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" name="password" required placeholder="Contraseña">
            <span class="password" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        
        <div class="form-group col-md-6">
          <label>Confirmar Contraseña <span>(*)</span></label>
            <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" required placeholder="Contraseña">
            <span class="password-confirm" role="alert" style="font-size:12px; color:red;"></span>
        </div>

        <div class="form-group col-md-6">
          <label>¿Qué tipo de trabajo buscas? <span>(*)</span></label>
            <select name="ocu_trabajo" id="ocu_trabajo" class="selectpicker show-tick form-control form-control-sm" data-width="100%" data-style="select-style" data-live-search="true" required>
                <option value="">Seleccione</option>
            @foreach($ocupaciones as $value)
                <option value="{{ $value->ocup_codigo }}">{{ $value->nombre }}</option>
            @endforeach
            </select>
            <span class="ocu_trabajo" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('ocu_trabajo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('ocu_trabajo') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group col-md-6">
          <label>¿Dónde lo buscas? <span>(*)</span></label>
          <select name="ubi_lugar" id="ubi_lugar" class="form-control form-control-sm selectpicker show-menu-arrow"  data-style="select-style" required>
            <option value="">Seleccione</option>
          @foreach($ubigeo as $ubi)
            <option value="{{ $ubi->ubi_codigo }}">{{ $ubi->ubi_descripcion }}</option>
          @endforeach
          </select>
          
          <span class="ubi_lugar" role="alert" style="font-size:12px; color:red;"></span>
            @if ($errors->has('ubi_lugar'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('ubi_lugar') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-row justify-content-end">
        <button type="submit" class="btn btn-primary">REGISTRAR</button>
    </div>
   

</div>

            
        </div>
    </div>
    
    </form>
</div>
@endsection
