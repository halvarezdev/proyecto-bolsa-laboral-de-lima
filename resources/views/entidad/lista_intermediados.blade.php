@extends('layouts.app-entidad')

@section('content')

<div class="container " >
<br>
<div class="row" >
        <div class="col"> 
            <h3>Listado de Intermediados Registradas</h3>
        </div>
    </div>
    <br>
    <div class="row" >
        @foreach($pedido as $i => $value)
            <div class="col-2"> 
                Descripcion:
                <br>
            </div>
            <div class="col">
                {{ $value->descripcion }}
            </div>    
            <div class="col-2">
            <a href="{{ route('pedido.buscar', ['pedido' => $pedido_codigo ,'empresa' => $empresa_codigo]) }}" class="btn btn-info">Buscar Postulante</a>
            
            </div>  
        @endforeach
    </div>
    <br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                <thead class="thead-dark ">
                        <tr>
                            <th>#</th>
                            <th>Documento</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Email</th>
                            <th>Tipo</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($lista_postulante as $i => $value)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $value->numero_documento }}</td>
                                <td>{{ $value->nombre }}</td>
                                <td>{{ $value->ape_pat." ".$value->ape_mat }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ ($value->tipo_entidad == '2' ? 'BD' : 'WEB') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-sm-6"> 
                {{ $lista_postulante->links() }}
            </div>
        </div>
   
       
</div>

@endsection
