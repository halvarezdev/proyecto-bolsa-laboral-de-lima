@extends('layouts.app-entidad')

@section('content')

<div class="container " >
<div class="card">
      <div class="card-body">

    <form action="{{ route('reporte.entidad.euno') }}" method="get">
    
    <div class="row">
        <div class="col">
            <select class="form-control" id="m" name="m">
              <option value="">Mes</option>
              <option value="01" {{ isset($Month) && $Month == '01' ? 'selected' : '' }}>Enero</option>
              <option value="02" {{ isset($Month) && $Month == '02' ? 'selected' : '' }}>Febrero</option>
              <option value="03" {{ isset($Month) && $Month == '03' ? 'selected' : '' }}>Marzo</option>
              <option value="04" {{ isset($Month) && $Month == '04' ? 'selected' : '' }}>Abril</option>
              <option value="05" {{ isset($Month) && $Month == '05' ? 'selected' : '' }}>Mayo</option>
              <option value="06" {{ isset($Month) && $Month == '06' ? 'selected' : '' }}>Junio</option>
              <option value="07" {{ isset($Month) && $Month == '07' ? 'selected' : '' }}>Julio</option>
              <option value="08" {{ isset($Month) && $Month == '08' ? 'selected' : '' }}>Agosto</option>
              <option value="09" {{ isset($Month) && $Month == '09' ? 'selected' : '' }}>Setiembre</option>
              <option value="10" {{ isset($Month) && $Month == '10' ? 'selected' : '' }}>Octubre</option>
              <option value="11" {{ isset($Month) && $Month == '11' ? 'selected' : '' }}>Noviembre</option>
              <option value="12" {{ isset($Month) && $Month == '12' ? 'selected' : '' }}>Diciembre</option>
            </select>
        </div>  
        <div class="col">
            <select class="form-control" id="a" name="a">
              <option value="">Año</option>
              <option value="2019" {{ isset($anio) && $anio == '2019' ? 'selected' : '' }}>2019</option>
              <option value="2020" {{ isset($anio) && $anio == '2020' ? 'selected' : '' }}>2020</option>
              <option value="2021" {{ isset($anio) && $anio == '2021' ? 'selected' : '' }}>2021</option>
              <option value="2021" {{ isset($anio) && $anio == '2022' ? 'selected' : '' }}>2022</option>
              <option value="2021" {{ isset($anio) && $anio == '2023' ? 'selected' : '' }}>2023</option>
            </select>
        </div>  
        <div class="col-2">
        <button type="submit" class="btn btn-success">Aplicar Filtro</button>
        </div>
    </div>
    
    </form>

      </div>
</div>      
<br>
<br>
    <div class="row" >
        <div class="col"> 
            <h3>Estadisticas Generales</h3>
        </div>
    </div>
    <br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                   <thead>
                       <tr style="background: #c3bdbd;">
                        <th colspan="2" ></th>
                        <th>DIRECCIÓN</th>
                        <th>PROFESIONAL</th>
                        <th>TÉCNICO</th>
                        <th>OPERATIVO</th>
                        <th>NO CALIFICADO</th>
                        <th>TOTAL</th>
                       </tr>
                   </thead>
                   <tbody>
                        <tr>
                           <th style="background: #c3bdbd;">OFERTA</th>
                           <th style="background: #c3bdbd;">VARONES</th>
                           <th style="text-align: center;">{{$dir_v}}</th>
                           <th style="text-align: center;">{{$pro_v}}</th>
                           <th style="text-align: center;">{{$tec_v}}</th>
                           <th style="text-align: center;">{{$ope_v}}</th>
                           <th style="text-align: center;">{{$no_v}}</th>
                           <th style="text-align: center;">{{ $dir_v + $pro_v + $tec_v + $ope_v + $no_v }}</th>
                        </tr>
                        <tr>
                           <th style="background: #c3bdbd;"></th>
                           <th style="background: #c3bdbd;">MUJERES</th>
                           <th style="text-align: center;">{{$dir_m}}</th>
                           <th style="text-align: center;">{{$pro_m}}</th>
                           <th style="text-align: center;">{{$tec_m}}</th>
                           <th style="text-align: center;">{{$ope_m}}</th>
                           <th style="text-align: center;">{{$no_m}}</th>
                           <th style="text-align: center;">{{ $dir_m + $pro_m + $tec_m + $ope_m + $no_m}}</th>
                        </tr>
                        <tr>
                            <th style="background: #c3bdbd;"></th>
                            <th style="background: #c3bdbd;">TOTAL</th>
                            <th style="text-align: center;">{{$dir_v+$dir_m}}</th>
                            <th style="text-align: center;">{{$pro_v+$pro_m}}</th>
                            <th style="text-align: center;">{{$tec_v+$tec_m}}</th>
                            <th style="text-align: center;">{{$ope_v+$ope_m}}</th>
                            <th style="text-align: center;">{{$no_v+$no_m}}</th>
                            <th style="text-align: center;">{{ $dir_v + $pro_v + $tec_v + $ope_v + $no_v +$dir_m + $pro_m + $tec_m + $ope_m + $no_m }}</th>
                        </tr>
                       <tr>
                            <th style="background: #c3bdbd;">DEMANDA</th>
                            <th style="background: #c3bdbd;">TOTAL</th>
                            <th style="text-align: center;">{{$dir}}</th>
                            <th style="text-align: center;">{{$pro}}</th>
                            <th style="text-align: center;">{{$tec}}</th>
                            <th style="text-align: center;">{{$ope}}</th>
                            <th style="text-align: center;">{{$no}}</th>
                            <th style="text-align: center;">{{$dir+$pro+$tec+$ope+$no}}</th>
                       </tr>
                       <tr>
                            <th style="background: #c3bdbd;">COLOCACIONES</th>
                            <th style="background: #c3bdbd;">VARONES</th>
                            <th style="text-align: center;">{{$co_dir_va}}</th>
                            <th style="text-align: center;">{{$co_pro_va}}</th>
                            <th style="text-align: center;">{{$co_tec_va}}</th>
                            <th style="text-align: center;">{{$co_ope_va}}</th>
                            <th style="text-align: center;">{{$co_no_va}}</th>
                            <th style="text-align: center;">{{$co_dir_va+$co_pro_va+$co_tec_va+$co_ope_va+$co_no_va}}</th>
                            
                       </tr>
                       <tr>
                           <th style="background: #c3bdbd;"></th>
                           <th style="background: #c3bdbd;">MUJERES</th>
                           <th style="text-align: center;">{{$co_dir_mu}}</th>
                           <th style="text-align: center;">{{$co_pro_mu}}</th>
                           <th style="text-align: center;">{{$co_tec_mu}}</th>
                           <th style="text-align: center;">{{$co_ope_mu}}</th>
                           <th style="text-align: center;">{{$co_no_mu}}</th>
                           <th style="text-align: center;">{{$co_dir_mu+$co_pro_mu+$co_tec_mu+$co_ope_mu+$co_no_mu}}</th>
                        </tr>
                        <tr>
                            <th style="background: #c3bdbd;"></th>
                            <th style="background: #c3bdbd;">TOTAL</th>
                            <th style="text-align: center;">{{$co_dir_va+$co_dir_mu}}</th>
                            <th style="text-align: center;">{{$co_pro_va+$co_pro_mu}}</th>
                            <th style="text-align: center;">{{$co_tec_va+$co_tec_mu}}</th>
                            <th style="text-align: center;">{{$co_ope_va+$co_ope_mu}}</th>
                            <th style="text-align: center;">{{$co_no_va+$co_no_mu}}</th>
                            <th style="text-align: center;">{{$co_dir_va+$co_pro_va+$co_tec_va+$co_ope_va+$co_no_va+$co_dir_mu+$co_pro_mu+$co_tec_mu+$co_ope_mu+$co_no_mu}}</th>
                        </tr>
                   </tbody>
                </table>
            </div>
        </div>
   
        <div class="row" >
            <div class="col"> 
                <h3>ESTADISTICAS SOBRE DEMANDA</h3>
            </div>
        </div>
        <br>

        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                   <thead>
                       <tr >
                        <th colspan="3" style="background: #c3bdbd;">EMPRESAS INSCRITAS DURANTE EL PERIODO</th>
                        <th style="text-align: center;">{{$totales_usarios}}</th>
                       </tr>
                       <tr >
                        <th colspan="3" style="background: #c3bdbd;">EMPRESAS USUARIAS QUE HAN REALIZADO PEDIDOS EN EL PERIODO</th>
                        <th style="text-align: center;">{{$pedidos}}</th>
                       </tr>
                   </thead>
                   <tbody>
                    
                   </tbody>
                </table>
            </div>
        </div>


        <div class="row" >
            <div class="col"> 
                <h3>DIST. DE VARIABLES DE MERCADOS DE TRABAJO SEG&Uacute;N NIVELES DE CALIFICACION</h3>
            </div>
        </div>
        <br>

        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                   <thead>
                       <tr >
                        <th style="text-align: center;background: #c3bdbd;"></th>
                        <th style="text-align: center;background: #c3bdbd;" colspan="2">OFERTA</th>
                        <th style="text-align: center;background: #c3bdbd;" colspan="2">DEMANDA</th>
                        <th style="text-align: center;background: #c3bdbd;" colspan="2">COLOCACIONES</th>
                       </tr>
                       <tr>
                           <th style="text-align: center;background: #c3bdbd;"></th>
                           <th style="text-align: center;background: #c3bdbd;">TOTAL</th>
                           <th style="text-align: center;background: #c3bdbd;">%</th>
                           <th style="text-align: center;background: #c3bdbd;">TOTAL</th>
                           <th style="text-align: center;background: #c3bdbd;">%</th>
                           <th style="text-align: center;background: #c3bdbd;">TOTAL</th>
                           <th style="text-align: center;background: #c3bdbd;">%</th>
                       </tr>
                       <?php $total_de = $dir_v+$dir_v+$pro_v+$pro_v+$tec_v+$tec_v+$ope_v+$ope_v+$no_v+$no_v; ?>
                       <?php $total_of = $dir+$pro+$tec+$ope+$no; ?>
                       <?php $total_co = $co_dir_va+$co_pro_va+$co_tec_va+$co_ope_va+$co_no_va+$co_dir_mu+$co_pro_mu+$co_tec_mu+$co_ope_mu+$co_no_mu; ?>
                       <tr >
                        <th style="background: #c3bdbd;">DIRECCION</th>
                        <th style="text-align: center;">{{$dir_v+$dir_v}}</th>
                        <th style="text-align: center;">{{  number_format($total_de == 0 ? '0' : (($dir_v+$dir_v) / $total_de) * 100, 2)  }}%</th>
                        <th style="text-align: center;">{{$dir}}</th>
                        <th style="text-align: center;">{{  number_format($total_of == 0 ? '0' : (($dir) / $total_of) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$co_dir_va+$co_dir_mu}}</th>
                        <th style="text-align: center;">{{  number_format($total_co == 0 ? '0' : (($co_dir_va+$co_dir_mu) / $total_co) * 100,2)  }}%</th>
                       </tr>
                       <tr >
                        <th style="background: #c3bdbd;">PROFESIONAL</th>
                        <th style="text-align: center;">{{$pro_v+$pro_v}}</th>
                        <th style="text-align: center;">{{ number_format($total_de == 0 ? '0' : (($pro_v+$pro_v) / $total_de) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$pro}}</th>
                        <th style="text-align: center;">{{ number_format($total_of == 0 ? '0' : (($pro) / $total_of) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$co_pro_va+$co_pro_mu}}</th>
                        <th style="text-align: center;">{{ number_format($total_co == 0 ? '0' : (($co_pro_va+$co_pro_mu) / $total_co) * 100,2)  }}%</th>
                       </tr>
                       <tr >
                        <th style="background: #c3bdbd;">T&Eacute;CNICO</th>
                        <th style="text-align: center;">{{$tec_v+$tec_v}}</th>
                        <th style="text-align: center;">{{ number_format($total_de == 0 ? '0' : (($tec_v+$tec_v) / $total_de) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$tec}}</th>
                        <th style="text-align: center;">{{ number_format($total_of == 0 ? '0' : (($tec) / $total_of) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$co_tec_va+$co_tec_mu}}</th>
                        <th style="text-align: center;">{{ number_format($total_co == 0 ? '0' : (($co_tec_va+$co_tec_mu) / $total_co) * 100,2)  }}%</th>
                       </tr>
                       <tr >
                        <th style="background: #c3bdbd;">OPERATIVO</th>
                        <th style="text-align: center;">{{$ope_v+$ope_v}}</th>
                        <th style="text-align: center;">{{ number_format($total_de == 0 ? '0' : (($ope_v+$ope_v) / $total_de) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$ope}}</th>
                        <th style="text-align: center;">{{ number_format($total_of == 0 ? '0' : (($ope) / $total_of) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$co_ope_va+$co_ope_mu}}</th>
                        <th style="text-align: center;">{{ number_format($total_co == 0 ? '0' : (($co_ope_va+$co_ope_mu) / $total_co) * 100,2)  }}%</th>
                       </tr>
                       <tr >
                        <th style="background: #c3bdbd;">NO CALIFICADO</th>
                        <th style="text-align: center;">{{$no_v+$no_v}}</th>
                        <th style="text-align: center;">{{ number_format($total_de == 0 ? '0' : (($no_v+$no_v) / $total_de) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$no}}</th>
                        <th style="text-align: center;">{{ number_format($total_of == 0 ? '0' : (($no) / $total_of) * 100,2)  }}%</th>
                        <th style="text-align: center;">{{$co_no_va+$co_no_mu}}</th>
                        <th style="text-align: center;">{{ number_format($total_co == 0 ? '0' : (($co_no_va+$co_no_mu) / $total_co) * 100,2)  }}%</th>
                       </tr>
                       <tr >
                        <th style="background: #c3bdbd;">TOTAL</th>
                        <th style="text-align: center;">{{$dir_v+$dir_v+$pro_v+$pro_v+$tec_v+$tec_v+$ope_v+$ope_v+$no_v+$no_v}}</th>
                        <th style="text-align: center;"></th>
                        <th style="text-align: center;">{{$total_of}}</th>
                        <th style="text-align: center;"></th>
                        <th style="text-align: center;">{{$total_co}}</th>
                        <th style="text-align: center;"></th>
                       </tr>
                   </thead>
                   <tbody>
                    
                   </tbody>
                </table>
            </div>
        </div>


</div>

@endsection
