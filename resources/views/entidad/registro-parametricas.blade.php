@extends('layouts.app-entidad')

@section('content')

<div class="container">

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <p class="font-italic" style="font-size:20px;margin-bottom: 0rem;">Registro Parametrica / Persona.</p>
                </div>
            </div>
        </div>
    </div>

<br>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ route('guarda.ocupacion.parametrica') }}">
                    <p class="font-italic" style="font-size:18px;margin-bottom: 0rem;">Parametrica ocupacion persona.</p>
                    <br>
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />  
                        <div class="form-group">
                            <label for="formGroupExampleInput">Agregar Ocupacion</label>
                            <select class="selectpicker show-tick form-control form-control-sm" data-style="select-style" data-live-search="true" id="profesion" name="profesion" required>
                                <option value="">Seleccione</option>
                                @foreach($profesiones as $profesion)
                                    <option value="{{ $profesion->profp_codigo }}">{{ $profesion->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                      
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm">
                        <thead class="thead-light">
                            <tr >
                                <td>#</td>
                                <td>Nombre</td>
                                <td>#</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($lista_profesion as $i => $profesion)
                            <tr>
                                <td>{{ $i +1 }}</td>
                                <td>{{ $profesion->nombre }}</td>
                                <td><a href="#"><i class="fas fa-times"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                

                </div>
            </div>        
        <div>
    </div>

</div>


@endsection