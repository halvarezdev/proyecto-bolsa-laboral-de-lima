@extends('layouts.app-entidad')

@section('content')
<style>
.td_list{
      font-size:13px;
}
.page-wrapper.toggled .page-content {
    padding-left: 250px;        
}
.page-wrapper .page-content > div {

padding: 5px 20px;
}
</style>

</style>
<div class="container " >
  
    <div class="row">
      <div class="col"> 
        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">{{ $total_usuarios }}</h5>
            <p class="card-text"><b>Registrados</b> {{ $Month_letra }}.</p>
          </div>
        </div>
      </div>
      
      <div class="col">
        <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">{{ $intermediado }}</h5>
            <p class="card-text"><b>Intermediados</b> {{ $Month_letra }}.</p>
          </div>
        </div>  
      </div>

      <div class="col">
        <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">{{ $colocados }}</h5>
            <p class="card-text"><b>Colocados</b> {{ $Month_letra }}.</p>
          </div>
        </div>
      </div>
      
     <div class="col">
      <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">{{ $totales_usarios }}</h5>
            <p class="card-text"><b>Total de usuarios</b> {{ $anio }}.</p>
          </div>
        </div>
     </div>   

    </div>
    

    <div class="card">
      <div class="card-body">

    <form action="{{ route('reporte.presona.grafico') }}" method="get">
    
    <div class="row">
        <div class="col">
            <select class="form-control" id="m" name="m">
              <option value="">Mes</option>
              <option value="01" {{ isset($Month) && $Month == '01' ? 'selected' : '' }}>Enero</option>
              <option value="02" {{ isset($Month) && $Month == '02' ? 'selected' : '' }}>Febrero</option>
              <option value="03" {{ isset($Month) && $Month == '03' ? 'selected' : '' }}>Marzo</option>
              <option value="04" {{ isset($Month) && $Month == '04' ? 'selected' : '' }}>Abril</option>
              <option value="05" {{ isset($Month) && $Month == '05' ? 'selected' : '' }}>Mayo</option>
              <option value="06" {{ isset($Month) && $Month == '06' ? 'selected' : '' }}>Junio</option>
              <option value="07" {{ isset($Month) && $Month == '07' ? 'selected' : '' }}>Julio</option>
              <option value="08" {{ isset($Month) && $Month == '08' ? 'selected' : '' }}>Agosto</option>
              <option value="09" {{ isset($Month) && $Month == '09' ? 'selected' : '' }}>Setiembre</option>
              <option value="10" {{ isset($Month) && $Month == '10' ? 'selected' : '' }}>Octubre</option>
              <option value="11" {{ isset($Month) && $Month == '11' ? 'selected' : '' }}>Noviembre</option>
              <option value="12" {{ isset($Month) && $Month == '12' ? 'selected' : '' }}>Diciembre</option>
            </select>
        </div>  
        <div class="col">
            <select class="form-control" id="a" name="a">
              <option value="">Año</option>
              @for($i=date('Y')-2 ; $i<=date('Y')+1;$i++)
              <option value="{{$i}}"  {{ isset($anio) && $anio == $i ? 'selected' : '' }}>{{$i}}</option>
              @endfor
                </select>
        </div>  
        <div class="col-2">
        <button type="submit" class="btn btn-success">Aplicar Filtro</button>
        </div>
    </div>
    
    </form>

      </div>
</div>      
<br>

    <div class="row" >

      <div class="col">
      <!-- ejemplo de reporte con graficos -->
      <div class="card">
        <div class="card-body">
      <div id="barchart_values" ></div>
      </div>
      </div>
      </div>

      <div class="col">
      <div class="card">
        <div class="card-body">
        <div id="divxedad"></div>
        </div>
        </div>
      </div>
    </div>
    <br>

    <div class="row">
      <div class="col">
      <div class="card">
        <div class="card-body">
        <!-- ejemplo de reporte con graficos -->
        <div id="columnchart_values" ></div>
        </div>
        </div>
      </div>
    </div>

</div>
        <script type="text/javascript" src="{{ asset('js/charts-loader.js') }}"></script>

  <script type="text/javascript">
     google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(xmeses);

    function xmeses() {
      <?php foreach($xmeses as $mess){ ?>
      var data = google.visualization.arrayToDataTable([
        ["Element", "Personas", { role: "style" } ],
        ["Enero", {{ $mess->enero }}, "#b87333"],
        ["Febrero", {{ $mess->febrero }}, "silver"],
        ["Marzo", {{ $mess->marzo }}, "gold"],
        ["Abril", {{ $mess->abril }}, "color: #e5e4e2"],
        ["Mayo", {{ $mess->mayo }}, "color: #2E64FE"],
        ["Junio", {{ $mess->junio }}, "color: #40FF00"],
        ["Julio", {{ $mess->julio }}, "color: #FE2E2E"],
        ["Agosto", {{ $mess->agosto }}, "color: #FF8000"],
        ["Setiembre", {{ $mess->setiembre }}, "color: #FFFF00"],
        ["Octubre", {{ $mess->octubre }}, "color: #A4A4A4"],
        ["Noviembre", {{ $mess->noviembre }}, "color: #82FA58"],
        ["Diciembre", {{ $mess->diciembre }}, "color: #0040FF"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Registros de Personas en el año {{ $anio }} : {{ $mess->total }}",
       // width: 100%,
        height: 300,
        legend: { position: "none" },
      };

      <?php } ?>
      
      var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
      chart.draw(view, options);
  }

  google.charts.setOnLoadCallback(xdias);

  function xdias() {

    <?php foreach($xdias as $i => $dia){ ?>
      var data = google.visualization.arrayToDataTable([
        ['Year', 'Personas', { role: 'style' } ],
        ["1", {{ $dia->diat1 }}, "#b87333"],
        ["2", {{ $dia->diat2 }}, "#e5e4e2"],
        ["3", {{ $dia->diat3 }}, "#b87333"],
        ["4", {{ $dia->diat4 }}, "#e5e4e2"],
        ["5", {{ $dia->diat5 }}, "#b87333"],
        ["6", {{ $dia->diat6 }}, "#e5e4e2"],
        ["7", {{ $dia->diat7 }}, "#b87333"],
        ["8", {{ $dia->diat8 }}, "#e5e4e2"],
        ["9", {{ $dia->diat9 }}, "#b87333"],
        ["10", {{ $dia->diat10 }}, "#e5e4e2"],
        ["11", {{ $dia->diat11 }}, "#b87333"],
        ["12", {{ $dia->diat12 }}, "#e5e4e2"],
        ["13", {{ $dia->diat13 }}, "#b87333"],
        ["14", {{ $dia->diat14 }}, "#e5e4e2"],
        ["15", {{ $dia->diat15 }}, "#b87333"],
        ["16", {{ $dia->diat16 }}, "#e5e4e2"],
        ["17", {{ $dia->diat17 }}, "#b87333"],
        ["18", {{ $dia->diat18 }}, "#e5e4e2"],
        ["19", {{ $dia->diat19 }}, "#b87333"],
        ["20", {{ $dia->diat20 }}, "#e5e4e2"],
        ["21", {{ $dia->diat21 }}, "#b87333"],
        ["22", {{ $dia->diat22 }}, "#e5e4e2"],
        ["23", {{ $dia->diat23 }}, "#b87333"],
        ["24", {{ $dia->diat24 }}, "#e5e4e2"],
        ["25", {{ $dia->diat25 }}, "#b87333"],
        ["26", {{ $dia->diat26 }}, "#e5e4e2"],
        ["27", {{ $dia->diat27 }}, "#b87333"],
        ["28", {{ $dia->diat28 }}, "#e5e4e2"],
        ["29", {{ $dia->diat29 }}, "#b87333"],
        ["30", {{ $dia->diat30 }}, "#e5e4e2"],
        ["31", {{ $dia->diat31 }}, "#b87333"],
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Registro por dia en el mes de {{ $Month_letra }}: {{ $dia->total }}",
      //  width: 970,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        fontSize: 13,
      };

      <?php } ?>
      
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }

  google.charts.setOnLoadCallback(xedad);

  function xedad() {
      var data = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['15 a 18', {{ $edad15_18 }}],
        ['19 a 25', {{ $edad19_25 }}],
        ['26 a 30', {{ $edad26_30 }}],
        ['31 a 45', {{ $edad31_45 }}],
        ['46 a 55', {{ $edad46_55 }}],
        ['55 a mas', {{ $edad56_mas }}]
      ]);

      var options = {
        title: 'Registro por edad en el mes de {{ $Month_letra }}: {{ $total_usuarios }}',
        height: 300,
      };

var chart = new google.visualization.PieChart(document.getElementById('divxedad'));

chart.draw(data, options);
}
  
  </script>



@endsection