@extends('layouts.app-entidad')

@section('content')

<div class="container " >

<div class="card">
      <div class="card-body">

    <form action="{{ route('reporte.entidad.etres') }}" method="get">
    
    <div class="row">
        <div class="col">
            <select class="form-control" id="m" name="m">
              <option value="">Mes</option>
              <option value="01" {{ isset($Month) && $Month == '01' ? 'selected' : '' }}>Enero</option>
              <option value="02" {{ isset($Month) && $Month == '02' ? 'selected' : '' }}>Febrero</option>
              <option value="03" {{ isset($Month) && $Month == '03' ? 'selected' : '' }}>Marzo</option>
              <option value="04" {{ isset($Month) && $Month == '04' ? 'selected' : '' }}>Abril</option>
              <option value="05" {{ isset($Month) && $Month == '05' ? 'selected' : '' }}>Mayo</option>
              <option value="06" {{ isset($Month) && $Month == '06' ? 'selected' : '' }}>Junio</option>
              <option value="07" {{ isset($Month) && $Month == '07' ? 'selected' : '' }}>Julio</option>
              <option value="08" {{ isset($Month) && $Month == '08' ? 'selected' : '' }}>Agosto</option>
              <option value="09" {{ isset($Month) && $Month == '09' ? 'selected' : '' }}>Setiembre</option>
              <option value="10" {{ isset($Month) && $Month == '10' ? 'selected' : '' }}>Octubre</option>
              <option value="11" {{ isset($Month) && $Month == '11' ? 'selected' : '' }}>Noviembre</option>
              <option value="12" {{ isset($Month) && $Month == '12' ? 'selected' : '' }}>Diciembre</option>
            </select>
        </div>  
        <div class="col">
            <select class="form-control" id="a" name="a">
              <option value="">Año</option>
              <option value="2019" {{ isset($anio) && $anio == '2019' ? 'selected' : '' }}>2019</option>
              <option value="2020" {{ isset($anio) && $anio == '2020' ? 'selected' : '' }}>2020</option>
              <option value="2021" {{ isset($anio) && $anio == '2021' ? 'selected' : '' }}>2021</option>
              <option value="2021" {{ isset($anio) && $anio == '2022' ? 'selected' : '' }}>2022</option>
              <option value="2021" {{ isset($anio) && $anio == '2023' ? 'selected' : '' }}>2023</option>
            </select>
        </div>  
        <div class="col-2">
        <button type="submit" class="btn btn-success">Aplicar Filtro</button>
        </div>
    </div>
    
    </form>

      </div>
</div>      
<br>
<br>
    <div class="row" >
        <div class="col"> 
            <h3>OFERTAS Y COLOCACIONES SEGUN SEXO Y RANGO DE EDADES</h3>
        </div>
    </div>
    <br>
        <div class="row" >
            <div class="col-12"> 
                <table class="table table-hover table-sm">
                   <thead>
                       <tr style="background: #b00002;color: #fff;">
                        <th colspan="12" >OFERTA</th>
                       </tr>
                       <tr style="background: #c3bdbd;">
                        <th colspan="6">MUJERES</th>
                        <th colspan="6">HOMBRES</th>
                       </tr>
                   </thead>
                   <tbody>
                        <tr>
                            <td>18_24</td>
                            <td>25_29</td>
                            <td>30_45</td>
                            <td>46_59</td>
                            <td>60 a mas</td>
                            <td>total</td>
                            <td>18_24</td>
                            <td>25_29</td>
                            <td>30_45</td>
                            <td>46_59</td>
                            <td>60 a mas</td>
                            <td>total</td>
                        </tr>
                        <tr>
                            <td>{{ $edad_m18_24 }}</td>
                            <td>{{ $edad_m25_29 }}</td>
                            <td>{{ $edad_m30_45 }}</td>
                            <td>{{ $edad_m46_59 }}</td>
                            <td>{{ $edad_m60_mas }}</td>
                            <td>{{ $edad_m18_24 + $edad_m25_29 + $edad_m30_45 + $edad_m46_59 + $edad_m60_mas }}</td>
                            <td>{{ $edad_f18_24 }}</td>
                            <td>{{ $edad_f25_29 }}</td>
                            <td>{{ $edad_f30_45 }}</td>
                            <td>{{ $edad_f46_59 }}</td>
                            <td>{{ $edad_f60_mas }}</td>
                            <td>{{ $edad_f18_24 + $edad_f25_29 + $edad_f30_45 + $edad_f46_59 + $edad_f60_mas }}</td>
                        </tr>
                   </tbody>
                </table>
                <br>
            </div>

            <div class="col-12"> 
                <table class="table table-hover table-sm">
                   <thead>
                       <tr style="background: #b00002;color: #fff;">
                        <th colspan="12" >COLOCACIONES</th>
                       </tr>
                       <tr style="background: #c3bdbd;">
                        <th colspan="6">MUJERES</th>
                        <th colspan="6">HOMBRES</th>
                       </tr>
                   </thead>
                   <tbody>
                        <tr>
                            <td>18_24</td>
                            <td>25_29</td>
                            <td>30_45</td>
                            <td>46_59</td>
                            <td>60 a mas</td>
                            <td>total</td>
                            <td>18_24</td>
                            <td>25_29</td>
                            <td>30_45</td>
                            <td>46_59</td>
                            <td>60 a mas</td>
                            <td>total</td>
                        </tr>
                        <tr>
                            <td>{{ $colo_m18_24 }}</td>
                            <td>{{ $colo_m25_29 }}</td>
                            <td>{{ $colo_m30_45 }}</td>
                            <td>{{ $colo_m46_59 }}</td>
                            <td>{{ $colo_m60_mas }}</td>
                            <td>{{ $colo_m25_29 + $colo_m25_29 + $colo_m30_45 + $colo_m46_59 + $colo_m60_mas }}</td>
                            <td>{{ $colo_f18_24 }}</td>
                            <td>{{ $colo_f25_29 }}</td>
                            <td>{{ $colo_f30_45 }}</td>
                            <td>{{ $colo_f46_59 }}</td>
                            <td>{{ $colo_f60_mas }}</td>
                            <td>{{ $colo_f25_29 + $colo_f25_29 + $colo_f30_45 + $colo_f46_59 + $colo_f60_mas }}</td>
                        </tr>
                   </tbody>
                </table>
            </div>
        </div>
   
       
</div>

@endsection
