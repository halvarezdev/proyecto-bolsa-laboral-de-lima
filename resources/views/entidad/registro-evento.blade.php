
<form method="POST" action="{{ route('eventos.submit') }}" id="formulario_evento" enctype="multipart/form-data">

<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
<input type="hidden" id="base_url" value="{{ asset('') }}">
<input type="hidden" id="entidad" name="entidad" value="{{ $entidad }}">
<input type="hidden" id="id_evento_evento" name="id_evento" value="0">
            <div class="card">
                <div class="card-body">
                        @csrf

                        <div class="form-group row">
                            <label for="fecha_inicio" class="col-md-2 col-form-label text-md-right col-form-label-sm">{{ __('Fecha Inicio') }}</label>

                            <div class="col-md-4">
                                <input id="fecha_ini_evento" type="date" class="form-control{{ $errors->has('fecha_ini') ? ' is-invalid' : '' }} form-control-sm" name="fecha_ini" id="fecha_ini" value="{{ old('fecha_ini') }}" required placeholder="Ingrese Fecha Inicio">

                                @if ($errors->has('fecha_ini'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fecha_ini') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label for="fecha_fin" class="col-md-2 col-form-label text-md-right col-form-label-sm">{{ __('Fecha Fin') }}</label>

                            <div class="col-md-4">
                                <input id="fecha_fin" type="date" class="form-control{{ $errors->has('fecha_fin') ? ' is-invalid' : '' }} form-control-sm" name="fecha_fin" id="fecha_fin" value="{{ old('fecha_fin') }}" required placeholder="Ingrese Fecha Fin">

                                @if ($errors->has('fecha_fin'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fecha_fin') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- FECHAS INICIO -->

                        <div class="form-group row">
                            
                        </div>

                        <!-- NOMBRE -->

                        <div class="form-group row">
                            <label for="nombre" class="col-md-2 col-form-label text-md-right col-form-label-sm">{{ __('Nombre') }}</label>

                            <div class="col-md-7">
                                <input id="nombre_evento" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }} form-control-sm" name="nombre" value="{{ old('nombre') }}" required placeholder="Ingrese Nombre">

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- DESCRIPCION -->

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-2 col-form-label text-md-right col-form-label-sm">{{ __('Descripcion') }}</label>

                            <div class="col-md-7">
                                <textarea class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} form-control-sm" name="descripcion" id="descripcion" placeholder="Ingrese Descripcion" cols="30" rows="5"></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- IMAGEN 1  -->
                        <div class="form-group row">

                            <div class="col-md-5">
                                <input id="imagen" type="file" class="{{ $errors->has('imagen') ? ' is-invalid' : '' }} form-control-sm" name="imagen" placeholder="Ingrese Imagen" >

                                @if ($errors->has('imagen'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('imagen') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5">
                            <input id="link_1" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }} form-control-sm" name="link_form1" value="{{ old('link') }}" placeholder="Link">
                                @if ($errors->has('link'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!--
                                <div class="col-md-2">
                            <select class="form-control" id="tipo_1" name="tipo_1">
                                <option value="0">Seleccione</option>
                                <option value="1">Registro Postulante</option>
                                <option value="2">Registro Empresa</option>
                            </select>
                            </div>
                            -->
                            <div class="col-md-2">
                            <select class="form-control" id="estado_1" name="estado1">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                            <input id="ruta_1" type="text" class="form-control{{ $errors->has('ruta_1') ? ' is-invalid' : '' }} form-control-sm" name="ruta_1" value="{{ old('ruta_1') }}" placeholder="Ruta">
                                @if ($errors->has('ruta_1'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ruta_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <hr>
                        <!-- IMAGEN 1  -->
                        <div class="form-group row">

                            <div class="col-md-5">
                                <input id="imagen2" type="file" class="{{ $errors->has('imagen2') ? ' is-invalid' : '' }} form-control-sm" name="imagen2"  placeholder="Ingrese Imagen" >

                                @if ($errors->has('imagen2'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('imagen2') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5">
                            <input id="link_2" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }} form-control-sm" name="link_form2" value="{{ old('link') }}" placeholder="link">
                                @if ($errors->has('link'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!--
                                <div class="col-md-2">
                            <select class="form-control" id="tipo_2" name="tipo_2">
                                <option value="0">Seleccione</option>
                                <option value="1">Registro Postulante</option>
                                <option value="2">Registro Empresa</option>
                            </select>
                            </div>
                             -->
                            <div class="col-md-2">
                            <select class="form-control" id="estado_2" name="estado2">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                            <input id="ruta_2" type="text" class="form-control{{ $errors->has('ruta_2') ? ' is-invalid' : '' }} form-control-sm" name="ruta_2" value="{{ old('ruta_2') }}" placeholder="Ruta 2">
                                @if ($errors->has('ruta_2'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ruta_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <!-- IMAGEN 1  -->
                        <div class="form-group row">
                            <div class="col-md-5">
                                <input id="imagen3" type="file" class="{{ $errors->has('imagen3') ? ' is-invalid' : '' }} form-control-sm" name="imagen3"  placeholder="Ingrese Imagen" >

                                @if ($errors->has('imagen3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('imagen3') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5">
                            <input id="link_3" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }} form-control-sm" name="link_form3" value="{{ old('link') }}" placeholder="link">
                                @if ($errors->has('link'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!--
                                <div class="col-md-2">
                            <select class="form-control" id="tipo_3" name="tipo_3">
                                <option value="0">Seleccione</option>
                                <option value="1">Registro Postulante</option>
                                <option value="2">Registro Empresa</option>
                            </select>
                            </div>
                             -->
                            <div class="col-md-2">
                            <select class="form-control" id="estado_3" name="estado3">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                            <input id="ruta_3" type="text" class="form-control{{ $errors->has('ruta_3') ? ' is-invalid' : '' }} form-control-sm" name="ruta_3" value="{{ old('ruta_3') }}" placeholder="Ruta 3">
                                @if ($errors->has('ruta_3'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ruta_3') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                </div>
            </div>


<br>
<div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4" style="text-align: right;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-success">
                                    {{ __('Aceptar') }}
                                </button>
                            </div>
                        </div>


</form>
<script>
function editaEvento(item){
    let base_url = $("#base_url").val();
    let nombre_h = $("#nombre_h"+item).val();
    let descripcion_h = $("#descripcion_h"+item).val();
    let fecha_inicio_h = $("#fecha_inicio_h"+item).val();
    let fecha_fin_h = $("#fecha_fin_h"+item).val();
    let id_evento_h = $("#id_evento_h"+item).val();
    let link_form1 = $("#ruta_imagen"+item).val();
    let link_form2 = $("#ruta_imagen_2"+item).val();
    let link_form3 = $("#ruta_imagen_3"+item).val();
    let estado_1 = $("#estado1"+item).val();
    let estado_2 = $("#estado2"+item).val();
    let estado_3 = $("#estado3"+item).val();
    let tipo_1 = $("#tipo1"+item).val();
    let tipo_2 = $("#tipo2"+item).val();
    let tipo_3 = $("#tipo3"+item).val();
    let ruta_1 = $("#ruta_1"+item).val();
    let ruta_2 = $("#ruta_2"+item).val();
    let ruta_3 = $("#ruta_3"+item).val();
    $("#id_evento_evento").val(id_evento_h);
    $("#nombre_evento").val(nombre_h);
    $("#descripcion").val(descripcion_h);
    $("#fecha_ini_evento").val(fecha_inicio_h.split(" ")[0]);
    $("#fecha_fin").val(fecha_fin_h.split(" ")[0]);
    if(link_form1 != ''){
        $("#link_1").val(base_url+link_form1);
    } else {
        $("#link_1").val("");
    }
    if(link_form2 != ''){
        $("#link_2").val(base_url+link_form1);
    } else {
        $("#link_2").val("");
    }

    if(link_form3 != ''){
        $("#link_3").val(base_url+link_form1);
    } else {
        $("#link_3").val("");
    }
    $("#estado_1").val(estado_1);
    $("#estado_2").val(estado_2);
    $("#estado_3").val(estado_3);
    $("#tipo_1").val(tipo_1);
    $("#tipo_2").val(tipo_2);
    $("#tipo_3").val(tipo_3);
    $("#ruta_1").val(ruta_1);
    $("#ruta_2").val(ruta_2);
    $("#ruta_3").val(ruta_3);
    document.getElementById("formulario_evento").setAttribute('action', base_url+ 'evento/update');
    // $("#formulario_evento").
}
function limpiar(){
    let base_url = $("#base_url").val();
    $("#id_evento_evento").val("");
    $("#nombre_evento").val("");
    $("#descripcion").val("");
    $("#fecha_ini_evento").val("");
    $("#fecha_fin").val("");
    $("#link_form1").val("");
    $("#link_form2").val("");
    $("#link_form3").val("");
    $("#estado_1").val("1");
    $("#estado_2").val("1");
    $("#estado_3").val("1");
    /*$("#tipo_1").val("0");
    $("#tipo_2").val("0");
    $("#tipo_3").val("0");*/
    $("#link_1").val("");
    $("#link_2").val("");
    $("#link_3").val("");
    $("#ruta_1").val("");
    $("#ruta_2").val("");
    $("#ruta_3").val("");
    document.getElementById("formulario_evento").setAttribute('action', base_url+ 'evento/registro');
}
</script>