@extends('layouts.app-entidad')

@section('content')

<br>
<div class="container">

	<div class="card">
		<div class="card-body">
		<div class="row justify-content-md-center">
		<div class="col">

			<form action="{{ route('entidad.vacante') }}" method="get">

				<div class="form-row align-items-center">
					<div class="col-sm-3 my-1">
						<label class="sr-only" for="ruc">Ruc</label>
						<input type="text" class="form-control" id="ruc" name="ruc" placeholder="Ruc" value="{{ $ruc_busqueda }}">
					</div>

					<div class="col-auto my-1">
						<button type="submit" class="btn btn-primary">Buscar</button>
					</div>
					<!--
                        <div class="col-3">
						<a href="{{ route('envio.masivo') }}">Enviar Masivos</a>
					</div>
                     -->
				</div>
			</form>


				<h3>PANEL ADMINISTRADOR DE VACANTES</h3>
				<table class="table table-hover ">
					<thead >
						<th >#</th>
						<th >CÓDIGO</th>
						<th >DESCRIPCION</th>
						<th >RUC</th>
						<th >RAZON SOCIAL</th>
						<th >NOMBRE CONTACTO</th>
						<th >TELEFONO</th>
						<th >EMAIL</th>
						<th >TOTAL VACANTES</th>
						<th  colspna="2">ACCIONES</th>
					</thead>
					<tbody>
						@foreach($pedidos as $i => $ped)
						<tr>
							<td style="font-size:13px;">{{ $i+1 }}
								<input type="hidden"  id="id_codigo{{$ped->pedido_codigo}}" value="{{ route('entidad.publicar',['id' => $ped->pedido_codigo]) }}">
							</td>
							<td style="font-size:13px;"><a href="{{ route('vacante.detalle',['id' => $ped->pedido_codigo,'e' => $entidad] ) }}">{{ $ped->pedido_codigo }}</a></td>
							<td style="font-size:13px;">{{ $ped->descripcion_pedido }}</td>
							<td style="font-size:13px;">{{ $ped->ruc }}</td>
							<td style="font-size:13px;">{{ $ped->razon_social }}</td>
							<td style="font-size:13px;">{{ $ped->name }}</td>
							<td style="font-size:13px;">{{ $ped->telefono }}</td>
							<td style="font-size:13px;">{{ $ped->email }}</td>
							<td style="font-size:13px; text-align: center;">{{ $ped->total_pedidos }}</td>
							<td style="font-size:13px;">
							<a href="#" style="" class="title" rel="Publicar Pedido" data-toggle="modal" data-target="#publicar" onclick="return publicar('{{ $ped->pedido_codigo }}')">
							<i class="fas fa-check"></i>
							</a>&nbsp;&nbsp;&nbsp;
							<a href="#" class="title" rel="No Publicar Pedido" data-toggle="modal" data-target="#no_publicar">
								<i class="fas fa-times"></i>
							</a>
							</td>
						
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

			
	<div class="row justify-content-md-center">
        <div class="col"> 
            {{ $pedidos->links() }}
        </div>
    </div>

		</div>
	</div>


</div>



<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="publicar" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
         ¿Esta Seguro de publicar?
        </div>
        
        
        <div class="modal-footer">
          <a href="#" class="btn btn-success" id="btnEliminar" onclick="return publicarPedido()">Publicar</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="no_publicar" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
		<form>
      <div class="modal-body">
         
					<div class="form-group">
						<label>No se publica por:</label>
						<select name="" id="" class="form-control">
								<option value="">Seleccione</option>
								<option value="2">Contiene informacion discriminatoria u ofensiva</option>
								<option value="3">Informacion de la vacante no esta completa o errónea</option>
								<option value="4">Los datos de la empresa no estan correctos</option>
								<option value="5">Los datos del contacto no estan completos</option>
								<option value="6">Este aviso se repite frecuentemente</option>
								<option value="7">Otros</option>
						</select>
					<!--	<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
					</div>
				 		
				
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="#" class="btn btn-success" id="btnEliminar" onclick="return publicarPedido()">Publicar</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
				</form>
    </div>
  </div>
</div>



<script type="text/javascript">
function publicar(tele) {
        ruta = $("#id_codigo"+tele).val();
  // alert(tele)
   // var tds = lnk.parentNode.parentNode.id;
 //   $("#btnEliminar").attr('onclick', 'EliminarPedidoPostulante2('+id+')');
    $("#btnEliminar").attr('href', ruta);
  }



</script>

@endsection
