@extends('layouts.app-entidad')

@section('content')

<div class="container " >
<br>
    <div class="row" >
        <div class="col"> 
            <h3>Listado de Personas Registradas</h3>
        </div>
    </div>
    <br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                    <thead class="thead-dark ">
                        <tr>
                            <th>#</th>
                            <th>Documento</th>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Email</th>
                            <th>Tipo</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listado_persona as $i => $value)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $value->numero_documento }}</td>
                                <td>{{ $value->nombre }}</td>
                                <td>{{ $value->ape_pat." ".$value->ape_mat }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ ($value->tipo_entidad == '2' ? 'BD' : 'WEB') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-sm-6"> 
                {{ $listado_persona->links() }}
            </div>
        </div>
   
       
</div>

@endsection
