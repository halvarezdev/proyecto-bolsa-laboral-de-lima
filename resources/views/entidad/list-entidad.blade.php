@extends('layouts.app-entidad')

@section('content')

<div class="container " >
<br>
<div class="row" >
        <div class="col"> 
            <h3>Listado de Sedes</h3>
        </div>
    </div>
    <br>
    <div class="row">
            <div class="form-inline col-md-12">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" title="Mostrar panel de sede">Nuevo</button>
            </div>
        </div><br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                <thead class="thead-dark ">
                        <tr>
                            <th>#</th>
                            <th>Descripc&iacute;on</th>
                            <th>Estado</th>
                            <th>Editar</th>
                            <th>enular</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($entidad as $i => $value)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $value->enti_nombre }}</td>
                                <td>{{ $value->enti_flag == '1' ? 'Activo' : 'Inactivo' }}</td>
                                <td><a href="#"  data-toggle="modal" data-target="#myModal" onClick="editEntity('{{$value->enti_nombre}}', '{{$value->id}}')">Editar</a></td>
                                @if($value->enti_flag == '1' && $value->id != '1') 
                                <td><a href="{{ route('entity.delete', ['id' => $value->id, 'flag' => '2']) }}" style="color:red">Anular</a></td>
                                @elseif($value->enti_flag == '1' && $value->id == '1')
                                <td>Anular</td>
                                @else
                                <td><a href="{{ route('entity.delete', ['id' => $value->id, 'flag' => '1']) }}" >Activar</a></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-sm-6"> 
                {{-- $lista_postulante->links() --}}
            </div>
        </div>
   
       
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
           <form action="{{ route('entity.save') }}" method="post" id="formEntity">
           <div class="modal-header">
                <h4 class="modal-title">SEDE</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">           
            @csrf

                <div class="row" id="filaDatos">
                    <div class="col-md-6 form-group">
                        <label>Descripción <span style="color:red">*</span></label>
                        <input type="hidden" name="enti_id" id="enti_id">
                        <input class="form-control input-sm" type="text" id="enti_nombre" name="enti_nombre">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success" title="Guardar Sede"><i class="fa fa-floppy" aria-hidden="true"></i> Guardar</button>
            </div>
           </form>
        </div>
    </div>
</div>

<script>
    function editEntity(nombre, id){
        $('#formEntity').attr('action', '{{ route("entity.update") }}');
        $('#enti_nombre').val(nombre);
        $('#enti_id').val(id);
        
    }
</script>
@endsection
