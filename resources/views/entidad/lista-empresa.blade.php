@extends('layouts.app-entidad')

@section('content')

<div class="container " >
<br>
    <div class="row" >
        <div class="col"> 
            <h3>Listado de Empresas Registradas</h3>
        </div>
    </div>
    <br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                    <thead class="thead-dark ">
                        <tr>
                            <th>#</th>
                            <th>Documento</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Descripcion</th>
                            <th>Tipo</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listado_empresa as $i => $value)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td><a href="{{ route('empresa.descripcion', ['codigo' => $value->codigo_empresa]) }}">{{ $value->numero_documento }}</a></td>
                                <td>{{ $value->razon_social }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->descripcion }}</td>
                                <td>{{ ($value->tipo_entidad == '2' ? 'BD' : 'WEB') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-sm-6"> 
                {{ $listado_empresa->links() }}
            </div>
        </div>
   
       
</div>

@endsection
