@extends('layouts.app-entidad')
@section('content')
<div class="container">

@if(isset($tipo) && $tipo == '2')
<style>
   /* body{
    background-image: url("{{ asset('fondo/cenfotur-fondo.png') }}" );
    background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
        
  }*/
</style>
<form method="POST" action="{{ route('entidad.empresa.submit') }}" id="formulario_persona">
@else
<form method="POST" action="{{ route('contacto.create') }}" class="registroformulario">
@endif

<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
<input type="hidden" id="base_url" value="{{ asset('') }}">
<input type="hidden" id="entidad" name="entidad" value="{{ $entidad }}">
    <div class="row justify-content-center">
        <div class="col-md-10">
                <div class="div_titulo">
                    <h4 class="titulo"><b>REGISTRO DE EMPRESA</b></h4>

                </div>
            <div class="card">
                <div class="card-body">
                    
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="titulo_formulario " style="color:black;">
                                    <!-- <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;"> -->
                                    Datos de Accesos
                                </h5>
                            </div>
                        </div>

                        <!-- CORREO ELECTRONICO -->

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Correo Electronico') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-sm" name="email" value="{{ old('email') }}" required placeholder="Ingrese Correo Electronico">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- CONTRASEÑA -->

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" name="password" required placeholder="Ingrese Contraseña">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- COMFIRMAR CONTRASEÑA -->

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Confirmar Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" required placeholder="Confirmar la Contraseña">
                            </div>
                        </div>
                </div>
            </div>

            <br>

             <!-- DATOS DE EMPRESA -->
             <div class="card">
                    <div class="card-body">


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="titulo_formulario " style="color:black;">
                                    <!-- <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;"> -->
                                    Datos de la Empresa
                                </h5>
                            </div>
                        </div>

                                                <!-- TIPO DOCUMENTO -->

                        <div class="form-group row">
                            <label for="tipo_documento" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Tipo documento') }}</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm" name="tipo_documento" value="{{ old('tipo_documento') }}" id="tipo_documento" required autofocus> 
                                    @foreach($documentos as $documento)
                                        <option value="{{ $documento->id }}">{{ $documento->tipdoc_descripion }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('tipo_documento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipo_documento') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>


                        <!-- NUMERO DOCUMENTO -->
                        <div class="form-group row">
                            <label for="numero_documento" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Numero de documento') }}</label>

                            <div class="col-md-6">
                                <input id="numero_documento" maxlength="11" type="text" class="form-control{{ $errors->has('numero_documento') ? ' is-invalid' : '' }} form-control-sm" name="numero_documento" value="{{ old('numero_documento') }}" required autofocus placeholder="Ingrese Numero de Documento">

                                @if ($errors->has('numero_documento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Numero Documento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!--   RAZION SOCIAL  -->

                        <div class="form-group row">
                            <label for="razon_social" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Razon Social') }}</label>

                            <div class="col-md-6">
                                <input id="razon_social" type="text" class="form-control{{ $errors->has('razon_social') ? ' is-invalid' : '' }} form-control-sm" name="razon_social" value="{{ old('razon_social') }}" required autofocus placeholder="Ingrese Razon Social">

                                @if ($errors->has('razon_social'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('razon_social') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <!--   DESCRIPCION  -->

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Descripción') }}</label>

                            <div class="col-md-6">
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="descripcion" placeholder="Ingrese Una Descripcion" required autofocus></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!--   RUBRO  -->

                        <div class="form-group row">
                            <label for="rubro" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Rubro de la Empresa') }}</label>

                            <div class="col-md-6">
                            <div class="control-group">
                                <select id="rubro" class="demo-default form-control form-control-sm buscar" data-placeholder="Seleccione Rubro" name="rubro" onfocus="limiartodo(this)" required>
                                    <option value="">Buscar</option>
                                    @foreach($sector as $sec)
                                        <option value="{{ $sec->id }}" >{{ $sec->sec_descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                        </div>

                        <!--   SITIO WEB  -->

                        <div class="form-group row">
                            <label for="sitio_web" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Sitio Web') }}</label>

                            <div class="col-md-6">
                                <input id="sitio_web" type="text" class="form-control form-control-sm" name="sitio_web" value="{{ old('sitio_web') }}" placeholder="Ingrese El Sitio Web">

                                @if ($errors->has('sitio_web'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sitio_web') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!--   DIRECCION   -->

                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Direccion') }}</label>

                            <div class="col-md-6">
                                <input id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }} form-control-sm" name="direccion" value="{{ old('direccion') }}" required autofocus placeholder="Ingrese la Direccion">

                                @if ($errors->has('direccion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('direccion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!--   UBIGEO  -->

                        <div class="form-group row">
                            <div class="col-12 col-sm-12 col-md-6">
                                <select class="form-control form-control-sm"
                                    name="departamento" value="{{ old('departamento') }}"
                                    id="departamento"
                                    required
                                    autofocus
                                    onchange="ComboboxProvincia(this)"> 
                                    <option value="">Seleccione</option>
                                    @foreach($ubigeo as $ubi)
                                        <option value="{{ $ubi->ubi_coddpto }}">{{ $ubi->ubi_descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                                <br>
                            <div class="col-12 col-sm-12 col-md-6">
                                <select class="form-control{{ $errors->has('provincia') ? ' is-invalid' : '' }} form-control-sm" name="provincia" value="{{ old('provincia') }}" id="provincia" required autofocus onchange="ComboboxDistrito(this)"> 
                                    <option value="0">Seleccione Provincia</option>
                                </select>
                            </div>
                            <br>
                            <div class="col-12 col-sm-12 col-md-6">
                                <select class="form-control{{ $errors->has('distrito') ? ' is-invalid' : '' }} form-control-sm" name="distrito" value="{{ old('distrito') }}" id="distrito" required autofocus > 
                                    <option value="0">Seleccione Distrito</option>
                                </select>
                            </div>

                        </div>


                    </div>
                </div>
<br>

<!-- DATOS DE CONTACTO -->
                <div class="card">
                    <div class="card-body">


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="titulo_formulario " style="color:black;">
                                    <!-- <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;"> -->
                                    Datos del Contacto de la Empresa
                                </h5>
                            </div>
                        </div>


                           <!-- NOMBRE -->

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Nombres') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-sm" name="name" value="{{ old('name') }}" required autofocus placeholder="Ingrese Nombre">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>    


                        <!-- TIPO DOCUMENTO -->

                        <div class="form-group row">
                            <label for="tipo_documento_contacto" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Tipo documento') }}</label>
                            <div class="col-sm-6">
                                <select class="form-control{{ $errors->has('tipo_documento_contacto') ? ' is-invalid' : '' }} form-control-sm" name="tipo_documento_contacto" value="{{ old('tipo_documento_contacto') }}" id="tipo_documento_contacto" required autofocus> 
                                    @foreach($documentos as $documento)
                                    @if($documento->id != '6')
                                        <option value="{{ $documento->id }}">{{ $documento->tipdoc_descripion }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('tipo_documento_contacto'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipo_documento_contacto') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>


                        <!-- NUMERO DOCUMENTO -->
                        <div class="form-group row">
                            <label for="numero_documento_contacto" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Numero de documento') }}</label>

                            <div class="col-md-6">
                                <input id="numero_documento_contacto" type="text" class="form-control{{ $errors->has('numero_documento_contacto') ? ' is-invalid' : '' }} form-control-sm" name="numero_documento_contacto" value="{{ old('numero_documento_contacto') }}" required autofocus placeholder="Ingrese Numero de Documento">

                                @if ($errors->has('numero_documento_contacto'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('numero_documento_contacto Documento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!-- CARGO -->
                        <div class="form-group row">
                            <label for="cargo" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Cargo') }}</label>

                            <div class="col-md-6">
                                <input id="cargo" type="text" class="form-control{{ $errors->has('cargo') ? ' is-invalid' : '' }} form-control-sm" name="cargo" value="{{ old('cargo') }}" required autofocus placeholder="Ingrese Cargo">

                                @if ($errors->has('cargo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cargo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                    


                        <!-- CELULAR -->
                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Telefono') }}</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }} form-control-sm" name="telefono" value="{{ old('telefono') }}" required autofocus placeholder="Ingrese Telefono">

                                @if ($errors->has('telefono'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  


                        <!-- CORREO ELECTRONICO -->
                        <div class="form-group row">
                            <label for="email_contacto" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Correo Electronico') }}</label>

                            <div class="col-md-6">
                                <input id="email_contacto" type="email" class="form-control{{ $errors->has('email_contacto') ? ' is-invalid' : '' }} form-control-sm" name="email_contacto" value="{{ old('email_contacto') }}" required autofocus placeholder="Ingrese Correo Electronico">

                                @if ($errors->has('email_contacto'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email_contacto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  




                    </div>



<br>
<div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Crear Cuenta') }}
                                </button>
                            </div>
                        </div>


        </div>
    </div>
</form>
</div>
<br><br>
@endsection
