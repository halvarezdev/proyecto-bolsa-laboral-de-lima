@extends('layouts.app-entidad')

@section('content')

<div class="container">

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <p class="font-italic" style="font-size:20px;margin-bottom: 0rem;">Reporte grafica / Profesión.</p>
                </div>
            </div>
        </div>
    </div>

<br>
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <div id="xprfesiones"></div>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <p class="font-italic" style="font-size:16px;margin-bottom: 0rem;"><b>Filtro Profesiones</b>.</p>
                        <br>
                       <form method="get" action="{{ route('reporte.presona.ocupacion.grafico') }}" id="form_actualiza">
                       
                       @foreach($parametrica as $i => $para)
                        <div class="custom-control custom-checkbox my-1 mr-sm-2">
                            <input <?php foreach($checkbox as $u => $dd){  if($dd['codigo'] == $para->cod_profesion){ echo "checked";} } ?> onclick="actualiza()" type="checkbox" value="{{ $para->cod_profesion }}" class="custom-control-input" id="profesiones{{ $i }}" name="profesiones{{ $i  }}">
                            <label class="custom-control-label" for="profesiones{{ $i  }}">{{ $para->nombre }}</label>
                        </div> 
                        @endforeach
                        <input type="hidden" name="totales" id="totales" value="{{ isset($i) ? $i : null }}">
                       </form>
                    </table>
                </div>
            </div>
        </div>
       
    </div>
<br>
    <div class="row">
        <div class="col">
            <div id="barchart_values" ></div>
        </div>
        <div class="col">
            <div id="divxedad" ></div>
        </div>
    </div>
<br>

<div class="row">
      <div class="col">
      <div class="card">
        <div class="card-body">
        <!-- ejemplo de reporte con graficos -->
        <div id="columnchart_values" ></div>
        </div>
        </div>
      </div>
    </div>

<br>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div id="distrito"></div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div id="xsexo"></div>
            </div>
        </div>
    </div>
</div>
<br>
  <div class="row">
    <div class="col">
    <div class="card">
            <div class="card-body">
      <table class="table table-striped table-sm ">
      <thead class="thead-light">
        <tr>
          <th></th>
          <th>M</th>
          <th>F</th>
          <th></th>
          <th>Total</th>
        </tr>
      </thead>
        <tbody>
          <tr>
              <td>N° Registados</td>
              <td>{{ $registrados_m }}</td>
              <td>{{ $registrados_f }}</td>
              <td>{{ $registrados_x }}</td>
              <td>{{ $registrados }}</td>
          </tr>
          <tr>
              <td>N° Intermediado</td>
              <td>{{ $intermediado_m }}</td>
              <td>{{ $intermediado_f }}</td>
              <td>{{ $intermediado_x }}</td>
              <td>{{ $intermediado }}</td>
          </tr>
          <tr>
              <td>N° Colocado</td>
              <td>{{ $colocado_m }}</td>
              <td>{{ $colocado_f }}</td>
              <td>{{ $colocado_x }}</td>
              <td>{{ $colocado }}</td>
          </tr>
          <tr>
              <td>N° Vacantes</td>
              <td></td>
              <td></td>
              <td></td>
              <td>{{ $vacantes }}</td>
          </tr>
          <tr>
              <td>N° Empresas</td>
              <td></td>
              <td></td>
              <td></td>
              <td>{{ $empresas }}</td>
          </tr>
        </tbody>
      </table>
      </div>
      </div>
    </div>
  </div>

<br>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

function actualiza(){

  $("#form_actualiza").submit();
}

    google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(xmeses);

function xmeses() {
  
  var data = google.visualization.arrayToDataTable([
    ["Element", "Personas", { role: "style" } ],
    ["Enero", {{ $enero }}, "#b87333"],
    ["Febrero", {{ $febrero }}, "silver"],
    ["Marzo", {{ $marzo }}, "gold"],
    ["Abril", {{ $abril }}, "color: #e5e4e2"],
    ["Mayo", {{ $mayo }}, "color: #2E64FE"],
    ["Junio", {{ $junio }}, "color: #40FF00"],
    ["Julio", {{ $julio }}, "color: #FE2E2E"],
    ["Agosto", {{ $agosto }}, "color: #FF8000"],
    ["Setiembre", {{ $setiembre }}, "color: #FFFF00"],
    ["Octubre", {{ $octubre }}, "color: #A4A4A4"],
    ["Noviembre", {{ $noviembre }}, "color: #82FA58"],
    ["Diciembre", {{ $diciembre }}, "color: #0040FF"]
  ]);

  var view = new google.visualization.DataView(data);
  view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" },
                   2]);

  var options = {
    title: "Reporte de personas por mes",
   // width: 100%,
    height: 300,
    legend: { position: "none" },
  };


  
  var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
  chart.draw(view, options);
}
// POR DIA 
google.charts.setOnLoadCallback(xdias);

  function xdias() {

    <?php foreach($xdias as $i => $dia){ ?>
      var data = google.visualization.arrayToDataTable([
        ['Year', 'Personas', { role: 'style' } ],
        ["1", {{ $dia->diat1 }}, "#b87333"],
        ["2", {{ $dia->diat2 }}, "#e5e4e2"],
        ["3", {{ $dia->diat3 }}, "#b87333"],
        ["4", {{ $dia->diat4 }}, "#e5e4e2"],
        ["5", {{ $dia->diat5 }}, "#b87333"],
        ["6", {{ $dia->diat6 }}, "#e5e4e2"],
        ["7", {{ $dia->diat7 }}, "#b87333"],
        ["8", {{ $dia->diat8 }}, "#e5e4e2"],
        ["9", {{ $dia->diat9 }}, "#b87333"],
        ["10", {{ $dia->diat10 }}, "#e5e4e2"],
        ["11", {{ $dia->diat11 }}, "#b87333"],
        ["12", {{ $dia->diat12 }}, "#e5e4e2"],
        ["13", {{ $dia->diat13 }}, "#b87333"],
        ["14", {{ $dia->diat14 }}, "#e5e4e2"],
        ["15", {{ $dia->diat15 }}, "#b87333"],
        ["16", {{ $dia->diat16 }}, "#e5e4e2"],
        ["17", {{ $dia->diat17 }}, "#b87333"],
        ["18", {{ $dia->diat18 }}, "#e5e4e2"],
        ["19", {{ $dia->diat19 }}, "#b87333"],
        ["20", {{ $dia->diat20 }}, "#e5e4e2"],
        ["21", {{ $dia->diat21 }}, "#b87333"],
        ["22", {{ $dia->diat22 }}, "#e5e4e2"],
        ["23", {{ $dia->diat23 }}, "#b87333"],
        ["24", {{ $dia->diat24 }}, "#e5e4e2"],
        ["25", {{ $dia->diat25 }}, "#b87333"],
        ["26", {{ $dia->diat26 }}, "#e5e4e2"],
        ["27", {{ $dia->diat27 }}, "#b87333"],
        ["28", {{ $dia->diat28 }}, "#e5e4e2"],
        ["29", {{ $dia->diat29 }}, "#b87333"],
        ["30", {{ $dia->diat30 }}, "#e5e4e2"],
        ["31", {{ $dia->diat31 }}, "#b87333"],
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Registro por dia ",
      //  width: 970,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
        fontSize: 13,
      };

      <?php } ?>
      
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
// por edad

google.charts.setOnLoadCallback(xedad);

function xedad() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['17  a menos', {{ $edad15_17 }}],
      ['18 a 29', {{ $edad18_29 }}],
      ['30 a 45', {{ $edad30_45 }}],
      ['46 a mas', {{ $edad46_100 }}],
    ]);

    var options = {
      title: 'Reporte de persona por edad',
      height: 300,
    };

var chart = new google.visualization.PieChart(document.getElementById('divxedad'));

chart.draw(data, options);
}

// por sexo

google.charts.setOnLoadCallback(xsexo);

function xsexo() {

  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Pizza');
  data.addColumn('number', 'Populartiy');
  data.addRows([
    ['Femenino', {{ $xsexo_mas }}],
    ['Masculino', {{ $xsexo_feme }}],
  //  ['Indistinto', {{ $xsexo_x }}]
  ]);

  var options = {
    title: 'Reporte Personas por Sexo',
    height: 300,
    fontSize: 12,
  };

  var chart = new google.visualization.PieChart(document.getElementById('xsexo'));
  chart.draw(data, options);
}


// distrito 


google.charts.setOnLoadCallback(xdistrito);

      function xdistrito() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            @foreach($array_distri as $value)
              ['{{ $value["nombre_distrito"] }}',  {{ $value["cantidad"] }}],
          @endforeach
        ]);

        var options = {
          title: 'Reporte por distrito',
          height: 300,
          fontSize: 12,
        };

        var chart = new google.visualization.PieChart(document.getElementById('distrito'));

        chart.draw(data, options);
      }

// por profesionces paste

      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            @foreach($array_profe as $profe)
                ['{{ $profe["nombre"] }}',  {{ $profe["cantidad"] }}],
            @endforeach
        ]);

        var options = {
          title: 'Profesiones',
          height: 300,
          fontSize: 12,
        };

        var chart = new google.visualization.PieChart(document.getElementById('xprfesiones'));

        chart.draw(data, options);
      }
    </script>


@endsection