@extends('layouts.app-entidad')

@section('content')

<div class="container " >
<br>
<div class="row" >
        <div class="col"> 
            <h3>Listado de Pedidos Registradas</h3>
        </div>
    </div>
    <br>
    <div class="row" >
        @foreach($empresa as $i => $value)
            <div class="col-2"> 
                Ruc:
                <br>
                Razon Social
                <br>
            </div>
            <div class="col">
                {{ $value->numero_documento }}
                <br>
                {{ $value->razon_social }}
            </div>    
            <div class="col-2">
            <a href="{{ route('enti.nuevo.pedido', ['empresa' => $value->codigo_empresa]) }}" class="btn btn-info">Nuevo Pedido</a>
            </div>  
        @endforeach
    </div>
    <br>
        <div class="row" >
            <div class="col"> 
                <table class="table table-hover table-sm">
                    <thead class="thead-dark ">
                        <tr>
                            <th>#</th>
                            <th>Descripcion</th>
                            <th>N° Vacantes</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($listado_pedido as $i => $value)
                            <tr>
                                <td>{{ $i+1 }}</td>  
                                <td><a href="{{ route('detalle.pedido', ['empresa' => $value->empresa_id,'pedido'=> $value->codigo_pedido]) }}">{{ $value->descripcion }}</a></td>
                                <td>{{ $value->num_vacantes }}</td>
                                <td>{{ ($value->tipo_entidad == '2' ? 'BD' : 'WEB') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-sm-6"> 
                {{ $listado_pedido->links() }}
            </div>
        </div>
   
       
</div>

@endsection
