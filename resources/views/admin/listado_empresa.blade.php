@extends('layouts.app-admin')

@section('content')
<style>
    .td_list{
        font-size:13px;
    }
.page-wrapper.toggled .page-content {
    padding-left: 250px;        
}
.page-wrapper .page-content > div {

padding: 5px 20px;
}
</style>
<div class="container">

<div class="card">
  <div class="card-body">
         <div class="row">
            <div class="col">
                <p class="font-italic" style="font-size:20px;">Listado de Empresa / Contacto.</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                    <form action="{{ route('empresa.listado.accede') }}" method="get">
                        <div class="form-row align-items-center">
                            <div class="col-sm-7 my-1">
                                <label class="sr-only" for="ruc">Buscar</label>
                                <input type="text" class="form-control" id="ruc" name="ruc" placeholder="Buscar RUC" value="{{ $ruc_empresa }}">
                            </div>

                            <div class="col-auto">
                                <button type="submit" class="btn btn btn btn-success">Buscar</button>
                            </div>
                            <div class="col-auto">
                                <a href="{{ route('exportar.listado.empresa') }}" class="btn btn-info">Descargar Excel</a>
                                
                            </div>
                        </div>
                    </form>
                </div>
        </div>

<br>
        <!-- lista tabla -->
        <div class="row justify-content-md-center">
		<div class="col">	
			<table class="table table-sm table-hover ">
				<thead class="thead-light">
					<th>#</th>
					<th>RUC</th>
					<th>RAZON SOCIAL</th>
					<th>RUBRO</th>
					<th>CONTACTO</th>
                    <th>DNI</th>
                    <th>EMAIL</th>
				</thead>
				<tbody>
					@foreach($list_empresas as $i => $emp)
					<tr>
						<td class="td_list">{{ $emp->cod_empresa }}</td>
						<td class="td_list">{{ $emp->numero_documento}}</td>
						<td class="td_list"><b>{{ $emp->razon_social }}</b></td>
						<td class="td_list">{{ $emp->area_descripcion }}</td>
						<td class="td_list">{{ $emp->name }}</td>
                        <td class="td_list">{{ $emp->dni }}</td>
                        <td class="td_list">{{ $emp->email }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	    </div>

    	<div class="row justify-content-md-center">
            <div class="col"> 
                {{ $list_empresas->links() }}
            </div>
        </div>

    </div>
</div>
	
<br>

</div>



@endsection
