@extends('layouts.app-admin')

@section('content')
<style>
    .td_list{
        font-size:13px;
    }
.page-wrapper.toggled .page-content {
    padding-left: 250px;        
}
.page-wrapper .page-content > div {

padding: 5px 20px;
}
</style>
<div class="container">

<div class="card">
  <div class="card-body">
         <div class="row">
            <div class="col">
                <p class="font-italic" style="font-size:20px;">Listado de Empresa / Persona.</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                    <form action="{{ route('persona.listado.accede') }}" method="get">
                        <div class="form-row align-items-center">
                            <div class="col-sm-7 my-1">
                                <label class="sr-only" for="ruc">Buscar</label>
                                <input type="text" class="form-control" id="dni" name="dni" placeholder="Buscar DNI" value="{{ $dni }}">
                            </div>

                            <div class="col-auto">
                                <button type="submit" class="btn btn btn btn-success">Buscar</button>
                            </div>
                            <div class="col-auto">
                                <a href="{{ route('exportar.listado.persona') }}" class="btn btn-info">Descargar Excel</a>
                                
                            </div>
                        </div>
                    </form>
                </div>
        </div>

<br>
        <!-- lista tabla -->
        <div class="row justify-content-md-center">
		<div class="col">	
			<table class="table table-sm table-hover ">
				<thead class="thead-light">
					<th>#</th>
					<th>DNI</th>
					<th>NOMBRE</th>
					<th>EMAIL</th>
					<th>TELEFONO</th>
                    <th>FECHA NACIMIENTO</th>
				</thead>
				<tbody>
					@foreach($list_personas as $i => $emp)
					<tr>
						<td class="td_list">{{ $emp->codigo }}</td>
						<td class="td_list">{{ $emp->numero_documento}}</td>
						<td class="td_list"><b>{{ $emp->ape_pat }} {{ $emp->ape_mat }} {{ $emp->name }}</b></td>
						<td class="td_list">{{ $emp->email }}</td>
						<td class="td_list">{{ $emp->telefono }}</td>
                        <td class="td_list">{{ $emp->fecha_nacimiento }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	    </div>

    	<div class="row justify-content-md-center">
            <div class="col"> 
                {{ $list_personas->links() }}
            </div>
        </div>

    </div>
</div>
	
<br>

</div>



@endsection
