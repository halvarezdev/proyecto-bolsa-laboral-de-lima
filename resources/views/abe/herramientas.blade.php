<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('maestro.bootstrap')
    <link href="{{ asset('css/asesoramiento.css') }}" rel="stylesheet">
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <title>::Accede::Descubriendo Nuestras Capacidades</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135004982-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135004982-1');
</script>
</head>
<body>
@include('maestro.nav')
<br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="sm-10">
            <h1 class="titulo-abe">HERRAMIENTAS BÁSICAS PARA <br>UN CURRÍCULUM VÍTAE</h1>
        </div>
        <div class="sm-10">
            <div style="width:1128px;height:1007px;    margin: 26px -103px 8px -96px; ;background: url({{ asset('images/taller2.png') }})">
    
    
    
            </div>
        </div>

    </div>


    <div class="row justify-content-center">
          
            <div class="col_1_of_5 col" class="facet_sidebar" class="facet_sidebar1">
                <a href="#" class="caption dgx"></a>    
            </div>

            <div class="col_1_of_5 col" class="facet_sidebar" class="facet_sidebar1">
                <a class="caption dgx1" href="#"></a>
            </div>

            <div class="col_1_of_5 col" class="facet_sidebar" class="facet_sidebar1">
                <a class="caption dgx4" href="#"></a>
            </div>

            <div class="col_1_of_5 col">
                <a class="caption dgx3" href="#"></a>
            </div>
            <div class="col_1_of_5 col">
                <a class="caption dgx5" href="#"></a>
            </div>			

    </div>


 
</div>

@include('maestro.footer_portal')
</body>
</html>