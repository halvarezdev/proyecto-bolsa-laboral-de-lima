<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="MMLima">
	<meta name="author" content="MMLima">

	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
    <title>BOLSA LABORAL - Comunicado | Municipalidad Metropolitana de Lima</title>
    <link rel="shortcut icon" type="image/x-icon" href="https://aplicativos.munlima.gob.pe//uploads/general/favicon1.ico">
    
    <link rel="stylesheet" href="{{asset('assets/bloqueo/style.css')}}"> 
    <link href="{{asset('errors/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('errors/bootstrap-extension.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/brands.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/solid.css')}}">
    <link href="{{asset('assets/bloqueo/MML_Popup.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bloqueo/app_general_style.css')}}">
    <link href="{{asset('assets/bloqueo/pace.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/bloqueo/theme.min.css')}}" rel="stylesheet">

  
   
 </head>
<body class="  pace-done" style="overflow: visible;"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
	<div id="preloader-active" style="display: none;">
		<div class="preloader d-flex align-items-center justify-content-center">
			<div class="preloader-inner position-relative">
				<div class="preloader-circle"></div>
				<div class="preloader-img pere-text">
					<img src="{{asset('assets/bloqueo/loder.png')}}" alt="">
				</div>
			</div>
		</div>
	</div>

   
    <div id="container" class="cls-container login-register">
        <div id="load"></div>
		<!-- BACKGROUND IMAGE -->
		<!--===================================================-->
		<div id="bg-overlay"></div>
		<div class="cls-content">
		    <div class="cls-content-sm panel">

				<div class="" align="center">
					<div class="">
						<div class="col-md-12" align="center" style="vertical-align: middle; background-color: #004789 !important;">
							<a class="img-med" style="padding-top:10px; padding-bottom:10px; text-align: center;">
                            <img style="padding-top:15px; padding-bottom:15px; height: 80px; text-align: center; alignment-adjust: middle;" 
                            src="{{asset('assets/bloqueo/LOGO_MUNI.png')}}"></a>
						</div>
					</div>
				</div>
				<div class="white-box">
					<div class="panel-body">
						<div class="mar-ver pad-btm">
						
							<div class="hidden-xs">
								<br>
							</div>
						
							<h1 class="h3">BOLSA LABORAL</h1>
                            <h3 class="h3">Sistema de Bolsa de Empleo Municipal</h3>
						</div>
						<form action="https://apl1.munlima.gob.pe/helpdesk" novalidate="novalidate">
							<div class="form-group just">
								Estimado usuario, usted no está autorizado a acceder al módulo. Favor de enviar un correo a <b>infraestructura@munlima.gob.pe</b> para gestionar el acceso o para continuar dar click <a href="aqui pones la url que cierre todas las variables de sesión de bolsa laboral y que retorne como último paso a https://calidadaplicativos.munlima.gob.pe/salir amarrar https://calidadaplicativos.munlima.gob.pe a una variable de entorno .env">aquí</a>
							</div>
						</form>
					</div>
				</div>
		        
		    </div>
		</div>		
    </div>
    
</body></html>