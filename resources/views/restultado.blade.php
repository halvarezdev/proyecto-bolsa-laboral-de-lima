@extends('layouts.app')

@section('content')
@if($inicio == "")
<style>
/*.container{
    max-width: 800px;
}*/
</style>
@endif
    <style>
        .border-lista{
            border-left: 4px solid #00A39C;
        }
        .border-lista:hover{
           /* -webkit-box-shadow: 1px 1px 20px -3px rgba(0,164,211,1);
            -moz-box-shadow: 1px 1px 20px -3px rgba(0,164,211,1);
            box-shadow: 1px 1px 20px -3px rgba(0,164,211,1);
            */
            border-top: 2px solid #00A39C;
            border-right: 1px solid #00A39C;
            border-bottom: 1px solid #00A39C;

        }
        .display_none{
            display:none;
        }
        .display_true{
            display:block;
        }

.hero-body{
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
}

.form{
  display: flex;
}

.input-busqueda{
  width: 400px;
  border-radius: 4px 0px 0px 4px;
  padding: 5px;
  border: 0;
  outline: none;
}

button{
  border-radius: 0px 4px 4px 0px;
  border: 0;
  padding: 7px 15px 7px 15px;
  cursor: pointer;
  color:#fff;
  background:#00A39C;

}

button:hover{
  color: #ffffff;
  background: #00A39C;

}
.search::placeholder{
 /* font-weight: bold;*/
  font-size:14px;
  color: {{ $entidad_color }};
}
.display-none{
    display:none;
}

p {
    margin-top: 0;
    margin-bottom: 0;
}

.searchjob .text-muted b{color:#3850A0}

.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #00A39C;
    border-color: #00A39C;
}
.page-link {
    color: #00A39C;
}
    </style>

   <div class="container searchjob">
   <br>
   <form id="form_busqueda" name="form_busqueda" method="get" action="{{ route('users.busqueda.inicio', ['e' => $entidad]) }}">

<br>

   <div class="row" style="padding-top: 15px;padding-bottom: 10px;border-radius: 15px;box-shadow: 0 0 20px #d8d8d8;">
        <div class="col-lg-3 col-md-4 col-sm-12">
            <div class="card" style="<?php  if($tipo_convocatoria == '1' || $tipo_convocatoria == '2'){ echo 'display:none;'; } ?>">
                <div class="card-header">
                <b style="font-size: 16px;">TIPO</b>
                </div>
                <div class="card-body">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input tipo_convocatoria" id="tipo_convocatoria" name="tipo_convocatoria" value="1" onclick="enviarBusquedaFomr()" <?php if(isset($tipo_convocatoria) && $tipo_convocatoria == '1'){ echo "checked"; } ?>>
                        <label class="custom-control-label" for="tipo_convocatoria">Privadas</label>
                     </div>
                     <div class="custom-control custom-checkbox mr-sm-2 tipo_convocatoria">
                        <input type="checkbox" class="custom-control-input" id="tipo_convocatoria_2" name="tipo_convocatoria" value="2" onclick="enviarBusquedaFomr()">
                        <label class="custom-control-label" for="tipo_convocatoria_2">Publicas</label>
                     </div>
                </div>
            </div>

            <div class="card" style="<?php  if(isset($descripcion) &&  $descripcion != ''){ echo 'display:none;'; } ?>">
                <div class="card-header">
                    <b style="font-size">PUESTO / PALABRA CLAVE</b>
                </div>

                <div class="card-body">
                <input type="text" class="form-control descripcion" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{ $des }}"  onkeyup = "if(event.keyCode == 13) return enviarBusquedaFomr()">
                </div>
            </div>
            <div class="card" style="<?php  if(isset($empresa) &&  $empresa != ''){ echo 'display:none;'; } ?>">
                <div class="card-header">
                    <b style="font-size: 16px;">EMPRESA / INSTITUCIÓN</b>
                </div>
                <div class="card-body">
                    <input type="text" class="form-control empresa" id="empresa" aria-describedby="empresaHelp" placeholder="Empresa" name="empresa" value="{{ $empresa }}" onkeyup = "if(event.keyCode == 13) return enviarBusquedaFomr()">
                </div>
            </div>

            <!-- CARD FILTRO POR UBIGEO -->
             <div class="card" style="<?php  if(isset($ubicacion) &&  $ubicacion != ''){ echo 'display:none;'; } ?>">
                <div class="card-header">
                    <b style="font-size:16px;">UBICACION</b>
                </div>
                <div class="card-body">
                    <div class="table-responsive" style="height: 200px;">
                        @foreach($ubigeo as $ubi)
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input ubicacion" id="ubicacion{{ $ubi->ubi_coddpto }}" name="ubicacion" value="{{ $ubi->ubi_coddpto }}"  onclick="enviarBusquedaFomr()" {{ $ubicacion ==  $ubi->ubi_coddpto ? 'checked' : ''}}>
                            <label class="custom-control-label" for="ubicacion{{ $ubi->ubi_coddpto }}" style="font-size:13px;">{{ $ubi->ubi_descripcion }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
             </div>

            <!-- CARD FILTRO CATEOGORIAS - AREAS -->

            <div class="card" style="<?php  if(isset($categoria) &&  $categoria != ''){ echo 'display:none;'; } ?>">
                <div class="card-header">
                    <b style="font-size: 16px;">CATEGORIA</b>
                </div>
                <div class="card-body">
                       <div class="form-group">
                       <div class="table-responsive" style="height: 200px;">
                        @foreach($areas as $key => $area)
                            <div>
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input categoria" id="cat{{ $area->id }}" name="categoria" value="{{ $area->id }}" <?php if(isset($categoria) && $categoria == $area->id){ echo "checked";} ?> onclick="enviarBusquedaFomr()">
                                    <label class="custom-control-label" for="cat{{ $area->id }}" style="font-size:13px;">{{ $area->area_descripcion }}</label>
                                </div>
                            </div>
                        @endforeach
                      </div>
                    </div>
                </div>
            </div>

            <!-- CARD TIPO CONTRATO FILTRO  -->

            <div class="card" style="<?php  if(isset($tipo_contrato) &&  $tipo_contrato != ''){ echo 'display:none;'; } ?>">
                <div class="card-header">
                    <b style="font-size: 16px;">TIPO CONTRATO</b>
                </div>
                <div class="card-body">
                    <div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input tipo_contrato" id="tipo_contrato1" name="tipo_contrato" value="1" {{ (isset($tipo_contrato) && $tipo_contrato == "1" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                            <label class="custom-control-label" for="tipo_contrato1" style="font-size:13px;">Indefinido</label>
                        </div>
                    </div>
                    <div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input tipo_contrato" id="tipo_contrato2" name="tipo_contrato" value="2" {{ (isset($tipo_contrato) && $tipo_contrato == "2" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                            <label class="custom-control-label" for="tipo_contrato2" style="font-size:13px;">Plazo fijo (Determinado)</label>
                        </div>
                    </div>
                    <div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input tipo_contrato" id="tipo_contrato3" name="tipo_contrato" value="3" {{ (isset($tipo_contrato) && $tipo_contrato == "3" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                            <label class="custom-control-label" for="tipo_contrato3" style="font-size:13px;">Tiempo parcial</label>
                        </div>
                    </div>
                    <div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input tipo_contrato" id="tipo_contrato4" name="tipo_contrato" value="4" {{ (isset($tipo_contrato) && $tipo_contrato == "4" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                            <label class="custom-control-label" for="tipo_contrato4" style="font-size:13px;">Practica pre-profesionales</label>
                        </div>
                    </div>
                    <div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="tipo_contrato5" name="tipo_contrato" value="5" {{ (isset($tipo_contrato) && $tipo_contrato == "5" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                            <label class="custom-control-label" for="tipo_contrato5" style="font-size:13px;">Practica profesionales</label>
                        </div>
                    </div>
                </div>
            </div>

        
            <div class="card" style="<?php  if(isset($turno_trabajo) &&  $turno_trabajo != ''){ echo 'display:none;'; } ?>">
                <div class="card-header">
                <b style="font-size: 16px;">TURNO TRABAJO</b>
                </div>
                <div class="card-body">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input turno_trabajo" id="turno_trabajo1" name="turno_trabajo" value="1" {{ (isset($turno_trabajo) && $turno_trabajo == "1" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                        <label class="custom-control-label" for="turno_trabajo1" style="font-size:13px;">Fijo</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input turno_trabajo" id="turno_trabajo2" name="turno_trabajo" value="2" {{ (isset($turno_trabajo) && $turno_trabajo == "2" ? "checked" : "") }} onclick="enviarBusquedaFomr()">
                        <label class="custom-control-label" for="turno_trabajo2" style="font-size:13px;">Rotativo</label>
                    </div>
                </div>
            </div>
           </form>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-12">
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-body">
                <blockquote class="blockquote">
                    <p class="mb-0">Mostrando {{ $cantidad }} vacantes de empleo.</p>
                </blockquote>

                    @if($tipo_convocatoria == '1')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_convocatoria')">Privadas&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @elseif($tipo_convocatoria == '2')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_convocatoria')">Publiicas&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($empresa) && $empresa != '')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('empresa')">{{ $empresa }}&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($categoria) && $categoria != '')
                    @foreach($areas as $key => $area)
                    @if($area->id == $categoria)
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('categoria')">{{ $area->area_descripcion }}&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @endforeach
                    @endif
                    @if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato == '1')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_contrato')">Indifinido&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato == '2')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_contrato')">Plazo fijo&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato == '3')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_contrato')">Tiempo parcial&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato == '4')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_contrato')">Practica pre-profesionales&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato == '5')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('tipo_contrato')">Practica profesionales&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($turno_trabajo) && $turno_trabajo != '' && $turno_trabajo == '1')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('turno_trabajo')">Fijo&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($turno_trabajo) && $turno_trabajo != '' && $turno_trabajo == '2')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('turno_trabajo')">Rotativo&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($descripcion) && $descripcion != '')
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('descripcion')">{{ $descripcion }}&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @if(isset($ubicacion) && $ubicacion != '')
                    @foreach($ubigeo as $ubi)
                    @if($ubicacion == $ubi->ubi_coddpto)
                    <a href="#" class="badge badge-secondary" onclick="enviarBusquedaFomrDelete('ubicacion')">{{ $ubi->ubi_descripcion }}&nbsp;&nbsp;&nbsp;<span><i class="fas fa-times"></i></span></a>
                    @endif
                    @endforeach
                    @endif
                <form id="form_busqueda_delete" method="get" action="{{ route('users.busqueda.inicio', ['e' => $entidad]) }}">
                    <input type="hidden" name="tipo_convocatoria" value="" >
                    <input type="hidden" name="empresa" value="" >
                <form>

                </div>
            </div>
            @foreach($pedidos as $ped)
                <div class="card border-lista" style="margin-bottom: 10px;">
                @if($ped->procedencia == '1')
                <a href="{{ route('vacante.detalle',['id' => $ped->codigo, 'e' => $entidad]) }}" style="text-decoration: none;">
                @else
                <a href="{{ route('vacante.detalle.publica',['id' => $ped->codigo, 'e' => $entidad]) }}" style="text-decoration: none;">
                @endif
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-2">
                                <img src="{{ asset('public/storage/'.$ped->perfil_foto) }}" class="img-thumbnail" alt="..."  style="border:solid 1px #00A39C; width:60px; height:60px;">
                            </div>
                            <div class="col-12 col-sm-6 col-md-7" style="padding-left: 0px;">
                                <small class="text-muted"><b>{{ str_limit(mb_strtoupper($ped->razon_social,'UTF-8'),50) }}</b></small><br>
                                <b style="color:black;">{{ str_limit(mb_strtoupper($ped->descripcion,'UTF-8'),50) }}</b>
                                <footer class="blockquote-footer" style="padding-left: 0px;"><cite title="Source Title" >{{ str_limit($ped->otro_conocimiento,120) }}</cite></footer>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3" style="border-left:solid 1px #00A39C;">
                                <b style="font-size:11px; color:black;">Fecha:</b>
                                <small class="text-muted" style="font-size:11px;">{{ date_format($ped->created_at, 'd/m/Y') }}</small><br>

                                <b style="font-size:11px; color:black;">Lugar:</b>

                                <small class="text-muted" style="font-size:11px;">{{ strtolower($ped->ubi_descripcion) }}</small><br>
                                @if($ped->procedencia == '1')
                                <b style="font-size:11px; color:black;">Categoria:</b>
                                @else
                                <b style="font-size:11px; color:black;">Regimen:</b>
                                @endif

                                <small class="text-muted" style="font-size:11px;">{{ str_limit($ped->area_descripcion,10) }}</small>
                            <!--    <footer class="blockquote-footer"><cite title="Source Title"><a href="{{ route('vacante.detalle',['id' => $ped->id] ) }}">Ver Detalle</a></cite></footer>-->
                            </div>
                        </div>
                    </div>
                </a>
                </div>
            @endforeach

            <div class="row justify-content-md-center">
                    <div class="col-6">
                        {{ $pedidos->appends(Request::only('tipo_convocatoria','descripcion','empresa','ubicacion','categoria','tipo_contrato','turno_trabajo'))->links() }}
                    </div>
                </div>
        </div>

   </div>


</form>
   </div>
   <br>

@include('maestro/footer_usuario')
<script>
function enviarBusquedaFomr(){
    //console.log(animals.push('cows'));

    $( "#form_busqueda" ).submit();
}

function enviarBusquedaFomrDelete(name){
    if(name == 'empresa'){
        $("."+name).val('');
    }else if(name == 'descripcion'){
        $("."+name).val('');
    }else{
        $("."+name).prop("checked", false);
    }

    $( "#form_busqueda" ).submit();
}
function verMasArea(){
    $(".area_mas").removeClass("display_none");
    $(".area_mas").addClass("display_true");
    $("#area_ver_mas").addClass('display_none');
    $("#area_ver_menos").removeClass('display_none');
    $("#area_ver_menos").addClass('display_true');
   // document.getElementById('area_mas').style="display:block";
}

function verMenosArea(){
    $(".area_mas").removeClass("display_true");
    $(".area_mas").addClass("display_none");
    $("#area_ver_mas").addClass('display_true');
    $("#area_ver_menos").removeClass('display_true');
    $("#area_ver_menos").addClass('display_none');
}


</script>
@endsection
