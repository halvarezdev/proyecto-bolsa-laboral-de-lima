@extends('layouts.app')
@section('content')
@include('maestro.cabezera')
@if($inicio == "")
<style>
.container{
    max-width: 800px;
}

</style>
@endif
<div class="container " id="Text_Formt">
<br>
<h3 style="font-family: Galano Grotesque Black;">CONSULTAS</h3>
<br>
<b>¿Cómo funciona?</b>
<br>
<p>Permite vincular a las personas que buscan trabajo y las empresas que buscan trabajadores a través de la plataforma digital <a href="www.ptepiedra.com.pe" target="_blank"
>www.ptepiedra.com.pe</a> </p>

<p>Permite a las empresas publicar sus ofertas laborales y buscar candidatos para dichas vacantes. Para ello sólo necesitas registrarte completando el formulario que ponemos a tu disposición.</p>

<p>Asimismo, permite a las personas postular a las ofertas laborales enviando su currículum. Para tener acceso a las ofertas laborales que se publican sólo necesitas registrarte completando el formulario que ponemos a tu disposición.</p>

<b>¿Interfiere en el proceso de selección?</b>

<p>Esta plataforma posibilita que las empresas publiquen sus avisos de trabajo y las personas interesadas en postular a estos avisos envían sus currículums a las empresas. La Municipalidad puede brindar orientación a las empresas y a las personas para su registro, así como para el manejo de la plataforma.</p>

<b>¿Cómo puedo crear mi currículum?</b>

<p>Simplemente completando los formularios luego de registrarte. Puedes actualizarlo o modificarlo en cualquier momento ingresando con tu correo electrónico y contraseña.  </p>

<p>Si ya tienes un currículum en formato Word o pdf, también puedes adjuntarlo para que las empresas lo revisen.</p>

<b>Ya estaba registrado ¿cómo puedo ingresar ahora?</b>

<p>Si tienes una cuenta puedes ingresar aquí, sólo debes colocar tu correo electrónico y tu contraseña que creaste al registrarte. Después de ingresar en tu cuenta podrás completar tu currículum y postularte a las ofertas de trabajo.</p>

<b>Olvidé mi contraseña, ¿cómo puedo acceder?</b>

<p>Si has olvidado tu contraseña puedes solicitar su recuperación haciendo click <a href="/users/password/reset">aquí</a>.</p>

<b>¿Cómo puedo postular a las vacantes de empleo?</b>

<p>Para postular a las ofertas laborales que publican las empresas debes tener una cuenta activa y tu currículum completado. Una vez que tengas ello podrás postular a las ofertas laborales con un solo click en el botón “Postular” que encontrarás en cada detalle de la vacante.</p>

<b>¿Cómo puedo ver si la empresa ha recibido mi postulación correctamente?</b>

<p>Dentro de tu cuenta, en la sección “Mis Postulaciones”, podrás visualizar todas las ofertas a las que te has inscrito. Sabrás que una empresa ha recibido tu currículum si aparece en este listado.</p>

<p>¿Dónde puedo ver cómo avanzan los procesos de selección en las ofertas a las que me he postulado?</p>

<p>Dentro de tu cuenta, en la sección “Mis Postulaciones”, se indica el estatus en el que se encuentra tu postulación. Podrás saber si la empresa está seleccionando los currículums, si ya está contactando con los candidatos o si el proceso de selección ya ha finalizado.</p>

<p>Recuerda que la Municipalidad no tiene injerencia en los procesos de selección de las empresas.</p>

<b>¿Cuántas veces puedo postularme a la misma oferta laboral?</b>

<p>Solo se puede postular una vez a cada oferta laboral. No podrás postularte nuevamente a una misma oferta laboral, aunque elimines la postulación anterior y tampoco es posible recuperarlas; por ello, te sugerimos estar completamente seguro antes de registrarte o abandonar una postulación.</p>

<b>¿Por qué no veo el correo electrónico de la empresa para enviar mi currículum?</b>

<p>Nos hemos adaptado a las necesidades de las empresas, de esta forma las empresas pueden gestionar muchos más currículums de manera más rápida. El candidato puede estar seguro que al postularse a una oferta las empresas reciben inmediatamente su currículum.</p>

<b>¿Cómo puedo contactar con las empresas, si no veo el correo electrónico de la empresa?</b>

<p>Al postularse a una oferta laboral, ya estás contactando con la empresa. A partir de ese momento la empresa contactará contigo si está interesada en tu candidatura.</p>

<b>¿De qué manera las empresas pueden contactar conmigo?</b>

<p>Las empresas pueden contactar contigo utilizando los datos de contacto que dejaste al registrarte. Te sugerimos que introduzcas datos de contacto correctos y que estén activos, esto te ayudará a ser contactado con mayor facilidad.</p>

<b>¿Puedo eliminar mi candidatura de una oferta laboral?</b>

<p>Si lo puedes hacer desde la sección “Mis Postulaciones”. Desde este listado tienes la opción de eliminar tu candidatura de una determinada oferta laboral. Recuerda que, si eliminas una candidatura, no podrás volver a postularse a la misma oferta laboral.</p>

<b>¿Quién puede ver mi currículum?</b>

<p>Hay dos formas en las que las empresas pueden ver tu currículum. Una de ellas es porque te has postulado a una de sus ofertas o bien porque la empresa ha encontrado tu perfil haciendo una búsqueda de currículums.
Si no deseas que tu currículum sea visible para las empresas a cuyas ofertas no te has postulado, debes cambiar la privacidad de tu currículum desde la sección “Configuración” dentro de tu cuenta.</p>

<b>¿Qué datos de mi currículum ven las empresas?</b>

<p>Las empresas pueden visualizar todos los datos que has introducido al registrar tu currículum. Si modificas tu currículum la empresa siempre verá la última actualización, incluso en las ofertas laborales a las que te has postulado previamente.</p>

<b>¿Por qué las empresas no contactan conmigo?</b>

<p>No podemos darte información específica de cada proceso de selección. Los motivos más comunes para no ser seleccionado pueden ser fallos de redacción en el currículum enviado en tus postulaciones, falta de adecuación a la vacante publicada, etc. </p>

<p>Recuerda que la Municipalidad no gestiona directamente los procesos de selección de las empresas.</p>

<b>¿Cómo puedo actualizar o modificar mi currículum?</b>

<p>Puedes realizar todos los cambios que creas pertinentes desde la sección “Mi Currículum”.
Las modificaciones que apliques a tu perfil serán también visibles para las empresas a las que te has postulado, por lo que las empresas siempre verán la versión más actualizada de tu currículum, incluso si han transcurrido días desde tu postulación.</p>

<b>¿Cómo puedo cambiar mis datos de acceso?</b>

<p>Puedes modificar los datos de acceso desde la sección “Configuración” dentro de tu cuenta o recuperarlo haciendo click aquí.</p>

<b>¿Cómo puedo cargar mi CV en formato Word o PDF?</b>

<p>Puedes cargar tu currículum en formato Word o PDF desde el enlace “Adjuntar CV” de tu cuenta. Si no te permite cargar el archivo, asegúrate de estar utilizando la versión más actualizada de tu navegador de internet.</p>

<b>¿Cómo puedo eliminar mi cuenta?</b>

<p>Puedes eliminar tu cuenta desde la sección “Configuración” de tu cuenta. Una vez allí, deberás dirigirte a “Eliminar currículum” y hacer clic en el botón “Eliminar”, con esto darás de baja a tu cuenta de la plataforma digital.</p>

</div>

@include('maestro.footer_usuario')
@endsection
