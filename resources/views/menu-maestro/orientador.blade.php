

@extends('layouts.app')

@section('content')

@include('maestro.cabezera')

@if($inicio == "")
<style>
.container{
    max-width: 800px;
}
</style>
@endif

<div class="container" id="Text_Formt">	<img src="{{ asset('fondo/segunda-imagen.png') }}" alt="nosotros" width="100%" >
<br>
<!--<h3 class="mt-3" style="font-family:Galano Grotesque Black;">NOSOTROS</h3><hr>-->
    <h4  class="mt-3" >¿QUIENES SOMOS?</h4>

    <p align="justify" >El Callao fue fundada por los colonizadores españoles en 1537, sólo dos años después de Lima (1535). Pronto se convirtió en el principal puerto para el comercio español en el Pacífico. El origen de su nombre es desconocido, tanto la India (especialmente Yunga, o la costa peruana) y fuentes españolas se acreditan, pero lo cierto es que se le conocía por ese nombre desde 1550.

A la altura del Virreinato, prácticamente todos los bienes producidos en el Perú, Bolivia y Argentina se llevaron a través de los Andes a lomo de mula hasta el Callao, para ser enviado a Panamá, llevado por tierra, y luego transportados a España, vía Cuba.

El 20 de agosto de 1836, durante la Confederación Perú-Boliviana, el presidente Andrés de Santa Cruz dispuso la creación de la Provincia Litoral del Callao (Provincia Litoral del Callao del), que tiene autonomía política en sus asuntos internos. Durante el gobierno del presidente Ramón Castilla, Callao se le dio el nombre de Provincia Constitucional (Provincia Constitucional), el 22 de abril 1857, antes de eso, Callao tenía el nombre de la provincia del Litoral. Todas las otras provincias peruanas habían dado sus nombres por la ley, mientras que el Callao fue dado por mandato constitucional.
</p>
<br>
<h4 >NUESTRO CALLAO</h4>
<figure class="wp-block-image size-large m-4">
<img src="{{ asset('fondo/foto001.jpg') }}" alt="logo-empresa" loading="lazy" width="100%" >
</figure>
<p align="justify" > 
Se construye en y alrededor de la península, el distrito de La Punta, un barrio residencial rico. Una fortaleza histórica, el Castillo de Real Felipe (sitio de «Last Stand Rodil»), se alza sobre un promontorio con vistas al puerto.

Una gran base naval está ubicado en el Callao. Su prisión tiene Abimael Guzmán, líder del movimiento rebelde Sendero Luminoso, y Vladimiro Montesinos, el ex director de seguridad interna durante el régimen de Fujimori.

Aeropuerto Internacional Jorge Chávez está ubicado en el Callao.

En un acantilado con vistas al puerto se encuentra el Colegio Militar Leoncio Prado, el instituto militar. La ciudad también tiene una universidad, la Universidad Nacional del Callao.

El principal hospital Naval, Centro Médico Naval se encuentra en la Avenida Venezuela en Bellavista. Contiene los EE.UU. Marina Prefectura Unidad de Investigación Médica Seis.

Callao tiene varias islas: San Lorenzo (en la actualidad una base militar), El Frontón (antigua prisión de alta seguridad), los Cavinzas y Palominos, donde numerosos leones marinos y aves marinas viven en un ecosistema casi virgen. Hay planes propuestos para construir un gran puerto marítimo, terrestre y aéreo en la Isla San Lorenzo. Este proyecto recibe el nombre de San Lorenzo Megapuerto Proyecto. Los residentes del Callao se conocen como chalacos.

Equipos profesionales de fútbol son Callao Sport Boys y Atlético Chalaco.

</p>




<div class="container">
  <div class="row">
    <div class="col-lg-6 col-sm-6 col-12 mt-2"><img src="{{ asset('fondo/foto002.png') }}" alt="logo-empresa" loading="lazy"  class="img-thumbnail  rounded "></div>
    <div class="col-lg-6 col-sm-6 col-12 mt-2"><img src="{{ asset('fondo/foto003.png') }}" alt="logo-empresa" loading="lazy"  class="img-thumbnail  rounded "></div>
    <div class="col-lg-6 col-sm-6 col-12 mt-2"><img src="{{ asset('fondo/foto004.png') }}" alt="logo-empresa" loading="lazy"  class="img-thumbnail  rounded "></div>
    <div class="col-lg-6 col-sm-6 col-12 mt-2"><img src="{{ asset('fondo/foto005.png') }}" alt="logo-empresa" loading="lazy"  class="img-thumbnail  rounded "></div>
  </div>
</div>


</div>

@include('maestro.footer_usuario')

@endsection
