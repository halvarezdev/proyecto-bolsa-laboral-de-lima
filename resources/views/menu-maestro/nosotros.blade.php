

@extends('layouts.app')

@section('content')
<br><br>
<div >	
<style>
    @media only screen
and (min-device-width: 736px){
    .div_fondo_nostros{
        background-image: url("{{ asset('fondo/portadanosotros.png') }}");
        height: 450px;
        background-repeat: no-repeat;
        background-size:100% 100%;
    }
    .div_quienes_somos{
        padding-top: 20%;
        padding-left: 25%;
    }
}
</style>
<br>
<div class="container">
    <center>
    <img src="{{ asset('images/nosotrosicon.png') }}" alt="nosotros" style="width: 100%;">
    <br>
    <h2 style="color:#C73C63; font-size:40px"><b>Nosotros</b></h2>
    </center>
</div>

    <div class="div_fondo_nostros" style="width: 100%;">
    <div class="row" style="width: 100%;">
        <div class="col-sm-12 col-md-5">
            <div class="div_quienes_somos">
                <div class="container">
                    <h4   style="color:#C73C63">
                    <b style="font-size: 26px;">¿QUIENES SOMOS?</b>
                    </h4>

                    <p align="justify" style="font-size: 20px;">
                    <b>Somos un equipo que busca insertar
                    en el mundo laboral</b> a ciudadanos de la
                    ciudad. Acercando ofertas y oportunidades
                    laborales de gran demanda a distintos perfiles
                    profesionales.. 
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@include('maestro.footer_usuario')

@endsection
