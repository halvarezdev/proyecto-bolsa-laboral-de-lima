@extends('layouts.app')

@section('content')
<style type="text/css">
    .form-control:hover{
        border:1px black solid;
    }
    .nav-tabs .nav-link.active{
      background-color:#007bff;
      style="color:#fff !important;"
    }
    .normal{
      color:#b2b2b2;
    }
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
      color: #fff;
      border-color: #dee2e6 #dee2e6 #003dc7;
  }
</style>

<?php 
$submenu = "1";
?>
<div class="d-none d-sm-none d-md-block">
@include('maestro/submenu')
</div>

<br>

<div class="container pb-4">

                <div class="row">
                    
                    <div class="col"> 
                        <div class="row">
                           
                            <div class="card " style="width:100%;min-height: 650px;" >
                                <div class="card-header degrade-griss" style="border: none; padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 25px;">
                                  <div class="row">
                                    <div class="col">
                                      <h5 style="text-align:left; color:#FFF;">Listado de Publicaciones</h5> 
                                    </div>
                                  </div>
                                    
                                </div>
                                <div class="card-body" style="padding-top: 25px;">

  <div class="nav nav-tabs " id="nav-tab" role="tablist">
    <a href="{{ route('contacto.dashboard',['state' => '1']) }}" class="nav-item nav-link {{ ($state == "1" ? 'active' : "normal") }} redondeado" style="padding-left: 20px;padding-right: 20px;">
    <?php
    if($state == "1"){
      ?>
      <img src="{{ asset('animacion/activas1.png') }}" width="15">&nbsp; &nbsp;Activas
      <?php
    }else{
      ?>
      <img src="{{ asset('animacion/activas0.png') }}" width="15">&nbsp; &nbsp;Activas
      <?php
    }
    ?>

    </a>
    <a href="{{ route('contacto.dashboard',['state' => '0']) }}" class="nav-item nav-link {{ ($state == "0" ? 'active' : "normal") }} redondeado" style="padding-left: 20px;padding-right: 20px;">
    <?php
    if($state == "0"){
      ?>
      <img src="{{ asset('animacion/final1.png') }}" width="15">&nbsp; &nbsp;Finalizadas
      <?php
    }else{
      ?>
      <img src="{{ asset('animacion/final0.png') }}" width="15">&nbsp; &nbsp;Finalizadas
      <?php
    }
    ?>
    </a>
    <a href="{{ route('contacto.dashboard',['state' => '2']) }}" class="nav-item nav-link {{ ($state == "2" ? 'active' : "normal") }} redondeado" style="padding-left: 20px;padding-right: 20px;">
    <?php
    if($state == "2"){
      ?>
      <img src="{{ asset('animacion/pendiente1.png') }}" width="15">&nbsp; &nbsp;Pendientes
      <?php
    }else{
      ?>
      <img src="{{ asset('animacion/pendiente0.png') }}" width="15" >&nbsp; &nbsp;Pendientes
      <?php
    }
    ?>
    </a>
    <a href="{{ route('contacto.dashboard',['state' => '3']) }}" class="nav-item nav-link {{ ($state == "3" ? 'active' : "normal") }} redondeado" style="padding-left: 20px;padding-right: 20px;">
    <?php
    if($state == "3"){
      ?>
      <img src="{{ asset('animacion/borradores1.png') }}" width="15">&nbsp; &nbsp;Borradores
      <?php
    }else{
      ?>
      <img src="{{ asset('animacion/borradores0.png') }}" width="15" >&nbsp; &nbsp;Borradores
      <?php
    }
    ?>
    </a>
  </div>
</nav>



<br>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

    <div class="row">
    <div class="col-12 col-sm-12 col-md-4">
    <form action="{{ route('contacto.dashboard') }}" method="get">
      <div class="form-row align-items-center">
        
      <div class="col-8 my-1" style="padding-right: 0px;">
          <label class="sr-only" for="descripcion_input">Descripcion</label>
          <input type="hidden" value="{{ $state }}" name="state">
          <input type="text" class="form-control form-control-sm" id="descripcion_input" name="descripcion_input" placeholder="Descripcion" value="{{ $descripcion_input }}" style="border:solid 1px {{ (isset($entidad_color) ? $entidad_color : '') }};border-top-left-radius: 0.5rem;border-top-right-radius: 0rem;border-bottom-right-radius: 0rem;border-bottom-left-radius: 0.5rem;border-right: none;">
        </div>
        <div class="col-2 my-1" style="padding-left: 0px;">
          <button type="submit" class="btn btn-secondary" style="background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size: 13px;border-color: {{ (isset($entidad_color) ? $entidad_color : '') }};border-top-left-radius: 0rem;border-top-right-radius: 0.5rem;border-bottom-right-radius: 0.5rem;border-bottom-left-radius: 0rem;" >Buscar</button>
        </div>
      </div>
    </form>
    </div>

    </diV>
      <!-- ininio --><br>
      <div class="table-responsive">
      <table class="table table-sm table-hover">
          <thead style="background:{{ (isset($entidad_color) ? $entidad_color : '') }}">
            <tr>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                N°
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff; width: 35%;">
                  Descripción
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                Vacantes
              </th> 
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                Publicado
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                Finaliza
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                  CVs leidos
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                CVs no leidos
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                Entrevistados
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                Colocados
              </th>
              <th colspan="2" style="text-align: center; font-size: 12PX; color:#fff;">
                Acciones
              </th>
              <th style="text-align: center; font-size: 12PX; color:#fff;">
                Gestión
              </th>
            </tr>
        </thead>
        @foreach($pedidos as $key => $pedido)
        <tbody>
          <tr>
            <td>
              <input type="hidden"  id="id_codigo{{$pedido->id}}" value="{{ route('contacto.publicar',['id' => $pedido->id]) }}">
              <a href="{{ route('vacante.detalle',['id' => $pedido->id,'e' => $entidad] ) }}" style="color:black">{{ $pedido->id }}</a>
            </td>
            <td>
              <input type="hidden" name="descri" id="descri" value="{{ $pedido->descripcion }}">
              <a href="{{ route('vacante.detalle',['id' => $pedido->id,'e' => $entidad] ) }}" style="color:black">{{ $pedido->descripcion }}</a>
            </td>
            <td style="text-align: center;">
              <span class="badge badge-primary badge-pill" style="background-color:{{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}">{{ $pedido->num_vacantes }}</span>
            </td>
            <td style="text-align: center;">
            <?php
              $startTime   = strtotime($pedido->fechcrea_pedido);
              $time = date("Y-m-d H:i:s",$startTime);
            ?>
            {{ date( "d/m/Y", strtotime( $time) + 5 * 3600 ) }}                                     
            </td>
            <td style="text-align: center;">
            <?php
              $startTime   = strtotime($pedido->fechlimit_pedido);
              $time = date("Y-m-d H:i:s",$startTime);
            ?>
            {{ date( "d/m/Y", strtotime( $time) + 5 * 3600 ) }}
            </td>
                                                 
            <td style="text-align: center;">
              {{ $pedido->total_vistos }}
            </td>
            <td style="text-align: center;">
              {{ $pedido->total_novisto }}
            </td>
            <td style="text-align: center;">
              {{ $pedido->total_entrevista }}
            </td>
            <td style="text-align: center;">
                {{ $pedido->total_contra }}
            </td>

            <td style="text-align: center;">
                @if($pedido->flag_estado == "2")
              <!-- <a href="#"   rel="Publicar Pedido" data-toggle="modal" data-target="#publicar" onclick="return EliminarPedidoPostulante('{{ $pedido->id }}')">-->
              <i class="fas fa-check"></i>
            <!--   </a> -->
              @endif
              @if($pedido->flag_estado == "1")
              <a href="#"   rel="Finalizar Publicación" data-toggle="modal" data-target="#cerrarpedido" onclick="return CancelarPedidoPostulante('{{ $pedido->id }}')">
              <i class="fas fa-power-off" style="color:{{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}"></i>
                </a>
              @endif
              @if($pedido->flag_estado == "0")
                <p   rel="Pedido Cerrado">
                   <i class="fas fa-times-circle"></i>
              </p>
              @endif
          </td>
          <td style="text-align: center;">
            @if($pedido->flag_estado == "0")
                <p   rel="No Editable">
                  <i class="fas fa-pencil-alt" style="color:#A4A4A4"></i>
              </p>
              @else
              <a href="{{ route('contacto.editar',['id' => $pedido->id] ) }}"   rel="editar" >
                <i class="fas fa-pencil-alt" style="color:{{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}" data-placement="left" data-toggle="tooltip"></i>
              </a>
              @endif
              
          </td>
          <td style="text-align: center;">
            
          <a   href="{{asset('matching/seguimiento/'.$pedido->id.'?empresa='.$pedido->empresa_id.'&contacto='.Auth::user()->id.'') }}"  rel="Buscar Postulantes">
            <img src="{{ asset('animacion/gestion.png') }}" width="22">
          </a>
               
        </td>
           </tr>
     </tbody>
            @endforeach
  </table>
    </div>
        <div class="row justify-content-md-center">
            <div class="col-xs-12"> 
                {{ $pedidos->links() }}
            </div>
        </div>          
      <!-- fin-->
  
  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab"></div>
  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab"></div>
</div>
    <br>
                                        
                                       
                                </div>
                        </div>
                        </div>


                        <!---->
                        
                    </div>
                   
                </div>
            
            </div>
        </div>
</div>        




<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="publicar" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
         ¿Esta Seguro de publicar?
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="#" class="btn btn-success" id="btnEliminar" onclick="return publicarPedido()">Publicar</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
  </div>
</div>



<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="cerrarpedido" aria-labelledby="cerrarpedido" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form method="post" action="{{ route('vacante.cierre') }}">
            <div class="modal-header">
                <p>CERRAR PEDIDO</p>
            </div>
            <div class="modal-body">
             @csrf
                <div class="container">
                      <div class="form-group row justify-content-md-center">
                        <label class="col-sm-2 col-form-label">Descripcion</label>
                        <div class="col-sm-6">
                            <input type="hidden" name="codigo" id="codigo">
                          <input type="text" class="form-control" id="descripcion_popup" disabled>
                        </div>
                      </div>
                      <div class="form-group row justify-content-md-center">
                        <label class="col-sm-2 col-form-label">Fecha</label>
                        <div class="col-sm-6">
                            <?php
                            $hoy = date('d/m/Y');
                            ?>
                          <input type="text" class="form-control" id="fechacierre" name="fechacierre" value="{{ $hoy }}" disabled>
                        </div>
                      </div>
                      <div class="form-group row justify-content-md-center">
                        <label class="col-sm-2 col-form-label">Motivo de Cierre</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="motivocierre">
                                <option value="">Seleccione</option>
                                <option value="1">Se cubrio la vacante</option>
                                <option value="2">Vacante quedo en stand By</option>
                                <option value="3">Los postulantes no cumplen con el perfil</option>
                                <option value="4">Los postulantes no se presentaron a la entrevista</option>
                                <option value="5">Vacante se cancelo</option>
                            </select>
                        </div>
                      </div>
                </div>
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
              <button class="btn btn-danger" id="btn" type="submit">Cerrar Pedido</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </form>
   
    </div>



<script type="text/javascript">
     function EliminarPedidoPostulante(tele) {
        ruta = $("#id_codigo"+tele).val();
  // alert(tele)
   // var tds = lnk.parentNode.parentNode.id;
 //   $("#btnEliminar").attr('onclick', 'EliminarPedidoPostulante2('+id+')');
    $("#btnEliminar").attr('href', ruta);
  }

  function CancelarPedidoPostulante(id){
    $("#codigo").val(id);
    $("#descripcion_popup").val($("#descri").val());
  }
</script>

<br>


@include('maestro/footer_usuario')
@endsection