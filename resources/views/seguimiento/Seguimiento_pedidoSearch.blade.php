@extends('layouts.app')

@section('content')
<style type="text/css">
    .text{
      font-size: 11px;
    }

    .border-arriba{
      border-top:solid 1px #007bff;
    }

    .titulo-shearh{
        color:#007bff;
        font-size:14px;
    }

    .subtitulo-shearh{
      font-size:12px;
    }

    .select_none{
        border:solid 1px {{ (isset($entidad_color) ? $entidad_color : '') }} !important;
        border-top-left-radius: 0.5rem;
        border-top-right-radius: 0rem;
        border-bottom-right-radius: 0rem;
        border-bottom-left-radius: 0.5rem;
        border-right: none !important;
    }
    .todo-redondedado{
        border:solid 1px {{ (isset($entidad_color) ? $entidad_color : '') }} !important;
        border-top-left-radius: 0.5rem;
        border-top-right-radius: 0.5rem;
        border-bottom-right-radius: 0.5rem;
        border-bottom-left-radius: 0.5rem;
    }
</style>

<?php
if($menu == '1'){
  $submenu = "1";

?>
<div class="d-none d-sm-none d-md-block">
@include('maestro/submenu')
</div>
<?php
}
?>
<form action="{{ route('buscar.postulante', ['pedido' => $pedido ]) }}" method="get" id="frmFormulario">
  <div class="container" style="max-width: 1290px;">
    <br>
    <div class="degrade-griss">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-3" style="padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 40px;">
          <h4 style="color:#fff;">Buscador de CVs</h4>
        </div>
        <div class="col-6 col-sm-6 col-md-3" style="padding-top: 8px;padding-bottom:8px; padding-right: 0px;">
          <select class="selectpicker show-tick form-control form-control-sm select_none" data-live-search="true" data-style="select-style" id="ocupacion" name="ocupacion" data-width="100%" >
            <option value="">Ingrese una palabra clave</option>
            @foreach($ocupaciones as $ocupacion)
            <option value="{{ $ocupacion->ocup_codigo }}" >{{ $ocupacion->nombre }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-4 col-sm-4 col-md-3" style="padding-top: 8px;padding-bottom: 8px; padding-left: 0px;">
          <a onclick="busquedaAjaxTotal()" class="btn btn-secondary" style="border-color:{{ (isset($entidad_color) ? $entidad_color : '') }};color:#fff;background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size: 13px;border-color: none;border-top-left-radius: 0rem;border-top-right-radius: 0.5rem;border-bottom-right-radius: 0.5rem;border-bottom-left-radius: 0rem;padding: .37rem .75rem;" >Buscar</a>
        </div>
        <div class="col-12 col-sm-12 col-md-2" style="padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 40px;">
          <div class="d-none d-sm-none d-md-block">
            <a onclick="regrear()" class="btn btn-secondary" style="border-color:{{ (isset($entidad_color) ? $entidad_color : '') }};color:#fff;background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size: 13px;border-color: none;border-top-left-radius: 0.5rem;border-top-right-radius: 0.5rem;border-bottom-right-radius: 0.5rem;border-bottom-left-radius: 0.5rem;padding: .31rem .75rem;" >Agregar Postulantes</a>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <!-- inicio filtro -->
      <div class="col-12 col-sm-12 col-md-3"> 
        <input type="hidden" name="pedido" id="pedido" value="{{$pedido}}">
        <input type="hidden" name="contacto" id="contacto" value="{{$contacto}}">
        <input type="hidden" name="empresa" id="empresa" value="{{$empresa}}">
        <div class="card">
          <div class="card-header" style="background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}; color:#fff;">
            <h6 style="color:#fff; font-size:16px;">Puesto y Formación Académica</h6>
          </div>
          <div class="card-body">
            <div class="form-row">

              <div class="form-group col-12" style="margin-bottom: 2px;">
              <a onclick="limpiarTodo()" href="#" class="btn btn-info form-control-sm" style="font-size: 11px;background-color: #fb0002;border-color: #fb0002">Limpiar</a>
        
                </div>


                <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Titulo del Puesto:</label> 
             
                  <select class="selectpicker show-tick form-control form-control-sm todo-redondedado" data-live-search="true" data-style="select-style" data-width="100%" id="titulo_puesto" name="titulo_puesto" onchange="return busquedaAjaxTotal()">
                            <option value="">Puesto</option>
                            @foreach($ocupaciones as $ocupacion)
                            <option style="width: 700px;" value="{{ $ocupacion->ocup_codigo }}" >{{ $ocupacion->nombre }}</option>
                            @endforeach
                  </select>
                </div>

                <div class="form-group col-12" style="margin-bottom: 2px;">
                    <label class="subtitulo-shearh">Empresa:</label>
                    <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="empresa_form" name="empresa" placeholder="Empresa" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
                </div>

                <div class="form-group col-md-12">
                    <label class="subtitulo-shearh">Tareas y funciones:</label><br>
                    <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="tareas" name="tareas" placeholder="Ingrese tareas" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
                </div>


                <div class="form-group col-12" style="margin-bottom: 2px;" >
                  <label class="subtitulo-shearh">Institución Educativa:</label>
                  <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="institucion" name="institucion" placeholder="Institución" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
              </div>

              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Profesión:</label>
                  <select class="selectpicker show-tick form-control form-control-sm  todo-redondedado" data-live-search="true" data-style="select-style" data-width="100%" id="profesion" name="profesion" onchange="return busquedaAjaxTotal()">
                    <option value="">Seleccione</option>
                    @foreach($profesiones as $profesion)
                    <option style="width: 700px;" value="{{ $profesion->profp_codigo }}">{{ $profesion->nombre }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Grados Académicos:</label>
                  <select class="form-control form-control-sm selectpicker show-menu-arrow todo-redondedado"  data-style="select-style" data-width="100%" name="gradosAcademicos" id="gradosAcademicos" onchange="return variarEstadoTshearh(this),busquedaAjaxTotal()">
                      <option value="">Seleccione</option>
                      @foreach($grado_intruccion as $value)
                      <option value="{{ $value->id }}">{{ $value->grad_descripcion }}</option>
                      @endforeach
                  </select>
                </div>

                <!--
                <div class="form-group col-12">
                  <label class="subtitulo-shearh">Estado:</label>
                  <select name="estado" id="estado_seleccion" class=" form-control form-control-sm  todo-redondedado" onchange="return busquedaAjaxTotal()">
                    <option value="">Seleccione</option>
                  </select>
                </div>
                -->


                 <!-- pretencion salarial -->

              <div class="form-group col-12 " >
              <label class="subtitulo-shearh">Pretencion Salarial:</label>
                  <select class="selectpicker show-menu-arrow form-control form-control-sm  todo-redondedado" data-style="select-style" data-width="100%" name="salario" id="salario" onchange="return busquedaAjaxTotal()">
                      <option value="">Seleccione</option> 
                      <option value="1" >0 - 930</option>
                      <option value="2">930 - 1200</option>
                      <option value="3">1200 - 1500</option>
                      <option value="4">1500 - 3000</option>
                      <option value="5">3000 - 5000</option>
                      <option value="6">5000 a mas</option>
                  </select>
              </div>

              <!-- idioma -->
                <div class="form-group col-12 ">
                      <label class="subtitulo-shearh">Idioma:</label>
                      <select class="selectpicker show-menu-arrow form-control form-control-sm  todo-redondedado" data-style="select-style" data-width="100%" name="idioma1" id="idioma1" onchange="return busquedaAjaxTotal()"> 
                        <option value="">Seleccione</option>
                        @foreach($idiomas as $value)
                        <option value="{{ $value->MAESP_CODIGO }}">{{ $value->MAESC_DESCRIPCION }}</option>
                        @endforeach
                      </select>
                </div>
              <!-- comptacion -->
                  <div class="form-group col-12 ">
                        <label class="subtitulo-shearh">Computacion:</label>
                        <select class="selectpicker show-menu-arrow form-control form-control-sm  todo-redondedado" data-style="select-style" data-width="100%" name="compu" id="compu" onchange="return busquedaAjaxTotal()"> 
                          <option value="">Seleccione</option>
                          @foreach($computacion as $value)
                          <option value="{{ $value->MAESP_CODIGO }}">{{ $value->MAESC_DESCRIPCION }}</option>
                          @endforeach
                        </select>
                  </div>
            </div>
          </div>
        </div>  
        <div class="card">
          <div class="card-header " style="background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}; color:#fff;">
            <h6 style="color:#fff; font-size:16px;">Información Personal</h6>
          </div>
          <div class="card-body">
            <div class="form-row">
              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Nombre:</label>
                  <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="nombre" name="nombre" placeholder="Nombre" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
              </div>

              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Apellido:</label>
                  <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="apellido" name="apellido" placeholder="Apellido" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
              </div>

              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">DNI:</label>
                  <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="documento" name="documento" placeholder="DNI" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
              </div>
            </div>
            <div class="form-row">
                <div class=" form-group col" >
                <label class="subtitulo-shearh">Edad </label>
                <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="edad_desde" name="edad_desde" placeholder="Desde" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
                </div>
                <div class=" form-group col"><label class="subtitulo-shearh"> ..</label>
                <input type="text" class="form-control form-control-sm limpiar todo-redondedado" id="edad_hasta" name="edad_hasta" placeholder="Hasta" onkeyup="if(event.keyCode == 13) busquedaAjaxTotal()" onblur="return busquedaAjaxTotal()">
                </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                  <label class="subtitulo-shearh">Genero:</label>
                  <select name="sexo" value="" id="sexo"  data-style="select-style" data-width="100%" class="todo-redondedado selectpicker show-menu-arrow form-control form-control-sm " onchange="return busquedaAjaxTotal()">
                    <option value="">Indistinto</option>
                    <option value="1">Masculino</option>
                    <option value="2">Femenino</option>
                  </select>
                </div>
            </div>
            <div class="form-row"> 
                <!--
                <div class="form-group col-12" style="margin-bottom: 2px;">
                    <label class="subtitulo-shearh">Pais:</label>
                    <?php echo \Form::select('pais',$lista_pais,'36',array('Class'=>'form-control form-control-sm selected  todo-redondedado','id'=>'pais','onchange'=>'busquedaAjaxTotal()')) ?>
                </div>
                -->

                <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Departamento:</label>
                  <?php echo \Form::select('deparme',$lista_depa,'0',array('Class'=>'form-control form-control-sm selected  todo-redondedado','id'=>'deparme','onchange'=>'ComboboxProvincia(this)','onchange'=>'busquedaAjaxTotal()')) ?>
                </div>

                <div class="form-group col-12" style="margin-bottom: 2px;">
                  <label class="subtitulo-shearh">Provincia:</label>
                  <?php echo \Form::select('provi',$lista_prov,'0',array('Class'=>'form-control form-control-sm selected  todo-redondedado','id'=>'provi','onchange'=>'ComboboxDistrito(this)','onchange'=>'busquedaAjaxTotal()')) ?>
                </div>

                <div class="form-group col-12">
                  <label class="subtitulo-shearh">Distrito:</label>
                  <?php echo \Form::select('distri',$lista_dist,'0',array('Class'=>'form-control form-control-sm selected  todo-redondedado','id'=>'distri','onchange'=>'busquedaAjaxTotal()')) ?>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-12" style="margin-bottom: 2px;">
                    <label class="subtitulo-shearh">¿Tiene licencia de conducir?</label>
                    <select class="form-control form-control-sm  selectpicker todo-redondedado" data-style="select-style" name="licenciaConduc" id="licenciaConduc" onchange="return busquedaAjaxTotal()">
                        <option value="">Seleccione</option> 
                        <option value="2">No</option>
                        <option value="1">Si</option>
                    </select>
                </div>

                <div class="form-group col-12" style="margin-bottom: 2px;">
                    <label class="subtitulo-shearh">Categoria:</label>
                    <select class="form-control form-control-sm  selectpicker todo-redondedado" data-style="select-style" name="categoria" id="categoria" onchange="return busquedaAjaxTotal()">
                        <option value="">Seleccione</option> 
                        <option value="1">A-I</option>
                        <option value="2">A-IIa</option>
                        <option value="3">A-IIb</option>
                        <option value="4">A-b</option>
                    </select>
                  </div>

                    <div class="form-group col-12" style="margin-bottom: 2px;">
                      <label class="subtitulo-shearh">¿Tiene persmiso para portar armas?</label>
                      <select class="form-control form-control-sm  selectpicker todo-redondedado" data-style="select-style" name="port_armas" id="port_armas" onchange="return busquedaAjaxTotal()">
                          <option value="">Seleccione</option> 
                          <option value="2">No</option>
                          <option value="1">Si</option>
                      </select>
                    </div>

                    <div class="form-group col-12" >
                      <label class="subtitulo-shearh">¿Tiene arma propia?</label>
                      <select class="form-control form-control-sm  selectpicker todo-redondedado" data-style="select-style" data-width="100%" name="arma_propia" id="arma_propia" onchange="return busquedaAjaxTotal()">
                          <option value="">Seleccione</option> 
                          <option value="2">No</option>
                          <option value="1">Si</option>
                      </select>
                    </div>
            </div>
            <div class="form-row">
  
              <div class="form-group col-12" style="margin-bottom: -5px;">
                    <label class="subtitulo-shearh">Ultima Actualizacion del CV:</label>
              </div>

              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input todo-redondedado" id="ultima_act1" name="ultima_act" value="1" onClick="return busquedaAjaxTotal()">
                    <label class="custom-control-label subtitulo-shearh" for="ultima_act1" >Menor a 1 mes</label>
                  </div>
              </div>
              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input todo-redondedado" id="ultima_act3" name="ultima_act" value="3" onClick="return busquedaAjaxTotal()">
                    <label class="custom-control-label subtitulo-shearh" for="ultima_act3" >Menor a 3 meses</label>
                  </div>
              </div>   
              <div class="form-group col-12" style="margin-bottom: 2px;">
                  <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input todo-redondedado" id="ultima_act6" name="ultima_act" value="6" onClick="return busquedaAjaxTotal()">
                    <label class="custom-control-label subtitulo-shearh" for="ultima_act6" >Menor a 6 meses</label>
                  </div>
              </div> 

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input todo-redondedado" id="discapcidad" name="discapcidad" value="1" onClick="return busquedaAjaxTotal()">
                <label class="custom-control-label subtitulo-shearh" for="discapcidad">Candidatos con discapacidad</label>
              </div>


              <!-- 
              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input todo-redondedado" id="cv_visible" name="cv_visible" value="1" onClick="return busquedaAjaxTotal()">
                <label class="custom-control-label subtitulo-shearh" for="cv_visible">Solo CV con foto</label>
              </div>
              -->
            </div>
          </div>
          <input type="hidden" id="base_url" value="{{ asset('') }}">
        </div>
      </div>
      <!-- fin filtro -->
      <!-- inicio restultado -->
      <div class="col-12 col-sm-12 col-md-9"> 
        <!-- incio foreach -->
        <div class="card">
          <div class="d-none d-sm-none d-md-block">
            <div class="card-header"  style="background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}; color:#fff;">
              <div class="row" >
                <div class="col-1" style="max-width: 0;">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="todo" name="todo" value="1">
                    <label class="custom-control-label subtitulo-shearh" for="todo"></label>
                  </div>
                </div>
                <div class="col-2">
                    <h6>Fotografía</h6>
                </div>
                <div class="col-3">
                    <h6>Información Basica</h6>
                </div>
                <div class="col-3">
                    <h6>Experiencia</h6>
                </div>
                <div class="col-2">
                    <h6>Educación</h6>
                </div>
                <div class="col-1" style="max-width: 14.333333%;">
                    <h6>Acciones</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="lista_restultado" id="lista_restultado" >
          <?php
          echo $tbl;
          ?>
                              <!-- fin foreach -->   
        </div>
      </div>
    </div>
  </div>
  <!-- fin resultado -->
</form>
    <script type="text/javascript">

   
/*
  function BuscarPostulante(id,bac) {
	  var base_url=$("#base_url").val();
	  var $form = $("#frmFormulario");
	  $form.submit();
	}
*/
	function ComboboxProvincia(idc){
		var base_url=$("#base_url").val();
		var comxDeparment=$("#"+idc.id).val();
		var urlData=base_url+"ubigeo/provincia/"+comxDeparment;
		$.ajax({
		        type:'GET',
		        url:urlData,
		        dataType:'JSON',  
		         beforeSend: function(){
		       $(".loader").css({display:'block'});
		      }      
		}).done(function( data, textStatus, jqXHR ){
		        var texto='';
		        texto+='<option value="00">*SELECCIÓNE*</option>';     
		        text='<option value="00">*SELECCIÓNE*</option>';     
		        if(data['success']!="false" && data['success']!=false){
		            if(typeof(data['ResulProvincia'])!='undefined'){
		              $.each(data['ResulProvincia'], function(i, field){
		                texto+='<option value="'+field.ubi_codprov+'">'+field.ubi_descripcion+'</option>';     
		              });
		              $('#provi').html(texto); 
		               $('#distri').html(text);  
		            }else{
		              $('#provi').html(texto);
		              $('#distri').html(text);
		            }
		        }else{
		          $('#provi').html(text);
		          $('#distri').html(text);
		        }
		    $(".loader").css({display:'none'});
		  }).fail(function( jqXHR, textStatus, errorThrown ){
		    mensaje='Solicitud fallida: ' + textStatus;
		    $(".mensajeGlobal").SendMessajeAlert({
		        messaje:mensaje+textStatus,
		        dataType:'Error',
		        type:'danger',
		        time:6000
		    });
		     $(".loader").css({display:'none'});
		  }); 
	}
	function ComboboxDistrito(){
		   var base_url=$("#base_url").val();
		  var comxDeparment=$("#deparme").val();
		  var comxProvin = $("#provi").val();
		  var urlData=base_url+"ubigeo/distrito/"+comxDeparment+'/'+comxProvin;
		  $.ajax({
		        type:'GET',
		        url:urlData,
		        dataType:'JSON', 
		         beforeSend: function(){
		       $(".loader").css({display:'block'});
		      }      
		    }).done(function( data, textStatus, jqXHR ){
		        var texto='';
		        texto+='<option value="00">*SELECCIÓNE*</option>';     
		        if(data['success']!="false" && data['success']!=false){
		            if(typeof(data['ResulDistrito'])!='undefined'){
		              $.each(data['ResulDistrito'], function(i, field){
		                texto+='<option value="'+field.ubi_coddist+'">'+field.ubi_descripcion+'</option>';     
		              });
		              $('#distri').html(texto);   
		          }else{
		              $('#distri').html(texto);
		          }
		        }else{
		          $('#distri').html(texto);
		        }
		    $(".loader").css({display:'none'});
		  }).fail(function( jqXHR, textStatus, errorThrown ){
		   mensaje='Solicitud fallida: ' + textStatus;
		    $(".mensajeGlobal").SendMessajeAlert({
		        messaje:mensaje+textStatus,
		        dataType:'Error',
		        type:'danger',
		        time:6000
		    });
		     $(".loader").css({display:'none'});
		  }); 
	}
  
  
  function agregarPostulante(ficha, persona){
    var url_history_back=$("#url_history_back").val();
    var base_url=$("#base_url").val();
      var pedido=$("#pedido").val();
		  var empresa=$("#empresa").val();
		  var contacto=$("#contacto").val();
      <?php 
            if($menu == '1'){
            ?>
      var urlData=base_url+"matching/agregarpost/"+ficha+'/'+persona+'/'+pedido;
      <?php
            }else{
              ?>
              var urlData=base_url+"entidad/agregarpost/"+ficha+'/'+persona+'/'+pedido;
              <?php
            }
      ?>
		  $.ajax({
		        type:'GET',
		        url:urlData,
            dataType:'JSON', 
            
		    }).done(function( data, textStatus, jqXHR ){
          if(data == "1"){
            <?php 
            if($menu == '1'){
            ?>
            window.location.href=base_url+'matching/seguimiento/'+pedido+'?empresa='+empresa+'&contacto='+contacto;
            <?php
            }else{
              ?>
              window.location.href=base_url+'entidad/pedido/detalle/'+empresa+'/'+pedido;
              <?php
            }
            ?>
          }else{
            alert("Postulante ya se encuentra vinculado");
          }
		      
		  }).fail(function( jqXHR, textStatus, errorThrown ){
       mensaje='Solicitud fallida: ' + textStatus;
       alert(mensaje);
		  }); 
  } 

function guardarPostulante(ficha, persona, check){

  var base_url=$("#base_url").val();
  var pedido=$("#pedido").val();
	var empresa=$("#empresa").val();
  var contacto=$("#contacto").val();
      
if( $(check).is(':checked') ) {
  <?php 
            if($menu == '1'){
            ?>
    var urlData=base_url+"matching/agregarpost/"+ficha+'/'+persona+'/'+pedido;
    <?php
            }else{
              ?>
              var urlData=base_url+"entidad/agregarpost/"+ficha+'/'+persona+'/'+pedido;
              <?php
            }
      ?>
		$.ajax({
		    type:'GET',
		    url:urlData,
        dataType:'JSON', 
		}).done(function( data, textStatus, jqXHR ){
      if(data == "0"){
        alert("Postulante ya se encuentra registrado");
        $(check).prop('checked', false);
      }
      
       // alert(data);
		    //  window.location.href=base_url+'matching/seguimiento/'+pedido+'?empresa='+empresa+'&contacto='+contacto;
		}).fail(function( jqXHR, textStatus, errorThrown ){
       mensaje='Solicitud fallida: ' + textStatus;
       alert(mensaje);
		}); 
  }else{
    <?php 
            if($menu == '1'){
            ?>
    var urlData=base_url+"matching/deletepost/"+ficha+'/'+persona+'/'+pedido;
    <?php
            }else{
              ?>
              var urlData=base_url+"entidad/deletepost/"+ficha+'/'+persona+'/'+pedido;
              <?php
            }
    ?>
		$.ajax({
		  type:'GET',
		  url:urlData,
      dataType:'JSON', 
		}).done(function( data, textStatus, jqXHR ){
        //  alert(data);
		    //  window.location.href=base_url+'matching/seguimiento/'+pedido+'?empresa='+empresa+'&contacto='+contacto;
		}).fail(function( jqXHR, textStatus, errorThrown ){
       mensaje='Solicitud fallida: ' + textStatus;
       alert(mensaje);
    }); 
      
  }

}

function regrear(){
  var base_url=$("#base_url").val();
  var pedido=$("#pedido").val();
	var empresa=$("#empresa").val();
  var contacto=$("#contacto").val();
  <?php
            if($menu == '1'){
            ?>
  window.location.href=base_url+'matching/seguimiento/'+pedido+'?empresa='+empresa+'&contacto='+contacto;
<?php
            }else{
?>

window.location.href=base_url+'entidad/pedido/detalle/'+empresa+'/'+pedido;

<?php
            }
?>
  
}


  function variarEstadoTshearh(tipo){
    
    var response = "<option value=''>Seleccione</option>";
    if(tipo.value == "7"){
  //alert(tipo.value);
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='16'>COLEGIADO</option>";
      response += "<option value='14'>BACHILLER</option>";
      response += "<option value='34'>CICLOS FINALES</option>";
      response += "<option value='35'>CICLOS INTERMEDIOS</option>";
      response += "<option value='36'>CICLOS INICIALES</option>";
    }
    if(tipo.value == "6"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='34'>CICLOS FINALES</option>";
      response += "<option value='35'>CICLOS INTERMEDIOS</option>";
      response += "<option value='36'>CICLOS INICIALES</option>";
    }
    if(tipo.value == "26"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
    }
    if(tipo.value == "27"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
    }
    $('#estado_seleccion').html(response);
  }

  function DescargarHojavida(id,PP_id,ficha_id,id_persona,idPedido,fen) {
   
    var _token=$("#csrf-token").val();
    base_url=$("#base_url").val();
   /* var urljsona=base_url+"cambiar/visto/"+PP_id;
    $.ajax({
            type:'get',
            url:urljsona,
            dataType:'json', 
          success:function(data) {
              console.log(data);
          }
    }).done(function( data, textStatus, jqXHR ){
      location.reload();
    }).fail(function( jqXHR, textStatus, errorThrown ){
    });

    */
    var urlData=base_url+"pdf/downloadcv";
    window.open(urlData+'/'+PP_id+'/'+ficha_id+'/'+id_persona+'/'+idPedido+'/'+fen)

  }
  function errorCv(){
    alert("No tiene CV adjunto");
  }

function busquedaAjaxTotal(){

  var base_url= $("#base_url").val();
  var formu = $("#frmFormulario");
  <?php
  if($menu == '1'){
    ?>
    var urlData=base_url+"matching/agregarPostAjax";
    <?php
  }else{
    ?>
    var urlData=base_url+"entidad/agregarPostAjax";
    <?php
  }
  ?>
	
  $('#lista_restultado').html('<div class="loading"><center><img src="'+base_url+'animacion/carga.gif" alt="loading" /></center></div>')
		  $.ajax({
		        type:'GET',
		        url:urlData,
            dataType:'JSON', 
            data:formu.serialize(),
            
		  }).done(function( data, textStatus, jqXHR ){
          $("#lista_restultado").html(data);
		  }).fail(function( jqXHR, textStatus, errorThrown ){
       mensaje='Solicitud fallida: ' + textStatus;
       alert(mensaje);
       return false;
		  }); 

}
 
function limpiarTodo(){

  $("#ocupacion").val("");
  $("#titulo_puesto").val("");
  $("#empresa_form").val("");
  $("#tareas").val("");
  $("#institucion").val("");
  $("#profesion").val("");
  $("#gradosAcademicos").val("");
  $("#estado_seleccion").val("");
  $("#salario").val("");
  $("#idioma1").val("");
  $("#compu").val("");
  $("#nombre").val("");
  $("#apellido").val("");
  $("#documento").val("");
  $("#edad_desde").val("");
  $("#edad_hasta").val("");
  $("#sexo").val("");
  $("#pais").val("36");
  $("#deparme").val("0");
  $("#provi").val("0");
  $("#distri").val("0");
  $("#provi").val("0");
  $("#licenciaConduc").val("");
  $("#categoria").val("");
  $("#port_armas").val("");
  $("#arma_propia").val("");
  $("#ultima_act1").prop('checked', false);
  $("#ultima_act3").prop('checked', false);
  $("#ultima_act6").prop('checked', false);
  $("#discapcidad").prop('checked', false);
  $("#cv_visible").prop('checked', false);
  location.reload();
}

</script>
<style>
.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    /*width: 100%;
    height: 100%;*/
    z-index: 9999;
    background: url("{{ asset('animacion/carga.gif') }}") 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>
<br>


@include('maestro/footer_usuario')

@endsection