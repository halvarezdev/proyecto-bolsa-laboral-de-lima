<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table >
				<tr>
					<td ><b>Nombre:</b></td>
					<td >{{!isset($request_db->ape_pat,$request_db->ape_mat, $request_db->name)?'':$request_db->ape_pat.' '. $request_db->ape_mat.' '. $request_db->name}}</td>
				</tr>
				<tr>
					<td ><b>DNI:</b></td>
					<td >{{$request_db->numero_documento}}</td>
				</tr>
				<tr>
					<td class="color_text_red"><b>Fecha Registro:</b></td>
					<td class="color_text_red">{{$request_db->fecha}}</td>
				</tr>
			</table>

		</div>
		<div class="col-md-12 m-2">
			<table>
				<tr>
					<td>Fecha de Entrevista</td>
					<td><input type="" id="FechaEnvio" style="width: 20px;height: 19px" maxlength="2" value="{{$request_db->fech_envio}}"></td>
					<td>{{date("m/Y")}}</td>
					<td>Ejemplo(d/m/yyyy)</td>
				</tr>
				<tr>
					<td>Fecha de Colocación</td>
					<td ><input type="" id="FechaColocacion" style="width: 20px;height: 19px" maxlength="2" value="{{$request_db->dia_coloc}}"></td>
					<td>{{date("m/Y")}}</td>
					<td>Ejemplo(d/m/yyyy)</td>
				</tr>
			</table>
			<div class="text-center">
				<?php $param=$_id.','.$request['PP_id']. ','. $request['ficha_id']. ','. $request['id_persona']. ','. $request['idPedido']?>
				<button class="btn btn-info btn-sm" onclick="ActualizarInformacionPedidoPer({{$param}})">Guardar</button>
			</div>
			
		</div>
	</div>
</div>