@extends('layouts.app')

@section('content')
    <style type="text/css">
      .btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    border-radius: 35px;
    font-size: 24px;
    line-height: 1.33;
        margin-right: 50px;
    margin-bottom: 10px;
}

.btn-circle {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
        margin-right: 50px;
    margin-bottom: 10px;
    border-color: #003dc7 !important;
    background-color: white!important;
}
.color_letra{
  color: #00a4d3;
   
}
.background_color{
     background-color: #003dc7 !important;
   
}
.border_color{
  background-color: #003dc7 !important;

}
.border_color h3{
  color: white;
  font-size: 20px !important;
}
.btn-default{
      color: black;
    background-color: #eaf3f4;;
    border-color:#eaf3f4;;
}
.btn.btn-file>input[type='file'] {
    position: absolute;
    top: 0;
    right: 0;
   
    font-size: 100px;
    text-align: right;
    opacity: 0;
    filter: alpha(opacity=0);
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.forulario .form-group{
    border-color: #fff;
margin-bottom: 0.2rem !important;
}
.sumTitle{
  color: #17a2b8!important;
}
#footer {
   position:fixed;
   left:0px;
   bottom:0px;
   height:40px;
   width:100%;
}
header{
  position:fixed;
   left:0px;
  margin-top: 0px;
   height:40px;
   width:100%;
}

 .ui-widget-content {
     border: 0px solid #aaaaaa;
}
.mje, .mje2, .mje3, .mje4 {
 
    float: left;
}
.form-control {
  border-:2px solid  #17a2b8;
    outline: 0;
    box-shadow: none;
    border: #fff;
    border-bottom-color: #17a2b8; 
  border-bottom:2px solid  #17a2b8;
     border-bottom-color: #17a2b8; 
    -webkit-box-shadow: none;
    box-shadow: none;
}

.form-control::-webkit-input-placeholder { text-transform: uppercase; } 
 
.form-control::-moz-placeholder {  text-transform: uppercase;} 
 
.form-control::-moz-placeholder {text-transform: uppercase; } 
 
.form-control::-ms-input-placeholder { text-transform: uppercase; }
.form-control:focus::-webkit-input-placeholder{color:transparent;}
.form-control:focus {
 border-bottom:2px solid  #17a2b8;
    outline: 0;
    box-shadow: none;
    border-color: #fff;
    border-bottom-color: #17a2b8; 
}
.textopequenio{
  font-size: 10px
}
.btn_color{
   background-color: #003dc7 !important;
   color: white
}
.btn_file{
  width:150px; height: 150px;
  background-color: #fefeff;
  border: 1px solid #17a2b8;
  text-align: center;
  text-transform: uppercase;
}

/*.fixed_header{
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
}*/
.sizeIco{
  font-size: 17px;
}
.fixed_header tbody{
  display:block;
  width: 100%;
  overflow: auto;
  height: 300px;
}

.fixed_header thead tr {
   display: block;
}

/*.fixed_header thead {
  background: black;
  color:#fff;
}*/

.fixed_header th, .fixed_header td {
  padding: 5px;
  text-align: left;
 width: 400px;
}.myButton {
 background-color:#edf7f0;
  -moz-border-radius:29px;
  -webkit-border-radius:29px;
  border-radius:29px;
  border:2px solid #4c37ed;
  display:inline-block;
  cursor:pointer;
  color:#0f0e0f;
  font-family:Arial;
  font-size:17px;
  padding:1px 32px;
  text-decoration:none;
  text-shadow:0px 0px 0px #b6b8b8;
}
.myButton:hover {
  background-color:#63c9db;
}
.myButton:active {
  position:relative;
  top:1px;
}.background_color {
    left: 0px;
    margin-top: 0px;
    height: 40px;
    width: 100%;
}
    </style>
<div>
  <div class="container-fluid">
    <div class="row mt-2">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="text-left">
          <img src="{{asset('logo.png')}}" style="width: 180px"><br>
          <a class="btn btn-sm myButton " href="javascript:window.history.back()">Regresar</a>
        </div>
      </div>
    </div>
      <!-- <img src="{{asset('logo.png')}}" style="margin-top: 40px;width: 180px">
        -->
        <div class="container-fluid">
    <div class="text-center">
        <h3 class="color_letra">BUSQUEDA DE POSTULANTE<p></p></h3>

	
    </div>
    <div class="row">
    	<div class="col-md-1"></div>
    	<div class="col-md-10">
    		    <h3 class="color_letra">buscar Postulante</h3>
	<form method="POST" action="{{ asset('matching/postulantes/'.$_id) }}" class="form-inline" id="frmFormulario">
		<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
  <input type="hidden" name="url_history_back" id="url_history_back" value="{{$request['url_history_back']}}">
  <input type="hidden" id="base_url" value="{{ asset('') }}">
  
<input type="hidden" name="_id" value="{{$_id}}">
    	<table class="table table-sm fixed_header" id="tableMecanicaProv" border="0">
    		<thead>
    			<tr>
    				<th></th>
    				<th>DNI</th>
    				<th>Ap. Paterno</th>
    				<th>Ap. Materno</th>
    				<th>Nombre(s)</th>
    				<th>Teléfono</th>
    				<th>Correo</th>
    				<th style="text-align: right;">Salario</th>
    				<th style="text-align: right;">Distrito</th>
    				<th>Ver CV</th>
    			</tr>
    		</thead>
    		<tbody>

    			<?php
echo $response_db['table'];
    			?>
    		</tbody>
    		<tfoot>
    		<?php
echo $response_db['theadPagin'];
    			?>	
    		</tfoot>
    	</table>
          <button type="button" id="btnguardar" class="btn btn-primary" onclick="AgregarPostulante('{{$_id}}')">Agregar</button>    
    	</div>
    	<div class="col-md-1"></div>
    	
    </div>
  </div>
</div>


 
 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

    <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
 
  <script type="text/javascript" src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>

  
<br><br><br>

<script type="text/javascript">
	function AgregarPostulante(id) {
		var detalle=savePostulantepedido();
		if (detalle=='') {
			return false;
		}
		var url_history_back=$("#url_history_back").val();
		$("#btnguardar").attr('display', 'none');
		var base_url=$("#base_url").val();
		var frmFormulario=$("#frmFormulario").serialize();
		var urlData=base_url+"matching/save/"+id;
		$.ajax({
		        type:'POST',
		        url:urlData,
		        dataType:'JSON', 
		        data:frmFormulario+'&detalle='+detalle, 
		         beforeSend: function(){
		       $(".loader").css({display:'block'});
		      }      
		}).done(function( data, textStatus, jqXHR ){
		    $("#btnguardar").attr('display', 'block'); 
		    $(".loader").css({display:'none'});
		    window.location.href=base_url+'matching/seguimiento/'+url_history_back
		}).fail(function( jqXHR, textStatus, errorThrown ){
		  	$("#btnguardar").attr('display', 'block');
		    mensaje='Solicitud fallida: ' + textStatus;
		    
		    $(".mensajeGlobal").SendMessajeAlert({
		        messaje:mensaje+textStatus,
		        dataType:'Error',
		        type:'danger',
		        time:6000
		    });
		     $(".loader").css({display:'none'});
		}); 
	
	}

	function savePostulantepedido(){
      var aDets = new Array();
      var trs = document.getElementById('tableMecanicaProv').tBodies[0].rows;
      var nt = trs.length, t;
      for (t = 0; t < nt; t ++) {
        var tr = trs[t];
        var inps2 =tr.cells;
        var inps =$(inps2[0]).find('input');
        var chk =inps[0].checked;
        if (chk) {
          var id_ficha=inps[1].value;
          var id_postu=inps[2].value;
          aDets.push(id_ficha+','+id_postu);
        }
      }
      var detalles = aDets.join(';');
      return detalles;
  }
</script>

<br>
@include('maestro/footer_usuario')

@endsection