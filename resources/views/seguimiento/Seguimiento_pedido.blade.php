  @extends('layouts.app')

  @section('content')

  <style type="text/css">
      .form-control:hover{
          border:1px black solid;
      }
      .table td, .table th {

      padding: .25rem;
      vertical-align: top;
      border-top: '1px solid {{ (isset($entidad_color) ? $entidad_color : '') }}';

      }
      .btn_buscar{
      background-color: '{{ (isset($entidad_color) ? $entidad_color : '') }}';
      font-size:13px;
      color: #fff;
      border-radius: 10rem !important;
      border: 'solid 1px {{ (isset($entidad_color) ? $entidad_color : '') }}';
      padding: 3px;
    }

    .btn_buscar:hover{
      background-color: #fff;
      font-size:13px;
      color: '{{ (isset($entidad_color) ? $entidad_color : '') }}';
      border-radius: 10rem !important;
      border: 'solid 1px {{ (isset($entidad_color) ? $entidad_color : '') }}';
      padding: 3px;
    }
    .btn_regresar{
      background-color: '{{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}';
      font-size:13px;
      color: #fff;
      border-radius: 10rem !important;
      border: solid 1px '{{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}';
      padding: 3px;
    }

    .btn_regresar:hover{
      background-color: #fff;
      font-size:13px;
      color: '{{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}';
      border-radius: 10rem !important;
      border: 'solid 1px {{ (isset($subcolor_entidad) ? $subcolor_entidad : '') }}';
      padding: 3px;
    }

  </style>
  <?php 
  $submenu = "2";
  ?>
  @include('maestro/submenu')

  <br>

  <div class="container" style="background-color: #fff;padding-right: 0px;padding-left: 0px;">
    <input type="hidden" name="estadoPedido" id="estadoPedido" value="{{isset($db_response->ped_InterColcCie) ? $db_response->ped_InterColcCie : ''}}">
      <div class="degrade-griss">
        <div class="row">
            
            <div class="col" style="padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 40px;">
            <h5 style="color:#fff;">Gestión de Vacante: {{isset($db_response->des_pedido) ? $db_response->des_pedido : ''}}</h5>
            </div>
            
        </div>
      </div>
      <br>
      <div class="row" style="margin-right: 15px;margin-left: 15px;"> 
          <div class="col-6 col-sm-6 col-md-2">
              <a href="{{ route('contacto.dashboard',['state' => '1']) }}" class="btn btn_regresar" style="width:100%; " ><i class="fas fa-undo-alt"></i>&nbsp;&nbsp;&nbsp;Regresar</a>
              </div>
          <div class="col-6 col-sm-6 col-md-2" >
              <a href="{{ route('buscar.postulante', ['pedido' => isset($db_response->id_pedido ) ? $db_response->id_pedido : '']) }}" class="btn btn_buscar" style="width:100%;" ><i class="fas fa-search"></i> &nbsp;&nbsp;&nbsp;Buscar CVs</a>
          </div>
      </div>
      <br>
      <div class="row" style="margin-right: 15px;margin-left: 15px;">
      
        <div class="col">
          
          <div style="margin-left: 20px">
          <div class="table-responsive">
          <table>
            <tr>
              <td style="color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size:13px;"><img src="{{ asset( (isset($icon1) ? $icon1 : '') ) }}">&nbsp;&nbsp;Código del Pedido:</td>
              <td style="font-size:13px;">{{isset($db_response->id_pedido) ? $db_response->id_pedido : ''}}</td>
            </tr>
            <tr>
              <td style="color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size:13px;"><img src="{{ asset( (isset($icon2) ? $icon2 : '') ) }}">&nbsp;&nbsp;Nro de Vacantes:</td>
              <td style="font-size:13px;">{{ isset($db_response->num_vacantes) ? $db_response->num_vacantes : ''}}</td>
            </tr>
            <tr>
              <td style="color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size:13px;"><img src="{{ asset( (isset($icon3) ? $icon3 : '') ) }}">&nbsp;&nbsp;Fecha Publicado:</td>
              <td style="font-size:13px;">{{ isset($db_response->fecha) ? $db_response->fecha : ''}}</td>
            </tr>
            <tr>
              <td style="color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size:13px;"><img src="{{ asset( (isset($icon4) ? $icon4 : '') ) }}">&nbsp;&nbsp;Fecha Finaliza:</td>
             <td style="font-size:13px;">{{ isset($db_response->fecha_limite) ? $db_response->fecha_limite : '' }}</td>
            </tr>
            <tr>
              <td style="color:{{ (isset($entidad_color) ? $entidad_color : '') }}; font-size:13px;"><img src="{{ asset( (isset($icon5) ? $icon5 : '') ) }}">&nbsp;&nbsp;Postulantes:</td>
             <td style="font-size:13px;">
          
            </td>
            </tr> 
          </table>
          </div>
          <br>
         </div>
         
         </div>
        </div> 
         <div class="col">
         
        <form method="POST" action="" class="form-inline" id="frmFormulario">
      <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
      <input type="hidden" id="base_url" value="{{ asset('') }}">
      <div class="table-responsive">
        <table class="table table-sm" id="tableMecanicaProv" >
          <thead style="background-color:{{ (isset($entidad_color) ? $entidad_color : '') }}">
            <tr>
              <td style="font-size:13px; color:#fff;">DNI</td>
              <td style="font-size:13px; color:#fff;">Postulantes</td>
            <td style="font-size:13px; color:#fff;">Teléfono</td>
              <td style="font-size:13px; color:#fff;">Puesto</td>
              <td style="font-size:13px; color:#fff;">CV</td>
              <td style="font-size:13px; color:#fff;">Descargar</td>
              <td style="font-size:13px; color:#fff;">Video</td>
              <td style="font-size:13px; color:#fff;">Procedencia</td>
              <td style="font-size:13px; color:#fff;">Eliminar</td>
              <td style="font-size:13px;line-height: 100%; color:#fff;">Revision <br>curricular</td>
              <td style="font-size:13px;line-height: 100%; color:#fff;">Proceso <br>entrevista</td>
              <td style="font-size:13px;line-height: 100%; color:#fff;">Proceso<br> colocacion</td>
            </tr>
          </thead>
          
          <tbody>
    
                    <?php
          echo $response_db['table'];
                    ?>
                  </tbody>
                  <tfoot>
                  <?php
          echo $response_db['theadPagin'];
                    ?>  
          </tfoot>
        </table>  
  </table>
        </div>
      
        </div>
      </div>
    </form>


   
   <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
  <!-- 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

      <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
   
    <script type="text/javascript" src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>

    


  <br><br><br><br>

   <script type="text/javascript">
     function cambiarAptoVideo(lnk,idPedido,id_persona,ficha_id){
        $("#myModal").modal('hide')
        var _token=$("#csrf-token").val();
        base_url=$("#base_url").val();
        var urlData=base_url+"matching/video/"+ficha_id;
        $.ajax({
                type:'POST',
                url:urlData,
                dataType:'JSON', 
                data:{_token,id_persona,ficha_id,idPedido}, 
                beforeSend: function(){
              $(".loader").css({display:'block'});
              }      
        }).done(function( data, textStatus, jqXHR ){
          $(".loader").css({display:'none'});
          $("#myModal").modal()
          $("#myModal .modal-content").css('width','600px')
          
          $("#myModal #cargarInformacion").html(data['video'])
            
        }).fail(function( jqXHR, textStatus, errorThrown ){
            $(".loader").css({display:'none'});
        }); 
     }
    function EliminarPedidoPostulante(lnk,PP_id,ficha_id,id_persona,idPedido) {
      var tds = lnk.parentNode.parentNode.id;
      $("#btnEliminar").attr('onclick', 'EliminarPedidoPostulante2('+PP_id+','+ficha_id+','+id_persona+','+idPedido+",'"+tds+"'"+')');
       
     
    
    }
    function EliminarPedidoPostulante2(PP_id,ficha_id,id_persona,idPedido,lnk) {
       $("#myModalDelete").modal('hide')
    var _token=$("#csrf-token").val();
      base_url=$("#base_url").val();
      var urlData=base_url+"matching/delete/"+PP_id;
      $.ajax({
              type:'POST',
              url:urlData,
              dataType:'HTML', 
               data:{_token,PP_id,ficha_id,id_persona,idPedido}, 
               beforeSend: function(){
             $(".loader").css({display:'block'});
            }      
      }).done(function( data, textStatus, jqXHR ){
          $(".loader").css({display:'none'});
          $("#"+lnk).remove()
          $("#myModalDelete").modal('hide')
          $(".modal-backdrop").removeClass('modal-backdrop')
      }).fail(function( jqXHR, textStatus, errorThrown ){
          $(".loader").css({display:'none'});
      }); 
    }

    function funcionaliddbtn(tipo,pedido) {
       var _token=$("#csrf-token").val();
      base_url=$("#base_url").val();
      var urlData=base_url+"matching/updatePedido";
      $.ajax({
              type:'POST',
              url:urlData,
              dataType:'HTML', 
               data:{tipos:tipo,ped:pedido,_token:_token}, 
               beforeSend: function(){
             $(".loader").css({display:'block'});
            }      
      }).done(function( data, textStatus, jqXHR ){
        console.log(tipo)
          $(".loader").css({display:'none'});
          if (tipo==1) {
            $("#btnControlIni1").removeClass('myButtonCNot').addClass('myButtonC');
            $("#btnControlIni2").removeClass('myButtonC').addClass('myButtonCNot');
            $("#btnControlIni3").removeClass('myButtonC').addClass('myButtonCNot');
             $("#estadoPedido").val(1);
          }else if(tipo==2){
            $("#btnControlIni1").removeClass('myButtonC').addClass('myButtonCNot');
            $("#btnControlIni2").removeClass('myButtonCNot').addClass('myButtonC');
            $("#btnControlIni3").removeClass('myButtonC').addClass('myButtonCNot');
            $("#estadoPedido").val(2);
          }else{
            $("#btnControlIni1").removeClass('myButtonC').addClass('myButtonCNot');
            $("#btnControlIni2").removeClass('myButtonC').addClass('myButtonCNot');
            $("#btnControlIni3").removeClass('myButtonCNot').addClass('myButtonC');
            $("#estadoPedido").val(3);
          }
      }).fail(function( jqXHR, textStatus, errorThrown ){
          $(".loader").css({display:'none'});
      }); 
    }
    function cargarContenidoPostulante(id,PP_id,ficha_id,id_persona,idPedido) {

      var _token=$("#csrf-token").val();
      base_url=$("#base_url").val();
      var urlData=base_url+"matching/editar/"+PP_id;
      $.ajax({
              type:'POST',
              url:urlData,
              dataType:'HTML', 
              data:{_token,PP_id,ficha_id,id_persona,idPedido}, 
               beforeSend: function(){
             $(".loader").css({display:'block'});
             $("#cargarInformacion").html('Cargando Información')
            }      
      }).done(function( data, textStatus, jqXHR ){
        $(".loader").css({display:'none'});
       $("#cargarInformacion").html(data)
      }).fail(function( jqXHR, textStatus, errorThrown ){
        $(".loader").css({display:'none'});
         $("#cargarInformacion").html('No hay resultado')
      });

    }
    function DescargarHojavida(id,PP_id,ficha_id,id_persona,idPedido,fen) {
      var _token=$("#csrf-token").val();
      base_url=$("#base_url").val();
      var urljsona=base_url+"cambiar/visto/"+PP_id;
      $.ajax({
              type:'get',
              url:urljsona,
              dataType:'json', 
            //  data:{_token,PP_id,ficha_id,id_persona,idPedido,FechaEnvio:FechaEnvio,FechaColocacion:FechaColocacion}, 
              /* beforeSend: function(){
             $(".loader").css({display:'block'});
            }    */  
            success:function(data) {
                console.log(data);
            }
      }).done(function( data, textStatus, jqXHR ){
        //  alert(data);
        location.reload();
      }).fail(function( jqXHR, textStatus, errorThrown ){
       // alert(jqXHR+"-"+textStatus+"-"+errorThrown);
      });

      
      var urlData=base_url+"pdf/downloadcv";
      window.open(urlData+'/'+PP_id+'/'+ficha_id+'/'+id_persona+'/'+idPedido+'/'+fen)

    }

    function ActualizarInformacionPedidoPer(id,PP_id,ficha_id,id_persona,idPedido) {
      var _token=$("#csrf-token").val();
      base_url=$("#base_url").val();
      var urlData=base_url+"matching/actualizarColocacion/"+PP_id;
      var FechaEnvio=$("#FechaEnvio").val();
      var FechaColocacion=$("#FechaColocacion").val();
      $.ajax({
              type:'POST',
              url:urlData,
              dataType:'HTML', 
              data:{_token,PP_id,ficha_id,id_persona,idPedido,FechaEnvio:FechaEnvio,FechaColocacion:FechaColocacion}, 
               beforeSend: function(){
             $(".loader").css({display:'block'});
            }      
      }).done(function( data, textStatus, jqXHR ){
        $(".loader").css({display:'none'});

       $("#myModal").modal('hide');
      }).fail(function( jqXHR, textStatus, errorThrown ){
        $(".loader").css({display:'none'});
      });
    }
      </script>
  <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h5 class="modal-title">ENTREVISTAS Y COLOCACIONES</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <div class="modal-body" id="cargarInformacion">
            Cargando Información...
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModalDelete">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <!-- Modal body -->
          <div class="modal-body">
           Esta Seguro de Eliminar
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="btnEliminar">Eliminar</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
      </div>
    </div>


    <!-- fecha de  -->

    <div>
      
      <form id="form_select" action="{{ route('cambiar.estadoestatus') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="codigo_cambio" id="codigo_cambio">
        <input type="hidden" name="selection" id="selection">
        <input type="hidden" name="persona" id="persona">
        <input type="hidden" name="ficha" id="ficha">
        </form>
        <script type="text/javascript">
            function cambiarStatus(id,cod,persona,ficha){
              $("#codigo_cambio").val(cod);
              $("#persona").val(persona);
              $("#ficha").val(ficha);
              $("#selection").val(id.value);

              $("#form_select").submit();
            }

function enviarMensaje(){
         var _token=$("#csrf-token").val();
                base_url=$("#base_url").val();
                let ficha = $("#ficha_pe").val();
                let persona = $("#persona_pe").val();
                let pedido = $("#pedido_pe").val();
                let telefono_pe = $("#telefono_pe").val();
                if($("#fecha").val()==''){
                 
                  alert('Seleccionar fecha de cita');
                  $("#fecha").focus();
                  return false;
                }
                if($("#hora").val()==''){
                 
                 alert('Seleccionar hora de cita');
                 $("#hora").focus();
                 return false;
               }
                let fecha = $("#fecha").val() ? $("#fecha").val() : "-";
                let hora = $("#hora").val() ? $("#hora").val() : "-";
                const myArray = telefono_pe.split("|");
                      tele=myArray[0]
                       
                      raz=myArray[1]
                      nombre=myArray[1]
                      var body='';
                      if(fecha=='-'){
                        body+="La bolsa de trabajo de la Municipalidad de Lima le informa que ha sido citado";
                        body+=" por la empresa *"+raz+"*. Esté atento a las comunicaciones que pronto pueda recibir. Buen día.";
                      }else{ 
                        body+="La bolsa de trabajo de la Municipalidad de Lima ";
                        body+="le informa que ha sido citado por la empresa *"+raz+"* el día *"+fecha+"* a las *"+hora+"* horas. Buen día."
                      }
                      
                      if(tele!='0'){
                        window.open('https://api.whatsapp.com/send?phone=+51'+tele+'&text='+body,'')
                      }
                     // location.reload();  
                var urlData=base_url+"matching/citado/"+ficha+"/"+persona+"/"+pedido+"/"+fecha+"/"+hora;
                $.ajax({
      type: "get",
      url: urlData,
       dataType:'JSON',
      beforeSend: function(){
       
      },
    }).done(function( data, textStatus, jqXHR ){
      location.reload();
    }).fail(function(jqXHR, ajaxOptions, thrownError){
       
       location.reload();
      
    });
                // $.ajax({
                //       type:'GET',
                //       url:urlData,
                //       dataType:'json', 
                //     success:function(data) {
                      
                //        console.log('')
                //       location.reload();
                //     }
                // });
}
            function cambiarCitado(input, pedido, persona, ficha, flag, apto,telefono){
              console.log('ssssssss')
              if(apto == "1"){
                $("#exampleModal").modal('show');
                $("#ficha_pe").val(ficha);
                $("#persona_pe").val(persona);
                $("#pedido_pe").val(pedido);
                $("#telefono_pe").val(telefono);
                
              }else{
                alert("falta seleccionar revición curricular");
              }
          
            }


            function cambiarApto(input, pedido, persona, ficha, flag){

              var _token=$("#csrf-token").val();
              base_url=$("#base_url").val();
              var urlData=base_url+"matching/apto/"+ficha+"/"+persona+"/"+pedido;
              $.ajax({
                      type:'GET',
                      url:urlData,
                      dataType:'json', 
                    success:function(data) {
                     // alert(data);
                      location.reload();
                    }
                });
            }

            function cambiarContratado(input, pedido, persona, ficha, flag, apto, status){

              if(apto == "1" && status == "1"){
                var _token=$("#csrf-token").val();
                base_url=$("#base_url").val();
                var urlData=base_url+"matching/contratado/"+ficha+"/"+persona+"/"+pedido;
                if (window.confirm("Esta seguro de generar el proceso de colocación?")) {
                $.ajax({
                        type:'GET',
                        url:urlData,
                        dataType:'json', 
                      success:function(data) {
                       // alert(data);
                        location.reload();
                      }
                  });
                }
              }else{
                alert("falta seleccionar revición curricular / proceso de colocación");
              }
              
            }
        </script>
    </div>

    <br>
 

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enviar Mensaje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="fecha">Fecha de Cita</label>
                <input type="hidden" name="ficha_pe" id="ficha_pe">
                <input type="hidden" name="persona_pe" id="persona_pe">
                <input type="hidden" name="pedido_pe" id="pedido_pe">
                <input type="hidden" name="telefono_pe" id="telefono_pe">
                <input type="date" class="form-control" id="fecha" aria-describedby="emailHelp" placeholder="Fecha">
              </div>
              <div class="form-group">
                <label for="hora">Hora de la Cita</label>
                <input type="time" class="form-control" id="hora" placeholder="Password">
              </div>
            </form>     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onClick="enviarMensaje()">Enviar</button>
      </div>
    </div>
  </div>
</div>
  @endsection
