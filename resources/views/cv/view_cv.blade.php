<!DOCTYPE html>
<!-- saved from url=(0040)http://www.jovenesproductivos.gob.pe/cv/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Required meta tags -->
   <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
  <link rel="stylesheet" href="{{ asset('css/portal.css') }}" >

  <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">

    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
   
    <title>Crea tu CV</title>
    <style type="text/css">
      .btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    border-radius: 35px;
    font-size: 24px;
    line-height: 1.33;
        margin-right: 50px;
    margin-bottom: 10px;
}

.btn-circle {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
        margin-right: 50px;
    margin-bottom: 10px;
}
.color_letra{
  color: white;
   
}
.background_color{
     background-color: #17a2b8;
   
}
.btn-default{
      color: black;
    background-color: #eaf3f4;;
    border-color:#eaf3f4;;
}
.btn.btn-file>input[type='file'] {
    position: absolute;
    top: 0;
    right: 0;
   
    font-size: 100px;
    text-align: right;
    opacity: 0;
    filter: alpha(opacity=0);
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.forulario .form-group{

margin-bottom: 0.2rem !important;
}
.background_color div{
  font-size: 23px
}
.sumTitle{
  color: #17a2b8!important;
}
#footer {
   position:fixed;
   left:0px;
   bottom:0px;
   height:30px;
   width:100%;
}

 .ui-widget-content {
     border: 0px solid #aaaaaa;
}
.mje, .mje2, .mje3, .mje4 {
 
    float: left;
}
    </style>
    <style type="text/css">body{ font-family: Verdana; font-size: 9 pt; color: #2A2B2A; margin: 0px }a:active   { font-variant: normal; font-size: 8 pt; font-family:Verdana, Arial, Helvetica, sans-serif; text-decoration: none;color: #0095CD; font-weight: bold }a:link { font-variant: normal; font-size: 8 pt; font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #0095CD; font-weight: bold }a:visited    { font-variant: normal; font-size: 8 pt; font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #0095CD; font-weight: bold }a:hover      { font-variant: normal; font-size: 8 pt; font-family: 
               Verdana, Arial, Helvetica, sans-serif; color: #FF0000; 
               font-weight: bold } .color_final  th,.color_final td{ color:red}
 li.MsoNormal
  {mso-style-parent:"";
  margin-bottom:.0001pt;
  font-size:11.0pt;
  font-family:"Calibri","sans-serif";
  margin-left:0cm; margin-right:0cm; margin-top:0cm}
 table.MsoNormalTable
  {mso-style-parent:"";
  font-size:10.0pt;
  font-family:"Times New Roman","serif"}
td{
  font-family:Verdana, Geneva, sans-serif;
  font-size:12px;
  text-align:justify;
}
.tit{
  font-family:Verdana, Geneva, sans-serif;
  font-size:14px;
  color:#039;
  font-weight:bold;
}
.tit-a{
  font-family:Verdana, Geneva, sans-serif;
  font-size:14px;
  color:#333;
  font-weight:bold;
  text-align:center;
}
  table {
  border-collapse: collapse;
} table .border_border{
  border-top: blue  1px solid;
}.rightd{text-align:right;}@media screen {  .no_mostrar { display: none; } .no_imprimir { display: block; }   }      
@media print {   .no_mostrar { display: block; } .no_imprimir { display: none; }    }
/*.border_table{
      border: 1px solid black;
      border-radius: 5px;
}*/
.color_letra2{
    color: #003dc7;
    font-size: 18px;
}
.background_color{
     background-color: #003dc7 !important;
   
}
.btn_file {
    width: 150px;
    height: 150px;
    background-color: #fefeff;
    border: 1px solid #17a2b8;
    text-align: center;
    text-transform: uppercase;
}
</style>
  </head>
  <body  style="background: #dedddd">

<div class="container">
<body bgcolor="#DADADA">
<div align="center">
<table border="0" width="800" cellspacing="1" cellpadding="2" bgcolor="#0063BE">
  <tbody><tr>
    <td bgcolor="#FFFFFF" valign="top">
    <table border="0" cellpadding="3" cellspacing="3" width="800">
        <tbody>
        <tr>
          <td align="center" valign="top" bgcolor="#FFFFFF" width="800">
<table border="0" cellpadding="3" cellspacing="3" width="95%" >
        <tbody>
        <tr>
          <td width="5%" ></td>
          <td align="center" valign="top" bgcolor="#FFFFFF" width="95%" >
          <table style="width: 100%" border="0">
            <tr>
              <td colspan="5"> 
                <table width="100%" border="0">
                  <tr>
                   
                    <td>
                      <h3 class="color_letra border_table background_color">
                        <div>&nbsp; &nbsp;&nbsp;Datos Personales </div>
                      </h3>
                    </td>
                    <td width="30%"><div class="no_imprimir"><a href="javascript:window.print()">Imprimir</a><a></a></div></td>
                  </tr>
                </table>
                
              </td>
            </tr>
            <tr>
              <td width="70%">
                <table border="0" width="100%">
              <tr>
              <td width="10%"></td>
                  <td colspan="3">
                    <b class="color_letra2">
               {{isset($informacionPersonal['txtNombres'])?$informacionPersonal['txtNombres']:''}}
               </b> </td>
               
                </tr>
            <tr>
               <td ></td>
              <td width="50%">DNI N°: </td>
              <td>{{isset($informacionPersonal['txtDNI'])?$informacionPersonal['txtDNI']:''}}</td>
              <td colspan="3"></td>
            </tr>
            <tr> <td ></td>
              <td>Edad: </td>
              <td>{{isset($informacionPersonal['txtEdad'])?$informacionPersonal['txtEdad']:''}}</td>
            </tr>
            <tr> <td ></td>
              <td valign="top">Dirección: </td>
              <td>{{isset($informacionPersonal['txtDireccion'])?$informacionPersonal['txtDireccion']:''}}</td>
            </tr>
            <tr> <td ></td>
              <td>Número de Teléfono: </td>
              <td>{{isset($informacionPersonal['txtTelf'])?$informacionPersonal['txtTelf']:''}}</td>
            </tr>
            <tr> <td ></td>
              <td>Correo Electrónico: </td>
              <td>{{isset($informacionPersonal['txtEmail'])?$informacionPersonal['txtEmail']:''}}</td>
            </tr>
            </table>
              </td>
              <td width="30%">
                <!-- <table>
                  <tr>
                    <td>
                      <label  class="btn_file">
                        <img src="{{isset($informacionPersonal['txtFoto'])?$informacionPersonal['txtFoto']:''}}" style="height: 150px;">
                      </label>
                      
                    </td>
                  </tr>
                </table> -->
              </td>
            </tr>
          </table>
      
        <table style="width: 100%" border="0" >
          <tr>
            <td colspan="3"> <h3 class="color_letra border_table background_color">
                          <div>&nbsp; &nbsp; &nbsp;Experiencia Laboral</div></h3></td>
          </tr>
          <?php
      if (!isset($informacionPersonal['chkExp1'])) {
        $x1=(isset($informacionPersonal['chkExp1'])?$informacionPersonal['chkExp1']:'off');
        $nulls=isset($informacionPersonal['txtEmpresa1'])?$informacionPersonal['txtEmpresa1']:'';
        if ($x1=='off' && $nulls!='') {
        ?>
          <tr>
            <td colspan="3">
              <table width="100%" border="0">
                <tr>
                    <td width="7%"></td>
                    <td colspan="2"> <h3 class="color_letra2">1° Experiencia Laboral</h3></td>
                  </tr>
                     <tr>
                      <td width="5%"></td>
                    <td width="35%">Nombre de la Empresa: </td>
                    <td>{{isset($informacionPersonal['txtEmpresa1'])?$informacionPersonal['txtEmpresa1']:''}}</td>
                  </tr>
                  <tr><td width="5%"></td>
                    <td valign="top">Cargo que Desempeño: </td>
                    <td>{{isset($informacionPersonal['txtCargo1'])?$informacionPersonal['txtCargo1']:''}}</td>
                  </tr>
                  <tr><td width="5%"></td>
                    <td valign="top">Funciones Desempeñadas: </td>
                    <td><?php echo nl2br(isset($informacionPersonal['txtFunciones1x'])?$informacionPersonal['txtFunciones1x']:'')?></td>
                  </tr>
                  <tr><td width="5%"></td>
                    <td>Fecha de Inicio: </td>
                    <td>{{isset($informacionPersonal['txtDesde1'])?$informacionPersonal['txtDesde1']:''}}</td>
                          </tr>
                  <tr><td width="5%"></td>
                    <td>Fecha de Termino: </td>
                    <td>{{isset($informacionPersonal['txtHasta1'])?$informacionPersonal['txtHasta1']:''}}</td>
                  </tr>
              </table>
            </td>
          </tr>
    <?php
    }
    }
    ?>
    <?php

      if (!isset($informacionPersonal['chkExp2'])) {
        $x2=(isset($informacionPersonal['chkExp2'])?$informacionPersonal['chkExp2']:'off');
         $nulls=isset($informacionPersonal['txtEmpresa2'])?$informacionPersonal['txtEmpresa2']:'';
        if ($x1=='off' && $nulls!='') {
        ?>
         <tr>
            <td colspan="3">

        <table width="100%" border="0">  
          <tr>
            <td width="7%"></td>
            <td colspan="2"> <h3 class="color_letra2">2° Experiencia Laboral</h3></td>
          </tr>
          
          <tr><td width="5%"></td>
            <td width="35%">Nombre de la Empresa: </td>
            <td valign="top">{{isset($informacionPersonal['txtEmpresa2'])?$informacionPersonal['txtEmpresa2']:''}}</td>
          </tr>
          <tr><td width="5%"></td>
            <td  valign="top">Cargo que Desempeño: </td>
            <td>{{isset($informacionPersonal['txtCargo2'])?$informacionPersonal['txtCargo2']:''}}</td>
          </tr>
          <tr><td width="5%"></td>
            <td valign="top">Funciones Desempeñadas: </td>
            <td><?php echo nl2br(isset($informacionPersonal['txtFunciones2x'])?$informacionPersonal['txtFunciones2x']:'')?></td>
          </tr>
          <tr><td width="5%"></td>
            <td>Fecha de Inicio: </td>
            <td>{{isset($informacionPersonal['txtDesde2'])?$informacionPersonal['txtDesde2']:''}}</td>
          </tr>
          <tr><td width="5%"></td>
            <td>Fecha de Termino: </td>
            <td>{{isset($informacionPersonal['txtHasta2'])?$informacionPersonal['txtHasta2']:''}}</td>
          </tr>
        </table>

          </td>
        </tr>
        <?php
}
}
        ?>
          <?php
      if (!isset($informacionPersonal['chkExp3'])) {
        $nulls=isset($informacionPersonal['txtEmpresa3'])?$informacionPersonal['txtEmpresa3']:'';
        if ($x1=='off' && $nulls!='') {
        ?>
         <tr>
            <td colspan="3">

        <table width="100%" border="0">  
          <tr>
            <td width="7%"></td>
            <td colspan="2"> <h3 class="color_letra2">3° Experiencia Laboral</h3></td>
          </tr>
          
          <tr><td width="5%"></td>
            <td  width="35%" valign="top">Nombre de la Empresa: </td>
            <td>{{isset($informacionPersonal['txtEmpresa3'])?$informacionPersonal['txtEmpresa3']:''}}</td>
          </tr>
          <tr><td width="5%"></td>
            <td width="25%" valign="top">Cargo que Desempeño: </td>
            <td>{{isset($informacionPersonal['txtCargo3'])?$informacionPersonal['txtCargo3']:''}}</td>
          </tr>
          <tr><td width="5%"></td>
            <td valign="top" valign="top">Funciones Desempeñadas: </td>
            <td><?php echo nl2br(isset($informacionPersonal['txtFunciones3x'])?$informacionPersonal['txtFunciones3x']:'')?></td>
          </tr>
          <tr><td width="5%"></td>
            <td>Fecha de Inicio: </td>
            <td>{{isset($informacionPersonal['txtDesde3'])?$informacionPersonal['txtDesde3']:''}}</td>
          </tr>
          <tr><td width="5%"></td>
            <td>Fecha de Termino: </td>
            <td>{{isset($informacionPersonal['txtHasta3'])?$informacionPersonal['txtHasta3']:''}}</td>
          </tr>
        </table>

          </td>
        </tr>
        <?php
}
}
        ?>
        </table> 
        <table style="width: 100%">
        <tr>
          <td colspan="3"> <h3 class="color_letra border_table background_color">
                          <div>&nbsp; &nbsp; &nbsp;Formación Académica</div></h3></td>
        </tr>
        <tr>
            <td colspan="3">
              <table width="100%" border="0">
    <?php
      if (!isset($informacionPersonal['chkSup'])) {
        $x123=(isset($informacionPersonal['chkSup'])?$informacionPersonal['chkSup']:'off');
        if ($x123=='off') {
        ?>           
         <tr>
          <td width="7%"></td>
          <td colspan="2"> <h3 class="color_letra2">Educación Superior</h3></td>
        </tr>
       <tr>
        <td width="5%"></td>
          <td width="35%">Carrera Técnica o Profecional: </td>
          <td>{{isset($informacionPersonal['txtCarrera'])?$informacionPersonal['txtCarrera']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Institución Educativa: </td>
          <td>{{isset($informacionPersonal['txtInsTec'])?$informacionPersonal['txtInsTec']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Fecha de Inicio: </td>
          <td>{{isset($informacionPersonal['txtDesdeTec1'])?$informacionPersonal['txtDesdeTec1']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Fecha de Termino: </td>
          <td>{{isset($informacionPersonal['txtHastaTec1'])?$informacionPersonal['txtHastaTec1']:''}}</td>
        </tr>
<?php
}
}
      if (!isset($informacionPersonal['chkOtros'])) {
        $x12=(isset($informacionPersonal['chkOtros'])?$informacionPersonal['chkOtros']:'off');
        if ($x12=='off') {
        ?>
        <tr><td width="7%"></td>
          <td colspan="2"> <h3 class="color_letra2">Otros Estudios</h3></td>
        </tr>
       <tr><td width="5%"></td>
          <td width="35%">Entidad de Capacitación.</td>
          <td>{{isset($informacionPersonal['txtECAP'])?$informacionPersonal['txtECAP']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Curso: </td>
          <td>{{isset($informacionPersonal['txtCurso'])?$informacionPersonal['txtCurso']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Fecha de Inicio: </td>
          <td>{{isset($informacionPersonal['txtDesdeOtros'])?$informacionPersonal['txtDesdeOtros']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Fecha de Termino: </td>
          <td>{{isset($informacionPersonal['txtHastaOtros'])?$informacionPersonal['txtHastaOtros']:''}}</td>
        </tr>   
<?php
}
}
?>

         <tr><td width="7%"></td>
          <td colspan="2"> <h3 class="color_letra2">Educación Secundaria</h3></td>
        </tr>
       <tr><td width="5%"></td>
          <td width="35%">Institución Educativa.</td>
          <td>{{isset($informacionPersonal['txtInsSec'])?$informacionPersonal['txtInsSec']:''}}</td>
        </tr>
       
        <tr><td width="5%"></td>
          <td>Fecha de Inicio: </td>
          <td>{{isset($informacionPersonal['txtDesdeSec'])?$informacionPersonal['txtDesdeSec']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Fecha de Termino: </td>
          <td>{{isset($informacionPersonal['txtHastaSec'])?$informacionPersonal['txtHastaSec']:''}}</td>
        </tr>

         <tr><td width="7%"></td>
          <td colspan="2"> <h3 class="color_letra2">Educación Primaria</h3></td>
        </tr>
       <tr><td width="5%"></td>
          <td width="35%">Institución Educativa.</td>
          <td>{{isset($informacionPersonal['txtInsPrim'])?$informacionPersonal['txtInsPrim']:''}}</td>
        </tr>
       
        <tr><td width="5%"></td>
          <td>Fecha de Inicio: </td>
          <td>{{isset($informacionPersonal['txtDesdePrim'])?$informacionPersonal['txtDesdePrim']:''}}</td>
        </tr>
        <tr><td width="5%"></td>
          <td>Fecha de Termino: </td>
          <td>{{isset($informacionPersonal['txtHastaPrim'])?$informacionPersonal['txtHastaPrim']:''}}</td>
        </tr>

        </table>
      </td>
      </tr>
      </table>
      <table style="width: 100%">
        <tr>
          <td colspan="3"> <h3 class="color_letra border_table background_color">
                          <div>&nbsp; &nbsp; &nbsp;Referencias Personales</div></h3></td>
        </tr>
       <tr>
            <td colspan="3">
              <table width="100%" border="0">
              <tr> <td width="7%"></td>
                <td width="33%">Nombre de la Persona Referente:</td>
                <td>{{isset($informacionPersonal['txtRefPers'])?$informacionPersonal['txtRefPers']:''}}</td>
              </tr>
              <tr> <td width="5%"></td>
                <td>Cargo y Nombre de la Organización:</td>
                <td>{{isset($informacionPersonal['txtRefOrg'])?$informacionPersonal['txtRefOrg']:''}}</td>
              </tr>
              <tr> <td width="5%"></td>
                <td>Teléfono de Contacto:</td>
                <td>{{isset($informacionPersonal['txtRefTelf'])?$informacionPersonal['txtRefTelf']:''}}</td>
              </tr>
            </table>
      </td>
      </tr>
      </table>

        </td>
      </tr>
    </tbody>
      </table>

   </td></tr> </tbody> 
  </table></td></tr></tbody>
          
          </table>
        </div>
  </div>
  
</div>
  <br>
  <br>
  <br>
  <br>

 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

    <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
 
  <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script defer="" src="js/fontawesome-all.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
  
</body></html>