<!DOCTYPE html>
<!-- saved from url=(0040)http://www.jovenesproductivos.gob.pe/cv/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Required meta tags -->
   <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
  <link rel="stylesheet" href="{{ asset('css/portal.css') }}" >

  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/accede-custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/accede-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
   
    <title>Crea tu CV</title>
    <style type="text/css">
      .btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    border-radius: 35px;
    font-size: 24px;
    line-height: 1.33;
        margin-right: 50px;
    margin-bottom: 10px;
}

.btn-circle {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
        margin-right: 50px;
    margin-bottom: 10px;
    border-color: #003dc7 !important;
    background-color: white!important;
}
.color_letra{
  color: #00a4d3;
   
}
.background_color{
     background-color: #003dc7 !important;
   
}
.border_color{
  background-color: #003dc7 !important;

}
.border_color h3{
  color: white;
  font-size: 20px !important;
}
.btn-default{
      color: black;
    background-color: #eaf3f4;;
    border-color:#eaf3f4;;
}
.btn.btn-file>input[type='file'] {
    position: absolute;
    top: 0;
    right: 0;
   
    font-size: 100px;
    text-align: right;
    opacity: 0;
    filter: alpha(opacity=0);
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.forulario .form-group{
    border-color: #fff;
margin-bottom: 0.2rem !important;
}
.sumTitle{
  color: #17a2b8!important;
}
#footer {
   position:fixed;
   left:0px;
   bottom:0px;
   height:40px;
   width:100%;
}
header{
  position:fixed;
   left:0px;
  margin-top: 0px;
   height:40px;
   width:100%;
}

 .ui-widget-content {
     border: 0px solid #aaaaaa;
}
.mje, .mje2, .mje3, .mje4 {
 
    float: left;
}
.form-control {
  border-:2px solid  #17a2b8;
    outline: 0;
    box-shadow: none;
    border: #fff;

  border-bottom:1px solid  #17a2b8;

    -webkit-box-shadow: none;
    box-shadow: none;
}

.form-control::-webkit-input-placeholder { text-transform: uppercase; } 
 
.form-control::-moz-placeholder {  text-transform: uppercase;} 
 
.form-control::-moz-placeholder {text-transform: uppercase; } 
 
.form-control::-ms-input-placeholder { text-transform: uppercase; }
.form-control:focus::-webkit-input-placeholder{color:transparent;}
.form-control:focus {
 border-bottom:2px solid  #17a2b8;
    outline: 0;
    box-shadow: none;
    border-color: #fff;
    border-bottom-color: #17a2b8; 
}
.textopequenio{
  font-size: 10px
}
.btn_color{
   background-color: #003dc7 !important;
   color: white
}
.btn_file{
  width:150px; height: 150px;
  background-color: #fefeff;
  border: 1px solid #17a2b8;
  text-align: center;
  text-transform: uppercase;
}
    </style>
  </head>
  <body style="background: #dedddd">
 <header class="background_color" id="color_head">
  
  </header>

<div style="background: white; ">
	<div class="container-fluid">
    <div class="text-center">
      <img src="{{asset('logo.png')}}" style="margin-top: 40px;width: 180px">
       
        <h3 class="color_letra">CREA TU CURRICULUM VITAE<p></p></h3>

      <button id="btn1" type="button" class="btn btn_color  btn-circle">1</button>
      <button id="btn2" type="button" class="btn  btn-default btn-circle">2</button>
      <button id="btn3" type="button" class="btn btn-default  btn-circle">3</button>
      <button id="btn4" type="button" class="btn btn-default  btn-circle">4</button>

       
      

    </div>
  </div>
</div>
  <div style="background: white; ">
   <div class="container-fluid">
    
    <div class="content" >
       
      <form id="frmPreview" enctype="multipart/form-data"  method="POST">
        <!--action="preview.php" -->
      <input type="hidden" name="_id" id="_id" value="">
      <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
      <div class="clearfix"></div>
      <div class="tab-content" id="nav-tabContent">
   
          <div class="tab-pane fade show active" id="nav-personales" role="tabpanel" aria-labelledby="nav-personales-tab">
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8 border_color">
                <h3 class="color_letra">Datos Personales</h3> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-3"></div>
           <!--    <div class="col-sm-3"> -->
             <!--    <div class="row">
                  <div class="col-md-6"><br>
                    <label class="btn btn-sm btn-file btn_file" title="" id="txtFotoIns_logo" >
                        <label class="removerText" style="margin-top: 40px"><i class="fa fa-upload"></i>Foto <br>(Opcional)</label>
                        <input style="width:150px; height: 150px;" type="file" id="image" name="image" onchange="encodeImagetoBase64(this)">
                        <img src="" id="fotoCV"  style="height: 100px; border-radius: 5px;" class="img-responsive">
                     
                      <textarea class="form-control limpiar" name="txtFoto" id="txtFoto" style="display:none;"></textarea>
                        </label>  
                  </div>
                  <div class="col-md-6"><br>
                    <label class="textopequenio"><span>Tamaño Máximo de las Imagen 200 kb</span><span>Formato: jpg, png,gif</span><span>Medida:No mayor de 640 x 840 px</span></label>
                    <label><label class="btn btn-sm btn-file btn-info" style="text-align: center;" title="" id="txtFotoIns_logo">
                        <i class="fa fa-upload"></i>Subir Foto
                        <input style="width:150px; height: 150px;" type="file" id="image2" name="image2" onchange="encodeImagetoBase64(this)">
                        </label>  </label>
                    <label style="margin-top: 3px"><button class="btn-sm btn btn-info">Eliminar Foto</button></label>
                  </div>
                </div> -->
                
                      
               <!--          <label id="chkFoto">
                           <input type="checkbox" name="chkFoto" id="chkFoto"> <small>No deseo colocar una foto</small>
                        </label>
                        
              </div> -->
              <div class="col-sm-7">

                <div class="form forulario">

                  <div class="form-group row">
                    
                    <div class="col-sm-8">
                      <input type="text" class="form-control " name="txtDNI" id="txtDNI" maxlength="8" placeholder="Ingresa tu número de DNI" onchange="getBuscarDni(this.value)">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8">
                      <input type="text" class="form-control limpiar" name="txtNombres" id="txtNombres" placeholder="Ingresa tus Nombres y Apellidos">
                    </div>
                  </div>
                  <div class="form-group row">
                     <div class="col-sm-8">
                      <input type="text" class="form-control limpiar" id="txtEdad" name="txtEdad" maxlength="2" placeholder="Ingrese tu Edad">
                    </div>
                  </div>
                  <div class="form-group row">
                   
                    <div class="col-sm-8">
                      <textarea class="form-control limpiar" id="txtDireccion" name="txtDireccion" placeholder="Ingresa tu Dirrección" rows="1" style="height: 40px;"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8">
                      <input type="text" class="form-control limpiar" id="txtTelf" name="txtTelf" placeholder="Ingrese tu Número telefóno">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8">
                      <input type="email" class="form-control limpiar" id="txtEmail" name="txtEmail" placeholder="Ingresa tu Correo Elecctronico">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8">
                      <textarea class="form-control limpiar" id="txtResumen" name="txtResumen" placeholder="Ingresa tu resumen o perfil profesional"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12 text-center">
                      <span class="mje"><small>* Debe ingresar los datos correctamente.</small></span>
                      <br>
                      <a href="#" class="btn btn-info" id="btnContinuar1" style="; color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Siguiente</a>
                      <br><br>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div> 
    
       

          <div class="tab-pane fade forulario" id="nav-laboral" role="tabpanel" aria-labelledby="nav-laboral-tab">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-10 border_color">
                <h3 class="color_letra">Experiencia Laboral</h3> 
              </div>
            </div>
            <div class="row form">
               <div class="col-md-1"></div>
              <div class="col-sm-5">
                 <h3 class="color_letra">1° Experiencia Laboral</h3> 
                 <div class="form-group row">
                    <div class="col-sm-12">
                      <input type="text" class="form-control limpiar" id="txtEmpresa1" name="txtEmpresa1" placeholder="Nombre de la empresa">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <input type="text" class="form-control limpiar" id="txtCargo1" name="txtCargo1" placeholder="Cargo que desempeñó">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea class="form-control limpiar" id="txtFunciones1x" name="txtFunciones1x" placeholder="Funciones desempeñadas"></textarea>
                      <textarea class="form-control limpiar" id="txtFunciones1" name="txtFunciones1" style="display: none;" placeholder="Funciones desempeñadas"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control limpiar date-pickerf" readonly="readonly" id="txtDesde1" name="txtDesde1" placeholder="Fecha de Inicio"> 
                        </div>
                        <div class="col-md-6">
                          <input type="text" class="form-control limpiar date-pickerf"  readonly="readonly" id="txtHasta1" name="txtHasta1" placeholder="Fecha Final">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <input type="checkbox" name="chkExp1" id="chkExp1"> <small>No cuento con experiencia laboral</small><br>
                        </div>
                      </div>
                    </div>
                  </div>
                <h3 class="color_letra">3° Experiencia Laboral</h3> 
                <div class="form">                     
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <input type="text" class="form-control limpiar" id="txtEmpresa3" name="txtEmpresa3" placeholder="Nombre de la empresa">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <input type="text" class="form-control limpiar" id="txtCargo3" name="txtCargo3" placeholder="Cargo que desempeñó">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea class="form-control limpiar" id="txtFunciones3x" name="txtFunciones3x" placeholder="Funciones desempeñadas"></textarea>
                      <textarea class="form-control limpiar" id="txtFunciones3" name="txtFunciones3" style="display: none;" placeholder="Funciones desempeñadas"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control limpiar date-pickerf " style="background: rgb(255, 255, 255);" readonly="readonly" id="txtDesde3" name="txtDesde3" placeholder="fecha de inicio"> 
                        </div>
                        <div class="col-md-6">
                          <input type="text" class="form-control limpiar date-pickerf" style="background: rgb(255, 255, 255);" readonly="readonly" id="txtHasta3" name="txtHasta3" placeholder="fecha final">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <input type="checkbox" name="chkExp3" id="chkExp3"> <small>No cuento con una segunda experiencia laboral</small><br>
                        </div>
                      </div>
                    </div>
                  </div>
                  <span class="mje2" style="font-weight: bold; float: right;"><small>* Debe ingresar los datos correctamente.</small></span>
                </div>
              </div>
              <div class="col-sm-5">
               <h3 class="color_letra">2° Experiencia Laboral</h3> 
                <div class="form">                     
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <input type="text" class="form-control limpiar" id="txtEmpresa2" name="txtEmpresa2" placeholder="Nombre de la empresa">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <input type="text" class="form-control limpiar" id="txtCargo2" name="txtCargo2" placeholder="Cargo que desempeñó">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea class="form-control limpiar" id="txtFunciones2x" name="txtFunciones2x" placeholder="Funciones desempeñadas"></textarea>
                      <textarea class="form-control limpiar" id="txtFunciones2" name="txtFunciones2" style="display: none;" placeholder="Funciones desempeñadas"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control limpiar date-pickerf " style="background: rgb(255, 255, 255);" readonly="readonly" id="txtDesde2" name="txtDesde2" placeholder="Fecha de Inicio"> 
                        </div>
                        <div class="col-md-6">
                          <input type="text" class="form-control limpiar date-pickerf" style="background: rgb(255, 255, 255);" readonly="readonly" id="txtHasta2" name="txtHasta2" placeholder="Fecha Final">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <input type="checkbox" name="chkExp2" id="chkExp2"> <small>No cuento con una segunda experiencia laboral</small><br>
                        </div>
                      </div>
                    </div>
                  </div>
                   
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-sm-12 text-center">
                    
                      <br>
                      
                      <a href="#" class="btn btn-info" id="btnRegresar2" style="; color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Anterior</a>
                      <a href="#" class="btn btn-info" id="btnContinuar2" style="; color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Siguiente</a>
                      <br><br>
               
                         
               
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade forulario" id="nav-academica" role="tabpanel" aria-labelledby="nav-academica-tab">
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-10 border_color">
                <h3 class="color_letra">Formación Académica</h3> 
              </div>
     
            </div>
            <div class="container">
              <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-5">
              <div class="form">
                 <div class="primario">
                    <div class="col-sm-12">
                        <h3 class="color_letra">Educación Primaria</h3>
                    </div>   
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input type="text" class="form-control limpiar" id="txtInsPrim" name="txtInsPrim" placeholder="Nombre de la institución educativa">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-md-6">
                            <input type="text" class="form-control limpiar date-pickerf2 " id="txtDesdePrim" style="background: white;" readonly="readonly" name="txtDesdePrim" placeholder="año"> 
                          </div>
                          <div class="col-md-6">
                            <input type="text" class="form-control limpiar date-pickerf2 
                            " id="txtHastaPrim" style="background: white;" readonly="readonly" name="txtHastaPrim" placeholder="año">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                 
                 <div class="superior">
                    <div class="col-sm-12">
                        <h3 class="color_letra">Estudios de nivel superior</h3>
                    </div>   
                    <div class="form-group ">
                        <input type="text" class="form-control limpiar" id="txtCarrera" name="txtCarrera" placeholder="Nombre de la carrera técnica o profesional">  
                    </div>
                    <div class="form-group ">
                        <input type="text" class="form-control limpiar" id="txtInsTec" name="txtInsTec" placeholder="Institución educativa">
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 ">
                        <table width="100%">
                          <tr>
                            <td>
                              <input type="text" class="form-control limpiar date-pickerf " style="background: white;" readonly="readonly" id="txtDesdeTec1" name="txtDesdeTec1" placeholder="mes/año"> 
                            </td>
                            <td>
                               <input type="text" class="form-control limpiar date-pickerf " style="background: white;" readonly="readonly" id="txtHastaTec1" name="txtHastaTec1" placeholder="mes/año">
                            </td>
                          </tr>
                        </table>
                        
                        <div class="row">
                          <div class="col-md-12">
                            <input type="checkbox" name="chkSup" id="chkSup"> <small>No cuento con estudios de nivel superior</small><br>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
               </div>
              </div>
              <div class="col-sm-5">
                  <div class="form">    
                     <div class="secundaria">
                      <div class="col-sm-12">
                          <h3 class="color_letra">Educación Secundaria</h3>
                      </div>   
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <input type="text" class="form-control limpiar" id="txtInsSec" name="txtInsSec" placeholder="Nombre de la institución educativa">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="row">
                            <div class="col-md-5">
                              <input type="text" class="form-control limpiar date-pickerf2 " id="txtDesdeSec" style="background: white;" readonly="readonly" name="txtDesdeSec" placeholder="año"> 
                            </div>
                            <div class="col-md-5">
                              <input type="text" class="form-control limpiar date-pickerf2 " id="txtHastaSec" name="txtHastaSec" style="background: white;" readonly="readonly" placeholder="año">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                      <div class="jovenesproductivos">
                        <div class="col-sm-12">
                            <h3 class="color_letra">Otros Estudios</h3>
                        </div>   
                        <div class="form-group row">
                       
                          <div class="col-sm-12">
                            <input type="text" class="form-control limpiar" id="txtECAP" name="txtECAP" placeholder="Entidad de Capacitación" disabled="">
                          </div>
                        </div>
                        <div class="form-group row">
                        
                          <div class="col-sm-12">
                            <input type="text" class="form-control limpiar" id="txtCurso" name="txtCurso" placeholder="Curso" disabled="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-12">
                            <div class="row">
                              <div class="col-md-6">
                                <input type="text" class="form-control limpiar date-pickerf " id="txtDesdeOtros" style="background: white;" readonly="readonly" name="txtDesdeOtros" placeholder="mes/año" disabled=""> 
                              </div>
                              <div class="col-md-6">
                                <input type="text" class="form-control limpiar date-pickerf " id="txtHastaOtros" style="background: white;" readonly="readonly" name="txtHastaOtros" placeholder="mes/año" disabled="">
                              </div>
                            </div>
                            <div class="row otrosest">
                              <div class="col-md-12">
                                <input type="checkbox" name="chkOtros" id="chkOtros"> <small>No cuento con otros estudios</small><br>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
              
              <div class="col-sm-12">
                    <div class=" text-center">
                      <span class="mje3" style="left: right"><small>* Debe ingresar los datos correctamente.</small></span>
                      <br>
                           <a href="#" class="btn btn-info" id="btnRegresar3" style=" color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Anterior</a>
                      <a href="#" class="btn btn-info" id="btnContinuar3" style=" color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Siguiente</a>
                 <br><br>
                 <br><br>
                    </div>
                  </div>
            </div> 
            </div>
            
          </div>
          <div class="tab-pane fade forulario" id="nav-referencias" role="tabpanel" aria-labelledby="nav-referencias-tab">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6 border_color">
                <h3 class="color_letra">Referencias Personales</h3> 
              </div>
     
            </div>
            <div class="row">
              <div class="col-sm-3">
               
              </div>
              <div class="col-sm-6">
                  <div class="form">
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input type="text" class="form-control limpiar" id="txtRefPers" name="txtRefPers" placeholder="Nombre de la persona referente">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input type="text" class="form-control limpiar" id="txtRefOrg" name="txtRefOrg" placeholder="Cargo y nombre de la organización">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input type="text" class="form-control limpiar" id="txtRefTelf" name="txtRefTelf" placeholder="Teléfono de contacto">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-center">
                        <span class="mje4"><small>* Debe ingresar los datos correctamente.</small></span>
                        <br>
                          <a href="#" class="btn btn-info" id="btnRegresar4" style=" color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Anterior</a>
                        <button type="button" class="btn btn-info" id="btnFinal" style=" color: white; font-weight: bold; text-shadow: 0 0 2px #666666;">Vista previa</button>
                      <br><br>
                      <br><br>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>      
      </form>
    </div>
</div>
 
   
   
  
 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

    <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
 
  <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script defer="" src="js/fontawesome-all.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
  
   



 <footer class="background_color" id="footer">
    <small style="color: white">Todos los derechos reservados 2018</small>
  </footer>
</body></html>