

<br>
    <div class="row  ">
    <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="get" action="{{ route('convocatoria') }}">
                    
                    

                        <input type="hidden" id="base_url" value="{{ asset('') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="titulo_formulario" style="color:black;">
                                    <img src="{{ asset('images/vineta_accede.png') }}" style="width: 35px;height: 35px;">
                                    Búsqueda de Vacante de Empleo
                                </h5>
                            </div>
                        </div>

                                             <!-- DESCRIPCION PEDIDO -->

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-3 col-form-label text-md-right col-form-label-sm">{{ __('Descripcion de la vacante') }}</label>

                            <div class="col-md-9">
                                <input id="descripcion" type="text" class="form-control form-control-sm" name="descripcion" value="{{ old('descripcion') }}" placeholder="Ingrese Descripcion">
                            </div>
                        </div>

                 
                    <!-- POR ZONA DE TRABAJO  -->

                        <div class="form-group row">
                            <label for="especialidad" class="col-md-3 col-form-label text-md-right col-form-label-sm">{{ __('Zona de Trabajo') }}</label>

                            <div class="col-md-3">
                                <?php echo \Form::select('departamento',$lista_depa,'0',array('Class'=>'form-control form-control-sm selected','id'=>'departamento','style'=>'width: 90%','onchange'=>'ComboboxProvincia(this)')) ?>
                            </div>
<!--
                            <div class="col-md-3">
                                
                                 <?php echo \Form::select('provincia',$lista_prov,'0',array('Class'=>'form-control form-control-sm selected','id'=>'provincia','style'=>'width: 90%','onchange'=>'ComboboxDistrito(this)')) ?>
                            </div>


                            <div class="col-md-3">
                              
                                <?php echo \Form::select('distrito',$lista_dist,'0',array('Class'=>'form-control form-control-sm selected','id'=>'distrito','style'=>'width: 90%')) ?>
                            </div>
                            -->
                        </div>

 

               <!--   MODALIDADES DE TRABAJO  -->
               <div class="form-group row">
                         <label for="experiencia" class="col-md-3 col-form-label text-md-right col-form-label-sm">{{ __('Area') }}</label>
                         <div class="col-sm-4">
                             <select id="area" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Area" name="area">
                                <option value="">Seleccione</option>
                                @foreach($areas as $area)
                                <option value="{{ $area->id }}">{{ $area->area_descripcion }}</option>
                                @endforeach
                            </select>
                         </div>

                         <label class="col-md-2 col-form-label col-form-label-sm text-md-right">{{ __('Modalidades') }}</label>
                            
                            <div class="col-md-3">
                                <select class="form-control form-control-sm" name="modalidades" value="{{ old('modalidades') }}" id="modalidades"> 
                                    <option value="">Seleccione</option>
                                    <option value="1">Permanente</option>
                                    <option value="2">Eventual</option>
                                    <option value="3">Bajo Cualquier Modalidad</option>
                                </select>
                                <span class="modalidades" style="color:red; font-size:12px;"></span>
                            </div>


                     </div>   

                         <!--   HORARIO DE TRABAJO  -->

                         <div class="form-group row">
                          <!--  
                            <label class="col-md-3 col-form-label col-form-label-sm text-md-right">{{ __('Horario de Trabajo') }}</label>
                            
                            <div class="col-md-3">
                                <select class="form-control form-control-sm" name="horario_trabajo" value="{{ old('horario_trabajo') }}" id="horario_trabajo"> 
                                    <option value="">Seleccione</option>
                                    <option value="1">A Tiempo Completo</option>
                                    <option value="2">A Tiempo Parcial</option>
                                    <option value="3">tarde</option>
                                    <option value="4">Bajo Cualquier Modalidad</option>
                                </select>
                                <span class="horario_trabajo" style="color:red; font-size:12px;"></span>
                            </div>

                            -->
                            <label class="col-md-3 col-form-label col-form-label-sm text-md-right">{{ __('Turno Trabajo') }}</label>
                            
                            <div class="col-md-3">
                                <select class="form-control form-control-sm" name="turno_trabajo" value="{{ old('turno_trabajo') }}" id="turno_trabajo" > 
                                    <option value="">Seleccione</option>
                                    <option value="1">Fijo</option>
                                    <option value="2">Rotativo</option>
                                </select>
                                <span class="turno_trabajo" style="color:red; font-size:12px;"></span>
                            </div>


                        </div>

 


                    <!--CREAR CUENTA -->
                    <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Buscar Vacante') }}
                                </button>
                            </div>
                        </div>
<script type="text/javascript">
    function ComboboxProvincia(idc){
        var base_url=$("#base_url").val();
        var comxDeparment=$("#"+idc.id).val();
        var urlData=base_url+"ubigeo/provincia/"+comxDeparment;
        $.ajax({
                type:'GET',
                url:urlData,
                dataType:'JSON',  
                 beforeSend: function(){
               $(".loader").css({display:'block'});
              }      
        }).done(function( data, textStatus, jqXHR ){

                var texto='';
                texto+='<option value="00">*SELECCIÓNE*</option>';     
                text='<option value="00">*SELECCIÓNE*</option>';     
                if(data['success']!="false" && data['success']!=false){
                    if(typeof(data['ResulProvincia'])!='undefined'){
                      $.each(data['ResulProvincia'], function(i, field){
                        texto+='<option value="'+field.ubi_codprov+'">'+field.ubi_descripcion+'</option>';     
                      });
                      $('#provincia').html(texto); 
                       $('#distrito').html(text);  
                    }else{
                      $('#provincia').html(texto);
                      $('#distrito').html(text);
                    }
                }else{
                  $('#provincia').html(text);
                  $('#distrito').html(text);
                }
            $(".loader").css({display:'none'});
          }).fail(function( jqXHR, textStatus, errorThrown ){
            mensaje='Solicitud fallida: ' + textStatus;
            $(".mensajeGlobal").SendMessajeAlert({
                messaje:mensaje+textStatus,
                dataType:'Error',
                type:'danger',
                time:6000
            });
             $(".loader").css({display:'none'});
          }); 
    }
    function ComboboxDistrito(){
           var base_url=$("#base_url").val();
          var comxDeparment=$("#departamento").val();
          var comxProvin = $("#provincia").val();
          var urlData=base_url+"ubigeo/distrito/"+comxDeparment+'/'+comxProvin;
          $.ajax({
                type:'GET',
                url:urlData,
                dataType:'JSON', 
                 beforeSend: function(){
               $(".loader").css({display:'block'});
              }      
            }).done(function( data, textStatus, jqXHR ){
                var texto='';
                texto+='<option value="00">*SELECCIÓNE*</option>';     
                if(data['success']!="false" && data['success']!=false){
                    if(typeof(data['ResulDistrito'])!='undefined'){
                      $.each(data['ResulDistrito'], function(i, field){
                        texto+='<option value="'+field.ubi_coddist+'">'+field.ubi_descripcion+'</option>';     
                      });
                      $('#distrito').html(texto);   
                  }else{
                      $('#distrito').html(texto);
                  }
                }else{
                  $('#distrito').html(texto);
                }
            $(".loader").css({display:'none'});
          }).fail(function( jqXHR, textStatus, errorThrown ){
           mensaje='Solicitud fallida: ' + textStatus;
            $(".mensajeGlobal").SendMessajeAlert({
                messaje:mensaje+textStatus,
                dataType:'Error',
                type:'danger',
                time:6000
            });
             $(".loader").css({display:'none'});
          }); 
    }
</script>
                    </form>

                </diV>
            </div>
        </div>   
    </div>
     
<br>