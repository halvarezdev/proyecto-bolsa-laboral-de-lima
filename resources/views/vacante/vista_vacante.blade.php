<style>
         body{
             background-color: #1A4A84 !important;
      padding-top:2% !important;
      padding-bottom:0px !important;
      padding-left:2% !important;
      padding-right:2% !important;
      }
    .navbar {
        background-color: #fff !important;
    }
      </style>
<div class="card" style="padding-bottom: 15px;">
    <div class="card-body">
    <!-- inicio card -->

        <!--  -->
        <div class=" form-group row justify-content-md-center">
            <div class="col-sm-10" style="background:#00A39C;">
                <h5 class="titulo_formulario ">
                    1. IDENTIFICACIÓN
                </h5>
            </div>
        </div>

        <!-- RUC -->
        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Ruc') }}</label>

            <div class="col-md-6">
            @foreach($empresas as $empre)
                <input id="ruc" type="text" class="form-control form-control-sm" name="ruc" value="{{ $empre->numero_documento }}" disabled >

            @endforeach

            </div>
        </div>

         <!-- Razon Social -->
         <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Razon Social') }}</label>

            <div class="col-md-6">
            @foreach($empresas as $empre)
                <input id="razon_social" type="text" class="form-control form-control-sm" name="razon_social" value="{{ $empre->razon_social }}" disabled >

            @endforeach

            </div>
        </div>

          <!-- PUESTO -->
          <div class=" form-group row justify-content-md-center">
            <div class="col-sm-10" style="background:#00A39C;">
                <h5 class="titulo_formulario ">
                    2. PUESTO
                </h5>
            </div>
        </div>



        <!-- descripcion del pedido -->

        <div class="form-group row">
            <label for="descripcion" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Descripcion del Pedido') }}</label>

            <div class="col-md-6">
                <input id="descripcion" type="text" class="form-control form-control-sm"  
                 name="descripcion" value="{{ (isset($request_db->descripcion) ? $request_db->descripcion : '') }}" 
                 required placeholder="Ingrese Descripcion de Pedido"  onpaste="return false" onkeypress="return soloLetras(event);return validateSpecialCharacters(event)" >
                <span class="descripcion" style="color:red; font-size:12px;"></span>
            </div>

        </div>


        <!-- DENOMINACION DEL PUESTO -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Denominacion del Puesto') }}</label>


            <div class="col-md-6">
                <div class="control-group">
					<select id="denominacion"  class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="denominacion">
                        <option value="">Seleccione</option>
                    @foreach($ocupacion as $ocu)
                    <?php
                        $seleced = "";
                        if(isset($request_db->denominacion) && $ocu->ocup_codigo == $request_db->denominacion){
                           $seleced = "selected";
                        }
                    ?>
                    <option style="width: 500px;" value="{{ $ocu->ocup_codigo }}" <?php echo $seleced; ?>>{{ $ocu->nombre }}</option>
                    @endforeach
                    </select>
                    <span class="denominacion" style="color:red; font-size:12px;"></span>
				</div>
            </div>
        </div>


         <!-- NUMERO DE VANCANTES -->

        <div class="form-group row">
            <label for="num_vacantes" class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Vacantes Requeridas') }}</label>

            <div class="col-md-6">
                <input id="num_vacantes"  onpaste="return false"  onkeypress="return soloNumero(event);" type="text" class="form-control form-control-sm" name="num_vacantes" value="{{ (isset($request_db->num_vacantes) ? $request_db->num_vacantes : '') }}" required placeholder="Ingrese Numero de Vacanetes">
                <span class="num_vacantes" style="color:red; font-size:12px;"></span>
            </div>
        </div>

        <!-- CATEGORIA -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Area') }}</label>

            <div class="col-md-6">
                <div class="control-group">
					<select id="area" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" data-placeholder="Seleccione Area" name="area">
                        <option value="">Seleccione</option>
                        @foreach($areas as $area)
                         <?php
                        $seleced = "";
                        if(isset($request_db->area) && $area->id == $request_db->area){
                           $seleced = "selected";
                        }
                    ?>
                        <option value="{{ $area->id }}" <?php echo $seleced; ?>>{{ $area->area_descripcion }}</option>
                        @endforeach
                    </select>
                    <span class="area" style="color:red; font-size:12px;" ></span>
				</div>
            </div>
        </div>

          <!-- REQUISITOS -->
          <div class=" form-group row justify-content-md-center">
            <div class="col-sm-10" style="background:#00A39C;">
                <h5 class="titulo_formulario ">
                    3. REQUISITOS
                </h5>
            </div>
        </div>


        <!-- estudios formales -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Estudios') }}</label>
            <div class="col-md-6">
                <div class="control-group">
					<select id="estudios_formales" class="form-control form-control-sm" name="estudios_formales" onchange="return variarEstadoTotal(this)">
                        @foreach($grado_intruccion as $grado)
                         <?php
                        $seleced = "";
                        if(isset($request_db->estudios_formales) && $grado->id == $request_db->estudios_formales){
                           $seleced = "selected";
                        }
                    ?>
                        <option value="{{ $grado->id }}" <?php echo $seleced; ?>>{{ $grado->grad_descripcion }}</option>
                        @endforeach
                    </select>
                    <span class="estudios_formales" style="color:red; font-size:12px;"></span>
				</div>
            </div>
        </div>

         <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Nivel Académico') }}</label>
            <input type="hidden" name="grado_edita" id="grado_edita" value="{{  (isset($request_db->grado) ? $request_db->grado : '') }}">
            <div class="col-md-6">
                <select name="grado" id="grado" class="form-control form-control-sm">
                    <option value="">Seleccione</option>
                </select>
                <span class="grado" style="color:red; font-size:12px;"></span>
            </div>
        </div>



        <!-- ESTUDIOS COMPUTACIONALES -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Estudios de Computación') }}</label>

            <div class="col-md-6">
                <div class="row">
                <div class="col">
                <input type="hidden" name="cod_compu1" value="{{ isset($compu_sql[0]->id) ? $compu_sql[0]->id : '' }}">
                    <div class="control-group">
                            <select id="compu1" onchange="verificar_si_existe_studio(1)" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="compu1">
                            <option value="">Seleccione</option>
                            @foreach($computacion as $compu)
                            <?php
                                $seleced = "";
                                if(isset($compu_sql[0]->compu_nom) && $compu->MAESP_CODIGO == $compu_sql[0]->compu_nom){
                                   $seleced = "selected";
                                }
                            ?>
                            <option style="width: 200px;" value="{{ $compu->MAESP_CODIGO }}" <?php echo $seleced; ?>>{{ $compu->MAESC_DESCRIPCION }}</option>
                            @endforeach
                        </select>
                        <span class="num_compu1" style="color:red; font-size:12px;"></span>
                    </div>
                </div>

                <div class="col">
                    <select class="form-control form-control-sm" name="nivel_compu1" id="nivel_compu_1" >

                        <option value="0">Seleccione</option>
                        <option value="1" <?php echo (isset($compu_sql[0]->compu_nivel) && $compu_sql[0]->compu_nivel == '1' ? 'selected' : ''); ?>>Basico</option>
                        <option value="2" <?php echo (isset($compu_sql[0]->compu_nivel) && $compu_sql[0]->compu_nivel == '2' ? 'selected' : ''); ?>>Medio</option>
                        <option value="3" <?php echo (isset($compu_sql[0]->compu_nivel) && $compu_sql[0]->compu_nivel == '3' ? 'selected' : ''); ?>>Avanzado</option>
                    </select>
                </div>
                </div>

            </div>
        </div>


        <!-- compuaiocn 2 -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm"></label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                <input type="hidden" name="cod_compu2" value="{{ isset($compu_sql[1]->id) ? $compu_sql[1]->id : '' }}">
                    <div class="control-group">

                        <select onchange="verificar_si_existe_studio(2)"  id="compu2" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="compu2">
                            <option    value="">Seleccione</option>
                            @foreach($computacion as $compu)
                            <option style="width: 200px;"  value="{{ $compu->MAESP_CODIGO }}" <?php echo (isset($compu_sql[1]->compu_nom) && $compu_sql[1]->compu_nom == $compu->MAESP_CODIGO  ? 'selected' : ''); ?>>{{ $compu->MAESC_DESCRIPCION }}</option>
                            @endforeach
                        </select>
                        <span class="num_compu2" style="color:red; font-size:12px;"></span>
                    </div>
                </div>

                <div class="col">
                    <select class="form-control form-control-sm" name="nivel_compu2" value="{{ old('nivel_compu_1') }}" id="nivel_compu_1" required autofocus>
                        <option value="0">Seleccione</option>
                        <option value="1" <?php echo (isset($compu_sql[1]->compu_nivel) && $compu_sql[1]->compu_nivel == '1' ? 'selected' : ''); ?>>Basico</option>
                        <option value="2" <?php echo (isset($compu_sql[1]->compu_nivel) && $compu_sql[1]->compu_nivel == '2' ? 'selected' : ''); ?>>Medio</option>
                        <option value="3" <?php echo (isset($compu_sql[1]->compu_nivel) && $compu_sql[1]->compu_nivel == '3' ? 'selected' : ''); ?>>Avanzado</option>
                    </select>
                </div>
                </div>
            </div>
        </div>

        <!-- compuarcion 3 -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm"></label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                <input type="hidden" name="cod_compu3" value="{{ isset($compu_sql[2]->id) ? $compu_sql[2]->id : '' }}">
                    <div class="control-group">
                    <input type="hidden" name="cod_compu3" value="{{ isset($compu_sql[2]->id) ? $compu_sql[2]->id : '' }}">
                        <select onchange="verificar_si_existe_studio(3)" id="compu3" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="compu3">
                            <option  value="">Seleccione</option>
                            @foreach($computacion as $compu)
                            <option style="width: 200px;" value="{{ $compu->MAESP_CODIGO }}" <?php echo (isset($compu_sql[2]->compu_nom) && $compu_sql[2]->compu_nom == $compu->MAESP_CODIGO  ? 'selected' : ''); ?>>{{ $compu->MAESC_DESCRIPCION }}</option>
                            @endforeach
                        </select>
                        <span class="num_compu3" style="color:red; font-size:12px;"></span>

                    </div>
                </div>
                <div class="col">
                    <select class="form-control form-control-sm" name="nivel_compu3" value="{{ old('nivel_compu_3') }}" id="nivel_compu_3" >
                        <option value="0">Seleccione</option>
                        <option value="1" <?php echo (isset($compu_sql[2]->compu_nivel) && $compu_sql[2]->compu_nivel == '1' ? 'selected' : ''); ?>>Basico</option>
                        <option value="2" <?php echo (isset($compu_sql[2]->compu_nivel) && $compu_sql[2]->compu_nivel == '2' ? 'selected' : ''); ?>>Medio</option>
                        <option value="3" <?php echo (isset($compu_sql[2]->compu_nivel) && $compu_sql[2]->compu_nivel == '3' ? 'selected' : ''); ?>>Avanzado</option>
                    </select>
                </div>
                </div>

            </div>
        </div>


        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Estudios Idiomas') }}</label>
            <div class="col-md-6">
                <div class="row">
                    <div class="col">
                    <input type="hidden" name="cod_idioma1" value="{{ isset($idioma_sql[0]->id) ? $idioma_sql[0]->id : '' }}">
                        <div class="control-group">
                        <select onchange="verificar_si_existe_Idioma(1)" id="idioma1" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="idioma1">
                                <option value="">Seleccione</option>
                                @foreach($idiomas as $idioma)
                                <option value="{{ $idioma->MAESP_CODIGO }}" <?php echo (isset($idioma_sql[0]->idioma_nom) && $idioma_sql[0]->idioma_nom == $idioma->MAESP_CODIGO  ? 'selected' : ''); ?>>{{ $idioma->MAESC_DESCRIPCION }}</option>
                                @endforeach
                            </select>
                            <span class="num_idioma1" style="color:red; font-size:12px;"></span>
                        </div>

                    </select>
                    </div>

                    <div class="col">
                    <select class="form-control form-control-sm" name="nivel_idioma1"  id="nivel_idioma_1"  >
                        <option value="0">Seleccione</option>
                        <option value="1" <?php echo (isset($idioma_sql[0]->idioma_nivel) && $idioma_sql[0]->idioma_nivel == '1' ? 'selected' : ''); ?>>Basico</option>
                        <option value="2" <?php echo (isset($idioma_sql[0]->idioma_nivel) && $idioma_sql[0]->idioma_nivel == '2' ? 'selected' : ''); ?>>Medio</option>
                        <option value="3" <?php echo (isset($idioma_sql[0]->idioma_nivel) && $idioma_sql[0]->idioma_nivel == '3' ? 'selected' : ''); ?>>Avanzado</option>
                    </select>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm"></label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                    <input type="hidden" name="cod_idioma2" value="{{ isset($idioma_sql[1]->id) ? $idioma_sql[1]->id : '' }}">
                    <div class="control-group">
                            <select onchange="verificar_si_existe_Idioma(2)" id="idioma2" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="idioma2">
                                <option value="">Seleccione</option>
                                @foreach($idiomas as $idioma)
                                <option value="{{ $idioma->MAESP_CODIGO }}" <?php echo (isset($idioma_sql[1]->idioma_nom) && $idioma_sql[1]->idioma_nom == $idioma->MAESP_CODIGO  ? 'selected' : ''); ?>>{{ $idioma->MAESC_DESCRIPCION }}</option>
                                @endforeach
                            </select>
                            <span class="num_idioma2" style="color:red; font-size:12px;"></span>
                        </div>
                        </div>
                            <div class="col">
                                <select class="form-control form-control-sm" name="nivel_idioma2" id="nivel_idioma_2" >
                                    <option value="0" >Seleccione</option>
                                    <option value="1" <?php echo (isset($idioma_sql[1]->idioma_nivel) && $idioma_sql[1]->idioma_nivel == '1' ? 'selected' : ''); ?>>Basico</option>
                                    <option value="2" <?php echo (isset($idioma_sql[1]->idioma_nivel) && $idioma_sql[1]->idioma_nivel == '2' ? 'selected' : ''); ?>>Medio</option>
                                    <option value="3" <?php echo (isset($idioma_sql[1]->idioma_nivel) && $idioma_sql[1]->idioma_nivel == '3' ? 'selected' : ''); ?>>Avanzado</option>
                                </select>
                            </div>
                </div>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm"></label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                <input type="hidden" name="cod_idioma3" value="{{ isset($idioma_sql[2]->id) ? $idioma_sql[2]->id : '' }}">
                        <div class="control-group">
                            <select onchange="verificar_si_existe_Idioma(2)"  id="idioma3" class="selectpicker show-tick form-control form-control-sm" data-live-search="true" name="idioma3">
                                <option value="">Seleccione</option>
                                @foreach($idiomas as $idioma)
                                <option value="{{ $idioma->MAESP_CODIGO }}" <?php echo (isset($idioma_sql[2]->idioma_nom) && $idioma_sql[2]->idioma_nom == $idioma->MAESP_CODIGO  ? 'selected' : ''); ?>>{{ $idioma->MAESC_DESCRIPCION }}</option>
                                @endforeach
                            </select>
                            <span class="num_idioma3" style="color:red; font-size:12px;"></span>
                        </div>

                            </div>

                            <div class="col">
                                <select class="form-control form-control-sm" name="nivel_idioma3" id="nivel_idioma_3"  >
                                    <option value="0">Seleccione</option>
                                    <option value="1" <?php echo (isset($idioma_sql[2]->idioma_nivel) && $idioma_sql[2]->idioma_nivel == '3' ? 'selected' : ''); ?>>Basico</option>
                                    <option value="2" <?php echo (isset($idioma_sql[2]->idioma_nivel) && $idioma_sql[2]->idioma_nivel == '3' ? 'selected' : ''); ?>>Medio</option>
                                    <option value="3" <?php echo (isset($idioma_sql[2]->idioma_nivel) && $idioma_sql[2]->idioma_nivel == '3' ? 'selected' : ''); ?>>Avanzado</option>
                                </select>
                            </div>
                </div>
            </div>
        </div>

        <!-- LICENCIA DE licencia_arma -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Licencia para portar arma') }}</label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                    <select class="form-control{{ $errors->has('licencia_arma') ? ' is-invalid' : '' }} form-control-sm" name="licencia_arma" value="{{ old('licencia_arma') }}" id="licencia_arma" required autofocus>
                        <option value="2" {{ (isset($request_db->licencia_arma ) && $request_db->licencia_arma  == "2" ? 'selected' : '') }}>No</option>
                        <option value="1" {{ (isset($request_db->licencia ) && $request_db->licencia_arma  == "1" ? 'selected' : '') }}>Si</option>
                    </select>
                </div>

                </div>
            </div>
        </div>


        <!-- LICENCIA DE CONDUCIR -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Licencia de Conducir') }}</label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                    <select class="form-control{{ $errors->has('licencia') ? ' is-invalid' : '' }} form-control-sm" name="licencia" value="{{ old('licencia') }}" id="licencia" required autofocus onchange="updateLicense(this)">
                        <option value="2" {{ (isset($request_db->licencia ) && $request_db->licencia  == "2" ? 'selected' : '') }}>No</option>
                        <option value="1" {{ (isset($request_db->licencia ) && $request_db->licencia  == "1" ? 'selected' : '') }}>Si</option>
                    </select>
                </div>

                <div class="col">
                    <select class="form-control{{ $errors->has('CATEGORIA') ? ' is-invalid' : '' }} form-control-sm" name="categoria" value="{{ old('categoria') }}" id="categoria" required autofocus disabled>
                        <option value="0">Seleccione</option>
                        <option value="1" {{ (isset($request_db->categoria ) && $request_db->categoria  == "1" ? 'selected' : '') }}>a1</option>
                        <option value="2" {{ (isset($request_db->categoria ) && $request_db->categoria  == "2" ? 'selected' : '') }}>a2</option>
                        <option value="3" {{ (isset($request_db->categoria ) && $request_db->categoria  == "3" ? 'selected' : '') }}>b2</option>
                    </select>
                </div>
                </div>
            </div>
        </div>

        <!-- VEHICULO PROPIO -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('¿Cuenta con Vehículo?') }}</label>
            <div class="col-md-6">
                <div class="row">
                <div class="col">
                    <select class="form-control form-control-sm" name="cuenta_carro" value="{{ old('cuenta_carro') }}" id="cuenta_carro" onchange="updateTypeCar(this)">
                        <option value="2" {{ (isset($request_db->cuenta_carro ) && $request_db->cuenta_carro  == "2" ? 'selected' : '') }}>No</option>
                        <option value="1" {{ (isset($request_db->cuenta_carro ) && $request_db->cuenta_carro  == "1" ? 'selected' : '') }}>Si</option>
                    </select>
                </div>

                <div class="col">
                    <select class="form-control form-control-sm" name="tipo_carro" value="{{ old('tipo_carro') }}" id="tipo_carro" disabled>
                        <option value="0">Seleccione</option>
                        <option value="1" {{ (isset($request_db->cuenta_carro ) && $request_db->cuenta_carro  == "1" ? 'selected' : '') }}>Moto</option>
                        <option value="2" {{ (isset($request_db->cuenta_carro ) && $request_db->cuenta_carro  == "1" ? 'selected' : '') }}>Camioneta</option>
                        <option value="3" {{ (isset($request_db->cuenta_carro ) && $request_db->cuenta_carro  == "1" ? 'selected' : '') }}>Camión</option>
                    </select>
                </div>
                </div>
            </div>
        </div>



        <!--   OTROS CONOCIMIENTOS  -->

        <div class="form-group row">
                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Requisitos') }}</label>

                            <div class="col-md-6">
                            <textarea class="form-control" onpaste="return false"  onkeypress="return soloLetras(event);return validateSpecialCharacters(event)" id="otro_conocimiento" rows="3" name="otro_conocimiento" placeholder="Ingrese Conocimiento" required autofocus>{{ (isset($request_db->otro_conocimiento) ? $request_db->otro_conocimiento : "") }}</textarea>
                            </div>
                        </div>





                         <!--   EXPERIENCIA LABORAL  -->

                         <div class="form-group row">

                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Experiencia Laboral') }}</label>

                            <div class="col-md-3">
                            <input id="experiencia_laboral" onpaste="return false"  type="number" class="form-control form-control-sm" name="experiencia_laboral" value="{{ (isset($request_db->experiencia_laboral) ? $request_db->experiencia_laboral : '') }}" required placeholder="Ingrese Experiencia Laboral">
                            </div>

                            <div class="col-md-3">
                                <select class="form-control form-control-sm" name="experiencia_nivel" value="{{ old('experiencia_nivel') }}" id="experiencia_nivel" required autofocus>
                                    <option value="0">Seleccione</option>
                                    <option value="1" {{ (isset($request_db->experiencia_nivel ) && $request_db->experiencia_nivel  == "1" ? 'selected' : '') }}>Año</option>
                                    <option value="2" {{ (isset($request_db->experiencia_nivel ) && $request_db->experiencia_nivel  == "2" ? 'selected' : '') }}>Meses</option>
                                    <option value="3" {{ (isset($request_db->experiencia_nivel ) && $request_db->experiencia_nivel  == "3" ? 'selected' : '') }}>Dias</option>
                                </select>
                            </div>
                        </div>

                         <!--   EXPERIENCIA LABORAL  -->

                         <div class="form-group row" style="display:none;">

                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Zona Residencial Del Trabajo') }}</label>

                            <div class="col-md-6">
                                <select class="form-control form-control-sm" name="zona" value="{{ old('zona') }}" id="zona">
                                    <option value="2" {{ (isset($request_db->zona ) && $request_db->zona  == "2" ? 'selected' : '') }}>Norte</option>
                                    <option value="0">Seleccione</option>
                                    <option value="1" {{ (isset($request_db->zona ) && $request_db->zona  == "1" ? 'selected' : '') }}>Sur</option>

                                    <option value="3" {{ (isset($request_db->zona ) && $request_db->zona  == "3" ? 'selected' : '') }}>Este</option>
                                    <option value="4" {{ (isset($request_db->zona ) && $request_db->zona  == "4" ? 'selected' : '') }}>Oeste</option>
                                    <option value="5" {{ (isset($request_db->zona ) && $request_db->zona  == "5" ? 'selected' : '') }}>Indistinto</option>
                                </select>

                            </div>

                        </div>

         <!-- SEXO -->

        <div class="form-group row" style="display:none">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Sexo del Trabajador') }}</label>

            <div class="col-md-6">
                <select class="form-control form-control-sm" name="sexo" value="{{ old('sexo') }}" id="sexo">
                        <option value="4">Seleccione</option>
                        <option value="3" {{ (isset($request_db->sexo ) && $request_db->sexo  == "3" ? 'selected' : '') }}>Indistinto</option>
                        <option value="1" {{ (isset($request_db->sexo ) && $request_db->sexo  == "2" ? 'selected' : '') }}>Masculino</option>
                        <option value="2" {{ (isset($request_db->sexo ) && $request_db->sexo  == "1" ? 'selected' : '') }}>Femenino</option>
                    </select>
            </div>
        </div>



    <!-- EDAD DEL TRABAJADOR  -->

    <div class="form-group row" style="display:none">
            <label class="col-md-4 col-form-label text-md-right col-form-label-sm">{{ __('Edad del Trabajador') }}</label>
            <div class="col-md-6">
                <div class="row">
                    <div class="col">
                    <input id="edad" type="text" class="form-control form-control-sm" name="edad" value="{{ (isset($request_db->edad) ? $request_db->edad : '') }}" placeholder="DESDE">
                    <span class="edad" style="color:red; font-size:12px;"></span>
                    </div>

                    <div class="col">
                    <input id="edad_total" type="text" class="form-control form-control-sm" name="edad_total" value="{{ (isset($request_db->edad_total) ? $request_db->edad_total : '') }}" placeholder="HASTA">
                    <span class="edad_total" style="color:red; font-size:12px;"></span>
                </div>
                </div>

                    </div>
                    </div>

                    <br>
       <!-- PERSONA A QUIEN DEBE RIDIGIRSE -->
       <div class=" form-group row justify-content-md-center">
            <div class="col-sm-10" style="background:#00A39C;">
                <h5 class="titulo_formulario ">
                    4. PERSONA A QUIEN DEBE DIRIGIRSE
                </h5>
            </div>
        </div>



        <!-- nombre del contacto -->


        <div class="form-group row">
            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Nombre') }}</label>
            <div class="col-md-4">
                <select class="form-control{{ $errors->has('contacto') ? ' is-invalid' : '' }} form-control-sm" name="contacto" value="{{ old('contacto') }}" id="contacto" required autofocus>
                   @foreach($contactos as $conta)
                    <option value="{{ $conta->id }}" {{ (Auth::user()->id == $conta->id ? 'selected' : '') }}>{{ $conta->name }}</option>
                    @endforeach
                </select>

            </div>
            <div class="col-md-4">
            <!-- <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="limpiarContacto()">Agregar Contacto</button> -->
            </div>
        </div>

        <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                <a href="#" class="btn btn-primary " style="background-color: #17a2b8;border-color: #17a2b8" onclick="siguienteVacante();">Siguiente</a>
                                </div>
                            </div>


    <!-- fin card -->
    </div>
</div>



<input type="hidden" id="base_url" value="{{ asset('') }}">

<script>

$( document ).ready(function() {
    variarEstadoTotal(document.getElementById("estudios_formales"));
    console.log("sddd");
    $("#grado").val($("#grado_edita").val());
});
function limpiarContacto() {

$("#documento_contacto").val("");
$("#numero_contacto").val("");
$("#nombre_contacto").val("");
$("#cargo_contacto").val("");
$("#telefono_contacto").val("");
$("#email_contacto").val("");
$("#password_contacto").val("");
//$("#form_contacto").attr("action", "{{ route('contacto.nuevo') }}");

}

  function variarEstadoTotal(tipo){
      console.log(tipo.value);
    response = "<option value='1'>Seleccione</option>";
    if(tipo.value == "7"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='16'>COLEGIADO</option>";
      response += "<option value='14'>BACHILLER</option>";
      response += "<option value='34'>CICLOS FINALES</option>";
      response += "<option value='35'>CICLOS INTERMEDIOS</option>";
      response += "<option value='36'>CICLOS INICIALES</option>";
    }
    if(tipo.value == "6"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='33'>TRUNCO</option>";
      response += "<option value='34'>CICLOS FINALES</option>";
      response += "<option value='35'>CICLOS INTERMEDIOS</option>";
      response += "<option value='36'>CICLOS INICIALES</option>";
    }
    if(tipo.value == "26"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
    }
    if(tipo.value == "27"){
      response += "<option value='15'>TITULADO</option>";
      response += "<option value='13'>EGRESADO</option>";
      response += "<option value='37'>EN CURSO</option>";
      response += "<option value='33'>TRUNCO</option>";
    }
    $('#grado').html(response).fadeIn();
  }

  function actualizarListaContacto(){


    base_url = $("#base_url").val();
    var urlajax = base_url + "actualiza/contacto/";

    $.ajax({
        type: 'GET',
        url: urlajax,
        dataType: 'json',
        success: function(data) {
            var options = "";
            $.each(data, function(key, val) {
              options += "<option value='"+val.id+"'>"+val.name+"</option>";
            });
            $("#contacto").html(options);
            $('.bd-example-modal-lg').modal('hide');
        }
    });

  }
</script>

<!-- Large modal -->


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">NUEVO CONTACTO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @include('popup_empresa.nuevo_contacto_vacante')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>

    function transformKeyToLowerCase(key) {
        return String.fromCharCode(key).toLowerCase();
    }

    function validateSpecialCharacters(e) {

        let key = e.keyCode || e.which;
        let keyLowerCase = transformKeyToLowerCase(key);

        let allowedCharacters = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        let allowedUnicodeCharacters = [8];

        let isActiveSpecialCharacter = false

        for (let i in allowedUnicodeCharacters) {
            if (key === allowedUnicodeCharacters[i]) {
                isActiveSpecialCharacter = true;
                break;
            }
        }

        if (allowedCharacters.indexOf(keyLowerCase) == -1 && !isActiveSpecialCharacter) {
            return false;
        }
    }

    function soloNumero(evt){

    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    var digitos = evt.target.value.length;

   if(digitos >= 9){
      return false;
   }
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}
    function updateLicense(item){
        if(item.value == '2') {
            $("#categoria").prop('disabled', true);
            $("#categoria").val("0");
        }
        if(item.value == '1') $("#categoria").prop('disabled', false);

    }

    function updateTypeCar(item){
        if(item.value == '2') {
            $("#tipo_carro").prop('disabled', true);
            $("#tipo_carro").val("0");
        }
        if(item.value == '1') $("#tipo_carro").prop('disabled', false);
    }
    function soloLetras(e){

key = e.keyCode || e.which;
tecla = String.fromCharCode(key).toLowerCase();
letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
especiales = "8-37-39-46";

tecla_especial = false
for(var i in especiales){
     if(key == especiales[i]){
         tecla_especial = true;
         break;
     }
 }

 if(letras.indexOf(tecla)==-1 && !tecla_especial){
     return false;
 }
}

function verificar_si_existe_studio(tipo){
    $('.num_compu2').html('')
    $('.num_compu3').html('')
    $('.num_compu1').html('')
    var compu3=$("#compu3").val();
    var compu2=$("#compu2").val();
    var compu1=$("#compu1").val(); 
     
    if(tipo=='2'){
        console.log(compu1+'X')
        if(compu1==''){
            return false
        }
      
        if(compu1==compu2){ 

            $('.num_compu2').html('Opcion repetido')
        }
        if(compu3==''){
            return false
        } 
        if(compu2==compu3){
            $('.num_compu2').html('Opcion repetido')
        }
    }else if(tipo=='3'){
       
        if(compu2==''){
            return false
        }
        if(compu2==compu3){
            $('.num_compu3').html('Opcion repetido')
        }
        if(compu1==''){
            return false
        }
        if(compu1==compu3){
            $('.num_compu3').html('Opcion repetido')
        }  
    }
   
}

function verificar_si_existe_Idioma(tipo){
    $('.num_idioma2').html('')
    $('.num_idioma3').html('')
    $('.num_idioma1').html('')
    var compu3=$("#idioma3").val();
    var compu2=$("#idioma2").val();
    var compu1=$("#idioma1").val(); 
     
    if(tipo=='2'){
         
        if(compu1==''){
            return false
        }
      
        if(compu1==compu2){ 

            $('.num_idioma2').html('Opcion repetido')
        }
        if(compu3==''){
            return false
        } 
        if(compu2==compu3){
            $('.num_idioma2').html('Opcion repetido')
        }
    }else if(tipo=='3'){ 
        if(compu2==''){
            return false
        }
        if(compu2==compu3){
            $('.num_idioma3').html('Opcion repetido')
        }
        if(compu1==''){
            return false
        }
        if(compu1==compu3){
            $('.num_idioma3').html('Opcion repetido')
        }  
    }
   
}

</script>
