

@extends('layouts.app')

@section('content')
@if($inicio == "")
<style>
/*.container{
    max-width: 800px;
}*/
</style>
@endif

<style>
.fondo-imagen{
    background-image: url('{{ asset("public/storage/$banner_foto") }}'); 
    background-color: #cccccc;
    background-repeat: no-repeat, repeat;
    height: 250px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover; 
}

.hero-body{
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
}

.form{
  display: flex;
}

input{
  width: 400px;
  border-radius: 4px 0px 0px 4px;
  padding: 5px;
  border: 0;
  outline: none;
}

button{
  border-radius: 0px 4px 4px 0px;
  border: 0;
  padding: 7px 15px 7px 15px;
  cursor: pointer;
  color:#ffffff;
  background-color:#C73C63;border-color: #C73C63;
}

button:hover{
  color: #ffffff;
  background:#575756;border-color: #575756;
  
}
.search::placeholder{
 /* font-weight: bold;*/
  font-size:14px;
  color: #C73C63; 
}
.display-none{
    display:none;
}

p {
    margin-top: 0;
    margin-bottom:10p;
}
.onlyColor{background-color:transparent;border-color: #C73C63;color:#C73C63}
.vacante_detalle b{  font-size:15px;}
.vacante_detalle .card-header{text-align:center; background-color:#575756;color:#fff;font-size:18px;}
.vacante_detalle .verfull a{width: 80%;border-color: #C73C63;padding: 5px 15px 5px 15px;font-size:12px;margin-top: -6%;background-color:#C73C63}

</style>


    <div class="container vacante_detalle" >
    <input type="hidden" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" id="codigo_postulante">
    <div class="fondo-imagen card" >
        <div class="card-body">
            <div class="row justify-content-between" style="display: block;">
                <div class="col-7 col-sm-6 col-md-3" style="margin-top: 10.5%;float: right;">
                <img src="{{ asset('public/storage/'.$perfil_foto) }}" class="img-thumbnail" style="width: 150px; height: 150px;border: 1px;background-color: #1A4A84;" rel="image_src" >
                </div>
            </div>
        </div>    
    </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" >
            @foreach ($pedidos as $ped)
                <li class="breadcrumb-item" ><a href="{{ route('welcome.entidad', ['id' => $entidad]) }}" style="color:#1A4A84;">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{ route('users.busqueda.inicio',['e' => (isset($entidad) ? $entidad : '1'),'categoria' => $ped->cod_area] ) }}" style="color:#1A4A84;">{{ $ped->area_descripcion }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ ucfirst(mb_strtolower($ped->des_empre,'UTF-8')) }}</li>
            @endforeach
            </ol>
        </nav>

    <div class="card">
        <div class="card-body">

        

            <div class="row">
            @foreach ($pedidos as $ped)
                <div class="col">
                    <input type="hidden" name="base_url" id="base_url" value="{{asset('')}}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input type="hidden" value="{{ $ped->codigo_pedido }}" id="codigo_perdido">
                        <b style="color:#C73C63; font-size:20px;">
                            {{ ucfirst(mb_strtolower($ped->des_empre,'UTF-8')) }}
                        </b>
                        <p>{{ $ped->razon_social }}</p>
                        @foreach ($contact as $con)
                        <p><b>Nombre:</b> {{ $con->name }}&nbsp;&nbsp;&nbsp;<b>Email:</b> {{ $con->email }}&nbsp;&nbsp;&nbsp;<b>N&uacute;mero:</b> {{ $con->telefono }}</p>
                        <p></p>
                        @endforeach
                </div>
                <div class="col-2 col-sm-2 col-md-1">
                     @if(isset($favorito) && count($favorito) > 0)
                     <a href="#" class="icon_favorite" onclick="cancelarFavorite(this)">
                     <i class="fas fa-star" id="icon_fab"></i>
                     </a>
                    @else
                    <a href="#" class="icon_favorite" onmouseover="favoriteHover(this)" onmouseout="favoriteNormal(this)" onclick="guardaFavorite(this)">
                    <i class="far fa-star" id="icon_fab"></i>
                    </a>
                    @endif
                    
                   
                </div>
                @endforeach  
            </div>
            
        </div>
    </div>

    <div class="card" >
        <div class="card-body">
        @foreach($pedidos as $ped)
        <div class="row ">
              <div class="col">
                    <div class="row ">
                        <div class="col">
                            <b class="parrafo-detalle">Area</b>
                            <p class="parrafo-detalle">{{ $ped->area_descripcion }}</p>
                        </div>
                        <div class="col">
                            <b class="parrafo-detalle">Jornada</b>
                            @if($ped->turno_trabajo == "1")
                                <p class="parrafo-detalle">Fijo</p>
                            @else    
                                <p class="parrafo-detalle">Rotativo</p>
                            @endif
                        </div>
                        <div class="col">
                            <b class="parrafo-detalle">Salario</b>
                            <p class="parrafo-detalle">{{ $ped->remuneracion }}
                                @if($ped->moneda == "1")
                                Mensual
                                @elseif($ped->moneda == "2")
                                Quinsenal
                                @elseif($ped->moneda == "3")
                                Semanal
                                @else
                                Diario
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="row vacante_detalle">
                        <div class="col-12 col-sm-12 col-md-4">
                            <b class="parrafo-detalle mb-5">Nombre Del Puesto</b>
                            <p class="parrafo-detalle">{{ ucfirst(mb_strtoupper($ped->des_empre,'UTF-8')) }}</p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-4">
                            <b class="parrafo-detalle">Horario</b>
                            <p class="parrafo-detalle">{{ ucfirst(mb_strtoupper($ped->horario,'UTF-8')) }}</p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-4">
                            <b class="parrafo-detalle">Ubicacion</b>
                                @foreach($departamento as $ubi)
                                    <p class="parrafo-detalle">{{ ucfirst(mb_strtoupper($ubi->ubi_descripcion,'UTF-8')) }} /
                                @endforeach
                                @foreach($provincia as $ubi)
                                    {{ ucfirst(mb_strtoupper($ubi->ubi_descripcion,'UTF-8')) }} /
                                @endforeach
                                @foreach($distrito as $ubi)
                                {{ ucfirst(mb_strtoupper($ubi->ubi_descripcion,'UTF-8')) }}</p>
                                @endforeach
                        </div>
                    </div>


                    <div class="row vacante_detalle">
                        <div class="col">
                        
                            <b class="parrafo-detalle">Experiencia</b>
                            @if($ped->experiencia_laboral == "0" || $ped->experiencia_laboral == "" || $ped->experiencia_laboral == null)
                            <p class="parrafo-detalle">Sin experiencia</p>
                            @else
                                <p class="parrafo-detalle">
                                {{ $ped->experiencia_laboral }}
                                @if($ped->experiencia_nivel == "1")
                                    Años
                                @elseif($ped->experiencia_nivel == "2")
                                    Meses
                                @else
                                    Dias
                                @endif
                                </p>
                            @endif
                        </div>
                        <div class="col">
                            <b>Vacante</b>
                                <p>{{ $ped->num_vacantes }}</p>
                        </div>
                        <div class="col">
                            <b>Fecha Limite </b>
                                <p>{{ $ped->fecha_limite }}</p>
                        </div>
                       
                    </div>

              </div>  
              <div class="col-12 col-sm-12 col-md-3 col-lg-3 ">
                    <div class="row">
                        <div class="col" style="margin-bottom: 10px;">
                            @guest
                            <button onclick="validarPostulacion()" type="button" class="btn  btn-lg btn-block redondeado" data-toggle="modal" data-target="#login_ultimate"  >
                            <b><i class="fas fa-suitcase"></i> &nbsp;&nbsp;Postular</b>
                            </button>
           
                            @else
                            <button id="abrirpostuluar" type="button" class="btn  btn-lg btn-block redondeado" data-toggle="modal" data-target="#exampleModal">
                            <b><i class="fas fa-suitcase"></i> &nbsp;&nbsp;Postular</b>
                            </button>
                        @endguest
                            
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="margin-bottom: 10px;">
                            <button onclick="imprimirAviso()" type="button" class="btn btn-lg btn-block redondeado onlyColor"  >
                            <b>Imprimir Aviso</b>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col text-center " >
                            <p>Compartir</p><br>
                            <?php 
                                echo Share::currentPage('accede.com.pe')
                                ->facebook('{{ ucfirst(mb_strtoupper($ped->des_empre)) }}')
                                ->twitter('{{ ucfirst(mb_strtoupper($ped->des_empre)) }}')
                                ->linkedin('{{ ucfirst(mb_strtoupper($ped->des_empre)) }}')
                                ->whatsapp('{{ ucfirst(mb_strtoupper($ped->des_empre)) }}');
                            ?>
                        </div>
                    </div>
                     <!-- Load Facebook SDK for JavaScript -->
   
      </div>

        </div>
            
            
        @endforeach    
        </div>
    </div>
            
    <div class="row vacante_detalle">
        <div class="col-12 col-sm-12 col-md-8" style="margin-top: 10px;">
            <div class="card" >
                <div class="card-body">
                    @foreach($pedidos as $ped)
                        <b class="parrafo-detalle">Descripcion</b>
                        <p class="parrafo-detalle">{{ $ped->des_empre }} </p>
                        <b class="parrafo-detalle">Requisitos</b>
                        <p class="parrafo-detalle">{{ ucfirst($ped->otro_conocimiento) }} </p>
                        <b class="parrafo-detalle">Funciones</b>
                        <p class="parrafo-detalle">{{ $ped->tareas }} </p>
                        <b class="parrafo-detalle">Beneficios</b>
                        <p class="parrafo-detalle">
                            @if($ped->horas_extras == '1')
                                - Horas Extras <br> 
                            @endif
                            @if($ped->movilidad == '1')
                                - Movilidad<br>
                            @endif
                            @if($ped->refrigerio == '1')
                                - Refrigerio<br>
                            @endif
                            @if($ped->bonificacion == '1')
                                - Bonificación<br>
                            @endif
                            @if($ped->comisiones == '1')
                                - Comisiones
                            @endif
                        </p>
                       
                    @endforeach
                </div>
            </div>    
        </div>
        <div class="col-12 col-sm-12 col-md-4" >
            <div class="card " style="margin-top: 10px;">
                <div class="card-header">Avisos Relacionados</div>

                @foreach($realcionados as $value)   
                <div class="row no-gutters" style="border-bottom: 2px solid #ebebeb;  ">
                    <div class="col-3 text-center">
                    <img src="{{ asset('public/storage/'.$value->perfil_foto) }}" class="img-thumbnail" style="width: 70px; height: 70px;margin-top: 15%;margin-bottom: 15%;">
                    </div>
                    <div class="col-5">
                        <div class="card-body" style="padding: 0.8rem;">
                            <p class="card-title" style="font-size:12px;font-weight: bold;">{{ str_limit(ucfirst(mb_strtoupper($value->des_empre,'UTF-8')),40) }}</p>
                            <p class="card-text" style="font-size:11px;">{{ str_limit($value->razon_social,40) }}</p>
                            
                        </div>
                    </div>
                    <div class="col-4 text-center">
                   
                    @if($value->cod_proce == '1')
                        <a href="{{ route('vacante.detalle',['id' => $value->codigo_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="btn btn-primary redondeado" style="padding: 5px 15px 5px 15px;font-size:10px;margin-top: 15%;margin-bottom: 15%;background-color:#C73C63;border-color: #C73C63;">VER AVISO</a>
                    @else
                        <a href="{{ route('vacante.detalle.publica',['id' => $value->codigo_pedido, 'e' => $entidad]) }}" class="btn btn-primary redondeado" style=";border-color: #C73C63;border-color: #C73C63;">VER AVISO</a>
                    @endif
                    
                    </div>
                </div>
                @endforeach
                
             </div> 
             <br>
             <div class="text-center verfull">
                <a href="{{ route('users.busqueda.inicio',['e' => (isset($entidad) ? $entidad : '1')]) }}" class="btn btn-primary redondeado">
                    <b>VER TODOS LOS AVISOS</b>
                </a>
             </div>
        </div>
    </div>


<!-- Modal para verificar si esta logeado o no -->

<div class="modal fade" id="exampleModal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body">
            @guest
            <div class="alert alert-danger" role="alert">
                    Debe ingresar a su cuenta para postular.
                </div>
           
        @else
             <div class="alert alert-success" role="alert" style="display:none;" id="postulo">
                Postulo Exitosamente.
            </div>
         ¿Esta seguro que desea postular a esta vacante?
         @endguest
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            
            @guest
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
               
                @else
                 <button type="button" class="btn btn-primary" onclick="postularPedido('{{$_id}}')" id="btnEliminar">SI</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
              @endguest
        </div>
        
      </div>
    </div>
  </div>

<script type="text/javascript">

    function postularPedido(id){
         var _token=$("#csrf-token").val();
    base_url=$("#base_url").val();
    var urlData=base_url+"vacante/guardar/"+id;
    $.ajax({
            type:'POST',
            url:urlData,
            dataType:'HTML', 
            data:{_token}, 
             beforeSend: function(){
           $(".animationload").css({display:'block'});
 
          }      
    }).done(function( data, textStatus, jqXHR ){
      $(".animationload").css({display:'none'});
        $("#exampleModal").modal('hide');
        document.getElementById("postulo").style = "display:block";
        console.log(data)
        //window.history.back();
        postularPedidoError(id);
    }).fail(function( jqXHR, textStatus, errorThrown ){
      $(".animationload").css({display:'none'});
      console.log("Fallo La Postulacion");
    });
    }
    function postularPedidoError(id){
         var _token=$("#csrf-token").val();
    base_url=$("#base_url").val();
    var urlData=base_url+"vacante/veriricarError/"+id;
    $.ajax({
            type:'POST',
            url:urlData,
            dataType:'HTML', 
            data:{_token}, 
             beforeSend: function(){
           $(".animationload").css({display:'block'});
 
          }      
    }).done(function( data, textStatus, jqXHR ){
      $(".animationload").css({display:'none'});
        $("#exampleModal").modal('hide');
        document.getElementById("postulo").style = "display:block";
        console.log(data)
        //window.history.back();
    }).fail(function( jqXHR, textStatus, errorThrown ){
      $(".animationload").css({display:'none'});
      console.log("Fallo La Postulacion");
    });
    }
    function imprimirAviso(){
        var contenido= document.getElementById("container").innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;

      //  window.print();
    }
    function favoriteHover(value){
        $("#icon_fab").removeClass("far")
        //$("#icon_fab").removeClass("fas")
        $("#icon_fab").addClass("fas")
      //  $("#icon_fab").addClass("fa-star")
    }

    function favoriteNormal(value){
        $("#icon_fab").removeClass("fas")
        //$("#icon_fab").removeClass("fas")
        $("#icon_fab").addClass("far")
      //  $("#icon_fab").addClass("fa-star")
    }

    function guardaFavorite(value){

        if($("#codigo_postulante").val() == '0'){
            alert("Debe estar Registrado para esta accion");
        }else{
            var _token=$("#csrf-token").val();
            base_url=$("#base_url").val();
            var codigo_pedido = $("#codigo_perdido").val();
            var codigo_postulante = $("#codigo_postulante").val();
            var urlData=base_url+"favorite/guardar/"+codigo_postulante+"/"+codigo_pedido;
            $.ajax({
                type:'get',
                url:urlData,
                dataType:'json', 
            }).done(function( data, textStatus, jqXHR ){
                console.log(data);
                location.reload();
                favoriteHover();
            });
        }
        
    }

    function cancelarFavorite(){
        if($("#codigo_postulante").val() == '0'){
            alert("Debe estar Registrado para esta accion");
        }else{
            var _token=$("#csrf-token").val();
            base_url=$("#base_url").val();
            var codigo_pedido = $("#codigo_perdido").val();
            var codigo_postulante = $("#codigo_postulante").val();
            var urlData=base_url+"favorite/cancelar/"+codigo_postulante+"/"+codigo_pedido;
            $.ajax({
                type:'get',
                url:urlData,
                dataType:'json', 
            }).done(function( data, textStatus, jqXHR ){
                console.log(data);
                location.reload();
                favoriteNormal();
            });
        }
    }

    
</script>

    </div>
    @include('maestro.footer_usuario')
    @endsection
