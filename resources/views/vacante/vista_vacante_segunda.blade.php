<div class="card" style="padding-bottom: 15px;">
    <div class="card-body">
        <!-- inicio card -->

        <!--  -->
        <div class=" form-group row justify-content-md-center">
            <div class="col-sm-10" style="background:#00A39C;">
                <h5 class="titulo_formulario ">
                    5. INFORMACION SOBRE EL PUESTO
                </h5>
            </div>
        </div>

        <!--  PRINCIPALES TAREAS  -->

        <div class="form-group row">
            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Funciones del Puesto.') }}</label>
            <input type="hidden" id="base_url" value="{{ asset('') }}">
            <div class="col-md-6">
                <textarea onpaste="return false"  class="form-control"  onkeypress="return soloLetras(event);return validateSpecialCharacters(event)" id="tareas" rows="3" name="tareas" placeholder="Ingrese las tareas y responsabilidades del puesto a cubrir" required autofocus>{{ (isset($request_db->tareas) ? $request_db->tareas : '') }}</textarea>
                <span class="tareas" style="color:red; font-size:12px;"></span>
            </div>
        </div>

        <!--   PERSONAL A CARGO  -->


        <div class="form-group row">

            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Tiene personal a cargo') }}</label>

            <div class="col-md-2">
                <select class="form-control form-control-sm" name="personal_cargo" value="{{ old('personal_cargo') }}" id="personal_cargo" onchange="personaCargo(this)">
                    <option value="1" {{ (isset($request_db->personal_cargo ) && $request_db->personal_cargo  == "1" ? 'selected' : '') }}>No</option>
                    <option value="2" {{ (isset($request_db->personal_cargo ) && $request_db->personal_cargo  == "2" ? 'selected' : '') }}>Si</option>

                </select>

            </div>
            <label class="col-md-2 col-form-label col-form-label-sm text-md-right">{{ __('Número') }}</label>
            <div class="col-md-2">
                <input id="cantidad" onpaste="return false"  type="text" class="form-control form-control-sm" name="cantidad" value="{{ (isset($request_db->cantidad) ? $request_db->cantidad : '') }}" placeholder="Ingrese Cantidad" disabled>
            </div>

        </div>

        <!--   MODALIDADES DE TRABAJO  -->

        <div class="form-group row">

            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Tipo de Contrato') }}</label>

            <div class="col-md-6">
                <select class="form-control form-control-sm" name="modalidades" value="{{ old('modalidades') }}" id="modalidades" required autofocus>
                    <option value="">Seleccione</option>
                    <option value="1" {{ (isset($request_db->modalidades ) && $request_db->modalidades  == "1" ? 'selected' : '') }}>Indefinido</option>
                    <option value="2" {{ (isset($request_db->modalidades ) && $request_db->modalidades  == "2" ? 'selected' : '') }}>Plazo fijo (Determinado)</option>
                    <option value="3" {{ (isset($request_db->modalidades ) && $request_db->modalidades  == "3" ? 'selected' : '') }}>Tiempo parcial</option>
                    <option value="4" {{ (isset($request_db->modalidades ) && $request_db->modalidades  == "4" ? 'selected' : '') }}>Practica pre-profesionales</option>
                    <option value="5" {{ (isset($request_db->modalidades ) && $request_db->modalidades  == "5" ? 'selected' : '') }}>Practica profesionales</option>
                </select>
                <span class="modalidades" style="color:red; font-size:12px;"></span>
            </div>
        </div>

        <!--   HORARIO DE TRABAJO  -->
        <!--
                         <div class="form-group row">

                            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Horario de Trabajo') }}</label>

                            <div class="col-md-6">
                                <select class="form-control form-control-sm" name="horario_trabajo" value="{{ old('horario_trabajo') }}" id="horario_trabajo" required autofocus>
                                    <option value="">Seleccione</option>
                                    <option value="1" {{ (isset($request_db->horario_trabajo ) && $request_db->horario_trabajo  == "1" ? 'selected' : '') }}>A Tiempo Completo</option>
                                    <option value="2" {{ (isset($request_db->horario_trabajo ) && $request_db->horario_trabajo  == "2" ? 'selected' : '') }}>A Tiempo Parcial</option>
                                    <option value="3" {{ (isset($request_db->horario_trabajo ) && $request_db->horario_trabajo  == "3" ? 'selected' : '') }}>tarde</option>
                                    <option value="4" {{ (isset($request_db->horario_trabajo ) && $request_db->horario_trabajo  == "4" ? 'selected' : '') }}>Bajo Cualquier Modalidad</option>
                                </select>
                                <span class="horario_trabajo" style="color:red; font-size:12px;"></span>
                            </div>
                        </div>
-->
        <!--   TURNO DE TRABAJO -->

        <div class="form-group row">

            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Turno Trabajo') }}</label>

            <div class="col-md-6">
                <select class="form-control form-control-sm" name="turno_trabajo" value="{{ old('turno_trabajo') }}" id="turno_trabajo" required autofocus>
                    <option value="">Seleccione</option>
                    <option value="1" {{ (isset($request_db->turno_trabajo ) && $request_db->turno_trabajo  == "1" ? 'selected' : '') }}>Fijo</option>
                    <option value="2" {{ (isset($request_db->turno_trabajo ) && $request_db->turno_trabajo  == "2" ? 'selected' : '') }}>Rotativo</option>
                </select>
                <span class="turno_trabajo" style="color:red; font-size:12px;"></span>
            </div>
        </div>

        <!--   ESPICIFICAR HORARIO  -->

        <div class="form-group row">

            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Especificar Horario') }}</label>

            <div class="col-md-6">
                <input id="horario" onpaste="return false"  type="text" class="form-control form-control-sm" onkeypress="return disableSomeSpecialCharacters(event);" name="horario" value="{{ (isset($request_db->horario) ? $request_db->horario : '') }}" required placeholder="Ingrese Horario">
                <span class="horario" style="color:red; font-size:12px;"></span>
            </div>
        </div>

        <!--   REMUNERACION OFRECIDA  -->

        <div class="form-group row">

            <label for="remuneracion" class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Remuneración Ofrecida') }}</label>

            <div class="col-md-4">
                <input id="remuneracion" onpaste="return false"  onkeypress="return soloNumero(event);" type="text" class="form-control form-control-sm" name="remuneracion" value="{{ (isset($request_db->remuneracion) ? $request_db->remuneracion : '') }}" required placeholder="Ingrese Remuneración">
                <span class="remuneracion" style="color:red; font-size:12px;"></span>
            </div>
            <div class="col-md-2">
                <select class="form-control form-control-sm" name="moneda" value="{{ old('moneda') }}" id="moneda" required autofocus>
                    <option value="">Seleccione</option>
                    <option value="1" {{ (isset($request_db->moneda ) && $request_db->moneda  == "1" ? 'selected' : '') }}>Mensual</option>
                    <option value="2" {{ (isset($request_db->moneda ) && $request_db->moneda  == "2" ? 'selected' : '') }}>Quincenal</option>
                    <option value="3" {{ (isset($request_db->moneda ) && $request_db->moneda  == "3" ? 'selected' : '') }}>Semanal</option>
                    <option value="4" {{ (isset($request_db->moneda ) && $request_db->moneda  == "4" ? 'selected' : '') }}>Diario</option>
                </select>
                <span class="moneda" style="color:red; font-size:12px;"></span>
            </div>
        </div>

        <!--   LUGAR DE CENTRO DE TRABAJO  -->

        <div class="form-group row">

            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Lugar de Centro de Trabajo') }}</label>

            <div class="col-md-2">
                <select class="form-control form-control-sm" name="departamento" value="{{ old('departamento') }}" id="departamento" required autofocus onchange="ComboboxProvincia(this)">
                    <option value="">Seleccione</option>
                    @foreach($ubigeo as $ubi)
                    <option value="{{ $ubi->ubi_coddpto }}" {{ (isset($request_db->ubigeo ) && substr($request_db->ubigeo,0,2)  == $ubi->ubi_coddpto ? 'selected' : '') }}>{{ $ubi->ubi_descripcion }}</option>
                    @endforeach
                </select>
                <span class="departamento" style="color:red; font-size:12px;"></span>
            </div>
            <div class="col-md-2">
                <select class="form-control form-control-sm" name="provincia" value="{{ old('provincia') }}" id="provincia" required autofocus onchange="ComboboxDistrito(this)">
                    <option value="0">Seleccione</option>
                    @if(isset($request_db->ubigeo))
                    @foreach($ubigeo_prov as $ubi)
                    <option value="{{ $ubi->ubi_codprov }}" {{ (substr($request_db->ubigeo,2,2)  == $ubi->ubi_codprov ? 'selected' : '') }}>{{ $ubi->ubi_descripcion }}</option>
                    @endforeach
                    @endif
                </select>
                <span class="provincia" style="color:red; font-size:12px;"></span>
            </div>
            <div class="col-md-2">
                <select class="form-control form-control-sm" name="distrito" value="{{ old('distrito') }}" id="distrito" required autofocus>
                    <option value="0">Seleccione</option>
                    @if(isset($request_db->ubigeo))
                    @foreach($ubigeo_dis as $ubi)
                    <option value="{{ $ubi->ubi_coddist }}" {{ (substr($request_db->ubigeo,4,2)  == $ubi->ubi_coddist ? 'selected' : '') }}>{{ $ubi->ubi_descripcion }}</option>
                    @endforeach
                    @endif
                </select>
                <span class="distrito" style="color:red; font-size:12px;"></span>
            </div>
        </div>


        <div class="form-group row">

            <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Direccion del centro de trabajo') }}</label>

            <div class="col-md-6">
                <input id="direccion_trabajo" onpaste="return false"  type="text" onkeypress="return validateSpecialCharactersDireccion(event);" class="form-control form-control-sm" name="direccion_trabajo" value="{{ (isset($request_db->direccion_trabajo) ? $request_db->direccion_trabajo : '') }}" required placeholder="Ingrese Direccion">
                <span class="direccion_trabajo" style="color:red; font-size:12px;"></span>
            </div>
        </div>
    </div>

    <!--   OTROS BENEFICIOS  -->

    <div class="form-group row">

        <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Otros Beneficios') }}</label>

        <div class="col-md-6">
            <div class="custom-control custom-checkbox mb-1" style="float:left; margin-right:20px;">
                <input  type="checkbox" class="custom-control-input form-control-sm" id="horas_extras" name="horas_extras" value="1" {{ (isset($request_db->horas_extras) && $request_db->horas_extras == '1' ? 'checked' : '') }}>
                <label class="custom-control-label col-form-label col-form-label-sm text-md-righ" for="horas_extras">Horas Extras</label>
                <div class="invalid-feedback">campo requerido</div>
            </div>

            <div class="custom-control custom-checkbox mb-1" style="float:left; margin-right:20px;">
                <input type="checkbox" class="custom-control-input form-control-sm" id="movilidad" name="movilidad" value="1" {{ (isset($request_db->movilidad) && $request_db->movilidad == '1' ? 'checked' : '') }}>
                <label class="custom-control-label col-form-label col-form-label-sm text-md-right" for="movilidad">Movilidad</label>
                <div class="invalid-feedback">campo requerido</div>
            </div>

            <div class="custom-control custom-checkbox mb-1" style="float:left; margin-right:20px;">
                <input type="checkbox" class="custom-control-input form-control-sm" id="refrigerio" name="refrigerio" value="1" {{ (isset($request_db->refrigerio) && $request_db->refrigerio == '1' ? 'checked' : '') }}>
                <label class="custom-control-label col-form-label col-form-label-sm text-md-right" for="refrigerio">Refrigerio</label>
                <div class="invalid-feedback">campo requerido</div>
            </div>

            <div class="custom-control custom-checkbox mb-1" style="float:left; margin-right:20px;">
                <input type="checkbox" class="custom-control-input form-control-sm" id="bonificacion" name="bonificacion" value="1" {{ (isset($request_db->bonificacion) && $request_db->bonificacion == '1' ? 'checked' : '') }}>
                <label class="custom-control-label col-form-label col-form-label-sm text-md-right" for="bonificacion">Bonificación</label>
                <div class="invalid-feedback">campo requerido</div>
            </div>

            <div class="custom-control custom-checkbox mb-1" style="float:left; margin-right:20px;">
                <input type="checkbox" class="custom-control-input form-control-sm" id="comisiones" name="comisiones" value="1" {{ (isset($request_db->comisiones) && $request_db->comisiones == '1' ? 'checked' : '') }}>
                <label class="custom-control-label col-form-label col-form-label-sm text-md-right" for="comisiones">Comisiones</label>
                <div class="invalid-feedback">campo requerido</div>
            </div>

        </div>

    </div>

    <!--   LUGAR DE CENTRO DE TRABAJO  -->

    <div class="form-group row">

        <label class="col-md-6 col-form-label col-form-label-sm text-md-right">{{ __('¿Esta Dispuesto a contratar a personas con discapcidad?') }}</label>

        <div class="col-md-4">
            <select class="form-control form-control-sm" name="discapacidad" value="{{ old('discapacidad') }}" id="discapacidad" required autofocus onchange="validarDiscapcidad(this)">
                <option value="2" {{ (isset($request_db->discapacidad) && $request_db->discapacidad == '2' ? 'selected' : '') }}>No</option>
                <option value="1" {{ (isset($request_db->discapacidad) && $request_db->discapacidad == '1' ? 'selected' : '') }}>Si</option>
            </select>
        </div>

    </div>


    <!-- PERSONA CON DISCAPACIDAD -->

    <div class="form-group row " id="oculto" style="visibility: hidden;">
        <label class="col-md-4 col-form-label col-form-label-sm text-md-right">Defina el tipo de discapacidad que se ajusta al puesto</label>
        <div class="hidden-mobile form-group row">   
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="radio-discapcidad" value="1" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '1' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad">Fisica Motora</label>
        </div>&nbsp;&nbsp;
        <div class="custom-control custom-radio mb-3">
            <input type="radio" class="custom-control-input" id="radio-discapcidad2" value="2" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '2' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad2">Mentales Psicosocioales</label>

        </div>&nbsp;&nbsp;
        <div class="custom-control custom-radio mb-3">
            <input type="radio" class="custom-control-input" id="radio-discapcidad3" value="3" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '3' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad3">Intelectual</label>

        </div>&nbsp;&nbsp;
        <div class="custom-control custom-radio mb-3">
            <input type="radio" class="custom-control-input" id="radio-discapcidad4" value="4" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '4' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad4">Sensoriales</label>
            <div class="invalid-feedback">Invalido</div>
        </div>&nbsp;&nbsp;
        </div>
        <!--mobile-->
        <div discapacidad_form class="col-md-6">
        <div class="custom-control custom-radio mb-1">
            <input type="radio" class="custom-control-input" id="radio-discapcidad" value="1" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '1' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad">Fisica Motora</label>
        </div>
        <div class="custom-control custom-radio mb-1">
            <input type="radio" class="custom-control-input" id="radio-discapcidad2" value="2" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '2' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad2">Mentales Psicosocioales</label>

        </div>
        <div class="custom-control custom-radio mb-1">
            <input type="radio" class="custom-control-input" id="radio-discapcidad3" value="3" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '3' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad3">Intelectual</label>

        </div>
        <div class="custom-control custom-radio mb-1">
            <input type="radio" class="custom-control-input" id="radio-discapcidad4" value="4" name="radio_discapacidad" {{ (isset($request_db->radio_discapacidad) && $request_db->radio_discapacidad == '4' ? 'checked' : '') }}>
            <label class="custom-control-label col-form-label-sm" for="radio-discapcidad4">Sensoriales</label>
            <div class="invalid-feedback">Invalido</div>
        </div>
        </div> 
         <!--mobile-->
    </div>
    
    <!-- OBSERBASINONES -->

    <div class="form-group row">
        <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Observaciones.') }}</label>

        <div class="col-md-6">
            <textarea onpaste="return false" onkeypress="return soloLetras(event);return validateSpecialCharacters(event)" class="form-control" id="obersavasiones" rows="3" name="obersavasiones" placeholder="Ingrese información adicional que considere relevante." required autofocus>{{ (isset($request_db->obersavasiones) ? $request_db->obersavasiones : '') }}</textarea>
        </div>


    </div>

    <div class=" form-group row justify-content-md-center">
        <div class="col-sm-10" style="background:#00A39C;">
            <h5 class="titulo_formulario ">
                6. VALIDO HASTA
            </h5>
        </div>
    </div>

    <!--  FECHA VACANE -->

    <div class="form-group row">

        <label class="col-md-4 col-form-label col-form-label-sm text-md-right">{{ __('Fecha') }}</label>

        <div class="col-md-6">
            <input id="fecha_limite" onpaste="return false"  type="date" min="<?= date("Y-m-d") ?>" class="form-control form-control-sm" name="fecha_limite" value="{{ (isset($request_db->fecha_limite) ? $request_db->fecha_limite : '') }}" required placeholder="Ingrese Fecha">
            <span class="fecha_limite" style="color:red; font-size:12px;"></span>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <input type="hidden" name="codigo" value="{{ (isset($request_db->id) ? $request_db->id : '') }}">
            <a href="#" class="btn btn-primary" onclick="enviarFormu()" style="background-color: #17a2b8;border-color: #17a2b8">
                {{ __('Crear Pedido') }}
            </a>

        </div>
    </div>

    <!-- fin card -->
</div>
</div>
<script>

    function test(e)  {
        console.log(e.target.value);
        console.log(e.which);
        console.log(e.keyCode);
    }

    function convertKeyToLowerCase(key) {
        return String.fromCharCode(key).toLowerCase();
    }

    function checkCharacters(event, canCheckSpecialCharacters = false, canCheckNumbers = false) {

        let key = (event.which) ? event.which : event.keyCode;
        let keyLowerCase = convertKeyToLowerCase(key);

        let allowedCharacters = "áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";

        if (canCheckSpecialCharacters) {
            for (var i in allowedSpecialCharacters) {
                if (key === allowedSpecialCharacters[i]) {
                    isActiveSpecialCharacter = true;
                    break;
                }
            }
        }

        if (canCheckNumbers) {
            allowedCharacters += "1234567890";
        }

        if (allowedCharacters.indexOf(keyLowerCase) == -1 && !isActiveSpecialCharacter) {
            return false;
        }

    }

    function disableSomeSpecialCharacters(e) {

        let key = e.keyCode || e.which;
        let keyLowerCase = transformKeyToLowerCase(key);

        let allowedCharacters = " áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";

        const allowedSpecialCharacters = [8, 46, 58, 45];

        let isActiveSpecialCharacter = false

        for (var i in allowedSpecialCharacters) {
            if (key === allowedSpecialCharacters[i]) {
                isActiveSpecialCharacter = true;
                break;
            }
        }

        if (allowedCharacters.indexOf(keyLowerCase) == -1 && !isActiveSpecialCharacter) {
            return false;
        }
    }

    function validateSpecialCharactersDireccion(e) {

        let key = (event.which) ? event.which : event.keyCode;
        let keyLowerCase = convertKeyToLowerCase(key);

        const allowedCharacters = " áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";
        let allowedUnicodeCharacters = [8];

        let isActiveSpecialCharacter = false

        for (let i in allowedUnicodeCharacters) {
            if (key === allowedUnicodeCharacters[i]) {
                isActiveSpecialCharacter = true;
                break;
            }
        }

        if (allowedCharacters.indexOf(keyLowerCase) == -1 && !isActiveSpecialCharacter) {
            return false;
        }
    }

    function validateSpecialCharacters(e) {

        let key = e.keyCode || e.which;
        let keyLowerCase = transformKeyToLowerCase(key);

        let allowedCharacters = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        let allowedUnicodeCharacters = [8];

        let isActiveSpecialCharacter = false

        for (let i in allowedUnicodeCharacters) {
            if (key === allowedUnicodeCharacters[i]) {
                isActiveSpecialCharacter = true;
                break;
            }
        }

        if (allowedCharacters.indexOf(keyLowerCase) == -1 && !isActiveSpecialCharacter) {
            return false;
        }
    }


    function soloNumero(evt) {

        // code is the decimal ASCII representation of the pressed key.
        var code = (evt.which) ? evt.which : evt.keyCode;
        var digitos = evt.target.value.length;

        if (digitos >= 9) {
            return false;
        }
        if (code == 8) { // backspace.
            return true;
        } else if (code >= 48 && code <= 57) { // is a number.
            return true;
        } else { // other keys.
            return false;
        }
    }

    function personaCargo(input) {
        if (input.value == "1") {
            document.getElementById("cantidad").disabled = true;
        } else {
            document.getElementById("cantidad").disabled = false;
        }

    }

    function validarDiscapcidad(dis) {
        if (dis.value == "Si") {
            document.getElementById("oculto").style = "visibility: visible;";
        } else {
            document.getElementById("oculto").style = "visibility: hidden;";
        }

    }
</script>
