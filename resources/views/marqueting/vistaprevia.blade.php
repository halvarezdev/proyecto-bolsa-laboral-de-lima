<!DOCTYPE html>
<html lang="en">
<?php
$host= $_SERVER["HTTP_HOST"];
$url= $_SERVER["REQUEST_URI"];
?>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="{{ $descripcion }}">
    <title>{{ $nombre }}</title>
    <meta title="{{ $nombre }}">
<meta name="description" content="{{ $descripcion }}">
<meta name="keywords" content="{{ $descripcion }}">
<meta property="og:title" content="{{ $nombre }}">
<meta property="og:site_name" content="{{ $nombre }}">
<meta property="og:url" content="{{ $urlruta }}">
<meta property="og:description" content="{{ $descripcion }}">
<meta property="og:image" content="{{ asset('storage/'.$image) }}">
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="400" />
<meta property="og:type" content="website">
<meta property="og:locale" content="es_ES">
<link rel="canonical" href="{{ $urlruta }}">
<!-- Styles -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/general.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/login/main.css') }}" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->
<meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
<script>

window.location.replace('{{ $urlruta }}');

</script>
</body>
</html>
