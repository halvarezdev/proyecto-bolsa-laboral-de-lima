<!DOCTYPE html>
<html lang="en">
<?php
$host= $_SERVER["HTTP_HOST"];
$url= $_SERVER["REQUEST_URI"];
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/general.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/login/main.css') }}" />
    <title>{{ $nombre }}</title>
    <meta title="{{ $nombre }}">
<meta name="description" content="{{ $descripcion }}">
<meta name="keywords" content="trabajos en Peru, trabajos, empleo, bolsa de trabajo, empleos, avisos de trabajo, avisos de empleo, buscar trabajo,curriculum, currículum, accede peru, accede">
<meta property="og:title" content="{{ $nombre }}">
<meta property="og:site_name" content="{{ $nombre }}">
<meta property="og:url" content="<?php echo "https://" . $host . $url; ?>">
<meta property="og:description" content="{{ $descripcion }}">
<meta property="og:image" content="{{ asset('storage/'.$image) }}">
<meta property="og:type" content="website">
<meta property="og:locale" content="es_ES">

<link rel="canonical" href="<?php echo "https://" . $host . $url; ?>">
<!-- Styles -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->
</head>
<body>
        <img src="{{ asset('storage/'.$image) }}" alt="" style="width:300px;">
        <?php
                                echo Share::currentPage('accede.com.pe')
                                ->facebook();
                            ?>

                            <script>
                                document.getElementByClass("social-button").text() = "compartir";
                            </script>
</body>
</html>
