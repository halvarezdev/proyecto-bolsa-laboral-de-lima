<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/general.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/login/main.css') }}" />
</head>
<body>
<br><br>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form method="post" action="{{ route('marqueting.save') }}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="emailHelp" placeholder="Ingrese Nombre">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripcion</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion">
                    </div>
                    <div class="form-group">
                        <label for="urlruta">Codigo Institucion</label>
                        <input type="text" class="form-control" id="urlruta" name="urlruta" placeholder="Url">
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <div class="form-group">
                        <input type="file" class="form-control-file" id="image" name="image">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</body>
</html>
