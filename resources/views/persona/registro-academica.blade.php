<style type="text/css">
 /*   .ui-datepicker-calendar {
    display: none;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current {
  
    display: none;
}
.ui-datepicker .ui-datepicker-buttonpane {
     margin: 0 0 0 0; 
}*/
</style>
<div class="card registroformulario">
                <div class="card-body " >
                    <div class=" form-group row justify-content-md-center">
                        <div class="col-sm-10" style="background:#003dc7">
                            <h5 class="titulo_formulario ">
                                FORMACIÓN ACADEMICA
                            </h5>
                        </div>
                    </div>
                <br>
                   <!-- NIVEL -->

                        <div class="form-group row justify-content-md-center">
                            <label for="nivel_estudio" class="col-md-2 col-form-label col-form-label-sm">Maximo nivel de estudio alcanzado</label>

                            <div class="col-md-6">
                                <select class="form-control form-control-sm" name="nivel_estudio" id="nivel_estudio" onchange="return nivelEstudio(this)"> 
                                    @foreach($grado_intruccion as $grado)
                                    <option value="{{ $grado->id }}">{{ $grado->grad_descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>    

                    <div class="form-group row justify-content-md-center">
                        <div class="col-sm-8">
                            <h5>Educación Primaria</h5>
                        </div>
                    
                    </div>
                     <!-- NIVEL -->

                     <div class="form-group row justify-content-md-center">
                     
                            <label for="centro_estudio1" class="col-md-2 col-form-label col-form-label-sm">Centro de Estudios</label>

                            <div class="col-md-6">
                                <input id="centro_estudio" type="text" class="form-control form-control-sm" name="centro_estudio" value="{{ old('centro_estudio') }}" placeholder="Ingrese Centro de Estudio">

                                @if ($errors->has('centro_estudio'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('centro_estudio') }}</strong>
                                    </span>
                                @endif
                            </div>   
                    </div>


                    <!-- FECHA INICIO -->

                    <div class="form-group row justify-content-md-center">
                     
                     <label class="col-md-2 col-form-label col-form-label-sm">Fecha Inicio</label>

                     <div class="col-md-6">
                         <input id="fecha_incio1" type="text" class="form-control form-control-sm date-pickerf" name="fecha_incio1" value="{{ old('fecha_incio1') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">
                     </div>   
             </div>
            

             <!--  FECHA TERMINO -->

             <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_fin1" class="col-md-2 col-form-label col-form-label-sm">Fecha Termino</label>

                     <div class="col-md-6">
                         <input id="fecha_fin1" type="text" class="form-control form-control-sm date-pickerf" name="fecha_fin1" value="{{ old('fecha_fin1') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">

                     </div>   
             </div>
             <script>
        $( function() {
            $( "#date-pickerf" ).datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            maxDate: '0',
            currentText: 'Hoy',
            closeText: 'Aceptar',
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dateFormat: 'mm/yy',
            onClose: function(dateText, inst) { 
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
                                        });
                                    } );
                            </script>

             <div class="form-group row justify-content-md-center">
                        <div class="col-sm-8">
                            <h5>Educación Secundaria</h5>
                        </div>
                    
                    </div>
                     <!-- NIVEL -->

                     <div class="form-group row justify-content-md-center">
                     
                            <label for="centro_estudio2" class="col-md-2 col-form-label col-form-label-sm">Centro de Estudios</label>

                            <div class="col-md-6">
                                <input id="centro_estudio2" type="text" class="form-control form-control-sm " name="centro_estudio2" value="{{ old('centro_estudio2') }}" placeholder="Ingrese Centro de Estudio">
                            </div>   
                    </div>


                    <!-- FECHA INICIO -->

                    <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_incio2" class="col-md-2 col-form-label col-form-label-sm">Fecha Inicio</label>

                     <div class="col-md-6">
                         <input id="fecha_incio2" type="text" class="form-control form-control-sm date-pickerf" name="fecha_incio2" value="{{ old('fecha_incio2') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">
                     </div>   
             </div>



             <!--  FECHA TERMINO -->

             <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_fin2" class="col-md-2 col-form-label col-form-label-sm">Fecha Termino</label>

                     <div class="col-md-6">
                         <input id="fecha_fin2" type="text" class="form-control form-control-sm date-pickerf" name="fecha_fin2" value="{{ old('fecha_fin2') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">
                     </div>   
             </div>


            <!-- EDUCACION TECNICA  -->

            <div class="form-group row justify-content-md-center">
                        <div class="col-sm-8">
                            <h5>Educación Tecnica</h5>
                        </div>
                    
                    </div>

                    <!-- ESPECIALIDAD -->

                        <div class="form-group row justify-content-md-center">
                            <label class="col-md-2 col-form-label col-form-label-sm">Especialidad</label>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <select id="especialidad3" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Especialidad" name="especialidad3" >
                                        <option value="">Buscar</option>
                                        @foreach($prefesiones as $profesion)
                                        <option value="{{ $profesion->id }}">{{ $profesion->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <script>
                                $('#especialidad3').selectize({
                                create: true,
                                sortField: {
                                    field: 'text',
                                    direction: 'asc'
                                },
                                dropdownParent: 'body'
                                });
                                </script>
                            </div>
                        </div>  


                    <!-- CENTRO DE ESTUDIOS -->

                        <div class="form-group row justify-content-md-center">
                            <label for="centro_estudio3" class="col-md-2 col-form-label col-form-label-sm">Centro de Estudios</label>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <select id="centro_estudio3" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Centro de Estudio" name="centro_estudio3">
                                        <option value="">Buscar</option>
                                        @foreach($centro_estudio as $estudio)
                                    <option value="{{ $estudio->CESTP_CODIGO }}">
                                        {{ $estudio->CESTC_DESCRIPCION }}
                                    </option>
                                    @endforeach
                                    </select>
                                </div>
                                <script>
                                $('#centro_estudio3').selectize({
                                create: true,
                                sortField: {
                                    field: 'text',
                                    direction: 'asc'
                                },
                                dropdownParent: 'body'
                                });
                                </script>
                               
                            </div>
                        </div> 

                     <!-- NIVEL -->

                     <div class="form-group row justify-content-md-center">
                     
                            <label for="otro_centro_estudio3" class="col-md-2 col-form-label col-form-label-sm">Otro Centro de Estudios</label>

                            <div class="col-md-6">
                                <input id="otro_centro_estudio3" type="text" class="form-control form-control-sm" name="otro_centro_estudio3" value="{{ old('otro_centro_estudio3') }}" placeholder="Ingrese Centro de Estudio">

                                @if ($errors->has('telefono'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>   
                    </div>


                    <!-- FECHA INICIO -->

                    <div class="form-group row justify-content-md-center">
                     
                     <label class="col-md-2 col-form-label col-form-label-sm">Fecha Inicio</label>

                     <div class="col-md-6">
                         <input id="fecha_incio3" type="text" class="form-control form-control-sm date-pickerf" name="fecha_incio3" value="{{ old('fecha_incio3') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">

                     </div>   
             </div>



             <!--  FECHA TERMINO -->

             <div class="form-group row justify-content-md-center">
                     
                     <label class="col-md-2 col-form-label col-form-label-sm">Fecha Termino</label>

                     <div class="col-md-6">
                         <input id="fecha_fin3" type="text" class="form-control form-control-sm date-pickerf" name="fecha_fin3" value="{{ old('fecha_fin3') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">

                         @if ($errors->has('fecha_fin3'))
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('fecha_fin3') }}</strong>
                             </span>
                         @endif
                     </div>   
             </div>


<!-- EDUCACION POST GRADO -->

 <!-- EDUCACION TECNICA  -->

                    <div class="form-group row justify-content-md-center">
                        <div class="col-sm-8">
                            <h5>Educación Universitario</h5>
                        </div>
                    
                    </div>

                    <!-- ESPECIALIDAD -->

                        <div class="form-group row justify-content-md-center">
                            <label class="col-md-2 col-form-label col-form-label-sm">Especialidad</label>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <select id="especialidad4" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Especialidad" name="especialidad4">
                                        <option value="">Buscar</option>
                                        @foreach($prefesiones as $profesion)
                                        <option value="{{ $profesion->id }}">{{ $profesion->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <script>
                                $('#especialidad4').selectize({
                                create: true,
                                sortField: {
                                    field: 'text',
                                    direction: 'asc'
                                },
                                dropdownParent: 'body'
                                });
                                </script>
                            </div>
                        </div>  


                    <!-- CENTRO DE ESTUDIOS -->

                        <div class="form-group row justify-content-md-center">
                            <label class="col-md-2 col-form-label col-form-label-sm">Centro de Estudios</label>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <select id="centro_estudio4" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Centro de Estudio" name="centro_estudio4">
                                        <option value="">Buscar</option>
                                        @foreach($centro_estudio as $estudio)
                                    <option value="{{ $estudio->CESTP_CODIGO }}">
                                        {{ $estudio->CESTC_DESCRIPCION }}
                                    </option>
                                    @endforeach
                                    </select>
                                </div>
                                <script>
                                $('#centro_estudio4').selectize({
                                create: true,
                                sortField: {
                                    field: 'text',
                                    direction: 'asc'
                                },
                                dropdownParent: 'body'
                                });
                                </script>
                            </div>
                        </div> 



                     <!-- NIVEL -->

                     <div class="form-group row justify-content-md-center">
                     
                            <label for="otro_centro_estudio4" class="col-md-2 col-form-label col-form-label-sm">Otro Centro de Estudios</label>

                            <div class="col-md-6">
                                <input id="otro_centro_estudio4" type="text" class="form-control form-control-sm" name="otro_centro_estudio4" value="{{ old('otro_centro_estudio4') }}" placeholder="Ingrese Otro Centro de Estudio">

                                @if ($errors->has('otro_centro_estudio4'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('otro_centro_estudio4') }}</strong>
                                    </span>
                                @endif
                            </div>   
                    </div>


                    <!-- FECHA INICIO -->

                    <div class="form-group row justify-content-md-center">
                     
                     <label class="col-md-2 col-form-label col-form-label-sm">Fecha Inicio</label>

                     <div class="col-md-6">
                         <input id="fecha_incio4" type="text" class="form-control form-control-sm date-pickerf" name="fecha_incio4" value="{{ old('fecha_incio4') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">

                     </div>   
             </div>



             <!--  FECHA TERMINO -->

             <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_fin4" class="col-md-2 col-form-label col-form-label-sm">Fecha Termino</label>

                     <div class="col-md-6">
                         <input id="fecha_fin4" type="text" class="form-control form-control-sm date-pickerf" name="fecha_fin4" value="{{ old('fecha_fin4') }}" placeholder="Ingrese Fecha" onclick="return estiloFecha()">

                     </div>   
             </div>

                                
            <div class="form-group row justify-content-md-center">
                <div class="col-sm-8">
                    <h5>Estudios Adicionales</h5>
                </div>
            </div>                        

            <!--  tipo -->
            <div class="form-group row justify-content-md-center">   
                <label class="col-md-2 col-form-label col-form-label-sm">Tipo</label>
                <div class="col-md-6">
                    <select name="estudio_tipo" id="estudio_tipo" class="form-control form-control-sm">
                        <option value="">SELECCIONE</option>
                        <option value="1">CURSO</option>
                        <option value="2">SEMINARIO</option>
                        <option value="3">TALLER</option>
                        <option value="4">CONFERENCIA</option>
                        <option value="5">CONGRESO</option>
                        <option value="6">CHARLA</option>
                        <option value="7">DIPLOMADO</option>            
                    </select>
                </div>   
            </div>
            
             <!--  NOMBRE DEL CURSO -->

             <div class="form-group row justify-content-md-center">
                     
                     <label class="col-md-2 col-form-label col-form-label-sm">Nombre del Curso</label>

                     <div class="col-md-6">
                         <input id="nombre_curso" type="text" class="form-control form-control-sm" name="nombre_curso" value="{{ old('nombre_curso') }}" placeholder="Ingrese Nombre del curso">
                     </div>   
             </div>


              <!--  NOMBRE DEL CURSO -->

              <div class="form-group row justify-content-md-center">
                     
                     <label class="col-md-2 col-form-label col-form-label-sm">Centro de Estudio</label>

                     <div class="col-md-6">
                         <input id="centro_curso" type="text" class="form-control form-control-sm" name="centro_curso" value="{{ old('centro_curso') }}" placeholder="Ingrese Centro de Eestudio">
                     </div>   
             </div>


        </div>
    </div>
