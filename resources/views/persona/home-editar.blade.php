<script>
    function soloLetrasHoja(e){
       key = e.keyCode || e.which;
       if (key==8) return true;
       if(key==32) { // backspace.
               
            }else{
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";
 
       
       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }
        //if(e.target.name != 'nombre'){
           
        //} 

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
    }
    function soloLetrasHojaV2(event){
       // /^[a-zA-9-ZñÑ\s\W]/ 
       // "^[a-zA-Z0-9 ]+$"
    var regex = new RegExp("^[a-zA-Z0-ÑñáéíóúAÉÍÓÚú0-9 ]+$");
  var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (key==8) return true;
  if (!regex.test(key)) {
    event.preventDefault();
    return false;
 }
}
</script>

<div class="card">
    <div class="card-body">
        <form action="{{ route('users.guardar') }}" method="post" id="formHomeEditar" >
        @csrf
            <div class="form-row ">
                <div class="form-group col-md-6">
                    <label>Primer Apellido<span>(*)</span></label>
                    <input id="ape_paterno" onpaste="return false"  onkeypress="return soloLetrasHoja(event);" type="text" class="form-control form-control-sm" name="ape_paterno" 
                                            value="@foreach($personas as $persona){{ ucwords($persona->ape_pat) }}@endforeach" style="text-transform:uppercase;"  required>
                </div>

                 <div class="form-group col-md-6">
                    <label>Segundo Apellido<span>(*)</span></label>
                    <input id="ape_materno" onpaste="return false"  onkeypress="return soloLetrasHoja(event);" type="text" class="form-control form-control-sm" name="ape_materno" 
                                            value="@foreach($personas as $persona){{ ucwords($persona->ape_mat) }}@endforeach" style="text-transform:uppercase;"  required>
                </div>   
 
    
                <div class="form-group col-md-6">
                    <label>Nombre <span>(*)</span></label>
                    <input id="nombre" onpaste="return false"  onkeypress="return soloLetrasHoja(event);" type="text" class="form-control form-control-sm" name="nombre" value="{{ ucwords(Auth::user()->name) }}" required style="text-transform:uppercase;" >
                </div> 
                <div class="form-group col-md-6">
                    <label>País<span>*</span></label>
                    <?php echo \Form::select('pais',$lista_pais,$id_pais,array('Class'=>'form-control form-control-sm selected','id'=>'pais', 'required' => 'required')) ?>
                </div>
                <div class="form-group col-md-6">
                    <label>Fecha de Nacimiento <span>(*)</span></label>
                    <input id="fecha_nac" onpaste="return false"  type="date" class="form-control form-control-sm" name="fecha_nac" 
                            value="@foreach($personas as $persona){{ $persona->fecha_nacimiento }}@endforeach" required
                            max="{{ date('Y-m-d') }}">        
                </div> 


                <div class="form-group col-md-6">
                    <label>Sexo <span>(*)</span></label>
                    <?php  
                    $hom = ""; 
                    $mun = ""; 
                    ?>
                    @foreach($personas as $persona)
                        @if($persona->sexo == "1")
                            <?php  $hom = "checked"; ?>
                        @endif
                        @if($persona->sexo == "2")
                            <?php  $mun = "checked"; ?>
                        @endif
                    @endforeach
                    <input type="radio"  id="sexo" value="1" name="sexo" {{ $hom }} required>
                    <label  for="masculino">Masculino</label>
                    &nbsp;&nbsp;
                    <input type="radio"  id="sexo2" value="2" name="sexo" {{ $mun }} required>
                    <label  for="2">Femenino</label>
                </div> 
                @foreach($personas as $per)
                    <?php
                        $dep = substr(''.$per->ubigeo.'',0,2); 
                        $prov = substr(''.$per->ubigeo.'',2,2); 
                        $dist = substr(''.$per->ubigeo.'',4,2); 
                        
                    ?>
                @endforeach
                 <?php
                 $departamento = isset($dep) ? $dep : '15';
                 $provincia = isset($prov) ? $prov : '15';
                 $distrito = isset($dist) ? $dist : '15';
                 ?>   
                <div class="form-group col-md-12">
                    <label>Domicilio <span>(*)</span></label>
                    <div class="row">
                        <div class="col-md-4">
                        <?php echo \Form::select('deparme',$lista_depa,$departamento,array('Class'=>'form-control form-control-sm selected', 'required' => 'required','id'=>'departamento','style'=>'width: 90%','onchange'=>'ComboboxProvincia(this)')) ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo \Form::select('provi',$lista_prov,$provincia,array('Class'=>'form-control form-control-sm selected', 'required' => 'required','id'=>'provincia','style'=>'width: 90%','onchange'=>'ComboboxDistrito(this)')) ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo \Form::select('distri',$lista_dist,$distrito,array('Class'=>'form-control form-control-sm selected', 'required' => 'required','id'=>'distrito','style'=>'width: 90%')) ?>
                        </div>  
                    </div>
                </div> 

                <div class="form-group col-md-7">
                    <label>Dirección <span>(*)</span></label>
                    <input id="direccion" onpaste="return false" onkeypress="return soloLetrasHojaV2(event);"  type="text" class="form-control form-control-sm" name="direccion" 
                                            value="@foreach($personas as $persona){{ ucwords($persona->direccion) }}@endforeach" style="text-transform:uppercase;" required>
                </div> 


                <div class="form-group col-md-3">
                    <label>Teléfono </label>
                    <input id="telefono" type="number" onpaste="return false"   class="form-control form-control-sm" name="telefono" 
                                            value="@foreach($personas as $persona){{ ucwords($persona->telefono) }}@endforeach">
                </div> 

                 <div class="form-group col-md-2">
                    <label>Pretensión Salarial</label>
                   <!-- <input id="salario" type="text" class="form-control form-control-sm" name="salario" value="@foreach($ficha as $persona){{ ucwords($persona->fic_salario) }}@endforeach"> -->
                    <select class="form-control form-control-sm" name="salario" id="salario" >
                        <option value="">Seleccione</option> 
                        <option value="1" @foreach($ficha as $persona){{ ($persona->fic_salario == "1" ? "selected" : "") }}@endforeach>0 - 930</option>
                        <option value="2" @foreach($ficha as $persona){{ ($persona->fic_salario == "2" ? "selected" : "") }}@endforeach>930 - 1200</option>
                        <option value="3" @foreach($ficha as $persona){{ ($persona->fic_salario == "3" ? "selected" : "") }}@endforeach>1200 - 1500</option>
                        <option value="4" @foreach($ficha as $persona){{ ($persona->fic_salario == "4" ? "selected" : "") }}@endforeach>1500 - 3000</option>
                        <option value="5" @foreach($ficha as $persona){{ ($persona->fic_salario == "5" ? "selected" : "") }}@endforeach>3000 - 5000</option>
                        <option value="6" @foreach($ficha as $persona){{ ($persona->fic_salario == "6" ? "selected" : "") }}@endforeach>5000 a mas</option>
                    </select>
                </div> 

        </div>



        <div class="form-row ">
                 <div class="form-group col-md-3">
            <label>¿Tiene licencia de conducir?</label>
            <select class="form-control form-control-sm" name="licenciaConduc" id="licenciaConduc" onchange="limitCategoria(this)">
                <option value="">Seleccione</option> 
                <option value="2" @foreach($ficha as $persona){{ ($persona->licenciaConduc == "2" ? "selected" : "") }}@endforeach >No</option>
                <option value="1" @foreach($ficha as $persona){{ ($persona->licenciaConduc == "1" ? "selected" : "") }}@endforeach >Si</option>
            </select>
          </div>

          <div class="form-group col-md-3"><br>
            <label>Categoría:</label>
            <select class="form-control form-control-sm" name="categoria" id="categoria" @foreach($ficha as $persona){{ ($persona->licenciaConduc == "2" ? "disabled" : "") }}@endforeach >
                <option value="">Seleccione</option> 
                <option value="1" @foreach($ficha as $persona){{ ($persona->categoria == "1" ? "selected" : "") }}@endforeach >A-I</option>
                <option value="2" @foreach($ficha as $persona){{ ($persona->categoria == "2" ? "selected" : "") }}@endforeach >A-IIa</option>
                <option value="3" @foreach($ficha as $persona){{ ($persona->categoria == "3" ? "selected" : "") }}@endforeach >A-IIb</option>
                <option value="4" @foreach($ficha as $persona){{ ($persona->categoria == "4" ? "selected" : "") }}@endforeach >A-IIIa</option>
                <option value="5" @foreach($ficha as $persona){{ ($persona->categoria == "5" ? "selected" : "") }}@endforeach >A-IIIb</option>
            </select>
          </div>

          <div class="form-group col-md-3">
            <label>¿Tiene permiso para portar armas?</label>
            <select class="form-control form-control-sm" name="port_armas" id="port_armas" onchange="limitArmas(this)">
                <option value="">Seleccione</option> 
                <option value="2" @foreach($ficha as $persona){{ ($persona->porta_arma == "2" ? "selected" : "") }}@endforeach >No</option>
                <option value="1" @foreach($ficha as $persona){{ ($persona->porta_arma == "1" ? "selected" : "") }}@endforeach >Si</option>
            </select>
          </div>

          <div class="form-group col-md-3"><br>
            <label>¿Tiene arma propia?</label>
            <select class="form-control form-control-sm" name="arma_propia" id="arma_propia" @foreach($ficha as $persona){{ ($persona->porta_arma == "2" ? "disabled" : "") }}@endforeach >>
                <option value="">Seleccione</option> 
                <option value="2" @foreach($ficha as $persona){{ ($persona->arma_propia == "2" ? "selected" : "") }}@endforeach >No</option>
                <option value="1" @foreach($ficha as $persona){{ ($persona->arma_propia == "1" ? "selected" : "") }}@endforeach >Si</option>
            </select>
          </div>


      </div>
      <div class="form-row ">

      <div class="form-group col-md-8">
        <label>¿Qué tipo de trabajo buscas? <span>(*)</span></label>
            <select name="ocu_trabajo" id="ocu_trabajo" class="selectpicker show-tick form-control form-control-sm" data-width="100%" data-style="select-style" data-live-search="true" required>
                <option value="">Seleccione</option>
            @foreach($ocupaciones as $value)
                <option value="{{ $value->ocup_codigo }}" @foreach($ficha as $persona){{ ($persona->ocu_trabajo == $value->ocup_codigo ? "selected" : "") }}@endforeach>{{ $value->nombre }}</option>
            @endforeach
            </select>
          </div>

          <div class="form-group col-md-4">
            <label>¿Dónde lo buscas? <span>(*)</span></label>
            <select name="ubi_lugar" id="ubi_lugar" class="form-control form-control-sm selectpicker show-menu-arrow"  data-style="select-style" required>
                <option value="">Seleccione</option>
            @foreach($ubigeo as $ubi)
                <option value="{{ $ubi->ubi_codigo }}" @foreach($ficha as $persona){{ ($persona->ubi_lugar == $ubi->ubi_codigo ? "selected" : "") }}@endforeach>{{ $ubi->ubi_descripcion }}</option>
            @endforeach
            </select>
          </div>
        </div>
        <div class="form-row ">

                <div class="form-group col-md-12">
                <label>Video CV (Si tienes una presentación en video, copia en el recuadro el link de tu video para que sea visto por las empresas) 
                    <a href="{{asset('/GUIA_VIDEO_CV.pdf')}}" target="_blank">Guía Vídeo Cv</a></label>
                    <input id="url_cv"  type="text" class="form-control form-control-sm" name="url_cv" 
                                            value="@foreach($ficha as $f){{ $f->url_cv }}@endforeach" >
                </div>     

        </div>
      <div class="form-row ">

                <div class="form-group col-md-12">
                    <label>Presentación Breve </label>
                    <textarea  onpaste="return false" onkeypress="return soloLetrasHojaV2(event);" style="text-transform:uppercase;" class="form-control" id="presentacion" rows="3" name="presentacion" placeholder="Ingrese una presentacion breve" required autofocus>@foreach($ficha as $persona){{ $persona->presentacion }}@endforeach</textarea>
                </div>     

          
            <input class="btn btn-primary" type="submit" value="Guardar" id="btnHomeEditar">
        </div>
        </form>

    </div>
</div>
                   
<script>
function limitCategoria(input){
    if(input.value == "2"){
        document.getElementById("categoria").disabled=true;
    }else{
        document.getElementById("categoria").disabled=false;
    }
}

function limitArmas(input){
    if(input.value == "2"){
        document.getElementById("arma_propia").disabled=true;
    }else{
        document.getElementById("arma_propia").disabled=false;
    }
}

function calcularEdad(fecha) {
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
}

function esMayorEdad(fecha){
    var edad = calcularEdad(fecha);
    if(edad >= 18){
        return true;
    }else{
        return false;
    }
}



document.addEventListener('DOMContentLoaded', function() {

    document.getElementById('btnHomeEditar').addEventListener('click', function(e) {
        e.preventDefault();
        var fecha = document.getElementById('fecha_nac').value;
        var edad = calcularEdad(fecha);
        if(edad < 18){
            alert("La fecha indica que no eres mayor de edad");
            return;
        }
        document.getElementById('formHomeEditar').submit();
    });

    
});

window.onload = function() {
    $('.selectpicker').selectpicker('refresh');
};



</script>