
<style>
.timeline {
  list-style-type: none;
  display: flex;
  align-items: center;
  justify-content: center;
}

.li {
  transition: all 200ms ease-in;
}

.timestamp {
  margin-bottom: 20px;
  padding: 0px 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-weight: 100;
}

.status {
  padding: 0px 40px;
  display: flex;
  justify-content: center;
  border-top: 2px solid #D6DCE0;
  position: relative;
  transition: all 200ms ease-in;
}

.status:before {
  content: "";
  width: 18px;
  height: 18px;
  background-color: white;
  border-radius: 18px;
  border: 1px solid #ddd;
  position: absolute;
  top: -10px;
  left: 42%;
  transition: all 200ms ease-in;
}

.li.complete .status {
  border-top: 2px solid #003dc7;
}
.li.complete .status:before {
  background-color: #003dc7;
  border: none;
  transition: all 200ms ease-in;
}
.li.complete .status .texto {
  color: #003dc7;
}

@media (min-device-width: 320px) and (max-device-width: 700px) {
  .timeline {
    list-style-type: none;
    display: block;
  }

  .li {
    transition: all 200ms ease-in;
    display: flex;
    width: inherit;
  }

  .timestamp {
    width: 100px;
  }

  .status:before {
    left: -8%;
    top: 30%;
    transition: all 200ms ease-in;
  }
}

/*
button {
  position: absolute;
  width: 100px;
  min-width: 100px;
  padding: 20px;
  margin: 20px;
  font-family: "Titillium Web", sans serif;
  border: none;
  color: white;
  font-size: 16px;
  text-align: center;
}*/
/*
#toggleButton {
  position: absolute;
  left: 50px;
  top: 20px;
  background-color: #75C7F6;
}*/

</style>


<div class="container pb-3">
  <div class="card mb-3" style="min-height: 650px;">
    <div class="card-body">
      <div class="row degrade-rojo " style="border-radius:0.5rem; margin-left: 1px; margin-right: 1px;" >
          <div class="col " style="padding:0.5rem;">&nbsp;
              <i class="fas fa-folder-open" style="font-size: 19px; color:#fff"></i>&nbsp;
              <b style="color:#fff">MIS POSTULACIONES</b>
          </div>
      </div>
      <br>
      <div class="row">
         <div class="col">
          <div class="table-responsive-sm">
              <table class="table table-sm table-hover">
                  <thead>
                      <tr>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">N°</th>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }};width:15%;">Empresa</th>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }};width:25;">Vacante</th>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Postulantes</th>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Estado</th>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Estado de Publicación</th>
                          <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Postulación</th>
                          <th colspan="3" class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Acciones</th>
                     
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($list_postulaciones as $i => $value)
                          <tr>
                              <td class="td-top">{{ $i+1 }}</td>
                              <td class="td-top">{{ strtoupper($value->razon_social) }}</td>
                              <td class="td-top">{{ strtoupper($value->descripcion) }}</td>
                              <td class="td-top" style="text-align:center"><i class="fas fa-users"></i>&nbsp;&nbsp;{{ $value->cant_postulantes }}</td>
                              <td class="td-top">
                                  @if($value->PP_Visto == "1")
                                      Su postulacion fue visto por la empresa
                                  @endif
                                  @if($value->PP_Visto == "0" || $value->PP_Visto == "")
                                      Su postulacion esta pendiente   
                                  @endif
                              </td>
                              <td class="td-top">
                                  @if($value->flag_estado == "0")
                                      La vancante se encuentra cerrado
                                  @endif
                                  @if($value->flag_estado == "1")
                                      La vacante se encuentra en proceso
                                  @endif
                              </td>
                              <td class="td-top" style="text-align:center">{{ $value->PP_Fecha }}</td>
                              <th class="td-top" style="text-align:center;">
                              <a  rel="Ver vacante" href="{{ route('vacante.detalle',['id' => $value->pedido_codigo, 'e' => $entidad] ) }}"><i class="fas fa-eye"></i></a>
                              </th>
                              <th>
                              <a href="#"  rel="Retirarme"><i class="fas fa-times-circle"></i></a>
                              </th>
                              <th>
                                  <!-- Button trigger modal -->
                                  <a href="#"  rel="Linea de Tiempo" onclick="listarTimeTiempo(this,'{{ $value->pedido_codigo }}')">
                                  <i class="fas fa-stream"></i>
                                  </a>
                              </th>
                          </tr>
                          <tr style="display:none;" id="time_tiempo{{ $value->pedido_codigo }}">
                              <td colspan="10">
                              <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700' rel='stylesheet' type='text/css'>
                                  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
                               <!--   <button id="toggleButton">Toggle</button> -->
                                  <input value="0" id="oculto{{ $value->pedido_codigo }}" type="hidden">
                                  <ul class="timeline" id="timeline">
                                  <li class="li complete">
                                      <div class="timestamp">
                                      <span class="date">{{ $value->PP_Fecha }}<span>
                                      </div>
                                      <div class="status">
                                      <b class="texto">Postulación </b>
                                      </div>
                                  </li>
                                  <li class="li {{ isset($value->PP_Visto) && $value->PP_Visto != 0 ? 'complete' : '' }}">
                                      <div class="timestamp">
                                      <span class="date">{{ isset($value->Fecha_visto) ? $value->Fecha_visto : '...' }}<span>
                                      </div>
                                      <div class="status">
                                      <b class="texto">Su Cv fue visto</b>
                                      </div>
                                  </li>
                                  <li class="li {{ isset($value->PostPed_apto) && $value->PostPed_apto != 0 ? 'complete' : '' }}">
                                      <div class="timestamp">
                                      <span class="date">{{ isset($value->PostPed_aptoFecha) ? $value->PostPed_aptoFecha : '...' }}<span>
                                      </div>
                                      <div class="status">
                                      <b class="texto">En revision</b>
                                      </div>
                                  </li>
                                  <li class="li {{ isset($value->PP_Status) && $value->PP_Status != 0 ? 'complete' : '' }}">
                                      <div class="timestamp">
                                      <span class="date">{{ isset($value->PP_StatusFecha) ? $value->PP_StatusFecha : '...' }}<span>
                                      </div>
                                      <div class="status">
                                      <b class="texto">Entrevista</b>
                                      </div>
                                  </li>
                                  <li class="li {{ isset($value->PostPed_contra) && $value->PostPed_contra != 0 ? 'complete' : '' }}">
                                      <div class="timestamp">
                                      <span class="date">{{ isset($value->PostPed_contraFecha) ? $value->PostPed_contraFecha : '...' }}<span>
                                      </div>
                                      <div class="status">
                                      <b class="texto">Contratado</b>
                                      </div>
                                  </li>
                                 <li class="li {{ isset($value->flag_estado) && $value->flag_estado == 0 ? 'complete' : '' }}" >
                                      <div class="timestamp">
                                      <span class="date">..<span>
                                      </div>
                                      <div class="status">
                                      <b class="texto"> Fin de Proceso </b>
                                      </div>
                                  </li>
                                  
                                  </ul>      
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
              {{ $list_postulaciones->links() }}
              </div>
          </div>
      </div>
</div>
  </div>
</div>


<script>

function listarTimeTiempo(tipe,id){
    
    var oculto = $("#oculto"+id).val();
    if(oculto == '0'){
        $("#oculto"+id).val("1");
        $("#time_tiempo"+id).css("display","table-row");
    }else{
       // alert("cerrar");
        $("#oculto"+id).val("0");
        $("#time_tiempo"+id).css("display","none");
    }

}

     /*
var completes = document.querySelectorAll(".complete");
var toggleButton = document.getElementById("toggleButton");


function toggleComplete(){
 var lastComplete = completes[completes.length - 1];
 lastComplete.classList.toggle('complete');
}

toggleButton.onclick = toggleComplete;
*/

</script>
