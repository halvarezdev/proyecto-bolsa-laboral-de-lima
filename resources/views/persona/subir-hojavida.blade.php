<div class="card">
    <div class="card-body">
    <div class="alert alert-sucess" role="sucess" id="sucess" style="display:none;">
            </div>
            <div class="alert alert-danger" role="alert" id="error" style="display:none;">
              ¡Tipo de documento no Permitido!
            </div>
        <form action="{{ route('users.save.file') }}" method="post" enctype="multipart/form-data" onsubmit="validarExt(event)">
        
            @csrf
            
            <input type="file" name="file" id="file" class="form-control" >
                <br>
                <spam style="color:red">Formato Pdf/Word - Tamaño Max:1MB</spam>
<br><br>

                <input class="btn btn-primary" type="submit" value="Adjuntar Archivo" >
        </form>
        <br>
         <div id="visorArchivo"></div>
        </div>
</div>

<script>
    function validarExt(event){
        
        var archivoInput = document.getElementById("file");
        var archivoRuta = archivoInput.value;
        
        var extPermitidas = /(.pdf|.doc|.docx)$/i;
        //alert(archivoInput.value);
        if(!extPermitidas.exec(archivoRuta)){
            event.preventDefault();
            document.getElementById("sucess").style = "display:none";
            document.getElementById("error").style = "display:block";
            alert("Tipo de archivo incorrecto");
            archivoInput.value = "";
            return false;
        }else{
            alert("Tipo de archivo correcto");
            document.getElementById("nombrefile").innerText = archivoInput.value;
            if(archivoInput.files && archivoInput.files[0]){
                var visor = new FileReader();
                visor.onload = function(e){
                    document.getElementById("visorArchivo").innerHTML = "<embed src='"+e.target.result+"' width='100%' height='500' />" ;     
                };
                visor.readAsDataURL(archivoInput.files[0]);
            }
        }
    }

  // Add the following code if you want the name of the file appear on select

/*$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
*/

</script>
<style type="text/css">
      .custom-file-input ~ .custom-file-label::after {
            content: "Seleccionar";
        }
</style>