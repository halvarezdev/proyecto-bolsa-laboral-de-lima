<div class="container">
	<div class="row" style="border-radius: 12px;margin-top: 20px;padding-top: 10px;padding-bottom: 20px;background-color: #d8d8d8;">
		<div class="col">
			<h4>Modificar correo electrónico y contraseña</h4>
			<div class="card">
			  <div class="card-body">
			    <div class="row">
			    	<div class="col-sm-11">
			    		
			    		Esta registrado con el e-mail <b>{{ Auth::user()->email }}</b> .
			    	
			    	</div>
			    	<div class="col-sm-1">
			    		<a href="">Editar</a>
			    	</div>
			    </div>
			    <br><br>
			    <div class="form_config row">
			    	<div class="col">
			    		<form action="{{ route('update.configuracion') }}" method="post">
			    			@csrf
						  <div class="form-row">
						    <div class="col-md-4 mb-3">
						      <label for="validationCustom02">Email</label>
						      <div class="input-group">
						        <div class="input-group-prepend">
						          <span class="input-group-text" id="inputGroupPrepend">@</span>
						        </div>
						        <input type="email" class="form-control" id="email" name="email" placeholder="Email" aria-describedby="inputGroupPrepend" required>
						        @if ($errors->has('email'))
                                <span role="alert">
                                    <strong style="color:red;">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
						      </div>
						    </div>
						    <div class="col-md-4 mb-3">
						      <label for="validationCustomUsername">Contraseña</label>
						      <div class="input-group">
						        <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" aria-describedby="inputGroupPrepend" required>
						        @if ($errors->has('password'))
                                <span role="alert">
                                    <strong style="color:red;">{{ $errors->first('password') }}</strong>
                                </span>
                            	@endif
						      </div>
						    </div>
						    <div class="col-md-4 mb-3">
						      <label for="validationCustomUsername">...</label>
						      <div class="input-group">
						        <div class="input-group-prepend">
						        </div>
						        <button class="btn btn-primary" type="submit">Modificar</button>
						      </div>
						    </div>
						  </div>
						</form>
			    	</div>
			    </div>
			  </div>
			</div>
			<!-- SEGUNDA PARTE  -->
<br><br>
			<h4>Modificar nivel de privacidad.</h4>
			<div class="card">
			  <div class="card-body">
			    <div class="form_config row">
			    	<div class="col">
			    		<form action="{{ route('update.curri') }}" method="post">
			    			@csrf
						  	<div class="form-check">
						    	<input type="radio" class="form-check-input " id="curri_visible" name="curri_visible" value="1" {{ ($personas[0]->per_curri_visible == "1" ? "checked" : "") }}>
						    	<label class="form-check-label" for="exampleCheck1">Curriculum visible para las empresas</label>
						  	</div>
							<div class="form-check">
						    	<input type="radio" class="form-check-input" id="curri_visible" name="curri_visible" value="0" {{ ($personas[0]->per_curri_visible == "0" ? "checked" : "") }}>
						    	<label class="form-check-label" for="exampleCheck1">Curriculum no visible para las empresas</label>
						    	<small id="emailHelp" class="form-text text-muted">Nunca compartiremos tus datos con nadie más. Solamente las empresas a cuyos avisos has postulado pueden visualizar tus datos y contactarte.</small>
						  	</div>
						 <br>
						  <button type="submit" class="btn btn-primary">Modificar</button>
						</form>
			    	</div>
			    </div>
			  </div>
			</div>

			<!-- TERCERA PARTE  -->

<br><br>
			<h4>Eliminar Curriculum</h4>
			<div class="card">
			  <div class="card-body">
			    <div class="form_config row">
			    	<div class="col">
			    		<form action="{{ route('update.elimina') }}" method="post">
			    			@csrf
						  	<div class="form-check">
						    	<input type="checkbox" class="form-check-input" id="elimina" name="elimina" value="1">
						    	<label class="form-check-label" for="exampleCheck1">Eliminar Curriculum</label>
						    	<small id="emailHelp" class="form-text text-muted">Esta opción hace que tu curriculum se elimine haciendo imposible recuperarlo en un futuro. Para voler a tener tu curriculum tendrás que crearlo y subirlo de nuevo.</small>
						  	</div>
						 <br>
						  <button type="submit" class="btn btn-primary">Eliminar</button>
						</form>
			    	</div>
			    </div>
			  </div>
			</div>


		</div>
	</div>
</div>

<br>
<br>