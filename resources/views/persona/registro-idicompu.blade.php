<div class="card ">
    <div class="card-body " >
                    <div class=" form-group row justify-content-md-center">
                        <div class="col-sm-10" style="background:#003dc7">
                            <h5 class="titulo_formulario ">
                                IDIOMA Y COMPUTACION
                            </h5>
                        </div>
                    </div>
                <br>
                    <!-- COMPUTACION E IDIOMA  -->
                    <div class="form-group row justify-content-md-center">
                        <div class="col-sm-8">
                            <h5>Idioma</h5>
                        </div>
                    
                    </div>


                    <!-- IDIOMA -->

                        <div class="form-group row justify-content-md-center">
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="idioma1" id="idioma1"> 
                                    <option value="0">Seleccione</option>
                                    @foreach($idiomas as $idioma)
                                    <option value="{{ $idioma->MAESP_CODIGO }}">{{ $idioma->MAESC_DESCRIPCION }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="nivel_idioma1" id="nivel_idioma1">
                                    <option value="0">Seleccione</option> 
                                    <option value="0">Seleccione</option> 
                                    <option value="Basico">Basico</option>
                                    <option value="Intermedio">Intermedio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>  

                    <!-- IDIOMA -->

                    <div class="form-group row justify-content-md-center">
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="idioma2" id="idioma2"> 
                                    <option value="0">Seleccione</option>
                                    @foreach($idiomas as $idioma)
                                    <option value="{{ $idioma->MAESP_CODIGO }}">{{ $idioma->MAESC_DESCRIPCION }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="nivel_idioma2" id="nivel_idioma2"> 
                                    <option value="0">Seleccione</option> 
                                    <option value="Basico">Basico</option>
                                    <option value="Intermedio">Intermedio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>


                        <!-- IDIOMA -->

                        <div class="form-group row justify-content-md-center">
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="idioma3" id="idioma3"> 
                                    <option value="0">Seleccione</option>
                                    @foreach($idiomas as $idioma)
                                    <option value="{{ $idioma->MAESP_CODIGO }}">{{ $idioma->MAESC_DESCRIPCION }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="nivel_idioma3" id="nivel_idioma3"> 
                                    <option value="0">Seleccione</option> 
                                    <option value="Basico">Basico</option>
                                    <option value="Intermedio">Intermedio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                     <!-- COMPUTACION E IDIOMA  -->
                     <div class="form-group row justify-content-md-center">
                        <div class="col-sm-8">
                            <h5>Computacion</h5>
                        </div>
                    
                    </div>


                    <!-- COMPUTACION -->

                        <div class="form-group row justify-content-md-center">
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="compu1" id="compu1"> 
                                    <option value="0">Seleccione</option>
                                    @foreach($computacion as $compu)
                                    <option value="{{ $compu->MAESP_CODIGO }}">{{ $compu->MAESC_DESCRIPCION }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="nivel_compu1" id="nivel_compu1"> 
                                    <option value="0">Seleccione</option> 
                                    <option value="Basico">Basico</option>
                                    <option value="Intermedio">Intermedio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>  

                    <!-- IDIOMA -->

                    <div class="form-group row justify-content-md-center">
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="compu2" id="compu2"> 
                                    <option value="0">Seleccione</option>
                                    @foreach($computacion as $compu)
                                    <option value="{{ $compu->MAESP_CODIGO }}">{{ $compu->MAESC_DESCRIPCION }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="nivel_compu2" id="nivel_compu2"> 
                                    <option value="0">Seleccione</option> 
                                    <option value="Basico">Basico</option>
                                    <option value="Intermedio">Intermedio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>


                        <!-- IDIOMA -->

                        <div class="form-group row justify-content-md-center">
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="compu3" id="compu3"> 
                                    <option value="0">Seleccione</option>
                                    @foreach($computacion as $compu)
                                    <option value="{{ $compu->MAESP_CODIGO }}">{{ $compu->MAESC_DESCRIPCION }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <select class="form-control form-control-sm" name="nivel_compu3" id="nivel_compu3"> 
                                    <option value="0">Seleccione</option> 
                                    <option value="Basico">Basico</option>
                                    <option value="Intermedio">Intermedio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>




    </div>

</div>
