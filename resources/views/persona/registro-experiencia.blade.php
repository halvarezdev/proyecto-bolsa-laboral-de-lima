<div class="card ">
    <div class="card-body " >
                    <div class=" form-group row justify-content-md-center">
                        <div class="col-sm-10" style="background:#003dc7">
                            <h5 class="titulo_formulario ">
                                EXPERIENCIA LABORAL
                            </h5>
                        </div>
                    </div>
                <br>
    <!-- EMPRESA -->

                <div class="form-group row justify-content-md-center">
                     
                     <label for="empresa" class="col-md-2 col-form-label col-form-label-sm">Empresa</label>

                     <div class="col-md-6">
                         <input id="empresa" type="text" class="form-control form-control-sm" name="empresa" value="{{ old('empresa') }}" placeholder="Ingrese Empresa">

                         @if ($errors->has('empresa'))
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('empresa') }}</strong>
                             </span>
                         @endif
                     </div>   
             </div>

   <!-- OCUPACION -->

            <div class="form-group row justify-content-md-center">
                     
                     <label for="ocupacion" class="col-md-2 col-form-label col-form-label-sm">Ocupacion</label>

                     <div class="col-md-6">
                        
                            <div class="control-group">
                                    <select id="ocupacion" class="demo-default form-control form-control-sm" data-placeholder="Seleccione Ocupacion" name="ocupacion">
                                        <option value="">Buscar</option>
                                        @foreach($ocupaciones as $ocupacion)
                                            <option value="{{ $ocupacion->ocup_codigo }}" >{{ $ocupacion->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <script>
                                $('#ocupacion').selectize({
                                create: true,
                                sortField: {
                                    field: 'text',
                                    direction: 'asc'
                                },
                                dropdownParent: 'body'
                                });
                                </script>
                     </div>   
             </div>

   <!-- FUNCIONES A CARGO -->

   <div class="form-group row justify-content-md-center">
                     
                     <label for="funciones_cargo" class="col-md-2 col-form-label col-form-label-sm">Funciones a Cargo</label>

                     <div class="col-md-6">
                         
                         <textarea name="" id="" class="form-control form-control-sm"></textarea>
                         @if ($errors->has('funciones_cargo'))
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('funciones_cargo') }}</strong>
                             </span>
                         @endif
                     </div>   
             </div>

              <!-- FECGHA  INICIO -->

            <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_ini_empresa" class="col-md-2 col-form-label col-form-label-sm">Fecha de Inicio</label>

                     <div class="col-md-6">
                         <input id="fecha_ini_empresa" type="date" class="form-control form-control-sm" name="fecha_ini_empresa" value="{{ old('fecha_ini_empresa') }}" placeholder="Ingrese Fecha">

                         @if ($errors->has('fecha_ini_empresa'))
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('fecha_ini_empresa') }}</strong>
                             </span>
                         @endif
                     </div>   
             </div>


              <!-- FECHA FIN -->

 <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_fin_empresa" class="col-md-2 col-form-label col-form-label-sm">Fecha Fin </label>

                     <div class="col-md-6">
                         <input id="fecha_fin_empresa" type="date" class="form-control form-control-sm" name="fecha_fin_empresa" value="{{ old('fecha_fin_empresa') }}" placeholder="Ingrese Fecha">

                         @if ($errors->has('fecha_fin_empresa'))
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('fecha_fin_empresa') }}</strong>
                             </span>
                         @endif
                     </div>   
             </div>

             <div class="form-group row justify-content-md-center">                    
                     <label for="fecha_fin" class="col-md-10 col-form-label-sm">Podras agregar mas de una experiencia laboral despues de haber terminado todos los pasos de registro de la ficha de postulante</label>
    
             </div>

    </div>
</div>