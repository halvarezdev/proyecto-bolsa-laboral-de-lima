            <style>
.border-lista{
    border-left: 4px solid {{ $subcolor_entidad }};
}
</style>
<div class="container">

<div class="card">
        <div class="card-body">

            <div class="row degrade-azul " style="border-radius:0.5rem; margin-left: 1px; margin-right: 1px;margin-bottom: 1%;" >
                <div class="col-12 col-sm-12 col-md-10" style="padding:0.5rem;">&nbsp;
                    <i class="far fa-file-alt" style="font-size: 19px; color:#fff"></i>&nbsp;
                    <b style="color:#fff">VACANTES SUGERIDAS</b>
                </div>
                <div class="col-12 col-sm-12 col-md-2">
                <a  style="margin-top:0.3rem;padding: 0.1rem .70rem; background-color:#00A4D3;border:solid 1px {{ $entidad_color }};font-size:13px;" class="btn btn-primary nav-sub" href="{{ route('users.busqueda.inicio', ['e' => (isset($entidad) ? $entidad : '')]) }}">Buscar vacante</a>

                  <!--  <a  style="margin-top:0.3rem;padding: 0.1rem .70rem; background-color:#00A4D3;border:solid 1px {{ $entidad_color }};font-size:13px;" class="btn btn-primary nav-sub" href="{{ route('user.buscar') }}">Buscar vacante</a>
                 -->
                  <!--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Buscar vacante</button> -->
                </div>
              <!--
                  <div class="col-12 col-sm-12 col-md-1">
                    <form action="{{ route('convocatoria') }}" method="get">
                    <input type="hidden" value="2" name="flag_ubi">
                    <a style="margin-top:0.3rem;padding: 0.1rem .70rem; background-color:#00A4D3;border:solid 1px {{ $entidad_color }};font-size:13px;" class="btn btn-primary nav-sub" >Limpiar</a>
                    </form>
                </div>
                -->
            </div>

            @if(isset($list_convoca) && $list_convoca != false)
            @foreach($list_convoca as $value)
            @if($value->procedencia == '1')
            <a href="{{ route('vacante.detalle',['id' => $value->cod_ped, 'e' => isset($entidad) ? $entidad : '1'] ) }}" style="text-decoration: none;">
           @else
           <a href="{{ route('vacante.detalle.publica',['id' => $value->cod_ped, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" style="text-decoration: none;">
            @endif
            <div class="card border-lista" style="margin-bottom: 1%;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-2">
                            <img class="img-fluid img-thumbnail" src="{{ asset('storage/'.$value->perfil_foto) }}" alt="Imagen Empresa" style="width:90px;height:90px">
                        </div>
                        <div class="col">
                            <span style="color:{{ $entidad_color }}; font-size:16px;color:black;">{{ $value->razon_social }}  </span><br>
                            <b>{{ $value->descripcion }}</b>
                            <footer class="blockquote-footer" style="padding-left: 0px;">
                                <cite class="Source Title">
                                {{ $value->otro_conocimiento }}
                                </cite>
                            </footer>
                        </div>
                        <div class="col-12 col-md-2">
                            <span style="color:black;"><b>Fecha:</b> {{ date_format($value->created_at,'d/m/Y') }}</span><br>
                            <span style="color:black;"><b>Lugar:</b> {{ $value->ubi_descripcion }}</span><br><br>
                            <b style="color:{{ $entidad_color }};">ver vacante</b>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            @endforeach
<br>
            {{ $list_convoca->links() }}
        @endif
        </div>
    </div>



</div>
