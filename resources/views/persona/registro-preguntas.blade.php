<div class="card ">
    <div class="card-body " >
        <div class=" form-group row justify-content-md-center">
            <div class="col-sm-10" style="background:#003dc7">
                <h5 class="titulo_formulario ">
                    PREGUNTAS
                </h5>
            </div>
        </div>
                <br>

        <div class="form-group row justify-content-md-center">
            <label for="fecha_fin" class="col-md-10 col-form-label-sm">Una vez respondida todas sus preguntas, su postulacion se registrara con exito </label>
        </div>

                       <!-- PREGUNTA 1 -->

            <div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_fin" class="col-md-6 col-form-label col-form-label-sm">¿Tiene diponibilidad inmediata? </label>

                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="disponibilidad1" name="disponibilidad" value="si" required>
                        <label class="custom-control-label" for="disponibilidad1">si</label>
                    </div>&nbsp;&nbsp;
                    <div class="custom-control custom-radio mb-4">
                        <input type="radio" class="custom-control-input" id="disponibilidad2" name="disponibilidad" value="no" required>
                        <label class="custom-control-label" for="disponibilidad2">no</label>
                         <div class="invalid-feedback">No valido</div>
                    </div> 
             </div>


<!-- PREGUNTA 2 -->
<div class="form-group row justify-content-md-center">
                     
                     <label for="fecha_fin" class="col-md-6 col-form-label col-form-label-sm">¿Declaras postular en esta convocatoria de manera transparente y de acurdo a las condiciones señaladas por la empresa? </label>

                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="declaracion1" name="declaracion" value="si" required>
                        <label class="custom-control-label" for="declaracion1">si</label>
                    </div>&nbsp;&nbsp;
                    <div class="custom-control custom-radio mb-4">
                        <input type="radio" class="custom-control-input" id="declaracion2" name="declaracion" value="no" required>
                        <label class="custom-control-label" for="declaracion2">no</label>
                         <div class="invalid-feedback">No valido</div>
                    </div> 
             </div>


             <!-- PREGUNTA 3 -->

             <div class="form-group row justify-content-md-center">
                     
                     <label for="pregunta3" class="col-md-6 col-form-label col-form-label-sm">¿Declaras tener habiles tus derechos civiles laborales? </label>

                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="derechos_laborales1" name="derechos_laborales" value="si" required>
                        <label class="custom-control-label" for="derechos_laborales1">si</label>
                    </div>&nbsp;&nbsp;
                    <div class="custom-control custom-radio mb-4">
                        <input type="radio" class="custom-control-input" id="derechos_laborales2" name="derechos_laborales" value="no" required>
                        <label class="custom-control-label" for="derechos_laborales2">no</label>
                         <div class="invalid-feedback">No valido</div>
                    </div> 
             </div>

        <!-- PREGUNTA 4 -->


        <div class="form-group row justify-content-md-center">
                     
                     <label for="pretencion" class="col-md-2 col-form-label col-form-label-sm">¿Cúanto deseas ganar?</label>

                     <div class="col-md-6">
                         <input id="pretencion" type="text" class="form-control form-control-sm" name="pretencion" value="{{ old('pretencion') }}" required autofocus placeholder="INGRESE CANTIDAD">

                         @if ($errors->has('pretencion'))
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('pretencion') }}</strong>
                             </span>
                         @endif
                     </div>   
             </div>


          


 </div>               
 </div>    