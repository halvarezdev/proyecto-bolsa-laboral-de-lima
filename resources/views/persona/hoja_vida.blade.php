
<style>
.img-perfil {
    width: 100px;
    height: 110px;
@if(isset($ruta_foto))
    @if($ruta_foto=='')
    background-image: url('{{ asset('public/images/no-photo.png') }}');
       
    @else
    background-image: url('{{ asset('/public/storage/'.$ruta_foto) }}');
    @endif
@else
    background-image: url('{{ asset('public/images/no-photo.png') }}');
@endif
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
} 
/*.img-perfil>span{
  display:none;
  height:30px;
  }

.img-perfil>span:hover{
  display:block;
  position:absolute;
  height:30px;
  bottom:0;
  left:0;
  background:rgba(0,0,0,0.7);
  width:100%;
  height:30px;
  vertical-align:middle;
  text-align:center
  
}
*/
.subirimg{
  position:absolute;
  bottom:5;
  /*left:0;*/
  background:rgba(0,0,0,0.7);
  width:100px;
  height:30px;
  vertical-align:middle;
  text-align:center;
  color:#fff;
  cursor:pointer;
}

</style>

<script>
function divhover(objeto){
  //alert("todo esta aqui");
  document.getElementById('span').style.display = "block";
}
function normalImg(objeto){
  document.getElementById('span').style.display = "none";
}

function subirfoto(){
//alert("alertcrj");
 $("#input_file").click();
}
      
function uptapeFotoPerfil(input){
  var archivoRuta =  document.getElementById("input_file").value;
  var extPermitidas = /(.png|.jpg|.jpge)$/i;

  if(!extPermitidas.exec(archivoRuta)){
    alert("Tipo de archivo incorrecto");
    return false;
  }
  
  size = input.files[0].size;
  if (size > 100 * 1024) {
    alert("La foto debe tener un tamaño no superior a 100 KB.");
    return false;
  }else{
    $( "#form_foto" ).submit();
  }

   /* var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlData = base_url + "edit/basico/" + codigo;
    $.ajax({
        type: 'GET',
        url: urlData,
        dataType: 'json',
        data:formu.serialize(),
        success: function(data) {
            $.each(data, function(key, value) {
                // console.log(value);
              
            });
        }
    });
*/
}


</script>
<div class="container">
    <div class="row">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
        <input type="hidden" id="base_url" value="{{ asset('') }}">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card mb-3" style="border: 1px solid #1A4A84;border-radius: 10px;">
                <div class="card-body">
                    <center>
                        <div class="row">
                            <div class="col">
                            <a href="#" id="uploadImage" onclick="subirfoto()">
                                <div  class="img-perfil rounded mx-auto d-block photo_perfil imagen-login" onmouseover="divhover(this)" onmouseout="normalImg(this)">
                                      <div class="subirimg" id="span" style="display:none;">
                                      <label id="subirimg" style="cursor:pointer;"><i class="fas fa-upload" ></i>&nbsp;Subir</label>
                                      </div>
                                </div>
                                </a> 
                                <form action="{{ route('users.ajax.foto') }}" method="post" enctype="multipart/form-data" id="form_foto">
                                  @csrf
                                  <input type="file" name="file" id="input_file" onchange="uptapeFotoPerfil(this)" style="width:0.1px;
  height:0.1px;
  opacity:0;
  overflow:hidden;
  position:absolute;
  z-index:-1;">
                                </form>
                              
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <b class="">
                               @if(count($personas)>0)
                                    @foreach($personas as $persona)
                                    {{ strtoupper($persona->name) }}&nbsp;{{ strtoupper($persona->ape_pat) }}&nbsp;{{ strtoupper($persona->ape_mat) }}
                                    @endforeach
                                    @endif;
                                </b>
                            </div>
                        </div>
                    </center>
                    <br>
                    <h5 class="card-title " style="color:{{ $subcolor_entidad }}">Información Personal</h5>

                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fas fa-calendar-alt"></i>
                        </div>
                        <div class="col-sm-10">
                            <b class="card-text">Fecha de nacimiento</b>
                            <p class="card-text">
                                @foreach($personas as $persona)
                                <?php 
                                if(isset($persona->fecha_nacimiento) && $persona->fecha_nacimiento != null && $persona->fecha_nacimiento != ''){
                                $porciones = explode("-", $persona->fecha_nacimiento);
                                  echo $porciones[2]."/".$porciones[1]."/".$porciones[0];
                              }else{
                                  echo "-"; 
                              } ?>
                                @endforeach
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fas fa-flag"></i>
                        </div>
                        <div class="col-sm-10">
                            <b class="card-text color-rojo">Nacionalidad</b>
                            <p class="card-text">
                                @if(isset($ficha))
                                @foreach($ficha as $fich)
                                {{ (isset($fich->pais_descripcion) ? $fich->pais_descripcion : "...") }}
                                @endforeach
                                @else
                                ...
                                @endif

                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="col-sm-10">
                            <b class="card-text color-rojo">Lugar de residencia</b>
                            <p class="card-text">
                                @foreach($personas as $persona)
                                {{ (isset($persona->ubi_descripcion) && $persona->ubi_descripcion != "" && $persona->ubi_descripcion != null ? $persona->ubi_departamento . ' - ' . $persona->ubi_departamento . ' - ' . $persona->ubi_descripcion : "-") }}
                                @endforeach
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fas fa-id-card"></i>
                        </div>
                        <div class="col-sm-10">
                            <b class="card-text color-rojo">Número de documento</b>
                            <p class="card-text">
                                {{ Auth::user()->numero_documento }}
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-sm-10">
                            <b class="card-text color-rojo">Correo Electrónico</b>
                            <p class="card-text">
                                {{ Auth::user()->email }}
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="row align-items-center align-self-center">
                        <div class="col">
                            <center>
                                <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                    data-target="#segudofile"
                                    style='padding: 0.1rem 2.7rem; background-color:{{ $entidad_color }};border:solid 1px {{ $entidad_color }};font-famlily:font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";"'>
                                    <i class="fas fa-user-edit" style="color:#fff;"></i>&nbsp;Editar Perfil
                                </a>
                            </center>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col align-items-center align-self-center">
                            <center>
                                <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                    data-target="#adjuntarcv"
                                    style='padding: 0.1rem 2.5rem; background-color:{{ $subcolor_entidad }};border:solid 1px {{ $subcolor_entidad }};font-famlily:font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";"'>
                                    <i class="fas fa-paperclip" style="color:#fff;"></i>&nbsp;Adjuntar Cv
                                </a>
                            </center>
                        </div>
                        
                    </div>
<br>
                    <div class="row">
                    <div class="col"></div>
                    <div class="col-5">
                        @foreach($ficha as $fich)
                                  @if(isset($fich->filecv) && $fich->filecv != null)
                                  &nbsp; <a  target="_blank"  href="{{asset('public/storage/'.$fich->filecv)}}" class="badge badge-success">Visualizar</a>
                                  @endif
                                @endforeach   
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- fin datos personales -->
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">

            <!-- INICIO FORMACION ACADEMICA -->
            <div class="card">
                <div class="card-header degrade-griss" id="headingOne" style="padding: 0.10rem 0.90rem;">
                    <div class="row ">
                        <div class="col ">
                            <div class="d-none d-sm-none d-md-block">
                                <p style="color:#fff;font-size:22px;margin-top: 0.1rem;margin-bottom: 0.2rem;">Mi Curriculum Vitae</p>
                            </div>
                            <div class="d-block d-sm-block d-md-none">
                                <p style="color:#fff;font-size:14px;margin-top: 0.1rem;margin-bottom: 0.2rem;">Mi Curriculum Vitae</p>
                            </div>
                            
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                            <a href="#" class="btn btn-primary nav-sub"
                                style="margin-top:0.5rem;padding: 0.1rem .70rem; background-color:#333333;border:solid 1px {{ $entidad_color }};"
                                onclick="return DescargarHojavida('{{ $ficha[0]->ficha_id }}','{{ $ficha[0]->persona_id }}');">
                                <span style="float:right; padding-right: 4px; " class="badge">Visualizar CV</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="accordion" id="accordionExample">
                <div class="card">

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordionExample">
                        <div class="card-body" style="padding-top:0.5rem">

                            <div class="row degrade-rojo " style="border-radius:0.5rem;">
                                <div class="col " style="padding:0.5rem;">
                                    <i class="fas fa-graduation-cap" style="font-size: 19px; color:#fff"></i>&nbsp;<b
                                        style="color:#fff">INFORMACIÓN ACADÉMICA</b>
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col">
                                    <b style="font-size: 16px;color:{{ $subcolor_entidad }};">FORMACIÓN BÁSICA</b>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-4 col-4">
                                    <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                        data-target="#formacionbasica"
                                        style="margin-top: 0.5rem;padding: 0.1rem .70rem; float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px {{ $subcolor_entidad }};"
                                        onclick="return limpiarBasico()">
                                        <span style="float:right; padding-right: 4px;" class="badge">
                                            Agregar
                                        </span>
                                    </a>
                                </div>
                            </div>

                            <div class="table-responsive-sm">
                                <table class="table table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                N°</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Centro</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Nivel</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Inicio</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Fin</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}"
                                                colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($formacion_basica as $key => $formacion)
                                        <tr>
                                            <td class="td-top">{{ $key+1 }}</td>
                                            <td class="td-top">{{ $formacion->perbas_centroestudio }}</td>
                                            <td class="td-top">{{ $formacion->grad_descripcion }}</td>
                                            <td class="td-top">{{ $formacion->perbas_fechaini  }}</td>
                                            <td class="td-top">{{ $formacion->perbas_fecafin }}</td>
                                            <td class="td-top">
                                                <a href="#" data-toggle="modal" data-target="#formacionbasica"
                                                    onclick="return editarBasico('{{ $formacion->basica_id }}',this)">
                                                    <i class="fas fa-pencil-alt"
                                                        style="color:{{ $subcolor_entidad }}"></i>
                                                </a>
                                            </td>
                                            <td class="td-top">
                                                <a href="{{ route('elimina.basico',['id' => $formacion->basica_id]) }}">
                                                    <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <b style="font-size: 16px;color:{{ $subcolor_entidad }};">FORMACIÓN SUPERIOR</b>
                                </div>
                                <div class="col-2">
                                    <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                        data-target="#formasionsuperior"
                                        style="padding: 0.1rem .70rem; float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px {{ $subcolor_entidad }};"
                                        onclick="return limpiarSuperior()">
                                        <span style="float:right; padding-right: 4px;" class="badge">
                                            Agregar
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive-sm">
                                <table class="table table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                N°</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Especialidad</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Nivel</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Estado</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Centro</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Inicio</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Fin</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}"
                                                colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($formacion_superior as $key => $value)
                                        <tr>
                                            <td style="font-size: 12px;">{{ $key+1 }}</td>
                                            <td style="font-size: 12px;">{{ $value->nombre }}</td>
                                            <td style="font-size: 12px;">{{ $value->grad_descripcion }}</td>
                                            <td style="font-size: 12px;">
                                                <?php
                                            if($value->persu_estado == "15"){
                                              echo "TITULADO";
                                            }elseif($value->persu_estado == "13"){
                                              echo "EGRESADO";
                                            }elseif($value->persu_estado == "33"){
                                              echo "TRUNCO";
                                            }elseif($value->persu_estado == "16"){
                                              echo "COLEGIADO TITULADO";
                                            }elseif($value->persu_estado == "14"){
                                              echo "BACHILLER";
                                            }elseif($value->persu_estado == "34"){
                                              echo "CICLOS FINALES";
                                            }elseif($value->persu_estado == "35"){
                                              echo "CICLOS INTERMEDIOS";
                                            }elseif($value->persu_estado == "36"){
                                              echo "CICLOS INICIALES";
                                            }
                                            
                                            ?>
                                            </td>
                                            <td style="font-size: 12px;">{{ $value->otro_centro }}</td>
                                            <td style="font-size: 12px;">{{ $value->fecha_inicio  }}</td>
                                            <td style="font-size: 12px;">{{ $value->fecha_fin }}</td>
                                            <td style="font-size: 12px;">
                                                <a href="#" data-toggle="modal" data-target="#formasionsuperior"
                                                    onclick="return editarSuperior('{{ $value->superior_id }}',this)">
                                                    <i class="fas fa-pencil-alt"
                                                        style="color:{{ $subcolor_entidad }}"></i>
                                                </a>
                                            </td>
                                            <td style="font-size: 12px;">
                                                <a href="{{ route('elimina.superior',['id' => $value->superior_id]) }}">
                                                    <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div id="collapseThree" class="collapse show" aria-labelledby="headingThree"
                        data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <b style="font-size: 16px;color:{{ $subcolor_entidad }};">POSGRADO Y PROGRAMAS DE
                                        ESPECIALIZACIÓN</b>
                                </div>
                                <div class="col-2">
                                    <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                        data-target="#posgrado"
                                        style="padding: 0.1rem .70rem;float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px {{ $subcolor_entidad }};"
                                        onclick="return limpiarPosgrado()">
                                        <span style="float:right; padding-right: 4px;" class="badge">
                                            Agregar
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive-sm">
                                <table class="table table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                N°</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Especialidad</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Nivel</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Estado</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Centro</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Inicio</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                                Fin</th>
                                            <th class="th-top"
                                                style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}"
                                                colspan="2"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($posgrado as $key => $value)
                                        <tr>
                                            <td style="font-size: 12px;">{{ $key+1 }}</td>
                                            <td style="font-size: 12px;">{{ $value->especialidad_id }}</td>
                                            <td style="font-size: 12px;">{{ $value->grad_descripcion }}</td>
                                            <td style="font-size: 12px;">
                                                <?php
                                            if($value->postsu_estado == "15"){
                                              echo "TITULADO";
                                            }elseif($value->postsu_estado == "13"){
                                              echo "EGRESADO";
                                            }elseif($value->postsu_estado == "33"){
                                              echo "TRUNCO";
                                            }elseif($value->postsu_estado == "37"){
                                              echo "COLEGIADO";
                                            }elseif($value->postsu_estado == "38"){
                                              echo "DOCTOR";
                                            }elseif($value->postsu_estado == "39"){
                                              echo "MAESTRO";
                                            }elseif($value->postsu_estado == "34"){
                                              echo "COMPLETO";
                                            }
                                            
                                            ?>
                                            </td>
                                            <td style="font-size: 12px;">{{ $value->otro_centro }}</td>
                                            <td style="font-size: 12px;">{{ $value->fecha_inicio  }}</td>
                                            <td style="font-size: 12px;">{{ $value->fecha_fin }}</td>
                                            <td style="font-size: 12px;">
                                                <a href="#" data-toggle="modal" data-target="#posgrado"
                                                    onclick="return editarPosgrado('{{ $value->posgrado_id }}',this)">
                                                    <i class="fas fa-pencil-alt"
                                                        style="color:{{ $subcolor_entidad }}"></i>
                                                </a>
                                            </td>
                                            <td style="font-size: 12px;">
                                                <a href="{{ route('elimina.posgrado',['id' => $value->posgrado_id]) }}">
                                                    <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN FORMACION ACADEMICA -->
            <br>
            <!-- INICIO IDIOMAS -->

            <div class="card">
                <div id="idioma" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">

                        <div class="row degrade-rojo" style="border-radius:0.5rem;">
                            <div class="col " style="padding:0.5rem;">
                                <i class="fas fa-language " style="font-size: 19px; color:#fff"></i>&nbsp;<b
                                    style="color:#fff">IDIOMA</b>
                            </div>
                            <div class="col-2">
                                <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                    data-target="#idiomapopup"
                                    style="margin-top: 0.5rem;padding: 0.1rem .70rem;float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px {{ $subcolor_entidad }};border-color:#fff;"
                                    onclick="return limpiarIdioma()">
                                    <span style="float:right; padding-right: 4px;" class="badge">
                                        Agregar
                                    </span>
                                </a>
                            </div>
                        </div>


                        <div class="table-responsive-sm">
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            N°</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Nombre</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Nivel</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}"
                                            colspan="2"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($idiomas as $key => $value)
                                    <tr>
                                        <td style="font-size: 12px;">{{ $key+1 }}</td>
                                        <td style="font-size: 12px;">{{ $value->MAESC_DESCRIPCION }}</td>
                                        <td style="font-size: 12px;">
                                            @if($value->idioma_nivel == "1")
                                            Basico
                                            @endif
                                            @if($value->idioma_nivel == "2")
                                            Intermedio
                                            @endif
                                            @if($value->idioma_nivel == "3")
                                            Avanzado
                                            @endif
                                        </td>
                                        <td style="font-size: 12px;">
                                            <a href="#" data-toggle="modal" data-target="#idiomapopup"
                                                onclick="return editarIdioma('{{ $value->idioma_id }}',this)">
                                                <i class="fas fa-pencil-alt" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        <td style="font-size: 12px;">
                                            <a href="{{ route('elimina.idioma',['id' => $value->idioma_id]) }}">
                                                <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>

                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN IDIOMAS -->
            <br>

            <!-- COMPUTACION -->
            <div class="card">
                <div id="computacion" class="collapse show" aria-labelledby="headingThree"
                    data-parent="#accordionExample">
                    <div class="card-body">

                        <div class="row degrade-rojo" style="border-radius:0.5rem;">
                            <div class="col" style="padding:0.5rem;">
                                <i class="fas fa-laptop color_azul_text"
                                    style="font-size: 19px; color:#fff"></i>&nbsp;<b style="color:#fff">CONOCIMIENTO DE
                                    INFORMÁTICA</b>
                            </div>
                            <div class="col-2">
                                <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                    data-target="#compupopup"
                                    style="margin-top: 0.5rem;padding: 0.1rem .70rem;float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px #fff;"
                                    onclick="return limpiarCompu()">
                                    <span style="float:right; padding-right: 4px;" class="badge">
                                        Agregar
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="table-responsive-sm">
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            N°</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Nombre</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Nivel</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}"
                                            colspan="2"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($computaciones as $key => $value)
                                    <tr>
                                        <td style="font-size: 12px;">{{ $key+1 }}</td>
                                        <td style="font-size: 12px;">{{ $value->MAESC_DESCRIPCION }}</td>
                                        <td style="font-size: 12px;">
                                            @if($value->compu_nivel == "1")
                                            Basico
                                            @endif
                                            @if($value->compu_nivel == "2")
                                            Intermedio
                                            @endif
                                            @if($value->compu_nivel == "3")
                                            Avanzado
                                            @endif
                                        </td>
                                        <td style="font-size: 12px;">
                                            <a href="#" data-toggle="modal" data-target="#compupopup"
                                                onclick="return editarCompu('{{ $value->compu_id }}',this)">
                                                <i class="fas fa-pencil-alt" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        <td style="font-size: 12px;">
                                            <a href="{{ route('elimina.ofimatica',['id' => $value->compu_id]) }}">
                                                <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FIN OCUPACION -->
            <br>
            <!-- INICIO ESTUDIOS ADICIONALES -->
            <div class="card">

                <div id="adicionales" class="collapse show" aria-labelledby="headingThree"
                    data-parent="#accordionExample">
                    <div class="card-body">

                        <div class="row degrade-rojo" style="border-radius:0.5rem;">
                            <div class="col" style="padding:0.5rem;">
                                <i class="fas fa-book-reader " style="font-size: 19px; color:#fff;"></i>&nbsp;<b
                                    style="color:#fff">ESTUDIOS ADICIONALES</b>
                            </div>
                            <div class="col-2">
                                <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                    data-target="#estudiosadiocionales"
                                    style="margin-top: 0.5rem;padding: 0.1rem .70rem;float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px #fff;"
                                    onclick="return limpiarEstudios()">
                                    <span style="float:right; padding-right: 4px;" class="badge">
                                        Agregar
                                    </span>
                                </a>
                            </div>
                        </div>


                        <div class="table-responsive-sm">
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            N°</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Tipo</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Curso</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Centro</th>
                                            <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Fin</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}"
                                            colspan="2"></th>
                                            
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($estudiosadi as $key => $value)
                                    <tr>
                                        <td style="font-size: 12px;">{{ $key+1 }}</td>
                                        <td style="font-size: 12px;">
                                            @if($value->estu_tipo == "1")
                                            Curso
                                            @endif
                                            @if($value->estu_tipo == "2")
                                            Seminario
                                            @endif
                                            @if($value->estu_tipo == "3")
                                            Taller
                                            @endif
                                            @if($value->estu_tipo == "4")
                                            Conferencia
                                            @endif
                                            @if($value->estu_tipo == "5")
                                            Congreso
                                            @endif
                                            @if($value->estu_tipo == "6")
                                            Charla
                                            @endif
                                            @if($value->estu_tipo == "7")
                                            Diplomado
                                            @endif
                                        </td>
                                        <td style="font-size: 12px;">{{ $value->estu_nombre }}</td>
                                        <td style="font-size: 12px;">{{ $value->estu_cetro }}</td>
                                        <td style="font-size: 12px;">{{ $value->fecha_fin }}</td>
                                        <td style="font-size: 12px;">
                                            <a href="#" data-toggle="modal" data-target="#estudiosadiocionales"
                                                onclick="return editarEstudios('{{ $value->adi_id }}',this)">
                                                <i class="fas fa-pencil-alt" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        <td style="font-size: 12px;">
                                            <a href="{{ route('elimina.adicionales',['id' => $value->adi_id]) }}">
                                                <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FIN ESTUDIOS -->

            <br>
            <!-- INICIO EXPERIENCIA LABORAL -->
            <div class="card">

                <div id="exp_laboral" class="collapse show" aria-labelledby="headingThree"
                    data-parent="#accordionExample">
                    <div class="card-body">

                        <div class="row degrade-rojo" style="border-radius:0.5rem;">
                            <div class="col" style="padding:0.5rem;">
                                <i class="fas fa-briefcase color_azul_text"
                                    style="font-size: 19px; color:#fff;"></i>&nbsp;<b style="color:#fff">EXPERIENCIA
                                    LABORAL</b>
                            </div>
                            <div class="col-2">
                                <a href="#" class="btn btn-primary nav-sub" data-toggle="modal"
                                    data-target="#experienciapersona"
                                    style="margin-top: 0.5rem;padding: 0.1rem .70rem;float: right;background-color:{{ $subcolor_entidad }}; border:solid 1px #fff;"
                                    onclick="return limpiarExperiencia()">
                                    <span style="float:right; padding-right: 4px;" class="badge">
                                        Agregar
                                    </span>
                                </a>
                            </div>
                        </div>


                        <div class="table-responsive-sm">
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            N°</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Empresa</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Rubro</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Inicio</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                            Fin</th>
                                        <th class="th-top"
                                            style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($experiencia_liest as $key => $value)
                                    <tr>
                                        <td style="font-size: 12px;">{{ $key+1 }}</td>
                                        <td style="font-size: 12px;">{{ $value->nom_empresa }}</td>
                                        <td style="font-size: 12px;">
                                            {{ ($value->id_ocupacion == "000000" ? $value->otro_cargo : $value->nombre) }}
                                        </td>
                                        <td style="font-size: 12px;">{{ $value->fech_inicio }}</td>
                                        <td style="font-size: 12px;">{{ $value->fech_fin }}</td>
                                        <td style="font-size: 12px;">
                                            <a href="#" data-toggle="modal" data-target="#experienciapersona"
                                                onclick="return editarExperiencia('{{ $value->expe_id }}',this)">
                                                <i class="fas fa-pencil-alt" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        <td style="font-size: 12px;">
                                            <a href="{{ route('elimina.experiencia',['id' => $value->expe_id]) }}">
                                                <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                            </a>
                                        </td>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FIN EXPERIENCIA LABORAL -->


        </div>

    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">ADJUNTAR FOTO DE PERFIL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.foto')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="segudofile" tabindex="-1" role="dialog"
    aria-labelledby="segudofileTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">EDITAR USUARIO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('persona.home-editar')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="formacionbasica" tabindex="-1" role="dialog"
    aria-labelledby="formacionbasica" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">FORMACIÓN BÁSICA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.basica')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="formasionsuperior" tabindex="-1" role="dialog"
    aria-labelledby="formasionsuperior" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">FORMACIÓN SUPERIOR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.superior')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="posgrado" tabindex="-1" role="dialog" aria-labelledby="posgrado"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">POSGRADOS Y PROGRAMAS DE ESPECIALIZACION</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.posgrado')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="idiomapopup" tabindex="-1" role="dialog" aria-labelledby="idiomapopup"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">IDIOMA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.idioma')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="compupopup" tabindex="-1" role="dialog" aria-labelledby="compupopup"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">CONOCIMIENTO DE INFORMATICA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.compu')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="estudiosadiocionales" tabindex="-1" role="dialog"
    aria-labelledby="estudiosadiocionales" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">ESTUDIOS ADICIONALES</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.estudiosadicionales')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="experienciapersona" tabindex="-1" role="dialog"
    aria-labelledby="experienciapersona" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">EXPERIENCIA LABORAL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_personas.experiencia')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="adjuntarcv" tabindex="-1" role="dialog" aria-labelledby="adjuntarcv"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="segudofileitle">ADJUNTAR CV</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('persona.subir-hojavida')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
//function DescargarHojavida(id,PP_id,ficha_id,id_persona,idPedido,fen) {
function DescargarHojavida(ficha_id, id_persona) {
    id = "00";
    PP_id = "00";
    idPedido = "00";
    fen = "2";
    base_url = $("#base_url").val();
    var urlData = base_url + "pdf/downloadcv";
    window.open(urlData + '/' + PP_id + '/' + ficha_id + '/' + id_persona + '/' + idPedido + '/' + fen)
}

function editarBasico(codigo, input) {
    //  $("#myModalDelete").modal('hide')
    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlData = base_url + "edit/basico/" + codigo;
    $.ajax({
        type: 'GET',
        url: urlData,
        dataType: 'json',
        //  data:{_token,PP_id,ficha_id,id_persona,idPedido}, 
        /*   beforeSend: function(){
         $(".loader").css({display:'block'});
        } */
        success: function(data) {
            $.each(data, function(key, value) {
                // console.log(value);
                $("#centro_estudio").val(value.perbas_centroestudio);
                $("#nivel_estudio").val(value.grainst_id);
                $("#fecha_incio1").val(value.perbas_fechaini);
                $("#fecha_fin1").val(value.perbas_fecafin);
                $("#codigo_basico").val(codigo);
                $("#form_basico").attr("action", "{{ route('form.basico.update') }}");
            });
        }
    });

}

function limpiarBasico() {
    $("#centro_estudio").val("");
    $("#nivel_estudio").val("1");
    $("#fecha_incio1").val("");
    $("#fecha_fin1").val("");
    $("#codigo_basico").val("");
    $("#form_basico").attr("action", "{{ route('users.basica') }}");
}


function editarSuperior(codigo, input) {

    var response = "<option value=''>Seleccione</option>";

    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlsuperior = base_url + "edit/superior/" + codigo;

    $.ajax({
        type: 'GET',
        url: urlsuperior,
        dataType: 'json',
        success: function(data_superior) {
            $.each(data_superior, function(key, superior) {
                $("#profesion").val(superior.especialidad_id);
                $("#nivel_estudio_superior").val(superior.nivel_id);
                if (superior.nivel_id == "6") {
                    response += "<option value='15'>TITULADO</option>";
                    response += "<option value='13'>EGRESADO</option>";
                    response += "<option value='33'>TRUNCO</option>";
                    response += "<option value='34'>CICLOS FINALES</option>";
                    response += "<option value='35'>CICLOS INTERMEDIOS</option>";
                    response += "<option value='36'>CICLOS INICIALES</option>";
                }
                if (superior.nivel_id == "7") {
                    response += "<option value='15'>TITULADO</option>";
                    response += "<option value='13'>EGRESADO</option>";
                    response += "<option value='33'>TRUNCO</option>";
                    response += "<option value='16'>COLEGIADO</option>";
                    response += "<option value='14'>BACHILLER</option>";
                    response += "<option value='34'>CICLOS FINALES</option>";
                    response += "<option value='35'>CICLOS INTERMEDIOS</option>";
                    response += "<option value='36'>CICLOS INICIALES</option>";
                }
                $("#estado_superior").html(response);

                $("#otro_centro_estudio3").val(superior.otro_centro);
                $("#fecha_incio1_superior").val(superior.fecha_inicio);
                $("#fecha_fin1_superior").val(superior.fecha_fin);
                $("#codigo_superior").val(codigo);
                $("#form_superior").attr("action", "{{ route('form.superior.update') }}");
                $("#estado_superior").val(superior.persu_estado);
                $('.selectpicker').selectpicker('refresh');
                //  console.log(superior);
            });
        }
    });
}

function limpiarSuperior() {

    $("#profesion").val("");
    $("#nivel_estudio_superior").val("1");
    $("#otro_centro_estudio3").val("");
    $("#fecha_incio1_superior").val("");
    $("#fecha_fin1_superior").val("");
    $("#codigo_superior").val("");
    $("#form_superior").attr("action", "{{ route('users.superior') }}");
    $("#estado_superior").val("");
    $('.selectpicker').selectpicker('refresh');

}

function editarPosgrado(codigo, input) {

    var response = "<option value=''>Seleccione</option>";
    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlsuperior = base_url + "edit/posgrado/" + codigo;

    $.ajax({
        type: 'GET',
        url: urlsuperior,
        dataType: 'json',
        success: function(data_superior) {
            $.each(data_superior, function(key, superior) {
                $("#profesion_posgrado").val(superior.especialidad_id);
                $("#nivel_estudio_posgrado").val(superior.nivel_id);
                if (superior.nivel_id == "26") {
                    // alert(tipo.value);
                    response += "<option value='37'>EN CURSO</option>";
                    response += "<option value='15'>TITULADO</option>";
                    response += "<option value='13'>EGRESADO</option>";
                    response += "<option value='39'>MAESTRO</option>";

                }
                if (superior.nivel_id == "27") {
                    response += "<option value='15'>TITULADO</option>";
                    response += "<option value='13'>EGRESADO</option>";
                    response += "<option value='37'>EN CURSO</option>";
                    response += "<option value='38'>DOCTOR</option>";
                }
                if (superior.nivel_id == "28") {
                    response += "<option value='37'>EN CURSO</option>";
                    response += "<option value='33'>TRUNCO</option>";
                    response += "<option value='40'>COMPLETO</option>";
                }
                if (superior.nivel_id == "29") {
                    response += "<option value='37'>EN CURSO</option>";
                    response += "<option value='33'>TRUNCO</option>";
                    response += "<option value='40'>COMPLETO</option>";
                }
                $("#estado_postgrado").html(response);

                $("#otro_centro_estudio3_posgrado").val(superior.otro_centro);
                $("#fecha_incio1_posgrado").val(superior.fecha_inicio);
                $("#fecha_fin1_posgrado").val(superior.fecha_fin);
                $("#codigo_posgrado").val(codigo);
                $("#form_posgrado").attr("action", "{{ route('form.posgrado.update') }}");
                $("#estado_postgrado").val(superior.postsu_estado);
                //  console.log(superior);
            });
        }
    });

}

function limpiarPosgrado() {

    $("#profesion_posgrado").val("");
    $("#nivel_estudio_posgrado").val("1");
    $("#otro_centro_estudio3_posgrado").val("");
    $("#fecha_incio1_posgrado").val("");
    $("#fecha_fin1_posgrado").val("");
    $("#codigo_posgrado").val("");
    $("#form_posgrado").attr("action", "{{ route('users.posgrado') }}");
    $("#estado_post").val("");
}

function editarIdioma(codigo, input) {

    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlidioma = base_url + "edit/idioma/" + codigo;

    $.ajax({
        type: 'GET',
        url: urlidioma,
        dataType: 'json',
        success: function(data_idioma) {
            $.each(data_idioma, function(key, idioma) {
                // alert(superior.idioma_nom);
                $("#idioma_nombre").val(idioma.idioma_nom);
                $('.selectpicker').selectpicker('refresh');
                $("#nivel_idioma").val(idioma.idioma_nivel);
                $("#codigo_idioma").val(codigo);
                $("#form_idioma").attr("action", "{{ route('form.idioma.update') }}");
                //  console.log(superior);
            });
        }
    });

}

function limpiarIdioma() {

    $("#idioma").val("");
    $("#nivel_idioma").val("");
    $("#idioma_nombre").val("");
    $('.selectpicker').selectpicker('refresh');
    $("#codigo_idioma").val("");
    $("#form_posgrado").attr("action", "{{ route('users.idioma') }}");

}

function editarCompu(codigo, input) {

    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlcompu = base_url + "edit/compu/" + codigo;

    $.ajax({
        type: 'GET',
        url: urlcompu,
        dataType: 'json',
        success: function(data_compu) {
            $.each(data_compu, function(key, compu) {
                // alert(superior.idioma_nom);
                if (compu.compu_nom == "30142") {
                    document.getElementById('otro_computacion').style = "display:block";
                } else {
                    document.getElementById('otro_computacion').style = "display:none";
                }
                $("#compu").val(compu.compu_nom);
                $('select[name=compu]').val(compu.compu_nom);
                $('.selectpicker').selectpicker('refresh');
                $("#nivel_compu").val(compu.compu_nivel);
                $("#codigo_compu").val(codigo);
                $("#otro_compu").val(compu.compu_otro);
                $("#form_compu").attr("action", "{{ route('form.compu.update') }}");
                //  console.log(superior);
            });
        }
    });
}

function limpiarCompu() {

    $("#compu").val("");
    $('.selectpicker').selectpicker('refresh');
    $("#nivel_compu").val("");
    $("#codigo_compu").val("");
    $("#form_compu").attr("action", "{{ route('users.comptacion') }}");

}

function editarEstudios(codigo, input) {

    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urladicional = base_url + "edit/estudios/" + codigo;

    $.ajax({
        type: 'GET',
        url: urladicional,
        dataType: 'json',
        success: function(data_adicional) {
            $.each(data_adicional, function(key, adicional) {
                // alert(superior.idioma_nom);

                $("#estudio_tipo_estudios").val(adicional.estu_tipo);
                $("#nombre_curso_estuios").val(adicional.estu_nombre);
                $("#codigo_adicional").val(codigo);
                $("#centro_curso_estuios").val(adicional.estu_cetro);
                $("#fecha_fin_estudioadi").val(adicional.fecha_fin);
                $("#form_adicional").attr("action", "{{ route('form.estudios.update') }}");
                //  console.log(superior);
            });
        }
    });
}

function limpiarEstudios() {

    $("#estudio_tipo_estudios").val("");
    $("#nombre_curso_estuios").val("");
    $("#centro_curso_estuios").val("");
    $("#form_compu").attr("action", "{{ route('users.estudios') }}");

}

function editarExperiencia(codigo, input) {

    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlexperiencia = base_url + "edit/experiencia/" + codigo;

    $.ajax({
        type: 'GET',
        url: urlexperiencia,
        dataType: 'json',
        success: function(data_experiencia) {
            $.each(data_experiencia, function(key, experiencia) {
                console.log(experiencia.id_ocupacion);
                $("#empresa").val(experiencia.nom_empresa);
                $("#ocupacion").val(experiencia.id_ocupacion);
                $('select[name=ocupacion]').val(experiencia.id_ocupacion);
                $('.selectpicker').selectpicker('refresh');
                $("#otro_cargo").val(experiencia.estu_cetro);
                $("#funciones_cargo").val(experiencia.funciones);
                $("#fecha_ini_empresa").val(experiencia.fech_inicio);
                $("#fecha_fin_empresa").val(experiencia.fech_fin);
                $("#codigo_experiencia").val(codigo);
                $("#form_experiencia").attr("action", "{{ route('form.experiencia.update') }}");
                //  console.log(superior);
            });
        }
    });
}

function limpiarExperiencia() {

    $("#empresa").val("");
    $("#ocupacion").val("");
    $('.selectpicker').selectpicker('refresh');
    $("#otro_cargo").val("");
    $("#funciones_cargo").val("");
    $("#fecha_ini_empresa").val("");
    $("#fecha_fin_empresa").val("");
    $("#codigo_experiencia").val("");
    $("#form_experiencia").attr("action", "{{ route('users.experiencia') }}");


}
</script>