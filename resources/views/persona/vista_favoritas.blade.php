
<div class="container pb-3">
  <div class="card mb-3" style="min-height: 650px;">
    <div class="card-body">
      <div class="row degrade-rojo " style="border-radius:0.5rem; margin-left: 1px; margin-right: 1px;" >
        <div class="col " style="padding:0.5rem;">&nbsp;
          <i class="far fa-star" style="font-size: 19px; color:#fff"></i>&nbsp;
          <b style="color:#fff">MIS FAVORITOS</b>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col">
          <div class="table-responsive-sm">
          <table class="table table-hover table-sm">
            <thead>
                <tr>
                    <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">N°</th>
                    <th class="th-top" style="width: 10%;border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">Ruc</th>
                    <th class="th-top" style="width: 30%;border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }};">Empresa</th>
                    <th class="th-top" style="width: 30%;border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Descripción</th>
                    <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Fecha Limite</th>
                    <th colspan="3" class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Acciones</th>
               
                </tr>
            </thead>
            <tbody>
                @foreach($list_favoritos as $i => $value)
                  <tr>
                      <td>{{ $i + 1 }}</td>
                      <td>{{ $value->numero_documento }}</td>
                      <td>{{ $value->razon_social }}</td>
                      <td><b>{{ $value->descripcion }}</b></td>
                      <td>{{ $value->fecha_limite  }}</td>
                      <td>
                        @if($value->pedfav_procedencia == '1')
                      <a href="{{ route('vacante.detalle',['id' => $value->codigo, 'e' => (isset($entidad) ? $entidad : '1')] ) }}"><i class="fas fa-search"></i></a>
                      @else
                      <a href="{{ route('vacante.detalle.publica',['id' => $value->codigo, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" >
                      <i class="fas fa-search"></i>
                      </a>
                      @endif
                      </td>
                  </tr>

                @endforeach
            </tbody>
          </table>
          {{ $list_favoritos->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

