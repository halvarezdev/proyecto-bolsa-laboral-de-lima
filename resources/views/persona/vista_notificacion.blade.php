
<div class="container pb-4">
            <div class="card mb-4" style="min-height: 650px;">
                <div class="card-body">
                    <div class="row degrade-rojo" style="border-radius:0.5rem; margin-left: 1px; margin-right: 1px;" >
                        <div class="col " style="padding:0.5rem;">&nbsp;
                            <i class="far fa-bell" style="font-size: 19px; color:#fff"></i>&nbsp;
                            <b style="color:#fff">MIS NOTIFICACIONES</b>
                        </div>
                    </div>
                    <br>
                    
                    <div class="row">
                        <div class="col">
                            <div class="table-responsive-sm">
                                <!--<thead>
                                    <tr>
                                        <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">N°</th>
                                        <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">Ruc</th>
                                        <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }};width:15%;">Empresa</th>
                                        <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Descripción</th>
                                        <th class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Fecha Limite</th>
                                        <th colspan="3" class="th-top" style="border-bottom: solid 1px {{ $subcolor_entidad }}; color:{{ $subcolor_entidad }}">Acciones</th>
                                   
                                    </tr>
                                </thead>
                                -->
                                <div class="card">
                                    <div class="card-body">
                                @foreach($list_notificacion as $value)

                                                @if($value->not_estado == '1')
                                                <div class="alert " role="alert">
                                                La empresa <strong>{{ $value->razon_social }}</strong> a visto su CV por la postulacion <a href="{{ route('vacante.detalle',['id' => $value->id_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="alert-link">{{ $value->descripcion }}</a>
                                                </div>
                                                @endif
                                                @if($value->not_estado == '2')
                                                <div class="alert " role="alert">
                                                Usted esta siendo considerado por la empresa: <strong>{{ $value->razon_social }}</strong> por la postulación <a href="{{ route('vacante.detalle',['id' => $value->id_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="alert-link">{{ $value->descripcion }}</a>
                                                </div>
                                                @endif
                                                @if($value->not_estado == '3')
                                                <div class="alert " role="alert">
                                                Usted fue citado por la empresa: <strong>{{ $value->razon_social }}</strong> por la postulación <a href="{{ route('vacante.detalle',['id' => $value->id_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="alert-link">{{ $value->descripcion }}</a>
                                                </div>
                                                @endif
                                                @if($value->not_estado == '4')
                                                <div class="alert alert-success" role="alert">
                                                Usted fue seleccionado por la empresa: <strong>{{ $value->razon_social }}</strong> por la postulación <a href="{{ route('vacante.detalle',['id' => $value->id_pedido, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="alert-link">{{ $value->descripcion }}</a>
                                                </div>
                                                @endif
                                            
                                   @endforeach
                                   </div>
                                </div>

                                {{ $list_notificacion->links() }}
                            </div>
                        </div>
                    </div>
 
                </div>
            </div>
</div>

