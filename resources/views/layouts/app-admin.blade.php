<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ strtoupper(Auth::guard('admin')->user()->name) }}</title>
    <link rel="shortcut icon" href="{{ asset('images/logo_menu.png') }}" />
<!-- Styles -->
@include('maestro.boot')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"> 
<!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/portal.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('css/general.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/login/main.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/usuario/main.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/entidad.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/estilo_fuentes.css') }}" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139274875-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config','UA-139274875-1');
</script>
<!-- Facebook Pixel Code -->
<!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '323115885033049');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=323115885033049&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->

</head>
<body>


 <!-- **********************USUARIO ADMIN************************-->
 @auth('admin')
 <div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
      
        <a href="#" style="color:#fff;">accede.com.pe</a>
        
        <div id="close-sidebar">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="{{ asset('/public/storage/persona/no-photo.png') }}"
            alt="User picture">
        </div>
        <div class="user-info">
          <span class="user-name">
            
            <strong>{{ strtoupper(Auth::guard('admin')->user()->name) }}</strong>
          </span>
          <span class="user-role">
          Administrador
          </span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>General</span>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-tachometer-alt"></i>
              <span>Inicio</span>
             <!-- <span class="badge badge-pill badge-warning">New</span> -->
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="{{ route('admin.dashboard') }}">Panel de Administrador
                   <!-- <span class="badge badge-pill badge-success">Pro</span> -->
                  </a>
                </li>
                <li>
                  <a href="#">Sistema</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
                <i class="fas fa-user"></i>
                <span>Personas</span>
             <!-- <span class="badge badge-pill badge-danger">3</span> -->
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Registro</a>
                </li>
                <li>
                  <a href="{{ route('persona.listado.accede') }}">Listado</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
                <i class="fas fa-building"></i>
                <span>Empresas</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Registro</a>
                </li>
                <li>
                  <a href="{{ route('empresa.listado.accede') }}">Listado</a>
                </li>
                <li>
                  <a href="#">Listado Pedido</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-chart-line"></i>
              <span>Reportes</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Reporte Personas</a>
                </li>
                <li>
                  <a href="#">Line chart</a>
                </li>
                <li>
                  <a href="#">Bar chart</a>
                </li>
                <li>
                  <a href="#">Histogram</a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
      <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>
      <a href="#">
        <i class="fa fa-cog"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>
      <a href="{{ route('logout.admin',['e' => $entidad]) }}">                                     
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </nav>
  <!-- sidebar-wrapper  --> 
 
  <main class="page-content " style="padding-top: 0px;"> 

    @yield('content')
   
  </main>
        <!-- ****************** FIN USUARIO ADMIN **********************-->
        @endguest  
    

        <script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 

<script src="{{ asset('js/persona.js') }}?v2={{date('YmdHis')}}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/entidad.js?v=12333') }}"></script>  

<script>
function createOptions(number) {
  var options = [], _options;

  for (var i = 0; i < number; i++) {
    var option = '<option value="' + i + '">Option ' + i + '</option>';
    options.push(option);
  }

  _options = options.join('');
  
  $('#number')[0].innerHTML = _options;
  $('#number-multiple')[0].innerHTML = _options;

  $('#number2')[0].innerHTML = _options;
  $('#number2-multiple')[0].innerHTML = _options;
}

var mySelect = $('#first-disabled2');

createOptions(4000);

$('#special').on('click', function () {
  mySelect.find('option:selected').prop('disabled', true);
  mySelect.selectpicker('refresh');
});

$('#special2').on('click', function () {
  mySelect.find('option:disabled').prop('disabled', false);
  mySelect.selectpicker('refresh');
});

$('#basic2').selectpicker({
  liveSearch: true,
  maxOptions: 1
});
</script>
</body>
</html>
