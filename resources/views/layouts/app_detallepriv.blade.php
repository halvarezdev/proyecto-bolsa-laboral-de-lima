<!DOCTYPE html>
<html lang="en">
<head>
<?php
$host= $_SERVER["HTTP_HOST"];
$url= $_SERVER["REQUEST_URI"];
?>
<meta charset="UTF-8"/>
<meta title="{{ ucfirst(mb_strtoupper($ped->des_empre,'UTF-8')) }}">
<title>{{ ucfirst(mb_strtoupper($ped->des_empre,'UTF-8')) }}</title>
<meta name="description" content="{{ $ped->razon_social }}">
<meta name="keywords" content="trabajos en Peru, trabajos, empleo, bolsa de trabajo, empleos, avisos de trabajo, avisos de empleo, buscar trabajo,curriculum, currículum, accede peru, accede">
<meta property="og:title" content="{{ ucfirst(mb_strtoupper($ped->des_empre,'UTF-8')) }}">
<meta property="og:site_name" content="{{ ucfirst(mb_strtoupper($ped->des_empre,'UTF-8')) }}">
<meta property="og:url" content="<?php echo "https://" . $host . $url; ?>">
<meta property="og:description" content="{{ $ped->razon_social }}">
<meta property="og:image" content="{{ asset('public/storage/'.$perfil_foto) }}">
<meta property="og:type" content="website">
<meta property="og:locale" content="es_ES">

<link rel="canonical" href="<?php echo "https://" . $host . $url; ?>">


<link rel="shortcut icon" href="{{ asset('images/logo_menu.png') }}" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
<!-- Styles -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->

<link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/portal.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('css/general.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/det-priv.css') }}" />
<script src="{{ asset('js/share.js') }}"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139274875-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config','UA-139274875-1');
</script>
<style>
  .pb-4, .py-4 {
      padding-bottom: 0px !important;
  }

</style>

 <!-- Facebook Pixel Code -->
<!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '323115885033049');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=323115885033049&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->

</head>
<body onload="return menuEsttico()">


        <div id="app" >
        <nav class="menu_solid navbar navbar-expand-md navbar-light navbar-laravel" style="background-color:{{ (isset($entidad_color) ? $entidad_color : '#003dc7' ) }};padding-left: 3%;">
            <div class="container" >
                <a class="nav-link" href="{{ route('welcome.entidad',['e' => (isset($entidad) ? $entidad : '')]) }}" >
                    <img src="{{ asset(isset($enti_logo) ? $enti_logo : '') }}" alt="photo">
                </a>  
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <div class="d-none d-sm-none d-md-block" >
        
        <!-- incio buscador  -->
        
                      <form method="get" action="{{ route('users.busqueda.inicio', ['e' => (isset($entidad) ? $entidad : '')]) }}" id="form_banner">
                      <input type="hidden" name="e" id="e" value="{{ (isset($entidad) ? $entidad : '') }}">
                      <div class="row display-none"  id="buscador">
                          <div class="col">
                          <div class="hero-body">
                        <div class="form">
                        
                          <input type="text" class="search" id="search"  name="descripcion">
                          <button onclick="enviarBusqueda()"><i class="fas fa-search"></i></button>
                          
                        </div>
                      </div>
                          </div>
                      </div>
                      </form>
                      
                      <!-- fin buscador -->

                      </div>
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item active item-basico" >
                            <a class="nav-link" href="{{ route('nosotros',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#fff;">Nosotros</a>
                          </li>
                          
                          <li class="nav-item active item-basico" >
                            <a class="nav-link" href="{{ route('consulta',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#fff;">Consulta</a>
                          </li>
                          <li class="nav-item active item-basico" >
                            <a class="nav-link" href="{{ route('contactenos',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#fff;">Contáctenos</a>
                        </li>

                        @if(isset($entidad) && $entidad != '1')
                        @guest
                      <!--  <li class="nav-item">
                          <a href="{{ route('entidad.login',['e'=>$entidad]) }}" class="btn border-circulo" style="background-color:#A4A4A4">
                             &nbsp;&nbsp;
                             <img src="{{ asset('animacion/red.png') }}">
                            &nbsp;
                            <font style="color:#fff;">Intranet</font>
                            &nbsp;&nbsp;
                            </a>
                         </li>
                         -->
                         @endguest
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

<!-- ********************* USUARIO CONTACTO ****************************** -->

                            @auth('contacto')
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#fff;">
                            {{ strtoupper(Auth::guard('contacto')->user()->name) }}
                            <span class="caret"></span>
  </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            
                            <a class="dropdown-item" href="{{ route('lista.contacto') }}">
                            {{ __('Mi Perfil') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('contacto.dashboard') }}">
                                        {{ __('Publicaciones') }}
                            </a>
                            <div class="d-block d-sm-block d-md-none">
                                <a class="dropdown-item" href="#" >
                                Buscar Cvs
                                </a>
                                <a class="dropdown-item" href="{{ route('empresa.servicios', ['e' => $entidad]) }}" >
                                Servicios
                                </a>
                                <a class="dropdown-item" href="{{ route('empresa.vacante') }}">
                                Publicar vacante
                                </a>
                            </div>
                           
                            <a class="dropdown-item" href="#"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form-contacto').submit();">
                                            {{ __('Salir') }}
                                    </a>
                                    <form id="logout-form-contacto" action="{{ route('contacto.logout') }}" method="post" style="display: none;">
                                            @csrf
                                            <input type="hidden" value="{{ $entidad }}" name="entidad">
                                    </form>
                            </div>
                            </li>
        <!-- ****************** FIN USUARIO CONTACTO **********************-->
                            @else
       

            <!-- ****************** USUARIO REGULAR **********************-->
                                        @auth
                                        <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#fff;">
                                        {{ strtoupper(Auth::user()->name) }}
                                        <span class="caret"></span>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="{{ route('home') }}">
                                                    {{ __('Perfil') }}
                                        </a>
                                        <div class="d-block d-sm-block d-md-none">
   
                                          <a class="dropdown-item" href="{{ route('postulaciones') }}" >Mis postulaciones</a>

                                          <a class="dropdown-item" href="{{ route('notificacion') }}" >Mis notificaciones</a>

                                          <a class="dropdown-item" href="{{ route('convocatoria') }}" >Vacantes sugeridas</a>

                                          <a class="dropdown-item" href="{{ route('favoritas') }}" >Mis favoritos</a>

                                          <a class="dropdown-item" href="{{ route('configuracion') }}" >Configuración</a>


                                        </div>
    
                                        <a class="dropdown-item" href="{{ route('logout.users',['e' => Auth::user()->cod_entidad]) }}">Salir</a>
                                        </div>
                                        </li>
                        <!-- ****************** USUARIO REGULAR **********************-->                
                                        @else
                        <!-- ****************** SIN USUARIO  **********************-->  
                     
                          
                              <li class="nav-item active">
                              <div class="d-none d-sm-none d-md-block">      
                                        <button class="btn btn-outline-primary nav-sub" style="width:100%; color:#fff; border:#fff solid 1px;" data-toggle="modal" data-target="#login_ultimate" onclick="popupPersonaLogin()">
                                          &nbsp;&nbsp;
                                          <i class="fas fa-user"></i>
                                          &nbsp;
                                          <font>Persona</font>
                                          &nbsp;&nbsp;
                                          </button>
                               </div>           
                              </li>
                              
                              <li class="nav-item active" style="margin-left:10px;">
                              <div class="d-none d-sm-none d-md-block">
                                          <button class="btn btn-outline-primary nav-sub" style="width:100%; color:#fff; border:#fff solid 1px;" data-toggle="modal" data-target="#login_ultimate_empresa" onclick="popuEmpresaLogin()">
                                          &nbsp;&nbsp;
                                          <i class="fas fa-industry"></i>
                                          &nbsp;
                                          <font>Empresa</font>
                                          &nbsp;&nbsp;
                                          </button>

                              </div>
                                </li>
                                <li class="nav-item active" style="margin-left:10px;">
                              <div class="d-none d-sm-none d-md-block">
                                          <button class="btn btn-primary nav-sub" style="width:100%; color:#fff; border:#00a4d3 solid 1px; background-color:{{ $subcolor_entidad }};" data-toggle="modal" data-target="#modal-banner">
                                          &nbsp;&nbsp;
                                          <i class="fas fa-briefcase"></i>
                                          &nbsp;
                                          <font>Publicar Gratis</font>
                                          &nbsp;&nbsp;
                                          </button>

                              </div>
                                </li>
                          <div class="d-block d-sm-block d-md-none">
                            <li class="nav-item active">
                              <a class="nav-link" href="{{ route('login',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#fff;">Persona</a>
                            </li>
                            <li class="nav-item active">
                              <a class="nav-link" href="{{ route('contacto.login',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#fff;">Empresa</a>
                            </li>
                          </div>
                            
                                  
                    <!-- ****************** FIN SIN USUARIO **********************-->                    
                                        @endguest 
                                    @endguest 
                           
                            

                    </ul>
                </div>
            </div>
        </nav>
      
        <main>
            @yield('content')
        </main>
    </div>

<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/js/bootstrap-select.min.js') }}"></script>
<style>

/* --- Clase que agregaremos cuando el usuario haga scroll --- */
.menu-fixed {
	position:fixed;
	z-index:1000;
	top:0;
	width:100%;
}
</style>
<script>
function menuEsttico(){
    
    var altura = $('.menu_solid').offset().top;
    var texto = "Busca por: Puesto, descripción o palabra clave.";
      var searchBar = $('#search').attr("placeholder", "");
      printLetter(texto, searchBar);
	$(window).on('scroll', function(){
    
		if ( $(window).scrollTop() > altura ){
      $('.menu_solid').addClass('menu-fixed');
      $(".item-basico").addClass("display-none");
      $("#buscador").removeClass('display-none');
      
		} else {
      $('.menu_solid').removeClass('menu-fixed');
      $(".item-basico").removeClass("display-none");
      $("#buscador").addClass('display-none');
    }
    
    

	});
}
function randDelay(min, max) {
      return Math.floor(Math.random() * (max-min+1)+min);
  }
  phCount = 0;
  function printLetter(string, el) {
   
      // split string into character seperated array
      var arr = string.split(''),
          input = el,
          // store full placeholder
          origString = string,
          // get current placeholder value
          curPlace = $(input).attr("placeholder"),
          // append next letter to current placeholder
          placeholder = curPlace + arr[phCount];
          
      setTimeout(function(){
          // print placeholder text
          $(input).attr("placeholder", placeholder);
          // increase loop count
          phCount++;
          // run loop until placeholder is fully printed
          if (phCount < arr.length) {
              printLetter(origString, input);
          }
      // use random speed to simulate
      // 'human' typing
      }, randDelay(50, 90));
  }  
  
  // function to init animation
  function placeholder() {
    var texto = "Busca por: Puesto, descripción o palabra clave.";
      var searchBar = $('#search').attr("placeholder", "");
      printLetter(texto, searchBar);
  }

</script>

<!-- ****************************** -->
 
      @include('maestro.login-modal')
     


<div  class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="login_ultimate" aria-hidden="true">
  <div  class="modal-dialog modal-lg" >
    <div style="background-color:Transparent; border:none" class="modal-content">
    @include('maestro.login')
    </div>
  </div>
</div>

<div  class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="login_ultimate_empresa" aria-hidden="true">
  <div  class="modal-dialog modal-lg" >
    <div style="background-color:Transparent; border:none" class="modal-content">
    @include('maestro.login-empresa')
    </div>
  </div>
</div>






</body>
</html>



