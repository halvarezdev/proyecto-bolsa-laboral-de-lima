<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8"/>
      <meta title="Municipalidad de Lima | Portal de ofertas laborales, empleos, trabajos | abdrl.com">
      <title>Municipalidad de Lima</title>
      <meta name="description" content="Postula a las mejores ofertas de grandes empresas en Municipalidad de Lima perú. Registrate y sube tu CV para recibir ofertas laborales a tu e-mail gratis de acuerdo a tu perfil. ¡Encuentra Trabajo ya!. Municipalidad de Lima Peru">
      <meta name="keywords" content="trabajos en Peru, trabajos, empleo, bolsa de trabajo, empleos, avisos de trabajo, avisos de empleo, buscar trabajo,curriculum, currículum, Municipalidad de Lima">
      <meta property="og:title" content="Municipalidad de Lima">
      <meta property="og:site_name" content="Municipalidad de Lima, Portal de ofertas laborales, empleos, trabajos | abdrl.com">
      <meta property="og:url" content="https://abdrl.com">
      <meta property="og:description" content="Postula a las mejores ofertas de grandes empresas en Municipalidad de Lima. Registrate y sube tu CV para recibir ofertas laborales a tu e-mail gratis de acuerdo a tu perfil. ¡Encuentra Trabajo ya!. Municipalidad de Lima">
      <meta property="og:image" content="https://abdrl.com/munipuentepiedra/entidad_logo/logo_menu.png">
      <meta property="og:type" content="website">
      <meta property="og:locale" content="es_ES">
      <link rel="canonical" href="https://abdrl.com">
      <link rel="shortcut icon" href="{{ asset('entidad_logo/favicon.ico?vr=202121') }}" />
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <!-- Styles -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/portal.css') }}" >
      <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300&display=swap" rel="stylesheet">
   <style> * {font-family: 'Oxygen', sans-serif;	}</style>
      <link rel="stylesheet" type="text/css" href="{{ asset('css/entidad.css') }}">
      @if(!isset($page))
     
      <!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->
      <link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
    
      <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
      
      <link rel="stylesheet" type="text/css" href="{{ asset('css/general.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/login/main.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/usuario/main.css') }}" />
      
      <link rel="stylesheet" type="text/css" href="{{ asset('css/estilo_fuentes.css') }}" />


      <link rel="stylesheet" type="text/css" href="{{ asset('fonts/style.css') }}"   />

     
      <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('dist/js/bootstrap-select.min.js') }}"></script>
      <script src="{{ asset('js/popper.js') }}"></script>
      
      <script src="{{ asset('js/share.js') }}"></script>
      @endif
      <script src="{{ asset('js/jquery.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
      <!--
         <link  rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
         -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139274875-1"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config','UA-139274875-1');
      </script>
      <style>
        
         .pb-4, .py-4 {
            padding-bottom: 0px !important;
         }
         .feature:nth-child(2) {
            /*background: #00A39C;*/
            /*height: 230px;*/
            /*margin-top: -12px;*/
         }
         .feature {
            text-align: center;
            background: #f8f8f8;
            height: 100%;
         }
         .feature .feature-inner {
             padding: 20px 25px;
             display: -ms-flex;
             display: -webkit-box;
             display: -ms-flexbox;
             display: block;
             -webkit-box-align: center;
             -ms-flex-align: center;
             align-items: center;
             -webkit-box-pack: center;
             -ms-flex-pack: center;
             justify-content: center;
             height: 100%;
         }
         .feature .feature-icon {
             display: block;
         }
         .feature:nth-child(2) h2 {
             /*color: #fff;*/
         }
         .feature h2 {
             font-size: 24px;
             text-transform: uppercase;
             display: inline-block;
             color: #504f4f;
         }
         .activets{
            background: #00A39C;
            height: 230px;
            margin-top: -12px;
         }
         .list{
            margin-top: 30px;
         }
         .list-item{
            padding-top: 10px;
            list-style: none;
            color: #504f4f;
            font-size: 18px;
         }

         .redes-flota{
             position: fixed;
             padding: 3px;
             top: 25%;
             left: 0;
             width: 95px;
             height: auto;
             background: #888;
             border: 2px solid #888;
             overflow: auto;
             z-index: 20001;
             border-radius: 0px 7px 7px 0px;
         }
         @media only screen
            and (min-width: 320px)
            and (max-width: 736px)
            {
            .redes-flota{
             display: none;
            }
         }
         .contact-social-flota {
             display: -ms-block;
             display: -webkit-box;
             display: -ms-flexbox;
             display: block;
             margin-bottom: 10px;
             padding-top: 5px;
             text-align: center;
         }
         .contact-social-flota a {
             display: -ms-inline-table;
             display: -webkit-inline-table;
             display: -ms-inline-table;
             display: inline-table;
             /*background: #f8f8f8ab;*/
             /*color: #888888;*/
             font-size: 26px;
             border-radius: 50%;
             -webkit-box-align: center;
             -ms-flex-align: center;
             align-items: center;
             -webkit-box-pack: center;
             -ms-flex-pack: center;
             justify-content: center;
             -webkit-transition: all 0.4s;
             -o-transition: all 0.4s;
             transition: all 0.4s;
             margin-top: 10px;
             margin-bottom: 10px;
         }
         .contact-social-flota .ayuda{
            cursor: pointer;
         }
         .contact-social-flota span{
            color: #ffffff;
         }
         #Buscador {
  background: url(https://cdn0.iconfinder.com/data/icons/slim-square-icons-basics/100/basics-19-32.png) no-repeat 0px 5px;
  background-size: 20px;
  background-position: right;
  width: 150px;
  border: transparent;
  border: solid 2px gray;
  padding: 2px 20px 2px 10px;
  outline: none;
  border-radius: 10px;
}

@media (min-width: 1200px){
   .container-menu {
      max-width: 100% !important;
      margin-right: 50px;
      margin-left: 20px;
      width: 100%;
      display: flex;
      margin-bottom: 10px;
   }
}
      </style>
      <!-- Facebook Pixel Code -->
      <!-- <script>
         !function(f,b,e,v,n,t,s)
         {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
         n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t,s)}(window, document,'script',
         'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '323115885033049');
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=323115885033049&ev=PageView&noscript=1"
         /></noscript> -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body >
      <!-- background-color:{{ (isset($entidad_color) ? $entidad_color : '#32509d' ) }}; -->
      <div class="bannerfondo" >
         <div id="app" style="padding-top: 5px;">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel" >
               <div class="container container-menu">
                  <a class="nav-link" href="{{ route('welcome.entidad',['e' => (isset($entidad) ? $entidad : '')]) }}" >
                     <img src="{{ asset('entidad_logo/logo_menu.svg') }}" alt="logo-empresa" loading="lazy" style="height: 60px;">
                  </a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                  <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                           <a class="nav-link" href="{{ route('nosotros',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#000;font-size: 19px;"><b>Nosotros</b></a>
                        </li>
                        <!--
                           <li class="nav-item active">
                           <a class="nav-link" href="{{ route('consulta',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#000;font-size: 19px;"><b>Consulta</b></a>
                        </li>
                        -->
                        <li class="nav-item active">
                           <a class="nav-link" href="{{ route('contactenos',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#000;font-size: 19px;"><b>Contacto</b></a>
                        </li>
                        @if(isset($entidad) && $entidad != '1')
                        @guest
                        <li class="nav-item">
                           <a href="{{ route('entidad.login',['e'=>$entidad]) }}" class="btn border-circulo" style="background-color:#A4A4A4">
                           &nbsp;&nbsp;
                           <img src="{{ asset('animacion/red.png') }}">
                           &nbsp;
                           <font style="color:#fff;">Intranet</font>
                           &nbsp;&nbsp;
                           </a>
                        </li>
                        @endguest
                        @endif

                        <!-- ********************* USUARIO CONTACTO ****************************** -->
                        @auth('contacto')
                        <li class="nav-item active">
                           <a class="nav-link" href="{{ route('nosotros',['e'=>isset($entidad) ? $entidad : '']) }}" style="font-size: 19px;">Nosotros</a>
                        </li>
                        <li class="nav-item dropdown">
                           <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: black;font-size: 18px;">
                              <b>{{ strtoupper(Auth::guard('contacto')->user()->name) }}</b>
                              <span class="navbar-toggler-icon"></span>
                              <!-- dropdown-toggle -->
                           </a>
                           <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
                              <a class="dropdown-item" href="{{ route('lista.contacto') }}" >
                              {{ __('Mi Perfil') }}
                              </a>
                              <a class="dropdown-item" href="{{ route('contacto.dashboard') }}">
                              {{ __('Mis Publicaciones') }}
                              </a>
                              <div class="d-block d-sm-block d-md-none">
                                 <a class="dropdown-item" href="#" >
                                 Buscar Cvs
                                 </a>
                                 <a class="dropdown-item" href="{{ route('empresa.servicios', ['e' => $entidad]) }}" >
                                 Servicios
                                 </a>
                                 <a class="dropdown-item" href="{{ route('empresa.vacante') }}">
                                 Publicar vacante
                                 </a>
                              </div>
                              <a class="dropdown-item" href="#"
                                 onclick="event.preventDefault();
                                 document.getElementById('logout-form-contacto').submit();">
                              {{ __('Salir') }}
                              </a>
                              <form id="logout-form-contacto" action="{{ route('contacto.logout') }}" method="post" style="display: none;">
                                 @csrf
                                 <input type="hidden" value="{{ $entidad }}" name="entidad">
                              </form>
                           </div>
                        </li>
                        <!-- ****************** FIN USUARIO CONTACTO **********************-->
                        @else
                        <!-- ****************** USUARIO REGULAR **********************-->
                        @auth
                        <li class="nav-item dropdown">
                           <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: black;font-size: 18px;">
                              <b>{{ strtoupper(Auth::user()->name) }}</b>
                              <span class="navbar-toggler-icon"></span>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                              <a class="dropdown-item" href="{{ route('home') }}">
                              {{ __('Perfil') }}
                              </a>
                              <div class="d-block d-sm-block d-md-none">
                                 <a class="dropdown-item" href="{{ route('postulaciones') }}" >Mis postulaciones</a>
                                 <a class="dropdown-item" href="{{ route('notificacion') }}" >Mis notificaciones</a>
                                 <a class="dropdown-item" href="{{ route('convocatoria') }}" >Vacantes sugeridas</a>
                                 <a class="dropdown-item" href="{{ route('favoritas') }}" >Mis favoritos</a>
                                 <a class="dropdown-item" href="{{ route('configuracion') }}" >Configuración</a>
                              </div>
                              <a class="dropdown-item" href="{{ route('logout.users',['e' => Auth::user()->cod_entidad]) }}">Salir</a>
                           </div>
                        </li>
                        <!-- ****************** USUARIO REGULAR **********************-->
                        @else
                        <!-- ****************** SIN USUARIO  **********************-->
                       
                        <!-- <li class="nav-item active" style="margin-left:10px;">
                           <a class="nav-link" href="javascript:void(0)" style="color:#fff;font-size: 19px;" data-toggle="modal" data-target="#login_ultimate_empresa" onclick="popuEmpresaLogin()">Soy Empresa</a>
                        </li> -->
                        <li class="nav-item active" style="margin-top: .52rem!important;">
                           <div class="d-none d-sm-none d-md-block">
                              <button 
                                 id="login-buttonemp-home"
                                 class="btn nav-sub pt-0 pb-0 pl-2"
                                 style="font-size:18px; background-color:#1A4A84; border:#1A4A84 solid 1px;color:#fff;"
                                 data-toggle="modal"
                                 data-target="#login_ultimate_empresa"
                                 onclick="popuEmpresaLogin(2)"
                              ><b>Soy Empresa</b></button>
                           </div>
                        </li>
                        <li class="nav-item active" style="margin-top: .52rem!important;margin-left:10px;">
                           <div class="d-none d-sm-none d-md-block">
                              <button
                                 id="signup-buttonemp-home"
                                 class="btn nav-sub pt-0 pb-0 pr-2"
                                 style="font-size:18px; background-color:#C73C63; border:#C73C63 solid 1px;color:#fff;"
                                 data-toggle="modal"
                                 data-target="#login_ultimate_empresa"
                                 onclick="popuEmpresaLogin(1)"
                              ><b>Soy Persona</b></button>
                           </div>
                        </li>
                        <li class="nav-item active" style="margin-top: .52rem!important;margin-left:10px;">
                           <div class="d-none d-sm-none d-md-block">
                              <input type="text" placeholder="" id="Buscador" />
                           </div>
                        </li>
                           <!-- ******************MENU DERECHA EN CELULARES **********************-->

                           <li class="nav-item active">
                           <div class="d-block d-sm-block d-md-none mb-4">
                              <button id="login-buttonemp-home" class="btn nav-sub pt-0 pb-0 pl-2" style="font-size:18px; background-color:#fff; border:#fff solid 1px;color:#1A4A84;" data-toggle="modal" data-target="#login_ultimate_empresa" onclick="popuEmpresaLogin()">Soy Empresa</button>
                           </div>
                        </li>
                        <li class="nav-item active">
                           <div class="d-block d-sm-block d-md-none mb-4">
                              <button id="signup-buttonemp-home" class="btn nav-sub pt-0 pb-0 pr-2" style="font-size:18px; background-color:#fff; border:#fff solid 1px;color:#1A4A84;" data-toggle="modal" data-target="#login_ultimate_empresa" onclick="popuEmpresaLogin()">Soy Persona</button>
                           </div>
                        </li>
                        
                        <!-- ****************** FIN SIN USUARIO **********************-->
                        @endguest
                        @endguest
                     </ul>
                  </div>
               </div>
            </nav>
         </div>
         <style>

         .hero-body{
           position: relative;
           display: flex;
           justify-content: center;
           align-items: center;
           flex-direction: column;
           height: 100%;
         }

         .form{
           display: flex;
         }

         input{
            width: 900px;
            padding: 15px;
            border-color: black;
            border-bottom: 1px solid;
            border-top: 1px solid;
            border-left: 0;
            border-right: 0;
            outline: none;
         }

         button{
           border-radius: 0px 4px 4px 0px;
           border: 0;
           padding: 14px 50px 14px 50px;
           cursor: pointer;
           color:#fff;
           background:#3850A0;

         }

         button:hover{
           color: #ffffff;
           background: #575756;

         }
         .search::placeholder{
          /* font-weight: bold;*/
           font-size:20px;
           color: #333333;
         }
         body{
             background-color:#fff;
         }
         @media only screen
and (min-device-width: 320px)
and (max-device-width: 736px) {
           .img-fluid-banner{
             height: 400px  !important;
           }
           
            
         }
         </style>

         <!-- INICIO DE SLIDER TAMAÑO RECOMENDADO 800 X 400-->
         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
               <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
               <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
               <!-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> -->
            </ol>
          
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img 
                     class="d-block w-100 img-fluid-banner"
                     src="{{ asset(isset($foto_banner1) ? $foto_banner1 : '') }}?vd=1234567890"
                     alt="First slide"
                     >
                  <div class="carousel-caption d-none d-md-block text-left text-banner" style="left: 16%;bottom: 40%;">
                     <h1 style="color: #fff;"><b>¡LAS EMPRESAS</b></h1>
                     <h1 style="color: #fff;"><b>TE ESTÁN BUSCANDO!</b></h1>
                     <h2 ><span style="color: #fff;background: #00A39C; padding: 5px;">Regístrate, postula y trabaja</span></h2>
                     <h5 style="color: #fff;">Mira las ofertas disponibles para ti:</h5>
                  </div>
               </div>
               <div class="carousel-item">
                  <img
                  class="d-block w-100 img-fluid-banner"
                  src="{{ asset(isset($foto_banner2) ? $foto_banner2 : '') }}?vd=1234567890"
                  alt="First slide">
                  <div class="carousel-caption d-none d-md-block text-left text-banner" style="left: 16%;bottom: 40%;">
                     <h1 style="color: #fff;">BOLSA DE EMPLEO</h1>
                     <h1 style="color: #fff;">MUNICIPAL</h1>
                     <h2 ><span style="color: #fff;background: #00A39C; padding: 5px;">Busca tu vacante</span></h2>
                     <h5 style="color: #fff;">Muchas empresas te están buscando</h5>
                  </div>
               </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
            </a>
         </div>

<br>
         <!-- BUSCADOR SOLO PARA CELULAR -->
         <div class="carousel-caption d-none d-md-block" style="right: 20%;left: 20%;bottom: auto;border-radius: 15px;">
            <!-- incio buscador  -->
            <form method="get" action="{{ route('users.busqueda.inicio', ['e' => (isset($entidad) ? $entidad : '')]) }}" id="form_banner">
               <input type="hidden" name="e" id="e" value="{{ (isset($entidad) ? $entidad : '') }}">
               <div class="row">
                  <div class="col">
                     <div class="hero-body">
                        <div class="form justify-content-center" style="padding: 2px;width: 95%;">
                           <span class="input-group-prepend">
                              <div 
                              style="    cursor: pointer !important;
    border-color: black !important;
    border-right: 0px !important;
    border: 1px solid black !important;border-radius: 15px 0px 0px 15px;"
                                 class="input-group-text bg-transparent border-0"
                                 style="cursor:pointer;"
                                 onclick="enviarBusqueda()"
                                 
                              >
                                 <i class="fa fa-search"></i>
                              </div>
                           </span>
                           <input type="text" class="search" id="search"  name="descripcion">
                           <button 
                              onclick="enviarBusqueda()"
                              style="width: 50%;background-color:#C73C63;border-radius: 0px 15px 15px 0px;"
                           ><b>Buscar Vacante</b></button>
                        </div>
                        <div class="input-group">
                        </div>
                     </div>
                  </div>
               </div>
            </form>
            <!-- fin buscador -->
         </div>

         <!-- BUSCADOR SOLO PARA CELULAR -->
         <br>
         <div class="d-block d-sm-block d-md-none">
            <form method="get" action="{{ route('users.busqueda.inicio', ['e' => (isset($entidad) ? $entidad : '')]) }}" id="form_banner_responsive">
               <input type="hidden" name="e" id="e" value="{{ (isset($entidad) ? $entidad : '') }}">

               <div class="container">
                  <div class="form-row align-items-center">
                     <div class="col-12" style="padding: 0px; ">
                        <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Busca un puesto, área o trabajo" name="descripcion" style="border-radius:0px;border: 1px solid #fff;">
                     </div>
                     <div class="col-12" >
                        <button type="button" class="btn btn-primary btn-lg btn-block" onclick="enviarBusquedaResponsive()" style="background-color:#00A39C;">Buscar</button>
                     </div>
                  </div>
               </div>
            </form>
         </div>

         
      </div>


      
      <!-- FIN DE CONTAINER  -->

         <main>
            @yield('content')
         </main>
      <!-- </div> -->
      <script src="{{ asset('js/general.js') }}"></script>
      @if(!isset($page))
      <script src="{{ asset('js/persona.js') }}"></script>
      
      <script src="{{ asset('js/scripts.js') }}"></script>
      <script src="{{ asset('js/entidad.js?v=12333') }}"></script>
      @endif
   
      <!-- ****************************** -->
      <div  class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="login_ultimate" aria-hidden="true">
         <div  class="modal-dialog modal-md" >
            <div style="background-color:Transparent; border:none" class="modal-content">
               @include('maestro.login')
            </div>
         </div>
      </div>
      <div  class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="login_ultimate_empresa" aria-hidden="true">
         <div  class="modal-dialog modal-lg" >
            <div style="background-color:Transparent; border:none" class="modal-content">
               @include('maestro.login-empresa')
            </div>
         </div>
      </div>

   </body>
</html>