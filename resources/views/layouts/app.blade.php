<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8"/>
      <meta title="Municipalidad de Lima| Portal de ofertas laborales, empleos, trabajos | abdrl.com">
      <title>Municipalidad de Lima</title>
      <meta name="description" content="Postula a las mejores ofertas de grandes empresas en Municipalidad de Limaperú. Registrate y sube tu CV para recibir ofertas laborales a tu e-mail gratis de acuerdo a tu perfil. ¡Encuentra Trabajo ya!. Municipalidad de LimaPeru">
      <meta name="keywords" content="trabajos en Peru, trabajos, empleo, bolsa de trabajo, empleos, avisos de trabajo, avisos de empleo, buscar trabajo,curriculum, currículum, Municipalidad de Lima">
      <meta property="og:title" content="Municipalidad de Lima">
      <meta property="og:site_name" content="Municipalidad de Lima, Portal de ofertas laborales, empleos, trabajos | abdrl.com">
      <meta property="og:url" content="https://abdrl.com">
      <meta property="og:description" content="Postula a las mejores ofertas de grandes empresas en Municipalidad de Lima. Registrate y sube tu CV para recibir ofertas laborales a tu e-mail gratis de acuerdo a tu perfil. ¡Encuentra Trabajo ya!. Municipalidad de Lima">
      <meta property="og:image" content="https://abdrl.com/munipuentepiedra/entidad_logo/logo_menu.png">
      <meta property="og:type" content="website">
      <meta property="og:locale" content="es_ES">
      <link rel="canonical" href="https://abdrl.com">
      <link rel="shortcut icon" href="{{ asset('entidad_logo/favicon.ico?vt=202121') }}" />
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <!-- Styles -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->
      <link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/portal.css') }}" >
      <link rel="stylesheet" type="text/css" href="{{ asset('css/general.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/login/main.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/usuario/main.css') }}" />
      
      <link rel="stylesheet" type="text/css" href="{{ asset('css/estilo_fuentes.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('css/entidad.css') }}">
      <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300&display=swap" rel="stylesheet">
      <style> * {font-family: 'Oxygen', sans-serif;	}</style>
<style>
          #Buscador {
  background: url(https://cdn0.iconfinder.com/data/icons/slim-square-icons-basics/100/basics-19-32.png) no-repeat 0px 5px;
  background-size: 20px;
  background-position: right;
  width: 150px;
  border: transparent;
  border: solid 2px gray;
  padding: 2px 20px 2px 10px;
  outline: none;
  border-radius: 10px;
}

@media (min-width: 1200px){
   .container-menu {
      max-width: 100% !important;
      margin-right: 50px;
      margin-left: 20px;
      width: 100%;
      display: flex;
      margin-bottom: 10px;
   }
}


      </style>
      <link rel="stylesheet" type="text/css" href="{{ asset('fonts/style.css') }}"   />

      <script src="{{ asset('js/jquery.min.js') }}"></script>
      <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('dist/js/bootstrap-select.min.js') }}"></script>
      <script src="{{ asset('js/popper.js') }}"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
      <script src="{{ asset('js/share.js') }}"></script>
      
      <!--
         <link  rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
         -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139274875-1"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config','UA-139274875-1');
      </script>
      <!-- Facebook Pixel Code -->
      <!-- <script>
         !function(f,b,e,v,n,t,s)
         {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
         n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t,s)}(window, document,'script',
         'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '323115885033049');
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=323115885033049&ev=PageView&noscript=1"
         /></noscript> -->
      <!-- End Facebook Pixel Code -->
   
   </head>
   <body >
      <div id="app" style="padding-top: 5px;">
         <nav class="navbar navbar-expand-md navbar-light navbar-laravel" >
         <div class="container container-menu" >
               <a class="nav-link" href="{{ route('welcome.entidad',['e' => (isset($entidad) ? $entidad : '')]) }}" >
               <img src="{{ asset('entidad_logo/logo_menu.svg') }}" alt="logo-empresa" loading="lazy" style="height: 60px;">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                           <a class="nav-link" href="{{ route('nosotros',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#000;font-size: 19px;"><b>Nosotros</b></a>
                        </li>
                        <!--
                           <li class="nav-item active">
                           <a class="nav-link" href="{{ route('consulta',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#000;font-size: 19px;"><b>Consulta</b></a>
                        </li>
                        -->
                        <li class="nav-item active">
                           <a class="nav-link" href="{{ route('contactenos',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#000;font-size: 19px;"><b>Contacto</b></a>
                        </li>
                        @if(isset($entidad) && $entidad != '1')
                        @guest
                        <li class="nav-item">
                           <a href="{{ route('entidad.login',['e'=>$entidad]) }}" class="btn border-circulo" style="background-color:#A4A4A4">
                           &nbsp;&nbsp;
                           <img src="{{ asset('animacion/red.png') }}">
                           &nbsp;
                           <font style="color:#575756;">Intranet</font>
                           &nbsp;&nbsp;
                           </a>
                        </li>
                        @endguest
                        @endif

                        <!-- ********************* USUARIO CONTACTO ****************************** -->
                        @auth('contacto')
                        <!--
                           <li class="nav-item active">
                           <a class="nav-link" href="{{ route('nosotros',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#575756;font-size: 19px; ">Nosotros</a>
                        </li>
                        -->
                        <li class="nav-item dropdown">
                           <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: black;font-size: 18px;">
                           <b>{{ strtoupper(Auth::guard('contacto')->user()->name) }}</b>
                           <span class="navbar-toggler-icon"></span>
                           <!-- dropdown-toggle -->
                           </a>
                           <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" >
                              <a class="dropdown-item" href="{{ route('lista.contacto') }}" >
                              {{ __('Mi Perfil') }}
                              </a>
                              <a class="dropdown-item" href="{{ route('contacto.dashboard') }}">
                              {{ __('Mis Publicaciones') }}
                              </a>
                              <div class="d-block d-sm-block d-md-none">
                                 <a class="dropdown-item" href="#" >
                                 Buscar Cvs
                                 </a>
                                 <a class="dropdown-item" href="{{ route('empresa.servicios', ['e' => $entidad]) }}" >
                                 Servicios
                                 </a>
                                 <a class="dropdown-item" href="{{ route('empresa.vacante') }}">
                                 Publicar vacante
                                 </a>
                              </div>
                              <a class="dropdown-item" href="#"
                                 onclick="event.preventDefault();
                                 document.getElementById('logout-form-contacto').submit();">
                              {{ __('Salir') }}
                              </a>
                              <form id="logout-form-contacto" action="{{ route('contacto.logout') }}" method="post" style="display: none;">
                                 @csrf
                                 <input type="hidden" value="{{ $entidad }}" name="entidad">
                              </form>
                           </div>
                        </li>
                        <!-- ****************** FIN USUARIO CONTACTO **********************-->
                        @else
                        <!-- ****************** USUARIO REGULAR **********************-->
                        @auth
                        <li class="nav-item dropdown">
                           <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: black;font-size: 18px;">
                           <b>{{ strtoupper(Auth::user()->name) }}</b>
                           <span class="navbar-toggler-icon"></span>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                              <a class="dropdown-item" href="{{ route('home') }}">
                              {{ __('Perfil') }}
                              </a>
                              <div class="d-block d-sm-block d-md-none">
                                 <a class="dropdown-item" href="{{ route('postulaciones') }}" >Mis postulaciones</a>
                                 <a class="dropdown-item" href="{{ route('notificacion') }}" >Mis notificaciones</a>
                                 <a class="dropdown-item" href="{{ route('convocatoria') }}" >Vacantes sugeridas</a>
                                 <a class="dropdown-item" href="{{ route('favoritas') }}" >Mis favoritos</a>
                                 <a class="dropdown-item" href="{{ route('configuracion') }}" >Configuración</a>
                              </div>
                              <a class="dropdown-item" href="{{ route('logout.users',['e' => Auth::user()->cod_entidad]) }}">Salir</a>
                           </div>
                        </li>
                        <!-- ****************** USUARIO REGULAR **********************-->
                        @else
                        <!-- ****************** SIN USUARIO  **********************-->
                       
                        <li class="nav-item active" style="margin-top: .52rem!important;">
                           <div class="d-none d-sm-none d-md-block">
                              <button 
                                 id="login-buttonemp-home"
                                 class="btn nav-sub pt-0 pb-0 pl-2"
                                 style="font-size:18px; background-color:#1A4A84; border:#1A4A84 solid 1px;color:#fff;"
                                 data-toggle="modal"
                                 data-target="#login_ultimate_empresa"
                                 onclick="popuEmpresaLogin()"
                              ><b>Soy Empresa</b></button>
                           </div>
                        </li>
                        <li class="nav-item active" style="margin-top: .52rem!important;margin-left:10px;">
                           <div class="d-none d-sm-none d-md-block">
                              <button
                                 id="signup-buttonemp-home"
                                 class="btn nav-sub pt-0 pb-0 pr-2"
                                 style="font-size:18px; background-color:#C73C63; border:#C73C63 solid 1px;color:#fff;"
                                 data-toggle="modal"
                                 data-target="#login_ultimate_empresa"
                                 onclick="popuEmpresaLogin()"
                              ><b>Soy Persona</b></button>
                           </div>
                        </li>
                        <li class="nav-item active" style="margin-top: .52rem!important;margin-left:10px;">
                           <div class="d-none d-sm-none d-md-block">
                              <input type="text" placeholder="" id="Buscador" />
                           </div>
                        </li>
                        <!--   <li class="nav-item active" style="margin-left:10px;">
                           <div class="d-none d-sm-none d-md-block">
                                       <button class="btn btn-primary nav-sub" id="boton_defecto" style="width:100%; color:#575756; border:#7ec356 solid 1px; background-color:#7ec356;" data-toggle="modal" data-target="#modal-banner">
                                       &nbsp;&nbsp;
                                       <i class="fas fa-briefcase"></i>
                                       &nbsp;
                                       <font>Publicar Gratis</font>
                                       &nbsp;&nbsp;
                                       </button>
                           
                           </div>
                             </li>
                             -->


                           <!-- ******************MENU DERECHA EN CELULARES **********************-->

                        <li class="nav-item active">
                           <div class="d-block d-sm-block d-md-none mb-4">
                              <button id="login-buttonemp-home" class="btn nav-sub pt-0 pb-0 pl-2" style="font-size:18px; background-color:#fff; border:#fff solid 1px;color:#1A4A84;" data-toggle="modal" data-target="#login_ultimate_empresa" onclick="popuEmpresaLogin()">Soy Empresa</button>
                           </div>
                        </li>
                        <li class="nav-item active">
                           <div class="d-block d-sm-block d-md-none mb-4">
                              <button id="signup-buttonemp-home" class="btn nav-sub pt-0 pb-0 pr-2" style="font-size:18px; background-color:#fff; border:#fff solid 1px;color:#1A4A84;" data-toggle="modal" data-target="#login_ultimate_empresa" onclick="popuEmpresaLogin()">Soy Persona</button>
                           </div>
                        </li>

                        <!--<div class="d-block d-sm-block d-md-none">
                           <li class="nav-item active">
                              <a class="nav-link" href="{{ route('login',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#575756;">Persona</a>
                           </li>
                           <li class="nav-item active">
                              <a class="nav-link" href="{{ route('contacto.login',['e'=>isset($entidad) ? $entidad : '']) }}" style="color:#575756;">Empresa</a>
                           </li>

                        </div>-->

                        
                        <!-- ****************** FIN SIN USUARIO **********************-->
                        @endguest
                        @endguest
                  </ul>
               </div>
            </div>
         </nav>
         <main >
            @yield('content')
         </main>
      </div>
      
      <script src="{{ asset('js/persona.js') }}"></script>
      <script src="{{ asset('js/general.js') }}"></script>
      <script src="{{ asset('js/scripts.js') }}"></script>
      <script src="{{ asset('js/entidad.js?v=12333') }}"></script>
      <!-- ****************************** -->
      <div  class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="login_ultimate" aria-hidden="true">
         <div  class="modal-dialog modal-md" >
            <div style="background-color:Transparent; border:none" class="modal-content">
               @include('maestro.login')
            </div>
         </div>
      </div>
      <div  class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="login_ultimate_empresa" aria-hidden="true">
         <div  class="modal-dialog modal-lg" >
            <div style="background-color:Transparent; border:none" class="modal-content">
               @include('maestro.login-empresa')
            </div>
         </div>
      </div>
   </body>
</html>