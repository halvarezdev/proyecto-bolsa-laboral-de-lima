<?php
$v2=date('YmdHis');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8"/>

<link rel="shortcut icon" href="{{ asset('images/logo_menu.png') }}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Intranet</title>
<!-- Styles -->
<link rel="stylesheet" type="text/css"  href="{{ asset('css/all.css') }}" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"> 
<!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->
<link rel="stylesheet" type="text/css" href="{{ asset('fonts/style.css') }}"   />
<link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/entidad.css') }}?ng={{$v2}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/component/sweetalert/sweetalert.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/citas/css/style.css')}}?ng={{$v2}}">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135004982-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135004982-1');
</script>
<!-- Facebook Pixel Code -->
<!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '323115885033049');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=323115885033049&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->

</head>
<body  style="background-repeat: no-repeat;
   background-position: 100% 100%;background-attachment: fixed;">
   <!-- background-image:url({{ asset('fondo/fondo-persona.png') }}) -->
<input type="hidden" value="{{env('APP_URL')}}" id="base_url">
<input type="hidden" id="_token" value="{{ csrf_token() }}">
<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#" style="color:#fff;">MUNICIPALIDAD DE <br>LIMA</a>
        <div id="close-sidebar">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <!-- <div class="user-pic">
          <img class="img-responsive img-rounded" src="{{ asset('/public/storage/persona/JC.jpg') }}"
            alt="User picture">
        </div> -->
        <div class="user-info">
          <span class="user-name">
            
            <strong>{{ strtoupper(Auth::user()->name) }}</strong>
          </span>
          <span class="user-role">
        @if(Auth::user()->rol_id)
        Administrador
        @endif
          </span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>
      <!-- sidebar-search  -->
      @if(Auth::user()->idperfil==3)
        @include('layouts.nav-entidad-formador')
      @else
      @include('layouts.nav-entidad-admin')
      @endif
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
      <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>
      <a href="#">
        <i class="fa fa-cog"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="{{ route('entidad.logout',['e' => $entidad]) }}">                                       
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </nav>
  <!-- sidebar-wrapper  --> 
 
  <main class="page-content " style="padding-top: 0px;"> 
  <style>
    body{
    
    background-size: cover;
   /* opacity:0.5; */
    -moz-background-size: cover;
    -webkit-background-size: cover;
    -o-background-size: cover;
        
  }
.degradado-rojo{
    
background: #b00002;
background: -moz-linear-gradient(left, #b00002 0%, #b00002 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, #b00002), color-stop(100%, #b00002));
background: -webkit-linear-gradient(left, #b00002 0%, #b00002 100%);
background: -o-linear-gradient(left, #b00002 0%, #b00002 100%);
background: -ms-linear-gradient(left, #b00002 0%, #b00002 100%);
background: linear-gradient(to right, #b00002 0%, #b00002 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b00002', endColorstr='#b00002', GradientType=1 );

}
.msj-1{
    background-color:#fff; 
    margin-left:25%;
    margin-right:25%;
    background-color:rgba(255,255,255,0.5);
    padding-top: 3%;
    padding-bottom: 3%;
}
.letra-msj{
    text-align:center; 
}
</style>
<div class="container " style="padding: 0px 40px;">

        <div class="row" >
            <div class="col degradado-rojo" style="border-top-left-radius: 0rem;border-top-right-radius: 0rem;border-bottom-right-radius: 2rem;border-bottom-left-radius: 2rem;"> 
            <h3 style="color:#fff; text-align:center;">
            PLATAFORMA DE SERVICIOS AL CIUDADANO
            </h3>
            </div>
        </div>
    <div class="row">
    <div class="col-md-12 mt-2 page_contenedor">
    @yield('content')
    </div>
    </div>
    <!-- <div class="row footer" >
        <div class="col-md-12 mt-2 ">
            <div class="col degradado-rojo" style="width: 100%; border-top-left-radius: 2rem;border-top-right-radius: 2rem;border-bottom-right-radius: 0rem;border-bottom-left-radius: 0rem;"> 
            
            <h3 style="color:red; text-align:center;">
            &nbsp;&nbsp;&nbsp;&nbsp;
            </h3>
            </div>
            </div>
        </div> -->
       
</div>

<style>

.footer {
	width: 70%;
	position: absolute;
	bottom: 0;
}

</style>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->  
<div class="animationload" style="display: none;">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/persona.js') }}"></script>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('dist/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/component/sweetalert/sweetalert.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/component/tableHeadFixer.js') }}"></script>
<script src="{{ asset('js/entidad.js') }}?ng={{$v2}}"></script>
<script src="{{ asset('assets/js/default.js') }}?ng={{$v2}}"></script>
<script>
  
$(document).ready(function(){
  $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
   };
   $.datepicker.setDefaults($.datepicker.regional['es']);
  });
function  addContenidoPrincipal(prog_panel,url,urljs,panel,prog_id) { 
    var base_url=get('base_url').value;
    var _token=get('_token').value;
    unloadJSCSS(urljs)   
    $.ajax({
          type:'post',
          url:base_url+ url,
          dataType:'HTML',
          data:{_token:_token,empresa:0,usert:0},
          beforeSend: function(){
           $(".animationload").css({display:'block'});
          },
    }).done(function( data, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'});     
        $('.'+panel).html(data);
        loadJSCSS(base_url,urljs,'')
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        
    });
}
</script>
</body>
</html>
