<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Accede</title>
    @include('maestro.bootstrap')
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135004982-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135004982-1');
</script>
<!-- Facebook Pixel Code -->
<!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '323115885033049');
  fbq('track', 'PageView');
</script> -->
<!-- <noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=323115885033049&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->

</head>
<body class="fondo_body">

<main class="py-4">
            @yield('content')
</main>

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<!-- 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

    <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
 
  <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script defer="" src="js/fontawesome-all.js"></script>
    <script type="text/javascript" src="js/persona.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
  function limiartodo(id){
    document.getElementById("rubro-selectized") = "";
    
  }
  </script>

</body>
</html>