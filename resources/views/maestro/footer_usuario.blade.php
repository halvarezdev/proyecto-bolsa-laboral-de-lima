<style type="text/css">
 
   .ColorText a{     font-family:Galano Grotesque Medium;
    color: #ffffff;
    font-size: 14px;}
   .ColorText .fas{color: #3654a5;font-size: 20px}.ColorText .far{color: #3654a5;font-size: 20px}
   .ColorText h3{    color: #ffffff;
   font-size: 1.2rem;font-family: Galano Grotesque Bold;}
</style>
<style type="text/css">
   .contact-social {
       display: -ms-flex;
       display: -webkit-box;
       display: -ms-flexbox;
       display: flex;
       margin-bottom: 15px;
       padding-top: 5px;
   }
   .contact-social a {
       display: -ms-inline-flex;
       display: -webkit-inline-box;
       display: -ms-inline-flexbox;
       display: inline-flex;
       width: 42px;
       height: 42px;
       background: #f0f0f0;
       color: #414141;
       font-size: 20px;
       border-radius: 50%;
       -webkit-box-align: center;
       -ms-flex-align: center;
       align-items: center;
       -webkit-box-pack: center;
       -ms-flex-pack: center;
       justify-content: center;
       margin-right: 12px;
       -webkit-transition: all 0.4s;
       -o-transition: all 0.4s;
       transition: all 0.4s;
   }
   
</style>
<!-- <br> -->
  
<div footer class="avisosfondo" style="background-image:  url('{{ asset('fondo/footer.png') }}')">
   <div class="container">
      <div class="row">
         <div class="col-md-4">
            
         </div>
         <div class="col-md-12">
            <br>
            <br>
            <center>
            <img src="{{ asset('images/telefono.png') }}" alt="" style="width: 100%;">
            </center>
            <br>
            <br>
         </div>
         <div class="col-md-12" style="text-align:center; color:#fff">
            <h1>Contáctate con nosotros</h1>
            <h2>(01) 632-3127 </h2>
            <p> <b>bolsalaboral@munlima.gob.pe</b></p>
            <p>Av. Garcilazo de la Vega 1348, cuarto piso, sector B Cercado de Lima</p>
            <p><b>BolsaLaboraldeLima</b></p>
         </div>
         <div class="col-lg-12 col-sm-8 mt-5 mb-5">
            <section class="ColorText">
               <div class="row">
                  <div class="col-md-12">
                     <div class="hidden-mobile">
                        <a target="_blank" href="https://www.facebook.com/MuniLima" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                           <img src="{{ asset('animacion/facebook.png') }}" alt="" width="30">
                           @MuniLima
                        </a>
                        <a target="_blank" href="https://twitter.com/MuniLima?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                           <img src="{{ asset('animacion/twiter.png') }}" alt="" width="30">
                           @MuniLima
                        </a>
                        <a target="_blank" href="https://www.instagram.com/munlima/?hl=es" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                           <img src="{{ asset('animacion/insta.png') }}" alt="" width="30">
                           @MuniLima
                        </a>
                        <a target="_blank" href="https://www.youtube.com/channel/UCJ3f5r4cWt0aB_wRZPSejWQ" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                           <img src="{{ asset('animacion/youtube.png') }}" alt="" width="30">
                           Municipalidad de Lima
                        </a>
                        <a target="_blank" href="https://www.tiktok.com/@tiktokmunlima?lang=es" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                           <img src="{{ asset('animacion/tiktok-icon.png') }}" alt="" width="30">
                           @tiktokmunlima
                        </a>
                     </div>
                     <div social_media>
                           <div class="col-12 col-md-8"> 
                              <a target="_blank" href="https://www.youtube.com/channel/UCJ3f5r4cWt0aB_wRZPSejWQ" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                                 <img src="{{ asset('animacion/youtube.png') }}" alt="" width="30">
                                 Municipalidad de Lima
                              </a>
                           </div>
                           <div class="col-12 col-md-8">
                              <a target="_blank" href="https://www.tiktok.com/@tiktokmunlima?lang=es" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                                 <img src="{{ asset('animacion/tiktok-icon.png') }}" alt="" width="30">
                                 @tiktokmunlima
                              </a>
                           </div>
                           <div class="col-12 col-md-8">  
                                 <a target="_blank" href="https://twitter.com/MuniLima?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                                    <img src="{{ asset('animacion/twiter.png') }}" alt="" width="30">
                                    @MuniLima
                                 </a>
                           </div>
                           <div class="col-12 col-md-8">  
                              <a target="_blank" href="https://www.instagram.com/munlima/?hl=es" style="text-decoration: none;margin-left: 10px;margin-right: 10px;">
                                 <img src="{{ asset('animacion/insta.png') }}" alt="" width="30">
                                 @MuniLima
                              </a>
                           </div>   
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>