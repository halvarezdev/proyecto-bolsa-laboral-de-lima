<style>
.footer{
    background-color:#000000;
}
</style>
<div class="container" style="background-color:#A4A4A4">
    <footer >
        <div class="row">
            <div class="col" >
               <center>
                    <font style="color:#E6E6E6;">Siguenos en nuestras Redes Sociales:</font>
                    <br>
                    <a href="#" class="redes_sociales" style="color:#E6E6E6;"><i class="fab fa-facebook-square fa-2x"></i></a>
                    <a href="#" class="redes_sociales" style="color:#E6E6E6;"><i class="fab fa-linkedin fa-2x"></i></a>
                    <a href="#" class="redes_sociales" style="color:#E6E6E6;"><i class="fab fa-twitter-square fa-2x"></i></a>
                    <a href="#" class="redes_sociales" style="color:#E6E6E6;"><i class="fab fa-google-plus-square fa-2x"></i></i></a>
               </center>
            </div>        
        </div>  
        <hr style="color:#fff;">
        <div class="row">
            <div class="col">
                <div class="footer_lista">
                    <ul>
                        <li class="menu_lista"><b>ENLACES EXTERNOS</b></li>
                        <br>
                        <li class="menu_lista">Portal del estado Peruano</li>
                        <li class="menu_lista">Impulsa Perú</li>
                        <li class="menu_lista">Plan Nacional de Calidad Turística del Perú</li>
                        <li class="menu_lista">Fondo Empleo</li>
                        <li class="menu_lista">Registro Nacional de Sanciones de Destitución y Despido</li>
                    </ul>
                </div>
            </div>
            <div class="col">
                <div class="footer_lista">
                    <ul>
                        <li class="menu_lista"><b>ENLACES INTERNO</b></li>
                        <br>
                        <li class="menu_lista">Acceso a la información pública</li>
                        <li class="menu_lista">Plazas y Vacantes</li>
                        <li class="menu_lista">Convocatoria para Prácticas</li>
                        <li class="menu_lista">Se parte de CENFOTUR</li>
                        <li class="menu_lista">Ubícanos</li>
                        <li class="menu_lista">Denuncia la corrupción</li>
                    </ul>
                </div>
            </div>
            <div class="col" style="backgrount-color:#fff;">
                <div class="footer_lista">
                    <!--<ul>
                        <li class="menu_lista"><b>Reclutadores</b></li>
                        <br>
                        <li class="menu_lista">Preguntas frecuentes de empresas</li>
                        <li class="menu_lista">Contacto para empresas</li>
                        <li class="menu_lista">Buscar candidatos</li>
                    </ul>
                    -->
                </div>
            </div>

        </div>
    </footer>
</div>
<footer class="footer" >
        <div class="container ">
                <div class="row">
                        <div class="col">
                                <div class="footer_lista">
                                    <center>
                                        <p style="font-size:11px;color:#BDBDBD;">Jr. Pedro Martino N° 320 Barranco - Lima Perú (Ver Mapa). Central Telefónica. (511) 319-800 <br>
                                        Funcionario Responsable de brindar la información requerida por los ciudadanos (Ley 27806)<br>
                                        Sonia Bethsabe Picovsky Novoa Jefe(a) de la ofincina de administración y finanzas <br>
                                        CENFOTUR. Todos los derechos reservados</p>
                                    </center>
                                </div>
                        </div>
                </div>
        </div>
        <br>
</footer>
