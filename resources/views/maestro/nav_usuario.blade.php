

<nav class="navbar navbar-expand-md navbar-dark">

  <a class="navbar-brand " href="#"><img src="{{ asset('images/icon-logo.jpg') }}" class="img-usuario"></a>
  

  <div class="collapse navbar-collapse " id="navbarsExample04">
 
    <ul class="navbar-nav justify-content-end">
        <li class="nav-item" style="padding: 10px;">
          <a class="nav-link icon" href="#">
            <i class="fas fa-user fa-2x"></i>
            </a>
        </li>
      <li class="nav-item" style="padding: 10px;">
        <a class="nav-link icon" href="#">
            <i class="far fa-images fa-2x"></i>
        </a>
      </li>
      <li class="nav-item" style="padding: 10px;">
        <a class="nav-link icon" href="{{ route('users.file') }}">
        <i class="fas fa-file-invoice fa-2x"></i>
        </a>
      </li>
      <li class="nav-item" style="padding: 10px;">
        <a class="nav-link icon" href="#">
            <i class="fas fa-redo-alt fa-2x"></i>
        </a>
      </li>
      <li class="nav-item" style="padding: 10px;">
        <a class="nav-link icon" href="{{ route('user.buscar') }}">
            <i class="fas fa-search fa-2x"></i>
        </a>
      </li>
    </ul>
    
  </div>
</nav>