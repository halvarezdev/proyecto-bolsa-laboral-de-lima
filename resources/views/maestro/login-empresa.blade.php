<style>
   /**
   * Bounce to the left side
   */
   @-webkit-keyframes bounceLeftemp {
   0% {
   -webkit-transform: translate3d(100%, -50%, 0);
   transform: translate3d(100%, -50%, 0);
   }
   50% {
   -webkit-transform: translate3d(-30px, -50%, 0);
   transform: translate3d(-30px, -50%, 0);
   }
   100% {
   -webkit-transform: translate3d(0, -50%, 0);
   transform: translate3d(0, -50%, 0);
   }
   }
   @keyframes bounceLeftemp {
   0% {
   -webkit-transform: translate3d(100%, -50%, 0);
   transform: translate3d(100%, -50%, 0);
   }
   50% {
   -webkit-transform: translate3d(-30px, -50%, 0);
   transform: translate3d(-30px, -50%, 0);
   }
   100% {
   -webkit-transform: translate3d(0, -50%, 0);
   transform: translate3d(0, -50%, 0);
   }
   }
   /**
   * Bounce to the left side
   */
   @-webkit-keyframes bounceRightemp {
   0% {
   -webkit-transform: translate3d(0, -50%, 0);
   transform: translate3d(0, -50%, 0);
   }
   50% {
   -webkit-transform: translate3d(calc(100% + 30px), -50%, 0);
   transform: translate3d(calc(100% + 30px), -50%, 0);
   }
   100% {
   -webkit-transform: translate3d(100%, -50%, 0);
   transform: translate3d(100%, -50%, 0);
   }
   }
   @keyframes bounceRightemp {
   0% {
   -webkit-transform: translate3d(0, -50%, 0);
   transform: translate3d(0, -50%, 0);
   }
   50% {
   -webkit-transform: translate3d(calc(100% + 30px), -50%, 0);
   transform: translate3d(calc(100% + 30px), -50%, 0);
   }
   100% {
   -webkit-transform: translate3d(100%, -50%, 0);
   transform: translate3d(100%, -50%, 0);
   }
   }
   /**
   * Show Sign Up form
   */
   @-webkit-keyframes showSignUpemp {
   100% {
   opacity: 1;
   visibility: visible;
   -webkit-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   }
   }
   @keyframes showSignUpemp {
   100% {
   opacity: 1;
   visibility: visible;
   -webkit-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   }
   }
   /**
   * Page background
   */
   .useremp {
   margin-top: 5%;
   justify-content: center;
   align-items: center;
   width: 100%;
   height: 100vh;
   }
   .useremp_options-container {
   position: relative;
   width: 100%;
   }
   .useremp_options-text {
   display: flex;
   justify-content: space-between;
   width: 100%;
   background-color: #D8D8D8;
   border-radius: 3px;
   }
   /**
   * Registered and Unregistered useremp box and text
   */
   .useremp_options-registered,
   .useremp_options-unregistered {
   width: 50%;
   padding: 60px 45px;
   color: #fff;
   font-weight: 300;
   }
   /*
   .useremp_registered-title,
   .useremp_unregistered-title {
   margin-bottom: 15px;
   font-size: 1.66rem;
   line-height: 1em;
   }
   .useremp_unregistered-text,
   .useremp_registered-text {
   font-size: 0.83rem;
   line-height: 1.4em;
   }
   .useremp_registered-login,
   .useremp_unregistered-signup {
   margin-top: 30px;
   border: 1px solid #ccc;
   border-radius: 3px;
   padding: 10px 30px;
   color: #fff;
   text-transform: uppercase;
   line-height: 1em;
   letter-spacing: 0.2rem;
   transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
   }
   .useremp_registered-login:hover,
   .useremp_unregistered-signup:hover {
   color: rgba(34, 34, 34, 0.85);
   background-color: #ccc;
   }
   */
   /**
   * Login and signup forms
   */
   .useremp_options-forms {
   position: absolute;
   top: 50%;
   left: 0px;
   width: calc(50% - 0px);
   min-height: 550px;
   background-color: #00A39C;
   border-radius: 3px;
   box-shadow: 2px 0 15px rgba(0, 0, 0, 0.25);
   overflow: hidden;
   -webkit-transform: translate3d(100%, -50%, 0);
   transform: translate3d(100%, -50%, 0);
   transition: -webkit-transform 0.4s ease-in-out;
   transition: transform 0.4s ease-in-out;
   transition: transform 0.4s ease-in-out, -webkit-transform 0.4s ease-in-out;
   }
   .useremp_options-forms .useremp_forms-login {
   transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out;
   }
   .useremp_options-forms .forms_title {
   margin-bottom: 45px;
   font-size: 1.5rem;
   font-weight: 500;
   line-height: 1em;
   text-transform: uppercase;
   color: #e8716d;
   letter-spacing: 0.1rem;
   }
   .useremp_options-forms .forms_field:not(:last-of-type) {
   margin-bottom: 20px;
   }
   .useremp_options-forms .forms_field-input {
   width: 100%;
   border-bottom: 1px solid #ccc;
   padding: 6px 20px 6px 6px;
   font-family: "Montserrat", sans-serif;
   font-size: 1rem;
   font-weight: 300;
   color: gray;
   letter-spacing: 0.1rem;
   transition: border-color 0.2s ease-in-out;
   }
   .useremp_options-forms .forms_field-input:focus {
   border-color: gray;
   }
   .useremp_options-forms .forms_buttons {
   display: flex;
   justify-content: space-between;
   align-items: center;
   margin-top: 35px;
   }
   .useremp_options-forms .forms_buttons-forgot {
   font-family: "Montserrat", sans-serif;
   letter-spacing: 0.1rem;
   color: #ccc;
   text-decoration: underline;
   transition: color 0.2s ease-in-out;
   }
   .useremp_options-forms .forms_buttons-forgot:hover {
   color: #b3b3b3;
   }
   .useremp_options-forms .forms_buttons-action {
   background-color: #e8716d;
   border-radius: 3px;
   padding: 10px 35px;
   font-size: 1rem;
   font-family: "Montserrat", sans-serif;
   font-weight: 300;
   color: #fff;
   text-transform: uppercase;
   letter-spacing: 0.1rem;
   transition: background-color 0.2s ease-in-out;
   }
   .useremp_options-forms .forms_buttons-action:hover {
   background-color: #e14641;
   }
   .useremp_options-forms .useremp_forms-signup,
   .useremp_options-forms .useremp_forms-login {
   position: absolute;
   top: 70px;
   left: 40px;
   width: calc(100% - 80px);
   opacity: 0;
   visibility: hidden;
   transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out, -webkit-transform 0.5s ease-in-out;
   transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out, transform 0.5s ease-in-out;
   transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out, transform 0.5s ease-in-out, -webkit-transform 0.5s ease-in-out;
   }
   .useremp_options-forms .useremp_forms-signup {
   -webkit-transform: translate3d(120px, 0, 0);
   transform: translate3d(120px, 0, 0);
   }
   .useremp_options-forms .useremp_forms-signup .forms_buttons {
   justify-content: flex-end;
   }
   .useremp_options-forms .useremp_forms-login {
   -webkit-transform: translate3d(0, 0, 0);
   transform: translate3d(0, 0, 0);
   opacity: 1;
   visibility: visible;
   }
   /**
   * Triggers
   */
   .useremp_options-forms.bounceLeftemp {
   -webkit-animation: bounceLeftemp 1s forwards;
   animation: bounceLeftemp 1s forwards;
   }
   .useremp_options-forms.bounceLeftemp .useremp_forms-signup {
   -webkit-animation: showSignUpemp 1s forwards;
   animation: showSignUpemp 1s forwards;
   }
   .useremp_options-forms.bounceLeftemp .useremp_forms-login {
   opacity: 0;
   visibility: hidden;
   -webkit-transform: translate3d(-120px, 0, 0);
   transform: translate3d(-120px, 0, 0);
   }
   .useremp_options-forms.bounceRightemp {
   -webkit-animation: bounceRightemp 1s forwards;
   animation: bounceRightemp 1s forwards;
   }

   .login_emp_pers p{ font-family: Galano-Grotesque Light;font-size:17px;color:#fff;}
   .login_emp_pers span{ font-family: Galano Grotesque Bold;font-size:24px;color:#fff; }
   .button_ingre .btn_portal{font-family: Galano Grotesque Bold;font-size: 18px;color:#fff; background:#b00002}
   .button_ingre .input{font-family: Galano-Grotesque Light;}
   .login_emp_pers button{font-family: Galano Grotesque Medium;}
   .button_ingre .regist{font-family: Galano-Grotesque Light;}
   /**
   * Responsive 990px
   */
   @media screen and (max-width: 990px) {
   .useremp_options-forms {
   min-height: 600px;
   }
   .login_emp_pers p{ font-size: 10px;}
   .login_emp_pers span{font-size:15px;}
   .useremp_options-forms .forms_buttons {
   flex-direction: column;
   }
   .useremp_options-forms .useremp_forms-login .forms_buttons-action {
   margin-top: 30px;
   }
   .useremp_options-forms .useremp_forms-signup,
   .useremp_options-forms .useremp_forms-login {
   top: 40px;
   }
   .useremp_options-registered,
   .useremp_options-unregistered {
   padding: 50px 45px;
   }
   }
   @media only screen
            and (min-width: 320px)
            and (max-width: 736px)
            {
            #useremp_options-forms{
               display:none;
            }
            .person_movil{
               display:none;
               
            }
            .person_movil_v2{
               width:100% !important;
            }
            .img-fluid {
               height: 100px !important;
            }
         }
</style>
<section class="useremp">
   <div class="useremp_options-container">
      <div class="useremp_options-text button_ingre">
         <!--Registro Empresa-->
         <div class="useremp_options-unregistered">
            <div class="row justify-content-center">
               <div class="col-xl-12 col-md-12  col-sm-12">
                  <form method="POST" action="{{ route('contacto.login.submit') }}" id="form_empresa_emp">
                     @csrf
                     <input type="hidden" id="base_url" value="{{ asset('') }}">
                     <div class="form-group row justify-content-center">
                        <div class="col-xl-6 col-md-6 col-sm-12">
                           <center>
                           <img src="{{ asset('entidad_logo/icon-empresaregister.png?ve=2021021') }}" alt="logo-empresa" class="img-fluid">
                           </center>
                        </div>
                     </div>
                     <br>
                     <div class="form-group row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                           <input id="email_passemp" onblur="validarExisteCorreoEmpresaemp(this)" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }} form-control-nuevo" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo">
                           <span class="invalid-feedback" role="alert">
                           <strong id="msj_emp_erroremp"></strong>
                           </span>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                           <input onkeyup = "if(event.keyCode == 13) loginEmpresaemp()" id="password_empresaemp" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} form-control-nuevo" name="password" required placeholder="Contraseña">
                           <span class="invalid-feedback" role="alert">
                           <strong id="msj_error_pass_empresaemp"></strong>
                           </span>
                        </div>
                     </div>
                     <div class="form-group row mb-0 justify-content-center">
                        <div class="col-xl-6 col-md-6 col-sm-12">
                           <a class="btn btn_portal"
                              onclick="loginEmpresaemp()" 
                              style="background: #1A4A84;"
                              >
                           Ingresar
                           </a>
                        </div>
                     </div><br>
                     <div class="form-group row justify-content-center">
                         
                     </div>
                     <br>
                     <div class="form-group row justify-content-center">
                        <div class="col-xl-12 col-md-12 col-sm-12 regist  text-center">
                           <font style="color:black;">¿No tienes una cuenta?</font>  <a href="{{ route('contacto.register',['e' => (isset($entidad) ? $entidad : '1' )]) }}"><b style="font-weight: bold;">Regístrate</b></a>
                        </div>
                     </div>
                  </form>
                  

               </div>
            </div>
         </div>
         <!--Registro Persona-->
         <div class="useremp_options-registered">
            <div class="row justify-content-center">
               <div class="col-xl-12 col-md-12 col-sm-12">
                  <form method="POST" action="{{ route('login') }}" id="form_useremps">
                     @csrf
                     {!! RecaptchaV3::field('user') !!}
                     <div class="form-group row justify-content-center">
                        <div class="col-xl-6 col-md-6 col-sm-12">
                           <center>
                           <img src="{{ asset('entidad_logo/personaicon-register.png') }}" alt="logo-empresa" class="img-fluid">
                           </center>
                        </div>
                     </div>
                     <br>
                     <div class="form-group row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                           <input id="email_useremps" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-nuevo" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo" onblur="validarExisteCorreouseremps(this)">
                           <span class="invalid-feedback" role="alert">
                           <strong id="msj_error_peremp"></strong>
                           </span>
                        </div>
                     </div>
                     <div class="form-group row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                           <input onkeyup = "if(event.keyCode == 13) logeoPersonaemp()" id="password_useremps" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-nuevo" name="password" required placeholder="Contraseña">
                           <span class="invalid-feedback" role="alert">
                           <strong id="msj_error_peremp_passemp"></strong>
                           </span>
                        </div>
                     </div>
                     <div class="form-group row justify-content-center">
                        <div class="col-xl-6 col-md-6 col-sm-12">
                           <a
                              type="btn"
                              class="btn btn_portal"
                              onclick="logeoPersonaemp()"
                              style="background-color:#00A39C; border-color:#00A39C;"
                              >
                           Ingresar
                           </a>
                        </div>
                     </div>
                      
                     <!--
                        <div class="form-group row justify-content-center">
                        <div class="col-xl-12 col-md-12 col-sm-12 text-center">
                           <a href="{{ route('social.auth', 'facebook') }}">
                           <img src="{{ asset('images/facebook.png') }}" alt="Facebook" class="img-fluid">
                           </a>
                        </div>
                     </div>
                     -->
                     <div class="form-group row justify-content-center">
                        <div class="form-group row justify-content-center">
                           <div class="col-xl-12 col-md-12 col-sm-12 regist">
                              <font style="color:black;">¿No tienes una cuenta?</font> <a href="{{ route('register',['e' => (isset($entidad) ? $entidad : '1' )]) }}" style="font-weight: bold;">Regístrate</a>
                           </div>
                        </div>
                  </form>
                  </div>
               </div>
            </div>
         </div>
      </div>

      
      <div class="useremp_options-forms login_emp_pers" id="useremp_options-forms" >
         <!--Bienvenida Empresa-->
         <div class="useremp_forms-login">
            <div class="row justify-content-center" style="margin-bottom: 50px;">
               <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                 <center>
                 <img  src="{{ asset(isset($login_logo_persona) ? $login_logo_persona : 'entidad_logo/logo_menu.svg') }}" alt="logo" class="img-fluid" style="height: 60px;">
                 </center>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                   <span  class="SegoeUil">Te da la Bienvenida</span>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
                  <p style=" text-align:center;" class="SegoeUil">¡Únete a nosotros y publica <br>tus ofertas laborales!</p>
                  <div style="    border-bottom: 1px solid #fff;
    text-align: center;
    margin-left: 10%;
    margin-right: 10%;"></div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-6 col-md-12 col-sm-12 mt-5">
                  <button 
                     type="button"
                     class="btn btn-light user_unregistered-signup redondeado SegoeUil"
                     id="signup-buttonemp"
                     style="color:#fff; width:100%; font-size:14px;background-color:#1A4A84; border-color:#1A4A84;"
                     ><b>Soy Persona</b></button>
               </div>
            </div>
         </div>
         <!--Bienvenida Persona-->
         <div class="useremp_forms-signup ">
            <br>
            <div class="row justify-content-center" style="margin-bottom: 50px;">
               <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                  <center>
                  <img  src="{{ asset('images/login-empresa.png') }}" alt="logo" class="img-fluid" style="height: 60px;">
                  </center>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                  <span  class="SegoeUil">Te da la Bienvenida</span>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-12 col-md-12 col-sm-12 mt-3">
               <p style=" text-align:center;" class="SegoeUil">
                  Accede y encuentra en trabajo <br> que buscas
               </p>
               <div style="    border-bottom: 1px solid #fff;
    text-align: center;
    margin-left: 10%;
    margin-right: 10%;"></div>
               </div>
               
            </div>
            <div class="row justify-content-center">
               <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                  <button
                     type="button"
                     class="btn btn-light useremp_registered-login redondeado SegoeUil"
                     id="login-buttonemp"
                     style="color:#fff; width:100%; font-size:14px;background-color:#00A39C; border-color:#00A39C;"
                     ><b>Soy Empresa</b></button>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
   function validarExisteCorreouseremps(codigo){
   
   var _token = $("#csrf-token").val();
     base_url = $("#base_url").val();
     var urlajax = base_url + "valida/correo/users/" + codigo.value;
   
     $.ajax({
         type: 'GET',
         url: urlajax,
         dataType: 'json',
         success: function(data) {
               if(data == "0"){
                 $("#email_useremps").addClass('is-invalid');
                 $("#msj_error_peremp").html('Este Email no se encuentra registrado');
               }else{
                 $("#email_useremps").removeClass('is-invalid');
               }
         }
     });
   
   }
   
   function logeoPersonaemp(){
   
     var urlajax =  "{{ route('login.users.ajax') }}";
   
     $.ajax({
         type: 'POST',
         url: urlajax,
         data: $("#form_useremps").serialize(),
         dataType: 'json',
         success: function(data) {
               if(data == "0"){
                 $("#password_useremps").addClass('is-invalid');  
                 $("#email_useremps").addClass('is-invalid');
                 $("#msj_error_peremp").html('');
                 $("#msj_error_peremp_passemp").html('Correo o Contraseña incorrectas');
               }else if (data == "1"){
                 $("#password_useremps").removeClass('is-invalid');
                 $("#email_useremps").removeClass('is-invalid');
                 $("#form_useremps").submit();
               } else {
                   $("#password_useremps").addClass('is-invalid');
                   $("#email_useremps").addClass('is-invalid');
                   $("#msj_error_peremp").html('');
                   $("#msj_error_peremp_passemp").html('Hubo un error en la solicitud, intentelo mas tarde');
               }
         }
     });
   
   }
   
   function validarExisteCorreoEmpresaemp(codigo){
   
   var _token = $("#csrf-token").val();
     base_url = $("#base_url").val();
     var urlajax = base_url + "valida/correo/empresa/" + codigo.value;
   
     $.ajax({
         type: 'GET',
         url: urlajax,
         dataType: 'json',
         success: function(data) {
               if(data == "0"){
                 $("#email_passemp").addClass('is-invalid');
                 $("#msj_emp_erroremp").html('Este Email no se encuentra registrado');
               }else{
                 $("#email_passemp").removeClass('is-invalid');
               }
         }
     });
   
   }
   
   function loginEmpresaemp(){
   
     var urlajax =  "{{ route('login.empresa.ajax') }}";
   
   $.ajax({
       type: 'POST',
       url: urlajax,
       data: $("#form_empresa_emp").serialize(),
       dataType: 'json',
       success: function(data) {
             if(data == "0"){
               $("#password_empresaemp").addClass('is-invalid');  
               $("#email_passemp").addClass('is-invalid');
               $("#msj_emp_erroremp").html('');
               $("#msj_error_pass_empresaemp").html('Correo o Contraseña incorrectas');
             }else{
               $("#password_empresaemp").removeClass('is-invalid');
               $("#email_passemp").removeClass('is-invalid');
               $("#form_empresa_emp").submit();
             }
       }
   });
   
   }
   /**
    * Variables
    */
   const signupButtonemp = document.getElementById('signup-buttonemp'),
       signupButtonempHome = document.getElementById('signup-buttonemp-home'),
       loginButtonemp = document.getElementById('login-buttonemp'),
       loginButtonempHome = document.getElementById('login-buttonemp-home'),
       userempForms = document.getElementById('useremp_options-forms')
   
   /**
    * Add event listener to the "Sign Up" button
    */
   signupButtonemp.addEventListener('click', () => {
     userempForms.classList.remove('bounceRightemp')
     userempForms.classList.add('bounceLeftemp')
     $("#useremp_options-forms").css('background-color','#1A4A84');
   }, false)
   
   signupButtonempHome.addEventListener('click', () => {
     userempForms.classList.remove('bounceRightemp')
     userempForms.classList.add('bounceLeftemp')
     $("#useremp_options-forms").css('background-color','#1A4A84');
   }, false)

   /**
    * Add event listener to the "Login" button
    */
   loginButtonemp.addEventListener('click', () => {
     userempForms.classList.remove('bounceLeftemp')
     userempForms.classList.add('bounceRightemp')
     $("#useremp_options-forms").css('background-color','#00A39C');
   }, false)
   
   loginButtonempHome.addEventListener('click', () => {
     userempForms.classList.remove('bounceLeftemp')
     userempForms.classList.add('bounceRightemp')
     $("#useremp_options-forms").css('background-color','#00A39C');
   }, false)
   
</script>


<script>
         function popuEmpresaLogin(value) {
            const empresa = document.querySelector('.useremp_options-unregistered');
            const person = document.querySelector('.useremp_options-registered');
            empresa.classList.add('person_movil_v2');
            person.classList.add('person_movil_v2');
            if(value == 1){
               empresa.classList.add('person_movil');
               person.classList.remove('person_movil');
            }else{
               person.classList.add('person_movil');
               empresa.classList.remove('person_movil');
            }
         }	
      </script>

