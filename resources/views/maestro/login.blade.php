<style>

  /**
   * Page background
   */
  .user {
    margin-top: 5%;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100vh;
    background-size: cover;
  }
  .user_options-container {
    position: relative;
    width: 100%;
  }
  .user_options-text {
    display: flex;
    justify-content: space-between;
    width: 100%;
    background-color: #D8D8D8;
    border-radius: 3px;
  }

  /**
   * Registered and Unregistered user box and text
   */

  .user_options-registered,
  .user_options-unregistered {
    width: 100%;
    padding: 50px 45px;
    color: #fff;
    font-weight: 300;
  }
  /*
  .user_registered-title,
  .user_unregistered-title {
    margin-bottom: 15px;
    font-size: 1.66rem;
    line-height: 1em;
  }

  .user_unregistered-text,
  .user_registered-text {
    font-size: 0.83rem;
    line-height: 1.4em;
  }

  .user_registered-login,
  .user_unregistered-signup {
    margin-top: 30px;
    border: 1px solid #ccc;
    border-radius: 3px;
    padding: 10px 30px;
    color: #fff;
    text-transform: uppercase;
    line-height: 1em;
    letter-spacing: 0.2rem;
    transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
  }


  .user_registered-login:hover,
  .user_unregistered-signup:hover {
    color: rgba(34, 34, 34, 0.85);
    background-color: #ccc;
  }
  */
  /**
   * Login and signup forms
   */
  .user_options-forms {
    position: absolute;
    top: 50%;
    left: 0px;
    width: calc(50% - 0px);
    min-height: 550px;
    border-radius: 3px;
    box-shadow: 2px 0 15px rgba(0, 0, 0, 0.25);
    overflow: hidden;
    -webkit-transform: translate3d(0, -50%, 0);
            transform: translate3d(0, -50%, 0);
    transition: -webkit-transform 0.4s ease-in-out;
    transition: transform 0.4s ease-in-out;
    transition: transform 0.4s ease-in-out, -webkit-transform 0.4s ease-in-out;
    background-color: #00A4D3;

  }

  .user_options-forms .user_forms-login {
    transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out;
  }

  .user_options-forms .forms_title {
    margin-bottom: 45px;
    font-size: 1.5rem;
    font-weight: 500;
    line-height: 1em;
    text-transform: uppercase;
    color: #e8716d;
    letter-spacing: 0.1rem;
  }

  .user_options-forms .forms_field:not(:last-of-type) {
    margin-bottom: 20px;
  }

  .user_options-forms .forms_field-input {
    width: 100%;
    border-bottom: 1px solid #ccc;
    padding: 6px 20px 6px 6px;
    font-family: "Montserrat", sans-serif;
    font-size: 1rem;
    font-weight: 300;
    color: gray;
    letter-spacing: 0.1rem;
    transition: border-color 0.2s ease-in-out;
  }
  .user_options-forms .forms_field-input:focus {
    border-color: gray;
  }
  .user_options-forms .forms_buttons {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 35px;
  }
  .user_options-forms .forms_buttons-forgot {
    font-family: "Montserrat", sans-serif;
    letter-spacing: 0.1rem;
    color: #ccc;
    text-decoration: underline;
    transition: color 0.2s ease-in-out;
  }
  .user_options-forms .forms_buttons-forgot:hover {
    color: #b3b3b3;
  }
  .user_options-forms .forms_buttons-action {
    background-color: #e8716d;
    border-radius: 3px;
    padding: 10px 35px;
    font-size: 1rem;
    font-family: "Montserrat", sans-serif;
    font-weight: 300;
    color: #fff;
    text-transform: uppercase;
    letter-spacing: 0.1rem;
    transition: background-color 0.2s ease-in-out;
  }
  .user_options-forms .forms_buttons-action:hover {
    background-color: #e14641;
  }
  .user_options-forms .user_forms-signup,
  .user_options-forms .user_forms-login {
    position: absolute;
    top: 70px;
    left: 40px;
    width: calc(100% - 80px);
    opacity: 0;
    visibility: hidden;
    transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out, -webkit-transform 0.5s ease-in-out;
    transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out, transform 0.5s ease-in-out;
    transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out, transform 0.5s ease-in-out, -webkit-transform 0.5s ease-in-out;
  }
  .user_options-forms .user_forms-signup {
    -webkit-transform: translate3d(120px, 0, 0);
            transform: translate3d(120px, 0, 0);
  }
  .user_options-forms .user_forms-signup .forms_buttons {
    justify-content: flex-end;
  }
  .user_options-forms .user_forms-login {
    -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
    opacity: 1;
    visibility: visible;
  }

  /**
   * Triggers
   */
  .user_options-forms.bounceRight {
    -webkit-animation: bounceRight 1s forwards;
            animation: bounceRight 1s forwards;
  }
  .user_options-forms.bounceRight .user_forms-signup {
    -webkit-animation: showSignUp 1s forwards;
            animation: showSignUp 1s forwards;
  }
  .user_options-forms.bounceRight .user_forms-login {
    opacity: 0;
    visibility: hidden;
    -webkit-transform: translate3d(-120px, 0, 0);
            transform: translate3d(-120px, 0, 0);
  }
  .user_options-forms.bounceLeft {
    -webkit-animation: bounceLeft 1s forwards;
            animation: bounceLeft 1s forwards;
  }

  /**
   * Responsive 990px
   */


  @media screen and (max-width: 990px) {
    .user_options-forms {
      min-height: 350px;
    }
    .user_options-forms .forms_buttons {
      flex-direction: column;
    }
    .user_options-forms .user_forms-login .forms_buttons-action {
      margin-top: 30px;
    }
    .user_options-forms .user_forms-signup,
    .user_options-forms .user_forms-login {
      top: 40px;
    }

    .user_options-registered,
    .user_options-unregistered {
      padding: 50px 45px;
    }
  }
  .button_ingre_pers .btn_portal{font-family: Galano Grotesque Bold;font-size: 18px;color:#fff; background:#1A4A84}
  .button_ingre_pers a, font{font-family: Galano Grotesque Medium;}
</style>
<section class="user" >
  <div class="user_options-container button_ingre_pers">
    <div class="user_options-text">

        <!-- *********login model 2  ********** -->

      <div class="user_options-registered">

      <div class="row justify-content-center">
        <div class="col-md-10">
                    <form method="POST" action="{{ route('login') }}" id="form_users">
                        @csrf
                        <input type="hidden" id="base_url" value="{{ asset('') }}">
                        <div class="form-group row justify-content-center">
                            <div class="col-xl-4 col-md-4 col-sm-12 text-center">
                                <img src="{{ asset('images/login-persona.png') }}" alt="logo-empresa" class="img-fluid">
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email_users" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-nuevo" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo" onblur="validarExisteCorreoUsers(this)">
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="msj_error_per"></strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input onkeyup = "if(event.keyCode == 13) logeoPersona()" id="password_users" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-nuevo" name="password" required placeholder="Contraseña">
                                    <span class="invalid-feedback" role="alert">
                                        <strong id="msj_error_per_pass"></strong>
                                    </span>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                            <a type="btn" class="btn btn_portal" onclick="logeoPersona()" >
                                Ingresar
                            </a>
                            </div>
                        </div>

                       

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8 text-center">
                                <a href="{{ route('social.auth', 'facebook') }}">
                                    <img src="{{ asset('images/facebook.png') }}" alt="Facebook" class="img-fluid">
                                </a>
                            </div>
                        </div>


                        <div class="form-group row justify-content-center">
                            <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                            <font style="color:black;">¿No tienes una cuenta?</font> <a href="{{ route('register',['e' => (isset($entidad) ? $entidad : '1' )]) }}">Regístrate</a>
                            </div>
                        </div>
                    </form>
                </div>

        </div>


      </div>

  </div>

  </div>
</section>

<script>


function validarExisteCorreoUsers(codigo){

var _token = $("#csrf-token").val();
  base_url = $("#base_url").val();
  var urlajax = base_url + "/valida/correo/users/" + codigo.value;

  $.ajax({
      type: 'GET',
      url: urlajax,
      dataType: 'json',
      success: function(data) {
            if(data == "0"){
              $("#email_users").addClass('is-invalid');
              $("#msj_error_per").html('Este Email no se encuentra registrado');
            }else{
              $("#email_users").addClass('is-valid');
            //  $("#email_users").removeClass('is-invalid');

            }
      }
  });

}

function logeoPersona(){

  var urlajax =  "{{ route('login.users.ajax') }}";

  $.ajax({
      type: 'POST',
      url: urlajax,
      data: $("#form_users").serialize(),
      dataType: 'json',
      success: function(data) {
            if(data == "0"){
              $("#password_users").addClass('is-invalid');
              $("#email_users").addClass('is-invalid');
              $("#msj_error_per").html('');
              $("#msj_error_per_pass").html('Correo o Contraseña incorrectas');
            }else{
              $("#password_users").removeClass('is-invalid');
              $("#email_users").removeClass('is-invalid');
              $("#form_users").submit();
            }
      }
  });

}

function validarExisteCorreoEmpresa(codigo){

var _token = $("#csrf-token").val();
  base_url = $("#base_url").val();
  var urlajax = base_url + "valida/correo/empresa/" + codigo.value;

  $.ajax({
      type: 'GET',
      url: urlajax,
      dataType: 'json',
      success: function(data) {
            if(data == "0"){
              $("#email_pass").addClass('is-invalid');
              $("#msj_emp_error").html('Este Email no se encuentra registrado');
            }else{
              $("#email_pass").removeClass('is-invalid');
            }
      }
  });

}

function loginEmpresa(){

var pass = $("#password_empresa").val();
/*
if(pass.length == 4){
  $("#msj_error_pass_empresa").html('Caracteres Insuficientes');
}*/
  var urlajax =  "{{ route('login.empresa.ajax') }}";

$.ajax({
    type: 'POST',
    url: urlajax,
    data: $("#form_empresa").serialize(),
    dataType: 'json',
    success: function(data) {
          if(data == "0"){
            $("#password_empresa").addClass('is-invalid');
            $("#email_pass").addClass('is-invalid');
            $("#msj_emp_error").html('');
            $("#msj_error_pass_empresa").html('Correo o Contraseña incorrectas');
          }else{
            $("#password_empresa").removeClass('is-invalid');
            $("#email_pass").removeClass('is-invalid');

            $("#form_empresa").submit();
          }
    }
});

}


</script>
