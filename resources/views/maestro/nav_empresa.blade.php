<style>
.img-usuario{
    width:200px !important; 
}

.icon {
  color:#014b72 !important;
}
</style>

<nav class="navbar navbar-expand-md navbar-dark">

  <a class="navbar-brand " href="#"><img src="{{ asset('images/icon-logo.jpg') }}" class="img-usuario"></a>
  

  <div class="collapse navbar-collapse" id="navbarsExample04">
  <ul class="navbar-nav mr-auto">
  </ul>
    <ul class="navbar-nav">
      
      
      <li class="nav-item" style="padding: 10px;">
        <a class="nav-link icon" href="#">
            <i class="fas fa-address-card fa-2x"></i>
        </a>
      </li>
      <li class="nav-item" style="padding: 10px;">
        <a class="nav-link icon" href="{{ route('empresa.vacante') }}" >
            <i class="fas fa-suitcase fa-2x" data-toggle="tooltip" data-placement="top" title="Nuevo Pedido"></i>
        </a>
      </li>
  
    </ul>
    
  </div>
</nav>
