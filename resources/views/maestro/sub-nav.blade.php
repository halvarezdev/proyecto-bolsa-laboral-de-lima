
    @auth('contacto')
        <!-- CUANDO ESTA LOGEADO -->
    @else
      @auth
      @else
        <!-- CUANDO NO ESTA LOGEADO -->
        <div class="container">
           <nav class="navbar navbar-expand-md navbar-dark " >
        <div class="collapse navbar-collapse" id="navbarsExample04">
        <div class="row justify-content-end" style="width: 100%;">
         
          <div class="col-sm-12 col-md-2">
           <!-- Button trigger modal Persona -->
            <button  type="button" class="btn btn-outline-primary nav-sub" style="width: 100%;" data-toggle="modal" data-target="#exampleModalCenter">
              <i class="fas fa-user"></i>
              &nbsp;&nbsp;
              <font>Persona</font>
            </button>
          </div>
          <div class="col-sm-12 col-md-2">
            <button  type="button" class="btn btn-outline-primary nav-sub" style="width: 100%;" data-toggle="modal" data-target="#empresalogin">
              <i class="fas fa-building"></i>
              &nbsp;&nbsp;
              <font>Empresa</font>
            </button>
          </div>

        </div>
        
          
        </div>
      </nav>
      </div>
      @endauth
    @endauth

        <script type="text/javascript">
          function enviarPersona(){
            window.location.href="{{ route('login') }}";
          }
          function enviarEmpresa(){
            window.location.href="{{ asset('contacto/login') }}";
          }
        </script>
            