<div class="contenedor">
<nav class="navbar navbar-light justify-content-between menu_solid" style="box-shadow: 0 4px 2px -2px #a6a6a6;">
<div class="container">
  <a class="navbar-brand aller_display color-rojo" href="{{ route('welcome') }}">
      PERSONAS       
  </a>

  <ul class="nav nav-pills" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link" href="{{ route('home') }}" aria-selected="false" style="color:#00A39C;"><i class="far fa-user"></i>&nbsp;Registro de mi perfil</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('postulaciones') }}" aria-selected="false" style="color:#00A39C;"><i class="far fa-folder-open"></i>&nbsp;Mis postulaciones</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('notificacion') }}" aria-selected="false" style="color:#00A39C;"><i class="far fa-bell"></i>&nbsp;Mis notificaciones</a>
    </li>
    <!-- <li class="nav-item">
      <a class="nav-link" href="{{ route('convocatoria') }}" aria-selected="false" style="color:#fff;"><i class="far fa-file-alt"></i>&nbsp;Vacantes sugeridas</a>
    </li> -->
    <li class="nav-item">
      <a class="nav-link" href="{{ route('users.busqueda.inicio', ['e' => 1]) }}" aria-selected="false" style="color:#00A39C;"><i class="far fa-bell"></i>&nbsp;Ofertas Laborales</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('favoritas') }}" aria-selected="false" style="color:#00A39C;"><i class="far fa-star"></i>&nbsp;Mis favoritos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('configuracion') }}" aria-selected="false" style="color:#00A39C;"><i class="fas fa-cogs"></i>&nbsp;Configuración</a>
    </li>
  </ul>

  </div>
</nav>
</div>
