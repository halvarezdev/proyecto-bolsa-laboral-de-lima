<div class="contenedor">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel menu_solid"style="box-shadow: 0 4px 2px -2px #a6a6a6;">
        <div class="container">
            <a class="navbar-brand aller_display" href="{{ route('welcome') }}" style="color:#00A39C;">
            EMPRESAS 
            </a>  
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <ul class="navbar-nav ml-auto">
            @if(isset($submenu) && $submenu == "1")
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('contacto.dashboard') }}" style="color:#00A39C;font-size: 16px;">
                    <img src="{{ asset('animacion/publicacion.png') }}">
                    Mis Publicaciones</a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{{ route('lista.contacto') }}" style="color:#00A39C;font-size: 16px;">
                        <img src="{{ asset('animacion/buscar_cv.png') }}">
                        Mi Perfil</a>
                    </li>
                <!--
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color:#575756;font-size: 16px;">
                        <img src="{{ asset('animacion/buscar_cv.png') }}">
                        Buscar Cvs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('empresa.servicios', ['e' => $entidad]) }}" style="color:#575756;font-size: 16px;">
                        <img src="{{ asset('animacion/servicio.png') }}">
                        Servicios</a>
                    </li>
                -->
                <li class="nav-item">
                    <a class="btn btn-danger" href="{{ route('empresa.vacante') }}" style="font-size: 12px; padding: .300rem .75rem;margin-top: 4px;margin-left: 45px;background-color: #00A39C;border: #00A39C;">Publicar vacante</a>
                </li>
            @endif
            </ul>    
        </div>
    </nav>  
</div>

