<div class="animationload" style="display: none;">
 <div class="osahanloading"></div>
</div>
<nav class="navbar navbar-expand-md navbar-dark fondo_portal">

  <a class="navbar-brand " href="{{ route('welcome') }}">ACCEDE</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

    <div class="collapse navbar-collapse" id="navbarsExample04">
      <div class="container">
            
            <ul class="navbar-nav mr-auto nav justify-content-end" >
            
              <li class="nav-item active">
                <a class="nav-link" href="{{ route('welcome') }}">INICIO<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">NOSOTROS</a>
              </li>
              
              <li class="nav-item active">
                <a class="nav-link" href="#">CONSULTA</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">CONTÁCTENOS</a>
              </li>
              
              <li class="nav-item dropdown">
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="{{ route('welcome') }}">INICIO</a>
                        <a class="dropdown-item" href="#">NOSOTROS</a>
                        <a class="dropdown-item" href="{{ route('register') }}">CONSULTA</a>
                        <a class="dropdown-item" href="{{ route('login') }}">CONTÁCTENOS</a>
                </div>
              </li>
            </ul>
      </div>
  </div>
</nav>
        
            