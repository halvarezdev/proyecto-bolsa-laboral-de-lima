@extends('layouts.app')

@section('content')
<style>
.img-perfil {
    width: 150px;
    height: 150px;
    background-image: url('{{ asset('/public/storage/'.$ruta_foto) }}');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    border: solid 1px #aba6a6;
}

/*.img-perfil>span{
  display:none;
  height:30px;
  }

.img-perfil>span:hover{
  display:block;
  position:absolute;
  height:30px;
  bottom:0;
  left:0;
  background:rgba(0,0,0,0.7);
  width:100%;
  height:30px;
  vertical-align:middle;
  text-align:center

}
*/
.subirimg {
    position: absolute;
    bottom: 47px;
    /*left:0;*/
    background: rgba(0, 0, 0, 0.7);
    width: 150px;
    height: 30px;
    vertical-align: middle;
    text-align: center;
    color: #fff;
    cursor: pointer;
}

input[type="file"] {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
}

.datos_empresa {
    transition: color 0.8s linear 0.2s;
}
</style>

@foreach($empresa as $emp)
<style>
.bannerfoto {
    background-image:url('{{ asset("public/storage/$emp->banner_foto") }}');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 250px;
}
</style>

@endforeach
<div class="hidden-mobile">
    <?php
$submenu = "1";
?>
    @include('maestro/submenu')
    <br>
</div>

<div class="show-mobile navbar-brand aller_display">
    MI PERFIL
</div>

<div class="container">
    <div class="card" style="border:none;">
        <div class="card-body">
            <div class="row">
                <div class="col-12 bannerfoto" style="border-bottom  : solid 1px #BDBDBD;">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('contacto.ajax.banner') }}" method="post"
                                enctype="multipart/form-data" id="form_bannenr">
                                @csrf
                                <input type="file" name="file_banner" id="file_banner"
                                    onchange="uptapeBannerPerfil(this)" accept=".png,.jpg,.jpge">
                            </form>
                        </div>
                        <div class="col-2">

                            <a href="#" class="hidden-mobile btn btn-primary nav-sub btn-sm"
                                style="width: 60%;margin-left: 25%;position: absolute;background-color:{{ $entidad_color }};padding:0px;margin-top: 10px;"
                                onclick="subirBanner()">Subir Imagen</a>

                            <a href="#" class="button-mobile btn btn-primary nav-sub btn-sm"
                                style="background-color:{{ $entidad_color }};" onclick="subirBanner()">Subir Imagen</a>

                        </div>
                    </div>


                </div>
                <p style="color:#b4b8ba;">Tamaño recomendado:<span>1000x300,</span> Min
                    Formatos : (jpg, png,jpge),
                    Menor de: <span>300KB</span>
                </p>
            </div>
            <br>
            <div class="hidden-mobile row">
                <div class="col-10">
                    <p>&nbsp; </p>
                </div>
                <div class="col">
                    <a href="#" class="btn btn-primary nav-sub btn-sm"
                        style="width: 60%;margin-left: 25%;position: absolute;background-color:{{ $entidad_color }};padding:0px;"
                        onclick="EditarDatosEmpresa()">Modificar</a>
                </div>
            </div>

            <!--MODIFICAR EMPRESA Y FOTO-->
            <div class="hidden-mobile row">
                <div class="col-2">
                    <a href="#" id="uploadImage" onclick="subirfoto()">
                        <div class="img-perfil rounded mx-auto d-block photo_perfil imagen-login"
                            onmouseover="divhover(this)" onmouseout="normalImg(this)">
                            <div class="subirimg" id="span" style="display:none;">
                                <label id="subirimg" style="cursor:pointer;"><i class="fas fa-upload"></i>Subir</label>
                            </div>
                        </div>
                    </a>

                    <br>
                    <form action="{{ route('contacto.ajax.foto') }}" method="post" enctype="multipart/form-data"
                        id="form_foto">
                        @csrf
                        <input type="file" name="file" id="input_file" onchange="uptapeFotoPerfil(this)"
                            accept=".png,.jpg,.jpge">
                    </form>

                </div>
                <div class="col-2">
                    <br>
                    <p style="color:#b4b8ba;">Tamaño <br>recomendado: <br><span>150x150</span> Min
                        <br>Formatos : jpg, png,jpge<br>
                        Menor de: <span>100KB</span>
                    </p>
                </div>


                <div class="col">

                    <div class="datos_empresa" id="datos_empresa">


                        @foreach($empresa as $emp)

                        <div class="row">
                            <div class="col-2" style="text-align: right;">
                                <b>RUC:</b>
                            </div>
                            <div class="col">
                                <p style="margin-bottom: 3px;">{{ strtoupper($emp->numero_documento) }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-2" style="text-align: right;">
                                <b>Razon Social:</b>
                            </div>
                            <div class="col">
                                <p style="margin-bottom: 3px;">{{ strtoupper($emp->razon_social) }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-2" style="text-align: right;">
                                <b>Rubro:</b>
                            </div>
                            <div class="col">
                                <p style="margin-bottom: 3px;">{{ strtoupper($emp->sec_descripcion) }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-2" style="text-align: right;">
                                <b>Descripción:</b>
                            </div>
                            <div class="col">
                                <p style="margin-bottom: 3px;">{{ strtoupper($emp->descripcion) }}</p>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-2" style="text-align: right;">
                                <b>Dirección:</b>
                            </div>
                            <div class="col">
                                <p style="margin-bottom: 3px;">{{ strtoupper($emp->direccion) }}</p>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-2" style="text-align: right;">
                                <b>Sitio Web:</b>
                            </div>
                            <div class="col">
                                <p style="margin-bottom: 3px;">{{ strtoupper($emp->sitio_web) }}</p>
                            </div>
                        </div>

                        <!--
    <div class="row">
  <div class="col-2" style="text-align: right;">
    <b>Video:</b>
    </div>
    <div class="col">
    <p style="margin-bottom: 3px;">{{ strtoupper($emp->video) }}</p>
    </div>
  </div>
  -->

                        @endforeach

                    </div>

                    <!-- editar empresa -->

                    <div class="edita_empresa" style="display:none;" id="edita_empresa">
                        <form action="{{ route('editar.contacto') }}" method="post">
                            @csrf
                            @foreach($empresa as $emp)
                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label>Rubro</label>
                                    <select id="rubro" class="demo-default form-control form-control-sm buscar"
                                        data-placeholder="Seleccione Rubro" name="rubro" onfocus="limiartodo(this)">
                                        <option value="">Buscar</option>
                                        @foreach($sector as $sec)
                                        <option value="{{ $sec->id }}"
                                            {{ (isset($emp->rubro) && $emp->rubro == $sec->id  ? "selected" : "") }}>
                                            {{ $sec->sec_descripcion }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Sitio Web</label>
                                    <input type="text" class="form-control form-control-sm" id="sitioweb"
                                        name="sitioweb" placeholder="Sitio Web" value="{{ ucwords($emp->sitio_web) }}">
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label>Dirección</label>
                                    <input type="text" class="form-control form-control-sm" id="direccion"
                                        name="direccion" placeholder="Direccion" value="{{ ucwords($emp->direccion) }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Descripción</label>
                                    <input type="text" class="form-control form-control-sm" id="descripcion"
                                        name="descripcion" placeholder="Descripcion"
                                        value="{{ ucwords($emp->descripcion) }}">
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-12">
                                    <label>Video de Presentación</label>
                                    <input type="text" class="form-control form-control-sm" id="video" name="video"
                                        placeholder="Video de Presentación " value="{{ ucwords($emp->emp_urlvideo) }}">
                                </div>
                            </div>
                            @endforeach

                            <button type="submit" class="btn btn-success form-control-sm">Editar Información</button>
                        </form>
                    </div>

                </div>


            </div>
        </div>

    </div>
    <br>
    <!--FIN MODIFICAR EMPRESA-->

    <!--MODIFICAR EMPRESA Y FOTO mobil -->
    <div mod_mobile class="row">
        <div class="col-6 .col-sm-4">
            <a href="#" id="uploadImage" onclick="subirfoto()">
                <div class="img-perfil rounded mx-auto d-block photo_perfil imagen-login" onmouseover="divhover(this)"
                    onmouseout="normalImg(this)">
                    <div class="subirimg" id="span" style="display:none;">
                        <label id="subirimg" style="cursor:pointer;"><i class="fas fa-upload"></i>Subir</label>
                    </div>
                </div>
            </a>

            <br>
            <form action="{{ route('contacto.ajax.foto') }}" method="post" enctype="multipart/form-data" id="form_foto">
                @csrf
                <input type="file" name="file" id="input_file" onchange="uptapeFotoPerfil(this)"
                    accept=".png,.jpg,.jpge">
            </form>

        </div>
        <div class="col-6 .col-sm-4">
            <br>
            <p style="color:#b4b8ba;">Tamaño <br>recomendado: <br><span>150x150</span> Min
                <br>Formatos : jpg, png,jpge<br>
                Menor de: <span>100KB</span>
            </p>
            <div modificar_mobile class="row">
                <div class="col-10">
                    <p>&nbsp; </p>
                </div>
                <div class="col">
                    <a href="#" class="btn btn-primary nav-sub btn-sm" style="background-color:{{ $entidad_color }};"
                        onclick="EditarDatosEmpresaMobile()">Modificar</a>
                </div>
            </div>
        </div>

        <br><br><br>

        <div class="col">

            <div class="datos_empresa_mobile" id="datos_empresa_mobile">


                @foreach($empresa as $emp)

                <div class="row">
                    <div class="col-6 .col-sm-4" style="text-align: right;">
                        <b>RUC:</b>
                    </div>
                    <div class="col-6 .col-sm-4">
                        <p p_wrap style="margin-bottom: 3px;">{{ strtoupper($emp->numero_documento) }}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 .col-sm-4" style="text-align: right;">
                        <b>Razon Social:</b>
                    </div>
                    <div class="col-6 .col-sm-4">
                        <p p_wrap style="margin-bottom: 3px;">{{ strtoupper($emp->razon_social) }}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 .col-sm-4" style="text-align: right;">
                        <b>Rubro:</b>
                    </div>
                    <div class="col-6 .col-sm-4">
                        <p p_wrap style="margin-bottom: 3px;">{{ strtoupper($emp->sec_descripcion) }}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 .col-sm-4" style="text-align: right;">
                        <b>Descripción:</b>
                    </div>
                    <div class="col-6 .col-sm-4">
                        <p p_wrap style="margin-bottom: 3px;">{{ strtoupper($emp->descripcion) }}</p>
                    </div>

                </div>

                <div class="row">
                    <div class="col-6 .col-sm-4" style="text-align: right;">
                        <b>Dirección:</b>
                    </div>
                    <div class="col-6 .col-sm-4">
                     <p p_wrap style="margin-bottom: 3px;"> {{ strtoupper($emp->direccion) }}</p>
                    </div>

                </div>

                <div class="row">
                    <div class="col-6 .col-sm-4" style="text-align: right;">
                        <b>Sitio Web:</b>
                    </div>
                    <div class="col-6 .col-sm-4">
                        <p p_wrap style="margin-bottom: 3px;">{{ strtoupper($emp->sitio_web) }}</p>
                    </div>
                </div>

                <!--
    <div class="row">
  <div class="col-2" style="text-align: right;">
    <b>Video:</b>
    </div>
    <div class="col">
    <p style="margin-bottom: 3px;">{{ strtoupper($emp->video) }}</p>
    </div>
  </div>
  -->

                @endforeach

            </div>

            <!-- editar empresa -->

            <div class="edita_empresa_mobile" style="display:none;" id="edita_empresa_mobile">
                <form action="{{ route('editar.contacto') }}" method="post">
                    @csrf
                    @foreach($empresa as $emp)
                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label>Rubro</label>
                            <select id="rubro" class="demo-default form-control form-control-sm buscar"
                                data-placeholder="Seleccione Rubro" name="rubro" onfocus="limiartodo(this)">
                                <option value="">Buscar</option>
                                @foreach($sector as $sec)
                                <option value="{{ $sec->id }}"
                                    {{ (isset($emp->rubro) && $emp->rubro == $sec->id  ? "selected" : "") }}>
                                    {{ $sec->sec_descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Sitio Web</label>
                            <input type="text" class="form-control form-control-sm" id="sitioweb" name="sitioweb"
                                placeholder="Sitio Web" value="{{ ucwords($emp->sitio_web) }}">
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label>Dirección</label>
                            <input type="text" class="form-control form-control-sm" id="direccion" name="direccion"
                                placeholder="Direccion" value="{{ ucwords($emp->direccion) }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Descripción</label>
                            <input type="text" class="form-control form-control-sm" id="descripcion" name="descripcion"
                                placeholder="Descripcion" value="{{ ucwords($emp->descripcion) }}">
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-12">
                            <label>Video de Presentación</label>
                            <input type="text" class="form-control form-control-sm" id="video" name="video"
                                placeholder="Video de Presentación " value="{{ ucwords($emp->emp_urlvideo) }}">
                        </div>
                    </div>
                    @endforeach

                    <button type="submit" class="btn btn-success form-control-sm">Editar Información</button>
                </form>
            </div>

        </div>


    </div>
</div>

</div>
<br>
<!--FIN MODIFICAR EMPRESA-->

<div class="card" style="border:none;">
    <div class="card-body">
        <div class="row degrade-azul" style="border-radius:0.5rem; margin-left: 1px; margin-right: 1px;">
            <div class="col-10" style="padding:0.5rem;">&nbsp;
                <i class="fas fa-folder-open" style="font-size: 19px; color:#fff"></i>&nbsp;
                <b style="color:#fff">MIS CONTACTOS</b>
            </div>
            <div class="col-2" style="border-bottom  : solid 1px #BDBDBD;">
                <!--  <button type="button" style="top: 15%;width: 60%;margin-left: 25%;position: absolute;background-color:{{ $entidad_color }};padding:0px;" class="btn btn-primary nav-sub btn-sm" 
                        data-toggle="modal" data-target="#contacto" onclick="limpiarContacto()">Agregar</button>
-->
            </div>
        </div>
        <br>
        <div  class="hidden-mobile row">
            <div class="col">
                <table class="table table-striped table-hover table-sm">
                    <thead>
                        <tr>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                N°</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Numero Documento</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Nombre</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Email</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Cargo</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($lista_contacto as $i => $value)
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $value->numero_documento }}</td>
                            <td>{{ strtoupper($value->name) }}</td>
                            <td>{{ strtoupper($value->email) }}</td>
                            <td>{{ strtoupper($value->cargo) }}</td>
                            <td style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#contacto"
                                    onclick="return editarContacto('{{ $value->id }}',this)">
                                    <i class="fas fa-pencil-alt" style="color:{{ $subcolor_entidad }}"></i>
                                </a>
                            </td>
                            <td style="font-size: 12px;">
                                @if($i > 0)
                                <a href="{{ route('elimina.contacto',['id' => $value->id]) }}">
                                    <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>


            </div>
        </div>

        <!--mis contactos-mobile-->
        <div  contactos_mobile class="table-responsive">
            
                <table class="table table-sm table-hover">
                    <thead>
                        <tr>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                N°</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Numero Documento</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Nombre</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Email</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                                Cargo</th>
                            <th class="th-top"
                                style="border-bottom: solid 1px {{ $subcolor_entidad }} !important; color:{{ $subcolor_entidad }}; ">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($lista_contacto as $i => $value)
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $value->numero_documento }}</td>
                            <td>{{ strtoupper($value->name) }}</td>
                            <td>{{ strtoupper($value->email) }}</td>
                            <td>{{ strtoupper($value->cargo) }}</td>
                            <td style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#contacto"
                                    onclick="return editarContacto('{{ $value->id }}',this)">
                                    <i class="fas fa-pencil-alt" style="color:{{ $subcolor_entidad }}"></i>
                                </a>
                            </td>
                            <td style="font-size: 12px;">
                                @if($i > 0)
                                <a href="{{ route('elimina.contacto',['id' => $value->id]) }}">
                                    <i class="fas fa-trash" style="color:{{ $subcolor_entidad }}"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>


          
        </div>
        <!--fin mis contactos mobile-->


    </div>

</div>

</div>

<br><br>
<!-- Large modal -->


<div class="modal fade bd-example-modal-lg" id="contacto" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">NUEVO CONTACTO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('popup_empresa.nuevo_contacto')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<br>

@endsection


<script>
function divhover(objeto) {
    //alert("todo esta aqui");
    document.getElementById('span').style.display = "block";
}

function normalImg(objeto) {
    document.getElementById('span').style.display = "none";
}

function subirfoto() {
    //alert("alertcrj");
    $("#input_file").click();
}

function subirBanner() {
    //alert("alertcrj");
    $("#file_banner").click();
}

function uptapeFotoPerfil(input) {
    var archivoRuta = document.getElementById("input_file").value;
    var extPermitidas = /(.png|.jpg|.jpge)$/i;

    if (!extPermitidas.exec(archivoRuta)) {
        alert("Tipo de archivo incorrecto");
        return false;
    }
    size = input.files[0].size;
    if (size > 100 * 1024) {
        alert("La foto debe tener un tamaño no superior a 100 KB.");
        return false;
    } else {
        $("#form_foto").submit();
    }
}

function uptapeBannerPerfil(input) {
    var archivoRuta = document.getElementById("file_banner").value;
    var extPermitidas = /(.png|.jpg|.jpge)$/i;

    if (!extPermitidas.exec(archivoRuta)) {
        alert("Tipo de archivo incorrecto");
        return false;
    }

    size = input.files[0].size;
    if (size > 1000 * 1024) {
        alert("La foto debe tener un tamaño no superior a 1000 KB.");
        return false;
    } else {
        $("#form_bannenr").submit();
    }
}

function EditarDatosEmpresa() {

    $("#datos_empresa").css('display', 'none');
    $("#edita_empresa").css('display', 'block');
}

function EditarDatosEmpresaMobile() {

    $("#datos_empresa_mobile").css('display', 'none');
    $("#edita_empresa_mobile").css('display', 'block');
}


function editarContacto(codigo, input) {

    $("#exampleModalLabel").html("EDITAR CONTACTO");

    var _token = $("#csrf-token").val();
    base_url = $("#base_url").val();
    var urlexperiencia = base_url + "edit/contacto/" + codigo;

    $.ajax({
        type: 'GET',
        url: urlexperiencia,
        dataType: 'json',
        success: function(data_experiencia) {
            $.each(data_experiencia, function(key, experiencia) {
                console.log(experiencia.id_ocupacion);
                $("#tipo_documento_contacto").val(experiencia.tipo_documento);
                $("#numero_documento").val(experiencia.numero_documento);
                $("#nombre").val(experiencia.name);
                $("#cargo").val(experiencia.cargo);
                $("#telefono").val(experiencia.telefono);
                $("#email").val(experiencia.email);
                $("#password").val("0000000");
                $("#codigo_contacto").val(codigo);
                $("#tipo_documento_contacto option").attr("disabled", 'disabled');
                $("#ocultar").css('display', 'none');
                $("#form_contacto").attr("action", "{{ route('form.contacto.update') }}");
                //  console.log(superior);
            });
        }
    });
}


function limpiarContacto() {
    $("#ocultar").css('display', 'none');
    $("#tipo_documento_contacto").val("");
    $("#numero_documento").val("");
    $("#tipo_documento_contacto option").removeAttr("disabled");
    $("#nombre").val("");
    $("#cargo").val("");
    $("#telefono").val("");
    $("#email").val("");
    $("#codigo_contacto").val("");
    $("#password").val("");
    $("#form_contacto").attr("action", "{{ route('contacto.nuevo') }}");




}
</script>