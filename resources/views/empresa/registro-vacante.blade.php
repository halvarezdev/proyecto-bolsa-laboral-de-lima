@extends('layouts.app')

@section('content')


<div style="padding-top:0px !important;
      padding-bottom:0px !important;
      padding-left:1% !important;
      padding-right:1% !important;">
@if(isset($tipo_entidad) && $tipo_entidad == '2')
<style>
  /*  body{
    background-image: url("{{ asset('fondo/cenfotur-fondo.png') }}" );
    background-size: cover;
        -moz-background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;

  }*/
</style>
    <form method="POST" action="{{ route('entidad.empresa.create') }}" id="formulario_vacante">
    <input name="empresa_cod" value="{{ $cod_empresa }}" type="hidden">
@else
   @if($accion == "2")
        <form method="POST" action="{{ route('empresa.create') }}" id="formulario_vacante">
   @else
        <form method="POST" action="{{ route('vacante.editarForm') }}" id="formulario_vacante">
   @endif
@endif
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
    <br>
    <div class="row justify-content-center pb-4 pt-3" style="background:#fff">
        <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5">
                        <div class="div_titulo" >
                            <h4 class="titulo" style="text-align: left;"><b>NUEVO PEDIDO</b></h4>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <!-- navs -->
                        <ul class="nav nav-pills mb-3 justify-content-left" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link active"   id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">1</button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link" disabled id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">2</button>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <!-- INICIO VACANTE 1 -->
                        @include('vacante/vista_vacante')

                    <!-- INICIO VACANTE 2 -->
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                        @include('vacante/vista_vacante_segunda')

                    </div>
                </div>

         </div>
     </div>


     </form>

</div>
<script>
function siguienteVacante(){

    if($("#descripcion").val() == ""){
        $(".descripcion").html('Tiene que Ingresar Descripcion').show();
        $("#descripcion").focus();
        return false;
    } else {
        $(".descripcion").html('').hide();
    }

    if($("#denominacion").val() == ""){
        $(".denominacion").html('Tiene que Ingresar Denominacion').show();
        $("#denominacion").focus();
        return false;
    } else {
        $(".denominacion").html('').hide();
    }

    if($("#num_vacantes").val() == ""){
        $(".num_vacantes").html('Tiene que Ingresar Numero de Vacantes').show();
        $("#num_vacantes").focus();
        return false;
    } else {
        $(".num_vacantes").html('').hide();
    }

    if($("#area").val() == ""){
        $(".area").html('Tiene que Ingresar Area').show();
        $("#area").focus();
        return false;
    } else {
        $(".area").html('').hide();
    }

   /* if($("#estudios_formales").val() == ""){
        $(".estudios_formales").html('Tiene que Ingresar Estudios Formales').show();
        $("#estudios_formales").focus();
        return false;
    }
*/
   /* if($("#especialidad").val() == ""){
        $(".especialidad").html('Tiene que Ingresar Especialidad').show();
        $("#especialidad").focus();
        return false;
    }
*/
    if($("#otra_especialidad").val() == ""){
        $(".otra_especialidad").html('Tiene que Ingresar Otra Especialidad').show();
        $("#otra_especialidad").focus();
        return false;
    } else {
        $(".otra_especialidad").html('').hide();
    }
    var compu3=$("#idioma3").val();
    var compu2=$("#idioma2").val();
    var compu1=$("#idioma1").val(); 
    var dumo=compu3+compu2+compu1;
    if(dumo.length >=10){

        if(compu3==compu2){
            alert('Estudios Idiomas duplicado')
            return false;
        }
        
        if(compu3==compu1){
            alert('Estudios Idiomas duplicado')
            return false;
        }else if(compu2==compu1){
            alert('Estudios Idiomas duplicado')
            return false;
        }
    }
    compu3=$("#compu3").val();
    compu2=$("#compu2").val();
    compu1=$("#compu1").val(); 
    dumo=compu3+compu2+compu1;
    if(dumo.length >=10){
        if(compu3==compu2 ){
            alert('Estudios de Computación duplicado')
            return false;
        }else if(compu3==compu1){
            alert('Estudios de Computación duplicado')
            return false;
        }else if(compu2==compu1){
            alert('Estudios de Computación duplicado')
            return false;
        }
    }
/*
    if($("#grado").val() == ""){
        $(".grado").html('Tiene que Ingresar Grado').show();
        $("#grado").focus();
        return false;
    }

    if($("#edad").val() == ""){
        $(".edad").html('Tiene que Ingresar Edad').show();
        $("#edad").focus();
        return false;
    }

    if($("#edad_total").val() == ""){
        $(".edad_total").html('Tiene que Ingresar Edad').show();
        $("#edad_total").focus();
        return false;
    }
*/

    $('#pills-home').removeClass('show');
    $('#pills-home').removeClass('active');
    $('#pills-home-tab').removeClass('show active');
    $('#pills-profile').addClass('show active');
    $('#pills-profile-tab').addClass('show active');


}

</script>
<script type="text/javascript" src="{{ asset('js/persona.js') }}?v2={{date('YmdHis')}}"></script>
<style type="text/css">
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: #fff;
        background-color: #1A4A84;
    }
</style>
<br>
@if(!isset($tipo_entidad) && $tipo_entidad != '2')
@include('maestro/footer_usuario')
@endif


@endsection
