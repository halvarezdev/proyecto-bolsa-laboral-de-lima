@extends('layouts.app')

@section('content')

<div class="hidden-mobile">
    <?php
$submenu = "1";
?>
    @include('maestro/submenu')
    <br>
</div>

<style>
.nav-link{
    padding-left: 20px;
    padding-right: 20px;
}
.nav-link:hover{
    padding-left: 21px;
    padding-right: 21px;
}
.nav-tabs .nav-link.active{
    color:#fff;
    background-color:{{ $entidad_color }};
}
</style>
<br>
<div class="show-mobile navbar-brand aller_display">
    SERVICIOS
</div>
<div class="container">

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    <img src="{{ asset('img_eficiencia/logo-eficiencia.png') }}" style="width:100%;" alt="">
                </div>
                <div class="col-10">

                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                <h3><b>EFICIENCIA PROFESIONAL</b></h3>
                <br>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-1"></div>
                <div class="col">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Servicios que Brindamos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Principales clientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Herramientas Informáticas:</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="informatica_tab" data-toggle="tab" href="#informatica" role="tab" aria-controls="informatica" aria-selected="false" href="">Nuestras Oficinas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="staf_tab" data-toggle="tab" href="#staf" role="tab" aria-controls="staf" aria-selected="false" href="">Staff</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <!-- servicios que brindamos -->

                        <br>
                        <h5><b>1. Servicios que Brindamos</b></h5>
                        <p>Nuestra experiencia a través de los años, nos ha permitido brindar soluciones a nuestros clientes, los cuales son:</p>
                        <br>
                        <img src="{{ asset('img_eficiencia/image_descri.png') }}" style="width:60%;display:flex;margin:0 auto;" alt="">
                        <br>
                        <br>
                        <h5><b>Atracción y Selección</b></h5>
                        <p>Garantizamos el ingreso del mejor talento a su organización, basándonos en el conocimiento de su empresa y estrategias innovadoras de atracción y selección de talento.  Contamos con 23 años de experiencia en la selección de ejecutivos, la tercerización de procesos de selección y recientemente ayudamos a nuestros clientes a crear y fortalecer su imagen y reputación como empleador en el mercado peruano.</p>
                        <p>La División de Atracción y Selección ofrece los siguientes servicios:</p>
                        <ul>
                            <li>Head Hunting y selección de ejecutivos </li>
                            <li>Benchmarking de selección y mapeo de la industria</li>
                            <li>Assessment Centers de selección</li>
                            <li>Búsqueda de alto potencial</li>
                            <li>Evaluación por competencias</li>
                            <li>Selección de perfiles por proyectos: apertura de tiendas, agencias, perfiles comerciales, atención al cliente, entre otros.</li>
                            <li>Proyectos de atracción y servicios especializados</li>
                        </ul>
                        <h5><b>Gestión y Desarrollo del Talento</b></h5>
                        <p>Las empresas buscan crear un proceso de identificación, desarrollo y retención de los futuros líderes de la organización. Por ello brindamos asesoría integral en el proceso de gestión del talento desde la definición de la matriz de potencial, identificación del talent pool, asesoría para asegurar la diversidad del pool de talento, programas de desarrollo a medida, desarrollo de una adecuada estrategia de comunicación y la formación a los líderes para lograr la sostenibilidad de los programas de desarrollo de talento, a través de los siguientes servicios:</p>
                        <ul>
                            <li>Identificación del Talent Pool</li>
                            <li>Programas de desarrollo de perfiles de alto potencial o high potential</li>
                            <li>Programas de desarrollo de mentores internos</li>
                            <li>Programa de desarrollo de líderes</li>
                            <li>Gestión y evaluación de desempeño</li>
                            <li>Gestión de la sucesión y líneas de carrera</li>
                            <li>Programas de Formación y Aprendizaje</li>
                        </ul>
                        <h5><b>Orientación Ejecutiva</b></h5>
                        <p>Ser consciente de las fortalezas y áreas de oportunidad pueden acelerar o interferir en la línea de carrera de un profesional. A través de este servicio asesoramos a su empresa con programas a medida para ejecutivos en proceso de cambio en sus carreras que requieren los servicios de:</p>
                        <ul>
                            <li>Coaching a ejecutivos de reorientación de carrera</li>
                        </ul>
                        <h5><b>Consultoría Estratégica</b></h5>
                        <p>Con nuestro servicio de Consultoría Estratégica acompañamos a las empresas en distintas soluciones a medida en Gestión Humana, con la finalidad de mejorar su performance en la industria o sector.</p>
                        <p>Eficiencia brinda los siguientes servicios en este rubro: </p>
                        <ul>
                            <li>Gestión de Marca Empleador y Desarrollo de Propuesta de Valor del </li>
                            <li>empleado</li>
                            <li>Gestión del clima laboral</li>
                            <li>Gestión del Compromiso y Retención</li>
                            <li>Diseño organizacional y gestión del cambio</li>
                        </ul>

                    <!-- fin servicios -->
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                        <!-- inicion 2.	Principales clientes -->
                        <br>
                        <h5><b>2. Principales clientes</b></h5>
                        <p>A continuación, detallamos algunos de nuestros clientes:</p>
                        <b>Sector Minería e Hidrocarburo</b>
                        <ul>
                            <li>Compañía Minera Antamina S.A.</li>
                            <li>Compañía Minera Gold Fields</li>
                            <li>Compañía Las Bambas</li>
                            <li>Compañía Minera Hudbay</li>
                            <li>PBF</li>
                        </ul>
                        <b>Sector Energía</b>
                        <ul>
                            <li>Enel</li>
                            <li>Statkraft</li>
                        </ul>
                        <b>Sector Financiero, Seguro, AFP.</b>
                        <ul>
                            <li>AFP Prima</li>
                            <li>Banco Interbank (En Lima y Provincias)</li>
                            <li>Financiera Uno</li>
                            <li>Interfondos SAF</li>
                            <li>Interseguro</li>
                            <li>Inteligo</li>
                            <li>La Positiva Seguros Generales</li>
                            <li>La Positiva Vida</li>
                            <li>Servitebca – Vales electrónicos</li>
                        </ul>
                        <b>Sector Retail</b>
                        <ul>
                            <li>Café Britt</li>
                            <li>Tiendas Peruanas – Oeschle</li>
                            <li>Adriana Hoyos</li>
                            <li>Promart</li>
                            <li>Tottus</li>
                            <li>Sodimac</li>
                            <li>Maestro</li>
                        </ul>
                        <b>Sector Hotelero, Facility Management</b>
                        <ul>
                            <li>Casa Andina Hoteles</li>
                            <li>JW Marriott Lima</li>
                            <li>Hoteles Libertador</li>
                            <li>Pastelería San Antonio</li>
                            <li>Sodexo</li>
                        </ul>
                        <b>Sector Industrial, Agroindustrial</b>
                        <ul>
                            <li>Alicorp</li>
                            <li>Belcorp</li>
                            <li>British American Tobacco</li>
                            <li>Papelera del Sur S.A.</li>
                            <li>Phillips</li>
                            <li>San Fernando</li>
                        </ul>
                        <b>Sector Automotriz</b>
                        <ul>
                            <li>Grupo Gildemeister</li>
                        </ul>
                        <b>Sector Tecnologia </b>
                        <ul>
                            <li>Conastec</li>
                            <li>Hibu</li>
                        </ul>
                        <b>Sector Servicios Educativos y otros Servicios</b>
                        <ul>
                            <li>Fundación Carlos Rodríguez-Pastor Mendoza</li>
                            <li>Intertek Testing Services del Perú</li>
                            <li>Jamming Escuela de Desarrollo</li>
                            <li>TCOPIA</li>
                            <li>Transearch, Búsqueda Ejecutiva</li>
                            <li>Universidad Continental</li>
                        </ul>
                        <b>Sector Farmacéutico</b>
                        <ul>
                            <li>Tecnofarma</li>
                        </ul>
                        <p>Complementamos con algunos datos del servicio prestado:</p>
                        <!-- fin 2.	Principales clientes -->

                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

                        <!-- inicio 3.	Herramientas Informáticas: -->
                        <br>
                        <h5><b>3. Herramientas Informáticas:</b></h5>
                        <p>Actualmente contamos con un sistema propio denominado EFIPRO System de administración y control de postulaciones en línea, el que nos permite tener actualizada nuestra base de datos de candidatos evaluados, así como desarrollar el seguimiento y control a los servicios, optimizando el tiempo y recursos.</p>
                        <p>Estamos suscritos en Bumeran, Linkedln y Computrabajo; las cuales nos permiten atraer candidatos potenciales para los diferentes procesos que son parte de los servicios de Atracción y Selección.</p>
                        <!-- fin 3.	Herramientas Informáticas: -->
                    </div>

                    <div class="tab-pane fade" id="informatica" role="tabpanel" aria-labelledby="informatica-tab">

                        <!-- inicio 	Nuestras Oficinas (Infraestructura) ---->
                        <br>
                        <h5><b>4. Nuestras Oficinas (Infraestructura)</b></h5>
                        <br>
                        <div class="row">
                            <div class="col col-md-6">
                                <p><b>4.1  En Lima:</b> Nuestra oficina se ubica en la calle Manuel Fuentes 310 esquina con avenida Juan de Arona cuadra 3 en el distrito de San Isidro. </p>
                            </div>
                            <div class="col col-md-6">
                                <img src="{{ asset('img_eficiencia/lima.png') }}" alt="" style="width:40%;display:flex;margin:0 auto;">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col col-md-6">
                                <img src="{{ asset('img_eficiencia/salon.png') }}" alt="" style="width:60%;display:flex;margin:0 auto;">
                            </div>
                            <div class="col col-md-6">
                                <p>Contamos con un amplio local, idóneo para realizar nuestro trabajo. Tenemos 3 salones con cómodo mobiliario que permite desarrollar procesos grupales e individuales, assessment center y dinámicas grupales. Asimismo, contamos con oficinas privadas para la realización de entrevistas.</p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col col-md-6">
                                <p><b>4.2 En Arequipa:</b> En la Av. Ricardo Palma 202 ,Umacollo.</p>
                            </div>
                            <div class="col col-md-6">
                                <img src="{{ asset('img_eficiencia/salon_total.png') }}" alt="" style="width:60%;display:flex;margin:0 auto;">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col col-md-6">
                                <p><b>4.3 En Cusco:</b> Av. Fortunato L Herrera 207, Urb. Magisterio, 1ra etapa Cusco</p>
                            </div>
                            <div class="col col-md-6">
                                <img src="{{ asset('img_eficiencia/img_salon.png') }}" alt="" style="width:60%;display:flex;margin:0 auto;">
                            </div>
                        </div>
                        <!-- fin 	Nuestras Oficinas (Infraestructura) ---->

                    </div>

                    <div class="tab-pane fade" id="staf" role="tabpanel" aria-labelledby="staf-tab">

                        <!-- inicio staff -->
                        <br>
                            <h5><b>5. Staff</b></h5>
                            <br>
                            <p>Nuestro staff de profesionales, posee amplia experiencia laboral en diversos sectores de la economía lo que nos permite generar sinergia a fin de brindar a nuestros clientes el mejor servicio. </p>
                        <!-- fin stall -->
                    </div>

                </div>


                </div>
                <div class="col-12 col-md-1"></div>
            </div>
        </div>
    </div>

</div>

@include('maestro/footer_usuario')
@endsection
