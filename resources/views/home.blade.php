@extends('layouts.app')

@section('content')

<div class="d-none d-sm-none d-md-block">

@include('maestro.cabezera-persona')
</div>

<br>
<div class="container mb-4">
  <div class="row" style="border-radius: 12px;border: 1px solid #1A4A84;margin: 2px;">
    <div class="col-md-12">
      <b class="color-rojo">¡Bienvenido/a postulante! </b>
      <p style="color:black">Estamos encantados que se haya unido a nosotros. Para postular a las ofertas laborales, le solicitamos completar la siguiente información correspondiente a su Currículum Vitae.</p>
    </div>
  </div>
</div>
<div class="tab-content" id="pills-tabContent" styl>
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col offset-md-4">
          @if(isset($mensaje) && $mensaje != "")
          <div class="alert alert-success" role="alert" id="sucess">
              {{ $mensaje }}
          </div>
          @endif
        </div>
      </div>
    </div>
    @include('persona.hoja_vida')

  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
  <div class="tab-pane fade" id="pills-congiguracion" role="tabpanel" aria-labelledby="pills-congiguracion-tab">...</div>
</div>

<br>
<br>

@endsection
