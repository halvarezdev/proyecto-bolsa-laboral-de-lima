<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevas Vacantes</title>
    
</head>
<body >
<div style="max-width: 600px; margin:0 auto;padding:2% 5% 2% 5%;background-color: #e7e7e7">

<table >
	<tr style=" background-color: #fff;">
        <td >
            <div style="width: 35%;float:left;padding:2%;">
                <a href="#" style="text-decoration: none;">
                    <img src="{{ asset('/images/icono-logo.png') }}" style="width: 100%;">
                </a>
            </div>
            <div style="width: 55%;float:right;padding: 0;text-align:right;">
                <p style="margin-right: 10%;font-size:13px;">Hola <b style="color:#00a4d3;">{{ $datos_users['nombre'] }} </b> </p>
            </div>
        </td>
	</tr>

	<tr style="background-color: #003dc7;">
		<td style="padding-left:2%;padding-right:2%;">
            <p style="font-size: 14px;text-align: center; color:#fff;">Le mostramos las últimas vacantes públicas y privadas publicadas actualmente en <b>{{ $datos_users['ubi_descripcion'] }}</b>, relacionado con <b>{{ $datos_users['name_ocupacion'] }}</b></p>
		</td>
	</tr>
    
    @foreach($datos_users['pedidos'] as $key => $pedidos)
            <tr style="background-color:#fff;">
                <td style="padding:2% 5% 2% 5%;">
                @if($pedidos->procedencia == '1')
                <a href="{{ route('vacante.detalle',['id' => $pedidos->codigo, 'e' => '1']) }}" style="text-decoration: none;">
                @else
                <a href="{{ route('vacante.detalle.publica',['id' => $pedidos->codigo, 'e' => '1']) }}" style="text-decoration: none;">
                @endif
                    <div style="float:left; width:20%;margin-left:2%;">
                    <img src="{{ asset('public/storage/'.$pedidos->foto) }}" style="width: 100%;height: 90px;border: 2px solid #e1d9d9;">
                    </div>
                    <div style="float:left; width:65%;margin-left:5%;display: inline-block;text-align:left">
                        
                        <p style="font-size:12px;color:black;margin-top: 0px;">
                        @if($pedidos->procedencia == '1')
                        <b>Vacante Privada:</b><br>
                        @else
                        <b>Vacante Publica:</b><br>
                        @endif
                        <b style="font-size:14px;color:#ec5e38;">{{ $pedidos->descripcion }}</b><br>
                        {{ $pedidos->razon_social }}<br>
                        <b style="color:blue">ver oferta </b>
                        </p>
                        
                    </div>
                    <div style="float:right; width:5%;padding-top: 3.5%;">
                        <img src="{{ asset('/animacion/img-email.png') }}" alt="" style="width:100%;">
                    </div>
                </a>
                </td>
            </tr>
            @endforeach
   
            <tr style="background-color:#fff;">
                <td style="padding:2% 5% 2% 5%;">
                    <a href="{{ route('users.busqueda.inicio', ['e' => '1','ocupacion' => $datos_users['ocu_trabajo'], 'porciones_vac' => $datos_users['name_ocupacion'], 'ubicacion' => substr($datos_users['ubi_lugar'],0,2)]) }}"> 
                    <b>Hay {{ $datos_users['cantidad'] }} vacantes totales para ver </b>
                    </a>
                </td>
            </tr>
        
            <tr>
                <td style="padding:2% 5% 2% 5%;">
                <span>Este email se genera por sistema. Si tiene alguna duda o ayuda <a href="#">Contacta</a> con nosotros.</span>
                </td>
            </tr>
            <tr>
            <td style="padding:2% 5% 2% 5%;">
                    <font >Siguenos en nuestras Redes Sociales:</font>
                    <br><br>
                    <a href="https://www.facebook.com/Accede.empleos" class="redes_sociales" style="color:#E6E6E6;">
                    <img src="{{ asset('/animacion/facebook.png') }}" alt="" style="width: 40px;height: 40px;">
                    </a>
                    <a href="https://twitter.com/AccedeEmpleo" class="redes_sociales" style="color:#E6E6E6;">
                    <img src="{{ asset('/animacion/gorjeo.png') }}" alt="" style="width: 40px;height: 40px;">
                    </a>
                    <a href="https://instagram.com/accede.empleos" class="redes_sociales" style="color:#E6E6E6;">
                    <img src="{{ asset('/animacion/instagram.png') }}" alt="" style="width: 40px;height: 40px;">
                    </a>
                    <a href="https://www.youtube.com/channel/UC7mH9rKWlr8TNVLNJiJyeoA?view_as=subscriber" class="redes_sociales" style="color:#E6E6E6;">
                    <img src="{{ asset('/animacion/youtube.png') }}" alt="" style="width: 40px;height: 40px;">
                    </a>
            </td>
            </tr>
          
    </table>
</div>
</body>
</html>