@extends('layouts.app_detallepubli')

@section('content')

<style>
.fondo-imagen{
    background-image: url("{{ asset('storage/'.$banner_insti) }}"); 
    background-color: #cccccc;
    background-repeat: no-repeat, repeat;
    height: 250px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover; 
}

.hero-body{
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
}

.form{
  display: flex;
}

input{
  width: 400px;
  border-radius: 4px 0px 0px 4px;
  padding: 5px;
  border: 0;
  outline: none;
}

button{
  border-radius: 0px 4px 4px 0px;
  border: 0;
  padding: 7px 15px 7px 15px;
  cursor: pointer;
  color:#fff;
  background:{{ $subcolor_entidad }};
  
}

button:hover{
  color: #ffffff;
  background: {{ $subcolor_entidad }};
  
}
.search::placeholder{
 /* font-weight: bold;*/
  font-size:14px;
  color: {{ $entidad_color }}; 
}
.display-none{
    display:none;
}

p {
    margin-top: 0;
    margin-bottom: 0;
}
</style>


    <div class="container">
    <input type="hidden" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '0' }}" id="codigo_postulante">
    <a href="{{ route('register',['e' => (isset($entidad) ? $entidad : '1' )]) }}">
    <div class="fondo-imagen card" >
        <div class="card-body">
            <div class="row justify-content-between">
                <div class="col-7 col-sm-6 col-md-4" style="margin-top: 5.5%;">
                <img src="{{ asset('storage/'.$perfil_foto) }}" class="img-thumbnail" style="width: 150px; height: 150px;">
                </div>
            </div>
        </div>    
    </div>
    </a>
    <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            @foreach ($pedidos as $ped)
                <li class="breadcrumb-item"><a href="{{ route('welcome.entidad', ['e' => $entidad]) }}">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{ route('users.busqueda.inicio',['e' => (isset($entidad) ? $entidad : '1'),'empresa' => $ped->nombre] ) }}">{{ ucfirst(mb_strtolower($ped->nombre,'UTF-8')) }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ ucfirst(mb_strtolower($ped->nombre_especialidad,'UTF-8')) }}</li>
            @endforeach
            </ol>
        </nav>
    <div class="card" style="margin-top: 5px;">
        <div class="card-body">

            <div class="row">
            @foreach ($pedidos as $ped)
                <div class="col">
                    <input type="hidden" name="base_url" id="base_url" value="{{asset('')}}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <input type="hidden" value="{{ $ped->codigo }}" id="codigo_perdido">
                        <b style="color:#003dc7; font-size:18px;">
                            {{ mb_strtoupper($ped->nombre_especialidad,'UTF-8') }}
                        </b>
                        <p>{{ mb_strtoupper($ped->nombre,'UTF-8') }}</p>
                     
                        {{ $ped->num_convocatoria }}
                   
                </div>
               <div class="col-2 col-sm-2 col-md-1" style="text-align: right">
               
                @if(isset($favorito) && count($favorito) > 0)
                     <a href="#" class="icon_favorite" onclick="cancelarFavoriteVacante(this)">
                     <i class="fas fa-star" id="icon_fab"></i>
                     </a>
                @else
                    <a href="#" class="icon_favorite" onmouseover="favoriteHover(this)" onmouseout="favoriteNormal(this)" onclick="guardaFavoriteVacante(this)">
                    <i class="far fa-star" id="icon_fab"></i>
                    </a>
                @endif
              
                </div>
                @endforeach  
            </div>
            
        </div>
    </div>

    <div class="card" >
        <div class="card-body">
        @foreach($pedidos as $ped)
        <div class="row">
                <div class="col-12 col-sm-12 col-md-4">
                    <b class="parrafo-detalle">Fecha de Postulacion</b>
                    <p class="parrafo-detalle">{{ mb_strtoupper($ped->fech_referencia,'UTF-8') }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-1">
                    <b class="parrafo-detalle">Sueldo</b>
                    <?php  
                        setlocale(LC_MONETARY, 'en_PE');
                    ?>
                    <p class="parrafo-detalle">S/ {{ $ped->sueldo }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-1">
                    <b class="parrafo-detalle">Regimen</b>
                    <p class="parrafo-detalle">{{ mb_strtoupper($ped->regimen_laboral,'UTF-8') }}
                    </p>
                </div>
                <div class="col">
                    <b>Vacante</b>
                        <p>{{ $ped->cant_vacantes }}</p>
                </div>
                <div class="col">
                <b class="parrafo-detalle">Ubicaci&oacute;n</b>
                    <p class="parrafo-detalle">{{ $ped->ubi_descripcion }}
                </div>
                <div class="col text-center">
                    <br>
                    <a href="{{ $ped->url_vanc }}" target="_blank" class="btn btn-outline-primary btn-lg btn-block redondeado" style="color:#fff;width: 100%;padding: 5px 15px 5px 15px;font-size:12px;margin-top: -6%;background-color:{{ $entidad_color }}">
                        <i class="fas fa-external-link-alt"></i> &nbsp;&nbsp;Postula
                    </a> 
                   <p>Compartir<br>
                   <?php 
                                echo Share::currentPage('accede.com.pe')
                                ->facebook('{{ mb_strtoupper($ped->nombre_especialidad,"UTF-8") }}')
                                ->twitter('{{ mb_strtoupper($ped->nombre_especialidad,"UTF-8") }}')
                                ->linkedin('{{ mb_strtoupper($ped->nombre_especialidad,"UTF-8") }}')
                                ->whatsapp('{{ mb_strtoupper($ped->nombre_especialidad,"UTF-8") }}');
                            ?>
                   
                </div>
            </div>
   
        @endforeach    
        </div>
    </div>
            
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8" style="margin-top: 10px;">
            <div class="card" >
                <div class="card-body">
                    @foreach($pedidos as $ped)
                        <b class="parrafo-detalle">Especialidad</b>
                        <p class="parrafo-detalle">{{ mb_strtoupper($ped->nombre_especialidad,'UTF-8') }} </p>
                        <b class="parrafo-detalle">Especializaci&oacute;n</b>
                        <p class="parrafo-detalle">{{ mb_strtoupper($ped->especializacion,'UTF-8') }} </p>
                        <b class="parrafo-detalle">Formaci&oacute;n Academica</b>
                        <p class="parrafo-detalle">{{ mb_strtoupper($ped->formacion_academica,'UTF-8') }} </p>
                        <b class="parrafo-detalle">Experiencia</b>
                        <p class="parrafo-detalle">{{ mb_strtoupper($ped->experiencia,'UTF-8') }}</p>
                        <b class="parrafo-detalle">Conocimiento</b>
                        <p class="parrafo-detalle">{{ mb_strtoupper($ped->conocimiento,'UTF-8') }} </p>
                        <b class="parrafo-detalle">Competencias</b>
                        <p class="parrafo-detalle">{{ mb_strtoupper($ped->competencias,'UTF-8') }} </p>
                        <b class="parrafo-detalle">Detalle</b>
                        <br>
                     <!--   <p class="parrafo-detalle">{{ mb_strtoupper($ped->detalle,'UTF-8') }} </p>  -->
                        
                        <a style="padding-left: 0px;" target="_blank" href="{{ isset($ped->url_detalle) && $ped->url_detalle != null && $ped->url_detalle != '' ? $ped->url_detalle : '#' }}" class="btn btn-link">
                            Mayor detalle de la convocatoria aqu&iacute;
                        </a> 
                        
                       
                    @endforeach
                <br><br><br>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-12 col-md-4 text-center">
                    <a href="{{ $ped->url_vanc }}" target="_blank" class="btn btn-outline-primary btn-lg btn-block redondeado" style="color:#fff;width: 100%;padding: 5px 15px 5px 15px;font-size:12px;margin-top: -6%;background-color:{{ $entidad_color }}">
                        <i class="fas fa-external-link-alt"></i> &nbsp;&nbsp;Postula
                    </a> 
                    </div>
                </div>
                    
                </div>
            </div>    


        </div>
        <div class="col-12 col-sm-12 col-md-4" >
            <div class="card mb-3" style="max-width: 540px;margin-top: 10px;">
                <div class="card-header" style="text-align:center; background-color:#A4A4A4;color:#fff;font-size:14px;">Avisos Relacionados</div>

                @foreach($realcionados as $value)   
                <div class="row no-gutters" style="border-bottom: 2px solid #ebebeb;  ">
                    <div class="col-3 text-center">
                    <img src="{{ asset('storage/'.$value->insti_logo) }}" class="img-thumbnail" style="width: 70px; height: 70px;margin-top: 15%;margin-bottom: 15%;">
                    </div>
                    <div class="col-5">
                        <div class="card-body" style="padding: 0.8rem;">
                            <p class="card-title" style="font-size:12px;font-weight: bold;">{{ str_limit(ucfirst(strtolower($value->nombre_especialidad)),40) }}</p>
                            <p class="card-text" style="font-size:11px;">{{ str_limit($value->nombre,40) }}</p>
                            
                        </div>
                    </div>
                    <div class="col-3 text-center">
                    @if($value->cod_proce == '1')
                        <a href="{{ route('vacante.detalle',['id' => $value->codigo, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="btn btn-primary redondeado" style="padding: 5px 15px 5px 15px;font-size:10px;margin-top: 15%;margin-bottom: 15%;background-color:{{ $entidad_color }}">VER AVISO</a>
                    @else
                    <a href="{{ route('vacante.detalle.publica',['id' => $value->codigo, 'e' => (isset($entidad) ? $entidad : '1')] ) }}" class="btn btn-primary redondeado" style="padding: 5px 15px 5px 15px;font-size:10px;margin-top: 15%;margin-bottom: 15%;background-color:{{ $entidad_color }}">VER AVISO</a>
                  
                    @endif
                      </div>
                </div>
                @endforeach
                
             </div> 
             <div class="text-center">
                <a href="{{ route('users.busqueda.inicio',['e' => (isset($entidad) ? $entidad : '1')]) }}" class="btn btn-primary redondeado" style="width: 80%;padding: 1px 15px 1px 15px;font-size:12px;margin-top: -6%;background-color:{{ $entidad_color }}">VER TODOS LOS AVISOS</a>
             </div>
        </div>
    </div>



    </div>

    <script>
      function favoriteHover(value){
        $("#icon_fab").removeClass("far")
        //$("#icon_fab").removeClass("fas")
        $("#icon_fab").addClass("fas")
      //  $("#icon_fab").addClass("fa-star")
    }

    function favoriteNormal(value){
        $("#icon_fab").removeClass("fas")
        //$("#icon_fab").removeClass("fas")
        $("#icon_fab").addClass("far")
      //  $("#icon_fab").addClass("fa-star")
    }
    
function guardaFavoriteVacante(value){

    if($("#codigo_postulante").val() == '0'){
        alert("Debe estar Registrado para esta accion");
    }else{
        var _token=$("#csrf-token").val();
        base_url=$("#base_url").val();
        var codigo_pedido = $("#codigo_perdido").val();
        var codigo_postulante = $("#codigo_postulante").val();
        var urlData=base_url+"favorite/guardarVacante/"+codigo_postulante+"/"+codigo_pedido;
        $.ajax({
            type:'get',
            url:urlData,
            dataType:'json', 
        }).done(function( data, textStatus, jqXHR ){
            console.log(data);
            location.reload();
            favoriteHover();
        });
    }

}

function cancelarFavoriteVacante(){
    if($("#codigo_postulante").val() == '0'){
        alert("Debe estar Registrado para esta accion");
    }else{
        var _token=$("#csrf-token").val();
        base_url=$("#base_url").val();
        var codigo_pedido = $("#codigo_perdido").val();
        var codigo_postulante = $("#codigo_postulante").val();
        var urlData=base_url+"favorite/cancelarVacante/"+codigo_postulante+"/"+codigo_pedido;
        $.ajax({
            type:'get',
            url:urlData,
            dataType:'json', 
        }).done(function( data, textStatus, jqXHR ){
            console.log(data);
            location.reload();
            favoriteNormal();
        });
    }
}
</script>
    @include('maestro.footer_usuario')
    @endsection
