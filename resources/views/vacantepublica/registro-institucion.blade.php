<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
    <title>Registro Instituciones</title>
</head>
<body>

<br><br>
    <div class="container">
        <div class="card">
            <div class="card-header text-center" style="background-color:#003dc7;">
                <img src="{{ asset('entidad_logo/logo-login.png') }}" alt="">
                <h1 style="color:#fff;">Formulario Registro de Instituciones</h1>
            </div>
            <div class="card-body">
                @if($vista == 1)
                <form action="{{ route('save.institucion') }}" method="post">
                @csrf
                <input class="form-control form-control-lg" type="text" placeholder="Ruc Institución" id="ruc_institucion" name="ruc_institucion"> 
                <br>
                <input class="form-control form-control-lg" type="text" placeholder="Nombre" name="nom_institucion"><br>  
                <input class="form-control form-control-lg" type="text" placeholder="Descripcion" name="descri_institucion"><br>  
                <input class="form-control form-control-lg" type="text" placeholder="Url Institucion" name="url_institucion"><br>  
                <br>
                <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
                @else
                    @foreach($list_institucion as $value)
                    <form action="{{ route('editar.institucion') }}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $value->id }}" name="codigo_insti">
                        <input class="form-control form-control-lg" value="{{ $value->ruc }}" type="text" placeholder="Ruc Institución" id="ruc_institucion" name="ruc_institucion"> 
                        <br>
                        <input class="form-control form-control-lg" value="{{ $value->nombre }}" type="text" placeholder="Nombre" name="nom_institucion"><br>  
                        <input class="form-control form-control-lg" value="{{ $value->descripcion }}" type="text" placeholder="Descripcion" name="descri_institucion"><br>  
                        <input class="form-control form-control-lg" value="{{ $value->url_institucion }}" type="text" placeholder="Url Institucion" name="url_institucion"><br>  
                        <br>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        </form>
                    @endforeach
                @endif
                
                
            </div>
        </div>
    </div>
</body>

<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 
<script src="{{ asset('js/jquery.min.js') }}"></script>
</html>