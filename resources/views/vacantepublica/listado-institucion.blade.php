<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"> 
<!-- Ruta donde se encuentran todos los iconos : https://fontawesome.com/icons?d=gallery  -->
    <title>Listado Instituciones</title>
</head>
<body>

<br><br>
    <div class="container">
        <div class="card">
            <div class="card-header text-center" style="background-color:#003dc7;">
                <img src="{{ asset('entidad_logo/logo-login.png') }}" alt="">
                <h1 style="color:#fff;">Listado de Intitucion</h1>
            </div>
            <div class="card-body">
            <div class="row">
            <div class="col">
            <a href="{{ route('vacante.index') }}" class="btn btn-success">Registro Vacante</a>
            <a href="{{ route('registro.institucion') }}" class="btn btn-success">Registro Institucion</a>
            </div>
            </div>
            <br>
               <table class="table table-sm table-hover">
                    <thead>
                    <tr class="table-secondary">
                    <th>#</th>
                    <th>RUC</th>
                    <th>NOMBRE</th>
                    <th colspan="2">ACCIONES</th>
                    </tr>
                    </thead>
                <tbody>
                @foreach($list_institucion as $key => $value)
                    <tr>
                    <td>{{ $value->id  }}</td>
                    <td>{{ $value->ruc  }}</td>
                    <td>{{ $value->nombre  }}</td>
                    <td>
                    <a href="{{ route('update.institucion',['i' => $value->id]) }}" class="btn btn-link" style="color:blue;">
                    <i class="fas fa-pen"></i>
                    </a>
                    
                    </td>
                    <td>
                    <a href="{{ route('lista.vacantes',['v' => $value->id]) }}" class="btn btn-link" style="color:blue;">
                    <i class="fas fa-search"></i>
                    </a>
                    </td>
                    </tr>
                    @endforeach 
                </tbody>
                    
               </table>
                {{ $list_institucion->links() }}
            </div>
        </div>
    </div>
    <br><br>
</body>
    
<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 
<script src="{{ asset('js/jquery.min.js') }}"></script>
</html>