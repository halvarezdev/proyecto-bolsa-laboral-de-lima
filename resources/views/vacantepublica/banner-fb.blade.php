<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
    <title>Formato - Facebook</title>
    <style>
    .fondo{
        background-image: url("{{ asset($ruta) }}");
        width: 460px;
        height: 460px;
        background-color: #cccccc;
        background-repeat: no-repeat, repeat;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .banner{
        float:center; 
        border:solid 1px black;
        text-align:center;
        padding:15px;
        width: 650px;
        height: 650px;
    }

    .titulo{
        {{ $color_titulo }};
    }
    
    .text-stilo{
        color:#DF7401;
        font-size:14px;
    }
    .text-primero{
        {{ $color_primer_parrafo }}
    }
    .text-segundo{
        {{ $color_segu_parrafo }}
    }
    .text-tecero{
        {{ $color_terce_parrafo }}
    }

    .sub_titulo{
        {{ $color_subtitulo }}
    }
    </style>
</head>
<body>
    <div class="container"><br>

        <center>
        <div class="banner fondo">
            <br><br><br><br><br><br>    
           
            <div class="row">
                <div class="col">
                    <h1 class="titulo"><b>{{ $cant_vanc }} vacantes CAS</b></h1>
                </div>
            </div>

            <div class="row">
            <div class="col-1"></div>
                <div class="col">
                <?php  $texto_especialidad = ""; ?>
                @foreach($lis_vanc as $key => $item) 
                <?php $texto_especialidad = $texto_especialidad.$item->nombre_especialidad."-"; ?>
                @endforeach
                <?php
                    $especialidad_partida = explode("-", $texto_especialidad);
                ?>
                    <span class="text-primero">
                    {{ isset($especialidad_partida[0]) ? $especialidad_partida[0] : '' }},
                    {{ isset($especialidad_partida[1]) ? $especialidad_partida[1] : '' }},
                    {{ isset($especialidad_partida[2]) ? $especialidad_partida[2] : '' }},
                    {{ isset($especialidad_partida[4]) ? $especialidad_partida[4] : '' }},
                    {{ isset($especialidad_partida[5]) ? $especialidad_partida[5] : '' }}
                    </span>
                </div>
                <div class="col-1"></div>
            </div>
        <br>
            <div class="row">
            <div class="col-1"></div>
                <div class="col" style="text-align:left;">
                <b class="sub_titulo">PROFESIONES Y OFICIOS</b>
                <br>
                <?php 
                $palabra_clave = "COMUNICACIÓN,PERIODISMO,COMUNICACIÓN SOCIAL,GESTIÓN DE INNOVACIÓN SOCIAL,GERENCIA DE PROYECTOS,AGROINDUSTRIAL,CONTABILIDAD,SECRETARIADO EJECUTIVO,EDUCACIÓN,NUTRICIÓN,INGENIERÍA ALIMENTARIA,INGENIERÍA ECONÓMICA,SALUD PÚBLICA,POLÍTICAS SOCIALES,CIENCIAS DE LA SALUD,POLÍTICAS EDUCATIVAS,GESTIÓN DE LA INVERSIÓN SOCIAL,POLÍTICAS PÚBLICAS,GERENCIA SOCIAL,PSICOLOGÍA,ANTROPOLOGÍA,CIENCIAS DE LA COMUNICACIÓN,ADMINISTRACIÓN PÚBLICA,GEOGRAFÍA,SOCIOLOGÍA,ADMINISTRACIÓN DE NEGOCIOS INTERNACIONALES,ADMINISTRACIÓN,ABOGADO,ECONOMÍA,BIOLOGÍA,GESTIÓN AMBIENTAL,CIENCIAS AMBIENTALES,INGENIERÍA EN ELECTRÓNICA,TELECOMUNICACIONES,PUESTO,CARRERA,INFORMÁTICA,INGENIERÍA DE SOFTWARE,INGENIERÍA DE SISTEMAS,ESTADISTICA,CIENCIAS POLITICAS,ARTES,ANTROPOLOGIA,SOCIOLOGIA,ADMINISTRACION,EDUCACION,MUSICA,INGENIERÍA INDUSTRIAL,DERECHO,SECRETARIO,INGENIERÍA FORESTAL,SECRETARIO,ASISTENTE,ADMINISTRADOR,ECONOMISTA,CIENCIAS POLÍTICAS,CIENCIAS DE LA COMUNICACIÓN,GESTION SOCIAL,GESTION CULTURAL"; 
                $palarbras_prosciones = explode(",", $palabra_clave);
                $palabra_final = "";
                $texto = "";
                $datos = "";
                ?>
                @foreach($lis_vanc as $key => $item) 
                    
                 <?php
                 $texto = $texto." ".$item->formacion_academica;
                    
                 ?>
                @endforeach
                    <?php
                    $texto = mb_strtoupper($texto,'UTF-8');
                        for($i=0;$i<count($palarbras_prosciones);$i++){
                            $posicion = strpos($texto, $palarbras_prosciones[$i]);
                            if($posicion){
                                if($palabra_final != $palarbras_prosciones[$i]){
                                       $datos =  $datos.$palarbras_prosciones[$i].'-';
                                   }
                            }

                            $palabra_final = $palarbras_prosciones[$i];
                            
                        }
                   
                    $porciones = explode("-", $datos);
                ?>
                
            </div>
            </div>    
            <div class="row">
            <div class="col-1"></div>
                    <div class="col" style="text-align:left;padding-left: 0px;">
                        <ul>
                            <li class="text-segundo">{{ isset($porciones[0]) ? ucwords(mb_strtolower($porciones[0],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[1]) ? ucwords(mb_strtolower($porciones[1],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[2]) ? ucwords(mb_strtolower($porciones[2],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[3]) ? ucwords(mb_strtolower($porciones[3],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[4]) ? ucwords(mb_strtolower($porciones[4],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[5]) ? ucwords(mb_strtolower($porciones[5],'UTF-8')) : '' }}</li>
                            
                        </ul>
                    </div>
                    <div class="col" style="text-align:left;padding-left: 0px;">
                        <ul>
                            <li class="text-segundo">{{ isset($porciones[6]) ? ucwords(mb_strtolower($porciones[6],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[7]) ? ucwords(mb_strtolower($porciones[7],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[8]) ? ucwords(mb_strtolower($porciones[8],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[9]) ? ucwords(mb_strtolower($porciones[9],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[10]) ? ucwords(mb_strtolower($porciones[10],'UTF-8')) : '' }}</li>
                            <li class="text-segundo">{{ isset($porciones[11]) ? ucwords(mb_strtolower($porciones[11],'UTF-8')) : '' }}</li>
                            
                            
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-5" style="text-align:left;">
                    <b class="sub_titulo">REMUNERACIÓN</b>
                    <br>
                        <?php 
                        $entero = 0;  
                        $menor = 10000000000;
                        $mayor = 0;
                        ?>
                        @foreach($lis_vanc as $key => $item) 
                            <?php 
                                $entero = str_replace("S/.", "",$item->sueldo);
                                $entero = str_replace("S/ .", "",$entero);
                                $entero = str_replace("S/. ", "",$entero);
                                $entero = str_replace("S/", "",$entero);
                                $entero = str_replace(",", "",$entero);
                              
                                $resultado = (int) $entero;
                                if($menor > $resultado){
                                    $menor = $resultado;
                                
                                }
                                if($mayor < $resultado){
                                    $mayor = $resultado;
                                }
                            ?>
                        @endforeach
                        <span class="text-tecero">
                               DE S/{{ number_format($menor) }} A S/{{ number_format($mayor) }}
                               
                        </span>

                    </div>
                    <div class="col-6" style="text-align:left;">
                        <b class="sub_titulo">LUGAR DE LABORES</b>
                        <br>
                        <?php $ubigeo = ""; ?>
                        @foreach($lis_vanc as $key => $item) 
                                <?php
                                    $ubigeo = $ubigeo.$item->ubi_descripcion.',';
                                ?>
                        @endforeach
                        <?php
                            $array = explode(",",$ubigeo);
                            $nuevo_ubigeo = array_unique($array);
                            $variable=implode($nuevo_ubigeo,",");
                            $variable = str_replace(",", " ",$variable);
                        ?>
                        <p>{{ $variable }}</p>
                    </div>
                </div>


        </div>
        </center>

    </div>


<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 
<script src="{{ asset('js/jquery.min.js') }}"></script>
</body>
</html>