<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap-select.min.css') }}">
    <title>Vacantes Publicas</title>
</head>
<body>

<br><br>
    <div class="container">
        <div class="card">
            <div class="card-header text-center" style="background-color:#003dc7;">
                <img src="{{ asset('entidad_logo/logo-login.png') }}" alt="">
                <h1 style="color:#fff;">Formulario de Ofertas Laborales del Estado</h1>
            </div>
            <div class="card-body">
                @if(isset($vista) && $vista == '1')
                <form action="{{ route('update.vacante') }}" method="post">
                @else
                <form action="{{ route('save.vacante') }}" method="post">
                @endif   
                @csrf
                <span id="nombre_institucion" style="color: #101be9;font-size: 20px;"></span><br>

                
                @if(isset($vacante))
                @foreach($vacante as $value)
                
                
                <input type="hidden" value="{{ isset($value->codigo) ? $value->codigo : '' }}" name="codigo_vacante">
                <input value="{{ isset($value->ruc) ? $value->ruc : '' }}" class="form-control form-control-lg" type="text" placeholder="Ruc Institución" id="ruc_institucion" name="ruc_institucion" onblur="validarExisteRucVacante(this)">
                <span class="invalid-feedback" role="alert">
                    <strong id="msj_error_per"></strong>
                </span>
                <br>
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Nro de Convocatoria" name="num_convocatoria" value="{{ isset($value->num_convocatoria) ? $value->num_convocatoria : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Nombre de Especialidad" name="nom_especialidad" value="{{ isset($value->nombre_especialidad) ? $value->nombre_especialidad : '' }}"><br>
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Régimen Laboral" name="reginem_lab" value="{{ isset($value->regimen_laboral) ? $value->regimen_laboral : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Experiencia" name="experiencia" value="{{ isset($value->experiencia) ? $value->experiencia : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Formación Académica" name="form_academica" value="{{ isset($value->formacion_academica) ? $value->formacion_academica : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Especialización" name="especializacion" value="{{ isset($value->especializacion) ? $value->especializacion : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Conocimiento" name="conocimiento" value="{{ isset($value->conocimiento) ? $value->conocimiento : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Competencias" name="competencia" value="{{ isset($value->competencias) ? $value->competencias : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Detalle" name="detalle" value="{{ isset($value->detalle) ? $value->detalle : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" placeholder="Url Detalle" name="url_detalle" value="{{ isset($value->url_detalle) ? $value->url_detalle : '' }}"><br>
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Sueldo" name="sueldo" value="{{ isset($value->sueldo) ? $value->sueldo : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Cantidad Vacantes" name="cant_vacante" value="{{ isset($value->cant_vacantes) ? $value->cant_vacantes : '' }}"><br>
                <input class="form-control form-control-lg" type="text" placeholder="Fecha Postulacion" name="fech_referencia" value="{{ isset($value->fech_referencia) ? $value->fech_referencia : '' }}"><br>
                <input class="form-control form-control-lg" type="date" placeholder="Fecha Inicio" name="fech_inicio" value="{{ isset($value->fech_inicio) ? Carbon\Carbon::parse($value->fech_inicio)->format('Y-m-d') : '' }}"><br>
                <input class="form-control form-control-lg" type="date" placeholder="Fecha Limite" name="fech_postulacion" value="{{ isset($value->fecha_postula) ? Carbon\Carbon::parse($value->fecha_postula)->format('Y-m-d') : '' }}"><br>
                <input class="form-control form-control-lg" type="text" placeholder="Url" name="url_vanc" value="{{ isset($value->url_vanc) ? $value->url_vanc : '' }}"><br> 
                <select name="departamento" id="departamento" class="form-control form-control-lg">
                    <option value="">Departamento</option>
                    @foreach($ubigeo as $ubi)
                    <option value="{{ $ubi->ubi_codigo }}" {{ isset($value->ubigeo) && $value->ubigeo == $ubi->ubi_codigo ? 'selected' : '' }}>{{ $ubi->ubi_descripcion }}</option>
                    @endforeach
                </select>
                @endforeach
                @else
                <input value="{{ isset($value->ruc) ? $value->ruc : '' }}" class="form-control form-control-lg" type="text" placeholder="Ruc Institución" id="ruc_institucion" name="ruc_institucion" onblur="validarExisteRucVacante(this)">
                <span class="invalid-feedback" role="alert">
                    <strong id="msj_error_per"></strong>
                </span>
                <br>
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Nro de Convocatoria" name="num_convocatoria" value="{{ isset($value->num_convocatoria) ? $value->num_convocatoria : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Nombre de Especialidad" name="nom_especialidad" value="{{ isset($value->nombre_especialidad) ? $value->nombre_especialidad : '' }}"><br>
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Régimen Laboral" name="reginem_lab" value="{{ isset($value->regimen_laboral) ? $value->regimen_laboral : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Experiencia" name="experiencia" value="{{ isset($value->experiencia) ? $value->experiencia : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Formación Académica" name="form_academica" value="{{ isset($value->formacion_academica) ? $value->formacion_academica : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Especialización" name="especializacion" value="{{ isset($value->especializacion) ? $value->especializacion : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Conocimiento" name="conocimiento" value="{{ isset($value->conocimiento) ? $value->conocimiento : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Competencias" name="competencia" value="{{ isset($value->competencias) ? $value->competencias : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Detalle" name="detalle" value="{{ isset($value->detalle) ? $value->detalle : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" placeholder="Url Detalle" name="url_detalle" value="{{ isset($value->url_detalle) ? $value->url_detalle : '' }}"><br>
                <input class="form-control form-control-lg" type="text" placeholder="Sueldo" name="sueldo" value="{{ isset($value->sueldo) ? $value->sueldo : '' }}"><br>  
                <input class="form-control form-control-lg" type="text" style="text-transform:uppercase;" placeholder="Cantidad Vacantes" name="cant_vacante" value="{{ isset($value->cant_vacantes) ? $value->cant_vacantes : '' }}"><br>
                <input class="form-control form-control-lg" type="text" placeholder="Fecha Postulacion" name="fech_referencia" value="{{ isset($value->fech_referencia) ? $value->fech_referencia : '' }}"><br>
                <input class="form-control form-control-lg" type="date" placeholder="Fecha Inicio" name="fech_inicio" value="{{ isset($value->fech_inicio) ? Carbon\Carbon::parse($value->fech_inicio)->format('Y-m-d') : '' }}"><br>
               <input class="form-control form-control-lg" type="date" placeholder="Fecha Limite" name="fech_postulacion" value="{{ isset($value->fecha_postula) ? Carbon\Carbon::parse($value->fecha_postula)->format('Y-m-d') : '' }}"><br>
                 
                <input class="form-control form-control-lg" type="text" placeholder="Url Postula" name="url_vanc" value="{{ isset($value->url_vanc) ? $value->url_vanc : '' }}"><br> 
                <select name="departamento" id="departamento" class="form-control form-control-lg">
                    <option value="">Departamento</option>
                    @foreach($ubigeo as $ubi)
                    <option value="{{ $ubi->ubi_codigo }}" {{ isset($value->ubigeo) && $value->ubigeo == $ubi->ubi_codigo ? 'selected' : '' }}>{{ $ubi->ubi_descripcion }}</option>
                    @endforeach
                </select>
                @endif
                <br>
                <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
                <input type="hidden" id="base_url" value="{{ asset('') }}">
                
            </div>
        </div>
    </div>
</body>

<script src="{{ asset('js/bootstrap.min.js') }}" ></script> 
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>
function validarExisteRucVacante(codigo){

  base_url = $("#base_url").val();
  var urlajax = base_url + "valida/ruc/vacante/" + codigo.value;

  $.ajax({
      type: 'GET',
      url: urlajax,
      dataType: 'json',
      success: function(data) {
            if(data == "0"){
              $("#ruc_institucion").addClass('is-invalid');
              $("#msj_error_per").html('Esta Institucion no se encuentra registrado');
            }else{
                $.each(data, function(key, val) {
                    $("#email_users").addClass('is-valid');
                    $("#email_users").removeClass('is-invalid');
                    $("#nombre_institucion").html(val.nombre);   
                });
              
            }
      }
  });

}
</script>
</html>