<div class="row">
<div class="col-md-2"></div>
<div class="col-md-8">
<div class="card card-primary">
                    <div class="card-header">
                    {{$titulo}}
                     
                    </div>
                    <div class="card-body">
                    <div class="row">
                    <div class="col-md-4   ">
                        <label for="sede">Sede </label>
                        <?php echo \Form::select('sede', $row_sede,0,array('Class'=>'form-control-sm input-sm selected ','id'=>'sede')) ?>
                    </div>
                                    <div class="col-md-4  ">
                                        <label for="fecdes">Desde  </label>
                                        <input type="text" name="fecdes" id="fecdes" class=" form-control-sm"
                                              readonly="" value="{{$fecha}}" >
                                    </div>
                                    <div class="col-md-4  ">
                                        <label for="fechasta">Hasta </label>
                                        <input type="text" name="fechasta" id="fechasta" class=" form-control-sm"
                                             readonly="" value="{{$fecha}}" >
                                    </div>
                                     
                                </div>
                        <center class="mt-2">
                    <a class=" text-center btn btn-success"  href="#" onclick="previo_reporte('xsl','{{$tipoRe}}','{{$titulo}}','',{{$wit}})"><i class="fa fa-file-excel"></i> EXCEL</a>
                    <a class=" text-center btn btn-success"    href="#" onclick="previo_reporte('pdf','{{$tipoRe}}','{{$titulo}}','',{{$wit}})"><i class="fa fa-file-pdf"></i> PREVIO</a>
                    <a class=" text-center btn btn-danger"  onclick="addContenidoPrincipal('','citas/dashboard/reporte','dashboardreporte','page_contenedor',1)" href="#">SALIR</a>
</center>
                   
                </div>
                </div>
</div>
<div class="col-md-2"></div>
</div>
