<div class="card card-primary">
    <div class="card-header">
        <h4 class="card-title">Mantenimiento de Sedes</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-inline col-md-12">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="nuevoSede('')" title="Mostrar panel de sede">Nuevo</button>
            </div>
        </div>
        <div class="table-responsive mt-2">
            <table id="tblReporte" class="table table-bordered">
                <thead style="background: #f5f5f5">
                    <tr>
                        <th>#</th> 
                        <th>Descripcion</th> 
                        <th>Estado</th> 
                        <th>AddServicio</th>
                        <th>Editar</th>
                        <th>Anular</th>
                       
                    </tr>
                </thead>
                <tbody id="bodySede">
                    <?php echo $data['table']?>
                </tbody>
                <tfoot id="table-paginaicionsede">         
                    <?php echo $data['theadPagin']?>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title">SEDE</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">           
                <input type="hidden" id="hidValidSaveItem" value="">

                <div class="row" id="filaDatos">
                    <div class="col-md-6 form-group">
                        <label>Descripción <span style="color:red">*</span></label>
                        <input class="form-control input-sm" type="text" id="txtNombreSede">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" onclick="sedeguardar()" title="Guardar esta cita"><i class="fa fa-floppy" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>


<div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h4 class="modal-title">AGREGAR SERVICIO A SEDE</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">           
                <input type="hidden" id="hidValidSaveItem_sede" value="">

                <div class="row" id="filaDatos">
                    <div class="col-md-6 form-group">
                        <label>Servicio <span style="color:red">*</span></label>
                        <?php echo \Form::select('serviocsesde', $row_sede,0,array('Class'=>'form-control input-sm input-sm selected ','id'=>'serviocsesde',)) ?>
                                 
                    </div>
                    <div class="col-md-6 form-group"><br>
                    <button type="button" class="btn btn-success" onclick="sedeServguardar2()" title="Guardar esta cita"><i class="fa fa-floppy" aria-hidden="true"></i> Guardar</button>
                    </div>
                    <div class="col-md-12 form-group">

                    <div class="table-responsive mt-2" style="height:250px" >
            <table id="agregarpaginacionClients" class="table table-bordered">
                <thead style="background: #f5f5f5">
                    <tr>
                        <th>#</th> 
                        <th>Descripcion</th> 
                         
                       
                    </tr>
                </thead>
                <tbody id="bodySedeser">
                    
                </tbody>
                
            </table>
        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                
            </div>
        </div>
    </div>
</div>