
 <div class="card card-primary">
                        <div class="card-header">
                        <div class="row">
                        <div class="col-md-9"><h4 class="card-title">Atenciones Presenciales</h4> </div>
                        <div class="col-md-3"> </div>
                        </div>
                            
                        </div>

                        <div class="card-body">

           
            <div class="row">
                <div class="form-inline col-md-12">
                  
                    <div class="col-md-3 form-group ">
                                <label for="sede">Fecha Inicio </label>
                                <input type="text" id="fechaInicia" value="{{$fecha}}" onchange="filter_Data(1)" class="form-control input-sm">
                            </div>

                            <div class="col-md-3 form-group ">
                                <label for="servicio">Fecha Final: </label>
                                
                                <input type="text" id="fechaFin" value="{{$fecha}}" onchange="filter_Data(1)"  class="form-control input-sm">
                                   
                                     
                            </div>
                            <div class="form-group col-md-3 text-left">
                                <br>
                                <button class="btn btn-success mr-2" onclick="filter_Data(1)"> Buscar </button>
                                
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="mostrarAtencion('')" title="Mostrar panel de atenciòn">
                                    Atencion
                                </button>
                             </div>
                             
                </div>
            </div>

           <br>
           <div class="table-responsive">
                    <br>
                    <table id="tblReporte" class="table table-bordered">
                        <thead style="background: #f5f5f5">
                        <tr><th class="">#</th>
                                                        
                        <th>Servicio  </th>                                       
                        <th class="text-center">Fecha </th>                                       
                        <th class="text-center">Hora Asistencia </th>                                                                           
                        <th>N° Documento  </th>                                       
                        <th>Ciudadano(a)/Razón Social  </th>                                       
                        <th>Telefono  </th>                                       
                        <th>Correo  </th>                                       
                        <th>  </th>                                       
                        <th>  </th>                                       
                        </tr></thead> 
                        <tbody id="bodyReporte" >
              <?php echo $data['table']?>
            </tbody>
            <tfoot id="table-paginaicionorden_pago">         
              <?php echo $data['theadPagin']?>
            </tfoot>
                        </tbody>
                    </table>

                </div>     
                
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <div class="modal-content ">
        <div class="modal-header">
            <h4 class="modal-title">MARCAR ASISTENCIA</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">           
                        <input type="hidden" id="txtFormulario" value="atencion"> 	         	
                        <input type="hidden" id="txtInicioAten" value="{{$horaini}}">
                        <input type="hidden" id="txtFinAten" value="">
                        <input type="hidden" id="hidValidSaveItem" value="">
                        
                        <input type="hidden" id="txtUsername" value="avalqui">
                		 <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="txtTipoDocAte">Tipo de documento <span style="color:red">*</span> </label>
                                <?php echo \Form::select('txtTipoDocAten', $row_tipdocum,2,array('Class'=>'form-control input-sm input-sm selected ','id'=>'txtTipoDocAten',)) ?>
                                 
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Nª de Documento <span style="color:red">*</span></label>
                                <input class="form-control input-sm" type="text" id="txtDocIdentidadAten">
                            </div>
                            <div class="col-md-1 form-group text-right">
                                <br> 
                                <button class="btn btn-secondary" type="button" onclick="buscarDocumentoAtencion()"> <i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        
                         <div class="row" id="filaDatos">
                            <div class="col-md-4 form-group">
                                <label>Nombres <span style="color:red">*</span></label>
                                <input class="form-control input-sm" type="text" id="txtNombreAten" style="text-transform: uppercase;">
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Apellido paterno <span style="color:red">*</span></label>
                                <input class="form-control input-sm" type="text" id="txtApPaternoAten" style="text-transform: uppercase;">
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Apellido materno </label>
                                <input class="form-control input-sm" type="text" id="txtApMaternoAten" style="text-transform: uppercase;">
                            </div>
                        </div>
                        
                          <div class="row" id="filaDatosRuc" style="display: none;">
                            <div class="col-md-12 form-group">
                                <label>Razón Social <span style="color:red">*</span></label>
                                <input class="form-control input-sm" type="text" id="txtRazonAten" style="text-transform: uppercase;">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label>Telefono <span style="color:red">*</span></label>
                                <input class="form-control input-sm" type="text" id="txtTelefonoAten">
                            </div>
                            <div class="col-md-8 form-group">
                                <label>Correo </label>
                                <div class="input-group">
                                   
                                    <input class="form-control input-sm" type="email" id="txtCorreoAten">
                                </div>
                            </div>
                            <div class="col-sm-4 form-group ">
        <label>Sexo </label><br>
         <select  id="txtSexoAten" class="form-control input-sm">
         <option value="">--SELECCION--</option>
         <option value="1">MASCULINO</option>
         <option  value="2">FEMININO</option>
         </select>
    </div> 
    <div class="col-sm-4 form-group">
        <label>Fecha Nacimiento </label><br>
        <input type="text" id="txtFecNacimientoAten" value="{{$fecha}}" class="form-control input-sm">
          
    </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                        <?php echo \Form::select('sede', $row_sede,0,array('Class'=>'form-control input-sm input-sm selected ','id'=>'sede','onchange'=>' actualizarComboServPRE(this.value);')) ?>
                        </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="txtServicioAten">Servicio <span style="color:red">*</span> </label>
                                <select name="txtServicioAten" id="txtServicioAten" class="form-control input-sm">
                                    <option value="0">-- SELECCIONAR -- </option>
                                    
                                       
                                </select>
                            </div>
                        </div>
                        
                         <div class="row" id="divAsistencia">
                            <div class="col-sm-12 text-center">
                                <h4> <b>MARCAR ATENCION </b> </h4>
                            </div>

                            <div class="col-sm-12 text-center">
                                <label>Inicio </label><br>
                                <div id="divInicioAten"><div class="bigIcon">{{$horaini}}</div></div>
                            </div>

                           
                            <div class="col-sm-12 text-center" style="display:none;">
                                <label>Final </label><br>
                                <div id="divFinAten"> <button onclick="marcarFinAten(2)" class="btn btn-danger bigIcon" id="btnFinAtencion"><i class="fa fa-clock-o"></i> </button></div>
                            </div>

                           
                        </div>
                        
                    </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success" onclick="marcarguardar()" title="Guardar esta cita">
                            <i class="fa fa-floppy" aria-hidden="true"></i> Guardar</button>
                      
        </div>
    </div>

  </div>
</div>
