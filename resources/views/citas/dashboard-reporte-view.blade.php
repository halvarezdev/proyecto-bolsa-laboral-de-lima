<div class="row">
<div class="col-md-3"></div>
<div class="col-md-6">
<div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title"> Reportes y Estadísticas de Atenciónes en línea</h4>
                        <i class="fa fa-file-text pull-right"></i>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <ul class="col-12 list-group">
                                <li class="list-group-item bg-light"><h4 class="card-title"><a class="text-danger text-center"  onclick="addContenidoPrincipal('','citas/dashboard/rep1','dashboardreporte','page_contenedor',1)"  href="#"> Atenciónes reservadas </a></h4></li>

                                <li class="list-group-item bg-light"><h4 class="card-title"><a class="text-danger text-center"   onclick="addContenidoPrincipal('','citas/dashboard/rep2','dashboardreporte','page_contenedor',1)"  href="#"> Atenciónes atendidas </a></h4></li>

                                <li class="list-group-item bg-light"><h4 class="card-title"><a class="text-danger text-center"  onclick="addContenidoPrincipal('','citas/dashboard/rep3','dashboardreporte','page_contenedor',1)"  href="#"> Reporte detallado </a></h4></li>

                                <li class="list-group-item bg-light"><h4 class="card-title"><a class="text-danger text-center"   onclick="addContenidoPrincipal('','citas/dashboard/rep4','dashboardreporte','page_contenedor',1)"  href="#"> Estadisticas </a> </h4></li>

                            </ul>
                        </div>
                    </div>
                </div>
</div>
<div class="col-md-3"></div>
</div>
