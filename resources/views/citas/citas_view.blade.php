<?php
$v2=date('YmdHis');
?>
    @include('citas.layouts.cabezera')

<div style="background-image: url('{{ asset('fondo/citas1.png') }}'); height:  300px;background-repeat: no-repeat;
    margin-top: -300px;"></div>
    <section>

        <div class="container ">
            <div class="row mt-5">
                <div class="col-sm-12 remove-pad ">
                    <div class="line-width">
                        <div class="line-text">
                            Citas en línea
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="citaslinea">
                <div class="col-sm-12 services-ground">
                    <div id="noticias">
                        <h4>Bienvenido a nuestro sistema de citas en línea. Por favor siga las indicaciones para
                            realizar la reserva en el servicio y horario dónde desea recibir atención.</h4>
                        <hr>
                        <div class="row">
                            <div class="col-md-3 form-group ">
                                <label for="sede">Sede <span style="color:red">*</span> </label>
                                <?php echo \Form::select('sede', $row_sede,0,array('Class'=>'form-control  ','id'=>'sede','onchange'=>' actualizarComboServ(this.value);')) ?>

                            </div>

                            <div class="col-md-8 form-group ">
                                <label for="servicio">Servicio <span style="color:red">*</span> </label>
                                <div class="input-group">
                                    <select name="servicio" id="servicio" class="form-control"
                                        onchange="seleccionarNuevoServicio()">
                                          
                                    </select>
                                    <a href="javascript:void(0)" onclick="mostrarEnlaceServicio()"
                                        class="btn btn-warning input-group-addon"> Infórmate más </a>
                                </div>
                            </div>
                            <div class="col-md-1 text-left" style="display: none;" id="divLoader">
                                <label> &nbsp; </label><br>
                                <img src="{{asset('assets/citas/img/loader.gif')}}">
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-3 form-group ">
                                <div id="divMotivoCese" style="display: none">
                                    <label for="motivoCese">Motivo de Cese <span style="color:red">*</span> </label>
                                    <select name="motivoCese" id="motivoCese" class="form-control"
                                        onchange="mostrarRequisitoLiquidacion()">
                                        <option value="0">-- SELECCIONE --</option>

                                        <option value="1"> RENUNCIA </option>

                                        <option value="2"> TERMINO DE CONTRATO </option>

                                        <option value="3"> DESPIDO </option>

                                        <option value="4"> INSOLVENCIA EMPRESARIAL </option>

                                        <option value="5"> CIERRE DE CENTRO DE TRABAJO </option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-9 form-group ">
                                <div class="col-md-1 text-left" style="display: none;" id="divLoaderReqLiquidacione">
                                    <label> &nbsp; </label><br>
                                    <img src="{{asset('assets/citas/img/loader.gif')}}">
                                </div>
                                <div id="opcionMotivoCese" style="display: none">
                                    <p style="color: #525252">Para el cálculo de liquidación de beneficios sociales,
                                        deberá traer los siguientes documentos:</p>
                                    <div id="listaOpcionMotivoCese"></div>
                                    <p id="nota" class="text-danger" style="display: none;">Nota: <span
                                            id="descripcionNota"></span></p>
                                    <button type="button" class="btn btn-danger" id="btnMostrarHorario"
                                        onclick="mostrarHorarioDisponible()">Mostrar Horarios disponibles</button>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="servicio">Seleccione un horario disponible haciendo click sobre el
                                            cuadro <span style="color:red">*</span>
                                        </label></div>
                                    <div class="col-md-6">
                                        <div class="btn-group" style="float: right !important;">
                                            <button class="btn btn-default" title="Semana anterior"
                                                onclick="anteriorSemana()" id="btnSemanaAnterior">
                                                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Anterior
                                                semana</button>
                                            <button class="btn btn-default" title="Semana siguiente "
                                                onclick="siguienteSemana()" id="btnSemananaSiguiente">
                                                Siguiente semana <i class="fa fa-angle-double-right"
                                                    aria-hidden="true"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="col-md-12">
                                <div class="panel panel-default" id="divTblHorario">
                                    <div class="table-responsive" style="overflow-y:auto; max-height: 350px;">
                                        <table id="tblHorario" class="table table-hover">
                                            
                                        </table>
                                    </div>
                                    <div id="abajoFrm"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 form-group ">
                                        <label for="fecha">Fecha seleccionada </label>
                                        <input name="txtfecha" id="txtfecha" class="form-control" readonly="true"
                                            type="text" style="font-weight: bold; ">
                                        <input type="hidden" id="horario" name="horario" value="0">
                                        <input type="hidden" id="fecha" name="fecha" value="0">
                                        <input type="hidden" id="sexo" name="sexo">
                                        <input type="hidden" id="fecNacimiento" name="fecNacimiento">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4  form-group">
                                        <label for="tipoDoc">Tipo de documento <span style="color:red">*</span> </label>
                                        <select name="tipoDoc" id="tipoDoc" class="form-control"
                                            onchange="tipoDocumento()">
                                            <option value="1"> DNI </option>
                                            <option value="2"> PASAPORTE</option>
                                            <option value="3"> CARNET DE EXTRANJERÍA </option>
                                            <option value="4"> PERMISO TEMPORAL PERMANENCIA</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group ">
                                        <label for="numDoc">Número <span style="color:red">*</span> </label>
                                        <input tyep="text" name="numDoc" id="numDoc" class="form-control"
                                            placeholder="N° de documento" maxlength="8">
                                    </div>
                                    <div class="col-md-1  ">
                                        <label> &nbsp; </label><br>
                                        <button class="btn btn-default" title="Buscar DNI en la RENIEC"
                                            onclick="buscarDNI()" id="btnBuscar">
                                            <i class="fa fa-search" aria-hidden="true"></i> Buscar </button>
                                    </div>
                                    <div class="col-md-1 text-left" style="display: none;" id="divLoader2">
                                        <label> &nbsp; </label><br>
                                        <img src="{{asset('assets/citas/img/loader.gif')}}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 form-group ">
                                        <label for="nombre">Nombre <span style="color:red">*</span> </label>
                                        <input type="text" name="nombre" id="nombre" class="form-control"
                                            placeholder="Nombre" readonly="" maxlength="35">
                                    </div>
                                    <div class="col-md-4 form-group ">
                                        <label for="apePat">Apellido paterno <span style="color:red">*</span> </label>
                                        <input type="text" name="apePat" id="apePat" class="form-control"
                                            placeholder="Apellido paterno" readonly="" maxlength="25">
                                    </div>
                                    <div class="col-md-4 form-group ">
                                        <label for="apeMat">Apellido materno </label>
                                        <input type="text" name="apeMat" id="apeMat" class="form-control"
                                            placeholder="Apellido materno" readonly="" maxlength="25">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 form-group ">
                                        <label for="telefono">Número telefónico <span style="color:red">*</span>
                                        </label>
                                        <input type="text" name="telefono" id="telefono" class="form-control"
                                            placeholder="Teléfono" maxlength="15">
                                    </div>
                                    <div class="col-md-8 form-group ">
                                        <label for="correo">Correo electrónico <span style="color:red">*</span> </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"
                                                style="border-radius: 5px 0px 0px 5px !important;">@</span>
                                            <input type="email" name="correo" id="correo" class="form-control"
                                                placeholder="correo@electronico" maxlength="80">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default ">
                                    <div class="panel-body">
                                        <ul>
                                            <li>
                                                <i> Usted es responsable de la información aquí ingresada.</i>
                                            </li>
                                            <li>
                                                <i style="color:red; font-size: 120%"> El tiempo máximo de tolerancia es
                                                    de 5 minutos.
                                                    Pasado este plazo, se procederá a cancelar su cita. </i></li>
                                        </ul>
                                        <input type="checkbox" id="chkMailing" name="chkMailing" checked="checked">
                                        <label for="chkMailing" style="font-size: 120%"> Deseo recibir boletines de
                                            información sobre éste servicio. </label>
                                        <br>
                                        <input type="checkbox" id="chkConforme" name="chkConforme"
                                            onchange="habilitarBotonGuardar(this)">
                                        <label for="chkConforme" style="font-size: 120%"> He leído los términos y
                                            procedo a realizar la reserva. </label>
                                        <br><br>
                                        <center>
                                            <button class="btn btn-sigamos" style="transform: scale(0.9);"
                                                id="btnGuardar" onclick="registrarCita()" disabled="true"> Reservar cita
                                            </button>
                                            <img src="{{asset('assets/citas/img/loader.gif')}}" id="imgLoaderReservar"
                                                style="display: none">
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!------  End Content ---->
    @include('citas.layouts.footer')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/component/sweetalert/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/citas/js/citas.js') }}?ng={{$v2}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/default.js') }}?ng={{$v2}}"></script>

</body>

</html>