
 <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title">
                            Control de asistencia
                            </h4>
                        </div>

                        <div class="card-body">

           
            <div class="row">
                <div class="form-inline col-md-12">
                    <input type="hidden" id="idUnidad" name="idUnidad" value="249">
                    <input type="hidden" id="username" name="username" value="avalqui">

                    <div class="col-md-3 form-group ">
                                <label for="sede">Sede <span style="color:red">*</span> </label>
                                <?php echo \Form::select('sede', $row_sede,0,array('Class'=>'form-control input-sm selected ','id'=>'sede','onchange'=>' actualizarComboServ(this.value);')) ?>

                            </div>

                            <div class="col-md-8 form-group ">
                                <label for="servicio">Servicio <span style="color:red">*</span> </label>
                                
                                    <select name="servicio" id="servicio" class="form-control"
                                        onchange="seleccionarNuevoServicio()">
                                          
                                    </select>
                                     
                            </div>
                            <div class="col-md-1 text-left" style="display: none;" id="divLoader">
                                <label> &nbsp; </label><br>
                                <img src="{{asset('assets/citas/img/loader.gif')}}">
                            </div>
                </div>
            </div>

           <br>
                    <div class="card card-primary">
                         

                        <div class="card-body">
                            <div class="row form-group">
                                <div class="col-md-8">
                                     
                                    <div id="divFecha" class="form-inline"  >
                                        <button class="btn btn-secondary" title="Hoy" onclick="activarHoy()">Hoy</button>
                                        &nbsp;&nbsp;
                                        <div class="btn-group">
                                            <button class="btn btn-secondary" title="Anterior"
                                                onclick="anteriorFecha()">
                                                <i class="fa fa-angle-double-left" aria-hidden="true"></i> </button>
                                            <button class="btn btn-secondary" title="Siguiente"
                                                onclick="siguienteFecha()">
                                                <i class="fa fa-angle-double-right" aria-hidden="true"></i> </button>
                                            &nbsp;&nbsp;
                                        </div>
                                        <input type="text" class="form-control" onchange="actualizarHorarioFecha()"
                                            id="fecha" value="{{$fecha}}" name="fecha" readonly>
                                        
                                    </div>
                                </div>    
                            </div>

                            
                                    <table id="tblHorarioFecha"
                                        class="table  table-bordered table-hover"
                                        >
                                        <thead>
                                            <tr>
                                                <th class="text-center"> Hr. programado </th>
                                                <th class="text-center"> Hr. asistencia </th>
                                                <th class="text-center"> Persona citada </th>
                                                <th class="text-center"> Servicio </th>
                                                <th class="text-center"> Acciones </th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodyHorarioFecha">
                                            <tr>
                                                <td class="text-center" colspan="6"> <i>Seleccione una ventanilla</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                

                            <div class="row">
                                <div class="hrcitado col-sm-2 text-center" style="font-size: 90%" id="divReservados">
                                    Reservados </div>
                                <div class="hrrealizado col-sm-2 text-center" style="font-size: 90%" id="divRealizados">
                                    Atendidos </div>
                                <div class="hrlibre col-sm-2 text-center" style="font-size: 90%" id="divLibres"> Libres
                                </div>
                                <div class="hrbloqueado col-sm-2 text-center" style="font-size: 90%" id="divBloqueados">
                                    Bloqueados </div>
                                <div class="hrabandonado col-sm-2 text-center" style="font-size: 90%"
                                    id="divAbandonados"> Abandonados </div>
                            </div>

                        </div>
                    </div>
                
</div>
</div>

