
    <footer>
        <div class="container-fluid footer">
            <div class="row">
                <div class="col-sm-12">
                    <p class="text-center">Gobierno Regional del Callao <br>
                      
                                       GRC © 2020 Todos los derechos reservados 
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <div id="abajo"></div>


    <div class="modal fade" id="modalMsg" role="dialog" style="margin-top: 250px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="modal-title">Titulo</h4>
                </div>
                <div class="modal-body" id="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalServicioMsg" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title" id="modal-servicio-title"></h4>
                </div>
                <div class="modal-body" id="modal-servicio-body">



                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-sm-12 remove-pad">
                                <div class="line-width">
                                    <div class="line-text">
                                        Virtual - Consultas al Trabajador y Empleador
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 services-ground" style="min-height: 50px;">
                                <div class="col-sm-12">
                                    <div class="details-text" style="padding-top: 15px">

                                        <div id="contenidoServicio">
                                            <p>El área de consultas es brindar un servicio de asesoría y orientación a
                                                los trabajadores y empleadores en el conocimiento, cumplimiento y
                                                aplicación de la normatividad laboral vigente. Esto se puede realizar de
                                                manera directa, telefónica, y a través de otros medios adecuados y la
                                                difusión de la legislación laboral y seguridad social.<b></b></p>
                                            <p><br></p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    