<?php
$v2=date('YmdHis');
?>
<!DOCTYPE html>
<!-- saved from url=(0040)http://www.jovenesproductivos.gob.pe/cv/ -->
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Required meta tags -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-3.3.2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-all.min.css') }}">

    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">


    <link rel="stylesheet" type="text/css" href="{{asset('assets/citas/css/style.css')}}?ng={{$v2}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/component/sweetalert/sweetalert.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/style.css') }}"   />
    <title>Extranet</title>

</head>

<body>
  
<div class="animationload" style="display: none;">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <input type="hidden" value="{{env('APP_URL')}}" id="base_url">
    <input type="hidden" id="_token" value="{{ csrf_token() }}">
    <header>
      
        <div class="container hidden-sm hidden-md hidden-lg">
            <div class="row">
                <div class="col-xs-12 ">

                </div>

                <div class="col-xs-12">

                </div>
                <div class="col-xs-12 text-right">
                                    </div>
                <div class="col-xs-12">

                </div>
            </div>
        </div>
        

        <div class="container hidden-xs">
            <div class="row">
                <!-- <div class="col-md-12 header">
                    <div class="col-sm-5 col-sm-offset-7 col-md-4 col-md-offset-8">
                        <div id="imaginary_container">

                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        &nbsp;

                    </div>
                    <div class="col-sm-5 col-sm-offset-3 col-md-4 col-md-offset-5"> </div>
                    <div class="col-sm-8 col-sm-offset-4">
                         <p class="title-red" style="text-decoration:none!important;">&nbsp;</p>
                    </div>
                    <div class="col-sm-5 col-sm-offset-7">
                         <p class="title-black" style="text-decoration:none!important;">&nbsp;</p>
                    </div>
                    <div class="col-sm-12">
                        <p class="text-center"><img src="{{asset('assets/citas/img/arrow-down.png')}}" class="arrow-down"
                                alt=""></p>
                    </div>
                </div> -->
            </div>
        </div>
    </header>

    <!------  Begin Content ---->

    <nav style="background-image: url('{{ asset('fondo/citas.png') }}'); height:  300px;background-repeat: no-repeat;
background-attachment: fixed">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav">

                        <li class="active"><a
                                href="#citaslinea"><span><i
                                        class="fa fa-calendar" aria-hidden="true"></i> Atenciones en Línea</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>