 
  <center>
<section style="max-width: 600px !important;">
            <div class="container"> 
                
                <div class="row" style="    margin-right: -15px;
    margin-left: -15px;" id="divPrinter">
                    <div class="col-sm-12 services-ground">
                        <div id="program">
                            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;">
                                <div class="hidden">
                                    <img src="{{asset('assets/citas/img/logo.jpg')}}">
                                </div>
                                <div class="panel panel-default" style="    border-color: #ddd;    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);">
                                    <div class="panel-heading" style="color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;"><h3 class="text-center"><span>Detalle de la cita reservada</span></h3></div>
                                    <div class="panel-body " id="divCitaDetalle"> 
                                        <input type="hidden" id="txtIdCita" value="{{$data->cta_id}}">
                                        <div class="row" style="    margin-right: -15px;
    margin-left: -15px;" style="margin-right: -15px;
    margin-left: -15px;">
                                            <div class="col-md-12" style="position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;">
                                                <label style="display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;" for="servicio"> 
                                                    Servicio solicitado: </label>
                                                <input type="url" name="servicio" class="form-control confirm" style="display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;    cursor: not-allowed;
    background-color: #eee;
    opacity: 1;" readonly="true" style="text-transform:uppercase" value="{{$data->srv_name}}">
                                                <br>
                                            </div>
                                        </div>
                                        <div class="row" style="    margin-right: -15px;
    margin-left: -15px;">
                                            <div class="col-md-12" style="position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;">
                                                <label style="display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;" for="fechaHoraCita">Fecha y hora de atención: </label>
                                                <input type="text" name="fechaHoraCita" class="form-control confirm" style="display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;    cursor: not-allowed;
    background-color: #eee;
    opacity: 1;" readonly="true" value="{{$data->cta_fectext}}">
                                                <br>
                                            </div>
                                        </div>
 
                                        <div class="row" style="    margin-right: -15px;
    margin-left: -15px;">
                                            <div class="col-md-12" style="position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;">
                                                <label style="display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;" for="documento">Documento de identificación: </label>
                                                <input type="text" name="documento" class="form-control confirm" style="display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;    cursor: not-allowed;
    background-color: #eee;
    opacity: 1;" readonly="true" value="{{$data->numero_documento}}">
                                                <br>
                                            </div>  
                                        </div>
                                        <div class="row" style="    margin-right: -15px;
    margin-left: -15px;">
                                            <div class="col-md-12" style="position: relative;
min-height: 1px;
padding-right: 15px;
padding-left: 15px;">
                                                <label style="display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;" for="persona">Nombre y Apellidos: </label>
                                                <input type="text" name="persona" value="{{$data->name}} {{$data->ape_pat}} {{$data->ape_mat}}" class="form-control confirm" style="display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;    cursor: not-allowed;
    background-color: #eee;
    opacity: 1;" readonly="true" style="text-transform:uppercase">  
                                                <br>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="panel-footer text-left" style="padding: 10px 15px;
    background-color: #f5f5f5;
    border-top: 1px solid #ddd;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;">
 
                                        <span style="color:red;">* Recuerde ser puntual, una tardanza de 5 minutos invalida su cita. </span>
                                        <br>
                    
                                        <span style="color:green;">* Si desea imprimir esta página puede hacerlo, pero no es necesario. </span>
                                        <br><br>
                                      
                                            
                                                
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        </center>