
 <div class="card card-primary">
                        <div class="card-header">
                        <div class="row">
                        <div class="col-md-9"><h4 class="card-title">Atenciones virtuales</h4> </div>
                        <div class="col-md-3"><h4 class="card-title"><form name="form_reloj">
<input type="text" name="reloj" size="10" style="color: #fff;
    font: bold 14pt 'Lucida Console';
    background: #be0a0a;
    padding: 10px;
    border: solid #ccc 3px;
    border-radius: 10px;
    vertical-align: middle;
    text-align: center;
    width: 95%;" onfocus="window.document.form_reloj.reloj.blur()">
</form></h4> </div>
                        </div>
                            
                        </div>

                        <div class="card-body">

           
            <div class="row">
                <div class="form-inline col-md-12">
                    <input type="hidden" id="idcita" name="idcita" value="">
                    <input type="hidden" id="idHorario" name="idHorario" value="">
                    <input type="hidden" id="fechaaa" name="fechaaa" value="">
                    <input type="hidden" id="tipofon" name="tipofon" value="">
                    
                    <div class="col-md-4 form-group ">
                                <label for="sede">Sede <span style="color:red">*</span> </label>
                                <?php echo \Form::select('sede', $row_sede,0,array('Class'=>'form-control ','id'=>'sede','onchange'=>' actualizarComboServ(this.value);')) ?>

                            </div>

                            <div class="col-md-7 form-group ">
                                <label for="servicio">Servicio <span style="color:red">*</span> </label>
                                
                                    <select name="servicio" id="servicio" class="form-control"
                                        onchange="seleccionarNuevoServicio()">
                                          <option>--SELECCIONE--</option>
                                    </select>
                                     
                            </div>
                            <div class="col-md-1 text-left" style="display: none;" id="divLoader">
                                <label> &nbsp; </label><br>
                                <img src="{{asset('assets/citas/img/loader.gif')}}">
                            </div>
                </div>
            </div>

           <br>
                    <div class="card card-primary">
                         

                    <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="servicio">Seleccione un horario disponible haciendo click sobre el
                                            cuadro <span style="color:red">*</span>
                                        </label></div>
                                    <div class="col-md-6">
                                        <div class="btn-group" style="float: right !important;">
                                            <button class="btn btn-default" title="Semana anterior"
                                                onclick="anteriorSemana()" id="btnSemanaAnterior">
                                                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Anterior
                                                semana</button>
                                            <button class="btn btn-default" title="Semana siguiente "
                                                onclick="siguienteSemana()" id="btnSemananaSiguiente">
                                                Siguiente semana <i class="fa fa-angle-double-right"
                                                    aria-hidden="true"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="panel panel-default" id="divTblHorario">
                                    <div class="table-responsive" style="overflow-y:auto; max-height: 350px;">
                                        <table id="tblHorario" class="table table-hover">
                                            
                                        </table>
                                    </div>
                                    <div id="abajoFrm"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">MARCAR ASISTENCIA</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <div class="row" id="divAsistencia">
      <div class="col-sm-4 text-center">
        <label>Sexo </label><br>
         <select  id="sexo" class="form-control">
         <option value="">--SELECCION--</option>
         <option value="1">MASCULINO</option>
         <option  value="2">FEMININO</option>
         </select>
    </div> 
    <div class="col-sm-4 text-center">
        <label>Fecha Nacimiento </label><br>
        <input type="text" id="fechaInicia" value="{{$fecha}}" class="form-control">
          
    </div> 
    <div class="col-sm-4 text-center">
        <label>Estado </label><br>
        <?php echo \Form::select('estadoAtencion', $row_estadoatencion,0,array('Class'=>'form-control input-sm selected ','id'=>'estadoAtencion')) ?>
          
          
    </div>                    

                            <div class="col-sm-4 text-center">
                                <label>Inicio </label><br>
                                <input type="hidden" id="iniciofecha">
                                <div id="divInicioAsis"> <button onclick="marcarInicioAsis(1)" class="btn btn-danger bigIcon"><i class="far fa-clock"></i> </button></div>
                            </div>

                            <div class="col-sm-4 text-center">
                                <label>Final </label><br>
                                <input type="hidden" id="finfecha">
                                <div id="divFinAsis"> <button onclick="marcarInicioAsis(2)" class="btn btn-danger bigIcon"><i class="far fa-clock"></i> </button></div>
                            </div>
                            
                            <div class="col-sm-4 text-center">
                            <input type="hidden" id="AbandonoAsis">
                                <label>Abandonado </label><br> 
                                <div id="divAbandonoAsis"> <button onclick="marcarInicioAsis(3)" class="btn btn-warning bigIcon"><i class="fa fa-power-off"></i> </button></div>
                            </div>
                            
                            <div class="col-sm-12 text-center">
                                <div id="divLoader" style="display: none;">
                                    <img src="/extranet/admin/resources/mtpe/img/loader.gif">
                                </div>
                            </div>                            

                            <div class="col-sm-12">
                                <label>Observaciones</label>

                                <div id="divObservacion" style="display: block;">
                                    <textarea class="form-control" rows="3" id="txtObservaciones"> </textarea>
                                </div>

                                 
                            </div>
                            <div class="col-sm-12">
                                <label>Canal de Atención</label>
     
    <?php echo \Form::select('canalaten', $row_canal,0,array('Class'=>'form-control input-sm selected ','id'=>'canalaten')) ?>
                     
                                 
                            </div>
                        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-success" onclick="marcarInicioAsis(100)" title="Guardar esta cita">
                            <i class="fa fa-floppy" aria-hidden="true"></i> Guardar</button>
                        <!-- <button type="button" class="btn btn-info" onclick="enviarCorreo()" title="Notificar esta cita por correo">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> Notificar</button> -->
      </div>
    </div>

  </div>
</div>
