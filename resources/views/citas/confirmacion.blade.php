@include('citas.layouts.cabezera')
<?php
$v2=date('YmdHis');
?>
<section>
            <div class="container"> 
                <div class="row">
                    <div class="col-sm-12 remove-pad">
                        <div class="line-width">
                            <div class="line-text">
                                Citas en línea
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="divPrinter">
                    <div class="col-sm-12 services-ground">
                        <div id="program">
                            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;">
                                <div class="hidden">
                                    <img src="{{asset('assets/citas/img/logo.jpg')}}">
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h3 class="text-center"><span>Detalle de la cita reservada</span></h3></div>
                                    <div class="panel-body " id="divCitaDetalle"> 
                                        <input type="hidden" id="txtIdCita" value="{{$data->cta_id}}">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="servicio"> 
                                                    Servicio solicitado: </label>
                                                <input type="url" name="servicio" class="form-control confirm" readonly="true" style="text-transform:uppercase" value="{{$data->srv_name}}">
                                                <br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="fechaHoraCita">Fecha y hora de atención: </label>
                                                <input type="text" name="fechaHoraCita" class="form-control confirm" readonly="true" value="{{$data->cta_fectext}}">
                                                <br>
                                            </div>
                                        </div>
 
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="documento">Documento de identificación: </label>
                                                <input type="text" name="documento" class="form-control confirm" readonly="true" value="{{$data->numero_documento}}">
                                                <br>
                                            </div>  
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="persona">Nombre y Apellidos: </label>
                                                <input type="text" name="persona" value="{{$data->name}} {{$data->ape_pat}} {{$data->ape_mat}}" class="form-control confirm" readonly="true" style="text-transform:uppercase">  
                                                <br>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="panel-footer text-left">
 
                                        <span style="color:red;">* Recuerde ser puntual, una tardanza de 5 minutos invalida su cita. </span>
                                        <br>
                    
                                        <span style="color:green;">* Si desea imprimir esta página puede hacerlo, pero no es necesario. </span>
                                        <br><br>
                                        <center>
                                            
                                                <button type="button" class="btn btn-default btn-sm" onclick="enviarCorreo(this);">
                                                    <span class="glyphicon glyphicon-envelope"></span> Enviar por correo
                                                </button>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            
                                            <button type="button" class="btn btn-default btn-sm" onclick="printDiv('divPrinter')">
                                                <span class="glyphicon glyphicon-print"></span> Imprimir
                                            </button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </section>
        
@include('citas.layouts.footer')
<script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/component/sweetalert/sweetalert.js') }}"></script>
  
    <script type="text/javascript" src="{{ asset('assets/js/default.js') }}?ng={{$v2}}"></script>
    <script>
        function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    var base_url=get('base_url').value
    var contenido = "<center><img src='"+base_url+"assets/img/logo.jpg'> ";
    contenido += "<div style='width: 80%;'>" + printContents + "</div>";
   
    contenido += "</center>";
    document.body.innerHTML = contenido;
    window.print();
    document.body.innerHTML = originalContents;
} function enviarCorreo(lik) {
    $(lik).attr('disabled','disabled')
        var idCita = document.getElementById("txtIdCita").value;

        var _token=get('_token').value;
        var base_url=get('base_url').value;
        $.ajax({
            type: "POST",
            url: base_url+"citas/enviarCorreo",
            data: {idCita: idCita,_token:_token, tipo: '1'},
            dataType: 'html',
            success: function (response) {
               
                if (response.status== "0")
                swal("Se detecto un error en el envío. Vuelva a intentarlo por favor.", "", "info")

                else{
                $(lik).removeAttr('disabled')
                swal("El correo se envío satisfactoriamente.", "", "success")
                  
                    }

            },
            error: function (xhr, status, error) {
                $(lik).removeAttr('disabled')

            swal("Se detecto un error en el envío. Vuelva a intentarlo por favor.", "", "info")
 
            }
        });
    }

        </script>
</body>

</html>
