@extends('layouts.app')

@section('content')

<div class="d-none d-sm-none d-md-block">

@include('maestro.cabezera-persona')
</div>
<br>

<div class="tab-content" id="pills-tabContent" styl>
  <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
   
    <!-- home -->

  </div>
  <div class="tab-pane fade " id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    
<!-- Postulaciones -->
  </div>

  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
    
<!-- convocatoria -->
  </div>
</div>
 <div class="tab-pane fade show active" id="pills-congiguracion" role="tabpanel" aria-labelledby="pills-congiguracion-tab">
   @include('persona.vista_configuracion')
 </div>
</div>

@endsection
