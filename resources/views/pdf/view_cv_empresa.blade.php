<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>.::FORMATO HOJA DE VIDA::.</title>
  <style type="text/css">
    body table tbody tr td{
      font-size: 12px;
      font-family: Arial, Helvetica, sans-serif;
    }
    .titulo{
      font-size: 14px;
      color: #58ACFA;
    }
    .color-fondo{
      color:#6E6E6E;
      background-color: #E6E6E6;
    }
    .color-fondo-titulo{
      background-color:#3281d9;
      color:#fff;
    }

   .color-resultado{
     color:#000000;
   }
  </style>
</head>
  <body>
    <table width="100%">
      <tr >
        <td style="width:120px;">
        <?php
        $type ='';
        $data_img ='';
        ?>
        
        <img src="{{ asset('/public/storage/'.$arrayDataPersona->foto_post) }}" alt="photo" class="rounded-circle img-thumbnail" width="100" height="110">
        </td>
        <td >
          <p >
            <span style="font-size:20px;color:#3281d9">{{ucwords($arrayDataPersona->name)}} 
            {{ucwords($arrayDataPersona->ape_pat)}} 
            {{ucwords($arrayDataPersona->ape_mat)}}</span>
            <br>
            <span style="font-size:16px;color:#3281d9;">
            @foreach($formacion_superior as $key => $formacion)
            @if($key == '0')
            {{ strtoupper($formacion->nombre) }}
            @endif
            @endforeach 
            </span>
            <br><br>
            <p style="font-size:11px;" ALIGN="justify">{{ucwords($arrayDataPersona->presentacion)}}</p>
          </p>
        </td>
        
      </tr>
     
    </table>
<br><br>
  <table width="100%">
    <thead>
      <tr>
        <td colspan="2" class="titulo color-fondo-titulo">1.&nbsp;&nbsp;INFORMACIÓN PERSONAL</td>

      </tr>
    </thead>
    <tbody>
      <tr >
        <td class="color-fondo" style="width:180px;"> Tipo de Documento </td>
        <td>DNI</td>
      </tr>
      <tr>
        <td class="color-fondo">Número de Documento </td>
        <td>{{$arrayDataPersona->numero_documento}}</td>
      </tr>
      <tr>
        <td class="color-fondo">Género </td>
        <td>
        @if($arrayDataPersona->sexo == "1")
        Masculino
        @elseif($arrayDataPersona->sexo == "2")
        Femenino
        @else
        -
        @endif
        </td>
      </tr>
      <tr>
        <td class="color-fondo">Fecha de Nacimiento: </td>
        <td>{{$arrayDataPersona->fecha_naci}}</td>
      </tr>
      <tr>
        <td class="color-fondo">Dirección: </td>
        <td>{{$arrayDataPersona->direccion}}</td>
      </tr>
      <tr>
        <td class="color-fondo">Número de Teléfono: </td>
        <td>{{$arrayDataPersona->telefono}}</td>
      </tr>
      <tr>
        <td class="color-fondo">Correo Electrónico: </td>
        <td>{{$arrayDataPersona->email}}</td>
      </tr>
    </tbody>
  </table>

<br><br>

<table width="100%">
    <thead>
      <tr>
        <th colspan="5" class="titulo color-fondo-titulo">2.&nbsp;&nbsp;INFORMACIÓN ACADÉMICA</th>

      </tr>
     
    </thead>
    <tbody>
       <tr>
        <td colspan="5" style="background:#BDBDBD">
          FORMACIÓN BÁSICA
        </td>
      </tr>
      <tr>
        <td class="color-fondo" colspan="2">Centro de Estudios</td>
        <td class="color-fondo">Tipo de Formación</td>
        <td class="color-fondo">Fecha Inicio</td>
        <td class="color-fondo">Fecha Fin</td>
      </tr>
        @foreach($formacion_basica as $key => $formacion)
                                        <tr>
                                            <td colspan="2">{{ $formacion->perbas_centroestudio }}</td>
                                            
                                            <td>{{ $formacion->grad_descripcion }}</td>
                                            <td>{{ $formacion->perbas_fechaini  }}</td>
                                            <td>{{ $formacion->perbas_fecafin }}</td>
                                           
                                        </tr>   
                                    @endforeach 
      <tr>
        <td colspan="5" style="background:#BDBDBD">
          FORMACIÓN SUPERIOR
        </td>
        <tr>
        <td class="color-fondo">Centro de Estudios</td>
        <td class="color-fondo">Tipo de Formación</td>
        <td class="color-fondo">Especialidad</td>
        <td class="color-fondo">Fecha Inicio</td>
        <td class="color-fondo">Fecha Fin</td>
      </tr>
       @foreach($formacion_superior as $key => $value)
                                        <tr>
                                            <td>{{ $value->otro_centro }}</td>
                                            <td>{{ $value->grad_descripcion }}</td>
                                            <td>{{ $value->nombre }}</td>
                                            <td>{{ $value->fecha_inicio  }}</td>
                                            <td>{{ $value->fecha_fin }}</td>
                                    @endforeach

      </tr>    

      <tr>
        <td colspan="5" style="background:#BDBDBD">
          POSGRADOS Y PROGRAMAS DE ESPECIALIZACIÓN 
        </td>
        <tr>
        <td class="color-fondo">Centro de Estudios</td>
        <td class="color-fondo">Tipo de Formación</td>
        <td class="color-fondo">Especialidad</td>
        <td class="color-fondo">Fecha Inicio</td>
        <td class="color-fondo">Fecha Fin</td>
      </tr>
       @foreach($posgrado as $key => $value)
                                        <tr>
                                          <td>{{ $value->otro_centro }}</td>
                                          <td>{{ $value->grad_descripcion }}</td>
                                            <td>{{ $value->especialidad_id }}</td>
                                            <td>{{ $value->fecha_inicio  }}</td>
                                            <td>{{ $value->fecha_fin }}</td>
                                        </tr>   
                                    @endforeach
    </tbody>
  </table>

<br><br>

  <table width="100%">
    <thead>
      <tr>
        <th colspan="2" class="titulo color-fondo-titulo">3.&nbsp;&nbsp;CONOCIMIENTOS DE INFORMÁTICA</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="color-fondo">Nombre</td>
        <td class="color-fondo">Nivel</td>
      </tr>
@foreach($computaciones as $key => $value)
                                        <tr>
                                            <td>{{ $value->MAESC_DESCRIPCION }}</td>
                                            <td>
                                            @if($value->compu_nivel == '1')
                                            Basico
                                            @elseif($value->compu_nivel == '2')
                                            Medio
                                            @else
                                            Avanzado
                                            @endif
                                            </td>
                                    @endforeach


    </tbody>
  </table>
      
      <br><br>

  <table width="100%">
    <thead>
      <tr>
        <th colspan="2" class="titulo color-fondo-titulo">4.&nbsp;&nbsp;IDIOMA</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="color-fondo">Nombre</td>
        <td class="color-fondo">Nivel</td>
      </tr>
   @foreach($idiomas as $key => $value)
                                        <tr>
                                            <td>{{ $value->MAESC_DESCRIPCION }}</td>
                                            <td>
                                            @if($value->idioma_nivel == '1')
                                            Basico
                                            @elseif($value->idioma_nivel == '2')
                                            Medio
                                            @else
                                            Avanzado
                                            @endif
                                            </td>
                                    @endforeach

    </tbody>
  </table>


      <br><br>

  <table width="100%">
    <thead>
      <tr>
        <th colspan="3" class="titulo color-fondo-titulo">5.&nbsp;&nbsp;ESTUDIOS ADICIONALES</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="color-fondo">Tipo</td>
        <td class="color-fondo">Centro</td>
        <td class="color-fondo">Detalle</td>
      </tr>
      
 @foreach($estudiosadi as $key => $value)
                                        <tr>
                                            <td>
                                                @if($value->estu_tipo == "1")
                                                    Curso
                                                @endif
                                                @if($value->estu_tipo == "2")
                                                    Seminario
                                                @endif
                                                @if($value->estu_tipo == "3")
                                                    Taller
                                                @endif
                                                @if($value->estu_tipo == "4")
                                                    Conferencia
                                                @endif
                                                @if($value->estu_tipo == "5")
                                                    Congreso
                                                @endif
                                                @if($value->estu_tipo == "6")
                                                    Charla
                                                @endif
                                                @if($value->estu_tipo == "7")
                                                    Diplomado
                                                @endif
                                            </td>
                                            <td>{{ $value->estu_cetro }}</td>
                                            <td>{{ $value->estu_nombre }}</td>
                                            
                                    @endforeach
    </tbody>
  </table>


    <br><br>

  <table width="100%">
    <thead>
      <tr>
        <th colspan="5" class="titulo color-fondo-titulo">6.&nbsp;&nbsp;EXPERIENCIA LABORAL</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="color-fondo">Empresa</td>
        <td class="color-fondo">Puesto</td>
        <td class="color-fondo">Funciones</td>
        <td class="color-fondo">Fecha Inicio</td>
        <td class="color-fondo">Fecha Fin</td>
      </tr>
      
         @foreach($experiencia_liest as $key => $value)
                                        <tr>
                                            <td>{{ $value->nom_empresa }}</td>
                                            <td>{{ ($value->id_ocupacion == "000000" ? $value->otro_cargo : $value->nombre ) }}</td>
                                            <td>{{ $value->funciones }}</td>
                                            <td>{{ $value->fech_inicio }}</td>
                                            <td>{{ $value->fech_fin }}</td>
                                    @endforeach

    </tbody>
  </table>


</body></html>