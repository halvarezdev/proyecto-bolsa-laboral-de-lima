<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntaPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('disponibilidad',10)->nullable();
            $table->string('declaracion',10)->nullable();
            $table->string('derechos_laborales',50)->nullable();
            $table->string('pretencion',100)->nullable();
            $table->timestamps();
            $table->unsignedInteger('ficha_id')->nullable();
            $table->foreign('ficha_id')->references('id')->on('fichapersona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_persona');
        $table->dropForeign(['ficha_id']);
        $table->dropColumn('ficha_id');
    }
}
