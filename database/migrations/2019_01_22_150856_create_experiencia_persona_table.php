<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienciaPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencia_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sector',50)->nullable();
            $table->string('empresa',100)->nullable();
            $table->string('rubro',50)->nullable();
            $table->string('area',100)->nullable();
            $table->string('ocupacion',50)->nullable();
            $table->string('tipo_contrato',50)->nullable();
            $table->string('persona_cargo',10)->nullable();
            $table->string('funciones_cargo',500)->nullable();
            $table->string('sueldo',500)->nullable();
            $table->timestamp('fecha_ini')->nullable();
            $table->timestamp('fecha_fin')->nullable();
            $table->string('motivo_cese',500)->nullable();
            $table->string('nombre_jefe',500)->nullable();
            $table->string('cargo_jefe',500)->nullable();
            $table->string('telefono',500)->nullable();
            $table->timestamps();
            $table->unsignedInteger('ficha_id')->nullable();
            $table->foreign('ficha_id')->references('id')->on('fichapersona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencia_persona');
        $table->dropForeign(['ficha_id']);
        $table->dropColumn('ficha_id');
    }
}
