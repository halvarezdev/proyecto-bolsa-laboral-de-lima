<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdiomacompupersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idioma_compu_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string('compu',50)->nullable();
            $table->string('nivel_compu',50)->nullable();
            $table->string('idioma',50)->nullable();
            $table->string('nivel_idioma',50)->nullable();
            $table->timestamps();
            $table->unsignedInteger('ficha_id')->nullable();
            $table->foreign('ficha_id')->references('id')->on('fichapersona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idioma_compu_persona');
        $table->dropForeign(['ficha_id']);
        $table->dropColumn('ficha_id');
    }
}
