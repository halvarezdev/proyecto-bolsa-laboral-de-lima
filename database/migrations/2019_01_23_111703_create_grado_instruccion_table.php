<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradoInstruccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gradoinstruccion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grad_descripcion',50)->nullable();
            $table->integer('grad_flag')->nullable();
            $table->string('grad_pcd',5)->nullable();
            $table->string('grad_orderby',5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gradoinstruccion');
    }
}
