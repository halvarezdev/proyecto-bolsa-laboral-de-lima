<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCiiuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciiu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('V_CODSEC',10)->nullable();
            $table->string('V_CODCIIU',10)->nullable();
            $table->string('V_DESCIIU',500)->nullable();
            $table->string('I_FLGSUNMIN',10)->nullable();
            $table->string('V_CIIU_SUNAT',10)->nullable();
            $table->string('V_CODCIIUV3',10)->nullable();
            $table->string('V_CODCIIUV4',10)->nullable();
            $table->string('V_DESCIIUV4',500)->nullable();
            $table->string('V_CODCIIUINEIV4',500)->nullable();
            $table->string('V_DESCIIUINEIV4',500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciiu');
    }
}
