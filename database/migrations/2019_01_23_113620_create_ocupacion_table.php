<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcupacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocupaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ocup_codigo',50)->nullable();
            $table->string('ggrupo',5)->nullable();
            $table->string('grupo',5)->nullable();
            $table->string('sgrupo',5)->nullable();
            $table->string('apertura',5)->nullable();
            $table->string('nombre',500)->nullable();
            $table->string('status',5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocupaviones');
    }
}
