<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_documento',5)->nullable();
            $table->string('numero_documento',30)->unique();
            $table->string('email',30)->nullable();
            $table->string('name',30)->nullable();
            $table->string('ape_pat',30)->nullable();
            $table->string('ape_mat',30)->nullable();
            $table->string('ubigeo',100)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('direccion',100)->nullable();
            $table->string('sexo',10)->nullable();
            $table->string('telefono',20)->nullable();
            $table->string('estado_civil',5)->nullable();
            $table->string('jefe_hogar',5)->nullable();
            $table->string('tiene_discapcidad',5)->nullable();
            $table->string('discapacidad',20)->nullable();
            $table->string('discapacidad_texto',100)->nullable();
            $table->timestamps();  
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
