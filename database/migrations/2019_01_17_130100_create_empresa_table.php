<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_documento',10)->nullable();
            $table->string('numero_documento',20)->nullable();
            $table->string('razon_social',50)->nullable();
            $table->string('descripcion',200)->nullable();
            $table->string('rubro',50)->nullable();
            $table->string('sitio_web',100)->nullable();
            $table->string('direccion',200)->nullable();
            $table->string('ubigeo',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
