<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion',100)->nullable();
            $table->string('denominacion',50)->nullable();
            $table->string('num_vacantes',5)->nullable();
            $table->string('estudios_formales',200)->nullable();
            $table->string('especialidad',100)->nullable();
            $table->string('grado',50)->nullable();
            $table->string('otro_conocimiento',400)->nullable();
            $table->string('experiencia_laboral',50)->nullable();
            $table->string('experiencia_nivel',10)->nullable();
            $table->string('zona',10)->nullable();
            $table->string('contacto',10)->nullable();
            $table->string('personal_cargo',10)->nullable();
            $table->string('cantidad',5)->nullable();
            $table->string('tareas',400)->nullable();
            $table->string('modalidades',50)->nullable();
            $table->string('horario_trabajo',10)->nullable();
            $table->string('horario',50)->nullable();
            $table->string('remuneracion',5)->nullable();
            $table->string('moneda',10)->nullable();
            $table->string('ubigeo',50)->nullable();
            $table->string('horas_extras',10)->nullable();
            $table->string('movilidad',10)->nullable();
            $table->string('refrigerio',10)->nullable();
            $table->string('bonificacion',10)->nullable();
            $table->string('seguro',10)->nullable();
            $table->string('comisiones',10)->nullable();
            $table->string('discapacidad',10)->nullable();
            $table->string('radio_discapacidad',10)->nullable();
            $table->string('puesto',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido');
    }
}
