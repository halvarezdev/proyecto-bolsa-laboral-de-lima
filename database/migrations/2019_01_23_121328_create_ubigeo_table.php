<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbigeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubigeo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ubi_codigo')->nullable();
            $table->integer('ubi_coddpto')->nullable();
            $table->integer('ubi_codprov')->nullable();
            $table->integer('ubi_coddist')->nullable();
            $table->string('ubi_descripcion',100)->nullable();
            $table->integer('ubi_estado')->nullable();
            $table->string('ubi_zona',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubigeo');
    }
}
