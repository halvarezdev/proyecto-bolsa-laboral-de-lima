<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCentroestudioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centroestudios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('CESTP_CODIGO',10)->nullable();
            $table->string('CESTC_DESCRIPCION',10)->nullable();
            $table->string('CESTC_TIPO',10)->nullable();
            $table->string('CESTC_ESTADO',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centroestudios');
    }
}
