<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichaPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fichaPersona', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('presentacion')->nullable();
            $table->integer('trabajo_celular')->nullable();
            $table->integer('trabajo_correo')->nullable();
            $table->integer('politica')->nullable();
            $table->string('nivel_estudio',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fichaPersona');
    }
}
