<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Maestro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maestros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('MAESP_CODIGO1',10)->nullable();
            $table->string('MAESP_CODIGO2',10)->nullable();
            $table->string('MAESP_CODIGO',10)->nullable();
            $table->string('MAESC_DESCRIPCION',100)->nullable();
            $table->string('MAESC_ABREVIATURA',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maestros');
    }
}
