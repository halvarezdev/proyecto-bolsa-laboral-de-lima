<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educPersonas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('especialidad',50)->nullable();
            $table->string('centro_estudio',50)->nullable();
            $table->string('otro_centro_estudio',50)->nullable();
            $table->timestamp('fecha_incio')->nullable();
            $table->timestamp('fecha_termino')->nullable();
            $table->string('tipo_educ',10)->nullable();
            $table->timestamps();
            $table->unsignedInteger('ficha_id')->nullable();
            $table->foreign('ficha_id')->references('id')->on('fichapersona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educPersonas');
        $table->dropForeign(['ficha_id']);
        $table->dropColumn('ficha_id');

    }
}
