$(document).ready(function(){
    $(document).on('click', '#tblReporte .pagination a',function(event){
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var page=$(this).attr('href').split('page=')[1];
        filter_Data(page,'S');
    });
    $("#fechaInicia").datepicker({changeMonth: true,changeYear: true})
    $("#fechaFin").datepicker({changeMonth: true,changeYear: true});
    //$("#agregarpaginacionClients").tableHeadFixer({'foot' : true,'head' : true}); 
   
});

function mostrarAtencion(id) {
    $("#myModal").modal('hide')
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $("#cargar_modal_nueo").html('')
     $("#cargar_final").css('display','none')
     
    $.ajax({
        type: "POST",
        url: base_url+"citas/grupoprece",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'HTML',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
    $("#cargar_modal_nueo").html(response)

        	$("#myModal").modal()
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
    });
}
function marcarguardar() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var tipdoc=get('txtTipoDocAten').value;   
    var dni=get('txtDocIdentidadAten').value;   
    var nombre=get('txtNombreAten').value;   
    var appat=get('txtApPaternoAten').value;   
    var apmat=get('txtApMaternoAten').value;   
    var tefon=get('txtTelefonoAten').value;   
    var correo=get('txtCorreoAten').value;   
    var sede=get('sede').value;   
    var sexo=get('txtSexoAten').value;   
    var fechna=setDate(get('txtFecNacimientoAten').value);   
    var serv=get('txtServicioAten').value;   
    var horaini=get('txtInicioAten').value;   
    var horafin=get('txtFinAten').value; 
    if(horafin==''){
        horafin='';
    }  
    var idcita=get('hidValidSaveItem').value;   
    var fecha=setDate(get('txtFechaAten').value);   
    var user=get('id_user_for').value;
    var perf=get('id_user_perf').value;
    var obser=get('txtObsercaAtenPrece').value;
    var fechatexto=get('txtFechaAten').value;
    var dias=["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    var dt = new Date();
    var text = dias[dt.getUTCDay()];
    fechatexto=text+' '+fechatexto+", "+horaini+" - "+horafin
    $.ajax({
        type: "POST",
        url: base_url+"citas/grupoprece",
        data: {fechatexto:fechatexto,obser:obser,perf:perf,user:user,fecha:fecha,sexo:sexo,fechna:fechna,idcita:idcita,horafin:horafin,horaini:horaini,
            serv:serv,sede:sede,correo:correo,tefon:tefon,apmat:apmat,appat:appat,nombre:nombre,
            dni:dni,tipdoc:tipdoc,_token:token,method:'save_pres'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){  

            $(".animationload").css({display:'none'}); 
            if(response['status']==2){
                alert("Inicie Session: se realizo una actualización")
                return false;
            }
            $("#hidValidSaveItem").val(response['datacita']['idcita'])
            $("#myModal").modal('hide')
            addContenidoPrincipal('','citas/dashboard/listaprecen','dashboardcitaspre','page_contenedor',1)
            $("#cargar_final").css('display','block')
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
    });    
}
function buscarDocumentoAtencion() {
    var base_url=$("#base_url").val()
    var dni=$("#txtDocIdentidadAten").val()
    var _token=get('_token').value;
     
                $.ajax({
                    type: "POST",
                    url: "http://ferialaboral.regioncallao.gob.pe/consulta.php?dni="+dni,
                    data: {_token:_token,dni: dni.trim()},
                    dataType: 'JSON',
                    beforeSend: function(){
                        $(".animationload").css({display:'block'});
                       },
                    }).done(function( data, textStatus, jqXHR ){ 
                        $(".animationload").css({display:'none'});
                    
                        if(data['coResultado']=='0000'){
                            document.getElementById("txtApPaternoAten").value = data['datosPersona'].apPrimer;
                            document.getElementById("txtApMaternoAten").value = data['datosPersona'].apSegundo;
                            document.getElementById("txtNombreAten").value = data['datosPersona'].prenombres;
                           }else{
                            habilitarLecturaDatoPersonal(false);
                            swal("En este momento no se pudo validar el DNI. Por favor ingrese sus datos personales2.", "", "info")
                           }
                         } ).fail(function(jqXHR, ajaxOptions, thrownError){
                            $(".animationload").css({display:'none'});
                        })
            
}
function marcarInicioAsisP(tipo) {
    var hoy = new Date();
    var horaini=$("#divInicioAten div").html()
    var finfecha=get('txtFinAten').value;
    if(finfecha!=''){
        return false;
    }
    var fechatexto=get('txtFechaAten').value;
    var AbandonoAsis='';
    var texto='';
    var tipofon=4;
    var hora =hourwithAMPM();
    
    grt=$("#btnFinAtencion").html();
    if(grt!=''){

    }
    $("#btnFinAtencion").html(hora); 
         
    finfecha=hora;
     
    var dias=["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    var dt = new Date();
    var text = dias[dt.getUTCDay()];
      fechatexto=text+' '+fechatexto+", "+horaini+" - "+finfecha
    var base_url=get('base_url').value;
    var sede=get('sede').value;
    var servicio=get('txtServicioAten').value;
    var _token=get('_token').value;
    var idcita=get('hidValidSaveItem').value;
    var idHorario=0;
   
    $.ajax({
        type: "POST",
        url: base_url+"citas/grupoprece",
        data: {estadoAtencion:5,method:'actualizarData',fechatexto:fechatexto,idcita:idcita,texto:texto,finfecha:finfecha,AbandonoAsis:AbandonoAsis,tipofon:tipofon,horaini:horaini,tipo:tipo,_token:_token,servicio:servicio,idHorario:idHorario,sede:sede},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'}); 
            get('txtFinAten').value=hora;
            filter_Data(1)
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, reporte al administrador."); 
        });
}
function hourwithAMPM() {
    var d = new Date();
    var ampm = (d.getHours() >= 12) ? "PM" : "AM";
    var hours = (d.getHours() >= 12) ? d.getHours()-12 : d.getHours();
    if(hours<=9){
        hours='0'+hours
    }
    minute=d.getMinutes();
    if(minute<=9){
        minute='0'+minute
    }
    return hours+" : "+minute+" "+ampm;
    
    }
function actualizarComboServPRE() {
	 
    var sede = document.getElementById("sede").value;
    if (sede !== '0') {
        $("#divLoader").show();
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
        var user=$("#id_user_for").val();
        var perf=$("#id_user_perf").val();
        $.ajax({
            type: "POST",
            url: base_url+"citas/traerServiciosv3",
            data: {perf:perf,user:user,sede: sede,_token:token},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0")
                poblarComboServPRE(response.listaConfiguracionServicio,response.listaConfiguracionObs);
                else
                    document.getElementById("txtServicioAten").innerHTML = "<option value='0'>-- NINGUN SERVICIO PARA MOSTRAR --</option>";
                
              
                 
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
            
    }
}
function poblarComboServPRE(rpta,rptav2) {
     
    document.getElementById("txtServicioAten").innerHTML = contenido;
    var contenido = "<option value='0'>-- SELECCIONE --</option>";
    $( rpta ).each(function( index ,fila) {
        contenido += "<option value='" + fila.codigo + "'>" + fila.names + "</option>";
    }); 
    $( rptav2 ).each(function( index ,fila) {
        contenido += "<option value='" + fila.codigo + "'>" + fila.names + "</option>";
    }); 
    document.getElementById("txtServicioAten").innerHTML = contenido;
}

function atende_cita(tbn,id,sede,servio) {
    var base_url=$("#base_url").val();
        var token=$("#_token").val();
        $("#myModal").modal('hide')
        $("#cargar_modal_nueo").html('')
        $.ajax({
            type: "POST",
            url: base_url+"citas/grupoprece",
            data: {id:id,_token:token,method:'nuevo'},
            dataType: 'HTML',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                $("#cargar_modal_nueo").html(response)

                $("#myModal").modal()
               /* if (response.status !== "0"){
                   
                     get('txtTipoDocAten').value=response['obj']['tipo_documento'];   
                     get('txtDocIdentidadAten').value=response['obj']['numero_documento'];  
                     get('txtNombreAten').value=response['obj']['name'];  
                    get('txtApPaternoAten').value=response['obj']['ape_pat'];     
                    get('txtApMaternoAten').value=response['obj']['ape_mat'];     
                    get('txtTelefonoAten').value=response['obj']['telefono'];     
                    get('txtCorreoAten').value=response['obj']['email'];     
                    get('sede').value=response['obj']['sde_id'];   // btnFinAtencion 
                    get('txtSexoAten').value=response['obj']['sexo'];     
                    get('txtFecNacimientoAten').value=setDate(response['obj']['fechana']);    
                     get('txtServicioAten').value=response['obj']['srv_id'];   
                     get('txtInicioAten').value=response['obj']['cta_horaini'];   
                     get('txtFinAten').value=response['obj']['cta_horafin'];  
                    get('hidValidSaveItem').value=response['obj']['cta_id'];    
                    get('txtObsercaAtenPrece').value=response['obj']['cta_descripcion'];    
                     get('txtFechaAten').value=setDate(response['obj']['fecha']);
                      
                    var hoy = new Date();
                  
                    var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
                    $("#btnFinAtencion").html(response['obj']['cta_horafin']==''?hora:response['obj']['cta_horafin']); 
                    $("#divInicioAten div").html(response['obj']['cta_horaini']); 
                     
                }
                */
                actualizarComboServPREve(sede,servio)
                if(id!=0){
                    $("#cargar_final").css('display','block')
                }
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                
            });
}
function actualizarComboServPREve(sede,ser) {
	  
    if (sede !== '0') {
        $("#divLoader").show();
        var base_url=$("#base_url").val();
        var user=$("#id_user_for").val();
        var perf=$("#id_user_perf").val();
        var token=$("#_token").val();
         
        $.ajax({
            type: "POST",
            url: base_url+"citas/traerServiciosv3",
            data: {user:user,perf:perf,sede: sede,_token:token},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0")
                poblarComboServPRE(response.listaConfiguracionServicio);
                else
                    document.getElementById("txtServicioAten").innerHTML = "<option value='0'>-- NINGUN SERVICIO PARA MOSTRAR --</option>";
                
              
                $("#txtServicioAten").val(ser)  
                
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
            
    }
}
function filter_Data(page) {
	
        var base_url=$("#base_url").val();
        var token=$("#_token").val();
      var daini=setDate(get('fechaInicia').value);
      var dafin=setDate(get('fechaFin').value);
      var user=get('id_user_for').value;
    var perf=get('id_user_perf').value;
        $.ajax({
            type: "POST",
            url: base_url+"citas/grupoprece",
            data: {perf:perf,user:user,hasta:dafin,desde:daini,page:page,_token:token,method:'filter'},
            dataType: 'json',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'}); 
                if (response.status !== "0"){
            $("#bodyReporte").html(response['response']['table'])
            $("#table-paginaicionorden_pago").html(response['response']['theadPagin'])
                            }
                
              
                 
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("Se detectó un error en el sistema, inténtelo más tarde.");
            });
   
}