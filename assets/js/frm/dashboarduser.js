function nuevoUser(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    get('hidValidSaveItem').value='0';
    get('txtNombreUser').value='';
    get('txtUserUser').value='';
    get('txtpas1User').value='';
    get('txtpas2User').value='';
}
function editarUser(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/grupoUser",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value=response['obj']['id'];
        get('txtNombreUser').value=response['obj']['name'];
        get('txtpas1User').value=response['obj']['textoalter'];
        get('txtpas2User').value=response['obj']['textoalter'];
        get('txtUserUser').value=response['obj']['email'];
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        alert("En este momento no se puede registrar una User. Por favor intente nuevamente más tarde.");      
    });
}
function anular_User(btn,id,estado) {
    if(id!=''){
        swal({
            title: "¿Desea anular?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            cancelButtonClass: "btn-danger",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true
        },
        function(inputValue){
            if (inputValue === true){
                var base_url=get('base_url').value;
                var token=get('_token').value;
                $.ajax({
                    type: "POST",
                    url: base_url+"citas/grupoUser",
                    data: {id:id,estado:estado,_token:token,method:'anular'},
                    dataType: 'JSON',
                    beforeSend: function(){
                        $(".animationload").css({display:'block'});
                    },
                }).done(function( response, textStatus, jqXHR ){   
                    $(".animationload").css({display:'none'});
                    btns=btn.parentNode.parentNode.cells;
                    btns[2].innerHTML='<b style="color:#f2dede">Anulado</b>';
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    $(".animationload").css({display:'none'});
                    alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
                });
            }
        });
    }
}
function Userguardar() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var nombre=get('txtNombreUser').value;   
    var id=get('hidValidSaveItem').value;
    var usuario=get('txtUserUser').value;
    var pas1=get('txtpas1User').value;
    var pas2=get('txtpas2User').value;
    if(pas1!=pas2){
        alert("Contraseña no esta igual ");
        return false;
    }
    if(nombre!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/grupoUser",
            data: {pas1:pas1,usuario:usuario,id:id,nombre:nombre,_token:token,method:'save'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
                $("#myModal").modal('hide');
                $("#bodyUser").html(response['data']['table']);
                $("#table-paginaicionUser").html(response['data']['theadPagin']);
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar. Por favor intente nuevamente más tarde.");      
        }); 
    }
}


function editarUserServcio(User){
    $("#hidValidSaveItem_User").val(User)
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/traerServicios",
        data: {User: User,_token:token},
        dataType: 'json',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'});
            html=''; 
            if (response.status !== "0"){
                $.each( response.listaConfiguracionServicio,function (ind,valor) {
                    html+='<tr>'
                    html+='<td>'+ind+'</td>'
                    html+='<td>'+valor.names+'</td>'
                    html+='</tr>'
                });
            }
            $("#bodyUserser").html(html)
           
             
            $("#agregarpaginacionClients").tableHeadFixer({'foot' : true,'head' : true}); 
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, inténtelo más tarde.");
        });
        
}

function UserServguardar2() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var nombre=get('serviocsesde').value;   
    var idUser=get('hidValidSaveItem_User').value;
    if(nombre!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/grupoUser",
            data: {idUser:idUser,nombre:nombre,_token:token,method:'save_Userservi'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
               // $("#myModal").modal('hide');
                editarUserServcio(idUser);
               
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
        }); 
    }
}