function nuevoServicio(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposervicio",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value='';
        get('txtNombre').value='';
        get('txtNombredetall').value=''
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        alert("En este momento no se puede registrar una sede. Por favor intente nuevamente más tarde.");      
    });
}
function editarServicio(id) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposervicio",
        data: {_token:token,id:id,method:'nuevo'},
        dataType: 'JSON',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'}); 
        get('hidValidSaveItem').value=response['obj']['srv_id'];
        get('txtNombre').value=response['obj']['srv_name'];
        get('txtNombredetall').value=response['obj']['detalle'];
        
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});
        alert("En este momento no se puede registrar una sede. Por favor intente nuevamente más tarde.");      
    });
}
function anular_servicio(btn,idsede,estado) {
    if(idsede!=''){
        swal({
            title: "¿Desea anular?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            cancelButtonClass: "btn-danger",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true
        },
        function(inputValue){
            if (inputValue === true){
                var base_url=get('base_url').value;
                var token=get('_token').value;
                $.ajax({
                    type: "POST",
                    url: base_url+"citas/gruposervicio",
                    data: {idsede:idsede,estado:estado,_token:token,method:'anular_servicio'},
                    dataType: 'JSON',
                    beforeSend: function(){
                        $(".animationload").css({display:'block'});
                    },
                }).done(function( response, textStatus, jqXHR ){   
                    $(".animationload").css({display:'none'});
                    btns=btn.parentNode.parentNode.cells;
                    btns[2].innerHTML='<b style="color:#f2dede">Anulado</b>';
                }).fail(function(jqXHR, ajaxOptions, thrownError){
                    $(".animationload").css({display:'none'});
                    alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
                });
            }
        });
    }
}
function servicioguardar() {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    var nombre=get('txtNombre').value;  
    var detalle=get('txtNombredetall').value;  
    var idsede=get('hidValidSaveItem').value;
    if(nombre!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/gruposervicio",
            data: {idsede:idsede,detalle:detalle,nombre:nombre,_token:token,method:'save_servicio'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
                $("#myModal").modal('hide');
                $("#bodyServicio").html(response['data']['table']);
                $("#table-paginaicionservicio").html(response['data']['theadPagin']);
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
        }); 
    }
}

function editarSedeServci_usero(user){
    $("#hidValidSaveItem_serv").val(user)
    var base_url=get('base_url').value;
    var token=get('_token').value;
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposervicio",
        data: {user: user,_token:token,method:'get_User'},
        dataType: 'json',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
           },
        }).done(function( response, textStatus, jqXHR ){   
            $(".animationload").css({display:'none'});
            html=''; 
            if (response.status !== "0"){
                index=1;
                $.each( response.obj,function (ind,valor) {
                    html+='<tr>'
                    html+='<td>'+index+'</td>'
                    html+='<td>'+valor.name+'</td>'
                    html+='<td>'+valor.email+'</td>'
                    html+='<td>'+(valor.usr_estado=='1'?'Activo':'<b style="color:red">Anulado</b>')+'</td>';
                   
                    html+='<td><a href="#" onclick="anular_user_servi(this,'+valor.ide_ref+","+valor.userid+","+valor.id_servi+')">Anular</a></td>'
                    html+='</tr>'
                    index++;
                });
            }
            $("#bodySedeser").html(html)
           
             
            $("#agregarpaginacionClients").tableHeadFixer({'foot' : true,'head' : true}); 
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            $(".animationload").css({display:'none'});  
            alert("Se detectó un error en el sistema, inténtelo más tarde.");
        });
        
}

function anular_user_servi(lnk,id,userid,servid) {
    var base_url=get('base_url').value;
    var token=get('_token').value;
    swal({
        title: "¿Desea anular?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-info",
        cancelButtonClass: "btn-danger",
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: true
    },
    function(inputValue){
        if (inputValue === true){
    $.ajax({
        type: "POST",
        url: base_url+"citas/gruposervicio",
        data: {id: id,userid:userid,servid:servid,_token:token,method:'get_AnularUser'},
        dataType: 'json',
        beforeSend: function(){
            $(".animationload").css({display:'block'});
        },
    }).done(function( response, textStatus, jqXHR ){   
        $(".animationload").css({display:'none'});
        td=lnk.parentNode.parentNode.cells;
         
        td[3].innerHTML='<b style="color:red">Anulado</b>';
    }).fail(function(jqXHR, ajaxOptions, thrownError){
        $(".animationload").css({display:'none'});  
        alert("Se detectó un error en el sistema, inténtelo más tarde.");
    });  
}
});
}

function Agregar_serviUser() {
    var base_url=get('base_url').value;
    var token=get('_token').value;   
    var ideusert=get('serviocsesde').value;   
    var idserv=get('hidValidSaveItem_serv').value;
    if(ideusert!=''){
        $.ajax({
            type: "POST",
            url: base_url+"citas/gruposervicio",
            data: {idserv:idserv,ideusert:ideusert,_token:token,method:'save_serviUser'},
            dataType: 'JSON',
            beforeSend: function(){
                $(".animationload").css({display:'block'});
               },
            }).done(function( response, textStatus, jqXHR ){   
                $(".animationload").css({display:'none'});
               // $("#myModal").modal('hide');
               editarSedeServci_usero(idserv)
               
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                $(".animationload").css({display:'none'});  
                alert("En este momento no se puede registrar una cita. Por favor intente nuevamente más tarde.");      
        }); 
    }
}