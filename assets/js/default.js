function convertirMayus(evt) {
    evt.target.value=evt.target.value.toUpperCase();
}
function validate_frm(evt){
    trg=evt.target;
    if(trg.value!=''){
        trg.style.border=''
    }else{
    
        if(evt.keyCode==13){
            if(trg.value==''){
                trg.style.border='2px solid #ee3a3a';
            }else{
                trg.style.border='';
            }
        }    
    }

}
function not_white(id){
    ide=$(id).attr("id");
       var x =$(id).val();
     if(x=="" || x==null){
      $(id).css('border', 'border: 1px solid #ced4da');
     }
      if(/^\s*$/.test($(id).val())){//: ;
          $(id).remove('style');
          limpia(ide);
          return false;  
      }else{
         $(id).css('border', 'border: 1px solid #ced4da');  
      }
  
}
function limpia(id) {
    var val = document.getElementById(id).value;
    var tam = val.length;
    for(i = 0; i < tam; i++) {
        if(!isNaN(val[i]))
            document.getElementById(id).value = '';
    }
}
function goNextSave(evt, id){
    evt = setEvent(evt);
    if(evt.keyCode == 13){
      $("#"+id).focus().select();
      return false;
    }
}
function gtn(obj, tag) {
    obj = getObj(obj);
    return obj.getElementsByTagName(tag);
}
function getBody() {
    return gtn(document, 'body')[0];
}
function getHead() {
    return gtn(document, 'head')[0];
}
function getObj(obj) {
    if (typeof obj == "string") obj = get(obj);
    return obj;
}
function get(id) {
    return document.getElementById(id);
}
function removeElement(obj) {
    obj = getObj(obj);
    if (obj) {
      if (typeof obj != 'undefined' && typeof obj.onremove === 'function') {
        obj.onremove();
      }
      obj.parentNode.removeChild(obj);
    }
  }
function unloadJSCSS(prgID) {
    removeElement('js-' + prgID);
    removeElement('css-' + prgID);
}
function loadJSCSS(ruta,prgIDJS,prgIDCSS) {
    console.log(prgIDJS)
if(prgIDJS!=''){
    var js = document.createElement('script');
    js.id = 'js-' + prgIDJS;
    js.type = 'text/javascript';
    js.src = ruta+'assets/js/frm/' + prgIDJS + '.js?rnd=' + Math.random();
    getBody().appendChild(js);
   
} 

if(prgIDCSS!=''){
    var css = document.createElement('link');
    css.id = 'css-' + prgIDCSS;
    css.type = "text/css";
    css.rel = "stylesheet";
    css.href = ruta+'assets/css/frm/' + prgIDCSS + '.css?rnd=' + Math.random();
    getHead().appendChild(css);
}



}

function setEvent(evt) {
    var e = new Object();

    var nav = getNavegador();

    if (nav) e.target = evt.srcElement;
    else e.target = evt.target;

    e.shiftKey = evt.shiftKey;
    e.ctrlKey = evt.ctrlKey;

    var tipo = evt.type;
    e.type = tipo;

    if (tipo == 'keydown' || tipo == 'keyup') {
        e.keyCode = evt.keyCode;
    }  /*else if (tipo == "keypress") {

    }*/

    e.preventDefault = function() {
        if (nav) evt.returnValue = false;
        else evt.preventDefault();
    };

    e.stopPropagation = function() {
        if (nav) evt.cancelBubble = true;
        else evt.stopPropagation();
    };

    var x = evt.clientX;
    var y = evt.clientY;

    e.clientX = x;
    e.clientY = y;

    if (nav) {
        e.scrollLeft = document.documentElement.scrollLeft;
        e.scrollTop = document.documentElement.scrollTop;

        x += e.scrollLeft;
        y += e.scrollTop;

    } else {
        x = evt.pageX;
        y = evt.pageY;

        e.scrollLeft = x - e.clientX;
        e.scrollTop = y - e.clientY;
    }

    e.pageX = x;
    e.pageY = y;

    return e;
}
function getNavegador() {
    var nav = window.navigator;
    var ver = 0;

    if (nav.appName.indexOf("Explorer") != -1) {
        oVer = nav.appVersion;
        var pi = oVer.indexOf("MSIE") + 5;
        var pf = oVer.indexOf(";", pi);

        ver = oVer.substr(pi, pf - pi);
    }

    return ver;
}

function toInt(num) {
    num = parseInt(num, 10);

    if (isNaN(num)) num = 0;

    return num;
}
function getTableOrderby(lkn,sql){
    tr=lkn.parentNode.cells;
    trleng=tr.length;
    resultado='';
    for (index = 0; index < trleng; index++) {
        if (typeof $(tr[index]).data().orderby !== 'undefined'){
            orderby=$(tr[index]).data().orderby;
            paramsql=$(tr[index]).data().paramsql;
            click=$(tr[index]).data().click;
            if(paramsql==sql){
                click=toInt(click)+1
                $(tr[index]).data().click=click;
                if(orderby==''){
                    $(tr[index]).data().orderby='ASC'
                    orderby='ASC'
                }
                if(click==1){
                    $(tr[index]).data().orderby='ASC'
                    orderby='ASC'
                }else if(click==2){
                    $(tr[index]).data().orderby='DESC'
                    orderby='DESC'
                }else{
                    $(tr[index]).data().orderby=''
                    orderby='' 
                }
                
                if(orderby=='DESC'){
                    $(lkn).removeClass('sorting_asc').removeClass('sorting');
                    $(lkn).addClass('sorting_desc');   
                }  
                if(orderby=='ASC'){
                    $(lkn).removeClass('sorting_desc').removeClass('sorting');
                    $(lkn).addClass('sorting_asc');      
                } 
                if(click==3){
                    $(tr[index]).data().click=0;
                    $(lkn).removeClass('sorting_desc').removeClass('sorting_asc');
                    $(lkn).addClass('sorting'); 
                }
                
            }            
            if(orderby!=''){
                coma='';
                if(index!=0){
                    coma=',';
                }
                resultado+=coma+' '+paramsql+' '+orderby
            }
        } 
    }
    return resultado;
}
function getTableDataInfo(tabla) {
    tr=$("#"+tabla+" thead tr")[0].cells;   
    trleng=tr.length;
    resultado='';
    for (index = 0; index < trleng; index++) {
        if (typeof $(tr[index]).data().orderby !== 'undefined'){
            orderby=$(tr[index]).data().orderby;
            paramsql=$(tr[index]).data().paramsql;
            if(orderby!=''){
                coma='';
                if(index!=0){
                    coma=',';
                }

                resultado+=coma+' '+paramsql+' '+orderby
            }
        }    
    }
    valorexit=resultado.charAt(0)
    if(valorexit==','){
        resultado=resultado.substring(1, resultado.length);
    }
  
    return resultado;
}

function setDate(fec) {
    if (fec == '') {
        return '';
    }
    var sep = '';

    if (fec.indexOf('/') != -1) {
        sep = '/';
    } else if (fec.indexOf('-') != -1) {
        sep = '-';
    } else {
        //alert('Fecha no válida');
        return false;
    }

    var fcs = fec.split(sep);

    if (fcs[2].length == 4) {
        return fcs[2] + '-' + fcs[1] + '-' + fcs[0];
    } else if (fcs[0].length == 4) {
        return fcs[2] + '/' + fcs[1] + '/' + fcs[0];
    } else {
        //alert('Fecha no válida');
        return false;
    }
}
function getDate() {
    var date = new Date();
  
    var day = date.getDate();
    if (day < 10) day = '0' + day;
  
    var month = date.getMonth() + 1;
    if (month < 10) month = '0' + month;
  
    return day + '/' + month + '/' + date.getFullYear();
  }
function mngWin(prgID) {
    var eles, firstEle = null;
    eles=document.forms[prgID];
  
    this.clearForm = function(execps) {
        if (typeof execps == 'undefined') execps = '';
        var e;
        for (e = 0; e < eles.length; e ++) {
            var ele = eles[e];
            if (execps.indexOf(ele.id) == -1) {
                var tag = ele.tagName.toLowerCase();
                if (tag == 'input') {
                    var type = ele.type.toLowerCase();

                    if (type == 'text' || type == 'password' || type == 'hidden' || type == 'file') ele.value = '';
                    else if (type == 'checkbox' || type == 'radio') ele.checked = false;
                }else if (tag == 'select') {
                    ele.selectedIndex = 0;
                }    else if (tag == 'textarea') ele.value = '';
            }
        }
    };
    this.clearFormNow = function(execps) {
        if (typeof execps == 'undefined') execps = '';
        var e;
        for (e = 0; e < eles.length; e ++) {
            var ele = eles[e];
            if (execps.indexOf(ele.id) == -1) {
                var tag = ele.tagName.toLowerCase();
                if (tag == 'input') {
                    var type = ele.type.toLowerCase();

                    if (type == 'text' || type == 'password' || type == 'hidden' || type == 'file') ele.value = '';
                    else if (type == 'checkbox' || type == 'radio') ele.checked = false;
                }else if (tag == 'select') {
                    ele.selectedIndex = 0;
                }    else if (tag == 'textarea') ele.value = '';
            }
        }
    };
  
}