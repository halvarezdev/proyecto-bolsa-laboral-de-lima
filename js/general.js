function enviarBusqueda(){
    $( "#form_banner" ).submit();
  }
  
  function randDelay(min, max) {
      return Math.floor(Math.random() * (max-min+1)+min);
  }
  phCount = 0;
  function printLetter(string, el) {
   
      // split string into character seperated array
      var arr = string.split(''),
          input = el,
          // store full placeholder
          origString = string,
          // get current placeholder value
          curPlace = $(input).attr("placeholder"),
          // append next letter to current placeholder
          placeholder = curPlace + arr[phCount];
          
      setTimeout(function(){
          // print placeholder text
          $(input).attr("placeholder", placeholder);
          // increase loop count
          phCount++;
          // run loop until placeholder is fully printed
          if (phCount < arr.length) {
              printLetter(origString, input);
          }
      // use random speed to simulate
      // 'human' typing
      }, randDelay(50, 90));
  }  
  
  // function to init animation
  function placeholder() {
    var texto = "Busca un puesto, área o empresa.";
      var searchBar = $('#search').attr("placeholder", "");
      printLetter(texto, searchBar);
  }
  window.onload = function() {
    placeholder();
  //  $( "#boton_defecto" ).click();
  };