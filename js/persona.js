function validarDiscapcidad(dis) {

    if (dis.value == "1") {
        document.getElementById("oculto").style = "visibility: visible;";
    } else {
        document.getElementById("oculto").style = "visibility: hidden;";
    }

}


function validaPersonalForm() {

    if ($("#tipo_documento").val() == "1") {
        $(".tipo_documento").html('Tiene que Ingresar Tipo Documento').show();
        $("#tipo_documento").focus();
        return false;
    }

}

function pregunta() {
    $('#pills-experiencia').removeClass('show');
    $('#pills-experiencia').removeClass('active');
    $('#pills-pregunta').addClass('show active');
    $('#pills-pregunta-tab').addClass('show active circulo');
}

function experiencia() {
    $('#pills-profile').removeClass('show');
    $('#pills-profile').removeClass('active');
    $('#pills-experiencia').addClass('show active');
    $('#pills-experiencia-tab').addClass('show active circulo');
}

function contact() {
    $('#pills-profile').removeClass('show');
    $('#pills-profile').removeClass('active');
    $('#pills-contact').addClass('show active');
    $('#pills-contact-tab').addClass('show active circulo');
}

function profile() {
    if ($("#numero_documento").val() == "") {
        $(".numero_documento").html('Tiene que Ingresar Numero de Documento').show();
        $("#numero_documento").focus();
        return false;
    }

    if ($("#email").val() == "") {
        $(".email").html('Tiene que Ingresar Email').show();
        $("#email").focus();
        return false;
    }
    if ($("#password").val() == "") {
        $(".password").html('Tiene que Ingresar Su Contraseña').show();
        $("#password").focus();
        return false;
    }
    if ($("#password-confirm").val() == "") {
        $(".password-confirm").html('Tiene que Ingresar Su Contraseña').show();
        $("#password-confirm").focus();
        return false;
    }
    if ($("#name").val() == "") {
        $(".name").html('Tiene que Ingresar Su Nombre').show();
        $("#name").focus();
        return false;
    }
    if ($("#ape_pat").val() == "") {
        $(".ape_pat").html('Tiene que Ingresar Apellido Paterno').show();
        $("#ape_pat").focus();
        return false;
    }
    if ($("#ape_mat").val() == "") {
        $(".ape_mat").html('Tiene que Ingresar Apellido Materno').show();
        $("#ape_mat").focus();
        return false;
    }
    if ($("#direccion").val() == "") {
        $(".direccion").html('Tiene que Ingresar Direccion').show();
        $("#direccion").focus();
        return false;
    }
    if ($("#fecha_nacimiento").val() == "") {
        $(".fecha_nacimiento").html('Tiene que Ingresar Fecha Nacimiento').show();
        $("#fecha_nacimiento").focus();
        return false;
    }
    if ($("#estado_civil").val() == "0") {
        $(".estado_civil").html('Tiene que Ingresar Estado Civil').show();
        $("#estado_civil").focus();
        return false;
    }
    if (!$("#che233").is(':checked')) {
        $(".che233").html('Debe de estar de acuerdo con la politica del portal para continuar').show();
        $("#che233").focus();
        return false;
    }

    $('#pills-home').removeClass('show');
    $('#pills-home').removeClass('active');
    $('#pills-profile').addClass('show active');
    $('#pills-profile-tab').addClass('show active circulo');
}

function home() {
    $('#pills-profile').removeClass('show');
    $('#pills-profile').removeClass('active');
    $('#pills-profile-tab').removeClass('show active circulo');
    $('#pills-home').addClass('show active');
    $('#pills-home-tab').addClass('show active circulo');
}

function profileAnterior() {
    $('#pills-experiencia').removeClass('show');
    $('#pills-experiencia').removeClass('active');
    $('#pills-experiencia-tab').removeClass('show active circulo');
    $('#pills-profile').addClass('show active');
    $('#pills-profile-tab').addClass('show active circulo');
}

function contactAnterior() {
    $('#pills-experiencia').removeClass('show');
    $('#pills-experiencia').removeClass('active');
    $('#pills-experiencia-tab').removeClass('show active circulo');
    $('#pills-contact').addClass('show active');
    $('#pills-contact-tab').addClass('show active circulo');
}

function experienciaAnterior() {
    $('#pills-pregunta').removeClass('show');
    $('#pills-pregunta').removeClass('active');
    $('#pills-pregunta-tab').removeClass('show active circulo');
    $('#pills-experiencia').addClass('show active');
    $('#pills-experiencia-tab').addClass('show active circulo');

}

function enviarFormulario() {
    $("#formulario_persona").submit();
}

function ComboboxProvincia(idc) {
    var base_url = $("#base_url").val();

    var urlData = base_url + "ubigeo/provincia/" + idc.value;
    $.ajax({
        type: 'GET',
        url: urlData,
        dataType: 'JSON',
        beforeSend: function() {
            $(".loader").css({ display: 'block' });
        }
    }).done(function(data, textStatus, jqXHR) {
        var texto = '';
        texto += '<option value="">Seleccione Provincia</option>';
        text = '<option value="">Seleccione Distrito</option>';
        if (data['success'] != "false" && data['success'] != false) {
            if (typeof (data['ResulProvincia']) != 'undefined') {
                $.each(data['ResulProvincia'], function(i, field) {
                    texto += '<option value="' + field.ubi_codprov + '">' + field.ubi_descripcion + '</option>';
                });
                $('#provincia').html(texto);
                $('#distrito').html(text);
            } else {
                $('#provincia').html(texto);
                $('#distrito').html(text);
            }
        } else {
            $('#provincia').html(text);
            $('#distrito').html(text);
        }
        $(".loader").css({ display: 'none' });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        mensaje = 'Solicitud fallida: ' + textStatus;
        $(".mensajeGlobal").SendMessajeAlert({
            messaje: mensaje + textStatus,
            dataType: 'Error',
            type: 'danger',
            time: 6000
        });
        $(".loader").css({ display: 'none' });
    });
}

function ComboboxDistrito(prov) {
    var base_url = $("#base_url").val();
    var comxDeparment = $("#departamento").val();
    //  var comxProvin = $("#provi").val();
    var urlData = base_url + "ubigeo/distrito/" + comxDeparment + '/' + prov.value;
    $.ajax({
        type: 'GET',
        url: urlData,
        dataType: 'JSON',
        beforeSend: function() {
            $(".loader").css({ display: 'block' });
        }
    }).done(function(data, textStatus, jqXHR) {
        var texto = '';
        texto += '<option value="">Seleccione Distrito</option>';
        if (data['success'] != "false" && data['success'] != false) {
            if (typeof (data['ResulDistrito']) != 'undefined') {
                $.each(data['ResulDistrito'], function(i, field) {
                    texto += '<option value="' + field.ubi_coddist + '">' + field.ubi_descripcion + '</option>';
                });
                $('#distrito').html(texto);
            } else {
                $('#distrito').html(texto);
            }
        } else {
            $('#distrito').html(texto);
        }
        $(".loader").css({ display: 'none' });
    }).fail(function(jqXHR, textStatus, errorThrown) {
        mensaje = 'Solicitud fallida: ' + textStatus;
        $(".mensajeGlobal").SendMessajeAlert({
            messaje: mensaje + textStatus,
            dataType: 'Error',
            type: 'danger',
            time: 6000
        });
        $(".loader").css({ display: 'none' });
    });
}

function checkDateLessCurrentDate(date) {

    const [year, month, day] = date.split('-');

    const dateLimit = new Date(year, (Number(month) - 1), day);
    const today = new Date();

    const hasPassedDate = dateLimit <= today;

    return hasPassedDate;
}

function enviarFormu() {
 
    if ($("#descripcion").val() == "") {
        $(".descripcion").html('Tiene que Ingresar Descripcion del pedido').show();
        $("#descripcion").focus();
        return false;
    } else {
        $(".descripcion").html('').hide();
    }

    if ($("#denominacion").val() == "") {
        $(".denominacion").html('Tiene que Ingresar Denominacion').show();
        $("#denominacion").focus();
        return false;
    } else {
        $(".denominacion").html('').hide();
    }

    if ($("#num_vacantes").val() == "") {
        $(".num_vacantes").html('Tiene que Ingresar Numero de Vacantes').show();
        $("#num_vacantes").focus();
        return false;
    } else {
        $(".num_vacantes").html('').hide();
    }

    if ($("#area").val() == "") {
        $(".area").html('Tiene que Ingresar Area').show();
        $("#area").focus();
        return false;
    } else {
        $(".area").html('').hide();
    }


    if ($("#tareas").val() == "") {
        $(".tareas").html('Tiene que Ingresar Tareas').show();
        $("#tareas").focus();
        return false;
    } else {
        $(".tareas").html('').hide();
    }

    if ($("#modalidades").val() == "") {
        $(".modalidades").html('Tiene que Ingresar Modalidades').show();
        $("#modalidades").focus();
        return false;
    } else {
        $(".modalidades").html('').hide();
    }

    if ($("#horario_trabajo").val() == "") {
        $(".horario_trabajo").html('Tiene que Ingresar Horario de Trabajo').show();
        $("#horario_trabajo").focus();
        return false;
    } else {
        $(".horario_trabajo").html('').hide();
    }

    if ($("#turno_trabajo").val() == "") {
        $(".turno_trabajo").html('Tiene que Ingresar Turno de Trabajo').show();
        $("#turno_trabajo").focus();
        return false;
    } else {
        $(".turno_trabajo").html('').hide();
    }

    if ($("#horario").val() == "") {
        $(".horario").html('Tiene que Ingresar horario').show();
        $("#horario").focus();
        return false;
    } else {
        $(".horario").html('').hide();
    }

    if ($("#remuneracion").val() == "") {
        $(".remuneracion").html('Tiene que Ingresar Remuneracion').show();
        $("#remuneracion").focus();
        return false;
    } else {
        $(".remuneracion").html('').hide();
    }

    if ($("#direccion_trabajo").val() == "") {
        $(".direccion_trabajo").html('Tiene que Ingresar Direccion del Trabajo').show();
        $("#direccion_trabajo").focus();
        return false;
    } else {
        $(".direccion_trabajo").html('').hide();
    }

    if ($("#departamento").val() == "" || $("#departamento").val() == "00") {
        $(".departamento").html('Tiene que Ingresar Departamento').show();
        $("#departamento").focus();
        return false;
    } else {
        $(".departamento").html('').hide();
    }

    if ($("#provincia").val() == "" || $("#provincia").val() == "00") {
        $(".provincia").html('Tiene que Ingresar Provincia').show();
        $("#provincia").focus();
        return false;
    } else {
        $(".provincia").html('').hide();
    }

    if ($("#distrito").val() == "" || $("#distrito").val() == "00") {
        $(".distrito").html('Tiene que Ingresar Distrito').show();
        $("#distrito").focus();
        return false;
    } else {
        $(".distrito").html('').hide();
    }

    if ($("#puesto").val() == "") {
        $(".puesto").html('Tiene que Ingresar Puesto').show();
        $("#puesto").focus();
        return false;
    } else {
        $(".puesto").html('').hide();
    }

    if ($("#fecha_limite").val() == "") {
        $(".fecha_limite").html('Tiene que Ingresar Fecha Limite').show();
        $("#fecha_limite").focus();
        return false;
    } else {
        $(".fecha_limite").html('').hide();
    }

    if ($("#fecha_limite").val() != "") {

        const fecha_limite = $("#fecha_limite").val();
        
        var fechadesde = new Date(fecha_limite); 
        
        var date1 = new Date();
        date1.setMonth(date1.getMonth() + 2);
     
 
        if (checkDateLessCurrentDate(fecha_limite)) {
            $(".fecha_limite").html('La fecha es invalida').show();
            $("#fecha_limite").focus();
            return false;
        }
        if(fechadesde<=date1){ 
        }else{
            $(".fecha_limite").html('La fecha tiene que no superior a 2 meses').show();
            $("#fecha_limite").focus();
            return false;
        }
        
    } else {
        $(".fecha_limite").html('').hide();
    }


    $("#formulario_vacante").submit();
}

function estiloFecha() {
    $(".ui-datepicker-calendar").css('display', 'none');
    $(".ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current").css('display', 'none');
    $(".ui-datepicker .ui-datepicker-buttonpane").css('margin', '0 0 0 0');
    /*   .ui-datepicker-calendar {
   display: none;
}
.ui-datepicker .ui-datepicker-buttonpane button.ui-datepicker-current {

   display: none;
}
.ui-datepicker .ui-datepicker-buttonpane {
    margin: 0 0 0 0;
}*/
}

function nivelEstudio(input) {

    if (input.value == "2" || input.value == "3") {

        document.getElementById("centro_estudio2").disabled = true;
        document.getElementById("centro_estudio2").value = "";
        document.getElementById("fecha_incio2").disabled = true;
        document.getElementById("fecha_incio2").value = "";
        document.getElementById("fecha_fin2").disabled = true;
        document.getElementById("fecha_fin2").value = "";
        document.getElementById("especialidad3").disabled = true;
        //      document.getElementById("especialidad3").value = "";
        document.getElementById("centro_estudio3").disabled = true;
        document.getElementById("centro_estudio3").value = "";
        document.getElementById("otro_centro_estudio3").disabled = true;
        document.getElementById("otro_centro_estudio3").value = "";
        document.getElementById("fecha_incio3").disabled = true;
        document.getElementById("fecha_incio3").value = "";
        document.getElementById("fecha_fin3").disabled = true;
        document.getElementById("fecha_fin3").value = "";
        document.getElementById("especialidad4").disabled = true;
        document.getElementById("especialidad4").value = "";
        document.getElementById("centro_estudio4").disabled = true;
        document.getElementById("centro_estudio4").value = "";
        document.getElementById("otro_centro_estudio4").disabled = true;
        document.getElementById("otro_centro_estudio4").value = "";
        document.getElementById("fecha_incio4").disabled = true;
        document.getElementById("fecha_incio4").value = "";
        document.getElementById("fecha_fin4").disabled = true;
        document.getElementById("fecha_fin4").value = "";
    } else if (input.value == "4" || input.value == "5") {
        document.getElementById("centro_estudio2").disabled = false;
        document.getElementById("centro_estudio2").value = "";
        document.getElementById("fecha_incio2").disabled = false;
        document.getElementById("fecha_incio2").value = "";
        document.getElementById("fecha_fin2").disabled = false;
        document.getElementById("fecha_fin2").value = "";
        document.getElementById("especialidad3").disabled = true;
        document.getElementById("especialidad3").value = "";
        document.getElementById("centro_estudio3").disabled = true;
        document.getElementById("centro_estudio3").value = "";
        document.getElementById("otro_centro_estudio3").disabled = true;
        document.getElementById("otro_centro_estudio3").value = "";
        document.getElementById("fecha_incio3").disabled = true;
        document.getElementById("fecha_incio3").value = "";
        document.getElementById("fecha_fin3").disabled = true;
        document.getElementById("fecha_fin3").value = "";
        document.getElementById("especialidad4").disabled = true;
        document.getElementById("especialidad4").value = "";
        document.getElementById("centro_estudio4").disabled = true;
        document.getElementById("centro_estudio4").value = "";
        document.getElementById("otro_centro_estudio4").disabled = true;
        document.getElementById("otro_centro_estudio4").value = "";
        document.getElementById("fecha_incio4").disabled = true;
        document.getElementById("fecha_incio4").value = "";
        document.getElementById("fecha_fin4").disabled = true;
        document.getElementById("fecha_fin4").value = "";
    } else {
        document.getElementById("centro_estudio2").disabled = false;
        document.getElementById("centro_estudio2").value = "";
        document.getElementById("fecha_incio2").disabled = false;
        document.getElementById("fecha_incio2").value = "";
        document.getElementById("fecha_fin2").disabled = false;
        document.getElementById("fecha_fin2").value = "";
        document.getElementById("especialidad3").disabled = false;
        document.getElementById("especialidad3").value = "";
        document.getElementById("centro_estudio3").disabled = false;
        document.getElementById("centro_estudio3").value = "";
        document.getElementById("otro_centro_estudio3").disabled = false;
        document.getElementById("otro_centro_estudio3").value = "";
        document.getElementById("fecha_incio3").disabled = false;
        document.getElementById("fecha_incio3").value = "";
        document.getElementById("fecha_fin3").disabled = false;
        document.getElementById("fecha_fin3").value = "";
        document.getElementById("especialidad4").disabled = false;
        document.getElementById("especialidad4").value = "";
        document.getElementById("centro_estudio4").disabled = false;
        document.getElementById("centro_estudio4").value = "";
        document.getElementById("otro_centro_estudio4").disabled = false;
        document.getElementById("otro_centro_estudio4").value = "";
        document.getElementById("fecha_incio4").disabled = false;
        document.getElementById("fecha_incio4").value = "";
        document.getElementById("fecha_fin4").disabled = false;
        document.getElementById("fecha_fin4").value = "";
    }


}
