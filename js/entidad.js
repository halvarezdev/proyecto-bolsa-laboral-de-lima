$(".sidebar-dropdown > a").click(function() {
    $(".sidebar-submenu").slideUp(200);
    if (
      $(this)
        .parent()
        .hasClass("active")
    ) {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .parent()
        .removeClass("active");
    } else {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .next(".sidebar-submenu")
        .slideDown(200);
      $(this)
        .parent()
        .addClass("active");
    }
  });
  
  $("#close-sidebar").click(function() {
    $(".page-wrapper").removeClass("toggled");
  });
  $("#show-sidebar").click(function() {
    $(".page-wrapper").addClass("toggled");
  });
  
  function BuscarDNI(btn){
    var tipo=$("#tipo_documento").val();
    var numero_documento=$("#numero_documento").val();
    if(tipo!='2'){
      return false;
    }
    var base_url=$("#base_url").val();
    $.ajax({
      url:base_url+'recsunat/reniec?dni='+numero_documento,
      type: 'GET', 
      dataType:'JSON',
      beforeSend: function(){
      $(btn).html('Buscando..');
      },
  }).done(function(data , textStatus, jqXHR ){
    $(btn).html('Buscar');
      if(data['coResultado']=='0000'){
        $("#ape_pat").val(data['datosPersona']['apPrimer']);
        $("#ape_mat").val(data['datosPersona']['apSegundo']);
        $("#name").val(data['datosPersona']['prenombres']); 
      }else{
        alert('No se encontro resultado')
      }
  }).fail(function(jqXHR, ajaxOptions, thrownError){
    $(btn).html('Buscando'); 
  });  
  }

