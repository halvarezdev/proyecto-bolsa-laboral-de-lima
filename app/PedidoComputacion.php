<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoComputacion extends Model
{
    protected $table = 'ped_compu';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'compu_nom', 'compu_nivel', 'pedido_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
