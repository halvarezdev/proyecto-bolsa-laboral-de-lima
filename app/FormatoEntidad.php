<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormatoEntidad extends Model
{
    protected $table = 'formatoentidad';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imageFondo', 'colorTitulo', 'colorSubTitulo', 'colorPrimerParrafo', 'colorSegundoParrafo', 'colorTercerParrafo',
        'id_institucion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
