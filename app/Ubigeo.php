<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model
{
    protected $table = 'ubigeo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ubi_codigo', 'ubi_coddpto', 'ubi_codprov', 'ubi_coddist', 'ubi_descripcion',
        'ubi_estado', 'ubi_zona', 'ubi_codigo', 'ubi_departamento', 'ubi_departamento'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
