<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducacionPersona extends Model
{
    protected $table = 'educpersonas';
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'especialidad', 'centro_estudio', 'otro_centro_estudio', 'fecha_incio','fecha_termino',
        'tipo_educ', 'ficha_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
