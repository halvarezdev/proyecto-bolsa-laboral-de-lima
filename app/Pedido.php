<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'pedidos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descripcion', 'denominacion', 'num_vacantes', 'estudios_formales', 'especialidad',
        'otro_conocimiento', 'experiencia_laboral', 'experiencia_nivel', 'zona',
        'contacto', 'personal_cargo', 'cantidad', 'tareas', 'modalidades',
        'horario_trabajo', 'horario', 'remuneracion', 'moneda', 'ubigeo',
        'horas_extras', 'movilidad', 'refrigerio', 'bonificacion', 'seguro',
        'comisiones', 'discapacidad', 'radio_discapacidad', 'puesto', 'empresa_id', 'contacto_id',
        'area','grado','sexo','edad','edad_total','actitudes','turno_trabajo','direccion_trabajo',
        'beneficios','licencia','cuenta_carro','categoria','fecha_limite','obersavasiones', 
        'tipo_carro','motivo_cierre','fecha_cierre','flag_estado', 'estudios_postgrado','fich_estado',
        'cod_entidad', 'tipo_entidad','licencia_arma'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

