<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maestro extends Model
{
    protected $table = 'maestros';
    /**
     * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'MAESP_CODIGO1', 'MAESP_CODIGO2', 'MAESP_CODIGO', 'MAESC_DESCRIPCION','MAESC_ABREVIATURA',
    ];

    /**
     * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
    //  'password', 'remember_token',
    ];
}
