<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
       protected $table = 'ocupaciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ocup_codigo', 'ggrupo', 'grupo', 'sgrupo', 'apertura',
        'nombre', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
