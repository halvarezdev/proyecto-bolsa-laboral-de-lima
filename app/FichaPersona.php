<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
class FichaPersona extends Model
{
    use Notifiable;
    protected $table = 'fichapersona';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trabajo_celular', 'trabajo_correo', 'politica', 'persona_id','presentacion',
        'nivel_estudio', 'filecv','id_pais','fic_salario','licenciaConduc', 'categoria',
        'port_arma','arma_propia','ocu_trabajo','cod_entidad','tipo_entidad','ubi_lugar', 'url_cv'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
