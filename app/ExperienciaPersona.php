<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienciaPersona extends Model
{
    protected $table = 'experiencia_persona';
    /**
     * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'sector', 'empresa', 'rubro', 'area','ocupacion', 'tipo_contrato', 'persona_cargo',
        'funciones_cargo', 'sueldo', 'fecha_ini', 'fecha_fin', 'motivo_cese', 'nombre_jefe',
        'cargo_jefe', 'telefono', 'ficha_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
    
    ];
}
