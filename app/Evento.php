<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'eventos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_evento', 'nombre', 'descripcion', 'id_usario', 'fecha_registro',
        'ruta_imagen', 'fecha_inicio', 'fecha_fin', 'estado', 'id_entidad',
        'ruta_imagen_2', 'ruta_imagen_3', 'estado1', 'estado2', 'estado3', 'tipo1',
        'tipo2', 'tipo3', 'ruta_1', 'ruta_2', 'ruta_3'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

