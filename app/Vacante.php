<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacante extends Model
{
    protected $table = 'vacantes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_institucion', 'num_convocatoria', 'nombre_especialidad', 'regimen_laboral','experiencia',
        'formacion_academica','especializacion','conocimiento','competencias','detalle','sueldo',
        'cant_vacantes','vac_flag','created_at','updated_at','url_vanc','fecha_postula','fech_referencia',
        'ubigeo','url_detalle','fech_inicio'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
