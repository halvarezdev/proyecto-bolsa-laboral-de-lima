<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $guard = 'empresas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_documento', 'numero_documento', 'razon_social', 'descripcion', 'rubro',
        'sitio_web', 'direccion', 'ubigeo','cod_entidad','tipo_entidad','emp_urlvideo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
