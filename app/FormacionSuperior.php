<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormacionSuperior extends Model
{
    protected $table = 'per_superior';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'especialidad_id', 'nivel_id', 'centro_id', 'otro_centro', 'fecha_inicio', 'fecha_fin',
        'pers_id', 'fichper_id','persu_estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

