<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnvioCita extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$subject = $this->datos_users['nombre'].", tenemos ".$this->datos_users['cantidad']." vacantes publicadas para ".$this->datos_users['name_ocupacion'];
       
        return $this->subject("Se genero una nueva cita")
        ->from('empleo.mun.ptepiedra@gmail.com', 'empleo.munipuentepiedra.gob.pe')
        ->view('mails.mail_cita',$this->data);

    //return $this->view('mails.mail_cita');

    }
}
