<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnvioMasivoPedVac extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $datos_users;

    public function __construct($datos_users)
    {
        $this->datos_users = $datos_users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->datos_users['nombre'].", tenemos ".$this->datos_users['cantidad']." vacantes publicadas para ".$this->datos_users['name_ocupacion'];
       
        return $this->subject($subject)
        ->from('bolsaempleo@accede.com.pe', 'accede.com.pe')
        ->view('mails.mail_masivo');

    return $this->view('mails.mail_masivo');

    }
}
