<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmergencyCallReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The data_resul object instance.
     *
     * @var data_resul
     */
    public $data_resul;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data_resul)
    {
        $this->data_resul = $data_resul;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('carlostorreszsax@gmail.com')
                    ->view('citas.mails.emergency_call');
    }
}
