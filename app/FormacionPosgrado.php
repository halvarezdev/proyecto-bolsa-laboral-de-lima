<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormacionPosgrado extends Model
{
    protected $table = 'per_posgrado';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'especialidad_id', 'nivel_id', 'centro_id', 'otro_centro', 'fecha_inicio', 'fecha_fin',
        'pers_id', 'fichper_id','postsu_estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

