<?php
//app/Helpers/Envato/User.php
namespace App\Helpers\Envato;

use Illuminate\Support\Facades\DB;
use App\Helpers\Envato;
class Menu {
    /**
     * metodo que se encarga de mostrar la el menu
     * @param int $rol_id
     * 
     * @return array
     */
    
    public static function getAllPrograms($profileID) {
      $EMPUsuario=session('EMPUsuario');
      $modules=array(); 
      $sql='';
        try {
           $sql = "SELECT * FROM (" .
                  " SELECT DISTINCT  prog_border ,prog_modal,  prog_lobiPanelv2, prg.prog_id, prg.modl_id AS moduleID, " .
                  "     mdl.modl_nomb, prg.prog_nomb, prg.prog_icono," .
                  "     prg.prog_form, mdl.modl_orden, prg.prog_orden ,prog_icosp,prog_panel" .
                  " FROM programas prg " .
                  " INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id " .
                  " INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id " .
                  " INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id " .
                  " INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  AND acce_estado = 1" .
                  " INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id ";

                $sql .= "INNER JOIN usuarios usr ON usr.perf_id = prf.perf_id " .
                       "WHERE    sis.sist_activo = 1 
                        AND IF(usr.perf_id='001',TRUE ,prog_modal=1) AND usr.usua_id = '" . $EMPUsuario."'";
            

           
            $sql .=
                   ") tbl " .
                   " ORDER BY  modl_orden, moduleID, prog_orden ";
                    $lista_cond = DB::select( $sql);
          if (count($lista_cond)>0) {
            foreach ($lista_cond as $key => $value) {
               $modules[$value->moduleID][]=$value;
            }
          }
            
        } catch (SQLException $ex) {
               
        }

        return $modules;
    }

    public static function getMenu($EMPUsuario,$profileID) {
        $coment='';
        //$coment=' AND mdl.modl_id in(1,3,2,4,7,9)';//
        $results='';
         $sql = "SELECT * FROM (SELECT DISTINCT prg.prog_id, prg.modl_id AS moduleID, " .
                  "     mdl.modl_nomb, prg.prog_nomb, prg.prog_icono," .
                  "     prg.prog_form, mdl.modl_orden, prg.prog_orden " .
                  " FROM programas prg " .
                  " INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id " .
                  " INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id " .
                  " INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id " .
                  " INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  AND acce_estado = 1" .
                  " INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id ";
            if ($profileID == 0) {
                $sql .= "INNER JOIN usuarios usr ON usr.perf_id = prf.perf_id " .
                       "WHERE sis.sist_activo = 1 AND usr.usua_id = " . $EMPUsuario.$coment;
            } else {
                $sql .= "WHERE sis.sist_activo = 1 ".$coment;
            }

            $sql .=
                    " UNION " .
                   "SELECT DISTINCT mdp.prog_id, mdp.modl_id AS moduleID, " .
                   "     mdl.modl_nomb, prg.prog_nomb, prg.prog_icono," .
                   "     prg.prog_form, mdl.modl_orden, prg.prog_orden " .
                   " FROM programas prg " .
                   " INNER JOIN mod_progs mdp ON prg.prog_id = mdp.prog_id" .
                   " INNER JOIN modulos mdl ON mdp.modl_id = mdl.modl_id " .
                   " INNER JOIN implementacion ipl ON mdp.prog_id = ipl.prog_id " .
                   " INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id" .
                   " INNER JOIN accesos acc ON mdp.prog_id = acc.prog_id AND acce_estado = 1 " .
                   " INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id ";

            if ($profileID == 0) {
                $sql .= " INNER JOIN usuarios usr ON usr.perf_id = prf.perf_id " .
                       " WHERE sis.sist_activo = 1 AND usr.usua_id = " . $EMPUsuario.$coment;
            } else {
                $sql .= " WHERE sis.sist_activo = 1 ".$coment ;
            }
            $sql .= ") tbl GROUP BY moduleID ORDER BY  modl_orden, moduleID, prog_orden ";

        $results = DB::select($sql);
       
            return $results;
    }

    /**
     * metodo que se encarga de mostrar sub menu
     * @param int $rol_id
     * @param String $menu_id
     * @return array
     */
    public static function getMenuSub($userID,$menu_id) {

        $results = DB::select('SELECT DISTINCT prg.prog_id, prg.modl_id AS moduleID, 
                       mdl.modl_nomb, prg.prog_nomb, prg.prog_icono,
                       prg.prog_form, mdl.modl_orden, prg.prog_orden,prg.prog_modal,prg.prog_border, prog_lobiPanelv2
                   FROM programas prg 
                   INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id 
                   INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id 
                   INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id 
                   INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  AND acce_estado = 1
                   INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id 
                    INNER JOIN usuarios usr ON usr.perf_id = prf.perf_id 
                       WHERE sis.sist_activo = 1 AND usr.usua_id = '.$userID.' and  prg.modl_id='.$menu_id.' ORDER BY prg.prog_orden ASC');
        // AND prg.prog_modal = 0
            return $results;
    }
    
    public static function fullCookie($params ='EMPSede'){
        $resultado='';
        try{
        $resul=json_decode(\Request::cookie('Sistema_cookie'));

            if (isset($resul->resultEN)) {
                foreach($resul->resultEN as $plataforma=>$pattern){
                    if ($plataforma==$params) {
                        $resultado= $pattern;
                        break;
                    }
                }
            }
        }catch(Exception $e){
            $resultado='Error: '.$e->getMessage();
        }
        return $resultado;
    }
 public static function UsuarioEmpresa2(){
      $EMPUsuario=session('EMPUsuario');
      $EMPCodigo=session('EMPCodigo');
      $rtn=array();
      $fic=date('Y-m-d');
      $existeuser=array();
      $limit=1;
      $sql1=DB::select('SELECT sist_emplimit  FROM sistemas WHERE sist_activo = 1');
      if (count($sql1)>0) {
        foreach ($sql1 as $key => $value) {
          $limit= $value->sist_emplimit;
        }
      }
      $sql = "SELECT us.usua_usuario,emp.empr_id, empr_nomb, empr_direcc, "
                    ."   empr_ubic, empr_tipo"
                    ." FROM empresas emp"
                    ." INNER JOIN administracion adm ON adm.empr_id = emp.empr_id"
                    ." INNER JOIN usuarios us ON us.usua_id = adm.usua_id "
                    ." WHERE us.usua_id =? "
                    ." AND us.usua_estado = 1"
                    ." ORDER BY emp.empr_id LIMIT 0," . $limit;
        $existeuser1=DB::select( $sql,[$EMPUsuario]);
        $empresa=array();
        if (count($existeuser1)>0) {
            foreach ($existeuser1 as $key => $value) {
               $existeuser[$value->empr_id]=$value->empr_id.' '. $value->empr_nomb;
            }
        }else{
           $existeuser=array();
        }

        return array('informaicon'=>$existeuser,'login'=>$EMPCodigo);
    }
}