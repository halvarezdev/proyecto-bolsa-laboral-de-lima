<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoIdioma extends Model
{
    protected $table = 'ped_idioma';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idioma_nom', 'idioma_nivel', 'pedido_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
