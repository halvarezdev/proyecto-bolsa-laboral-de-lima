<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciiu extends Model
{
    protected $table = 'ciiu';
    /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
 protected $fillable = [
     'V_CODSEC', 'V_CODCIIU', 'V_DESCIIU', 'I_FLGSUNMIN','V_CIIU_SUNAT', 'V_CODCIIUV3', 'V_CODCIIUV4', 'V_DESCIIUV4',
     'V_CODCIIUINEIV4', 'V_DESCIIUINEIV4', 
 ];

 /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
 protected $hidden = [
   //  'password', 'remember_token',
 ];
}
