<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Facades\Lang;

class ResetPasswordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $token;
    private $portEnv;
    private $hostEnv;
    private $usernameEnv;
    private $passwordEnv;
    private $encryptionEnv;
    private $authEnv;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
        $this->portEnv = 587;
        $this->hostEnv = "smtp.gmail.com";
        $this->usernameEnv = "giansolis93@gmail.com";
        $this->passwordEnv = "hewyturhhmdysnrn*";
        $this->encryptionEnv = "SSL";
        $this->authEnv = true;
        $this->mailCopy = "appsmml@munilima.gob.pe";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function email() {
        return view("email");
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = new PHPMailer(true);

        $html = '<h4>Solicitud de restablecimiento de contraseña</h4>';
        $html .= '<p>Se solicitó un restablecimiento de contraseña para tu cuenta ' . $notifiable->getEmailForPasswordReset() . ', haz clic en el botón que aparece a continuación para cambiar tu contraseña.</p>';

        //AQUI EL ENLACE DE RECUPERACION
        $html .= '<a href="'.asset("/").'" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1">Ingrese aqui</a><br><br>';
        
        $html .= 'Si tu no realizaste la solicitud de cambio de contraseña, solo ignora este mensaje.<br><br>Este enlace solo es válido dentro de los proximos 5 minutos.<br><br>';
        $html .= 'Municipalidad de Lima';
        
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $this->hostEnv;       
            $mail->SMTPAuth = $this->authEnv;
            $mail->Username = $this->usernameEnv;  
            $mail->Password = $this->passwordEnv;  
            $mail->SMTPSecure = $this->encryptionEnv;
            $mail->Port = $this->portEnv;

            $correoRemitente = ($this->usernameEnv.date("YmdHis")."@munlima.gob.pe");
 
            $mail->setFrom($this->usernameEnv, 'Solicitud de restablecimiento de contraseña');
            $mail->addAddress($notifiable->getEmailForPasswordReset());
            //$mail->addBCC($this->mailCopy);

            $mail->isHTML(true);
 
            $mail->Subject = Lang::getFromJson('Solicitud de restablecimiento de contraseña');
            $mail->Body    =  $html;
 
            if( !$mail->send() ) {
                echo 'El mensaje no pudo ser enviado.';
            }
            
            else {
                echo 'Mensaje de recuperacion enviado';
                // return back()->with("success", "Email has been sent.");
            }
 
        } catch (Exception $e) {
            echo 'El mensaje no pudo ser enviado. Error: ', $mail->ErrorInfo; 
        }
       
        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
