<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormacionBasica extends Model
{
    protected $table = 'pers_basica';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'perbas_centroestudio', 'grainst_id', 'perbas_fechaini', 'perbas_fecafin', 'pers_id',
        'fichper_id', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

