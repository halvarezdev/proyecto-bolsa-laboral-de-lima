<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use App\Notifications\ContactoResetPasswordNotification;
use App\Notifications\VerifyEmailContacto;
class Admin extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use AuthenticableTrait;

    protected $guard = 'admin';
    protected $table = 'admins';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'tipo_documento', 'numero_documento',
        'cargo', 'telefono','empresa_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
 

  /*  public function persona()
    {
        return $this->hasOne('Persona','user_id');
    }
*/

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ContactoResetPasswordNotification($token));
    }
    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailContacto);
    }


}
