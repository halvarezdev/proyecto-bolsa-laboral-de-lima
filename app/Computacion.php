<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Computacion extends Model
{
    protected $table = 'pers_compu';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'compu_nom', 'compu_nivel', 'pers_id', 'fichper_id','compu_otro',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
