<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoPostulante extends Model
{
    protected $table = 'postulantepedido';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idFicha', 'idPostulante', 'idPedido', 'PP_Fecha', 'PP_FechaColocacion',
        'PP_FechaEnvio', 'PP_Estado', 'PP_Visto','PP_Prosedendia','Fecha_visto', 'fecha', 'hora'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

