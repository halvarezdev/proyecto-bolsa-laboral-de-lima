<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradoInstruccion extends Model
{
    protected $table = 'gradoinstruccion';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'grad_descripcion', 'grad_flag', 'grad_pcd', 'grad_orderby',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
