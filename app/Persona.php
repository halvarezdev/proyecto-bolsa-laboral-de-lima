<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
class Persona extends Model
{
    use Notifiable;
    protected $table = 'personas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_documento','numero_documento','email','name','ape_pat','ape_mat','ubigeo',
        'direccion', 'fecha_nacimiento', 'sexo','telefono',
        'jefe_hogar','discapacidad','estado_civil', 'empresa_id', 'tiene_discapcidad',
        'discapacidad_texto','pers_edad','cod_entidad',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       // 'password', 'remember_token',
    ];

  

}
