<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    /**
     * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'area_descripcion', 'area_estado', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
    //  'password', 'remember_token',
    ];
}
