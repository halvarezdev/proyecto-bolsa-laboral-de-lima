<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReporteEntidad extends Model
{
    protected $table = 'reporte_entidad';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cod_entidad', 'cod_profesion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
