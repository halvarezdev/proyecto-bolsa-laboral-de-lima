<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroEstudio extends Model
{
    protected $table = 'centroestudios';
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'CESTP_CODIGO', 'CESTC_DESCRIPCION', 'CESTC_TIPO', 'CESTC_ESTADO',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      //  'password', 'remember_token',
    ];
}