<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Facebook;

class MarquetingFacebook extends Controller
{

    public function index(){
        return view('marqueting.inicio');
    }

    public function compartirFacebook($formacion){

        $data['metodo'] = Input::get('imprime');

        $formacion_list = Facebook::where('id',$formacion)->get();
        foreach($formacion_list as $form){
            $data['nombre'] = $form->nombre;
            $data['descripcion'] = $form->descripcion;
            $data['image'] = $form->image;
            $data['urlruta'] = $form->urlruta;
            $data['codigo'] = $form->id;
        }


        return view('marqueting.compartir',$data);
    }

    public function guardaDatos(Request $request){

        if($request->hasFile('image')){

            $ruta = $request->file('image')->store('facebook');
            $formacion = Facebook::create([
                'nombre' => $request->nombre,
                'descripcion' => $request->descripcion,
                'image' => $ruta,
                'urlruta' => $request->urlruta,
            ]);

            $file = $request->file('image');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/storage/facebook/', $ruta);

            return redirect()->action('InicioController@busqueda',['entidad' => '1','formacion' => $formacion]);

        }

        echo "ingrese una imagen";
       //

    }

    public function vistaPrevia($formacion){

        $formacion_list = Facebook::where('id',$formacion)->get();
        foreach($formacion_list as $form){
            $data['nombre'] = $form->nombre;
            $data['descripcion'] = $form->descripcion;
            $data['image'] = $form->image;
            $data['urlruta'] = $form->urlruta;
            $data['codido'] = $form->id;
        }
        return view('marqueting.vistaprevia', $data);
    }


}
