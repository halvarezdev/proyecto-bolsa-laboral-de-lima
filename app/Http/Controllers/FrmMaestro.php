<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controles;

class FrmMaestro extends Controller{
    public function get_ubigeo(Request $request){
        $response['status']=0;
		$response['message']='';
        try{  
            $provincias=Controles::get_data_table_simple('SELECT prov_id,prov_nomb FROM provincias WHERE depa_id="'.$request->depa.'"');
            if($provincias['status']==0)
                throw new \Exception($provincias['message']);

            $response['provincia']=$provincias['response'];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response; 
    }

    public function get_ubigeodis(Request $request){
        $response['status']=0;
		$response['message']='';
        try{  
            $distritos=Controles::get_data_table_simple('SELECT dist_id,dist_nomb FROM distritos WHERE depa_id="'.$request->depa.'" AND prov_id="'.$request->prov.'"');
            if($distritos['status']==0)
                throw new \Exception($distritos['message']);

            $response['distrito']=$distritos['response'];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }

    public function ubigeo(Request $request){
        $response['status']=0;
		$response['message']='';
        try{  
            $provincias=Controles::get_data_table_simple('SELECT prov_id,prov_nomb FROM provincias WHERE depa_id="'.$request->depa.'"');
            if($provincias['status']==0)
                throw new \Exception($provincias['message']);

            $distritos=Controles::get_data_table_simple('SELECT dist_id,dist_nomb FROM distritos WHERE depa_id="'.$request->depa.'" AND prov_id="'.$request->prov.'"');
            if($distritos['status']==0)
                throw new \Exception($distritos['message']);

            $response['distrito']=$distritos['response'];
            $response['provincia']=$provincias['response'];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response; 
    }
}