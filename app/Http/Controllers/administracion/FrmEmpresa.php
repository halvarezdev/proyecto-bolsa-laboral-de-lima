<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
class FrmEmpresa extends Controller{

    public function index(Request $request){
        $response['titulo']='Empresa';  
          
        $response['status']=0;
		$response['message']='';
        try{  
           
            $data=$this->get_paginate_empresa($request->all());
            if($data['status']==0)
                throw new \Exception($data['message']);
            

            $response['data']=$data;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        if($response['status']==0){
            if($request->usua==1){
                print_r($response);
                exit();
            }
        } 
        return view('administracion.frmtienda_index',$response);
    }

    function get_tiendaGroup(Request $request){
        $report=$request->method;
        $nombreclase = "\FrmEmpresa::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }

    function get_edit($request){
        $response['titulo']='Empresa'; 
        $response['status']=0;
		$response['message']='';
        try{  
            

            $zona_tiendas=Controles::get_data_table_simple('SELECT zone_id,zone_nomb FROM zona_tiendas WHERE zone_estado=1 ORDER BY zone_id');
            if($zona_tiendas['status']==0)
                throw new \Exception($zona_tiendas['message']);

            foreach ($zona_tiendas['response'] as $key => $value) {
                $row_zona_tiendas[$value->zone_id]=$value->zone_nomb;
            }
            $result=array();
            if($request['id']!=''){
                $sql='SELECT empr_direcc,empr_telf,fisc_dire,d.fisc_id,zone_id,empr_seriedoc,e.empr_id,empr_social,DATE_FORMAT(empr_fecinvIns,"%d/%m/%Y")as fecha,empr_nomb,empr_abrev,
                empr_ruc,empr_tipo,empr_estado,empr_ctabanca1,empr_ctabanca2,empr_ctabanca3
                 FROM empresas e
                 LEFT JOIN direc_fiscal as d ON e.dirf_id=d.fisc_id
                 WHERE e.empr_id=?';
                $row=DB::select($sql,[$request['id']]);
                $result=$row[0];
                $nex_id['response']=$request['id'];
            }else{
                $nex_id=Controles::nextIDTable('empresas','empr_id',2);
                if($nex_id['status']==0)
                    throw new \Exception($nex_id['message']);
            }
            
            $response['response']=$result;
            $response['nex_id']=$nex_id['response'];
            
            $response['request']=$request;
            $response['zona_tiendas']=$row_zona_tiendas;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
            print_r($response);
        }
         
        return view('administracion.frmtienda_now',$response);
    }

    public function get_save($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $param="'".$request['id']."','".$request['rzon']."','".$request['nmb'];
            $param.="','".$request['abr']."','".$request['ruc']."','".$request['tipo']."','".$request['tf']."','".$request['fecinv']."','".$request['dire']."','".$request['zona'];
            $param.="','".$request['dirfis']."','".$request['serie']."','".$request['cta1']."','".$request['cta2']."','".$request['cta3']."'";
            $sql='CALL sp_save_empresa('.$param.')';
            $row=DB::select($sql);
            $response['response']=$row[0];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;    
    }
    
    function get_paginate_empresa($request){
        $response['status']=0;
		$response['message']='';
        try{ 
           
            $html='';
            $Lista =DB::table('empresas')
            ->select(DB::raw('empr_id,empr_estado,empr_seriedoc,empr_social,empr_nomb,empr_abrev,empr_ruc,empr_direcc,empr_telf,empr_tipo'))
            
            ->orderByRaw('empr_id')
            ->paginate(40);
            if (count($Lista)>0) {
                foreach ($Lista as $key => $rst) {
                    $html.='<tr  id="table_tr_td_cliente'.$rst->empr_id.'">';
                    $html.='<td >'.$rst->empr_id.'</td>';
                    $html.='<td >'.$rst->empr_social.'</td>';
                    
                    $html.='<td >'.$rst->empr_abrev.'</td>';
                    $html.='<td >'.$rst->empr_ruc.'</td>';     
                    
                    $html.='<td >'.$rst->empr_telf.'</td>';     
                    $html.='<td >'.$rst->empr_tipo.'</td>';     
                    $html.='<td >'.$rst->empr_seriedoc.'</td>';     
                    $html.='<td class="estadodocu_'.$rst->empr_estado.'">'.($rst->empr_estado==1?'Activo':'Anulado').'</td>';
                    $html.='<td><button title="Editar Empresa" onclick="edit_frmtienda(this,'."'".$rst->empr_id."'".','.$rst->empr_estado.')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-edit"></i></a></td>';
                    $html.='<td><button title="Anular Empresa"  onclick="del_frmtienda(this,'."'".$rst->empr_id."'".','.$rst->empr_estado.')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-cancel"></i></a></td>';
                    $html.='<td><button title="Configurar Empresa"  onclick="confi_frmtienda(this,'."'".$rst->empr_id."'".','.$rst->empr_estado.')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-sunat"></i></a></td>';
                    $html.='</tr>';
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"6\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="6">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
		    $response['table']=$initial_content;
		    $response['theadPagin']=$initial_content2;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
      
    }
}