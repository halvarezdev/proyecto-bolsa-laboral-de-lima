<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;

class Frmcliente extends Controller{

    public function index($tipo,Request $request){
        $response['titulo']='Cliente';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $obj=array('cliente'=>'',
            'direcio'=>'','codigo'=>'','zona'=>'','ruc'=>'','tipo'=>$tipo,'orderby'=>'');
            $acciones=Controles::get_acciones($request->perf_id,$request->prog_id);
            if($acciones['status']==0)
                throw new \Exception($acciones['message']);
            
            $response['funciones']=$acciones['row_funciones'];
            $response['acciones']=$acciones['response'];
            
            $data=$this->get_paginate_cliente($obj,$request->all());
            if($data['status']==0)
                throw new \Exception($data['message']);
            
            $row_tipoPersona=array(''=>'TODOS');
            $tipoPersona=Controles::get_data_table_simple('SELECT tppt_tipodoc,tppt_nomb 
            FROM tb_tipo_persona  WHERE tppt_estado=1  ORDER BY tppt_orderby');
            if($tipoPersona['status']==0)
                throw new \Exception($tipoPersona['message']);
            
            foreach ($tipoPersona['response'] as $key => $value) {
                $row_tipoPersona[$value->tppt_tipodoc]=$value->tppt_nomb;
            }
            $response['data']=$data;
            $response['tipo']=$tipo;
            $response['row_tipoPersona']=$row_tipoPersona;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        if($response['status']==0){
            if($request->usua==1){
                print_r($response);
                exit();
            }
        } 
        return view('administracion.cliente_index',$response);
        
    }
    function get_clienteGrupo(Request $request){
        $report=$request->method;
        $nombreclase = "\Frmcliente::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function get_edit($request){
        $response['titulo']='Cliente';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $tipoPersona=Controles::get_data_table_simple('SELECT tppt_tipodoc,tppt_nomb 
            FROM tb_tipo_persona  WHERE tppt_estado=1  ORDER BY tppt_orderby');
            if($tipoPersona['status']==0)
                throw new \Exception($tipoPersona['message']);
            
            foreach ($tipoPersona['response'] as $key => $value) {
                $row_tipoPersona[$value->tppt_tipodoc]=$value->tppt_nomb;
            }
      
            $row_zonacliente=array('0'=>'SELECCIONE');
            $zonacliente=Controles::get_data_table_simple('SELECT  zcl_id,zcl_nomb,zcl_estado 
            FROM zonas_cliente WHERE zcl_estado=1 ORDER BY zcl_id ASC');
            if($zonacliente['status']==0)
                throw new \Exception($zonacliente['message']);
            
            foreach ($zonacliente['response'] as $key => $value) {
                $row_zonacliente[$value->zcl_id]=$value->zcl_nomb;
            }
            $tipo_domt_persona=Controles::get_data_table_simple('SELECT tdp_id,tdp_nomb FROM tipo_domt_persona WHERE tdp_estado=1 ORDER BY tdp_orderby');
            if($tipo_domt_persona['status']==0)
                throw new \Exception($tipo_domt_persona['message']); 

            foreach ($tipo_domt_persona['response'] as $key => $value) {
                $row_tipo_domt_persona[$value->tdp_id]=$value->tdp_nomb;
            }
            $response['response']=array();
            $response['direccion']='';
            if($request['id']!='0'){
                $sql='SELECT clie_ruta,zcl_id,clie_vendedor,clie_idalterno,clie_premisse,clie_linea,clie_categoria,clie_giro,clie_ugi,clie_id,clie_ruc,clie_nomb,clie_repre,
                clie_obs,clie_tipo,clie_tipdoc,clie_estado ,DATE_FORMAT(clie_fecreg,"%d/%m/%Y")as fecha
                FROM clientes WHERE  clie_id=?';
                $row=DB::select($sql,[$request['id']]);
                $response['response']=$row[0];
                $direcion=$this->get_direccion_cli($request['id']);
                
                if($direcion['status']==0)
                    throw new \Exception($direcion['message']);

                $response['direccion']=$direcion['response'];
                $nex_id=Controles::nextIDTable('clientes','clie_idalterno',6);
                if($nex_id['status']==0)
                    throw new \Exception($nex_id['message']);
            }else{
                $nex_id=Controles::nextIDTable('clientes','clie_idalterno',6);
                if($nex_id['status']==0)
                    throw new \Exception($nex_id['message']);

                $row=new \stdClass();
                $row->clie_idalterno=$nex_id['response'];
                $response['response']=$row;
            }
            $row_vendedor=array('0'=>'SELECCIONE');
            $vendedor=Controles::get_data_table_simple("SELECT clie_id ,clie_nomb
            FROM clientes  WHERE clie_tipo IN('E','T','V') AND clie_estado=1");
            if($vendedor['status']==0)
                throw new \Exception($vendedor['message']);
            
            foreach ($vendedor['response'] as $key => $value) {
                $row_vendedor[$value->clie_id]=$value->clie_nomb;
            }
            $response['nex_id']=$nex_id['response'];
            $response['request']=$request;
            $response['row_vendedor']=$row_vendedor;
            $response['tipo_persona']=$row_tipoPersona;
            $response['row_zonacliente']=$row_zonacliente;
            $response['tipo_domt_persona']=$row_tipo_domt_persona;

            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        if($response['status']==0){
            if($request['usert']==1){
                print_r($response);
                exit();
            }
        } 
        return view('administracion.cliente_nowedit',$response);
    }
    public function get_clidirecion($request){
        $response['status']=0;
		$response['message']='';
        try{ 
              
            $sql='SELECT dirs_telf, dirs_celular,dirs_email,dirs_id,dirs_nomb,dirs_contac,dirs_estado
            FROM direcciones WHERE  clie_id=?  ORDER BY dirs_id ';
            $row_dir=DB::select($sql,[$request['id']]);
            $response['response_dir']=$row_dir;
            
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;   
    }
    public function get_cliide($request){
        $response['status']=0;
		$response['message']='';
        try{ 
            $sql='SELECT clie_ruta,zcl_id,clie_vendedor,clie_idalterno,clie_premisse,clie_linea,clie_categoria,clie_giro,clie_ugi,clie_id,clie_ruc,clie_nomb,clie_repre,
                    clie_obs,clie_tipo,clie_tipdoc,clie_estado ,DATE_FORMAT(clie_fecreg,"%d/%m/%Y")as fecha
                    FROM clientes WHERE  clie_id=?';
            $row=DB::select($sql,[$request['id']]);
            if($row>0){
                $response['status']=0;
                $response['response']=$row[0];
                $sql='SELECT dirs_telf, dirs_celular,dirs_email,dirs_id,dirs_nomb,dirs_contac,dirs_estado
                FROM direcciones WHERE  clie_id=? ORDER BY dirs_id ';
                $row_dir=DB::select($sql,[$request['id']]);
                $response['response_dir']=$row_dir;
            } 
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;    
    }
    public function get_buscarcliente($request){
        $response['status']=0;
		$response['message']='';
        try{  
          
            $sql='SELECT clie_id,clie_ruc,clie_nomb,clie_repre,
            clie_obs,clie_tipo,clie_tipdoc,clie_estado ,DATE_FORMAT(clie_fecreg,"%d/%m/%Y")as fecha
            FROM clientes WHERE  clie_ruc=?';
            $row=DB::select($sql,[$request['id']]);
            $response['response']=$row[0];
            $direcion=$this->get_direccion_cli($row[0]->clie_id);                
            if($direcion['status']==0)
                throw new \Exception($direcion['message']);

            $response['direccion']=$direcion['response'];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
          
    }
    public function get_save($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $ugi=$request['ugi'];
            $categoria=$request['categoria'];
            $linea=$request['linea'];
            $premisse=$request['premisse'];
            $giro=$request['giro'];
            $ruta=$request['ruta'];
            $vendedor=$request['vendedor'];
            $cdalt=$request['cdalt'];
            $zonacli=$request['zonacli'];
          
            $param="'".$request['oper']."',".$request['id'].",'".$request['razonso'];
            $param.="','".$request['tipdocm']."','".$request['tipo']."','".$request['ruc']."','".$request['repres']."','".$request['feingr']."','".$request['obser']."','".$request['direccion']."'";
            $param.=",'$ugi','$categoria','$linea','$premisse','$giro','$ruta',$vendedor,'$cdalt','$zonacli'";
            $sql='CALL sp_save_cliente('.$param.')';
            $row=DB::select($sql);
            $response['response']=$row[0];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function get_next_codalt($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $nex_id=Controles::nextIDTable('clientes WHERE clie_tipo='."'".$request['tipo']."'",'clie_idalterno',6);
            if($nex_id['status']==0)
                throw new \Exception($nex_id['message']);
            $response['nex_id']=$nex_id['response']; 
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;

    }
    public function del_cliente($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $post=['clie_estado'=>9];
            $row=DB::table('clientes')
            ->where('clie_id',$request['id']) 
            ->update($post); 
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function get_direccion_cli($id){
        $response['status']=0;
		$response['message']='';
        try{  
            $html='';
            $sql='SELECT dirs_telf, dirs_celular,dirs_email,dirs_id,dirs_nomb,dirs_contac,dirs_estado
            FROM direcciones WHERE  clie_id=? ORDER BY dirs_id ';
            $row=DB::select($sql,[$id]);
            foreach ($row as $key => $it) {
                $html.='<tr id="table_tr_td_frmdirecion'.$it->dirs_id.'">';
                $html.='<td>'.$it->dirs_id.'</td>';
                $html.='<td>'.$it->dirs_nomb.'</td>';
                $html.='<td>'.$it->dirs_email.'</td>';
                $html.='<td>'.$it->dirs_celular.'</td>';
                $html.='<td>'.$it->dirs_telf.'</td>'; 
                $html.='<td class="estadodocu_'.$it->dirs_estado.'">'.($it->dirs_estado==1?'Activo':'Anulado').'</td>';
                $html.='<td><a href="#" title="Editar Dirección" onclick="getEditarDirCliente_dircli(this,'."'".$it->dirs_id."','".$it->dirs_estado."'".')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-edit"></i></a></td>';
                $html.='<td><a href="#" title="Anular Dirección"  onclick="ModalEliminarDirCliente_dir(this,'."'".$it->dirs_id."','".$it->dirs_estado."'".')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-cancel"></i></a></td>';
                
                $html.='</tr>';
            }
            
            $response['response']=$html;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function get_searchclie($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $data=$this->get_paginate_cliente($request,$request);
            if($data['status']==0)
                throw new \Exception($data['message']);

            $response['response']=$data;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
       
    }
    function get_paginate_cliente($request){
        $response['status']=0;
		$response['message']='';
        try{ 
            $where=' cli.clie_id IS NOT NULL';
            if($request['cliente']!=''){
                $union='';
                $tex=explode('*', $request['cliente']);
                $cn=count($tex);
                if ($cn>0) {
                    for ($i=0; $i < $cn; $i++) { 
                        $union.=$tex[$i]."%";
                    }
                    $where.=' AND (clie_nomb LIKE '."'".$union."')"  ;
                }else{
                    $where.=' AND (clie_nomb LIKE '."'%".$request['cliente']."%')"  ;
                }
                
            }
            if($request['ruc']!="")
                $where.=' AND clie_ruc LIKE '."'%".$request['ruc']."%'";

            if($request['zona']!="")
                $where.=' AND zona_nomb LIKE '."'%".$request['zona']."%'";

            if($request['codigo']!="")
                $where.=' AND clie_idalterno LIKE '."'%".$request['codigo']."%'"; 
            
            if($request['direcio']!="")
                $where.=' AND dirs_nomvia LIKE '."'%".$request['direcio']."%'";
                
               
            if($request['tipo']!="")
                $where.=' AND tb_tipo_persona.tppt_tipodoc='."'".$request['tipo']."'";

            $html='';
            $Lista =DB::table(DB::raw('clientes AS cli'))
            ->select(DB::raw('LEFT(dirs_nomvia,30)AS direcc,LEFT(zona_nomb,10)zonnn,LEFT(tppt_nomb,10) AS tipocli,dirs_nomvia,cli.clie_id,clie_nomb,clie_ruc,clie_repre,clie_estado'))
            ->leftJoin('tb_tipo_persona','tb_tipo_persona.tppt_tipodoc','=','cli.clie_tipo')
              ->leftJoin('direcciones','direcciones.clie_id','=','cli.clie_id')
              ->leftJoin('zonas','zonas.zona_id','=','direcciones.zona_id')
            ->whereRaw($where)
            ->orderByRaw($request['orderby']==''?'clie_nomb':$request['orderby'])
            ->paginate(40);
            if (count($Lista)>0) {
                $item=0;
                foreach ($Lista as $key => $rst) {
                    $html.='<tr id="table_tr_td_cliente'.$rst->clie_id.'">';
                    $html.='<td >'.$rst->clie_nomb.'</td>';
                    $html.='<td >'.$rst->clie_ruc.'</td>';
                    $html.= "<td >" . ($rst->direcc) . "</td>"
                    . "<td >" . ($rst->zonnn) . "</td>"
                    . "<td >" .$rst->tipocli . "</td>";    
                    $html.='<td class="estadodocu_'.$rst->clie_estado.'">'.($rst->clie_estado==1?'Activo':'Anulado').'</td>';
                    $html.='<td class="disable_btn_01"><button disabled type="button" title="Editar"  onclick="edit_frmcliente(this,'."'".$rst->clie_id."'".','.$rst->clie_estado.')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-edit"></i></button></td>';
                     $html.='<td class="disable_btn_02"><button disabled type="button" title="Anular cliente"  onclick="del_frmcliente(this,'."'".$rst->clie_id."'".','.$rst->clie_estado.')"class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-cancel"></i></button></td>';
                    $html.='<td><div class="dropdown-success dropdown open">
                    <button class="btn btn-info btn-outline-info btn-mini waves-effect context-menu" type="button"  
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="true"  
                     data-title="'.$rst->clie_nomb.'"
                     data-json-parametro="'.$rst->clie_id.'|'.$rst->clie_estado.'"
                     data-container-id="context-menu-items-frmcliente"
                     data-row-id="'.$rst->clie_id.'_frmcliente"></button>
                    <div class="dropdown-menu" aria-labelledby="dropdown-3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <a class="dropdown-item waves-effect waves-light" href="#">Action</a> 
                    </div>
                </div></td>';
                    $html.='</tr>';
                    //context-menu
                    $item++;
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"9\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="9">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
		    $response['table']=$initial_content;
		    $response['theadPagin']=$initial_content2;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
      
    }
}