<?php

namespace App\Http\Controllers\administracion;
 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;

class FrmDirecciones extends Controller{

    function get_groupdirec(Request $request){
        $report=$request->method;
        $nombreclase = "\FrmDirecciones::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }

    public function get_now($request){
        $response['titulo']='Direccion';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            
            $nex_id=Controles::nextIDTable('direcciones WHERE clie_id='.$request['idcl'],'dirs_id',2);
            if($nex_id['status']==0)
                throw new \Exception($nex_id['message']);

            $depa_id=15;
            $prov_id='01';
            $row_dir=array();

            if($request['id']!='0'){
                $sql='SELECT dirs_longitude,dirs_latitude,dirs_ref,dirs_ubicacion,dir.dirs_id,dir.clie_id,z.zona_id, depa_id,prov_id,dist_id,dirs_estado, 
                dirs_contac,dirs_nomvia, dirs_nomb, 
                dir.empr_id, emp.empr_nomb,zona_nomb, dirs_telf,dirs_celular, dirs_email  
                FROM direcciones  dir 
                LEFT JOIN empresas emp ON emp.empr_id = dir.empr_id
                LEFT JOIN zonas z on z.zona_id = dir.zona_id
                    WHERE  dir.clie_id=? AND  dir.dirs_id="'.$request['id'].'"';
                $row=DB::select($sql,[$request['idcl']]);
                $row_dir=$row[0];

                $depa_id=$row_dir->depa_id;
                $prov_id=$row_dir->prov_id;
                
            }
            $row_distritos=array();
            $row_provincias=array();
            $departamentos=Controles::get_data_table_simple('SELECT depa_id,depa_nomb FROM departamentos');
            if($departamentos['status']==0)
                throw new \Exception($departamentos['message']);
            
            foreach ($departamentos['response'] as $key => $value) {
                $row_departamentos[$value->depa_id]=$value->depa_nomb;
            }
            $provincias=Controles::get_data_table_simple('SELECT prov_id,prov_nomb FROM provincias WHERE depa_id="'.$depa_id.'"');
            if($provincias['status']==0)
                throw new \Exception($provincias['message']);
            
            foreach ($provincias['response'] as $key => $value) {
                $row_provincias[$value->prov_id]=$value->prov_nomb;
            }
            $distritos=Controles::get_data_table_simple('SELECT dist_id,dist_nomb FROM distritos WHERE depa_id="'.$depa_id.'" AND prov_id="'.$prov_id.'"');
            if($distritos['status']==0)
                throw new \Exception($distritos['message']);
            
            foreach ($distritos['response'] as $key => $value) {
                $row_distritos[$value->dist_id]=$value->dist_nomb;
            }

            $response['nex_id']=$nex_id['response'];
            $response['departamentos']=$row_departamentos;
            $response['provincias']=$row_provincias;
            $response['distritos']=$row_distritos;
            $response['request']=$request;
            $response['response']=$row_dir;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        if($response['status']==0){
            if($request['usert']==1){
                print_r($response);
                exit();
            }
        } 
        return view('administracion.direccion_clien',$response);
        
    }
    public function get_save($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $ubic=$request['ubic'];
            $ref=$request['ref'];
            $longitude=$request['longitude'];
            $latitude=$request['latitude'];
            $param="'".$request['id']."','".$request['idcli']."','".$request['contacto']."','".$request['tienda']."','".$request['zonas']."','".$request['depa']."','".$request['prov']."','".$request['distri'];
            $param.="','".$request['email']."','".$request['celular']."','".$request['telefono']."','".$request['nombreVia']."','".$request['direccion']."'";
            $param.=",'$ubic','$ref','$longitude','$latitude'";
            $sql='CALL sp_save_direcciones('.$param.')';
            $row=DB::select($sql);
            $response['response']=$row[0];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;    
    }
    public function del_direc($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $post=['dirs_estado'=>9];
            $row=DB::table('direcciones')
            ->where('dirs_id',$request['id']) 
            ->where('clie_id',$request['idlci']) 
            ->update($post); 
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
}