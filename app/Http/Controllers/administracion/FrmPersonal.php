<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;

class FrmPersonal extends Controller{

    public function index($tipo,Request $request){
        $response['titulo']='Cliente';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $obj=array('cliente'=>'','ruc'=>'','tipo'=>$tipo,'orderby'=>'');
            $acciones=Controles::get_acciones($request->perf_id,$request->prog_id);
            if($acciones['status']==0)
                throw new \Exception($acciones['message']);
            
            $response['funciones']=$acciones['row_funciones'];
            $response['acciones']=$acciones['response'];
            
            $data=$this->get_paginate_cliente($obj,$request->all());
            if($data['status']==0)
                throw new \Exception($data['message']);
            
            $row_tipoPersona=array(''=>'TODOS');
            $tipoPersona=Controles::get_data_table_simple('SELECT tppt_tipodoc,tppt_nomb 
            FROM tb_tipo_persona  WHERE tppt_estado=1  ORDER BY tppt_orderby');
            if($tipoPersona['status']==0)
                throw new \Exception($tipoPersona['message']);
            
            foreach ($tipoPersona['response'] as $key => $value) {
                $row_tipoPersona[$value->tppt_tipodoc]=$value->tppt_nomb;
            }
            $response['data']=$data;
            $response['tipo']=$tipo;
            $response['row_tipoPersona']=$row_tipoPersona;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        if($response['status']==0){
            if($request->usua==1){
                print_r($response);
                exit();
            }
        } 
        return view('administracion.personal_index',$response);
        
    }
    function get_personagrupo(Request $request){
        $report=$request->method;
        $nombreclase = "\FrmPersonal::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function get_edit($request){
        $response['titulo']='Cliente';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $tipoPersona=Controles::get_data_table_simple('SELECT tppt_tipodoc,tppt_nomb 
            FROM tb_tipo_persona  WHERE tppt_estado=1  ORDER BY tppt_orderby');
            if($tipoPersona['status']==0)
                throw new \Exception($tipoPersona['message']);
            
            foreach ($tipoPersona['response'] as $key => $value) {
                $row_tipoPersona[$value->tppt_tipodoc]=$value->tppt_nomb;
            }

            $tipo_domt_persona=Controles::get_data_table_simple('SELECT tdp_id,tdp_nomb FROM tipo_domt_persona WHERE tdp_estado=1 ORDER BY tdp_orderby');
            if($tipo_domt_persona['status']==0)
                throw new \Exception($tipo_domt_persona['message']); 

            foreach ($tipo_domt_persona['response'] as $key => $value) {
                $row_tipo_domt_persona[$value->tdp_id]=$value->tdp_nomb;
            }
            $response['response']=array();
            $response['direccion']='';
            if($request['id']!='0'){
                $sql='SELECT clie_vendedor,clie_idalterno,clie_premisse,clie_sueldo,clie_correo,clie_giro,clie_telefono,clie_id,clie_ruc,clie_nomb,clie_repre,
                clie_obs,clie_tipo,clie_tipdoc,clie_estado ,DATE_FORMAT(clie_fecreg,"%d/%m/%Y")as fecha
                FROM clientes WHERE  clie_id=?';
                $row=DB::select($sql,[$request['id']]);
                $response['response']=$row[0];
                 
                $nex_id=Controles::nextIDTable('clientes','clie_idalterno',6);
                if($nex_id['status']==0)
                    throw new \Exception($nex_id['message']);
            }else{
                $nex_id=Controles::nextIDTable('clientes','clie_idalterno',6);
                if($nex_id['status']==0)
                    throw new \Exception($nex_id['message']);

                $row=new \stdClass();
                $row->clie_idalterno=$nex_id['response'];
                $response['response']=$row;
            }
            
            $response['nex_id']=$nex_id['response'];
            $response['request']=$request; 
            $response['tipo_persona']=$row_tipoPersona;
            $response['tipo_domt_persona']=$row_tipo_domt_persona;

            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        if($response['status']==0){
            if($request['usert']==1){
                print_r($response);
                exit();
            }
        } 
        return view('administracion.persona_nowedit',$response);
    }
    public function get_buscarcliente($request){
        $response['status']=0;
		$response['message']='';
        try{  
          
            $sql='SELECT clie_id,clie_ruc,clie_nomb,clie_repre,
            clie_obs,clie_tipo,clie_tipdoc,clie_estado ,DATE_FORMAT(clie_fecreg,"%d/%m/%Y")as fecha
            FROM clientes WHERE  clie_ruc=?';
            $row=DB::select($sql,[$request['id']]);
            $response['response']=$row[0];
            $direcion=$this->get_direccion_cli($row[0]->clie_id);                
            if($direcion['status']==0)
                throw new \Exception($direcion['message']);

            $response['direccion']=$direcion['response'];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
          
    }
    public function get_save($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $correo=$request['correo'];
            $celular=$request['celular'];
            $sueldo=$request['sueldo'];
            $premisse=$request['premisse'];
            $giro=$request['giro'];
            $ruta=$request['ruta'];
            $vendedor=$request['vendedor'];
            $cdalt=$request['cdalt'];
            
            $param="'".$request['oper']."',".$request['id'].",'".$request['razonso'];
            $param.="','".$request['tipdocm']."','".$request['tipo']."','".$request['ruc']."','".$request['repres']."','".$request['feingr']."','".$request['obser']."','".$request['direccion']."'";
            $param.=",'$correo','$celular','$sueldo','$premisse','$giro','$ruta',$vendedor,'$cdalt'";
            $sql='CALL sp_save_personal('.$param.')';
            $row=DB::select($sql);
            $response['response']=$row[0];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function get_next_codalt($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $nex_id=Controles::nextIDTable('clientes WHERE clie_tipo='."'".$request['tipo']."'",'clie_idalterno',6);
            if($nex_id['status']==0)
                throw new \Exception($nex_id['message']);
            $response['nex_id']=$nex_id['response']; 
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;

    }
    public function del_cliente($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $post=['clie_estado'=>9];
            $row=DB::table('clientes')
            ->where('clie_id',$request['id']) 
            ->update($post); 
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    
    public function get_searchclie($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $data=$this->get_paginate_cliente($request,$request);
            if($data['status']==0)
                throw new \Exception($data['message']);

            $response['response']=$data;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
       
    }
    function get_paginate_cliente($request,$request2){
        $response['status']=0;
		$response['message']='';
        try{ 
            $where=' cli.clie_id IS NOT NULL';
            if($request['cliente']!=''){
                $union='';
                $tex=explode('*', $request['cliente']);
                $cn=count($tex);
                if ($cn>0) {
                    for ($i=0; $i < $cn; $i++) { 
                        $union.=$tex[$i]."%";
                    }
                    $where.=' AND (clie_nomb LIKE '."'".$union."')"  ;
                }else{
                    $where.=' AND (clie_nomb LIKE '."'%".$request['cliente']."%')"  ;
                }
                
            }
            if($request['ruc']!="")
                $where.=' AND clie_ruc ILIKE '."'%".$request['ruc']."%'";
            
            if($request['tipo']!="")
                $where.=' AND tb_tipo_persona.tppt_tipodoc='."'".$request['tipo']."'";
            $html='';
            $Lista =DB::table(DB::raw('clientes AS cli'))
            ->select(DB::raw('clie_correo,clie_telefono,LEFT(tppt_nomb,10) AS tipocli,cli.clie_id,clie_nomb,clie_ruc,clie_repre,clie_estado'))
            ->leftJoin('tb_tipo_persona','tb_tipo_persona.tppt_tipodoc','=','cli.clie_tipo') 
            ->whereRaw($where)
            ->orderByRaw($request['orderby']==''?'clie_nomb':$request['orderby'])
            ->paginate(40);
            if (count($Lista)>0) {
                $item=0;
                foreach ($Lista as $key => $rst) {
                    $html.='<tr id="table_tr_td_personal'.$rst->clie_id.'">';
                    $html.='<td >'.$rst->clie_nomb.'</td>';
                    $html.='<td >'.$rst->clie_ruc.'</td>';
                    $html.= "<td >" . ($rst->clie_correo) . "</td>"
                    . "<td >" . ($rst->clie_telefono) . "</td>"
                    . "<td >" .$rst->tipocli . "</td>";    
                    $html.='<td class="estadodocu_'.$rst->clie_estado.'">'.($rst->clie_estado==1?'Activo':'Anulado').'</td>';
                    $html.='<td class="disable_btn_01"><button disabled type="button" title="Editar"  onclick="edit_frmpersonal(this,'."'".$rst->clie_id."'".','.$rst->clie_estado.')" class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-edit"></i></button></td>';
                     $html.='<td class="disable_btn_02"><button disabled type="button" title="Anular personal"  onclick="del_frmpersonal(this,'."'".$rst->clie_id."'".','.$rst->clie_estado.')"class="btn btn-info btn-outline-info btn-mini waves-effect"><i class="spw spw-cancel"></i></button></td>';
                    $html.='<td><div class="dropdown-success dropdown open">
                    <button class="btn btn-info btn-outline-info btn-mini waves-effect context-menu" type="button"  
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="true"  
                     data-title="'.$rst->clie_nomb.'"
                     data-json-parametro="'.$rst->clie_id.'|'.$rst->clie_estado.'"
                     data-container-id="context-menu-items-frmpersonal"
                     data-row-id="'.$rst->clie_id.'_frmpersonal"></button>
                    <div class="dropdown-menu" aria-labelledby="dropdown-3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <a class="dropdown-item waves-effect waves-light" href="#">Action</a> 
                    </div>
                </div></td>';
                    $html.='</tr>';
                    //context-menu
                    $item++;
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"9\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="9">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
		    $response['table']=$initial_content;
		    $response['theadPagin']=$initial_content2;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
      
    }
}