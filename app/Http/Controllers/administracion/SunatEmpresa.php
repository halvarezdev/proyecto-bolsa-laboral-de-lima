<?php

namespace App\Http\Controllers\administracion;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
 
 
class SunatEmpresa extends Controller{
		public function getIndex(Request $request){
			$metodo=$request->metodo;
			if ($metodo=='CargarModalSunat') {
				return $this->CargarModalSunat($request->id);
			}elseif($metodo=='Process_correo'){
				$data['info']=$this->Process_correo($request->all());
				return json_encode($data);
			}
		}
		public function Process_correo($request){
			$idEmpresa=$request['idEmpresa'];
			$idDocume=$request['_partpdc_tipodoc'];
			$tipoDocu=$request['_parfact_nrodoc'];
			$arayInfo= new \stdClass();
			$Lista=DB::select("call spw_facturacionelCorreoElect('$idEmpresa','$idDocume','$tipoDocu')");
			 if (count($Lista)>0){
	        	foreach ($Lista as $key => $value) {
	        		$arayInfo=$value;
	        	}
	        }
	        return $arayInfo;
		}
		public function CargarModalSunat($id=''){
			$arayInfo=array();
            $Lista=DB::select("SELECT DATE_FORMAT(empr_feahcvencert,'%d/%m/%Y') AS feahcvencert, empr_tipo_decorreo,
			empr_mailpass,empr_mailcorreo,empr_mailport,empr_mailhost, 
			empr_correo, empr_certificado,empr_ruc,empr_betasunat,empr_logoFac, empr_claveCert,
			 empr_claveUsuario,empr_id,empr_social,empr_nomb,empr_claveSol from empresas   where empr_id='".$id."'");
	        if (count($Lista)>0){
	        	foreach ($Lista as $key => $value) {
	        		$arayInfo=$value;
	        	}
	        }
			$empresa_cer=DB::select("SELECT cers_serialNumber,
			DATE_FORMAT(cers_valid_desde,'%d/%m/%Y') AS desde ,
			DATE_FORMAT(cers_valid_hasta,'%d/%m/%Y') AS hasta 
			
			  from empresa_cersunat   where cers_empid='$id'");

				$data['empresa_cer']=$empresa_cer;

			$data['empr_tipo_decorreo']=isset($arayInfo->empr_tipo_decorreo)?$arayInfo->empr_tipo_decorreo:'1';
			$data['empr_mailpass']=isset($arayInfo->empr_mailpass)?$arayInfo->empr_mailpass:'';
			$data['empr_mailcorreo']=isset($arayInfo->empr_mailcorreo)?$arayInfo->empr_mailcorreo:'';
			$data['empr_mailport']=isset($arayInfo->empr_mailport)?$arayInfo->empr_mailport:'';
			$data['empr_mailhost']=isset($arayInfo->empr_mailhost)?$arayInfo->empr_mailhost:''; 
			$data['feahcvencert']=isset($arayInfo->feahcvencert)?$arayInfo->feahcvencert:'';
			$data['empr_ruc']=isset($arayInfo->empr_ruc)?$arayInfo->empr_ruc:'';
			$data['empr_betasunat']=isset($arayInfo->empr_betasunat)?$arayInfo->empr_betasunat:'';
			$data['empr_logoFac']=isset($arayInfo->empr_logoFac)?$arayInfo->empr_logoFac:'';
			$data['empr_certificado']=isset($arayInfo->empr_certificado)?$arayInfo->empr_certificado:'';
			$data['empr_claveCert']=isset($arayInfo->empr_claveCert)?$arayInfo->empr_claveCert:'';
			$data['empr_claveUsuario']=isset($arayInfo->empr_claveUsuario)?$arayInfo->empr_claveUsuario:'';
			$data['empr_id']=isset($arayInfo->empr_id)?$arayInfo->empr_id:'';
			$data['empr_social']=isset($arayInfo->empr_social)?$arayInfo->empr_social:'';
			$data['empr_nomb']=isset($arayInfo->empr_nomb)?$arayInfo->empr_nomb:'';
			$data['empr_claveSol']=isset($arayInfo->empr_claveSol)?$arayInfo->empr_claveSol:'';
			$data['empr_email_pdf']=isset($arayInfo->empr_email_pdf)?$arayInfo->empr_email_pdf:'';
			$data['empr_email_xml']=isset($arayInfo->empr_email_xml)?$arayInfo->empr_email_xml:'';
			$data['empr_correo']=isset($arayInfo->empr_correo)?$arayInfo->empr_correo:'';
		 
			 
			$randor=date('YmdHis');
			$data['image_url']='';// $_ruta.'logo'.'/'.(isset($arayInfo->empr_ruc)?$arayInfo->empr_ruc:'').'_'.$id.'.png?vrt='.$randor;
	        return view('administracion.SunatElectronci',$data);
		}
		public function save(Request $request){
			$empr_id=$request->_sunatempr_id;
			$txtNcTxtNomDocInss=$request->txtNcTxtNomDocInss;
			$txtNcTxtNumeroDocInsRAS=$request->txtNcTxtNumeroDocInsRAS;
			$txtNcTxtNomDocRefInsRAS=$request->txtNcTxtNomDocRefInsRAS;
			$beta_certificado=$request->beta_certificado;
			$empr_certificado=$request->empr_certificado;
			$empr_correo=$request->Correo_empresa1;
			$vamePdf=$request->vamePdf;
			$vameXml=$request->vameXml;
			$colofactura=$request->colofactura;
			$colorboleta=$request->colorboleta;
			$colorncr=$request->colorncr;
			$colornbd=$request->colornbd;
			$colorguiarem=$request->colorguiarem;
			$txtmailhost_sunat=$request->txtmailhost_sunat;
			$mailport=$request->txtmailport_sunat;
			$txtmailcorreo_sunat=$request->txtmailcorreo_sunat;
			$mailpass=$request->txtmailpasssunat;
			$feahcven=$request->feahcven;
			$tipo_decorreo=$request->tipo_decorreo;
			
			$fec=[ 'empr_correo'=>$empr_correo,'empr_claveUsuario' => $txtNcTxtNomDocInss,
				'empr_claveUsuario' => $txtNcTxtNomDocInss,'empr_claveSol'=>$txtNcTxtNumeroDocInsRAS,
				'empr_claveCert'=>$txtNcTxtNomDocRefInsRAS,'empr_betasunat'=>$beta_certificado,
				'empr_certificado'=>$empr_certificado,
				'empr_mailhost'=>$txtmailhost_sunat,
				'empr_mailport'=>$mailport,
				'empr_mailcorreo'=>$txtmailcorreo_sunat,
				'empr_mailpass'=>$mailpass,
				'empr_feahcvencert'=>$feahcven,
				'empr_tipo_decorreo'=>$tipo_decorreo,
			];
			DB::table('empresas')->where('empr_id', $empr_id)->update($fec);
			 $rtn = array( 'success'=>true,
                      'fact'=>$fec,
                  );
			 return json_encode($rtn,JSON_PRETTY_PRINT);
			// $sertificado = $request->file('txtNcTxtNumeroDocRefInsRA');
			// $nombre = $sertificado->getClientOriginalName();
			// echo $nombre;
			// $logo = $request->file('FechaRefiRAs');
			// //$Logo = $request->file('Logo');
			// $ruta=env('RUTA_CERTIFICADO');
			// $path=$_SERVER['DOCUMENT_ROOT'];
			// echo $sertificado;
			// $mime = Input::file('txtNcTxtNumeroDocRefInsRA')->getMimeType();
		 //    $extension = strtolower(Input::file('txtNcTxtNumeroDocRefInsRA')->getClientOriginalExtension());
		 //    $fileName = uniqid().'.'.$extension;
			// //move_uploaded_file($tmp_name,$nuevo_path);
			// \Request::file('txtNcTxtNumeroDocRefInsRA')->move($path, $fileName);
			// \File::get('root/public/'.$sertificado);
			// $sertificado_nombre = $sertificado->getClientOriginalName();
			// \public::disk('local')->put($sertificado_nombre,  \File::get($sertificado));
		}
}
