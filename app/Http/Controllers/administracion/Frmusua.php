<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;

class Frmusua extends Controller{
    
    public function index(Request $request){
        $response['titulo']='Usuario';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $acciones=Controles::get_acciones($request->perf_id,$request->prog_id);
            if($acciones['status']==0)
                throw new \Exception($acciones['message']);
            
            $response['acciones']=$acciones['response'];
            $where='';
            if($request->perf_id!='1'){
                $where=' WHERE perfiles.perf_id!=1';
            }
            $sql="SELECT usua_id,usua_usuario,usua_nomb,perf_nomb,usua_estado " ;
            $sql.="FROM usuarios ";
            $sql.="INNER JOIN perfiles ON perfiles.perf_id=usuarios.perf_id";
            $sql.=" $where ORDER BY usua_id DESC ";
            $row=DB::select($sql); 
          
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
      
        return view('administracion.usuario_index',$response);
    }
    public function get_usuaGrupo(Request $request){
        $report=$request->method;
        $nombreclase = "\Frmusua::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    function get_now($request){
        $response['titulo']='Usuario';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $response['rando_1']=date('YmdHsi');
            $dias='true|7|19;true|7|19;true|7|19;true|7|19;true|7|19;true|7|19;true|7|19';
            if($request['id']!=0){
                $sql="SELECT vend_id,usua_id,usua_usuario,usua_clave,usua_nomb,usua_email,usua_estado,
                usua_serie,perf_id,usua_foto ,usua_dias " ;
                $sql.=" FROM usuarios WHERE usua_id=?";
                $row=DB::select($sql,[$request['id']]); 
                $row=$row[0];
                $response['response']=$row;
                $datadias=$this->getSplit(1,$row->usua_dias==''?$dias:$row->usua_dias); 
            }else{
                $datadias=$this->getSplit(2,$dias);
                $response['response']=array();
            }
            $data=Controles::get_requeridoData($request);
            $row_vendedor=array('0'=>'SELECCIONE');
            $vendedor=Controles::get_data_table_simple("SELECT clie_id ,clie_nomb
            FROM clientes  WHERE clie_tipo IN('E','T','V') AND clie_estado=1 ORDER BY  clie_nomb");
            if($vendedor['status']==0)
                throw new \Exception($vendedor['message']);
            
            foreach ($vendedor['response'] as $key => $value) {
                $row_vendedor[$value->clie_id]=$value->clie_nomb;
            }
            $response['row_vendedor']=$row_vendedor;
            $response['data']=$data;
            $response['datadias']=$datadias;
            $hora=$this->getHora();
            $response['hora']=$hora;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
  
        return view('administracion.usuario_nowedit',$response);    
    }
    public function get_save($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $vendedor=$request['vendedor'];
            $dias=$request['dias'];
            $sql="CALL sp_save_usuario(".$request['id'].",'".$request['usu']."','".$request['pass']."','$dias','".$request['name']."','".$request['emag']."','".$request['perfil']."',$vendedor,'".$request['serie']."','".$request['foto']."','".$request['emps']."')";
            $row=DB::select($sql); 
            $response['response']=$row[0];
            $response['status']=1;
            $response['request']=$request;
            $response['sql']=$sql;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    function get_del($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $post=['usua_estado'=>9];
            $row=DB::table('usuarios')
            ->where('usua_id',$request['id']) 
            ->update($post); 
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function getHora(){
        $hora[1]='01 AM';
        for ($i=2; $i < 12; $i++) { 
            $hora[$i]=($i<=9?'0':'').$i.' AM';
        }
        $hora[12]='12 PM';
        for ($i=13; $i <= 24; $i++) { 
            $hora[$i]=$i.' PM';
        }
        return $hora; 
    }
    public function getSplit($tipo,$dia){
        $objeto=array();
        $dias=explode(';',$dia); 
         
        for ($i=0; $i < count($dias); $i++) {  
            if(isset($dias[$i])){
            $data=explode('|',$dias[$i]);
            $ob=$this->getDias($i);
            $domingo=true;
            if($tipo==2)   {
                if($ob=='domingo'){
                    $domingo=false;
                }
            }else{
                $domingo=isset($data[0])?$data[0]:true; 
            }  
            $objeto[$ob]=array('dia'=>$domingo,
            'desde'=>isset($data[1])?$data[1]:7,
            'hasta'=>isset($data[2])?$data[2]:19);
            }
        }
      return  $objeto;
    }
    public function getDias($i){
        $dia[0]='lunes';
        $dia[1]='martes';
        $dia[2]='miercoles';
        $dia[3]='jueves';
        $dia[4]='viernes';
        $dia[5]='sabado';
        $dia[6]='domingo';
        return $dia[$i];
    }
}