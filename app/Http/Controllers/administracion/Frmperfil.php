<?php

namespace App\Http\Controllers\administracion;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class Frmperfil extends Controller{
    
    
    public function index(Request $request){
        $response['titulo']='Perfil';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $menu=$this->get_all_programas($request->perf_id,$request->usua,$request->sist_id);
            $sql="SELECT perf_id,perf_nomb,perf_estado FROM perfiles   ORDER BY perf_nomb";
            $row=DB::select($sql ); 
            $acciones=$this->get_acciones();
          
            $funciones=$this->get_all_programas_funciones($request->perf_id,$request->usua);
            if($funciones['status']==0)
                throw new \Exception($funciones['message']);
            $response['funciones']=$funciones['response'];
            $response['response']=$row;
            $response['menu']=$menu;
            $response['acciones']=$acciones;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
       
        return view('administracion.perfil_index',$response);
    }

   

    function get_perfilGrupo(Request $request){
        $report=$request->paramt;
        $nombreclase = "\Frmperfil::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    function get_edit($request){
        $response['status']=0;
		$response['message']='';
        try{ 
            $sql="SELECT  prg.modl_id,prg.prog_id,mdl.modl_nomb, prg.prog_nomb,prog_icosp,modl_icon,prog_notacc,prog_modal
            FROM programas prg 
            INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id 
            INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id 
            INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id 
            INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  AND acce_estado=1
            INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id
            WHERE   sis.sist_id=? AND sis.sist_activo=? AND prf.perf_id=?
             ORDER BY  modl_orden, prog_orden";

            $row=DB::select($sql,['01','1',$request['id']]); 

            $sql='SELECT perf_pfpag,perf_pfemi,perf_pcosto,perf_pventa,perf_id,perf_nomb,perf_estado 
            FROM perfiles WHERE perf_id='.$request['id'];
            $row_per=DB::select($sql);

            $acciones=$this->get_all_acciones($request);
            $response['programs']=$row;
            $response['row_per']=$row_per[0];
            $response['all_acciones']=$acciones;
            $response['status']=1;
          
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;

    }
    function get_del($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $post=['perf_estado'=>9];
            $row=DB::table('perfiles')
            ->where('perf_id',$request['id']) 
            ->update($post); 
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    function get_save($request){
       
        $response['status']=0;
		$response['message']='';
        try{  
            $paramn=array(
                        $request['perf_id'],
                        $request['name_perfil'],$request['tipo'],
                        $request['detalles'],$request['acciones']
                        ,$request['pventa']
                        ,$request['pcosto']
                        ,$request['femicio']
                        ,$request['fcobr']
                    );
            $sql="CALL sp_save_perfil(?,?,?,?,?,?,?,?,?)";
            $row=DB::select($sql,$paramn); 
            $response['response']=$row[0];
            $response['paramn']=$paramn;
          
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;      
    }
    function get_all_acciones($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql="SELECT mod_id, perf_id, prog_id, acci_funcion FROM acciones WHERE perf_id=?";
            $row=DB::select($sql,[$request['id']]);             
            $response['response']=$row;      
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }   
        return $response; 
    }

    function get_all_programas($perf_id,$usuario,$empr_id){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql="SELECT prg.modl_id,prg.prog_id,mdl.modl_nomb, prg.prog_nomb,prog_icosp,modl_icon,prog_notacc,prog_modal
            FROM programas prg 
            INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id 
            INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id 
            INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id 
            INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  AND acce_estado=?
            INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id
            INNER JOIN usuarios usr ON usr.perf_id = prf.perf_id 
            WHERE   sis.sist_id=? AND sis.sist_activo=? AND usr.usua_id =? AND acc.perf_id=?
             ORDER BY  modl_orden, prog_orden";
            $row=DB::select($sql,[1,'01',1,$usuario,$perf_id]); 
            $cant_row=count($row);
            $groupdict=array();
            if($cant_row>0){
                foreach ($row as $key => $value) {
                    $groupdict[$value->modl_id][]= $value;
                }

            }
            $response['response']=$groupdict;
       
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }    
        return $response;
    }
    function get_all_programas_funciones($sist_id,$usuario){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql="SELECT prog_acceso,prg.modl_id,prg.prog_id,mdl.modl_nomb, prg.prog_nomb,prog_icosp,modl_icon,prog_notacc,prog_modal
            FROM programas prg 
            INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id  
            INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id 
            INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  
            INNER JOIN usuarios usr ON usr.perf_id = acc.perf_id 
            WHERE prog_notacc=1 AND acc.perf_id=?   AND usr.usua_id =?
             ORDER BY  modl_orden, prog_orden";
            $row=DB::select($sql,[$sist_id,$usuario]); 
            $cant_row=count($row);
            $groupdict=array();
            $html='';
            if($cant_row>0){
                foreach ($row as $key => $value) {
                    $groupdict[$value->modl_id][]= $value;
                }
                foreach ($groupdict as $key => $value) {
                    $html.='<li  >';
                    $html.='<input class="_case" type="checkbox" id="accion_mod_'.$value[0]->modl_id.'" value="'.$value[0]->modl_id.'"   onchange="showModule_accesos('.$value[0]->modl_id.', this);"/>';
                    $html.='<a href="#" class="text-ellipsis">'.$value[0]->modl_nomb.'</a>';
                    $html.='<ul>';
                    foreach ($value as $ke => $val){
                        if ($val->prog_notacc == '1'){
                            $html.='<li>';
                            $html.='<input class="_case" type="checkbox" id="accion_prog_'.$val->prog_id.'"   value="'.$val->prog_id.'" onchange="setProgram_accesos('.$val->prog_id.',this);" /> '; 
                            $html.='<input type="hidden" value="'.$val->prog_modal.'">';
                            $html.='<a href="#" class="text-ellipsis"><i class="spw  '.$val->prog_icosp.'"></i>&nbsp;&nbsp;'.$val->prog_nomb.'</a>';
                            $html.='<ul class="acciones">';
                            $row_fill=DB::select('SELECT func_icon,func_id,func_desc FROM funciones WHERE func_id IN('.$val->prog_acceso.')'); 
                            $cant_row=count($row_fill);
                            if($cant_row>0){
                                foreach ($row_fill as $k => $acc) {
                                    $html.='<li>';
                                    $html.='<input class="_case" type="checkbox" id="accion_acc_'.$value[0]->modl_id.'_'.$val->prog_id .'_'. $acc->func_id.'"  value="'.$acc->func_id.'"  onchange="setAccesos_frmperfi('."'".$acc->func_desc."'".',this);"/>'; 
                                    $html.='<input type="hidden" value="'.$val->prog_id.'">';    
                                    $html.='<a href="#" class="text-ellipsis"><i class="spw  '.$acc->func_icon.'"></i>&nbsp;&nbsp;'.$acc->func_desc.'</a></li>';
                                }
                            }
                             
                            $html.='</ul>';
                            $html.='</li>';
                        }
                    } 
                    $html.='</ul>';
                    $html.='</li>';
                }
            }

            $response['response']=$html;
       
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }    
        return $response;
    }
    function get_acciones(){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql="SELECT func_id, func_desc, func_icon, func_order, func_estado 
            FROM funciones ORDER BY func_order ASC";
            $row=DB::select($sql);             
            $response['response']=$row;      
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }   
        return $response; 
    }
}