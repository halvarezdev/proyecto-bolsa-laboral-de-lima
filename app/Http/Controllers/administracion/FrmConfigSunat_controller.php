<?php

namespace App\Http\Controllers\administracion;
 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;

class FrmConfigSunat_controller extends Controller{

    function ConfSunatGroup(Request $request){
        $report=$request->method;
        $nombreclase = "\FrmConfigSunat_controller::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function get_edit($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql='SELECT empr_ruc,empr_betasunat,empr_claveSol,empr_claveUsuario,empr_claveCert,empr_logoFac
            empr_certificado,empr_correo FROM empresas WHERE empr_id='."'".$request['id']."'";
            $row=DB::select($sql);
            $response['response']=$row[0];
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return view('administracion.frmConfigSunat_now',$response);      
    }
}