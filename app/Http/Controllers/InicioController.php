<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PedidoPostulante;
use App\Http\Requests;
use App\Notificacion;
use Carbon\Carbon;
use App\Favorito;
use App\Vacante;
use App\Contacto;
use App\Admin;
use App\Entidad;
use App\Empresa;
use App\Pedido;
use App\Ubigeo;
use App\User;
use App\Area;
use App\Facebook;
use App\Evento;


class InicioController extends Controller
{

    public function __construct()
    {

       // $this->middleware('auth:contacto');

    }

    public function index($name_entidad = "munilima"){

        $entidades = Entidad::where('enti_nameUrl','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();

        if(count($entidades) <= 0){
            $entidades = Entidad::where('id','=',$name_entidad)
            ->where('enti_flag','=','1')
            ->get();
        }
        
        $feria = Evento::orderBy('fecha_inicio', 'desc')->get();

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
            $data['foto_banner2'] = $value->foto_banner2;
            $data['foto_banner3'] = $value->foto_banner3;
        }

        $pedidos = Empresa::select('pedidos.id as id_pedido', 'empresas.perfil_foto', 'empresas.razon_social', 'pedidos.descripcion',
                'pedidos.created_at', 'pedidos.otro_conocimiento', 'ubigeo.ubi_descripcion', 'areas.area_descripcion','banner_foto')
                ->leftJoin('pedidos', 'pedidos.empresa_id', '=', 'empresas.id')
                ->leftjoin('ubigeo','pedidos.ubigeo','=','ubigeo.ubi_codigo')
                ->leftjoin('areas','pedidos.area','=','areas.id')
                ->where('pedidos.flag_estado', '1')
                ->orderBy('pedidos.created_at', 'DESC')
                ->take(12)
                ->get();

        $date = Carbon::now();
        $date = $date->format('Y-m-d');



        $data['vacantes'] = Vacante::select('fech_referencia','nombre','insti_logo','nombre_especialidad','fecha_postula',
        'formacion_academica','num_convocatoria','vacantes.id as codigo','ubi_descripcion')
        ->leftjoin('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->leftJoin('ubigeo' , 'vacantes.ubigeo','=','ubigeo.ubi_codigo')
        ->where('fecha_postula', '>', $date.' 23:59:59')
        //->whereBetween('fecha_postula',array('vacantes.fecha_postula','vacantes.fecha_postula'))
        ->orderBy('vacantes.fech_referencia', 'DESC')
        ->take(40)
        ->get();

        $where = ['ubi_codprov' => '00', 'ubi_coddist' => '00'];
        $ubigeo = Ubigeo::where($where)->get();


        $total_empresa = Empresa::count();
        $total_persona = User::count();
        $areas = Area::get();

        $data['areas'] = $areas;
        $data['pedidos'] = $pedidos;
        $data['ubigeo'] = $ubigeo;
        $data['total_pedido'] = Pedido::count();
        $data['total_Vacante'] = Vacante::count();
        $data['total_empresa'] = $total_empresa;
        $data['total_persona'] = $total_persona;
        $data['inicio'] = "1";
        $data['menu_login'] = '0';
        $data['entidades'] = $entidades;
        $data['ferias'] = $feria;
        $data['page'] = "inicio";

        return view('welcome', $data);
    }
    public function busqueda($entidad){

        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
        }

        # inputs de la vista de filtrado
        $tipo_convocatoria = Input::get('tipo_convocatoria');
        $categoria = input::get('categoria');
        $tipo_contrato = Input::get('tipo_contrato');
        $turno_trabajo = Input::get('turno_trabajo');
        $des = Input::get('descripcion');
        $ubicacion = Input::get('ubicacion');
        $where = ['ubi_codprov' => '00', 'ubi_coddist' => '00'];
        $ubigeo = Ubigeo::where($where)->get();
        $whereTotales = ['flag_estado' => '1'];
        $empresa = Input::get('empresa');
        $descripcion_empresa = Input::get('descripcion_empresa');

        $ocupacion = Input::get('ocupacion');
        $porciones_vac = Input::get('porciones_vac');

        if(isset($des) && $des != ""){
            $porciones_vac = $des;

        }
        if(isset($ocupacion) && $ocupacion != ""){
            $whereTotales['denominacion'] = $ocupacion;
            $porciones = explode(" ", $ocupacion);
        }

        if(isset($porciones_vac) && $porciones_vac != ""){
            $porciones_vac = explode(" ", $porciones_vac);
        }

        if(isset($descripcion_empresa) && $descripcion_empresa != ''){
            $porciones_vac = explode(" ", $descripcion_empresa);
            $empresa = $descripcion_empresa;
        }

        $categoria_letra = null;
        if(isset($categoria) && $categoria != ''){
            $categorialit = Area::where('id',$categoria)->get();
            foreach($categorialit as $cat_value){
                $categoria_letra = explode(" ", $cat_value->area_descripcion);
            }
        }

        $porciones = explode(" ", $des);
        $orwhere = [];
        $date = Carbon::now();
        $date = $date->format('Y-m-d');
        $vacantes = Vacante::select('vacantes.procedencia','nombre_especialidad as descripcion','conocimiento as otro_conocimiento',
        'vacantes.id as codigo','insti_logo as perfil_foto','institucion.nombre as razon_social','vacantes.fecha_postula as created_at',
        'ubi_descripcion','vacantes.num_convocatoria as area_descripcion','facebook.nombre as nom_etiqueta',
        'facebook.descripcion as descr_etiqueta', 'facebook.image as image_etiqueta')
        ->leftjoin('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->leftJoin('ubigeo' , 'vacantes.ubigeo','=','ubigeo.ubi_codigo')
        ->leftJoin('facebook', 'facebook.urlruta','=','institucion.id')
        ->where('fecha_postula', '>=', $date.' 00:00:00')
        ->where((function($query) use ($porciones_vac,$tipo_convocatoria,$empresa,$categoria_letra,$tipo_contrato,$turno_trabajo,$ubicacion){
            if(isset($porciones_vac) && $porciones_vac != '' && $porciones_vac != null && count($porciones_vac) >0){
                foreach($porciones_vac as $value){
                    $query->orWhere('vacantes.nombre_especialidad', 'LIKE', '%'.$value.'%');
                }
            }
            if($tipo_convocatoria != "0" && $tipo_convocatoria != null){
                $query->where('procedencia', $tipo_convocatoria);
            }
            if(isset($empresa) && $empresa != '' && $empresa != null){
                $query->where('institucion.nombre','like','%'.(isset($empresa) && $empresa != "" ? $empresa : '').'%');
            }
            if(isset($categoria_letra) && $categoria_letra != '' && $categoria_letra != null){
                foreach($categoria_letra as $tvalue){
                    $query->orWhere('vacantes.formacion_academica', 'LIKE', '%'.$tvalue.'%');
                }
            }
            if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato != null){
                $query->where('vacantes.id', '000000');
            }
            if(isset($turno_trabajo) && $turno_trabajo != '' && $turno_trabajo != null){
                $query->where('vacantes.id', '000000');
            }
            if(isset($ubicacion) && $ubicacion != '' && $ubicacion != null){
                $query->where('ubigeo.ubi_codigo','LIKE' , $ubicacion.'%');
            }
        }));

        $pedidos = Pedido::select('pedidos.procedencia','pedidos.descripcion', 'pedidos.otro_conocimiento',
        'pedidos.id as codigo', 'empresas.perfil_foto','empresas.razon_social', 'pedidos.fecha_limite as created_at',
        'ubigeo.ubi_descripcion', 'areas.area_descripcion','facebook.nombre as nom_etiqueta',
        'facebook.descripcion as descr_etiqueta', 'facebook.image as image_etiqueta')
            ->leftjoin('ubigeo', 'ubigeo.ubi_codigo', '=', 'pedidos.ubigeo')
            ->leftjoin('empresas', 'empresas.id', '=', 'pedidos.empresa_id')
            ->leftjoin('areas', 'areas.id', '=', 'pedidos.area')
            ->leftJoin('facebook', 'facebook.urlruta','=','empresas.id')
            ->where((function($query) use ($porciones, $tipo_convocatoria,$categoria,$empresa,$tipo_contrato,$turno_trabajo,$ubicacion){
                foreach($porciones as $value){
                    $query->where('pedidos.descripcion', 'LIKE', '%'.$value.'%');
                }
                if($tipo_convocatoria != "0" && $tipo_convocatoria != null){
                    $query->where('procedencia', $tipo_convocatoria);
               }
               if(isset($categoria) && $categoria != '' && $categoria != null){
                    $query->where('pedidos.area', $categoria);
               }
               if(isset($empresa) && $empresa != '' && $empresa != null){
                    $query->where('empresas.razon_social', 'LIKE', '%'.(isset($empresa) && $empresa != "" ? $empresa : '').'%');
               }
               if(isset($tipo_contrato) && $tipo_contrato != '' && $tipo_contrato != null){
                    $query->where('pedidos.modalidades', $tipo_contrato);
               }
                if(isset($turno_trabajo) && $turno_trabajo != '' && $turno_trabajo != null){
                    $query->where('pedidos.turno_trabajo', $turno_trabajo);
                }
                if(isset($ubicacion) && $ubicacion != '' && $ubicacion != null){
                    $query->where('ubigeo.ubi_codigo','LIKE' , $ubicacion.'%');
                }
            }))
            ->where($whereTotales)
        ->unionAll($vacantes)
        ->orderBy('created_at', 'DESC');

        $areas = Area::get();

        # variables para la vista
        $data['areas'] = $areas;
        $data['tipo_convocatoria'] = $tipo_convocatoria;
        $data['empresa'] = $empresa;
        $data['categoria'] = $categoria;
        $data['tipo_contrato'] = $tipo_contrato;
        $data['turno_trabajo'] = $turno_trabajo;
        $data['ubigeo'] = $ubigeo;
        $data['descripcion'] = $des;
        $data['cantidad'] = $pedidos->count();
        $data['pedidos'] = $pedidos->paginate(8);

        $data['ubicacion'] = $ubicacion;
        $data['des'] = $des;

        $data['inicio'] = "1";

        return view('restultado', $data);


    }

    public function detalleVacante($id){

        $entidad = Input::get('e');
        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['entidad'] = $value->id;
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
        }


        $perfil_foto = "";
        $area_ped = "";
        $area_desc = "";

        $pedidos = Pedido::select('banner_foto','pedidos.id as codigo_pedido','pedidos.ubigeo','pedidos.descripcion as des_empre',
        'pedidos.created_at as created_at', 'pedidos.tareas','pedidos.otro_conocimiento', 'pedidos.moneda', 'pedidos.remuneracion',
        'pedidos.horario', 'pedidos.num_vacantes', 'empresas.razon_social as razon_social', 'secciones.sec_descripcion as sec_descripcion',
        'areas.area_descripcion', 'pedidos.turno_trabajo', 'pedidos.experiencia_laboral', 'pedidos.experiencia_nivel','areas.id as cod_area',
        'pedidos.grado', 'pedidos.beneficios', 'empresas.perfil_foto',DB::raw("DATE_FORMAT(fecha_limite, '%d/%m/%Y') as fecha_limite"),
        'pedidos.horas_extras','pedidos.movilidad', 'pedidos.refrigerio', 'pedidos.bonificacion', 'pedidos.comisiones','pedidos.area', 'pedidos.empresa_id')
        ->leftjoin('empresas', 'pedidos.empresa_id', '=', 'empresas.id')
        ->leftjoin('secciones', 'secciones.id' , '=', 'empresas.rubro')
        ->leftjoin('areas', 'pedidos.area' , '=', 'areas.id')
        ->where('pedidos.id', '=', $id)
        ->get();

        $contact = [];
        $departamento=[];
        $provincia=[];
        $where_dist=[];
        $distrito=[];
        $where_prov=[];
        $area_ped='';
        $area_desc='';  
        $data['banner_foto'] ='';
            foreach ($pedidos as $key => $value) {
            $perfil_foto = $value->perfil_foto;
            $data['banner_foto'] = $value->banner_foto;
            $where_dep = ['ubi_coddpto' => substr($value->ubigeo,0,2) ,'ubi_codprov' => '00', 'ubi_coddist' => '00'];
            $departamento = Ubigeo::select('ubi_descripcion')->where($where_dep)->get();
            $where_prov = ['ubi_coddpto' => substr($value->ubigeo,0,2) ,'ubi_codprov' => substr($value->ubigeo,2,2), 'ubi_coddist' => '00'];
            $provincia = Ubigeo::select('ubi_descripcion')->where($where_prov)->get();
            $where_dist = ['ubi_coddpto' => substr($value->ubigeo,0,2) ,'ubi_codprov' => substr($value->ubigeo,2,2), 'ubi_coddist' => substr($value->ubigeo,4,2)];
            $distrito = Ubigeo::select('ubi_descripcion')->where($where_dist)->get();
            $area_ped = $value->area;
            $area_desc = $value->area_descripcion;
            if(Auth::guard('entidad')->user()){
                $contact = Contacto::where('empresa_id','=',$value->empresa_id)->get();
            }
            
        }
    

        $date = Carbon::now();
        $date = $date->format('Y-m-d');

        $vacantes = Vacante::select('insti_logo as perfil_foto','nombre_especialidad as des_empre',
        'nombre as razon_social','vacantes.id as codigo_pedido', 'fecha_postula as fecha','vacantes.procedencia as cod_proce')
        ->leftjoin('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->leftJoin('ubigeo' , 'vacantes.ubigeo','=','ubigeo.ubi_codigo')
        ->where('fecha_postula', '>', $date.' 23:59:59')
        ->where('vacantes.nombre_especialidad', 'LIKE', '%'.$area_desc.'%');

        $data['realcionados'] = Pedido::select('empresas.perfil_foto','pedidos.descripcion as des_empre',
        'empresas.razon_social','pedidos.id as codigo_pedido','pedidos.fecha_limite as fecha','pedidos.procedencia as cod_proce')
        ->where('pedidos.area', $area_ped)
        ->where('pedidos.id', '!=', $id)
        ->leftjoin('empresas', 'pedidos.empresa_id', '=', 'empresas.id')
        ->unionAll($vacantes)
        ->orderBy('fecha', 'DESC')
        ->take(5)
        ->get();

        # falta el relacionados

        if(isset(Auth::user()->persona_id)){
            $data['favorito'] = Favorito::where('pedidos_id', $id)
            ->where('users_id',Auth::user()->id)
            ->where('pedfav_procedencia','1')
            ->get();
        }

        $data['departamento'] = $departamento;
        $data['provincia'] = $provincia;
        $data['distrito'] = $distrito;
        $data['_id']=$id;
        $data['perfil_foto']=$perfil_foto;
        $data['pedidos']=$pedidos;
        $data['inicio'] = "1";
        $data['contact'] = $contact;
        
        return view('vacante/vacante_detalle', $data);

    }
    public function guardarVacante($pedido,Request $request){
        $persona_id=0;
        if(isset(\Auth::user()->persona_id)){
            $persona_id=\Auth::user()->persona_id;
        }
       

        $sql1=" call acc_savepostulantepedido($pedido,$persona_id)";
        $Lista1=DB::select($sql1);
        if (count($Lista1)>0) {
            foreach ($Lista1 as $key => $value) {
                $resultado=$value; 
            }
      }
      echo 'sudido';
      echo $persona_id;
     // return $resultado;

    }

    public function indexNosotros($name_entidad = "accede"){
        $entidades = Entidad::where('enti_nameUrl','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();

        if(count($entidades) <= 0){
            $entidades = Entidad::where('id','=',$name_entidad)
            ->where('enti_flag','=','1')
            ->get();
        }

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }
        $data['inicio'] = "1";
        return view('menu-maestro/nosotros', $data);
    }
    public function indexOrientador($name_entidad = "accede"){
        $entidades = Entidad::where('enti_nameUrl','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();

        if(count($entidades) <= 0){
            $entidades = Entidad::where('id','=',$name_entidad)
            ->where('enti_flag','=','1')
            ->get();
        }

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }
        $data['inicio'] = "1";
        return view('menu-maestro/orientador', $data);
    }
    public function indexConsulta($name_entidad = "accede"){
        $entidades = Entidad::where('enti_nameUrl','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();

        if(count($entidades) <= 0){
            $entidades = Entidad::where('id','=',$name_entidad)
            ->where('enti_flag','=','1')
            ->get();
        }

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }
        $data['inicio'] = "1";
        return view('menu-maestro/consulta', $data);
    }


    public function indexContactenos($name_entidad = "accede"){
        $entidades = Entidad::where('enti_nameUrl','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();

        if(count($entidades) <= 0){
            $entidades = Entidad::where('id','=',$name_entidad)
            ->where('enti_flag','=','1')
            ->get();
        }

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }
        $data['inicio'] = "1";
        return view('menu-maestro/contactenos', $data);
    }

    public function cambiarVisto($id,$name_entidad = "accede"){
        $date = Carbon::now();

        $entidades = Entidad::where('enti_nameUrl','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();

        if(count($entidades) <= 0){
            $entidades = Entidad::where('id','=',$name_entidad)
            ->where('enti_flag','=','1')
            ->get();
        }

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }
        $persona = PedidoPostulante::where('PP_id', $id)
        ->update([
          'PP_Visto' => '1',
          'Fecha_visto' => $date,
        ]);
        $msj = "ItemCambiasdo";

        $datos_pedpost = PedidoPostulante::where('PP_id', $id)->get();

        foreach($datos_pedpost as $value){
            $agregarNotificacion = Notificacion::create([
                'not_estado' => '1',
                'not_mensaje' => 'Su Cv fue visto',
                'id_contacto' => Auth::guard('contacto')->user()->id,
                'id_users' => $value->idPostulante,
                'id_pedido' => $value->idPedido,
            ]);
        }

       // echo Auth::user()->id;

        return json_encode($msj,JSON_PRETTY_PRINT);
    }

    function politicaProvacidad($entidad){

        $data['inicio'] = "1";

        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }

        return view('politica-privacidad', $data);

    }

    public function validaCorreoElectronicoUsers($correo){

        $users = User::select('email')->where('email',$correo)->get();
        $value = "0";
        if(count($users) > 0){
          $value = "1";
        }
        return json_encode($value,JSON_PRETTY_PRINT);

    }

    public function validaCorreoElectronicoEmpresa($correo){

        $users = Contacto::select('email')->where('email',$correo)->get();
        $value = "0";
        if(count($users) > 0){
          $value = "1";
        }
        return json_encode($value,JSON_PRETTY_PRINT);

    }

    public function validaRucEmpresa($ruc){

        $users = Empresa::select('numero_documento')->where('numero_documento',$ruc)->get();
        $value = "0";
        if(count($users) > 0){
          $value = "1";
        }
        return json_encode($value,JSON_PRETTY_PRINT);

    }

    public function ejemplo(){

        return view('mails.mail_masivo');
    }

}
