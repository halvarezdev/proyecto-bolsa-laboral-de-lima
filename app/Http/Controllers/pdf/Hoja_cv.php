<?php
namespace App\Http\Controllers\pdf;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FormacionBasica;
use App\Profesion;
use App\CentroEstudio;
use App\FormacionSuperior;
use App\FormacionPosgrado;
use App\Maestro;
use App\Idioma;
use App\Computacion;
use App\EstudiosAdicionales;
use App\Ocupacion;
use App\Experiencia;
use App\PedidoPostulante;
use App\Area;
use Illuminate\Support\Facades\Auth;
class Hoja_cv extends Controller{

     protected $redirectTo = 'auth.contacto-login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
  public function download_cv($id = "",$ficha_id,$id_persona,$idPedido = "",$fin = "2"){
    $arrayDataPersona=array();
    $arrayDataPersonaExpe=array();
    $arrayDataPersonaEducaBasica=array();
    $arrayDataPersonaEducaSuperior=array();
    $arrayDataPersonaEducaPosgrado=array();
    $arrayDataPersonaEducaOtros=array();
    $arrayDataPersonIdioma=array();
    $arrayDataPersonCompu=array();
    $sql='SELECT sexo,DATE_FORMAT(fecha_nacimiento,"%d/%m/%Y") AS fecha_naci, personas.telefono,
    personas.direccion,personas.id AS id_personas,personas.numero_documento,personas.email,personas.ape_pat,
    personas.ape_mat,personas.name, fichapersona.foto_post, fichapersona.presentacion 
    FROM personas INNER JOIN fichapersona ON fichapersona.persona_id=personas.id AND fic_estado=1 
    WHERE fichapersona.id='.$ficha_id.' AND personas.id='.$id_persona;
    $Lista=DB::select($sql);
    if (count($Lista)) {
          foreach ($Lista as $key => $rst) {  
            $arrayDataPersona=$rst;
          }
          }

      $formacion_basica = FormacionBasica::
        join('gradoinstruccion', 'gradoinstruccion.id', '=', 'pers_basica.grainst_id')
        ->select('pers_basica.perbas_centroestudio', 'pers_basica.perbas_fechaini', 'pers_basica.perbas_fecafin', 
        'gradoinstruccion.grad_descripcion', 'pers_basica.id as basica_id')
        ->where('pers_id', '=', $id_persona)
        ->orderBy('pers_basica.updated_at', 'DESC')
        ->get();

        $formacion_superior =FormacionSuperior::
        join('gradoinstruccion', 'gradoinstruccion.id', '=', 'per_superior.nivel_id')
        ->join('profesionales', 'profesionales.profp_codigo', '=', 'per_superior.especialidad_id')
     //   ->join('centroestudios', 'centroestudios.CESTP_CODIGO', '=', 'per_superior.centro_id')
        ->select('profesionales.nombre', 'gradoinstruccion.grad_descripcion', 'per_superior.otro_centro', 'per_superior.fecha_inicio' ,'per_superior.fecha_fin', 'per_superior.id as superior_id')
        ->where('pers_id', '=', $id_persona)
        ->orderBy('per_superior.updated_at', 'DESC')
        ->get();

        $posgrado =FormacionPosgrado::
        join('gradoinstruccion', 'gradoinstruccion.id', '=', 'per_posgrado.nivel_id')
       // ->join('profesionales', 'profesionales.profp_codigo', '=', 'per_posgrado.especialidad_id')
      //  ->join('centroestudios', 'centroestudios.CESTP_CODIGO', '=', 'per_posgrado.centro_id')
        ->select('per_posgrado.especialidad_id', 'gradoinstruccion.grad_descripcion', 'per_posgrado.otro_centro', 'per_posgrado.fecha_inicio' ,'per_posgrado.fecha_fin', 'per_posgrado.id as posgrado_id')
        ->where('pers_id', '=', $id_persona)
        ->orderBy('per_posgrado.updated_at', 'DESC')
        ->get();

        $idiomas = Idioma::
        join('maestros', 'maestros.MAESP_CODIGO', '=', 'pers_idioma.idioma_nom')
        ->select('maestros.MAESC_DESCRIPCION', 'pers_idioma.idioma_nivel', 'pers_idioma.id as idioma_id')
        ->where('pers_id', '=', $id_persona)
        ->orderBy('pers_idioma.updated_at', 'DESC')
        ->get();

        $computaciones = Computacion::
        join('maestros', 'maestros.MAESP_CODIGO', '=', 'pers_compu.compu_nom')
        ->select('maestros.MAESC_DESCRIPCION', 'pers_compu.compu_nivel', 'pers_compu.id as compu_id')
        ->where('pers_id', '=', $id_persona)
        ->orderBy('pers_compu.updated_at', 'DESC')
        ->get();

        $estudiosadi = EstudiosAdicionales::select('pers_estudiosadi.estu_tipo', 'pers_estudiosadi.estu_nombre', 'pers_estudiosadi.estu_cetro', 'pers_estudiosadi.id as adi_id')
          ->where('pers_id', '=', $id_persona)
          ->orderBy('pers_estudiosadi.updated_at', 'DESC')
          ->get();

        $experiencia_liest = Experiencia::
        join('ocupaciones', 'ocupaciones.ocup_codigo', '=', 'pers_experiencia.id_ocupacion')
        ->select('pers_experiencia.nom_empresa', 'ocupaciones.nombre', 'pers_experiencia.fech_inicio', 'pers_experiencia.fech_fin','pers_experiencia.id as expe_id','pers_experiencia.id_ocupacion','otro_cargo','pers_experiencia.funciones')
        ->where('pers_id', '=', $id_persona)
        ->orderBy('pers_experiencia.updated_at', 'DESC')
        ->get();


        $data['arrayDataPersona']=$arrayDataPersona;
        $data['formacion_basica']=$formacion_basica;
        $data['formacion_superior']=$formacion_superior;
        $data['posgrado']=$posgrado;
        $data['estudiosadi'] = $estudiosadi;
        $data['idiomas'] = $idiomas;
        $data['computaciones'] = $computaciones;
        $data['experiencia_liest'] = $experiencia_liest;

        $view =  \View::make('pdf.view_cv_empresa',$data)->render();
   // echo $view ;
     $pdf =PDF::loadHTML($view);//->setPaper('a4', 'landscape');
        if ($fin=='1') {
          return $pdf->download($arrayDataPersona->numero_documento.'-cv.pdf');
        }else{
          return $pdf->stream($arrayDataPersona->numero_documento.'-cv.pdf');
        }
    
       // return $pdf->download($arrayDataPersona->numero_documento.'-cv.pdf');//return $pdf->stream('invoice');
  }



}