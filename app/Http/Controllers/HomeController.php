<?php
namespace App\Http\Controllers;
use App\Http\Controllers\control\UbigeoLista;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\EstudiosAdicionales;
use Illuminate\Http\Request;
use App\FormacionSuperior;
use App\FormacionPosgrado;
use App\GradoInstruccion;
use App\PedidoPostulante;
use App\FormacionBasica;
use App\Http\Requests;
use App\CentroEstudio;
use App\FichaPersona;
use App\Notificacion;
use App\Computacion;
use App\Experiencia;
use App\Vacante;
use App\Ubigeo;
use App\Profesion;
use App\Ocupacion;
use Carbon\Carbon;
use App\Favorito;
use App\Persona;
use App\Maestro;
use App\Entidad;
use App\Idioma;
use App\Pedido;
use App\Area;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($mensaje = "")
    {

      $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
      $data['ubigeo'] = Ubigeo::where($where)->get();


      $data['personas'] = Persona::
      leftJoin('ubigeo','ubigeo.ubi_codigo','=','personas.ubigeo')
      ->where('personas.id', Auth::user()->persona_id)->get();

      $data['ficha'] = FichaPersona::select('fichapersona.id as ficha_id', 'fichapersona.persona_id','fichapersona.foto_post',
      'paises.pais_descripcion', 'fichapersona.filecv', 'fichapersona.fic_salario', 'fichapersona.presentacion',
      'fichapersona.licenciaConduc','fichapersona.categoria', 'fichapersona.porta_arma','fichapersona.arma_propia',
      'fichapersona.ocu_trabajo','fichapersona.ubi_lugar', 'fichapersona.url_cv', 'fichapersona.id_pais')
      ->join('paises','paises.pais_cod','=','fichapersona.id_pais')
      ->where('fichapersona.persona_id',Auth::user()->persona_id)->get();

      foreach ($data['ficha'] as $key => $value) {
        $data['ruta_foto'] = $value->foto_post;
        $data['id_pais'] = $value->id_pais;
      }

      foreach($data['personas'] as $per){
        if(isset($per->ubigeo) && $per->ubigeo != null){
          $dep = substr(''.$per->ubigeo.'',0,2);
          $prov = substr(''.$per->ubigeo.'',2,2);
          $dist = substr(''.$per->ubigeo.'',4,2);

        }

      }
      $departamento = isset($dep) ? $dep : '15';
      $provincia = isset($prov) ? $prov : '01';
      $distrito = isset($dist) ? $dist : '01';

      $data['lista_pais'] = UbigeoLista::getPaisSelectOption();
      $data['lista_depa'] = UbigeoLista::getDepartSelectOption();
      $data['lista_prov'] = UbigeoLista::getProviSelectOption($departamento);
      $data['lista_dist'] = UbigeoLista::getDistritoSelectOption($departamento,$provincia);

      $data['grado_intruccion_posgrado'] = GradoInstruccion::where('grad_flag','3')->get();
      $data['grado_intruccion_superior'] = GradoInstruccion::where('grad_flag','2')->get();
      $data['grado_intruccion_basico'] = GradoInstruccion::where('grad_flag','1')->get();

      $data['formacion_basica'] = FormacionBasica::
      join('gradoinstruccion', 'gradoinstruccion.id', '=', 'pers_basica.grainst_id')
      ->select('pers_basica.perbas_centroestudio', 'pers_basica.perbas_fechaini', 'pers_basica.perbas_fecafin', 'gradoinstruccion.grad_descripcion', 'pers_basica.id as basica_id')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->orderBy('pers_basica.updated_at', 'DESC')
      ->get();

      $data['formacion_superior'] =FormacionSuperior::
      join('gradoinstruccion', 'gradoinstruccion.id', '=', 'per_superior.nivel_id')
      ->join('profesionales', 'profesionales.profp_codigo', '=', 'per_superior.especialidad_id')
    //  ->join('centroestudios', 'centroestudios.CESTP_CODIGO', '=', 'per_superior.centro_id')
      ->select('per_superior.persu_estado','profesionales.nombre', 'gradoinstruccion.grad_descripcion', 'per_superior.otro_centro', 'per_superior.fecha_inicio' ,'per_superior.fecha_fin', 'per_superior.id as superior_id')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->where('profesionales.ggrupo','!=', 'x')
      ->orderBy('per_superior.updated_at', 'DESC')
      ->get();

      $data['posgrado'] =FormacionPosgrado::
      join('gradoinstruccion', 'gradoinstruccion.id', '=', 'per_posgrado.nivel_id')
    //  ->join('profesionales', 'profesionales.profp_codigo', '=', 'per_posgrado.especialidad_id')
    //  ->join('centroestudios', 'centroestudios.CESTP_CODIGO', '=', 'per_posgrado.centro_id')
      ->select('per_posgrado.postsu_estado','per_posgrado.especialidad_id', 'gradoinstruccion.grad_descripcion', 'per_posgrado.otro_centro', 'per_posgrado.fecha_inicio' ,'per_posgrado.fecha_fin', 'per_posgrado.id as posgrado_id')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->orderBy('per_posgrado.updated_at', 'DESC')
      ->get();

      $data['idiomas'] = Idioma::
      join('maestros', 'maestros.MAESP_CODIGO', '=', 'pers_idioma.idioma_nom')
      ->select('maestros.MAESC_DESCRIPCION', 'pers_idioma.idioma_nivel', 'pers_idioma.id as idioma_id')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->orderBy('pers_idioma.updated_at', 'DESC')
      ->get();

      $data['computaciones'] = Computacion::
      join('maestros', 'maestros.MAESP_CODIGO', '=', 'pers_compu.compu_nom')
      ->select('maestros.MAESC_DESCRIPCION', 'pers_compu.compu_nivel', 'pers_compu.id as compu_id')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->orderBy('pers_compu.updated_at', 'DESC')
      ->get();

      $data['estudiosadi'] = EstudiosAdicionales::select('pers_estudiosadi.fecha_fin','pers_estudiosadi.estu_tipo', 'pers_estudiosadi.estu_nombre', 'pers_estudiosadi.estu_cetro', 'pers_estudiosadi.id as adi_id')
        ->where('pers_id', '=', Auth::user()->persona_id)
        ->orderBy('pers_estudiosadi.updated_at', 'DESC')
        ->get();

      $data['experiencia_liest'] = Experiencia::
      join('ocupaciones', 'ocupaciones.ocup_codigo', '=', 'pers_experiencia.id_ocupacion')
      ->select('pers_experiencia.nom_empresa', 'ocupaciones.nombre', 'pers_experiencia.fech_inicio', 'pers_experiencia.fech_fin','pers_experiencia.id as expe_id','pers_experiencia.id_ocupacion','otro_cargo')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->orderBy('pers_experiencia.updated_at', 'DESC')
      ->get();

      $data['profesiones'] = Profesion::where('ggrupo', '!=', 'x')->get();
      $data['centro_estudio'] = CentroEstudio::where('CESTC_ESTADO','=','2')->get();
      $data['idioma_lista'] = Maestro::where('MAESP_CODIGO1','6')->get();
      $data['compu_lista'] = Maestro::where('MAESP_CODIGO1','3')->get();
      $data['ocupaciones'] = Ocupacion::where('status', '=', '9')->get();


    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['submenu_color'] = $value->submenu_color;
    }

      return view('home', $data);
      }


    public function BuscarPedido(){
        $data['list_grado'] = GradoInstruccion::where('grad_flag','=','2')->get();
        $data['estudios'] = GradoInstruccion::where('grad_flag','=','1')->get();
        $data['ocupacion'] = Ocupacion::where('ggrupo', '!=', 'x')->get();
        $data['areas'] = Area::get();
        $data['computacion'] = Maestro::where('MAESP_CODIGO1','=','3')->get();
        $data['idiomas'] = Maestro::where('MAESP_CODIGO1','=','6')->get();

        $data['lista_depa']=UbigeoLista::getDepartSelectOption();
        $data['lista_prov']=UbigeoLista::getProviSelectOption('15');
        $data['lista_dist']=UbigeoLista::getDistritoSelectOption('15','01');
        $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
        ->where('enti_flag','=','1')
        ->get();

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['submenu_color'] = $value->submenu_color;
        }


        return view('auth/busqueda', $data);
    }
    public function BuscarPedidoSher(Request $reques){


    }

  public function busquedaSubmmit(Request $request){

    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
        ->where('enti_flag','=','1')
        ->get();

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['submenu_color'] = $value->submenu_color;
        }

    $data['response_db']=$this->ResultadoFiltro($request->all());

    return view('auth/restultado-buesqueda-persona', compact('data'));

  }
    public function ResultadoFiltro($request){

      $where='';
      $innerjoin = '';
      $where_ubi = '';

      if($request['descripcion'] != "" && isset($request['descripcion'])){
          $where .=' and p.descripcion like "%'.$request['descripcion'].'%" ';
      }

  /*    if ($request['ocupacion'] != "" && isset($request['ocupacion'])) {
          $innerjoin .= ' inner join ocupaciones oc on oc.ocup_codigo = p.denominacion ';
          $where .= ' and p.denominacion = "'.$request['ocupacion'].'" ';
      }
*/
      if ($request['area'] != "" && isset($request['area'])) {
          $innerjoin .= ' inner join areas ar on ar.id = p.area ';
          $where .= ' and p.area = "'.$request['area'].'" ';
      }

     /* if ($request['grado'] != "" && isset($request['grado']) ) {
          $innerjoin .= ' inner join gradoinstruccion gra on gra.id = p.grado ';
          $where .= ' and p.grado = "'.$request['grado'].'" ';
      }

      if ($request['estudios_formales'] != "" && isset($request['estudios_formales'])) {
          $innerjoin .= ' inner join gradoinstruccion es on es.id = p.estudios_formales ';
          $where .= ' and p.estudios_formales = "'.$request['estudios_formales'].'" ';
      }

      if ($request['compu2'] != "" && isset($request['compu2'])) {
          $innerjoin .= ' inner join ped_compu com on com.pedido_id = p.id ';
          $where .= ' and com.compu_nom = "'.$request['compu2'].'" ';
      }
*/
      if ($request['idioma2'] != "" && isset($request['idioma2'])) {
          $innerjoin .= 'inner join ped_idioma idi on idi.pedido_id = p.id ';
          $where .= ' and idi.idioma_nom = "'.$request['idioma2'].'" ';
      }

      if ($request['licencia'] != "" && isset($request['licencia'])) {
          $where .= ' and p.licencia = "'.$request['licencia'].'" ';
      }

      if ($request['categoria'] != "" && isset($request['categoria'])) {
          $where .= ' and p.categoria = "'.$request['categoria'].'" ';
      }
/*
      if ($request['cuenta_carro'] != "" && isset($request['cuenta_carro'])) {
          $where .= ' and p.cuenta_carro = "'.$request['cuenta_carro'].'" ';
      }

      if ($request['otro_conocimiento'] != "" && isset($request['otro_conocimiento'])) {
          $where .= ' and p.otro_conocimiento like "%'.$request['otro_conocimiento'].'%" ';
      }
*/
      if ($request['experiencia'] != "" && isset($request['experiencia'])) {
          $where .= ' and p.experiencia_laboral = "'.$request['experiencia'].'" ';
      }

      if($request['tipo_experiencia'] != "" && isset($request['tipo_experiencia'])){
        $where .= 'and p.experiencia_nivel = "'.$request['tipo_experiencia'].'" ';
      }
        //buscamos en el ubigeo los departamentos
        if ($request['departamento']!='0') {
            $where_ubi =' AND  LEFT(p.ubigeo,2)="'.$request['departamento'].'" ';
        }

        //buscamos los ubigeos provincia y deparmet.
        if ($request['provincia']!='0') {
            $where_ubi =' AND  LEFT(p.ubigeo,4)="'.$request['departamento'].$request['provincia'].'" ';
        }

        //ubigeo completo
        if ($request['distrito']!='0') {
            $where_ubi =' AND p.ubigeo="'.$request['departamento'].$request['provincia'].$request['distrito'].'" ';
        }
/*
        if ($request['sexo'] != "") {
            $where .= ' and p.sexo = "'.$request['sexo'].'" ';
        }

        if ($request['edad'] != "" && isset($request['edad'])) {
            $where .= ' and p.edad = "'.$request['edad'].'" ';
        }

        if ($request['edad_total'] != "" && isset($request['edad_total'])) {
            $where .= ' and p.edad_total = "'.$request['edad_total'].'" ';
        }
*/
        if ($request['tareas']!='' && isset($request['tareas'])) {
          $where.=' AND p.tareas LIKE "%'.$request['tareas'].'%" ';
        }

        if ($request['modalidades'] != "" && isset($request['modalidades'])) {
            $where .= ' and p.modalidades = "'.$request['modalidades'].'" ';
        }

        if ($request['horario_trabajo'] != "" && isset($request['horario_trabajo'])) {
            $where .= ' and p.horario_trabajo = "'.$request['horario_trabajo'].'" ';
        }

        if($request['turno_trabajo'] != "" && isset($request['turno_trabajo'])){
            $where .= ' and p.turno_trabajo = "'.$request['turno_trabajo'].'" ';
        }
/*
        if ($request['pedido_pcd'] != "" && isset($request['pedido_pcd'])) {
            $where .= ' and p.discapacidad = "'.$request['pedido_pcd'].'" ';
        }
*/
      $sql = 'select *,p.id as id_pedido, p.created_at as fecha, p.descripcion as name_pedido  from pedidos p inner join empresas em on em.id = p.empresa_id '.$innerjoin.' where p.flag_estado = "1" '.$where.' '.$where_ubi;
      $pedidos=DB::select($sql);


       // $Lista=DB::select($sql);
     /*   $pedidos = DB::table(DB::raw('pedidos p'))
        ->select('horario_trabajo','cantidad','razon_social','remuneracion','p.id as id_pedido','p.descripcion as name_pedido',DB::raw("DATE_FORMAT(p.created_at,'%d/%m/%Y') AS fecha"))
        ->join(DB::raw('empresas EM'),'EM.id','=','p.empresa_id')
        ->whereRaw($where)->paginate(50);*/
      $tbl2='';
      $tbl='';
      try {
        if (count($pedidos)) {
          foreach ($pedidos as $key => $rst) {
            $tbl .= "<tr id=Frb-" . $rst->id_pedido. ">" .
                "<td>".$rst->id_pedido."</td>" .
                "<td>" . $rst->razon_social . "</td>" .
                "<td>" . $rst->fecha . "</td>" .
                "<td>" . $rst->name_pedido . "</td>" .
                  "<td>".$rst->remuneracion."</td>" .
                  "<td>".$rst->cantidad."</td>";
            $tbl .="<td>".$rst->horario_trabajo."</td>";
            $tbl .="<td  align=\"center\"><a href=".asset('vacante/detalle/'.$rst->id_pedido.'/?e='.Auth::user()->cod_entidad).">Ver</a></td>";

            $tbl .=  "</tr>";
          }
        }
        $tbl2 .='<tr><td colspan="10">';
       // $tbl2  .=$pedidos->setPath('pagin/FactIns');//->links('pagination::bootstrap-4');
        $tbl2 .='</td></tr>';
      } catch (Exception $e) {

      }
      ob_start();
      echo $tbl;
      $initial_content = ob_get_contents();
      ob_end_clean();

      ob_start();
      echo $tbl2;
      $initial_content2 = ob_get_contents();
      ob_end_clean();
      return array('table'=>$initial_content ,'theadPagin'=>$initial_content2);
    }

    public function editarDatos(){
      $personas = Persona::where('id', Auth::user()->persona_id)->get();

        $ficha = "";
        $ubigeo = "";

        foreach ($personas as $key => $value) {
            $ficha = FichaPersona::where('persona_id',$value->id)->get();
         //   $depa = UbigeoLista::where('ubi_codigo', substr($value->ubigeo,0,2)."0000")->select('ubi_descripcion as dep')->get();
          //  $pro = UbigeoLista::where('ubi_codigo', substr($value->ubigeo,0,4)."00")->select('ubi_descripcion as pro')->get();
          //  $dis = UbigeoLista::where('ubi_codigo', $value->ubigeo)->select('ubi_descripcion as dis')->get();
        }

      /*  foreach ($depa as $key => $value) {
          $p = $value->dep;
        }

        foreach ($pro as $key => $value) {
          $h = $value->pro;
        }

        foreach ($dis as $key => $value) {
          $t = $value->dis;
        }
        $ubigeo = $p."-".$h."-".$t;
      */
        $lista_depa=UbigeoLista::getDepartSelectOption();
        $lista_prov=UbigeoLista::getProviSelectOption('15');
        $lista_dist=UbigeoLista::getDistritoSelectOption('15','01');


      return view('persona/home-editar' , compact('personas','ficha','lista_depa','lista_prov','lista_dist'));
    }

    public function guardarDatos(Request $request){

      $edad = Carbon::parse($request->fecha_nac)->age; // 1990-10-25
      
      $personas = Persona::where('id', Auth::user()->persona_id)
          ->update(
            [
              'ape_pat' => $request->ape_paterno,
              'ape_mat' => $request->ape_materno,
              'name' => $request->nombre,
              'fecha_nacimiento' => $request->fecha_nac,
              'ubigeo' => $request->deparme.$request->provi.$request->distri,
              'direccion' => $request->direccion,
              'telefono' => $request->telefono,
              'sexo' => $request->sexo,
              'pers_edad' => $edad,

          ]);

          $ficha = FichaPersona::where('persona_id', Auth::user()->persona_id)
          ->update(
            [
              'presentacion' => $request->presentacion,
              'id_pais' => $request->pais,
              'fic_salario' => $request->salario,
              'licenciaConduc' => $request->licenciaConduc,
              'categoria' => $request->categoria,
              'porta_arma' => $request->port_armas,
              'arma_propia' => $request->arma_propia,
              'ocu_trabajo' => $request->ocu_trabajo,
              'ubi_lugar' => $request->ubi_lugar,
              'url_cv' => $request->url_cv,
          ]);

          $user = User::where('id', Auth::user()->id)
          ->update(
            [
              'name' => $request->nombre,

          ]);
         // dd($personas); // 26
        return redirect()->action('HomeController@index');

    }

    public function listfile(){
      $personas = Persona::where('id', Auth::user()->persona_id)->get();

        $ficha = "";

        foreach ($personas as $key => $value) {
            $ficha = FichaPersona::where('persona_id',$value->id)->get();
        }

      return view('persona/subir-hojavida', compact('ficha', 'personas'));
    }

    public function saveFile(Request $request){
     // echo $request->file;
      
      if($request->hasFile('file')){

      $ruta = $request->file('file')->store('files');

       $uardado= FichaPersona::where('persona_id', Auth::user()->persona_id)
      ->update(['filecv' => $ruta]);

      $file = $request->file('file');
      $name = time().$file->getClientOriginalName();
      $file->move(public_path().'/storage/files/', $ruta);
     }else{
      echo "vino vacio; ";
     }
     return redirect()->action('HomeController@index');


     // $request->file('file')->store('files');
    }

    public function ajaxFotoPostulante(Request $request){

     if($request->hasFile('file')){

      $ruta = $request->file('file')->store('persona');
       $uardado= FichaPersona::where('persona_id', Auth::user()->persona_id)
          ->update(['foto_post' => $ruta]);

      $file = $request->file('file');
      $name = time().$file->getClientOriginalName();
      $file->move(public_path().'/storage/persona/', $ruta);
     }


          $mensaje = "Se subio correctamente el archivo seleccionado";

       return redirect()->action('HomeController@index');

  }

  public function formacionBasica(Request $request){

     $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $value) {
        $formacion = FormacionBasica::create([
            'perbas_centroestudio' => $request->centro_estudio,
            'grainst_id' => $request->nivel_estudio,
            'perbas_fechaini' => $request->fecha_incio1,
            'perbas_fecafin' => $request->fecha_fin1,
            'pers_id' => Auth::user()->persona_id,
            'fichper_id' => $value->id,
        ]);

     }

     return redirect()->action('HomeController@index');

  }

  public function formacionSuperior(Request $request){
    $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $hey => $value) {
       if($hey == 0){
          $formacion = FormacionSuperior::create([
            'especialidad_id' => $request->profesion,
            'nivel_id' => $request->nivel_estudio,
          //  'centro_id' => $request->centro_estudio3,
            'otro_centro' => $request->otro_centro_estudio3,
            'fecha_inicio' => $request->fecha_incio1,
            'fecha_fin' => $request->fecha_fin1,
            'persu_estado' => $request->estado,
            'pers_id' => Auth::user()->persona_id,
            'fichper_id' => $value->id,
          ]);
       }

     }

     return redirect()->action('HomeController@index');
  }

  public function formacionPosgrado(Request $request){

    $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $value) {
        $formacion = FormacionPosgrado::create([
            'especialidad_id' => $request->profesion,
            'nivel_id' => $request->nivel_estudio,
           // 'centro_id' => $request->centro_estudio3,
            'otro_centro' => $request->otro_centro_estudio3,
            'fecha_inicio' => $request->fecha_incio1,
            'postsu_estado' => $request->estado_post,
            'fecha_fin' => $request->fecha_fin1,
            'pers_id' => Auth::user()->persona_id,
            'fichper_id' => $value->id,
        ]);

     }

    // return $this->index();
     return redirect()->action('HomeController@index');
  }

  public function idioma(Request $request){

    $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $value) {
        $formacion = Idioma::create([
            'idioma_nom' => $request->idioma,
            'idioma_nivel' => $request->nivel_idioma,
            'pers_id' => Auth::user()->persona_id,
            'fichper_id' => $value->id,
        ]);

     }
    // return $this->index();
     return redirect()->action('HomeController@index');

  }

  public function comptacion(Request $request){

     $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $value) {
        $formacion = Computacion::create([
            'compu_nom' => $request->compu,
            'compu_nivel' => $request->nivel_compu,
            'pers_id' => Auth::user()->persona_id,
            'fichper_id' => $value->id,
            'compu_otro' => $request->otro_compu,
        ]);

     }
    // return $this->index();
     return redirect()->action('HomeController@index');


  }

  public function estudiosAdicionales(Request $request){

     $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $value) {
        $formacion = EstudiosAdicionales::create([
            'estu_tipo' => $request->estudio_tipo,
            'estu_nombre' => $request->nombre_curso,
            'estu_cetro' => $request->centro_curso,
            'pers_id' => Auth::user()->persona_id,
            'fecha_fin' => $request->fecha_fin_estudioadi,
            'fichper_id' => $value->id,
        ]);

     }
    // return $this->index();
     return redirect()->action('HomeController@index');


  }

  public function experienciaLaboral(Request $request){


    $id = FichaPersona::where('persona_id', Auth::user()->persona_id)->select('id')->get();
     foreach ($id as $value) {
          $formacion = Experiencia::create([
            'id_ocupacion' => $request->ocupacion,
            'nom_empresa' => $request->empresa,
            'funciones' => $request->funciones_cargo,
            'fech_inicio' => $request->fecha_ini_empresa,
            'fech_fin'  => $request->fecha_fin_empresa,
            'pers_id' => Auth::user()->persona_id,
            'otro_cargo' => $request->otro_cargo,
            'fichper_id' => $value->id,
        ]);

     }
     //return $this->index();
     return redirect()->action('HomeController@index');


  }

  public function postulaciones(){


    $data['list_postulaciones'] = PedidoPostulante::select('pedidos.id as pedido_codigo','pedidos.descripcion','denominacion',
      DB::raw('DATE_FORMAT(PP_Fecha,"%d/%m/%Y") as PP_Fecha'),DB::raw('DATE_FORMAT(PostPed_aptoFecha,"%d/%m/%Y") as PostPed_aptoFecha'),
      DB::raw('(select count(*) from postulantepedido where postulantepedido.idPedido = pedidos.id) as cant_postulantes'),
      'flag_estado', 'PP_Visto', 'empresas.razon_social',DB::raw('DATE_FORMAT(Fecha_visto,"%d/%m/%Y") as Fecha_visto'),'PostPed_apto',
      DB::raw('DATE_FORMAT(PP_StatusFecha,"%d/%m/%Y") as PP_StatusFecha'),'PP_Status',
      DB::raw('DATE_FORMAT(PostPed_contraFecha,"%d/%m/%Y") as PostPed_contraFecha'),'PostPed_contra')
      ->join('pedidos', 'pedidos.id', '=', 'postulantepedido.idPedido')
      ->join('empresas', 'empresas.id', '=', 'pedidos.empresa_id')
      ->where('idPostulante', '=', Auth::user()->persona_id)
      ->orderBy('postulantepedido.PP_Fecha', 'DESC')
      ->paginate(30);

    //$data['']
    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['submenu_color'] = $value->submenu_color;
            $data['subcolor_entidad'] = $value->subcolor_entidad;

    }

    return view('postulaciones', $data);

  }

  public function convocatoria(){


    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['submenu_color'] = $value->submenu_color;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
    }

    $date = Carbon::now();
    $date = $date->format('Y-m-d');
    $list_convoca = false;
    $usuario = User::select('fichapersona.ocu_trabajo','fichapersona.ubi_lugar','ocupaciones.nombre as name_ocupacion')
    ->join('fichapersona' ,'fichapersona.persona_id','=','users.persona_id')
    ->join('ocupaciones', 'ocupaciones.ocup_codigo','=','fichapersona.ocu_trabajo')
    ->where('users.id',Auth::user()->id)
    ->get();

    foreach ($usuario as $item){

      $porciones = explode(" ", $item->name_ocupacion);

      $vacantes = Vacante::select('vacantes.procedencia','nombre_especialidad as descripcion','insti_logo as perfil_foto',
        'nombre as razon_social','vacantes.fecha_postula as created_at','vacantes.id as cod_ped', 'ubi_descripcion',
        'conocimiento as otro_conocimiento')
        ->join('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->leftJoin('ubigeo' , 'vacantes.ubigeo','=','ubigeo.ubi_codigo')
        ->where('fecha_postula', '>=', $date.' 00:00:00')
        ->where('vacantes.ubigeo',$item->ubi_lugar)
        ->where((function($query) use ($porciones){
              foreach($porciones as $value){
                  $query->orWhere('vacantes.formacion_academica', 'LIKE', '%'.$value.'%');
              }
        }));

    $list_convoca = Pedido::select('pedidos.procedencia','pedidos.descripcion','empresas.perfil_foto','empresas.razon_social',
     'pedidos.created_at', 'pedidos.id as cod_ped','ubigeo.ubi_descripcion','pedidos.otro_conocimiento')
        ->join('empresas','empresas.id','=','pedidos.empresa_id')
        ->join('ubigeo', 'ubigeo.ubi_codigo', '=', 'pedidos.ubigeo')
        ->where('pedidos.denominacion',$item->ocu_trabajo)
        ->where(DB::raw('LEFT(pedidos.ubigeo,2)'),substr($item->ubi_lugar,0,2))
        ->where('pedidos.fecha_limite','>', $date.' 23:59:59')
        ->where('pedidos.flag_estado','1')
      ->unionAll($vacantes)
      ->orderBy('created_at', 'DESC')
      ->paginate(30);

    }



    $data['list_convoca'] = $list_convoca;




    return view('convocatoria',$data);
  }

  public function configuracion(){

    $personas = Persona::leftJoin('ubigeo','ubigeo.ubi_codigo','=',DB::raw('CONCAT(LEFT(personas.ubigeo,2),"0000")'))
      ->where('personas.id', Auth::user()->persona_id)->get();

    $ficha = FichaPersona::select('fichapersona.id as ficha_id','fichapersona.persona_id')->join('paises','paises.pais_cod','=','fichapersona.id_pais')
      ->where('fichapersona.persona_id',Auth::user()->persona_id)->get();

    $data['personas'] = $personas;
    $data['ficha'] = $ficha;

    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['submenu_color'] = $value->submenu_color;
    }


    return view('configuracion', $data);
  }

  public function notificacion(){

    $personas = Persona::join('ubigeo','ubigeo.ubi_codigo','=',DB::raw('CONCAT(LEFT(personas.ubigeo,2),"0000")'))
      ->where('personas.id', Auth::user()->persona_id)->get();

    $ficha = FichaPersona::select('fichapersona.id as ficha_id','fichapersona.persona_id')
    ->join('paises','paises.pais_cod','=','fichapersona.id_pais')
    ->where('fichapersona.persona_id',Auth::user()->persona_id)->get();

    $data['personas'] = $personas;
    $data['ficha'] = $ficha;

    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    $data['list_notificacion'] = Notificacion::select('notificaciones.id_pedido','empresas.razon_social','notificaciones.not_estado','pedidos.descripcion')
    ->where('id_users',Auth::user()->persona_id)
    ->join('pedidos','pedidos.id','=','notificaciones.id_contacto')
    ->join('contactos','contactos.id','=','notificaciones.id_contacto')
    ->join('empresas','empresas.id','=','contactos.empresa_id')
    ->orderBy('notificaciones.updated_at','desc')
    ->paginate(15);

    foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['submenu_color'] = $value->submenu_color;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
    }

    return view('notificacion', $data);
  }

  public function favoritas(){

    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['submenu_color'] = $value->submenu_color;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
    }


    $vacante_favorito = Favorito::select(DB::raw('DATE_FORMAT(fecha_postula,"%d/%m/%Y") as fecha_limite'),
    'institucion.ruc as numero_documento','institucion.nombre as razon_social',
    'vacantes.nombre_especialidad as descripcion','vacantes.id as codigo','ped_favoritos.created_at','pedfav_procedencia')
    ->where('users_id',Auth::user()->id)
    ->where('pedfav_procedencia','2')
    ->join('vacantes','vacantes.id','=','ped_favoritos.pedidos_id')
    ->join('institucion','institucion.id','=','vacantes.id_institucion');


    $data['list_favoritos'] = Favorito::select(DB::raw('DATE_FORMAT(fecha_limite,"%d/%m/%Y") as fecha_limite'),
    'numero_documento','razon_social','pedidos.descripcion','pedidos.id as codigo','ped_favoritos.created_at','pedfav_procedencia')
    ->where('users_id',Auth::user()->id)
    ->where('pedfav_procedencia','1')
    ->join('pedidos','pedidos.id','=','ped_favoritos.pedidos_id')
    ->join('empresas', 'empresas.id','=','pedidos.empresa_id')
    ->unionAll($vacante_favorito)
    ->orderBy('created_at', 'DESC')
    ->paginate(30);


    return view('favoritas', $data);

  }

  public function eliminaBasico($id){

    $deletedRows = FormacionBasica::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }

  public function eliminaSuperior($id){

    $deletedRows = FormacionSuperior::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }

    public function eliminaPosgrado($id){

    $deletedRows = FormacionPosgrado::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }

    public function eliminaIdioma($id){

    $deletedRows = Idioma::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }
    public function eliminaOfimatica($id){

    $deletedRows = Computacion::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }
    public function eliminaAdcionales($id){

    $deletedRows = EstudiosAdicionales::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }
    public function eliminaExperiencia($id){

    $deletedRows = Experiencia::where('id', $id)->delete();
    //echo $deletedRows;
    return redirect()->action('HomeController@index');

  }

  public function updateConfiguracion(Request $request){

    $this->validate($request, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);


    $user = User::where('id', Auth::user()->id)
            ->update([
            'email' => $request->email,
            'password' => Hash::make($request->password),
    ]);

    $persona = Persona::where('id', Auth::user()->persona_id)->update([
            'email' => $request->email,
        ]);


    return redirect()->action('HomeController@configuracion');

  }

  public function updateConfiguracionCurriculum(Request $request){

    $persona = Persona::where('id', Auth::user()->persona_id)->update([
            'per_curri_visible' => $request->curri_visible,
        ]);

    return redirect()->action('HomeController@configuracion');
  }

  public function updateConfiguracionEliminar(Request $request){

    $persona = Persona::where('id', Auth::user()->persona_id)->update([
            'per_elimuna_curri' => $request->elimina,
        ]);

    return redirect()->action('HomeController@configuracion');
  }

  public function editBasico($id){

    $formacion_basica = FormacionBasica::
      select('pers_basica.perbas_centroestudio', 'pers_basica.perbas_fechaini', 'pers_basica.perbas_fecafin', 'pers_basica.id as basica_id', 'pers_basica.grainst_id')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->where('pers_basica.id', '=', $id)
      ->get();

    return json_encode($formacion_basica,JSON_PRETTY_PRINT);
  }

  public function editarFormularioDatos(Request $request){

    $persona = FormacionBasica::where('pers_id', Auth::user()->persona_id)
    ->where('pers_basica.id',$request->codigo_basico)
    ->update([
      'pers_basica.perbas_centroestudio' => $request->centro_estudio,
      'pers_basica.grainst_id' => $request->nivel_estudio,
      'pers_basica.perbas_fechaini' => $request->fecha_incio1,
      'pers_basica.perbas_fecafin' => $request->fecha_fin1,
    ]);

    return redirect()->action('HomeController@index');

  }

  public function editSuperior($id){

    $formacion_superior =FormacionSuperior::
      select('per_superior.especialidad_id', 'per_superior.nivel_id', 'per_superior.otro_centro', 'per_superior.fecha_inicio' ,'per_superior.fecha_fin', 'per_superior.persu_estado')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->where('per_superior.id', $id)
      ->get();

    return json_encode($formacion_superior,JSON_PRETTY_PRINT);

  }

  public function editarFormularioSuperior(Request $request){

    $persona = FormacionSuperior::where('pers_id', Auth::user()->persona_id)
    ->where('per_superior.id',$request->codigo_superior)
    ->update([
      'per_superior.especialidad_id' => $request->profesion,
      'per_superior.nivel_id' => $request->nivel_estudio,
      'per_superior.persu_estado' => $request->estado,
      'per_superior.otro_centro' => $request->otro_centro_estudio3,
      'per_superior.fecha_inicio' => $request->fecha_incio1,
      'per_superior.fecha_fin' => $request->fecha_fin1,
    ]);

    return redirect()->action('HomeController@index');

  }

  public function editPosgrado($id){

    $formacion_posgrado =FormacionPosgrado::
      select('per_posgrado.especialidad_id', 'per_posgrado.nivel_id', 'per_posgrado.otro_centro', 'per_posgrado.fecha_inicio' ,'per_posgrado.fecha_fin', 'per_posgrado.postsu_estado')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->where('per_posgrado.id', $id)
      ->get();

    return json_encode($formacion_posgrado,JSON_PRETTY_PRINT);

  }


  public function editarFormularioPosgrado(Request $request){

    $post = FormacionPosgrado::where('pers_id', Auth::user()->persona_id)
    ->where('per_posgrado.id',$request->codigo_posgrado)
    ->update([
      'per_posgrado.especialidad_id' => $request->profesion,
      'per_posgrado.nivel_id' => $request->nivel_estudio,
      'per_posgrado.postsu_estado' => $request->estado_post,
      'per_posgrado.otro_centro' => $request->otro_centro_estudio3,
      'per_posgrado.fecha_inicio' => $request->fecha_incio1,
      'per_posgrado.fecha_fin' => $request->fecha_fin1,
    ]);

    return redirect()->action('HomeController@index');

  }


  public function editIdioma($id){

    $idioma =Idioma::
      select('pers_idioma.idioma_nom', 'pers_idioma.idioma_nivel')
      ->where('pers_id', '=', Auth::user()->persona_id)
      ->where('pers_idioma.id', $id)
      ->get();

    return json_encode($idioma,JSON_PRETTY_PRINT);

  }

  public function editarFormularioIdioma(Request $request){

    $post = Idioma::where('pers_id', Auth::user()->persona_id)
    ->where('pers_idioma.id',$request->codigo_idioma)
    ->update([
      'pers_idioma.idioma_nom' => $request->idioma,
      'pers_idioma.idioma_nivel' => $request->nivel_idioma,
    ]);

    return redirect()->action('HomeController@index');

  }

  public function editCompu($id){

    $compu =Computacion::
    select('pers_compu.compu_nom', 'pers_compu.compu_nivel', 'pers_compu.compu_otro')
    ->where('pers_id', '=', Auth::user()->persona_id)
    ->where('pers_compu.id', $id)
    ->get();

  return json_encode($compu,JSON_PRETTY_PRINT);

  }

  public function editarFormularioCompu(Request $request){

    $post = Computacion::where('pers_id', Auth::user()->persona_id)
    ->where('pers_compu.id',$request->codigo_compu)
    ->update([
      'pers_compu.compu_nom' => $request->compu,
      'pers_compu.compu_nivel' => $request->nivel_compu,
      'pers_compu.compu_otro' => $request->otro_computacion,
    ]);
    //  echo $post;
    return redirect()->action('HomeController@index');

  }


  public function editEstudios($id){

    $estudios =EstudiosAdicionales::
    select('pers_estudiosadi.estu_tipo', 'pers_estudiosadi.estu_nombre', 'pers_estudiosadi.estu_cetro', 'pers_estudiosadi.fecha_fin')
    ->where('pers_id', '=', Auth::user()->persona_id)
    ->where('pers_estudiosadi.id', $id)
    ->get();

  return json_encode($estudios,JSON_PRETTY_PRINT);

  }

  public function editarFormularioEstudios(Request $request){

    $post = EstudiosAdicionales::where('pers_id', Auth::user()->persona_id)
    ->where('pers_estudiosadi.id',$request->codigo_adicional)
    ->update([
      'pers_estudiosadi.estu_tipo' => $request->estudio_tipo,
      'pers_estudiosadi.estu_nombre' => $request->nombre_curso,
      'pers_estudiosadi.estu_cetro' => $request->centro_curso,
      'fecha_fin' => $request->fecha_fin_estudioadi,
    ]);
    //  echo $post;
    return redirect()->action('HomeController@index');

  }


  public function editExperiencia($id){

    $experiencia =Experiencia::
    select('pers_experiencia.id as codigo','pers_experiencia.nom_empresa', 'pers_experiencia.funciones',
    'pers_experiencia.fech_inicio', 'pers_experiencia.fech_fin', 'pers_experiencia.id_ocupacion', 'pers_experiencia.otro_cargo')
    ->where('pers_id', '=', Auth::user()->persona_id)
    ->where('pers_experiencia.id', $id)
    ->get();

  return json_encode($experiencia,JSON_PRETTY_PRINT);

  }

  public function editarFormularioExperiencia(Request $request){

    $post = Experiencia::where('pers_id', Auth::user()->persona_id)
    ->where('pers_experiencia.id',$request->codigo_experiencia)
    ->update([
      'pers_experiencia.nom_empresa' => $request->empresa,
      'pers_experiencia.funciones' => $request->funciones_cargo,
      'pers_experiencia.fech_inicio' => $request->fecha_ini_empresa,
      'pers_experiencia.fech_fin' => $request->fecha_fin_empresa,
      'pers_experiencia.id_ocupacion' => $request->ocupacion,
      'pers_experiencia.otro_cargo' => $request->otro_cargo,
    ]);
      //echo $post;
    return redirect()->action('HomeController@index');

  }

  public function guardaFavorito($postulante, $pedido){

    $favorito = Favorito::create([
      'users_id' => $postulante,
      'pedidos_id' => $pedido,
      'pedfav_procedencia' => '1'
    ]);

    return json_encode($favorito, JSON_PRETTY_PRINT);

  }

  public function CancelarFavorito($postulante, $pedido){

    $favorito = Favorito::where('users_id', $postulante)
    ->where('pedidos_id', $pedido)
    ->where('pedfav_procedencia','1')
    ->delete();

    $value ="0";
    return json_encode($value, JSON_PRETTY_PRINT);
  }

  public function guardaFavoritoVacante($postulante, $pedido){

    $favorito = Favorito::create([
      'users_id' => $postulante,
      'pedidos_id' => $pedido,
      'pedfav_procedencia' => '2'
    ]);

    return json_encode($favorito, JSON_PRETTY_PRINT);

  }

  public function CancelarFavoritoVacante($postulante, $pedido){

    $favorito = Favorito::where('users_id', $postulante)
    ->where('pedidos_id', $pedido)
    ->where('pedfav_procedencia','2')
    ->delete();

    $value ="0";
    return json_encode($value, JSON_PRETTY_PRINT);
  }


}


