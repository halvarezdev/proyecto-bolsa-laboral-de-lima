<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class AutocompleteIns extends Controller{
    public function get_routesIns(Request $request){
        $report=$request->method;
        $nombreclase = "\AutocompleteIns::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function getFinderCapInsumo($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $descr=$request['term'];
            $sql="SELECT insu_comision,insu_verstock, CONCAT(LEFT(CONCAT(insu_codalterno,SPACE(10),''),20),insu_nomb) AS value,
         insu_id,insu_codalterno,insu_nomb,i.unid_id, insu_preccosto,
          unid_unicompra, un.unid_uniconsumo AS und, insu_precventa,
          insu_preccostod,insu_precventad from  insumos i 
          INNER JOIN unidades un ON un.unid_id = i.unid_id WHERE insu_estado = 1";
      
        if (!$descr=="") {
            $union='';
            $tex=explode('*', $descr);
            $cn=count($tex);
            if ($cn>0) {
                for ($i=0; $i < $cn; $i++) { 
                    $union.=$tex[$i]."%";
                }
                $sql.=' AND (insu_nomb LIKE '."'".$union."' OR insu_codalterno LIKE '%".$union."' )"  ;
            }else{
                $sql.=' AND (insu_nomb LIKE '."'%".$descr."' OR insu_codalterno LIKE '%".$descr."%' )"  ;
            }
          
                $sql .= " ORDER BY insu_codalterno ASC";
          
        }
        $sql .= "  LIMIT 0, 200";
            $Lista=DB::select($sql);
             
        }catch (\Exception $exc) {
            $Lista=array('value'=>$exc->getMessage().' Line=> '.$exc->getLine());
			 
        }
        return  $Lista; 
    }
    public function getFinderCapDirFisc($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql = "SELECT CONCAT(fisc_id,' ',fisc_dire)AS value ,fisc_id,fisc_dire from direc_fiscal
             where fisc_estado=1";
      
            if (!$request['term']=="") {
                if (substr($request['term'],0,1)=="*"){
                    $sql .= " AND fisc_id LIKE '%".substr($request['term'],1,strlen($request['term']))."%'";
                } else {
                    $sql .= " AND fisc_dire like '".$request['term']."%'";
                }
            }
            $sql .= " ORDER BY fisc_id ASC "
                . " LIMIT 0, 25";
            $Lista=DB::select($sql);
            
            $response['status']=1;
        }catch (\Exception $exc) {
            $Lista=array('value'=>'');
			 
        }
        return  $Lista;  
    }
    public function getFinderCapClVePro($request){
        $response['status']=0;
		$response['message']='';
        try{  
            $sql = "SELECT clie_repre,clie_ruc,clie_id,CONCAT(clie_idalterno,' ',clie_nomb)AS value ,clie_idalterno,clie_nomb 
            from clientes  WHERE clie_id IS NOT NULL ";
            $sql .=' AND clie_tipo IN('.$this->getStr4IN($request['tipocli']).')';
            $descr=$request['term'];
            if (!$descr=="") {
                $union='';
                $tex=explode('*', $descr);
                $cn=count($tex);
                if ($cn>0) {
                    for ($i=0; $i < $cn; $i++) { 
                        $union.=$tex[$i]."%";
                    }
                    $sql.=' AND (clie_nomb LIKE '."'%".$union."' OR clie_idalterno LIKE '%".$union."' OR clie_ruc LIKE '%".$union."')"  ;
                }else{
                    $sql.=' AND (clie_nomb LIKE '."'%".$descr."' OR clie_idalterno LIKE '%".$descr."%' OR clie_ruc LIKE '%".$descr."%' )"  ;
                }
                
            } 
            
            $sql .= " ORDER BY clie_nomb ASC "
                . " LIMIT 100";
            $Lista=DB::select($sql);
            
            $response['status']=1;
        }catch (\Exception $exc) {
            $Lista=array('value'=>$exc->getMessage().' Line=> '.$exc->getLine());
			 
        }
        return  $Lista; 
    }
    public function getFinderCapTienda($request){
         
        $response['status']=0;
		$response['message']='';
        try{  
            $sql = "SELECT CONCAT(empr_id,' ',empr_social)AS value ,empr_id,empr_social from empresas where clie_id is NULL";
      
            if (!$request['term']=="") {
                if (substr($request['term'],0,1)=="*"){
                    $sql .= " AND empr_id LIKE '%".substr($request['term'],1,strlen($request['term']))."%'";
                } else {
                    $sql .= " AND empr_social like '".$request['term']."%'";
                }
            }
            $sql .= " ORDER BY empr_social ASC "
                . " LIMIT 0, 25";
            $Lista=DB::select($sql);
            
            $response['status']=1;
        }catch (\Exception $exc) {
            $Lista=array('value'=>'');
			 
        }
        return  $Lista;  
    }
    public function getFinderCapZonaVenta($request){
         
        $response['status']=0;
		$response['message']='';
        try{  
            $sql = "SELECT CONCAT(zona_id,' ',zona_nomb)AS value ,zona_id,zona_nomb from zonas where zona_estado=1";
      
            if (!$request['term']=="") {
                if (substr($request['term'],0,1)=="*"){
                    $sql .= " AND zona_id LIKE '%".substr($request['term'],1,strlen($request['term']))."%'";
                } else {
                    $sql .= " AND zona_nomb like '".$request['term']."%'";
                }
            }
            $sql .= " ORDER BY zona_nomb ASC "
                . " LIMIT 0, 25";
            $Lista=DB::select($sql);
            
            $response['status']=1;
        }catch (\Exception $exc) {
            $Lista=array('value'=>'');
			 
        }
        return  $Lista;  
    }
    public  function getStr4IN($str) {
        $res = "";
        $vals =explode(',', $str);

        for ($i = 0; $i < count($vals); $i ++){
            $res .= "'" . $vals[$i] . "', ";
        } 

        return substr($res,0, strlen($res) - 2);
    }
}