<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
 

class IntegracionIntranetMMLController extends Controller{
    function integracionIntranetMML(Request $request){
        $codigoAcceso= $request->codigoAcceso;
        $_SESSION['USUARIO_SESSION']=$request->codigoAcceso;
        $httpcode=200;
        $url_sis=env('APP_URL');
     
        $url=env('APP_URL_CALIDAD_INTRANET') ;

        $curl = curl_init(); 
        curl_setopt_array($curl, array(
        CURLOPT_URL =>$url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('CodigoAcResp' =>$codigoAcceso),
        
        )); 
    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    echo "<pre>";
    curl_close($curl);
    
    if($httpcode==200){
        $resultado= json_decode($response);
         
        if(count($resultado->listaUsu)>0){
            $usua=$resultado->listaUsu[0];
            
            $listaMen =array( 
							'_SESSION_'=>$resultado->listaMen);	
                            session($listaMen);
            $dni=$usua->USUDNI;
            $default= DB::select("SELECT * FROM entidadusers where numero_documento= '$dni'"); 
            if(count($default)<=0){
                $pass2=Hash::make($dni);
                DB::table('entidadusers')->insert([
                    ['idperfil' => '0','numero_documento'=>$dni, 'textoalter'=>$dni,'email'=>$dni,'entidad_id'=>1,'estado'=>1,
                    'name' =>$usua->USUAPEPAT.' '.$usua->USUAPEMAT.' '.$usua->USUNOM,'password'=>$pass2], 
                ]);
            }
            if (Auth::guard('entidad')->attempt([ 'email' =>$dni, 'password' => $dni ],$dni)) {
                return redirect()->away($url_sis."entidad/inicio");
            }
            
          //return redirect()->back()->withInput($request->only('email', 'remenber'));
        }
    }
    
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"codigoAcceso=". $codigoAcceso); // the SOAP request
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        print_r($response);*/
    }
}