<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class Controlador extends Controller{

    
    public function get_upload_img(Request $request){
        
        $response['status']=0;
		$response['message']='';
        try{  
         
            $filename=$request->input('filename');
            $file=$request->input('file');
            $nombreimg=$request->input('nombreimg');
            $response['status']=1;
             
            $extencion = strtolower($request->file($filename)->getClientOriginalExtension());
            $fileName =$nombreimg.'.'.$extencion;
            $path = "static/images/".$file;

   
            if (!file_exists("static/images/".$file)) {
                if(!mkdir("static/images/".$file, 0777, true)) {
                  throw new \Exception('Fallo al crear las carpetas...');
                }
            }
            if ($request->file($filename)->isValid()){
                $request->file($filename)->move($path, $fileName);
            }
            $response['name_file']=$request->input('nombreimg').'.'.$extencion;
            $response['deleti']=$request->input('deleti');
            $response['extencion']=$extencion;
            $response['uploaded_file_url']=$path.$fileName;

        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return   $response;      
    }
    
    public function get_recor_img(Request $request){
        $response['status']=0;
		$response['message']='';
        try{  
            $b64=$request->base64;
            
            if (file_exists("static/images/".$request->file)) {
                $image = $this->base64_to_jpeg($b64, $request->file2);
            }
            $response['status']=1; 
            $response['image']=$image; 
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return   $response;
    }
    function base64_to_jpeg( $base64_string, $output_file ) {
       $ifp = fopen( $output_file, 'wb' ); 
       $data = explode( ',', $base64_string );
       fwrite( $ifp, base64_decode( $data[ 1 ] ) );
       fclose( $ifp ); 
       return $output_file; 
    }
    
    
}