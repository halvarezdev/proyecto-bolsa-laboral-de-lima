<?php

namespace App\Http\Controllers;
use App\Http\Controllers\control\UbigeoLista;
use App\Http\Controllers\control\LibreriaFecha;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PedidoComputacion;
use App\FormacionSuperior;
use App\GradoInstruccion;
use App\PedidoPostulante;
use App\TipoDocumento;
use App\FichaPersona;
use App\PedidoIdioma;
use App\Experiencia;
use Carbon\Carbon;
use App\Ocupacion;
use App\Profesion;
use App\Contacto;
use App\Empresa;
use App\Entidad;
use App\ReporteEntidad;
use App\Persona;
use App\Maestro;
use App\Pedido;
use App\Ubigeo;
use App\Sector;
use App\Area;
use App\User;
use App\Ciiu;

class EntidadController extends Controller
{

  protected $redirectTo = 'auth.entidad-login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:entidad');
    }

    public function listEntity()
    {
      $entidad = Entidad::all();
      $entidades = Entidad::where('id','=',Auth::user()->entidad_id)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['enti_nombre'] = $value->enti_nombre;
            $data['fondo_intranet'] = $value->fondo_intranet;
        }

        $resultado = "bienvenido";
        $data['resultado'] = $resultado;
        $data['menu'] = '2';
        $data['entidad'] = $entidad;
      return view('entidad.list-entidad', $data);
    }

    public function deleteEntity($id, $flag){
      Entidad::where('id', $id)
      ->update([
          'enti_flag' => $flag,
      ]);
    return redirect()->action('EntidadController@listEntity');
    }

    public function updateEntity(Request $request){
      Entidad::where('id', $request->enti_id)
      ->update([
          'enti_nombre' => strtoupper($request->enti_nombre),
      ]);
    return redirect()->action('EntidadController@listEntity');
    }

    public function saveEntity(Request $request){
      $entidad = new Entidad();
      $entidad->enti_nombre = strtoupper($request->enti_nombre);
      $entidad->enti_flag = 1;
      $entidad->save();
      return redirect()->action('EntidadController@listEntity');
    
    }
    
    public function index(){

        $entidades = Entidad::where('id','=',Auth::user()->entidad_id)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['enti_nombre'] = $value->enti_nombre;
            $data['fondo_intranet'] = $value->fondo_intranet;
        }

        $resultado = "bienvenido";
        $data['resultado'] = $resultado;
        $data['menu'] = '2';

        return view('entidad-home', $data);
        
    }

    public function registroPersonas(){

        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
      
        $data['documentos'] = TipoDocumento::get();
        $data['ubigeo'] = Ubigeo::where($where)->get();
        $data['ocupaciones'] = Ocupacion::where('status','=','9')->get();
        $data['entidad'] = Auth::user()->entidad_id;
        $data['tipo'] = "2";
        $data['menu'] = '2';
        return view('entidad.registro-persona', $data);
    }

    public function registroPersonaSutmib(Request $request){
        
     /*   $rules = array
        (
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'tipo_documento' => ['required', 'string'],
            'numero_documento' => ['required', 'unique:users'],
        );
   

        $messages = array
        (
            'required'   => 'El campo es obligatorio.',
            'different'  => 'El campo ubicacion y traslado deben ser diferente.',
            'unique' => 'Este dato ya se encuentra registrado en nuestras base de datos',
            'date'       => 'La fecha es invalida',
        );
  

        Validator::make($request->all(), $rules, $messages);
        */
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'tipo_documento' => ['required', 'string'],
            'numero_documento' => ['required', 'unique:users'],
        ]);
        if( $validator->fails() ) {
          //  return $validator->messages()->first();
           // return redirect()->back($validator->messages()->first());
            return redirect()->back()->withInput()->withErrors( $validator );
        }
        $persona = Persona::create([
            'tipo_documento' => $request->tipo_documento,
            'numero_documento' => $request->numero_documento,
            'email' => $request->email,
            'name' => $request->name,
            'ape_pat' => $request->ape_pat,
            'ape_mat' => $request->ape_mat,
            'cod_entidad' => $request->entidad,
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'numero_documento' => $request->numero_documento,
            'persona_id' => $persona->id,
            'cod_entidad' => $request->entidad,
        ]);

        $ficha = FichaPersona::create([
            'persona_id' => $persona->id,
            'ocu_trabajo' => $request->ocu_trabajo,
            'ubi_lugar' => $request->ubi_lugar,
            'cod_entidad' => $request->entidad,
            'tipo_entidad' => '2',
        ]);

        return redirect()->action('EntidadController@index');
        
    }

    public function listadoPeronas(){

        $data['listado_persona'] = Persona::select('personas.name as nombre', 'personas.numero_documento',
        'personas.ape_pat', 'personas.ape_mat', 'personas.email', 'fichapersona.tipo_entidad')
        ->join('fichapersona', 'fichapersona.persona_id', '=', 'personas.id')
        ->where('fichapersona.cod_entidad', Auth::user()->entidad_id)
        ->paginate(15);
        $data['menu'] = '2';
        $data['entidad'] = Auth::user()->entidad_id;

        return view('entidad.lista-persona', $data);

    }

    public function registroEnpresa(){
        
        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
        $data['ubigeo'] = Ubigeo::where($where)->get();
        $data['documentos'] = TipoDocumento::orWhere('id', '=', '2')
                ->orWhere('id', '=', '5')
                ->orWhere('id', '=', '6')
                ->orderBy('id', 'desc')
                ->get();
        $data['sector'] = Sector::get();    
        $data['entidad'] = Auth::user()->entidad_id;
        $data['tipo'] = "2";
        $data['menu'] = '2';
        $data['entidad'] = Auth::user()->entidad_id;

        return view('entidad/registro-empresa', $data);

    }

    public function registroEmpresaSutmib(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:contactos'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
          //  'tipo_documento' => ['required', 'string'],
            'numero_documento' => ['required', 'unique:contactos'],
          //  'numero_documento' => ['required', 'unique:personas'],
        ]);
        if( $validator->fails() ) {
            //  return $validator->messages()->first();
             // return redirect()->back($validator->messages()->first());
              return redirect()->back()->withInput()->withErrors( $validator );
          }
        $persona = Persona::create([
            'tipo_documento' => $request->tipo_documento_contacto,
            'numero_documento' => $request->numero_documento_contacto,
            'telefono' => $request->telefono,
            'email' => $request->email_contacto,
            'name' => $request->name,
        ]);
       
        $empresa = Empresa::create([
            'tipo_documento' => $request->tipo_documento,
            'numero_documento' => $request->numero_documento,
            'razon_social' => $request->razon_social,
            'descripcion' => $request->descripcion,
            'rubro' => $request->rubro,
            'sitio_web' => (isset($request->sitio_web) ? $request->sitio_web : ""),
            'direccion' => $request->direccion,
            'ubigeo' => $request->departamento.$request->provincia.$request->distrito,
            'cod_entidad' => $request->entidad,
            'tipo_entidad' => '2',
        ]);
        
        $contacto = Contacto::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'cargo' => $request->cargo,
            'numero_documento' => $request->numero_documento_contacto,
            'empresa_id' => $empresa->id,
            'persona_id' => $persona->id,
            'cod_entidad' => $request->entidad,
            
        ]);

        return redirect()->action('EntidadController@index');

    }

    public function listadoEmpresa(){

        $data['listado_empresa'] = Empresa::select('empresas.tipo_entidad','empresas.numero_documento','empresas.razon_social',
        'empresas.descripcion','empresas.id as codigo_empresa', 'contactos.email')
        ->join('contactos', 'contactos.empresa_id', '=', 'empresas.id')
        ->where('empresas.cod_entidad', Auth::user()->entidad_id)
        ->paginate(15);
        $data['menu'] = '2';
        $data['entidad'] = Auth::user()->entidad_id;

        return view('entidad.lista-empresa', $data);

    }

    public function DatosEmpresa($cod_empresa){

        $data['empresa'] = Empresa::select('empresas.tipo_entidad','empresas.numero_documento','empresas.razon_social',
        'empresas.descripcion','empresas.id as codigo_empresa')
        ->where('empresas.id', $cod_empresa)
        ->get();

        $data['listado_pedido'] = Pedido::select('pedidos.descripcion','pedidos.num_vacantes','pedidos.tipo_entidad', 
        'pedidos.id as codigo_pedido','pedidos.empresa_id')
        ->where('pedidos.empresa_id', $cod_empresa)
        ->paginate(15);
        $data['menu'] = '2';
        $data['entidad'] = Auth::user()->entidad_id;

        return view('entidad.lista-pedidos', $data);
    }

    public function entidadNuevoPedido($cod_empresa){

        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
        $data['empresas']= Empresa::where('id',$cod_empresa)->get();
        $data['grado_intruccion']= $grado_intruccion = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','1')->orWhere('grad_flag','2')->orWhere('grad_flag','3')->get();
        $data['ubigeo']= Ubigeo::where($where)->get(); 
        $data['computacion']= Maestro::where('MAESP_CODIGO1','=','3')->get(); 
        $data['idiomas']= Maestro::where('MAESP_CODIGO1','=','6')->get();
        $data['areas']= Area::get();
        $data['ocupacion']= Ocupacion::where('status','=','9')->get();
        $data['grado_normal']= GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','2')->get();
        $data['sector']= Sector::get();
        $data['accion']="2";
        $data['grado_posgrado'] = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','3')->get();
        $data['tipo_entidad'] = "2";
        $data['cod_empresa'] = $cod_empresa;
        $data['menu'] = '2';
        $data['entidad'] = Auth::user()->entidad_id;
        $data['contactos'] = Contacto::where('empresa_id', $cod_empresa)->get();
        $data['documentos'] = TipoDocumento::orWhere('id', '=', '2')
        ->orWhere('id', '=', '5')
        ->orWhere('id', '=', '6')
        ->orderBy('id', 'desc')
        ->get();

        return view('empresa/registro-vacante', $data);

    }

    public function registroPedidoSubmit(Request $request){

        $empresa_cod = Input::get('empresa_cod');

        $pedido = Pedido::create([
            'descripcion' => $request->descripcion,
            'denominacion' => $request->denominacion,
            'num_vacantes' => $request->num_vacantes,
            'area' => $request->area,
            'estudios_formales' => $request->estudios_formales,
            'licencia_arma' => $request->licencia_arma,
          //  'estudios_postgrado' => $request->estudios_postgrado, 
          //  'grado' => $request->grado,
            'otro_conocimiento' => $request->otro_conocimiento,
            'experiencia_laboral' => $request->experiencia_laboral,
            'experiencia_nivel' => $request->experiencia_nivel,
            'zona' => $request->zona,
            'sexo' => $request->sexo,
            'edad' => $request->edad,
            'edad_total' => $request->edad_total,
            'contacto' => $request->contacto,
            'tareas' => $request->tareas,
            'personal_cargo' => $request->personal_cargo,
            'cantidad' => (isset($request->cantidad) ? $request->cantidad : "0"),
            'modalidades' => $request->modalidades,
           // 'horario_trabajo' => $request->horario_trabajo,
            'turno_trabajo' => $request->turno_trabajo,
            'horario' => (isset($request->horario) ? $request->horario : "0"),
            'remuneracion' => $request->remuneracion,
            'moneda' => $request->moneda,
            'ubigeo' => $request->departamento.$request->provincia.$request->distrito,
            'direccion_trabajo' => $request->direccion_trabajo,
            'horas_extras' => (isset($request->horas_extras) ? $request->horas_extras : "0"),
            'movilidad' => (isset($request->movilidad) ? $request->movilidad : "0"),
            'refrigerio' => (isset($request->refrigerio) ? $request->refrigerio : "0"),
            'bonificacion' => (isset($request->bonificacion) ? $request->bonificacion : "0"),
            'comisiones' => (isset($request->comisiones) ? $request->comisiones : "0"),
            'discapacidad' => (isset($request->discapacidad) ? $request->discapacidad : "0"),
            'radio_discapacidad' => (isset($request->radio_discapacidad) ? $request->radio_discapacidad : "0"),
            'obersavasiones' => (isset($request->obersavasiones) ? $request->obersavasiones : ""),
            'licencia' => (isset($request->licencia) ? $request->licencia : "0"),
            'categoria' => (isset($request->categoria) ? $request->categoria : "0"),
            'cuenta_carro' => (isset($request->cuenta_carro) ? $request->cuenta_carro : "0"),
            'fecha_limite' => (isset($request->fecha_limite) ? $request->fecha_limite : "0"),
            'tipo_carro' => (isset($request->tipo_carro) ? $request->tipo_carro : "0"),
            'empresa_id' => $empresa_cod,
            'contacto_id' => $request->contacto,
            'flag_estado' => "2",
            'fich_estado' => (isset($request->estado) ? $request->estado : "0"),
            'tipo_entidad' => '2',
            'cod_entidad' => Auth::user()->entidad_id,
        ]);
            
            if( isset($request->compu1) && $request->compu1 != ""){
              $compu_sql = PedidoComputacion::create([
                'compu_nom' => $request->compu1,
                'compu_nivel' => $request->nivel_compu1,
                'pedido_id' => $pedido->id,
              ]);
            }             
            if(isset($request->compu2) && $request->compu2 != ""){
             $compu_sql = PedidoComputacion::create([
                  'compu_nom' => $request->compu2,
                  'compu_nivel' => $request->nivel_compu2,
                  'pedido_id' => $pedido->id,
              ]);
           }
           if(isset($request->compu2) && $request->compu2 != ""){
            $compu_sql = PedidoComputacion::create([
                'compu_nom' => $request->compu3,
                'compu_nivel' => $request->nivel_compu3,
                'pedido_id' => $pedido->id,
            ]);
            }
            if(isset($request->idioma1) && $request->idioma1 != ""){
            $idioma = PedidoIdioma::create([
                'idioma_nom' => $request->idioma1,
                'idioma_nivel' => $request->nivel_idioma1,
                'pedido_id' => $pedido->id,
            ]);
          }
          if(isset($request->idioma2) && $request->idioma2 != ""){
             $idioma = PedidoIdioma::create([
                'idioma_nom' => $request->idioma2,
                'idioma_nivel' => $request->nivel_idioma2,
                'pedido_id' => $pedido->id,
            ]);
           }
           if(isset($request->idioma3) && $request->idioma3 != ""){
              $idioma = PedidoIdioma::create([
                'idioma_nom' => $request->idioma3,
                'idioma_nivel' => $request->nivel_idioma3,
                'pedido_id' => $pedido->id,
            ]);
             }   

             return redirect()->action('EntidadController@index');

    }

    public function detallePedido($empresa, $pedido){

        $data['pedido'] = Pedido::where('pedidos.id','=',$pedido)->get();

        $data['lista_postulante'] = PedidoPostulante::select('personas.name as nombre', 'personas.numero_documento',
        'personas.ape_pat', 'personas.ape_mat', 'personas.email', 'fichapersona.tipo_entidad')
        ->join('personas','personas.id','=','postulantepedido.idPostulante')
        ->join('fichapersona', 'fichapersona.persona_id', '=', 'personas.id')
        //->join('pedidos','pedidos.id','postulantepedido.idPedido')
        ->where('postulantepedido.idPedido',$pedido)
        ->paginate();

        $data['pedido_codigo'] = $pedido;
        $data['empresa_codigo'] =$empresa;
        
        $data['menu'] = '2';
        $data['entidad'] = Auth::user()->entidad_id;

        return view('entidad.lista_intermediados',  $data);

    }

    public function buscarPostulantes($pedido,$empresa){

      $entidades = Entidad::where('id','=',Auth::user()->entidad_id)
      ->where('enti_flag','=','1')
      ->get();
      $data['menu'] = "2";
      
      foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['btn_buscar'] = $value->btn_buscar;
        $data['login_logo_persona'] = $value->login_logo_persona;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['submenu_color'] = $value->submenu_color;
        $data['icon1'] = $value->icon1;
        $data['icon2'] = $value->icon2;
        $data['icon3'] = $value->icon3;
        $data['icon4'] = $value->icon4;
        $data['icon5'] = $value->icon5;
        $data['btn_apto'] = $value->btn_apto;
        $data['btn_citado'] = $value->btn_citado;
        $data['btn_contratado'] = $value->btn_contratado;
      }
          // lista de maestros para el buscador
      $data['lista_pais']=UbigeoLista::getPaisSelectOption();
      $data['lista_depa']=UbigeoLista::getDepartSelectOption();
      $data['lista_prov']=UbigeoLista::getProviSelectOption('15');
    $data['lista_dist']=UbigeoLista::getDistritoSelectOption('15','01');
    $data['computacion'] = Maestro::where('MAESP_CODIGO1','=','3')->get(); 
    $data['idiomas'] = Maestro::where('MAESP_CODIGO1','=','6')->get();
    $data['ocupaciones'] = Ocupacion::where('status', '=', '9')->get();
    $data['profesiones'] = Profesion::where('ggrupo', '!=', 'x')->get();
    $data['grado_intruccion'] = GradoInstruccion::orWhere('grad_flag','1')->orWhere('grad_flag','2')->orWhere('grad_flag','3')->get();

  #/* buesqueda por eloquent */ 

  $list_query = Persona::select('fichapersona.filecv','personas.pers_edad','fichapersona.foto_post', 'personas.id as id_persona','fichapersona.id as ficha_id','personas.name','personas.email','personas.telefono','personas.sexo','personas.fecha_nacimiento');
  $list_query=$list_query->Join('fichapersona', 'fichapersona.persona_id', '=', 'personas.id');
  $list_query=$list_query->where('fichapersona.fic_estado' , '=', '1')
  ->orderByRaw( "FIELD(personas.cod_entidad, ".(isset(Auth::user()->entidad_id) ? Auth::user()->entidad_id : 1).") DESC" )
 // ->orderBy('personas.created_at', 'desc')
  ->paginate(15);

$tbl = '';
if (count($list_query)) {
  foreach ($list_query as $key => $value) {       
    $tbl .= '<div class="card" >';
      $tbl .= '<div class="card-body">';   
        $tbl .= '<div class="row">';
  
          $tbl .= '<div class="col-1" style="max-width: 0;">';
            $tbl .= '<div class="custom-control custom-checkbox">';
              $tbl .= '<input type="checkbox" class="custom-control-input" id="todo'.$value->id_persona.'" name="todo'.$value->id_persona.'" value="1" onclick="return guardarPostulante('.$value->ficha_id.','.$value->id_persona.',this)">';
              $tbl .= '<label class="custom-control-label subtitulo-shearh" for="todo'.$value->id_persona.'"></label>';
            $tbl .= '</div>';
          $tbl .= '</div>';
  
    $tbl .='<div class="col-2">';
    $tbl .='<img src="'.asset("/public/storage/".$value->foto_post).'" alt="..."  style="width:100px; height:100px;">';
    $tbl .='</div>';
  
    $tbl .='<div class="col-2">';
    $tbl .='<span style="color:#003dc7; font-size:14px;">'.$value->name.'</span><br>';
    $tbl .='<p style="font-size:13px;line-height: 140%;">'.$value->ubi_descripcion.'<br>'.($value->sexo == "2" ? "Femenino" : "Masculino")."&nbsp;&nbsp;".$value->pers_edad.' A単os<br>'.$value->email.'<br>'.$value->telefono.'<br>';
    $tbl .='</p>';
    $tbl .='</div>';
  
    $tbl .='<div class="col-3" style="flex: 0 0 25%; max-width: 25%">';
    
    $list_experiencia= Experiencia::select('ocupaciones.nombre as nombre_ocupacion','pers_experiencia.nom_empresa','fech_fin','fech_inicio','pers_experiencia.id')
    ->leftJoin('ocupaciones', 'ocupaciones.ocup_codigo','pers_experiencia.id_ocupacion')
    ->where('pers_experiencia.fichper_id','=',$value->ficha_id)
    ->where('pers_experiencia.pers_id','=',$value->id_persona)
    ->get();

    if(count($list_experiencia)){
      
        $tbl .= '<div id="carousel_experiencia" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';

      $tbl .= '<div class="carousel-inner" style="height: 70%;">';
        foreach($list_experiencia as $i => $value_exp){
          $total = $i +1;
          $active =  ($i == '0' ? "active" : "");
         $tbl .=' <div class="carousel-item '.$active.'">
          <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_ocupacion.'</span><br>
            <p style="font-size:13px;line-height: 140%;">'.$value_exp->nom_empresa.'<br>
            '.$value_exp->fech_inicio.'-/-'.$value_exp->fech_fin.'<br></p>

            
          </div>';
        }

      $tbl .='</div>
      <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-end">
        <li class="page-item" >
          <a class="page-link" href="#carousel_experiencia" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item" >
          <a class="page-link" href="#carousel_experiencia" role="button" data-slide="next" style="padding: .1rem .75rem;">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>';
       $tbl .= ' </div>';
    }
    
    
    $tbl .='</div>';
  
    $tbl .='<div class="col-3" style="flex: 0 0 30%; max-width: 30%">';

    $list_eucacion= FormacionSuperior::select('grad_descripcion as nombre_grado','profesionales.nombre as nombre_prefecion','per_superior.otro_centro','fecha_fin','fecha_inicio')
    ->leftJoin('profesionales', 'profesionales.profp_codigo','per_superior.especialidad_id')
    ->leftJoin('gradoinstruccion', 'gradoinstruccion.id','per_superior.nivel_id')
    ->where('per_superior.fichper_id','=',$value->ficha_id)
    ->where('per_superior.pers_id','=',$value->id_persona)
    ->get();

    if(count($list_eucacion)){
      
        $tbl .= '<div id="carousel_education" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';
        $tbl .= ' <ol class="carousel-indicators">
        <li data-target="#carousel_education" data-slide-to="0" class="active"></li>
        <li data-target="#carousel_education" data-slide-to="1"></li>
        <li data-target="#carousel_education" data-slide-to="2"></li>
      </ol>';
        $tbl .= '<div class="carousel-inner" style="height: 70%;">';
        foreach($list_eucacion as $i => $value_exp){
          $active =  ($i == '0' ? "active" : "");
         $tbl .=' <div class="carousel-item '.$active.'" >
          <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_prefecion.'</span><br>
            <p style="font-size:13px;line-height: 140%;">'.$value_exp->otro_centro.'<br>'.$value_exp->nombre_grado.'<br>
            '.$value_exp->fech_inicio.'-/-'.$value_exp->fecha_fin.'<br></p>
          </div>';
        }

      $tbl .='</div>
      <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-end">
        <li class="page-item" >
          <a class="page-link" href="#carousel_education" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item" onclick="return cambiarmasexp('.$value_exp->id.');">
          <a class="page-link" href="#carousel_education" role="button" data-slide="next" style="padding: .1rem .75rem;">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>';
 
    $tbl .='</div>';
      
    }

    $tbl .='</div>';
  
    $tbl .='<div class="col-1" style="max-width: 1.333333%;">';
    $tbl .= '<div class="btn-group">
    <a  href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <img src="'.asset("images/icono_proceso.png").'">
    </a>
    <div class="dropdown-menu dropdown-menu-right">
      <a href="#" onclick="DescargarHojavida(this,2,'.$value->ficha_id.','.$value->id_persona.','.$pedido.',2)" class="dropdown-item" style="font-size:14px">Ver CV</a>';
      if(isset($value->filecv) && $value->filecv != "" && $value->filecv != null){
        $tbl .= '<a href="'.route("download.hojavida",["id" => $value->ficha_id]).'" class="dropdown-item" style="font-size:14px"><i class="fas fa-check-square"></i>&nbsp;&nbsp;Descargar CV Adjunto</a>';
      //  $tbl .="<td style='font-size:13px;'><a  href='".route("download.hojavida",["id" => $rst->ficha_id])."' class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
      }else{
        $tbl .= '<a href="#" onclick="return errorCv()" class="dropdown-item" style="font-size:14px; ">Descargar CV Adjunto</a>';
      }
      
      $tbl .= '<a href="#" onclick="return agregarPostulante('.$value->ficha_id.','.$value->id_persona.')" class="dropdown-item" style="font-size:14px">Vincular Postulante al proceso</a>
      <a href="#" class="dropdown-item" style="font-size:14px">Compartir CV por Email</a>
    </div>
  </div>';
    $tbl .='</div>';
  
    $tbl .='</div>';
        
    $tbl .='</div>';
    $tbl .='</div>';
    
  }
}

$tbl .= '  <div class="row justify-content-md-center">
<div class="col-sm-6"> 
    '.$list_query->links().'
</div>
</div>';

ob_start();
echo $tbl;
$initial_content = ob_get_contents();
ob_end_clean();

    $data['tbl'] = $tbl;
    $data['pedido'] = $pedido;
    $data['contacto'] = Auth::user()->id;
    $data['empresa'] = $empresa;

     return view('seguimiento/Seguimiento_pedidoSearch',$data);
    }

    public function agregarPostAjax(){

      // lista de inputs get
      $ocupacion = Input::get('ocupacion');
      $edad_desde = Input::get('edad_desde');
      $edad_hasta = Input::get('edad_hasta');
      $sexo = Input::get('sexo');
      $id_pais = Input::get('pais');
      $deparme = Input::get('deparme');
      $provi = Input::get('provi');
      $distri = Input::get('distri');
      $titulo_puesto = Input::get('titulo_puesto');
      $empresa = Input::get('empresa');
      $tareas = Input::get('tareas');
      $salario = Input::get('salario');
      $institucion = Input::get('institucion');
      $profesion = Input::get('profesion');
      $gradosAcademicos = Input::get('gradosAcademicos');
      $estado = Input::get('estado');
      $idioma1 = Input::get('idioma1');
      $compu = Input::get('compu');
      $nombre = Input::get('nombre');
      $apellido = Input::get('apellido');
      $documento = Input::get('documento');
      $licenciaConduc = Input::get('licenciaConduc');
      $categoria = Input::get('categoria');
      $port_armas = Input::get('port_armas');
      $arma_propia = Input::get('arma_propia');
      $ultima_act = Input::get('ultima_act');
      $discapcidad = Input::get('discapcidad');
      $cv_visible = Input::get('cv_visible');
      $pedido = Input::get('pedido');
      $porta_arma = Input::get('porta_arma');
      $arma_propia = Input::get('arma_propia');
      $discapacidad = Input::get('discapacidad');
    #/* buesqueda por eloquent */ 

    $list_query = Persona::select('fichapersona.filecv','personas.pers_edad','fichapersona.foto_post', 'personas.id as id_persona','fichapersona.id as ficha_id','personas.name','personas.email','personas.telefono','personas.sexo','personas.fecha_nacimiento');
    $list_query=$list_query->leftJoin('fichapersona', 'fichapersona.persona_id', '=', 'personas.id');


    # ocupacion 
    if ((isset($ocupacion) && $ocupacion !='') || (isset($tareas) && $tareas != "") 
    || (isset($titulo_puesto) && $titulo_puesto != "") || (isset($empresa) && $empresa != "")){

      
      $list_query=$list_query->leftJoin('pers_experiencia', function($join) use ($titulo_puesto){

        $join->on('pers_experiencia.fichper_id', '=', 'fichapersona.id');

       /* if(isset($titulo_puesto) && $titulo_puesto !=''){  # falta
          $join->on('pers_experiencia.id_ocupacion' , '=', DB::raw("'".$titulo_puesto."'"));
       }
*/
      });

      if(isset($ocupacion) && $ocupacion !=''){
        $list_query=$list_query->OrWhere('pers_experiencia.id_ocupacion' , '=', $ocupacion)->OrWhere('fichapersona.ocu_trabajo', '=', $ocupacion);
      
        
      }  
    
      if(isset($tareas) && $tareas != "" ){
        $list_query=$list_query->where('pers_experiencia.funciones' , 'like', '%'.$tareas.'%');
      }

      if(isset($titulo_puesto) && $titulo_puesto !=''){  # falta
        $list_query=$list_query->OrWhere('pers_experiencia.id_ocupacion' , '=', $titulo_puesto)->OrWhere('fichapersona.ocu_trabajo', '=', $titulo_puesto);
      }

      if(isset($empresa) && $empresa != ""){
        $list_query=$list_query->where('pers_experiencia.nom_empresa' , 'like', '%'.$empresa.'%');
      }
      


    }


  //EDAD MINIMA
 if (isset($edad_desde) && $edad_desde!='') {
    $list_query=$list_query->where('personas.pers_edad' , '>=', $edad_desde);
  }
  //EDAD MAXIMA
  if (isset($edad_hasta)  && $edad_hasta!='') {
      $list_query=$list_query->where('personas.pers_edad' , '=<', $edad_hasta);
  }

   //FILTRO DE SEXO
  
   if (isset($sexo) && $sexo !='') {
     $list_query=$list_query->where('personas.sexo' , '=', $sexo);
   }

  
  //FILTRO DE UBIGEO
  if (isset($pais) && $pais!='0') {
    $list_query=$list_query->where('fichapersona.id_pais' , '=', $id_pais);
  }
  //buscamos en el ubigeo los departamentos
  if (isset($deparme) && $deparme!='0') {
    $list_query=$list_query->where('personas.ubigeo' , 'like', $deparme.'%');
  }
  //buscamos los ubigeos provincia y deparmet.
  if (isset($provi) && $provi!='0') {
    $list_query=$list_query->where('personas.ubigeo' , 'like', $deparme.$provi.'%');
  }
  //ubigeo completo
  if (isset($distri) && $distri!='0') {
    $list_query=$list_query->where('personas.ubigeo' , '=', $deparme.$provi.$distri);
  }
   
  // FILTRO DE SALARIO 

  if(isset($salario) && $salario != ""){
    $list_query=$list_query->where('fichapersona.fic_salario' , '=', $salario);
  }


  # filtro por educacion 
  if ((isset($gradosAcademicos) && $gradosAcademicos != "") || (isset($profesion) && $profesion != "") || (isset($institucion) && $institucion != "")) {

    $list_query=$list_query->leftJoin('per_superior', 'per_superior.fichper_id', '=', 'fichapersona.id');
    if(isset($gradosAcademicos) && $gradosAcademicos != "" && $gradosAcademicos != "1"){
      $list_query=$list_query->where('per_superior.nivel_id' , '=', $gradosAcademicos);
      if(isset($estado) && $estado != ""){
        $list_query=$list_query->where('per_superior.persu_estado' , '=', $estado);
        
      }

    }

    if(isset($profesion) && $profesion!=''){
      $list_query=$list_query->where('per_superior.especialidad_id' , '=', $profesion);
    }
    
    if(isset($institucion) && $institucion != ""){
      $list_query=$list_query->where('per_superior.otro_centro' , 'like', '%'.$institucion.'%');
    }


}

# filtro idioma 
if (isset($idioma1) && $idioma1 !='') {
  $list_query=$list_query->where('pers_idioma.idioma_nom' , '=', $idioma1);
  $list_query=$list_query->leftJoin('pers_idioma', 'pers_idioma.fichper_id', '=', 'fichapersona.id');
}

#filtro ocupacion
if (isset($compu) && $compu!='') {
  $list_query=$list_query->where('pers_compu.compu_nom' , '=', $compu);
  $list_query=$list_query->leftJoin('pers_compu', 'pers_compu.fichper_id', '=', 'fichapersona.id');
}



  //licencia de conducir
  if (isset($licenciaConduc) && $licenciaConduc!='') {
    $list_query=$list_query->where('fichapersona.licenciaConduc' , '=', $licenciaConduc);
  }
 
  if(isset($categoria) && $categoria != ""){
    $list_query=$list_query->where('fichapersona.categoria' , '=', $categoria);
  }

  if(isset($port_armas) && $port_armas != ""){
    $list_query=$list_query->where('fichapersona.porta_arma' , '=', $porta_arma);
  }

  if(isset($arma_propia) && $arma_propia != ""){
    $list_query=$list_query->where('fichapersona.arma_propia' , '=', $arma_propia);
  }



  // daots personales

  if(isset($nombre) && $nombre != ""){
    $list_query=$list_query->where('personas.name' , 'like', '%'.$nombre.'%');
  }

  if(isset($apellido) && $apellido != ""){
   // $where.=' AND (p.ape_pat like "%'.$apellido.'%" or p.ape_mat like "%'.$apellido.'%")';
    $list_query=$list_query->where('personas.name' , 'like', '%'.$apellido.'%');
  }

  if(isset($documento) && $documento != ""){
    $list_query=$list_query->where('personas.numero_documento' , 'like', '%'.$documento.'%');
  }
  
  //SI ESTAS BUSCANDO PERSONAS CON DISCAPA
  if (isset($discapcidad) && $discapcidad!='') {
      $list_query=$list_query->where('personas.discapacidad' , '=', $discapacidad);
  }
  
  #filtro por cv visible 
  if (isset($cv_visible) && $cv_visible!='') {
      $list_query=$list_query->where('fichapersona.foto_post' , '!=', 'null');
  }

  if(isset($ultima_act) && $ultima_act != ""){
    $now = new \DateTime();
    
   // $where .= ' and TIMESTAMPDIFF(MONTH, FP.updated_at, "'.$now->format('d-m-Y H:i:s').'") = "'.$ultima_act.'" ';
  }

  $list_query=$list_query->where('fichapersona.fic_estado' , '=', '1')
  ->orderByRaw( "FIELD(personas.cod_entidad, ".(isset(Auth::user()->entidad_id) ? Auth::user()->entidad_id : 1).") DESC" )
  ->distinct()
  ->paginate(15);

  $tbl = '';
  if (count($list_query)) {
    foreach ($list_query as $key => $value) {       
      $tbl .= '<div class="card" >';
        $tbl .= '<div class="card-body">';   
          $tbl .= '<div class="row">';
    
            $tbl .= '<div class="col-1" style="max-width: 0;">';
              $tbl .= '<div class="custom-control custom-checkbox">';
                $tbl .= '<input type="checkbox" class="custom-control-input" id="todo'.$value->id_persona.'" name="todo'.$value->id_persona.'" value="1" onclick="return guardarPostulante('.$value->ficha_id.','.$value->id_persona.',this)">';
                $tbl .= '<label class="custom-control-label subtitulo-shearh" for="todo'.$value->id_persona.'"></label>';
              $tbl .= '</div>';
            $tbl .= '</div>';
    
      $tbl .='<div class="col-2">';
      $tbl .='<img src="'.asset("/public/storage/".$value->foto_post).'" alt="..."  style="width:100px; height:100px;">';
      $tbl .='</div>';
    
      $tbl .='<div class="col-2">';
      $tbl .='<span style="color:#003dc7; font-size:14px;">'.$value->name.'</span><br>';
      $tbl .='<p style="font-size:13px;line-height: 140%;">'.$value->ubi_descripcion.'<br>'.($value->sexo == "2" ? "Femenino" : "Masculino")."&nbsp;&nbsp;".$value->pers_edad.' A単os<br>'.$value->email.'<br>'.$value->telefono.'<br>';
      $tbl .='</p>';
      $tbl .='</div>';
    
      $tbl .='<div class="col-3" style="flex: 0 0 25%; max-width: 25%">';
      
      $list_experiencia= Experiencia::select('ocupaciones.nombre as nombre_ocupacion','pers_experiencia.nom_empresa','fech_fin','fech_inicio','pers_experiencia.id')
      ->leftJoin('ocupaciones', 'ocupaciones.ocup_codigo','pers_experiencia.id_ocupacion')
      ->where('pers_experiencia.fichper_id','=',$value->ficha_id)
      ->where('pers_experiencia.pers_id','=',$value->id_persona)
      ->get();

      if(count($list_experiencia)){
        
          $tbl .= '<div id="carousel_experiencia'.$value->ficha_id.'" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';
        /*  $tbl .= ' <ol class="carousel-indicators">
          <li data-target="#carousel_experiencia" data-slide-to="0" class="active"></li>
          <li data-target="#carousel_experiencia" data-slide-to="1"></li>
          <li data-target="#carousel_experiencia" data-slide-to="2"></li>
        </ol>';*/
        $tbl .= '<div class="carousel-inner" style="height: 70%;">';
          foreach($list_experiencia as $i => $value_exp){
            $total = $i +1;
            $active =  ($i == '0' ? "active" : "");
           $tbl .=' <div class="carousel-item '.$active.'">
            <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_ocupacion.'</span><br>
              <p style="font-size:13px;line-height: 140%;">'.$value_exp->nom_empresa.'<br>
              '.$value_exp->fech_inicio.'-/-'.$value_exp->fech_fin.'<br></p>

              
            </div>';
          }

        $tbl .='</div>
        <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          <li class="page-item" >
            <a class="page-link" href="#carousel_experiencia'.$value->ficha_id.'" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item" >
            <a class="page-link" href="#carousel_experiencia'.$value->ficha_id.'" role="button" data-slide="next" style="padding: .1rem .75rem;">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>';

        /* $tbl .= '<a class="carousel-control-prev" href="#carousel_experiencia" role="button" data-slide="prev" >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel_experiencia" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>' ;*/
         $tbl .= ' </div>';
        
      }
      
      
      $tbl .='</div>';
    
      $tbl .='<div class="col-3" style="flex: 0 0 30%; max-width: 30%">';

      $list_eucacion= FormacionSuperior::select('grad_descripcion as nombre_grado','profesionales.nombre as nombre_prefecion','per_superior.otro_centro','fecha_fin','fecha_inicio')
      ->leftJoin('profesionales', 'profesionales.profp_codigo','per_superior.especialidad_id')
      ->leftJoin('gradoinstruccion', 'gradoinstruccion.id','per_superior.nivel_id')
      ->where('per_superior.fichper_id','=',$value->ficha_id)
      ->where('per_superior.pers_id','=',$value->id_persona)
      ->get();

      if(count($list_eucacion)){
        
          $tbl .= '<div id="carousel_education'.$value->ficha_id.'" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';
         /* $tbl .= ' <ol class="carousel-indicators">
          <li data-target="#carousel_education'.$value->ficha_id.'" data-slide-to="0" class="active"></li>
          <li data-target="#carousel_education'.$value->ficha_id.'" data-slide-to="1"></li>
          <li data-target="#carousel_education'.$value->ficha_id.'" data-slide-to="2"></li>
        </ol>';*/
          $tbl .= '<div class="carousel-inner" style="height: 70%;">';
          foreach($list_eucacion as $i => $value_exp){
            $active =  ($i == '0' ? "active" : "");
           $tbl .=' <div class="carousel-item '.$active.'" >
            <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_prefecion.'</span><br>
              <p style="font-size:13px;line-height: 140%;">'.$value_exp->otro_centro.'<br>'.$value_exp->nombre_grado.'<br>
              '.$value_exp->fech_inicio.'-/-'.$value_exp->fecha_fin.'<br></p>
            </div>';
          }

        $tbl .='</div>
        <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          <li class="page-item" >
            <a class="page-link" href="#carousel_education'.$value->ficha_id.'" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item" onclick="return cambiarmasexp('.$value_exp->id.');">
            <a class="page-link" href="#carousel_education'.$value->ficha_id.'" role="button" data-slide="next" style="padding: .1rem .75rem;">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>';
       /*   
      $tbl .= '<a class="carousel-control-prev" href="#carousel_education" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel_education" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>';*/      
      $tbl .='</div>';
        
      }

      $tbl .='</div>';
    
      $tbl .='<div class="col-1" style="max-width: 1.333333%;">';
      $tbl .= '<div class="btn-group">
      <a  href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="'.asset("images/icono_proceso.png").'">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a href="#" onclick="DescargarHojavida(this,2,'.$value->ficha_id.','.$value->id_persona.','.$pedido.',2)" class="dropdown-item" style="font-size:14px">Ver CV</a>';
        if(isset($value->filecv) && $value->filecv != "" && $value->filecv != null){
          $tbl .= '<a href="'.route("download.hojavida",["id" => $value->ficha_id]).'" class="dropdown-item" style="font-size:14px"><i class="fas fa-check-square"></i>&nbsp;&nbsp;Descargar CV Adjunto</a>';
        //  $tbl .="<td style='font-size:13px;'><a  href='".route("download.hojavida",["id" => $rst->ficha_id])."' class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
        }else{
          $tbl .= '<a href="#" onclick="return errorCv()" class="dropdown-item" style="font-size:14px; ">Descargar CV Adjunto</a>';
        }
        
        $tbl .= '<a href="#" onclick="return agregarPostulante('.$value->ficha_id.','.$value->id_persona.')" class="dropdown-item" style="font-size:14px">Vincular Postulante al proceso</a>
        <a href="#" class="dropdown-item" style="font-size:14px">Compartir CV por Email</a>
      </div>
    </div>';
      $tbl .='</div>';
    
      $tbl .='</div>';
          
      $tbl .='</div>';
      $tbl .='</div>';
      
    }
  }

    $tbl .= '  <div class="row justify-content-md-center">
      <div class="col-sm-6"> 
          '.$list_query->links().'
      </div>
    </div>';

  ob_start();
  echo $tbl;
  $initial_content = ob_get_contents();
  ob_end_clean();

  return json_encode($tbl,JSON_PRETTY_PRINT);

  }

  public function agregarpost($ficha, $persona, $idPedido){

    $where[] = ['idPedido',$idPedido];
    $where[] = ['idFicha',$ficha];
    $where[] = ['idPostulante',$persona];

    $formacion = PedidoPostulante::where($where)->get();
    $msj="0"; 

    if(!count($formacion) > 0){
      $form = PedidoPostulante::create([
        'idPedido' => $idPedido,
        'idFicha' => $ficha,
        'idPostulante' => $persona,
        'PP_Estado' => '1',
        'PP_Prosedendia' => '1',
      ]);
      $msj="1"; 
    }else{
      $msj="0"; 
    }
          
    return json_encode($msj,JSON_PRETTY_PRINT);
    
  }

  public function deletepost($ficha, $persona, $idPedido){

    $where[] = ['idPedido',$idPedido];
    $where[] = ['idFicha',$ficha];
    $where[] = ['idPostulante',$persona];

    $deletedRows = PedidoPostulante::where($where)->delete();

    $msj="eliminado correctamente"; 

    return json_encode($msj,JSON_PRETTY_PRINT);

  }
  
  public function personaGraficos(){

    
    # FORMATO CARBON PARA LAS FECHAS 
    $date = Carbon::now();
    $anio = $date->format('Y');
    $Month = $date->format('m');

    $mes = Input::get('m');
    $annio = Input::get('a');
    if(isset($mes) && $mes != ''){
      $Month = $mes;
    }
    if(isset($annio) && $annio != ''){
      $anio = $annio;
    }

    $data['anio'] = $anio;
    $data['Month'] = $Month;
    
    $data['Month_letra'] = LibreriaFecha::parseFechaLetraMes($Month);
    

    # TOTALES DE USUARIOS EN GENERAL DEL MES 
    $data['total_usuarios'] = User::where('cod_entidad','=',Auth::user()->entidad_id)
    ->whereMonth('created_at', $Month)
    ->whereYear('created_at', $anio)->count();

    # TOTALES DE USUARIOS EN TODO EL TIEMPO

    $data['totales_usarios'] = User::where('cod_entidad','=',Auth::user()->entidad_id)
    ->count();

    # TOTAL DE INTERMEDIADIOS (FALTA)
    $data['total_intermediados'] = User::where('cod_entidad','=',Auth::user()->entidad_id)
    ->whereMonth('created_at', $Month)
    ->whereYear('created_at', $anio)->count();

    # TOTALES DE USUARIOS POR CADA DIA DEL MES
    $dias_totales = date("d",mktime(0,0,0,$Month+1,0,$anio));
    $xdias = User::select(DB::raw("count(*) as total"))
    ->where('cod_entidad','=',Auth::user()->entidad_id)
    ->whereMonth('created_at', $Month)
    ->whereYear('created_at', $anio);
    
   for ($i=1; $i <= $dias_totales; $i++) { 
    $xdias = $xdias->addSelect(DB::raw("(select count(*) from users where day(created_at) = ".$i." and month(created_at) = '".$Month."' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'diat".$i."' "));
   }

   $data['xdias'] = $xdias->get();


    $data['xmeses'] = User::select(
    DB::raw("(select count(*) from users where year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."')  as total"),
    DB::raw("(select count(*) from users where month(created_at) = '01' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'enero' "),
    DB::raw("(select count(*) from users where month(created_at) = '02' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'febrero' "),
    DB::raw("(select count(*) from users where month(created_at) = '03' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'marzo' "),
    DB::raw("(select count(*) from users where month(created_at) = '04' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'abril' "),
    DB::raw("(select count(*) from users where month(created_at) = '05' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'mayo' "),
    DB::raw("(select count(*) from users where month(created_at) = '06' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'junio' "),
    DB::raw("(select count(*) from users where month(created_at) = '07' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'julio' "),
    DB::raw("(select count(*) from users where month(created_at) = '08' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'agosto' "),
    DB::raw("(select count(*) from users where month(created_at) = '09' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'setiembre' "),
    DB::raw("(select count(*) from users where month(created_at) = '10' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'octubre' "),
    DB::raw("(select count(*) from users where month(created_at) = '11' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'noviembre' "),
    DB::raw("(select count(*) from users where month(created_at) = '12' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'diciembre' "))
    ->whereYear('created_at', $anio)
    ->get();

  /*  $data['xedad'] = User::select(
      DB::raw("(select count(*) from personas where month(created_at) = '".$Month."' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as total"),
      DB::raw("(select count(*) from personas where pers_edad >= 15 and pers_edad <= 18 and month(created_at) = '".$Month."' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'edad15_18' "),
      DB::raw("(select count(*) from personas where pers_edad >= 19 and pers_edad <= 25 and month(created_at) = '".$Month."' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'edad19_25' "),
      DB::raw("(select count(*) from personas where pers_edad >= 26 and pers_edad <= 100 and month(created_at) = '".$Month."' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'edad26_100' "))
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)
      ->where('cod_entidad', '=',Auth::user()->entidad_id)
      ->get();
*/
      $data['edad15_18'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 15)
      ->where('pers_edad', '<=', 18)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['edad19_25'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 19)
      ->where('pers_edad', '<=', 25)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['edad26_30'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 26)
      ->where('pers_edad', '<=', 30)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['edad31_45'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 31)
      ->where('pers_edad', '<=', 45)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['edad46_55'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 46)
      ->where('pers_edad', '<=', 55)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['edad56_mas'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 56)
      ->where('pers_edad', '<=', 100)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['intermediado'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->where('cod_entidad',Auth::user()->entidad_id)
      ->whereMonth('postulantepedido.PP_Fecha', $Month)
      ->whereYear('postulantepedido.PP_Fecha', $anio)
      ->count();

      $data['colocados'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->where('cod_entidad',Auth::user()->entidad_id)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();


    $data['menu'] = '2';
    $data['entidad'] = Auth::user()->entidad_id;

    return view('entidad.reporte-persona', $data);

  }

  public function empresaGraficos(){

      # FORMATO CARBON PARA LAS FECHAS 
      $date = Carbon::now();
      $anio = $date->format('Y');
      $Month = $date->format('m');

      $mes = Input::get('m');
      $annio = Input::get('a');
      if(isset($mes) && $mes != ''){
        $Month = $mes;
      }
      if(isset($annio) && $annio != ''){
        $anio = $annio;
      }

      $data['anio'] = $anio;
      $data['Month'] = $Month;

      $data['Month_letra'] = LibreriaFecha::parseFechaLetraMes($Month);

      # TOTALES DE EMPRESAS EN GENERAL DEL MES 
      $data['total_usuarios'] = Empresa::where('cod_entidad','=',Auth::user()->entidad_id)
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)->count();

      $data['xmeses'] = Empresa::select(
        DB::raw("(select count(*) from empresas where year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."')  as total"),
        DB::raw("(select count(*) from empresas where month(created_at) = '01' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'enero' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '02' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'febrero' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '03' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'marzo' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '04' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'abril' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '05' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'mayo' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '06' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'junio' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '07' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'julio' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '08' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'agosto' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '09' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'setiembre' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '10' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'octubre' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '11' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'noviembre' "),
        DB::raw("(select count(*) from empresas where month(created_at) = '12' and year(created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id."') as 'diciembre' "))
      ->whereYear('created_at', $anio)
      ->get();


      $data['intermediado'] = PedidoPostulante::join('pedidos','pedidos.id','=','postulantepedido.idPedido')
      ->join('empresas','empresas.id','pedidos.empresa_id')
      ->where('pedidos.cod_entidad',Auth::user()->entidad_id)
      ->whereMonth('postulantepedido.PP_Fecha', $Month)
      ->whereYear('postulantepedido.PP_Fecha', $anio)
      ->count();

      $data['colocados'] = PedidoPostulante::join('pedidos','pedidos.id','=','postulantepedido.idPedido')
      ->join('empresas','empresas.id','pedidos.empresa_id')
      ->where('pedidos.cod_entidad',Auth::user()->entidad_id)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();
      
      $data['totales_usarios'] = Empresa::where('cod_entidad','=',Auth::user()->entidad_id)
      ->count();

     
      $data['menu'] = '2';
    $data['entidad'] = Auth::user()->entidad_id;

    return view('entidad.reporte-empresa', $data);
  
  }


  public function filtroOcuapcion(){

    $entidad_codigo = Auth::user()->entidad_id;
    $data['profesiones'] = Profesion::where('ggrupo', '!=', 'x')->get();
    $data['entidad'] = $entidad_codigo;
    $data['lista_profesion'] = ReporteEntidad::join('profesionales','profesionales.profp_codigo','=','reporte_entidad.cod_profesion')
    ->where('cod_entidad',$entidad_codigo)
    ->get();
    
    return view('entidad.registro-parametricas',$data);
  }


  public function ocupacionSave(Request $request){

    $ocupacion = ReporteEntidad::create([
      'cod_entidad' => Auth::user()->entidad_id,
      'cod_profesion' => $request->profesion,
    ]);

    return redirect()->action('EntidadController@filtroOcuapcion');

  } 

  public function reporteGraficosOcupacion(){

    $entidad_codigo = Auth::user()->entidad_id;
    $date = Carbon::now();
    $anio = $date->format('Y');
    $Month = $date->format('m');

    $total = Input::get('totales');
    $where[] = [];
  

    $data['parametrica']= ReporteEntidad::join('profesionales','profesionales.profp_codigo','=','reporte_entidad.cod_profesion')
    ->where('ggrupo', '!=', 'x')
    ->where('cod_entidad',$entidad_codigo)
    ->get();

    $array_profe[] = ['nombre' => 'vacio', 'cantidad' => 5];

    foreach($data['parametrica'] as $i => $value){

      $cantidad = User::join('per_superior','per_superior.pers_id','=','users.persona_id')
      ->where('especialidad_id',$value->cod_profesion)
      ->where('users.cod_entidad','=',$entidad_codigo)
      //->whereMonth('users.created_at', $Month)
     // ->whereYear('users.created_at', $anio)
     ->count();

      $array_profe[$i] = array(
        'nombre' => $value->nombre,
        'cantidad' => $cantidad);

    }

    $data['enero'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '01')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
          
      }
    }))->count();
    $data['febrero'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '02')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['marzo'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '03')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['abril'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '04')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['mayo'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '05')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['junio'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '06')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['julio'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '07')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['agosto'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '08')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['setiembre'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '09')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['octubre'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '10')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['noviembre'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '11')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();
    $data['diciembre'] = User::leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereMonth('users.created_at', '12')
    ->whereYear('users.created_at', $anio)
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))->count();

      $data['edad15_17'] = User::join('personas','personas.id','=','users.persona_id')
      ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
      ->where((function($query) use($total,$where){
        if(isset($total)){
          for($i = 0; $i <= $total; $i++){
            $profesion = Input::get('profesiones'.$i);
            if(isset($profesion) && $profesion != null){
              $query->orWhere('per_superior.especialidad_id',$profesion);
            }
          }
        }
      }))
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 0)
      ->where('pers_edad', '<=', 17)
      ->count();

      $data['edad18_29'] = User::join('personas','personas.id','=','users.persona_id')
      ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
      ->where((function($query) use($total,$where){
        if(isset($total)){
          for($i = 0; $i <= $total; $i++){
            $profesion = Input::get('profesiones'.$i);
            if(isset($profesion) && $profesion != null){
              $query->orWhere('per_superior.especialidad_id',$profesion);
            }
          }
        }
      }))
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 18)
      ->where('pers_edad', '<=', 29)
      ->count();

      $data['edad30_45'] = User::join('personas','personas.id','=','users.persona_id')
      ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
      ->where((function($query) use($total,$where){
        if(isset($total)){
          for($i = 0; $i <= $total; $i++){
            $profesion = Input::get('profesiones'.$i);
            if(isset($profesion) && $profesion != null){
              $query->orWhere('per_superior.especialidad_id',$profesion);
            }
          }
        }
      }))
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 30)
      ->where('pers_edad', '<=', 45)
      ->count();

      $data['edad46_100'] = User::join('personas','personas.id','=','users.persona_id')
      ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
      ->where((function($query) use($total,$where){
        if(isset($total)){
          for($i = 0; $i <= $total; $i++){
            $profesion = Input::get('profesiones'.$i);
            if(isset($profesion) && $profesion != null){
              $query->orWhere('per_superior.especialidad_id',$profesion);
            }
          }
        }
      }))
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 46)
      ->where('pers_edad', '<=', 100)
      ->count();

       # TOTALES DE USUARIOS POR CADA DIA DEL MES
    $dias_totales = date("d",mktime(0,0,0,$Month+1,0,$anio));
    $xdias = User::select(DB::raw("count(*) as total"))
    ->where('cod_entidad','=',Auth::user()->entidad_id)
    ->whereMonth('created_at', $Month)
    ->whereYear('created_at', $anio);
    
    $where_otro = "";

    for ($i=1; $i <= $dias_totales; $i++) { 
      for($e = 0; $e <= $total; $e++){
        $profesion = Input::get('profesiones'.$e);
        if(isset($profesion) && $profesion != null){
          $where_otro .= " or (per_superior.especialidad_id =". $profesion.")";
          
        }
      }
      
      $xdias = $xdias->addSelect(DB::raw("(select count(*) from users left join per_superior on per_superior.pers_id = users.persona_id where day(users.created_at) = ".$i." and month(users.created_at) = '".$Month."' and year(users.created_at) = '".$anio."' and cod_entidad = '".Auth::user()->entidad_id. $where_otro." ') as 'diat".$i."' "));
    }

   $data['xdias'] = $xdias->get();

   # POR EDAD 

   $data['xsexo_mas'] = User::join('personas','personas.id','=','users.persona_id')
   ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
   ->where('sexo','1')
   ->where((function($query) use($total,$where){
    if(isset($total)){
      for($i = 0; $i <= $total; $i++){
        $profesion = Input::get('profesiones'.$i);
        if(isset($profesion) && $profesion != null){
          $query->orWhere('per_superior.especialidad_id',$profesion);
        }
      }
    }
  }))
   ->count();

   $data['xsexo_x'] = User::join('personas','personas.id','=','users.persona_id')
   ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
   ->whereNull('sexo')
   ->where((function($query) use($total,$where){
    if(isset($total)){
      for($i = 0; $i <= $total; $i++){
        $profesion = Input::get('profesiones'.$i);
        if(isset($profesion) && $profesion != null){
          $query->orWhere('per_superior.especialidad_id',$profesion);
        }
      }
    }
  }))
   ->count();

   $data['xsexo_feme'] = User::join('personas','personas.id','=','users.persona_id')
   ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
   ->where('sexo','2')
   ->where((function($query) use($total,$where){
    if(isset($total)){
      for($i = 0; $i <= $total; $i++){
        $profesion = Input::get('profesiones'.$i);
        if(isset($profesion) && $profesion != null){
          $query->orWhere('per_superior.especialidad_id',$profesion);
        }
      }
    }
  }))
   ->count();

   $parametrica_distrito = Ubigeo::where('ubi_coddpto','15')
   ->where('ubi_codigo','!=','150000')
   ->where('ubi_codigo','!=','150100')
   ->where('ubi_codprov','01')->get();

 
  // $array_distri[] = [];
   foreach($parametrica_distrito as $i => $ubigeo){

    $catiddad_distri = User::join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->where('ubigeo',$ubigeo->ubi_codigo)
    ->where('users.cod_entidad',$entidad_codigo)
    ->count();

    $array_distri[$i] = array(
      'nombre_distrito' => $ubigeo->ubi_descripcion,
      'cantidad' => $catiddad_distri);

   }

    # TOTALES DE USUARIOS EN GENERAL DEL MES 
    $data['registrados'] = User::where('cod_entidad','=',$entidad_codigo)
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->count();

    # TOTALES DE USUARIOS EN GENERAL DEL MES  masculino
    $data['registrados_m'] = User::join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->where('sexo','1')
    ->count();

    # TOTALES DE USUARIOS EN GENERAL DEL MES femenino
    $data['registrados_f'] = User::join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->where('sexo','2')
    ->count();
    
    # TOTALES DE USUARIOS EN GENERAL DEL MES sin sexo
    $data['registrados_x'] = User::join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->whereNull('sexo')
    ->count();

    # TOTALES DE INTERMEDIADOS 
    $data['intermediado'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->count();
   
     # TOTALES DE INTERMEDIADOS masculino
     $data['intermediado_m'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
     ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
     ->join('personas','personas.id','=','users.persona_id')
     ->where('sexo','1')
     ->where('users.cod_entidad','=',$entidad_codigo)
     ->count();

      # TOTALES DE INTERMEDIADOS femenino
    $data['intermediado_f'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->join('personas','personas.id','=','users.persona_id')
     ->where('sexo','2')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->count();

    # TOTALES DE INTERMEDIADOS femenino
    $data['intermediado_x'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
    ->join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
     ->whereNull('sexo')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->count();

   # TOTALES DE COLOCADOS
   $data['colocado'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
   ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->where('postulantepedido.PostPed_contra','1')
    ->count();
   
     # TOTALES DE COLOCADOS masculino
   $data['colocado_m'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
   ->join('personas','personas.id','=','users.persona_id')
   ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
     ->where('sexo','1')
   ->where('users.cod_entidad','=',$entidad_codigo)
   ->where('postulantepedido.PostPed_contra','1')
   ->count();

    # TOTALES DE COLOCADOS femenino
    $data['colocado_f'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
    ->join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
     ->where('sexo','2')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->where('postulantepedido.PostPed_contra','1')
    ->count();

    # TOTALES DE COLOCADOS nigungo
    $data['colocado_x'] = PedidoPostulante::join('users','users.persona_id','=','postulantepedido.idPostulante')
    ->join('personas','personas.id','=','users.persona_id')
    ->leftJoin('per_superior','per_superior.pers_id','=','users.persona_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('per_superior.especialidad_id',$profesion);
          }
        }
      }
    }))
    ->whereNull('sexo')
    ->where('users.cod_entidad','=',$entidad_codigo)
    ->where('postulantepedido.PostPed_contra','1')
    ->count();


    # TOTALES VACANTES

    $data['vacantes'] = Pedido::join('empresas','empresas.id','=','pedidos.empresa_id')
    ->where((function($query) use($total,$where){
      if(isset($total)){
        for($i = 0; $i <= $total; $i++){
          $profesion = Input::get('profesiones'.$i);
          if(isset($profesion) && $profesion != null){
            $query->orWhere('pedidos.especialidad',$profesion);
          }
        }
      }
    }))
    ->where('empresas.tipo_entidad',$entidad_codigo)
    ->count();

    # TOTALES DE EMPRESAS
    $checkbox = [];
    if(isset($total)){
      for($i = 0; $i <= $total; $i++){
        $profesion = Input::get('profesiones'.$i);
        $checkbox[$i] = array(
          'codigo' => $profesion,
        );
      }
    }
    $data['checkbox'] = $checkbox;
    $data['empresas'] = Empresa::where('empresas.tipo_entidad',$entidad_codigo)
    ->count();

//dd($array_distri);
   // dd($array_profe);
    $data['array_distri'] = $array_distri;
    $data['array_profe'] = $array_profe;
    $data['entidad'] = $entidad_codigo;

    return view('entidad.reporte-ocupacion',$data);
  }

  public function vacante(){

    $entidades = Entidad::where('id','=',Auth::user()->entidad_id)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
      $data['enti_logo'] = $value->enti_logo;
      $data['entidad'] = $value->id;
      $data['entidad_color'] = $value->color_entidad;
      $data['btn_buscar'] = $value->btn_buscar;
      $data['login_logo_persona'] = $value->login_logo_persona;
      $data['subcolor_entidad'] = $value->subcolor_entidad;
      $data['enti_nombre'] = $value->enti_nombre;
    }

      $ruc = Input::get('ruc');
      $whereTotales = ['pedidos.flag_estado' => '2'];
      if(isset($ruc)){
        $whereTotales = ['empresas.numero_documento' => $ruc];
      }

      $pedidos = Pedido::select(DB::raw('(SELECT count(pedidos.id) from pedidos where pedidos.empresa_id = empresas.id) as total_pedidos'),'pedidos.id as pedido_codigo','pedidos.descripcion as descripcion_pedido','empresas.numero_documento as ruc', 'empresas.razon_social', 'contactos.name', 'contactos.telefono', 'contactos.email')
      ->join('empresas', 'empresas.id', '=', 'pedidos.empresa_id')
      ->join('contactos', 'contactos.empresa_id', '=', 'empresas.id')
     // ->join('personas', 'contactos.persona_id', '=', 'personas.id')
      ->where($whereTotales)
      ->orderBy('pedidos.created_at', 'DESC')
      ->paginate(5);

      $data['ruc_busqueda'] = $ruc;
      $data['pedidos'] = $pedidos;
      return view('entidad.vacante', $data);
  }

  public function publiarVacante($id){

    $idioma = Pedido::where('id', $id)
      ->update([
          'flag_estado' => "1",
      ]);
    return redirect()->action('EntidadController@vacante');

  }

  public function ReporteEUno(){
    # FORMATO CARBON PARA LAS FECHAS 
    $date = Carbon::now();
    $anio = $date->format('Y');
    $Month = $date->format('m');

    $mes = Input::get('m');
    $annio = Input::get('a');
    if(isset($mes) && $mes != ''){
      $Month = $mes;
    }
    if(isset($annio) && $annio != ''){
      $anio = $annio;
    }

    $data['anio'] = $anio;
    $data['Month'] = $Month;
    
    $data['Month_letra'] = LibreriaFecha::parseFechaLetraMes($Month);

    $entidades = Entidad::where('id','=',Auth::user()->entidad_id)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
      $data['enti_logo'] = $value->enti_logo;
      $data['entidad'] = $value->id;
      $data['entidad_color'] = $value->color_entidad;
      $data['btn_buscar'] = $value->btn_buscar;
      $data['login_logo_persona'] = $value->login_logo_persona;
      $data['subcolor_entidad'] = $value->subcolor_entidad;
      $data['enti_nombre'] = $value->enti_nombre;
    }

    $data['dir_v'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','1%')
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['dir_m'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','1%')
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['pro_v'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','2%')
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['pro_m'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','2%')
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['tec_v'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','3%')
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['tec_m'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','3%')
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['ope_v'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','4%')
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['ope_m'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','4%')
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();


      $data['no_v'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','9%')
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

      $data['no_m'] = User::join('personas','personas.id','=','users.persona_id')
      ->join('fichapersona', 'personas.id','=','fichapersona.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('fichapersona.ocu_trabajo', 'like','9%')
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();
      
      $data['dir'] = Pedido::where('denominacion', 'like','1%')
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)->count();

      $data['pro'] = Pedido::where('denominacion', 'like','2%')
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)->count();

      $data['tec'] = Pedido::where('denominacion', 'like','3%')
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)->count();

      $data['ope'] = Pedido::where('denominacion', 'like','4%')
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)->count();

      $data['no'] = Pedido::where('denominacion', 'like','9%')
      ->whereMonth('created_at', $Month)
      ->whereYear('created_at', $anio)->count();

      $data['co_dir_va'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','1%')
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_dir_mu'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','1%')
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_pro_va'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','2%')
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_pro_mu'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','2%')
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_tec_va'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','3%')
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_tec_mu'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','3%')
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_ope_va'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','4%')
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_ope_mu'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','4%')
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_no_va'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','9%')
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['co_no_mu'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->where('denominacion', 'like','9%')
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['pedidos'] = PedidoPostulante::join('pedidos','pedidos.id','postulantepedido.idPedido')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();
      
      $data['totales_usarios'] = Empresa::where('cod_entidad','=',Auth::user()->entidad_id)
      ->count();


      return view('entidad.reporte-euno', $data);
  }

  public function ReporteETres(){
    $entidades = Entidad::where('id','=',Auth::user()->entidad_id)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
      $data['enti_logo'] = $value->enti_logo;
      $data['entidad'] = $value->id;
      $data['entidad_color'] = $value->color_entidad;
      $data['btn_buscar'] = $value->btn_buscar;
      $data['login_logo_persona'] = $value->login_logo_persona;
      $data['subcolor_entidad'] = $value->subcolor_entidad;
      $data['enti_nombre'] = $value->enti_nombre;
    }

    # FORMATO CARBON PARA LAS FECHAS 
    $date = Carbon::now();
    $anio = $date->format('Y');
    $Month = $date->format('m');

    $mes = Input::get('m');
    $annio = Input::get('a');
    if(isset($mes) && $mes != ''){
      $Month = $mes;
    }
    if(isset($annio) && $annio != ''){
      $anio = $annio;
    }

    $data['anio'] = $anio;
    $data['Month'] = $Month;
    
    $data['Month_letra'] = LibreriaFecha::parseFechaLetraMes($Month);

    $data['edad_m18_24'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 18)
      ->where('pers_edad', '<=', 24)
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_m25_29'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 35)
      ->where('pers_edad', '<=', 29)
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_m30_45'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 35)
      ->where('pers_edad', '<=', 29)
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_m46_59'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 46)
      ->where('pers_edad', '<=', 59)
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_m60_mas'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 60)
      ->where('pers_edad', '<=', 100)
      ->where('sexo', '=', 1)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();
      
      $data['edad_f18_24'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 18)
      ->where('pers_edad', '<=', 24)
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_f25_29'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 35)
      ->where('pers_edad', '<=', 29)
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_f30_45'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 35)
      ->where('pers_edad', '<=', 29)
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_f46_59'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 46)
      ->where('pers_edad', '<=', 59)
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['edad_f60_mas'] = User::join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 60)
      ->where('pers_edad', '<=', 100)
      ->where('sexo', '=', 2)
      ->whereMonth('users.created_at', $Month)
      ->whereYear('users.created_at', $anio)->count();

    $data['colo_m18_24'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 18)
      ->where('pers_edad', '<=', 24)
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();
    
      $data['colo_m25_29'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 25)
      ->where('pers_edad', '<=', 29)
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['colo_m30_45'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 30)
      ->where('pers_edad', '<=', 45)
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['colo_m46_59'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 46)
      ->where('pers_edad', '<=', 59)
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['colo_m60_mas'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 60)
      ->where('pers_edad', '<=', 100)
      ->where('sexo', '=', 1)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();


      $data['colo_f18_24'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 18)
      ->where('pers_edad', '<=', 24)
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();
    
      $data['colo_f25_29'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 25)
      ->where('pers_edad', '<=', 29)
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['colo_f30_45'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 30)
      ->where('pers_edad', '<=', 45)
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['colo_f46_59'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 46)
      ->where('pers_edad', '<=', 59)
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();

      $data['colo_f60_mas'] = PedidoPostulante::join('users','users.persona_id','postulantepedido.idPostulante')
      ->join('personas','personas.id','=','users.persona_id')
      ->where('users.cod_entidad', '=',Auth::user()->entidad_id)
      ->where('pers_edad', '>=', 60)
      ->where('pers_edad', '<=', 100)
      ->where('sexo', '=', 2)
      ->where('PostPed_contra','1')
      ->whereMonth('postulantepedido.PostPed_contraFecha', $Month)
      ->whereYear('postulantepedido.PostPed_contraFecha', $anio)
      ->count();
    
    return view('entidad.reporte-etres', $data);
  }
}
