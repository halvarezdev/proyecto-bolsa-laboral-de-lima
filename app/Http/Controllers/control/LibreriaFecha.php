<?php

namespace App\Http\Controllers\control;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class LibreriaFecha extends Controller{

    public static function parseFechaLetraMes($mes){

        $mes_letra = "";

        switch ($mes) {
            case '01':
                $mes_letra = 'Enero';
            break;
            case '02':
                $mes_letra = 'Febrero';
            break;
            case '03':
                $mes_letra = 'Marzo';
            break;
            case '04':
                $mes_letra = 'Abril';
            break;
            case '05':
                $mes_letra = 'mayo';
            break;
            case '06':
                $mes_letra = 'Junio';
            break;
            case '07':
                $mes_letra = 'Julio';
            break;
            case '08':
                $mes_letra = 'Agosto';
            break;
            case '09':
                $mes_letra = 'Setiembre';
            break;
            case '10':
                $mes_letra = 'Octubre';
            break;
            case '10':
                $mes_letra = 'Noviembre';
            break;
            case '10':
                $mes_letra = 'Diciembre';
            break;
            default:
                $mes_letra = 'Enero';
            break;
        }

        return $mes_letra;
    }




}