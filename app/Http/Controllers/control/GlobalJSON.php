<?php

namespace App\Http\Controllers\control;


use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class GlobalJSON extends Controller{
	public  function getProviJson($depa) {
		$result_prov=array();
        try {
            $lista_prov = DB::select('SELECT id,ubi_codprov,ubi_descripcion FROM ubigeo where  ubi_coddist="00" and ubi_codprov!="00" and ubi_coddpto='.'"'.$depa.'" order by ubi_descripcion ASC ');
           // $result_prov['0']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[]=$value;
                }
            }
        } catch (Exception $e) {
        }
        return response()->json(array('ResulProvincia'=>$result_prov));
	}
	public  function getDistritoJson($depa='',$priv) {
		$result_prov=array();
        try {
            $lista_prov = DB::select('SELECT id,ubi_coddist,ubi_descripcion FROM ubigeo where  ubi_coddist!="00" and ubi_codprov="'.$priv.'" and ubi_coddpto='.'"'.$depa.'" order by ubi_descripcion ASC ');

           // $result_prov['0']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[]=$value;
                }
            }
        } catch (Exception $e) {
        }
       return response()->json(array('ResulDistrito'=>$result_prov));
	}

    public  function automplitPuesto(Request $request) {
        $result_prov=array();

        try {
            $lista_prov = DB::select('SELECT id,profp_codigo,nombre as value FROM profesionales WHERE nombre LIKE "'.$request->term.'%"  order by nombre ASC LIMIT 20');

           // $result_prov['0']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[]=$value;
                }
            }
        } catch (Exception $e) {
        }
       return response()->json($result_prov);
    }
     public  function automplitOpupacion(Request $request) {
        $result_prov=array();

        try {
            $lista_prov = DB::select('SELECT id,ocup_codigo,nombre as value FROM ocupaciones WHERE nombre LIKE "'.$request->term.'%"  order by nombre ASC LIMIT 20');

           // $result_prov['0']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[]=$value;
                }
            }
        } catch (Exception $e) {
        }
       return response()->json($result_prov);
    }
}