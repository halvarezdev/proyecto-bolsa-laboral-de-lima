<?php

namespace App\Http\Controllers\control;


use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UbigeoLista extends Controller{

	public static function getPaisSelectOption() {
		$result_prov=array();
        try {
            $lista_prov = DB::select('SELECT pais_cod, pais_descripcion FROM paises order by pais_descripcion ASC ');
            $result_prov['']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[$value->pais_cod]=$value->pais_descripcion;
                }
            }
        } catch (Exception $e) {
        }
        return $result_prov;
	}
	public static function getDepartSelectOption() {
		$result_prov=array();
        try {
            $lista_prov = DB::select('SELECT id,ubi_coddpto,ubi_descripcion FROM ubigeo where  ubi_coddist="00" and ubi_codprov="00" order by ubi_descripcion ASC ');
            $result_prov['']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[$value->ubi_coddpto]=$value->ubi_descripcion;
                }
            }
        } catch (Exception $e) {
        }
        return $result_prov;
	}
	public static function getProviSelectOption($depa) {
		$result_prov=array();
        try {
            $lista_prov = DB::select('SELECT id,ubi_codprov,ubi_descripcion FROM ubigeo where  ubi_coddist="00" and ubi_codprov!="00" and ubi_coddpto='.'"'.$depa.'" order by ubi_descripcion ASC ');
            $result_prov['']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[$value->ubi_codprov]=$value->ubi_descripcion;
                }
            }
        } catch (Exception $e) {
        }
        return $result_prov;
	}
	public static function getDistritoSelectOption($depa,$priv) {
		$result_prov=array();
        try {
            $lista_prov = DB::select('SELECT id,ubi_coddist,ubi_descripcion FROM ubigeo where  ubi_coddist!="00" and ubi_codprov="'.$priv.'" and ubi_coddpto='.'"'.$depa.'" order by ubi_descripcion ASC ');

            $result_prov['']='SELECCIONE';
            if(count($lista_prov)>0){
                foreach ($lista_prov as $key => $value) {
                    $result_prov[$value->ubi_coddist]=$value->ubi_descripcion;
                }
            }
        } catch (Exception $e) {
        }
        return $result_prov;
	}
}