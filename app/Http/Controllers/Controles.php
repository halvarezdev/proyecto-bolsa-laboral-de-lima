<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

class Controles extends Controller{
   

   


   
    public static function nextIDTable($table,$primary,$sellcero) {
        $sql='';
      $result['status']=0;
      $result['mesaje']=''; 
      try{
        $arraySemanas=array();
        $sql ="SELECT fillZeros(IF(MAX($primary)IS NULL,0,MAX($primary))+1,$sellcero) AS id_next  FROM $table ORDER BY $primary DESC";
        $Lista=DB::select($sql);
        if(count($Lista)>0){
            foreach ($Lista as $key => $rst){
                $arraySemanas=$rst->id_next;
            }
        }
        $result['status']=1;
        $result['response']=$arraySemanas;
      }catch(\SQLException $ex){
          
          $result['mesaje']=$ex->getMessage();
      }
      return $result;
    }
    public static  function get_data_table_simple($sql){
        $response['status']=0;
		$response['message']='';
        try{  
            $row=DB::select($sql);
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
		}
		return $response;      
    }

   
}