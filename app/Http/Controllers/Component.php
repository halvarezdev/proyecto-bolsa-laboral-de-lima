<?php

namespace App\Http\Controllers;

class Component extends Controller{
     /**
     *  
     * @param  date  $fecha 
     * @param  String  $tipo + 1 days,+ 1 week, + 1 month,+ 1 year
     * @return date
     */
    public static  function get_strtotime($fecha,$tipe="+ 2 days",$format='/'){
        $diaretur='';
        $diamas=date("d-m-Y",strtotime($fecha.$tipe)); 
        $date=date_create($diamas);
        if($format=='/'){
           $diaretur=date_format($date,"d/m/Y");
        }else{
            $diaretur=date_format($date,"Y/m/d"); 
        }
        return  $diaretur;
        
    }
}