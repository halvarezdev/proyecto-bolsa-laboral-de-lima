<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\FormatoEntidad;
use App\Institucion;
use Carbon\Carbon;
use App\Favorito;
use App\Entidad;
use App\Vacante;
use App\Ubigeo;
use App\Pedido;


class VacanteController extends Controller
{
    public function __construct()
    {
       
       // $this->middleware('auth:contacto');

    }

    public function index(){

        $data['ubigeo'] = Ubigeo::where('ubi_codprov','00')
        ->where('ubi_coddist','00')
        ->get();

        return view('vacantepublica/registro',$data);

    }

    public function validaRucInstitucion($id){

        $institucion = Institucion::select('id','nombre')->where('ruc', $id)->get(); 
        $value = "0";
        if(count($institucion) > 0){
          $value = $institucion;			
        }
        return json_encode($value,JSON_PRETTY_PRINT);

    }

    public function saveVacante(Request $request){

        $id_institucion = Institucion::where('ruc', $request->ruc_institucion)->get();

        if(count($id_institucion) > 0){
            foreach($id_institucion as $value){
                $vacante = Vacante::create([
                    'id_institucion' => $value->id,
                    'num_convocatoria' => $request->num_convocatoria,
                    'nombre_especialidad' => $request->nom_especialidad,
                    'regimen_laboral' => $request->reginem_lab,
                    'experiencia' => ''.$request->experiencia.'',
                    'formacion_academica' => $request->form_academica,
                    'especializacion' => $request->especializacion,
                    'conocimiento' => $request->conocimiento,
                    'competencias' => $request->competencia,
                    'detalle' => $request->detalle,
                    'sueldo' => $request->sueldo,
                    'cant_vacantes' => $request->cant_vacante,
                    'fecha_postula' => $request->fech_postulacion,
                    'url_vanc' => $request->url_vanc,
                    'url_detalle' => $request->url_detalle,
                    'fech_referencia' => $request->fech_referencia,
                    'ubigeo' => $request->departamento,
                    'fech_inicio' => $request->fech_inicio,
                    'vac_flag' => '1',
                  ]);
               // echo $vacante->id;
            }
        }
        
       // echo $id_institucion->id;
       return redirect()->action('VacanteController@listaInstitucion');
    }

    public function registroInstitucion(){

        $data['vista'] = "1";

        return view('vacantepublica/registro-institucion', $data);

    }


    public function sabeInstitucion(Request $request){

        $institucion = Institucion::create([
            'ruc' => $request->ruc_institucion,
            'nombre' => $request->nom_institucion,
            'descripcion' => $request->descri_institucion,
            'url_institucion' => $request->url_institucion,
            'insti_flag' => '1',
            //'insti_logo' => 

        ]);

        return redirect()->action('VacanteController@listaInstitucion');

    }

    public function listaInstitucion(){

        $data['list_institucion'] = Institucion::orderBy('institucion.created_at', 'DESC')->paginate(15);

        return view('vacantepublica/listado-institucion', $data);
    }


    public function listaVacante($vacante){

        $data['list_vacante'] = Vacante::select('nombre_especialidad','fecha_postula',
        'formacion_academica','num_convocatoria','vacantes.id as codigo','fech_referencia','created_at','id_institucion')
        ->where('id_institucion',$vacante)
        ->orderBy('vacantes.created_at', 'DESC')
        ->paginate(15);

        return view('vacantepublica/listado-vacante', $data);

    }

    public function detalleVacantePublica($id){

        $date = Carbon::now();
        $date = $date->format('Y-m-d');
        $entidad = Input::get('e');
        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();
        
        $perfil_foto = "";
        $area_ped = "";

        $pedidos = Vacante::select('fech_referencia','nombre','insti_logo','nombre_especialidad','fecha_postula',
        'formacion_academica','num_convocatoria','vacantes.id as codigo','experiencia','cant_vacantes',
        'conocimiento','detalle','institucion.banner_insti','institucion.id as cod_insti',
        'sueldo','regimen_laboral','url_vanc','ubi_descripcion','url_detalle','num_convocatoria','competencias','especializacion')
        ->join('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->leftJoin('ubigeo' , 'vacantes.ubigeo','=','ubigeo.ubi_codigo')
        //->where('fecha_postula', '>', $date.' 00:00:00')
        ->where('vacantes.id', '=', $id)
        ->get();
        
        $id_insti = "";
        $banner_insti = "";
        foreach ($pedidos as $key => $value) {
            $perfil_foto = $value->insti_logo;
            $id_insti = $value->cod_insti;
            $banner_insti = $value->banner_insti;
            $formacion_academica = $value->formacion_academica;
        }

        # falta el relacionados
        
        if(isset(Auth::user()->persona_id)){
            $data['favorito'] = Favorito::where('pedidos_id', $id)->where('users_id',Auth::user()->id)->where('pedfav_procedencia','2')->get();
        }

        $palabra_clave = "CONTABILIDAD,ADMINISTRACIÓN,CIENCIA POLÍTICA,ABOGADO,ECONOMÍA,BIOLOGÍA,GESTIÓN AMBIENTAL,CIENCIAS AMBIENTALES,INGENIERÍA EN ELECTRÓNICA,TELECOMUNICACIONES,PUESTO,CARRERA,INFORMÁTICA,INGENIERÍA DE SOFTWARE,INGENIERÍA DE SISTEMAS,ESTADISTICA,CIENCIAS POLITICAS,ARTES,ANTROPOLOGIA,SOCIOLOGIA,ADMINISTRACION,EDUCACION,MUSICA,INGENIERÍA INDUSTRIAL,DERECHO,SECRETARIO,INGENIERÍA FORESTAL,SECRETARIO,ASISTENTE,ADMINISTRADOR,ECONOMISTA,CIENCIAS POLÍTICAS,CIENCIAS DE LA COMUNICACIÓN,GESTION SOCIAL,GESTION CULTURAL"; 
        $palarbras_prosciones = explode(",", $palabra_clave);
        $datos = "";
        $palabra_final = "";
        $buscar = false;
        for($i=0;$i<count($palarbras_prosciones);$i++){
            $posicion = strpos($formacion_academica, $palarbras_prosciones[$i]);
            if($posicion){
                if($buscar) {
                    $datos = $datos.'-';
                }
                $datos =  $datos.$palarbras_prosciones[$i];
                $buscar = true;
            }

            $palabra_final = $palarbras_prosciones[$i];
            
        }
       
        $data['banner_insti'] = $banner_insti;
        $porciones = explode("-", $datos);
        $date = Carbon::now();
        $date = $date->format('Y-m-d');

        $pedido_list = Pedido::select('pedidos.procedencia','empresas.perfil_foto as insti_logo',
        'pedidos.descripcion as nombre_especialidad',
        'empresas.razon_social as nombre','pedidos.id as codigo','pedidos.fecha_limite as fecha')
        ->where((function($query) use ($porciones, $buscar){
            foreach($porciones as $value){
                if($buscar){
                    $query->orWhere('pedidos.descripcion', 'LIKE', '%'.$value.'%');
                }
                
            }
        }))
        ->join('empresas', 'pedidos.empresa_id', '=', 'empresas.id');

        $data['realcionados'] = Vacante::select('vacantes.procedencia','insti_logo','nombre','nombre_especialidad',
        'vacantes.id as codigo','fecha_postula as fecha')
        ->join('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->where('fecha_postula', '>', $date.' 23:59:59')
        ->where('vacantes.id', '!=', $id)
        ->where((function($query) use ($porciones, $buscar){
            foreach($porciones as $value){
                if($buscar){
                    $query->orWhere('vacantes.nombre_especialidad', 'LIKE', '%'.$value.'%');
                }
                
            }
        }))
        ->unionAll($pedido_list) 
        ->orderBy('fecha', 'DESC')
        ->take(5)
        ->get();
        
        $data['perfil_foto']=$perfil_foto;
        $data['pedidos']=$pedidos;
        $data['inicio'] = "1";

        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
        }


        return view('vacantepublica/vacante_detalle_publica', $data);
    }


    public function formatoFacebook($codigo){

        $date = Carbon::now();
        $date = $date->format('Y-m-d');
        $data['ruta'] = "";
        $data['color_titulo'] = "";
        $data['color_subtitulo'] = "";
        $data['color_primer_parrafo'] = "";
        $data['color_segu_parrafo'] = "";
        $data['color_terce_parrafo'] = "";

        $data['cant_vanc'] = Vacante::where('id_institucion',$codigo)
        ->where('fecha_postula', '>=', $date.' 00:00:00')
        ->sum('cant_vacantes');;

        $data['lis_vanc'] = Vacante::where('id_institucion',$codigo)
        ->where('fecha_postula', '>=', $date.' 00:00:00')
        ->leftJoin('ubigeo' , 'vacantes.ubigeo','=','ubigeo.ubi_codigo')
        ->get();
        $data['formato'] = FormatoEntidad::where('id_institucion', $codigo)->get();

        foreach($data['formato'] as $value){
            $data['ruta'] = $value->imageFondo;
            $data['color_titulo'] = $value->colorTitulo;
            $data['color_subtitulo'] = $value->colorSubTitulo;
            $data['color_primer_parrafo'] = $value->colorPrimerParrafo;
            $data['color_segu_parrafo'] = $value->colorSegundoParrafo;
            $data['color_terce_parrafo'] = $value->colorTercerParrafo;
        }

        return view('vacantepublica/banner-fb', $data);

    }

    public function editarVacante(){
       $vacante = Input::get('vacante');
        $data['vacante'] = Vacante::select('institucion.ruc','vacantes.id as codigo','num_convocatoria','nombre_especialidad','regimen_laboral',
        'experiencia','formacion_academica','especializacion','conocimiento','competencias','detalle','sueldo','cant_vacantes',
        'fecha_postula','url_vanc','fech_referencia','ubigeo','url_detalle','fech_inicio')
        ->where('vacantes.id', $vacante)
        ->join('institucion' , 'institucion.id','=','vacantes.id_institucion')
        ->get();
        $data['ubigeo'] = Ubigeo::where('ubi_codprov','00')
        ->where('ubi_coddist','00')
        ->get();
        $data['vista'] = '1';
        return view('vacantepublica/registro', $data);

    }

    public function eliminaVacante(){
        $vacante = Input::get('vacante');
        $id_institucion = Input::get('id_institucion');

        $deletedRows = Vacante::where('vacantes.id', $vacante)->delete();
        //echo $deletedRows;
        return redirect()->action('VacanteController@listaVacante',['id_institucion' => $id_institucion]);

    }


    public function updateVacante(Request $request){

        $id_institucion = Institucion::where('ruc', $request->ruc_institucion)->get();

        if(count($id_institucion) > 0){
            foreach($id_institucion as $value){
                $vacante = Vacante::where('vacantes.id', $request->codigo_vacante)
                ->update([
                    'id_institucion' => $value->id,
                    'num_convocatoria' => $request->num_convocatoria,
                    'nombre_especialidad' => $request->nom_especialidad,
                    'regimen_laboral' => $request->reginem_lab,
                    'experiencia' => $request->experiencia,
                    'formacion_academica' => $request->form_academica,
                    'especializacion' => $request->especializacion,
                    'conocimiento' => $request->conocimiento,
                    'competencias' => $request->competencia,
                    'detalle' => $request->detalle,
                    'url_detalle' => $request->url_detalle,
                    'sueldo' => $request->sueldo,
                    'cant_vacantes' => $request->cant_vacante,
                    'fecha_postula' => $request->fech_postulacion,
                    'url_vanc' => $request->url_vanc,
                    'fech_referencia' => $request->fech_referencia,
                    'ubigeo' => $request->departamento,
                    'fech_inicio' => $request->fech_inicio,
                    'vac_flag' => '1',
                  ]);
               // echo $vacante->id;
            }

    }
    return redirect()->action('VacanteController@listaInstitucion');

    }

    public function updateInstitucion($institucion){

        $data['list_institucion'] = Institucion::where('id',$institucion)->get();
        $data['vista'] = "2";

        return view('vacantepublica/registro-institucion',$data);
    }

    public function editarInstitucion(Request $request){

        $vacante = Institucion::where('id', $request->codigo_insti)
            ->update([
                'ruc' => $request->ruc_institucion,
                'nombre' => $request->nom_institucion,
                'descripcion' => $request->descri_institucion,
                'url_institucion' => $request->url_institucion,
                'insti_flag' => '1',
            ]);

        return redirect()->action('VacanteController@listaInstitucion');

    }

    public function vacanteListaTotal(){

        $data['list_vacante'] = Vacante::select('vacantes.nombre_especialidad','vacantes.fecha_postula',
        'vacantes.formacion_academica','vacantes.num_convocatoria','vacantes.id as codigo','vacantes.fech_referencia','vacantes.created_at','id_institucion','institucion.nombre','institucion.ruc')
        ->join('institucion','institucion.id','=','vacantes.id_institucion')
        ->orderBy('vacantes.created_at', 'DESC')
        ->paginate(15);

        return view('vacantepublica/vacante_total', $data);

    }

}
