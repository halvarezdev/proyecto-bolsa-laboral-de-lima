<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
class DashboardServicioController extends Controller{

    public function index(){
        $response['status']=0;
        $response['message']='';
        try{
            $datais=$this->paginregisrtro('');
            if($datais['status']==0)
                throw new \Exception($datais['message']);
            
            $response['data']=$datais;
            $response['fecha']=date('d/m/Y');
           return view('citas/dashboard-servicio-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function gruposervicio(Request $request){
        $report=$request->method;
        $nombreclase = "\DashboardServicioController::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function nuevo($request){
        $response['status']=0;
        $response['message']='';
        try{
            $response['fecha']=date('d/m/Y');
            $obj=array();
            if($request['id']!=''){
                $sql='SELECT srv_detalle as detalle,srv_id,srv_name,srv_estado FROM tb_servicio WHERE srv_id='.$request['id'];
                $row=DB::select($sql);
                $obj= $row[0];
            }
            $response['obj']=$obj;  
            $response['id']=$request['id'];
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;    
    }
    public function save_servicio($request){
        $response['status']=0;
        $response['message']='';
        try{
            $pass="".($request['idsede']==''?'0':$request['idsede']).",'".$request['nombre']."','".$request['detalle']."'";
            $sql="CALL acc_saveservicio(".$pass.")";
            $row=DB::select($sql);
            $obg=$row[0];
            $datais=$this->paginregisrtro('');
            $response['datasede']=$obg;
            $response['data']=$datais;
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function anular_servicio($request){
        $response['status']=0;
        $response['message']='';
        try{
            $estado=($request['estado']=='1'?0:1);
            $post=['srv_estado'=>$estado];
            $row=DB::table('tb_servicio')
            ->where('srv_id',$request['idsede']) 
            ->update($post);
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function paginregisrtro($request){
        $response['status']=0;
        $response['message']='';
         
        try{ 
            $html='';
            $Lista =DB::table('tb_servicio')
            ->select(DB::raw('srv_name,srv_id,srv_estado'))
            // ->whereRaw($where)
            ->orderByRaw('srv_id')
            ->paginate(30);
            if (count($Lista)>0) {
                $item=0;
                foreach ($Lista as $key => $rst) {
                    $html.='<tr id="table_tr_td_sede'.$rst->srv_id.'">';
                    $html.='<td>'.($key+1).'</td>';
                    $html.='<td>'.$rst->srv_name.'</td>';
                    $html.='<td>';
                    if($rst->srv_estado=='1'){
                        $html.='<b style="color:#f588a1">Activo</b>';
                    }else{
                        $html.='<b style="color:#f2dede">Anulado</b>';
                    }
                    $html.='</td>';
                    $html.='<td><a href="#" data-toggle="modal" data-target="#myModal" onclick="editarServicio('.$rst->srv_id.')">Editar</a></td>';
                    $html.='<td><a href="#" onclick="anular_servicio(this,'.$rst->srv_id.','.$rst->srv_estado.')">Anular</a></td>';
                    $html.='</tr>';
                    $item++;
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"5\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="5">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
            $response['table']=$initial_content;
            $response['theadPagin']=$initial_content2;
             
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }

}