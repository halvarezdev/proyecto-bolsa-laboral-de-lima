<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
class CitasController extends Controller{

    public function index(){
        $response['status']=0;
		$response['message']='';
        try{
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);
                foreach ($sede['response'] as $key => $value) {
                    $row_sede[$value->sde_id]=$value->sde_name;
                }
            $response['row_sede']=$row_sede;
           return view('citas/citas_view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function registraCita(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            
            $sql="CALL acc_savecitapersona('$request->tipoDoc','$request->numDoc','$request->apePat','$request->apeMat','$request->nombre','$request->correo','$request->telefono','$request->txtfecha','$request->fecha','$request->servicio','$request->horario','$request->sede')";
            $row=DB::select($sql);
            $obg=$row[0];
            $response['datacita']=$obg;
            $response['status']=1;
         
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function validarDNI(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            $sql="SELECT email,numero_documento,name,ape_pat,ape_mat
             FROM personas 
            WHERE numero_documento=$request->dni";
            $row=DB::select($sql);
            $row_data=$row[0];
        $response['status']=1;
            
            $response['row_data']=$row_data;
         
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;

        
    }
    public function filtraHorarios(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            $row_obj=array();
            
            $sql='SELECT hro_fecdes,ha.hra_id,hra_name,hro_id ,
           
            hro_lunes, hro_lnactp,hro_lnfec,
            hro_martes ,hro_mtactp,hro_mtfec,
            hro_miercoles ,hro_mcactp,hro_mcfec,
            hro_jueves,hro_jvactp,hro_jvfec,
            hro_viernes,hro_vnactp,hro_vnfec,
            hro_sabado,hro_sbactp,hro_sbfec,
            hro_domingo FROM tb_horario  ha
        INNER JOIN tb_hora h ON h.hra_id=ha.hra_id
        WHERE hra_estado=1 AND hro_estado=1 
        AND sde_id='.$request->sede.' AND srv_id='.$request->servicio .' AND
        hro_fecdes="'.$request->inicio.'" AND hro_fechas="'.$request->fin.'"';
            $row=DB::select($sql);
        foreach ($row as $key => $value) {
            $row_obj[$value->hra_id][]=$value;
        }
        $contLibres=0;
        $contRows=0;
        $htmlRow='';
        foreach ($row_obj as $k => $value) {
        //foreach ($row_obj as $key => $value) {
            $htmlRow .= "<tr>";
            $htmlRow .= "<td class='text-center'><b>" .$value[0]->hra_name. "</b></td>";
           
                 
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_lunes;
                $fechal=$this->getFechaMayor($val->hro_lnfec); 
                if($fechal==1){
                    if ($val->hro_lnactp === '1') {
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",0,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    } else {
                        $cssClass = "hrbloqueado";
                        $htmlRow .= "<div class='" .$cssClass . " text-center'> ";
                        $htmlRow .= $hrStr;
                        $htmlRow .= "</div>";
                    }
                }else{
                    $htmlRow .= "<div class='hrbloqueado text-center'> No disponible </div>";
                }
            }
            $htmlRow .= "</td>";
            
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_martes;
                $fecham=$this->getFechaMayor($val->hro_mtfec); 
                if($fecham==1){
                    if ($val->hro_mtactp === '1') {
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",1,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    } else {
                        $cssClass = "hrbloqueado";
                        $htmlRow .= "<div class='" .$cssClass . " text-center'> ";
                        $htmlRow .= $hrStr;
                        $htmlRow .= "</div>";
                    }
                }else{
                    $htmlRow .= "<div class='hrbloqueado text-center'> No disponible </div>";
                }
            }
            $htmlRow .= "</td>";

            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_miercoles;
                $fecham=$this->getFechaMayor($val->hro_mcfec); 
                if($fecham==1){
                    if ($val->hro_mcactp === '1') {
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ", 2,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    } else {
                        $cssClass = "hrbloqueado";
                        $htmlRow .= "<div class='" .$cssClass . " text-center'> ";
                        $htmlRow .= $hrStr;
                        $htmlRow .= "</div>";
                    }
                }else{
                    $htmlRow .= "<div class='hrbloqueado text-center'> No disponible </div>";
                }
            }

            $htmlRow .= "</td>";
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_jueves;
                $fecham=$this->getFechaMayor($val->hro_jvfec); 
                if($fecham==1){
                    if ($val->hro_jvactp === '1') {
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ", 3,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    } else {
                        $cssClass = "hrbloqueado";
                        $htmlRow .= "<div class='" .$cssClass . " text-center'> ";
                        $htmlRow .= $hrStr;
                        $htmlRow .= "</div>";
                    }
                }else{
                    $htmlRow .= "<div class='hrbloqueado text-center'> No disponible </div>";
                }
            }
            
            $htmlRow .= "</td>";
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_viernes;
                $fecham=$this->getFechaMayor($val->hro_vnfec); 
                if($fecham==1){
                    if ($val->hro_vnactp === '1') {
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",4,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    } else {
                        $cssClass = "hrbloqueado";
                        $htmlRow .= "<div class='" .$cssClass . " text-center'> ";
                        $htmlRow .= $hrStr;
                        $htmlRow .= "</div>";
                    }
                }else{
                    $htmlRow .= "<div class='hrbloqueado text-center'> No disponible </div>";
                }
            }
            
            $htmlRow .= "</td>";
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_sabado;
                $fecham=$this->getFechaMayor($val->hro_sbfec); 
                if($fecham==1){
                    if ($val->hro_sbactp === '1') {
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",5,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    } else {
                        $cssClass = "hrbloqueado";
                        $htmlRow .= "<div class='" .$cssClass . " text-center'> ";
                        $htmlRow .= $hrStr;
                        $htmlRow .= "</div>";
                    }
                }else{
                    $htmlRow .= "<div class='hrbloqueado text-center'> No disponible </div>";
                }
            }
            
            $htmlRow .= "</td>";
            $htmlRow .= "</tr>";
            if ($contLibres > 0) {
                
                $contRows++;
            }
            }
             
            //if ($contRows == 0)
            //  $htmlRow .= "<tr><td colspan='6' class='text-center'> No hay horarios disponibles para esta semana.</td></tr>";
            $fecha=$this->getFechaMayor('2020-12-07');
            $response['fecha']=$fecha; 
            $response['dataHora']=$htmlRow; 
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        
        return $response;
    }
    public function getFechaMayor($fecha){
         
        $fecha_actual = strtotime(date("Y-m-d H:i:00",time()));
        $fecha_entrada = strtotime($fecha);
        if( $fecha_entrada<$fecha_actual){
	        return 0;
	    }else{
		    return 1;
		}
    }
    public function BuscarDettaleServi(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            
            
            $sql='SELECT srv_name as names,srv.srv_detalle as detalle FROM   tb_servicio srv   WHERE srv.srv_id='.$request->servicio;
            $row=DB::select($sql);
            
            $response['datais']=$row[0]; 
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        
        return $response;
    }
    public function traerServicios(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            
            
            $sql='SELECT srv_name as names,srv.srv_id as codigo FROM tb_sedeservicio  AS sds
            INNER JOIN tb_servicio srv ON sds.srv_id=srv.srv_id WHERE sds.sde_id='.$request->sede;
            $row=DB::select($sql);
            
            $response['listaConfiguracionServicio']=$row; 
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        
        return $response;
    }
}