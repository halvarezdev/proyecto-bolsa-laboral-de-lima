<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
use Illuminate\Support\Facades\Auth;
class DashboardCitaController extends Controller{

    public function index(){
        $response['status']=0;
		$response['message']='';
        try{
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);

            foreach ($sede['response'] as $key => $value) {
                $row_sede[$value->sde_id]=$value->sde_name;
            }

            $row_canal=array('0'=>'-- SELECCIONE --');
            $canal=Controles::get_data_table_simple("SELECT stc_idrefe,sct_name FROM tb_maestrocita 
                WHERE sct_estado=1 AND stc_tipo='02' ORDER BY sct_orderby ASC");
            if($canal['status']==0)
                throw new \Exception($canal['message']);

            foreach ($canal['response'] as $key => $value) {
                $row_canal[$value->stc_idrefe]=$value->sct_name;
            }
            $row_estadoatencion=array('0'=>'-- SELECCIONE --');
            $estadoatencion=Controles::get_data_table_simple("SELECT stc_idrefe,sct_name FROM tb_maestrocita 
                WHERE sct_estado=1 AND stc_tipo='01' ORDER BY sct_orderby ASC");
            if($estadoatencion['status']==0)
                throw new \Exception($estadoatencion['message']);

            foreach ($estadoatencion['response'] as $key => $value) {
                $row_estadoatencion[$value->stc_idrefe]=$value->sct_name;
            }

            $response['row_estadoatencion']=$row_estadoatencion;
            $response['row_canal']=$row_canal;
            $response['row_sede']=$row_sede;
            $response['fecha']=date('d/m/Y');
            
           return view('citas/dashboard-citas_view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function buscarDataFill(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            $sql="SELECT cta_estado,cta_descripcion,c.cta_id,
            IF(cta_abandona IS NULL,'',cta_abandona) AS cta_abandona,
            IF(cta_horaini IS NULL,'',cta_horaini) AS cta_horaini,
            IF(cta_horafin IS NULL,'',cta_horafin) AS cta_horafin, c.cta_fectext FROM tb_citas c
            WHERE c.cta_id=$request->id";
            $row=DB::select($sql);
            $obg=$row[0];
            // $post=['clie_estado'=>9];
            // $row=DB::table('clientes')
            // ->where('clie_id',$request['id']) 
            // ->update($post); 
            $response['status']=1;
            $response['response']=$obg;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function actualizarData(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            $user_id=2;
            $post=['cta_horaini'=>$request->horaini,'cta_horafin'=>$request->finfecha,
            'cta_abandona'=>$request->AbandonoAsis,
            'cta_descripcion'=>$request->texto,
            'cta_estado'=>$request->tipofon,
            'user_id'=>$user_id,
            'cta_horaaten'=>$request->horaini.'-'.$request->finfecha,
        ];
            $row=DB::table('tb_citas')
            ->where('cta_id',$request['idcita']) 
            ->update($post); 
            $response['status']=1;
            $response['response']=$row;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function verHorariosCitas(Request $request){
        $response['status']=0;
		$response['message']='';
         
        try{ 
            $where=" tb_citas.hro_id=$request->idHorario  AND  tb_citas.sde_id=$request->sede 
             AND tb_citas.srv_id=$request->servicio   AND cta_fecha='$request->fecha'";
          
            $html='';
            $Lista =DB::table('tb_citas')
            ->select(DB::raw('telefono,email,cta_id,cta_estado,ape_mat ,cta_horaaten, cta_fectext,numero_documento,name,ape_pat,
            srv_name'))
            ->leftJoin('personas','personas.id','=','tb_citas.per_id')
            ->leftJoin('tb_servicio','tb_servicio.srv_id','=','tb_citas.srv_id')
            ->whereRaw($where)
            ->orderByRaw('cta_id')
            ->paginate(30);
            if (count($Lista)>0) {
                $item=0;
                foreach ($Lista as $key => $rst) {
                    $html.='<tr  id="table_tr_td_cita'.$rst->cta_id.'">';
                    $html.='<td >'.$rst->cta_horaaten.'</td>';
                    $html.='<td >'.$rst->numero_documento.'</td>';
                    $html.='<td >'.$rst->name.' '.$rst->ape_pat.' '.$rst->ape_mat.'</td>';
                    $html.='<td >'.$rst->telefono.'</td>';
                    $html.='<td >'.$rst->email.'</td>';
                         
                    $html.='<td>';
                    if($rst->cta_estado=='1'){
                        $html.='<b style="color:#AFEEEE">Pendiente</b>';
                    }elseif($rst->cta_estado=='2'){
                        $html.='<b style="color:#f588a1">Bloqueado</b>';
                    }elseif($rst->cta_estado=='3'){
                        $html.='<b style="color:#f2dede">Abandonado</b>';
                    }elseif($rst->cta_estado=='5'){
                        $html.='<b style="color:#f2dede">En Proceso</b>';
                    }elseif($rst->cta_estado=='4'){
                        $html.='<b style="color:#f2dede">Atendido</b>';
                        
                    }
                    $html.='</td>';
                     $html.='<td> <a href="#" data-toggle="modal" data-target="#myModal" onclick="atende_cita(this,'.$rst->cta_id.')">Atender</a>   
                        </td>';
                    $html.='</tr>';
                    $item++;
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"7\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="7">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
		    $response['table']=$initial_content;
            $response['theadPagin']=$initial_content2;
            
            
             
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
}