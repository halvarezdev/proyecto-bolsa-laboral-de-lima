<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
use Illuminate\Support\Facades\Hash;
class DashboardUserController extends Controller{

    public function index(){
        $response['status']=0;
		$response['message']='';
        try{
            $sede=Controles::get_data_table_simple("SELECT srv_id,srv_name FROM tb_servicio 
                  ORDER BY srv_id ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);

            foreach ($sede['response'] as $key => $value) {
                $row_sede[$value->srv_id]=$value->srv_name;
            }
            $datais=$this->paginregisrtro('');
            if($datais['status']==0)
                throw new \Exception($datais['message']);
            
            $response['data']=$datais;
            $response['row_sede']=$row_sede;
            $response['fecha']=date('d/m/Y');
           return view('citas/dashboard-user-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    
    public function grupoUser(Request $request){
        $report=$request->method;
        $nombreclase = "\DashboardUserController::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
   
    public function nuevo($request){
        $response['status']=0;
        $response['message']='';
        try{
            $response['fecha']=date('d/m/Y');
            $obj=array();
            if($request['id']!=''){
                $sql='SELECT id,name,estado ,email,textoalter FROM entidadusers WHERE id='.$request['id'];
                $row=DB::select($sql);
                $obj= $row[0];
            }
            $response['obj']=$obj;  
            $response['id']=$request['id'];
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;    
    }
    public function save($request){
        $response['status']=0;
        $response['message']='';
        try{
            $pass2=Hash::make($request['pas1']);
            $nombre=$request['nombre'];
            $usuario=$request['usuario'];
            $pas1=$request['pas1'];
            $pass=" ".($request['id']==''?'0':$request['id']).",'$nombre','$usuario','$pas1','$pass2'";
            $sql="CALL acc_saveuser_enti(".$pass.")";
            $row=DB::select($sql);
            $obg=$row[0];
            $datais=$this->paginregisrtro('');
            $response['datasede']=$obg;
            $response['data']=$datais;
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    
    public function anular($request){
        $response['status']=0;
        $response['message']='';
        try{
            $estado=($request['estado']=='1'?0:1);
            $post=['estado'=>$estado];
            $row=DB::table('entidadusers')
            ->where('id',$request['id']) 
            ->update($post);
            $response['response']=$row;
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function paginregisrtro($request){
        $response['status']=0;
        $response['message']='';
         
        try{ 
            $html='';
            $where='idperfil=3 AND entidad_id=1';
            $Lista =DB::table('entidadusers')
            ->select(DB::raw('email,name,id,estado'))
             ->whereRaw($where)
            ->orderByRaw('id')
            ->paginate(30);
            if (count($Lista)>0) {
                $item=0;
                foreach ($Lista as $key => $rst) {
                    $html.='<tr id="table_tr_td_User'.$rst->id.'">';
                    $html.='<td>'.($key+1).'</td>';
                    $html.='<td>'.$rst->name.'</td>';
                    $html.='<td>'.$rst->email.'</td>';
                    $html.='<td>';
                    if($rst->estado=='1'){
                        $html.='<b style="color:#f588a1">Activo</b>';
                    }else{
                        $html.='<b style="color:#f2dede">Anulado</b>';
                    }
                    $html.='</td>';
                    
                    $html.='<td><a href="#" data-toggle="modal" data-target="#myModal" onclick="editarUser('.$rst->id.')">Editar</a></td>';
                    $html.='<td><a href="#" onclick="anular_User(this,'.$rst->id.','.$rst->estado.')">Anular</a></td>';
                    $html.='</tr>';
                    $item++;
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"5\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="5">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
            $response['table']=$initial_content;
            $response['theadPagin']=$initial_content2;
             
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
}