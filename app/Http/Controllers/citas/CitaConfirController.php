<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
use App\Mail\EmergencyCallReceived;
use Illuminate\Support\Facades\Mail;
class CitaConfirController extends Controller{

    public function index(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            $sql="SELECT c.cta_id, c.cta_fectext,p.numero_documento,p.name,p.ape_pat,p.ape_mat,
            s.srv_name FROM tb_citas c
            INNER JOIN tb_servicio s ON c.srv_id=s.srv_id
            INNER JOIN personas p ON p.id=c.per_id
            WHERE c.cta_id=$request->id";
            $row=DB::select($sql);
            $obg=$row[0];
		    $response['data']=$obg;

           return view('citas/confirmacion',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function enviarCorreo(Request $request){
        $sql="SELECT email,c.cta_id, c.cta_fectext,p.numero_documento,p.name,p.ape_pat,p.ape_mat,
        s.srv_name FROM tb_citas c
        INNER JOIN tb_servicio s ON c.srv_id=s.srv_id
        INNER JOIN personas p ON p.id=c.per_id
        WHERE c.cta_id=$request->idCita";
        $row=DB::select($sql);
        $obg=$row[0];
        $subject = 'Reserva de cita en linea';
        $for = $obg->email;
       
   
        Mail::send('citas.mails.emergency_call',array('data'=>$obg), function($msj) use($subject,$for){
            $msj->from(env('MAIL_USERNAME',''),"Extranet CALLAO");
            $msj->subject($subject);
            $msj->to($for);
        });  
    }
}