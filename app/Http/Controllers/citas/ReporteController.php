<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class ReporteController extends Controller{

    public static function reporte01($request){
        $where='';
        $title2='';
        if($request['sede']!='0'){
            $where.=' AND s.sde_id='.$request['sede'];
        }
        $title2='Desde: '.$request['fecdes'].' Hasta '.$request['fechasta'];
        $date=date('d/m/Y');
        echo'<table class="table table-sm" border="0" width="100%" style=" border-collapse: collapse; ">';
        echo'<thead class="thead-dark"><tr>
        <td colspan="3"></td>
            <td colspan="10" style="text-align: center;"><b><p>'.$request['titulo'].'</p></b><b><p></p></b></td>
            <td colspan="3" style="text-align: right;"><div class="date">Fecha:'.$date .'</div></td>
        </tr>';
        echo'<tr>
            <td colspan="3">'.($request['tipo']=='pdf'?'<div class="no_imprimir"><a href="javascript:window.print()">Imprimir<a></div>':'').'</td>
            <td colspan="10" style="text-align: center;"><b>'.$title2.'</b></td>
            <td colspan="3" style="text-align: right;"><div class="date">Hora: '.date('h:i:s a').'</div></td>
        </tr><tr style="font-size: 13px;border-top:1px solid black;border-bottom:1px solid black" class="table-success">
        <th>#</th><th colspan="2">SEDE</th><th colspan="3">SERVICIO</th>
        <th>FECHA</th><th colspan="2">HORA DE SERVICIO</th><th>N°DOCUMENTO</th>
        <th colspan="3">CIUDADANO(A)</th><th>TELEFONO</th></tr>
        </thead>';
        $sql="SELECT s.sde_name,v.srv_name, DATE_FORMAT(c.cta_fecha,'%d/%m/%Y') AS fecha,c.cta_fectext,p.numero_documento,p.telefono,CONCAT(p.name,' ',p.ape_pat,' ',p.ape_mat)AS nombre FROM tb_citas c
        INNER JOIN personas p ON p.id=c.per_id
        INNER JOIN tb_sede s ON s.sde_id=c.sde_id
        INNER JOIN tb_servicio v ON v.srv_id=c.srv_id
        WHERE c.cta_fecha BETWEEN "."'".$request['fd']."'"." AND "."'".$request['fh']."'"." ".$where;
        $i=0;
        
        $Lista=DB::select($sql);
        foreach ($Lista as $ke => $array) {
            echo '<tr>';
            echo '<td>'.$i.'</td>';
            echo '<td colspan="2">'.$array->sde_name.'</td>';
            echo '<td colspan="3">'.$array->srv_name.'</td>';
            echo '<td>'.$array->fecha.'</td>';
            echo '<td colspan="2">'.$array->cta_fectext.'</td>';
            echo '<td >'.$array->numero_documento.'</td>';
            echo '<td colspan="3">'.$array->nombre.'</td>';
            echo '<td>'.$array->telefono.'</td>';
            echo '</tr>';
            $i++;
        }

    }
    public static function reporte02($request){
        $where='';
        $title2='';
        if($request['sede']!='0'){
        $where.=' AND s.sde_id='.$request['sede'];
        }
        $title2='Desde: '.$request['fecdes'].' Hasta '.$request['fechasta'];
        $date=date('d/m/Y');
        echo'<table class="table table-sm" border="0" width="100%" style=" border-collapse: collapse; ">';
        echo'<thead class="thead-dark"><tr>
        <td colspan="3"></td>
        <td colspan="10" style="text-align: center;"><b><p>'.$request['titulo'].'</p></b><b><p></p></b></td>
        <td colspan="3" style="text-align: right;"><div class="date">Fecha:'.$date .'</div></td>
        </tr>';
        echo'<tr>
      <td colspan="3">'.($request['tipo']=='pdf'?'<div class="no_imprimir"><a href="javascript:window.print()">Imprimir<a></div>':'').'</td>
      <td colspan="10" style="text-align: center;"><b>'.$title2.'</b></td>
      <td colspan="3" style="text-align: right;"><div class="date">Hora: '.date('h:i:s a').'</div></td>
        </tr><tr style="font-size: 13px;border-top:1px solid black;border-bottom:1px solid black" class="table-success">
        <th>#</th><th colspan="2">SEDE</th><th colspan="3">SERVICIO</th>
        <th>FECHA</th><th colspan="2">HORA DE SERVICIO</th><th>N°DOCUMENTO</th>
        <th colspan="3">CIUDADANO(A)</th><th>OBSERVACIÓN</th></tr>
        </thead>';
        $sql="SELECT cta_descripcion,s.sde_name,v.srv_name, DATE_FORMAT(c.cta_fecha,'%d/%m/%Y') AS fecha,c.cta_fectext,p.numero_documento,p.telefono,CONCAT(p.name,' ',p.ape_pat,' ',p.ape_mat)AS nombre FROM tb_citas c
        INNER JOIN personas p ON p.id=c.per_id
        INNER JOIN tb_sede s ON s.sde_id=c.sde_id
        INNER JOIN tb_servicio v ON v.srv_id=c.srv_id
        WHERE cta_estado!=1 AND c.cta_fecha BETWEEN "."'".$request['fd']."'"." AND "."'".$request['fh']."'"." ".$where;
        $i=0;
    
        $Lista=DB::select($sql);
        foreach ($Lista as $ke => $array) {
        echo '<tr>';
        echo '<td>'.$i.'</td>';
        echo '<td colspan="2">'.$array->sde_name.'</td>';
        echo '<td colspan="3">'.$array->srv_name.'</td>';
        echo '<td>'.$array->fecha.'</td>';
        echo '<td colspan="2">'.$array->cta_fectext.'</td>';
        echo '<td >'.$array->numero_documento.'</td>';
        echo '<td colspan="3">'.$array->nombre.'</td>';
        echo '<td>'.$array->cta_descripcion.'</td>';
        echo '</tr>';
        $i++;
        }

    }
    public static function reporte03($request){
        $where='';
        $title2='';
        if($request['sede']!='0'){
        $where.=' AND s.sde_id='.$request['sede'];
        }
        $title2='Desde: '.$request['fecdes'].' Hasta '.$request['fechasta'];
        $date=date('d/m/Y');
        
        echo'<table class="table table-sm" border="0" width="100%" style=" border-collapse: collapse; ">';
        echo'<thead class="thead-dark"><tr>
        <td colspan="3"></td>
        <td colspan="10" style="text-align: center;"><b><p>'.$request['titulo'].'</p></b><b><p></p></b></td>
        <td colspan="3" style="text-align: right;"><div class="date">Fecha:'.$date .'</div></td>
        </tr>';
        echo'<tr>
      <td colspan="3">'.($request['tipo']=='pdf'?'<div class="no_imprimir"><a href="javascript:window.print()">Imprimir<a></div>':'').'</td>
      <td colspan="10" style="text-align: center;"><b>'.$title2.'</b></td>
      <td colspan="3" style="text-align: right;"><div class="date">Hora: '.date('h:i:s a').'</div></td>
        </tr><tr style="font-size: 13px;border-top:1px solid black;border-bottom:1px solid black" class="table-success">
        <th>#</th><th colspan="2">SEDE</th><th colspan="3">SERVICIO</th>
        <th>FECHA</th><th colspan="2">HORA DE SERVICIO</th><th>N°DOCUMENTO</th>
        <th colspan="3">CIUDADANO(A)</th><th>TELEFONO</th><th>CORREO</th><th>OBSERVACIÓN</th></tr>
        </thead>';
        $sql="SELECT email,cta_descripcion,s.sde_name,v.srv_name, DATE_FORMAT(c.cta_fecha,'%d/%m/%Y') AS fecha,c.cta_fectext,p.numero_documento,p.telefono,CONCAT(p.name,' ',p.ape_pat,' ',p.ape_mat)AS nombre FROM tb_citas c
        INNER JOIN personas p ON p.id=c.per_id
        INNER JOIN tb_sede s ON s.sde_id=c.sde_id
        INNER JOIN tb_servicio v ON v.srv_id=c.srv_id
        WHERE cta_estado!=1 AND c.cta_fecha BETWEEN "."'".$request['fd']."'"." AND "."'".$request['fh']."'"." ".$where;
        $i=0;
    
        $Lista=DB::select($sql);
        foreach ($Lista as $ke => $array) {
        echo '<tr>';
        echo '<td>'.$i.'</td>';
        echo '<td colspan="2">'.$array->sde_name.'</td>';
        echo '<td colspan="3">'.$array->srv_name.'</td>';
        echo '<td>'.$array->fecha.'</td>';
        echo '<td colspan="2">'.$array->cta_fectext.'</td>';
        echo '<td >'.$array->numero_documento.'</td>';
        echo '<td colspan="3">'.$array->nombre.'</td>';
        echo '<td>'.$array->telefono.'</td>';
        echo '<td>'.$array->email.'</td>';
        echo '<td>'.$array->cta_descripcion.'</td>';
        echo '</tr>';
        $i++;
        }

    }
    public static function reporte04($request){
        $where='';
        $title2='';
        if($request['sede']!='0'){
        $where.=' AND s.sde_id='.$request['sede'];
        }
        $title2='Desde: '.$request['fecdes'].' Hasta '.$request['fechasta'];
        $date=date('d/m/Y');
        
        echo'<table class="table table-sm" border="0" width="100%" style=" border-collapse: collapse; ">';
        echo'<thead class="thead-dark"><tr>
        <td colspan="3"></td>
        <td colspan="4" style="text-align: center;"><b><p>'.$request['titulo'].'</p></b><b><p></p></b></td>
        <td colspan="3" style="text-align: right;"><div class="date">Fecha:'.$date .'</div></td>
        </tr>';
        echo'<tr>
      <td colspan="3">'.($request['tipo']=='pdf'?'<div class="no_imprimir"><a href="javascript:window.print()">Imprimir<a></div>':'').'</td>
      <td colspan="4" style="text-align: center;"><b>'.$title2.'</b></td>
      <td colspan="3" style="text-align: right;"><div class="date">Hora: '.date('h:i:s a').'</div></td>
        </tr><tr style="font-size: 13px;border-top:1px solid black;border-bottom:1px solid black" class="table-success">
        <th>#</th><th colspan="2">SEDE</th><th colspan="3">SERVICIO</th>
         <th>CITAS RESERVADAS</th><th>CITAS ATENDIDAS</th></tr>
        </thead>';
        $sql="SELECT COUNT(*) AS sitasresser, 
        IF(cta_estado=5,COUNT(*),0)AS atendido,
        cta_estado,v.srv_name, s.sde_name,c.cta_fectext 
        FROM tb_citas c
      INNER JOIN tb_sede s ON s.sde_id=c.sde_id
      INNER JOIN tb_servicio v ON v.srv_id=c.srv_id
      WHERE  c.cta_fecha BETWEEN "."'".$request['fd']."'"." AND "."'".$request['fh']."'"." ".$where.' GROUP BY s.sde_id,v.srv_id';
       $i=0;
        $Lista=DB::select($sql);
        foreach ($Lista as $ke => $array) {
        echo '<tr>';
        echo '<td>'.$i.'</td>';
        echo '<td colspan="2">'.$array->sde_name.'</td>';
        echo '<td colspan="3">'.$array->srv_name.'</td>';
        echo '<td>'.$array->sitasresser.'</td>';
        echo '<td  >'.$array->atendido.'</td>';
       
        echo '</tr>';
        $i++;
        }

    }
}