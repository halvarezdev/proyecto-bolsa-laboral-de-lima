<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
use Illuminate\Support\Facades\Auth;
class DashboardCitaPrecenciaController extends Controller{

    public function index(Request $request){
        $response['status']=0;
		$response['message']='';
        try{ 
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);

            foreach ($sede['response'] as $key => $value) {
                $row_sede[$value->sde_id]=$value->sde_name;
            }

            
            $tipdocum=Controles::get_data_table_simple("SELECT id,tipdoc_descripion 
            FROM tipo_documento  ORDER BY id ASC");
            if($tipdocum['status']==0)
                throw new \Exception($tipdocum['message']);

            foreach ($tipdocum['response'] as $key => $value) {
                $row_tipdocum[$value->id]=$value->tipdoc_descripion;
            }
            $fecha=date('Y-m-d');
            $response['row_sede']=$row_sede;
            $response['row_tipdocum']=$row_tipdocum;
            $obj=array('desde'=>$fecha,'hasta'=>$fecha);
            $datais=$this->paginregisrtro($obj);
            if($datais['status']==0)
                throw new \Exception($datais['message']);
            
            
            $response['data']=$datais;
            $response['horaini']=date('H:m:s');
            $response['fecha']=date('d/m/Y');            
            return view('citas/dashboard-citas-reservada_view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function grupoprece(Request $request){
        $report=$request->method;
        $nombreclase = "\DashboardCitaPrecenciaController::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function filter($request){
        $response['status']=0;
		$response['message']='';
        try{ 
        $datais=$this->paginregisrtro($request);
            if($datais['status']==0)
                throw new \Exception($datais['message']);
            
            
            $response['response']=$datais;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function nuevo($request){
        $response['status']=0;
		$response['message']='';
        try{ 
            
            $response['fecha']=date('d/m/Y');
            $obj=array();
            if($request['id']!=''){
                $sql='SELECT srv_id,sde_id,sexo,DATE_FORMAT(fecha_nacimiento,"%d/%m/%Y") as fechana,tipo_documento,
                telefono,email,cta_horaini,cta_horafin,DATE_FORMAT(cta_fecha,"%d/%m/%Y") as fecha,
                cta_id,cta_estado,ape_mat ,cta_horaaten, cta_fectext,numero_documento,name,ape_pat
                 FROM tb_citas c INNER JOIN personas p ON p.id=c.per_id
                WHERE c.cta_id='.$request['id'];
                $row=DB::select($sql);
                $obj= $row[0];
            }
            $response['obj']=$obj;  
            $response['id']=$request['id'];
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;    
    }
    public function save_pres($request){
        $response['status']=0;
		$response['message']='';
        try{
            $horafin=$request['horaini'].'-'.$request['horafin'];
            $fech=date('d/m/Y').' '.$request['horaini'].'-'.$request['horafin'];
            $pass="".($request['idcita']==''?'0':$request['idcita']).",'".$request['tipdoc']."','".$request['dni']."','".$request['appat']."','".$request['apmat']."','".$request['nombre']."','".$request['correo']."','".$request['tefon']."'";
            $pass2=",'".$request['sexo']."','".$request['fechna']."','".$fech."','".$request['fecha']."','".$request['serv']."',NULL,'".$request['sede']."'";
            $pass3=",'".$request['horaini']."','".$request['horafin']."','".$horafin."'";
            $sql="CALL acc_savecitapersonaprecencial(".$pass."".$pass2.$pass3.")";
            $row=DB::select($sql);
            $obg=$row[0];
            $response['datacita']=$obg;
            $response['status']=1;
         
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function paginregisrtro($request){
        $response['status']=0;
		$response['message']='';
         
        try{ 
            $where="  tb_citas.cta_tiporegistr='INTRANET'  AND cta_fecha BETWEEN '".$request['desde']. "' AND '".$request['hasta']."'";
          
            $html='';
            $Lista =DB::table('tb_citas')
            ->select(DB::raw('telefono,email,cta_id,cta_estado,ape_mat ,cta_horaaten, cta_fectext,numero_documento,name,ape_pat,
            srv_name'))
            ->leftJoin('personas','personas.id','=','tb_citas.per_id')
            ->leftJoin('tb_servicio','tb_servicio.srv_id','=','tb_citas.srv_id')
            ->whereRaw($where)
            ->orderByRaw('cta_id')
            ->paginate(30);
            if (count($Lista)>0) {
                $item=0;
                foreach ($Lista as $key => $rst) {
                    $html.='<tr  id="table_tr_td_cita'.$rst->cta_id.'">';
                    $html.='<td >'.$item .'</td>';
                    $html.='<td >'.$rst->srv_name.'</td>';
                    $html.='<td >'.$rst->cta_fectext.'</td>';
                    $html.='<td >'.$rst->cta_horaaten.'</td>';
                    $html.='<td >'.$rst->numero_documento.'</td>';
                    $html.='<td >'.$rst->name.' '.$rst->ape_pat.' '.$rst->ape_mat.'</td>';
                    $html.='<td >'.$rst->telefono.'</td>';
                    $html.='<td >'.$rst->email.'</td>';
                         
                    $html.='<td>';
                    if($rst->cta_estado=='1'){
                        $html.='<b style="color:#AFEEEE">Pendiente</b>';
                    }elseif($rst->cta_estado=='2'){
                        $html.='<b style="color:#f588a1">Bloqueado</b>';
                    }elseif($rst->cta_estado=='3'){
                        $html.='<b style="color:#f2dede">Abandonado</b>';
                    }elseif($rst->cta_estado=='5'){
                        $html.='<b style="color:#f2dede">En Proceso</b>';
                    }elseif($rst->cta_estado=='4'){
                        $html.='<b style="color:#f2dede">Atendido</b>';
                        
                    }
                    $html.='</td>';
                     $html.='<td> <a href="#" data-toggle="modal" data-target="#myModal" onclick="atende_cita(this,'.$rst->cta_id.')">Atender</a>   
                        </td>';
                    $html.='</tr>';
                    $item++;
                }
            }else{
                $html .= "<tr  >"; 
                $html .="<td colspan=\"7\" align=\"center\"><b>No se Encontro Resultado</b></td>" ;
                $html .="</tr>";
            }
            $tbl2 ='<tr><td colspan="7">';
            $tbl2  .=$Lista->links(); 
            $tbl2 .='</td></tr>'; 
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['status']=1;
		    $response['table']=$initial_content;
            $response['theadPagin']=$initial_content2;
            
            
             
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
}