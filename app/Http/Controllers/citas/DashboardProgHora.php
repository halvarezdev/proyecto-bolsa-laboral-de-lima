<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
use Illuminate\Support\Facades\Auth;
class DashboardProgHora extends Controller{

    public function index(Request $request){
       
            $response['status']=0;
            $response['message']='';
            try{
                $row_sede=array('0'=>'-- SELECCIONE --');
                $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                    WHERE sde_estado=1 ORDER BY sde_orderby ASC");
                if($sede['status']==0)
                    throw new \Exception($sede['message']);
    
                foreach ($sede['response'] as $key => $value) {
                    $row_sede[$value->sde_id]=$value->sde_name;
                }
    
                $row_canal=array('0'=>'-- SELECCIONE --');
                $canal=Controles::get_data_table_simple("SELECT stc_idrefe,sct_name FROM tb_maestrocita 
                    WHERE sct_estado=1 AND stc_tipo='02' ORDER BY sct_orderby ASC");
                if($canal['status']==0)
                    throw new \Exception($canal['message']);
    
                foreach ($canal['response'] as $key => $value) {
                    $row_canal[$value->stc_idrefe]=$value->sct_name;
                }
                $row_estadoatencion=array('0'=>'-- SELECCIONE --');
                $estadoatencion=Controles::get_data_table_simple("SELECT stc_idrefe,sct_name FROM tb_maestrocita 
                    WHERE sct_estado=1 AND stc_tipo='01' ORDER BY sct_orderby ASC");
                if($estadoatencion['status']==0)
                    throw new \Exception($estadoatencion['message']);
    
                foreach ($estadoatencion['response'] as $key => $value) {
                    $row_estadoatencion[$value->stc_idrefe]=$value->sct_name;
                }
    
                $response['row_estadoatencion']=$row_estadoatencion;
                $response['row_canal']=$row_canal;
                $response['row_sede']=$row_sede;
                $response['fecha']=date('d/m/Y');
                
               return view('citas/dashboard-Progrmacion_view',$response);
            }catch (\Exception $exc) {
                $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
            } 
            return $response;
        }
    public function grupopPrcale(Request $request){
        $report=$request->method;
        $nombreclase = "\DashboardProgHora::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function getCountServ($request){
        $response['status']=0;
        $response['message']='';
        try{
            $sql="SELECT COUNT(*) as cantidad FROM tb_horario 
            WHERE sde_id=".$request['sede']." AND 
            srv_id=".$request['servicio']." AND hro_fechas="."'".$request['fin']."'";
            $row=DB::select($sql);
            $obg=$row[0];
            // $post=['clie_estado'=>9];
            // $row=DB::table('clientes')
            // ->where('clie_id',$request['id']) 
            // ->update($post); 
            $response['status']=1;
            $response['response']=$obg;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function filtraHorarios(Request $request){
        $response['status']=0;
		$response['message']='';
        try{
            $row_obj=array();
            
            $sql='SELECT hro_fecdes,ha.hra_id,hra_name,hro_id ,
           
            hro_lunes, hro_lnactp,hro_lnfec,
            hro_martes ,hro_mtactp,hro_mtfec,
            hro_miercoles ,hro_mcactp,hro_mcfec,
            hro_jueves,hro_jvactp,hro_jvfec,
            hro_viernes,hro_vnactp,hro_vnfec,
            hro_sabado,hro_sbactp,hro_sbfec,
            hro_domingo FROM tb_horario  ha
        INNER JOIN tb_hora h ON h.hra_id=ha.hra_id
        WHERE hra_estado=1 AND hro_estado=1 
        AND sde_id='.$request->sede.' AND srv_id='.$request->servicio .' AND
        hro_fecdes="'.$request->inicio.'" AND hro_fechas="'.$request->fin.'"';
            $row=DB::select($sql);
        foreach ($row as $key => $value) {
            $row_obj[$value->hra_id][]=$value;
        }
        $contLibres=0;
        $contRows=0;
        $htmlRow='';
        foreach ($row_obj as $k => $value) {
        //foreach ($row_obj as $key => $value) {
            $htmlRow .= "<tr>";
            $htmlRow .= "<td class='text-center'><b>" .$value[0]->hra_name. "</b></td>";
           
                 
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_lunes;
                
                $cssClass = "hrlibre";
                $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",0,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                $contLibres++;
                     
                
            }
            $htmlRow .= "</td>";
            
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_martes;
                 
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",1,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    
            }
            $htmlRow .= "</td>";

            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_miercoles;
                 
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ", 2,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                     
            }

            $htmlRow .= "</td>";
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_jueves;
                 
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ", 3,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    
            }
            
            $htmlRow .= "</td>";
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_viernes;
                
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",4,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                     
            }
            
            $htmlRow .= "</td>";
            $htmlRow .= "<td class='text-center'>";
            foreach ($value as $ks => $val) {
                $hrStr=$val->hro_sabado;
                 
                            $cssClass = "hrlibre";
                        $htmlRow .= "<a class='" .$cssClass . "' href='javascript:;' onclick='seleccionarHora(" . $val->hro_id . ",5,\"" . $hrStr . "\", this)'> " . $hrStr . "</a>";
                        $contLibres++;
                    
            }
            
            $htmlRow .= "</td>";
            $htmlRow .= "</tr>";
            if ($contLibres > 0) {
                
                $contRows++;
            }
            }
             
            //if ($contRows == 0)
            //  $htmlRow .= "<tr><td colspan='6' class='text-center'> No hay horarios disponibles para esta semana.</td></tr>";
            //$fecha=$this->getFechaMayor('2020-12-07');
           // $response['fecha']=$fecha; 
            $response['dataHora']=$htmlRow; 
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        
        return $response;
    }
    public function generarCalendario($request){
        $response['status']=0;
        $response['message']='';
        try{
            $lunes=$request['inicio']; 
            $martes=date("Y-m-d",strtotime($lunes."+ 1 days")); 
            $miercoles=date("Y-m-d",strtotime($martes."+ 1 days")); 
            $jueves=date("Y-m-d",strtotime($miercoles."+ 1 days")); 
            $viernes=date("Y-m-d",strtotime($miercoles."+ 1 days")); 
            $sabado=date("Y-m-d",strtotime($miercoles."+ 1 days")); 
            $domingo=date("Y-m-d",strtotime($miercoles."+ 1 days"));
            $sde=$request['sede'];
            $servicio=$request['servicio'];
            $row=DB::select("CALL acc_generarHorarioSemana($sde,$servicio,'$lunes','$martes','$miercoles','$jueves','$viernes','$sabado','$domingo','".$request['inicio']."','".$request['fin']."')");

            $response['status']=1;
            $response['response']=$row[0];
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
            }
}