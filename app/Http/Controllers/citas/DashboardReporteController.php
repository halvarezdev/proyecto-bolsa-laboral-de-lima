<?php

namespace App\Http\Controllers\citas;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Controles;
class DashboardReporteController extends Controller{

    public function index(){
        $response['status']=0;
		$response['message']='';
        try{
             
            $response['fecha']=date('d/m/Y');
            
           return view('citas/dashboard-reporte-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function rep1(){
        $response['status']=0;
		$response['message']='';
        try{
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);
                foreach ($sede['response'] as $key => $value) {
                    $row_sede[$value->sde_id]=$value->sde_name;
                }
            $response['row_sede']=$row_sede;
            $response['tipoRe']='reporte01';
            $response['wit']='1200';
            $response['titulo']='Reporte de citas reservadas';
            $response['fecha']=date('d/m/Y');
            
           return view('citas/dashboard-reporte1-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function rep2(){
        $response['status']=0;
		$response['message']='';
        try{
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);
                foreach ($sede['response'] as $key => $value) {
                    $row_sede[$value->sde_id]=$value->sde_name;
                }
            $response['row_sede']=$row_sede;
            $response['fecha']=date('d/m/Y');
            $response['titulo']='Reporte de citas atendidas';
            $response['tipoRe']='reporte02';
            $response['wit']='1200';
            
           return view('citas/dashboard-reporte1-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function rep3(){
        $response['status']=0;
		$response['message']='';
        try{
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);
                foreach ($sede['response'] as $key => $value) {
                    $row_sede[$value->sde_id]=$value->sde_name;
                }
            $response['row_sede']=$row_sede;
            $response['fecha']=date('d/m/Y');
            $response['titulo']='Reporte detallado de citas';
            $response['tipoRe']='reporte03';
            $response['wit']='1200';
            
           return view('citas/dashboard-reporte1-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function rep4(){
        $response['status']=0;
		$response['message']='';
        try{
            $row_sede=array('0'=>'-- SELECCIONE --');
            $sede=Controles::get_data_table_simple("SELECT sde_id,sde_name FROM tb_sede 
                WHERE sde_estado=1 ORDER BY sde_orderby ASC");
            if($sede['status']==0)
                throw new \Exception($sede['message']);
                foreach ($sede['response'] as $key => $value) {
                    $row_sede[$value->sde_id]=$value->sde_name;
                }
            $response['row_sede']=$row_sede;
            $response['fecha']=date('d/m/Y');
            $response['titulo']='Estadísticas';
            $response['tipoRe']='reporte04';
            $response['wit']='800';
            
           return view('citas/dashboard-reporte1-view',$response);
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function gruporeporte(Request $request){
        if($request->tipo=='pdf'){
		
			
			echo $this->getBodyHTML($request->landscape,$request->width);
			echo '</td></tr></tbody></table></div></body></html>'; 
        }else{
			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename='.$request->titulo.'.xls');
			header('Pragma: no-cache');
			header('Expires: 0');
			echo "\xEF\xBB\xBF";
		}
		$nombreclase = "\ReporteController::";
		$reporte=$request->method;
		$arrayinf=array('request'=>$request->all());
		 
		return call_user_func_array(__NAMESPACE__.$nombreclase.$reporte,$arrayinf);
		  exit();
    }
    
    public function getBodyHTML($landscape='',$wint=900){
	    $html= '<html><head><meta http-equiv="Content-Type" content="text/html;">
		<meta charset="ISO-8859-1">
		<title>REPORTE</title>
		<link rel="stylesheet" href="'.asset('static/css/bootstrap.css').'">
		<style type="text/css">
		.table tbody tr:nth-child(2n) td {
		  background-color: white;
		  color: rgb(32, 76, 121);
		  font-family: inherit;
		  font-weight: normal;padding: 2px 5px;
		  height: 12px;
		  font-size: 11px;
	  }.table tbody td {
		background-color: #f8f9fa;
		color: rgb(32, 76, 121);
		font-family: inherit;
		font-weight: normal;
		padding: 2px 5px;
		height: 12px;
		font-size: 11px;
	  }
	  .table thead tr td{font-size: 12px;}
	  .tableprima  tr td b{font-size: 12px;}

	  '.($landscape=='landscape'?'@page { size: landscape; }':'').'
		@media screen {  .no_mostrar { display: none; } .no_imprimir { display: block; }   }      
		@media print {   .no_mostrar { display: block; } .no_imprimir { display: none; }    }
		
		</style></head>
		
		<body style="background-color: #dfe4e8;">
		<div align="center">
	 
	    <table border="0" width="'.($wint==''?900:$wint).'" cellspacing="1" cellpadding="2" bgcolor="#0063BE">
	      <tbody><tr>
	        <td bgcolor="#FFFFFF" valign="top">
	        <table border="0" cellpadding="3" cellspacing="3" width="100%">
	            <tbody>
	        <tr>
	          <td align="center" valign="top" bgcolor="#FFFFFF">';       
	     
	         ob_start();
	        echo $html;
	        $initial_content = ob_get_contents();
	        ob_end_clean();
	        return $initial_content;
  	}
}