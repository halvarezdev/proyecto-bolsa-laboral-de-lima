<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Pedido;
use App\Empresa;
use App\Persona;
use App\Conocimiento;
use App\Porfesion;
use App\GradoInstruccion;
use App\Ubigeo;
use App\Maestro;
use App\Area;
use App\Ocupacion;
use App\Sector;
use App\Contacto;
use App\TipoDocumento;
use App\PedidoComputacion;
use App\PedidoIdioma;
use App\FichaPersona;
use App\Entidad;


class ContactoController extends Controller
{

    protected $redirectTo = 'auth.contacto-login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:contacto');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();


      foreach($entidades as $value){
          $data['enti_logo'] = $value->enti_logo;
          $data['entidad'] = $value->id;
          $data['entidad_color'] = $value->color_entidad;
          $data['btn_buscar'] = $value->btn_buscar;
          $data['login_logo_persona'] = $value->login_logo_persona;
          $data['subcolor_entidad'] = $value->subcolor_entidad;
          $data['submenu_color'] = $value->submenu_color;
          $data['btn_gestion'] = $value->btn_gestion;
      }

      $where['pedidos.contacto_id'] = Auth::user()->id;

      $state = Input::get("state");
      $descripcion_input = Input::get('descripcion_input');
      //flag_estado = 2 (pendiente); 0 (finalizado); 1 (activos); 3 (borrador) --hildebrando
      $pedidos = Pedido::select('pedidos.id as id','pedidos.descripcion','pedidos.flag_estado','pedidos.num_vacantes','pedidos.empresa_id','pedidos.created_at as fechcrea_pedido','pedidos.fecha_limite as fechlimit_pedido',
      // DB::raw('(SELECT count(postulantepedido.PP_id) from postulantepedido where postulantepedido.idPedido = pedidos.id) as total_post'),
      DB::raw('(SELECT count(postulantepedido.PP_id) from postulantepedido where postulantepedido.idPedido = pedidos.id and postulantepedido.PP_Visto = "1") as total_vistos'),
      DB::raw('(SELECT count(postulantepedido.PP_id) from postulantepedido where postulantepedido.idPedido = pedidos.id and postulantepedido.PP_Visto = "0") as total_novisto'),
      DB::raw('(SELECT count(postulantepedido.PP_id) from postulantepedido where postulantepedido.idPedido = pedidos.id and postulantepedido.PP_Status = "1") as total_entrevista'),
      DB::raw('(SELECT count(postulantepedido.PP_id) from postulantepedido where postulantepedido.idPedido = pedidos.id and postulantepedido.PostPed_contra = "1") as total_contra'),
      DB::raw('(SELECT count(postulantepedido.PP_id) from postulantepedido where postulantepedido.idPedido = pedidos.id and (postulantepedido.PP_Status = "2" or postulantepedido.PP_Status = "4" or postulantepedido.PP_Status = "1")) as total_entrevista'))
      ->leftJoin('empresas', 'pedidos.empresa_id', '=', 'empresas.id')
      ->where((function($query) use ($state,$descripcion_input){
        if(isset($state) && $state == 2){
          $query->where('pedidos.flag_estado','2');
        }elseif(isset($state) && $state == 0){
          $query->where('pedidos.flag_estado','0');
        }elseif(isset($state) && $state == 3){
          $query->where('pedidos.flag_estado','3');
        }else{
          $query->where('pedidos.flag_estado','1');
        }

        if(isset($descripcion_input) && $descripcion_input != ""){
          $query->where('pedidos.descripcion','like','%'.$descripcion_input.'%');
        }


      }))
      ->where('empresas.id',Auth::user()->empresa_id)
      ->orderBy('pedidos.created_at', 'DESC')
      ->paginate(7);

      $data['descripcion_input'] = $descripcion_input;
      $data['state'] = (isset($state) || $state != "" ? $state : "1");
      $data['pedidos'] = $pedidos;

      return view('contacto-home', $data);
    }

    public function vacante(){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();


      foreach($entidades as $value){
          $data['enti_logo'] = $value->enti_logo;
          $data['entidad'] = $value->id;
          $data['entidad_color'] = $value->color_entidad;
          $data['btn_buscar'] = $value->btn_buscar;
          $data['login_logo_persona'] = $value->login_logo_persona;
          $data['subcolor_entidad'] = $value->subcolor_entidad;
      }
        $empresas = Empresa::where('id',Auth::user()->empresa_id)->get();
        $grado_posgrado = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','3')->get();
        $grado_normal = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','2')->get();
        $grado_intruccion = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','1')->orWhere('grad_flag','2')->orWhere('grad_flag','3')->get();

        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
        $ubigeo = Ubigeo::where($where)->get();
        $computacion = Maestro::where('MAESP_CODIGO1','=','3')->get();
        $idiomas = Maestro::where('MAESP_CODIGO1','=','6')->get();
        $areas = Area::get();
        $ocupacion = Ocupacion::where('status','=','9')->get();
        $sector = Sector::get();
        $accion = "2";
        $pedido = "";
        $data['contactos'] = Contacto::where('empresa_id', Auth::user()->empresa_id)->get();

        $data['empresas']=$empresas;
        $data['grado_intruccion']=$grado_intruccion;
        $data['ubigeo']=$ubigeo;
        $data['computacion']=$computacion;
        $data['idiomas']=$idiomas;
        $data['areas']=$areas;
        $data['ocupacion']=$ocupacion;
        $data['grado_normal']=$grado_normal;
        $data['sector']=$sector;
        $data['accion']=$accion;
        $data['grado_posgrado'] = $grado_posgrado;
        $data['tipo_entidad'] = '1';
        $data['documentos'] = TipoDocumento::orWhere('id', '=', '2')
                ->orWhere('id', '=', '5')
                ->orWhere('id', '=', '6')
                ->orderBy('id', 'desc')
                ->get();


        return view('empresa/registro-vacante', $data);
    }

    public function guardarEmpresa(Request $request){

        $pedido = Pedido::create([
            'descripcion' => $request->descripcion,
            'denominacion' => $request->denominacion,
            'num_vacantes' => $request->num_vacantes,
            'area' => $request->area,
            'estudios_formales' => $request->estudios_formales,
          //  'estudios_postgrado' => $request->estudios_postgrado,
          //  'grado' => $request->grado,
            'otro_conocimiento' => $request->otro_conocimiento,
            'experiencia_laboral' => $request->experiencia_laboral,
            'experiencia_nivel' => $request->experiencia_nivel,
            'licencia_arma' => $request->licencia_arma,
            'zona' => $request->zona,
            'sexo' => $request->sexo,
            'edad' => $request->edad,
            'edad_total' => $request->edad_total,
            'contacto' => $request->contacto,
            'tareas' => $request->tareas,
            'personal_cargo' => $request->personal_cargo,
            'cantidad' => (isset($request->cantidad) ? $request->cantidad : "0"),
            'modalidades' => $request->modalidades,
           // 'horario_trabajo' => $request->horario_trabajo,
            'turno_trabajo' => $request->turno_trabajo,
            'horario' => (isset($request->horario) ? $request->horario : "0"),
            'remuneracion' => $request->remuneracion,
            'moneda' => $request->moneda,
            'ubigeo' => $request->departamento.$request->provincia.$request->distrito,
            'direccion_trabajo' => $request->direccion_trabajo,
            'horas_extras' => (isset($request->horas_extras) ? $request->horas_extras : "0"),
            'movilidad' => (isset($request->movilidad) ? $request->movilidad : "0"),
            'refrigerio' => (isset($request->refrigerio) ? $request->refrigerio : "0"),
            'bonificacion' => (isset($request->bonificacion) ? $request->bonificacion : "0"),
            'comisiones' => (isset($request->comisiones) ? $request->comisiones : "0"),
            'discapacidad' => (isset($request->discapacidad) ? $request->discapacidad : "0"),
            'radio_discapacidad' => (isset($request->radio_discapacidad) ? $request->radio_discapacidad : "0"),
            'obersavasiones' => (isset($request->obersavasiones) ? $request->obersavasiones : ""),
            'licencia' => (isset($request->licencia) ? $request->licencia : "0"),
            'categoria' => (isset($request->categoria) ? $request->categoria : "0"),
            'cuenta_carro' => (isset($request->cuenta_carro) ? $request->cuenta_carro : "0"),
            'fecha_limite' => (isset($request->fecha_limite) ? $request->fecha_limite : "0"),
            'tipo_carro' => (isset($request->tipo_carro) ? $request->tipo_carro : "0"),
            'empresa_id' => Auth::user()->empresa_id,
            'contacto_id' => Auth::user()->id,
            'flag_estado' => "2",
            'fich_estado' => (isset($request->estado) ? $request->estado : "0"),
            'tipo_entidad' => '1',
            'cod_entidad' => '1',
            'grado' => $request->grado,
        ]);

            if( isset($request->compu1) && $request->compu1 != ""){
              $compu_sql = PedidoComputacion::create([
                'compu_nom' => $request->compu1,
                'compu_nivel' => $request->nivel_compu1,
                'pedido_id' => $pedido->id,
              ]);
            }
            if(isset($request->compu2) && $request->compu2 != ""){
             $compu_sql = PedidoComputacion::create([
                  'compu_nom' => $request->compu2,
                  'compu_nivel' => $request->nivel_compu2,
                  'pedido_id' => $pedido->id,
              ]);
           }
           if(isset($request->compu2) && $request->compu2 != ""){
            $compu_sql = PedidoComputacion::create([
                'compu_nom' => $request->compu3,
                'compu_nivel' => $request->nivel_compu3,
                'pedido_id' => $pedido->id,
            ]);
            }
            if(isset($request->idioma1) && $request->idioma1 != ""){
            $idioma = PedidoIdioma::create([
                'idioma_nom' => $request->idioma1,
                'idioma_nivel' => $request->nivel_idioma1,
                'pedido_id' => $pedido->id,
            ]);
          }
          if(isset($request->idioma2) && $request->idioma2 != ""){
             $idioma = PedidoIdioma::create([
                'idioma_nom' => $request->idioma2,
                'idioma_nivel' => $request->nivel_idioma2,
                'pedido_id' => $pedido->id,
            ]);
           }
           if(isset($request->idioma3) && $request->idioma3 != ""){
              $idioma = PedidoIdioma::create([
                'idioma_nom' => $request->idioma3,
                'idioma_nivel' => $request->nivel_idioma3,
                'pedido_id' => $pedido->id,
            ]);
             }

       return redirect()->action('ContactoController@index');


    }

    public function BuscaPostulante(){

        return view('empresa/busqueda-Postulante');

    }

    public function editarInforamcion(Request $request){

         $empresa = Empresa::where('id', Auth::user()->empresa_id)
          ->update(
            [
              'direccion' => $request->direccion,
              'sitio_web' => $request->sitioweb,
              'rubro' => $request->rubro,
              'descripcion' => $request->descripcion,
              'emp_urlvideo' => $request->video,
          ]); 

    return redirect()->action('ContactoController@listaContacto');

    }

    public function listaContacto(){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();


      foreach($entidades as $value){
          $data['enti_logo'] = $value->enti_logo;
          $data['entidad'] = $value->id;
          $data['entidad_color'] = $value->color_entidad;
          $data['btn_buscar'] = $value->btn_buscar;
          $data['login_logo_persona'] = $value->login_logo_persona;
          $data['subcolor_entidad'] = $value->subcolor_entidad;
          $data['submenu_color'] = $value->submenu_color;
          $data['btn_gestion'] = $value->btn_gestion;
      }
        $documentos = TipoDocumento::orWhere('id', '=', '2')
                ->orWhere('id', '=', '5')
                ->orWhere('id', '=', '6')
                ->orderBy('id', 'desc')
                ->get();
               
        $empresa = Empresa::select('emp_urlvideo','numero_documento','razon_social','descripcion','sitio_web','sec_descripcion',
        'direccion','perfil_foto','rubro','banner_foto')
        ->where('empresas.id',Auth::user()->empresa_id)
        ->leftjoin('secciones', 'secciones.id', '=', 'empresas.rubro')
        ->get();
        
        
        $lista_contacto = Contacto::where('empresa_id',Auth::user()->empresa_id)->get();
        $ruta_foto='';
        $ruta_fot = "";
        foreach($empresa as $emp){
          $ruta_foto = $emp->perfil_foto;
        }

        $data['ruta_foto'] = $ruta_foto;
        $data['lista_contacto'] = $lista_contacto;
        $data['documentos'] = $documentos;
        $data['empresa'] = $empresa;
        $data['sector'] = Sector::get();

        return view('empresa/lista-contacto', $data);

    }

    public function nuevoContacto(Request $request){

     /*    $this->validate(request(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:contactos'],
            'numero_documento' => ['required', 'unique:contactos'],
		]);

		if( $validator->fails() ) {
            //  return $validator->messages()->first();
             // return redirect()->back($validator->messages()->first());
              return redirect()->back()->withInput()->withErrors($validator);
		  }
      */

        $contacto = Contacto::create([
            'numero_documento' =>$request->numero_documento,
            'tipo_documento' => $request->tipo_documento_contacto,
            'telefono' => $request->telefono,
            'name' => $request->nombre,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'cargo' => $request->cargo,
            'empresa_id' => Auth::user()->empresa_id,
            'cod_entidad' => Auth::user()->cod_entidad,
        ]);

        return redirect()->action('ContactoController@listaContacto');


    }
    public function nuevoContactoJson(Request $request){

         $contacto = Contacto::create([
             'numero_documento' =>$request->numero_contacto,
             'tipo_documento' => $request->documento_contacto,
             'telefono' => $request->telefono_contacto,
             'name' => $request->nombre_contacto,
             'email' => $request->email_contacto,
             'password' => Hash::make($request->password_contacto),
             'cargo' => $request->cargo_contacto,
             'empresa_id' => Auth::user()->empresa_id,
             'cod_entidad' => Auth::user()->cod_entidad,
         ]);
          $value ="1";
         return json_encode($value,JSON_PRETTY_PRINT);


     }
public function listaNuevoContactos(){

  $value = Contacto::where('empresa_id', Auth::user()->empresa_id)->get();


  return json_encode($value,JSON_PRETTY_PRINT);
}

public function SubirForo(Request $request){


     if($request->hasFile('file')){

      $ruta = $request->file('file')->store('empresa');
       $uardado= Empresa::where('id', Auth::user()->empresa_id)
          ->update(['perfil_foto' => $ruta]);

      $file = $request->file('file');
      $name = time().$file->getClientOriginalName();
      $file->move(public_path().'/storage/empresa/', $ruta);
     }


          $mensaje = "Se subio correctamente el archivo seleccionado";

       return redirect()->action('ContactoController@listaContacto');


}

public function SubirBanner(Request $request){


  if($request->hasFile('file_banner')){

   $ruta = $request->file('file_banner')->store('empresa');
    $uardado= Empresa::where('id', Auth::user()->empresa_id)
       ->update(['banner_foto' => $ruta]);

   $file = $request->file('file_banner');
   $name = time().$file->getClientOriginalName();
   $file->move(public_path().'/storage/empresa/', $ruta);
  }


       $mensaje = "Se subio correctamente el archivo seleccionado";

    return redirect()->action('ContactoController@listaContacto');


}

  public function downloadFile($id){
    $ficha = FichaPersona::where('id',$id)->get();

    foreach ($ficha as $key => $value) {
            $pathtoFile = public_path().'/storage/'.$value->filecv;
            return  response()->download($pathtoFile);
    }
     // echo "vino";

  }

public function editarVacante($id){

  $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();


      foreach($entidades as $value){
          $data['enti_logo'] = $value->enti_logo;
          $data['entidad'] = $value->id;
          $data['entidad_color'] = $value->color_entidad;
          $data['btn_buscar'] = $value->btn_buscar;
          $data['login_logo_persona'] = $value->login_logo_persona;
          $data['subcolor_entidad'] = $value->subcolor_entidad;
          $data['submenu_color'] = $value->submenu_color;
          $data['btn_gestion'] = $value->btn_gestion;
      }
      $data['tipo_entidad'] = '1';
  $pedido_sql = Pedido::select(DB::raw("*,DATE_FORMAT(fecha_limite, '%Y-%m-%d') as fecha_limite"))->where('pedidos.id', $id)->get();
                 
   $compu_sql = PedidoComputacion::where('pedido_id', $id)->get();
   $idioma_sql = PedidoIdioma::where('pedido_id', $id)->get();


  $empresas = Empresa::where('id',Auth::user()->empresa_id)->get();
  $grado_posgrado = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','3')->get();
  $grado_normal = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','2')->get();
  $grado_intruccion = GradoInstruccion::orWhere('grad_flag','0')->orWhere('grad_flag','1')->orWhere('grad_flag','2')->orWhere('grad_flag','3')->get();
  $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
  $ubigeo = Ubigeo::where($where)->get();
  $computacion = Maestro::where('MAESP_CODIGO1','=','3')->get();
  $idiomas = Maestro::where('MAESP_CODIGO1','=','6')->get();
  $areas = Area::get();
  $ocupacion = Ocupacion::where('status','=','9')->get();
  $sector = Sector::get();
  $accion = "1";
  $arrayResul=array();

  foreach ($pedido_sql as $key => $value) {
    $arrayResul= $value;
  }
    $where_prov = ['ubi_coddpto' => substr($arrayResul->ubigeo,0,2) , 'ubi_coddist' => '00'];
  $ubigeo_prov = Ubigeo::where($where_prov)->where('ubi_codprov','!=','00')->get();
  $where_dist = ['ubi_coddpto' => substr($arrayResul->ubigeo,0,2) , 'ubi_codprov' => substr($arrayResul->ubigeo,2,2)];
  $ubigeo_dis = Ubigeo::where($where_dist)->where('ubi_coddist','!=','00')->get();
  $data['request_db']=$arrayResul;
  $data['ubigeo_prov'] = $ubigeo_prov;
  $data['ubigeo_dis'] = $ubigeo_dis;
  $data['compu_sql'] = $compu_sql;
  $data['idioma_sql'] = $idioma_sql;
  $data['empresas']=$empresas;
  $data['grado_intruccion']=$grado_intruccion;
  $data['ubigeo']=$ubigeo;
  $data['computacion']=$computacion;
  $data['idiomas']=$idiomas;
  $data['areas']=$areas;
  $data['ocupacion']=$ocupacion;
  $data['grado_normal']=$grado_normal;
  $data['sector']=$sector;
  $data['accion']=$accion;
  $data['grado_posgrado'] = $grado_posgrado;
  $data['documentos'] = TipoDocumento::orWhere('id', '=', '2')
  ->orWhere('id', '=', '5')
  ->orWhere('id', '=', '6')
  ->orderBy('id', 'desc')
  ->get();

  $data['contactos'] = Contacto::where('empresa_id', Auth::user()->empresa_id)->get();

  return view('empresa/registro-vacante', $data);

}


  public function editaVacanteForm(Request $request){

  $empresa = Empresa::where('id', Auth::user()->empresa_id)
          ->update(
            [
              'direccion' => $request->direccion,
              'sitio_web' => $request->sitioweb,

          ]);

  $pedido = Pedido::where('id', $request->codigo)
          ->update([
            'licencia_arma' => $request->licencia_arma,
            'descripcion' => $request->descripcion,
            'denominacion' => $request->denominacion,
            'num_vacantes' => $request->num_vacantes,
            'area' => $request->area,
            'estudios_formales' => $request->estudios_formales,
            'grado' => $request->grado,
            'estudios_postgrado' => $request->estudios_postgrado,
            'otro_conocimiento' => $request->otro_conocimiento,
            'experiencia_laboral' => $request->experiencia_laboral,
            'experiencia_nivel' => $request->experiencia_nivel,
            'zona' => $request->zona,
            'sexo' => $request->sexo,
            'edad' => $request->edad,
            'edad_total' => $request->edad_total,
            'contacto' => $request->contacto,
            'tareas' => $request->tareas,
            'personal_cargo' => $request->personal_cargo,
            'cantidad' => (isset($request->cantidad) ? $request->cantidad : "0"),
            'modalidades' => $request->modalidades,
          //  'horario_trabajo' => $request->horario_trabajo,
            'turno_trabajo' => $request->turno_trabajo,
            'horario' => (isset($request->horario) ? $request->horario : "0"),
            'remuneracion' => $request->remuneracion,
            'moneda' => $request->moneda,
            'ubigeo' => $request->departamento.$request->provincia.$request->distrito,
            'direccion_trabajo' => $request->direccion_trabajo,
            'horas_extras' => (isset($request->horas_extras) ? $request->horas_extras : "0"),
            'movilidad' => (isset($request->movilidad) ? $request->movilidad : "0"),
            'refrigerio' => (isset($request->refrigerio) ? $request->refrigerio : "0"),
            'bonificacion' => (isset($request->bonificacion) ? $request->bonificacion : "0"),
            'comisiones' => (isset($request->comisiones) ? $request->comisiones : "0"),
            'discapacidad' => (isset($request->discapacidad) ? $request->discapacidad : "0"),
            'radio_discapacidad' => (isset($request->radio_discapacidad) ? $request->radio_discapacidad : "0"),
            'obersavasiones' => (isset($request->obersavasiones) ? $request->obersavasiones : ""),
            'licencia' => (isset($request->licencia) ? $request->licencia : "0"),
            'categoria' => (isset($request->categoria) ? $request->categoria : "0"),
            'cuenta_carro' => (isset($request->cuenta_carro) ? $request->cuenta_carro : "0"),
            'fecha_limite' => (isset($request->fecha_limite) ? $request->fecha_limite : "0"),
            'tipo_carro' => (isset($request->tipo_carro) ? $request->tipo_carro : "0"),
            'empresa_id' => Auth::user()->empresa_id,
            'contacto_id' => Auth::user()->id,
            'flag_estado' => "2",
            'grado' => $request->grado,
        ]);

        $valida_computacion1 = PedidoComputacion::where('id',$request->cod_compu1)->get();

        if(count($valida_computacion1) > 0){

            if( isset($request->compu1) && $request->compu1 != ""){

              $compu_sql = PedidoComputacion::where('id',$request->cod_compu1)
              ->update([
                'compu_nom' => $request->compu1,
                'compu_nivel' => $request->nivel_compu1,
              ]);
              }

        }else{

          if( isset($request->compu1) && $request->compu1 != ""){
            $compu_sql = PedidoComputacion::create([
              'compu_nom' => $request->compu1,
              'compu_nivel' => $request->nivel_compu1,
              'pedido_id' => $request->codigo,
            ]);
          }


        }

      $valida_computacion2 = PedidoComputacion::where('id',$request->cod_compu2)->get();

     if(count($valida_computacion2) > 0){

      if(isset($request->compu2) && $request->compu2 != ""){
        $compu_sql = PedidoComputacion::where('id', $request->cod_compu2)
        ->update([
              'compu_nom' => $request->compu2,
              'compu_nivel' => $request->nivel_compu2,
          ]);
      }

     } else{
      if(isset($request->compu2) && $request->compu2 != ""){
        $compu_sql = PedidoComputacion::create([
            'compu_nom' => $request->compu2,
            'compu_nivel' => $request->nivel_compu2,
            'pedido_id' => $request->codigo,
        ]);
        }
     }

     $valida_computacion3 = PedidoComputacion::where('id',$request->cod_compu3)->get();

     if(count($valida_computacion3) > 0){

      if(isset($request->compu3) && $request->compu3 != ""){
        $compu_sql = PedidoComputacion::where('id', $request->cod_compu3)
        ->update([
              'compu_nom' => $request->compu3,
              'compu_nivel' => $request->nivel_compu3,
          ]);
      }

     } else{
      if(isset($request->compu3) && $request->compu3 != ""){
        $compu_sql = PedidoComputacion::create([
            'compu_nom' => $request->compu3,
            'compu_nivel' => $request->nivel_compu3,
            'pedido_id' => $request->codigo,
        ]);
        }
     }

     $validar_idioma1 = PedidoIdioma::where('id',$request->cod_idioma1)->get();

     if(count($validar_idioma1) > 0){
      if(isset($request->idioma1) && $request->idioma1 != ""){
        $idioma = PedidoIdioma::where('id', $request->cod_idioma1)
        ->update([
            'idioma_nom' => $request->idioma1,
            'idioma_nivel' => $request->nivel_idioma1,
        ]);
      }

    }else{
      if(isset($request->idioma1) && $request->idioma1 != ""){
        $idioma = PedidoIdioma::create([
            'idioma_nom' => $request->idioma1,
            'idioma_nivel' => $request->nivel_idioma1,
            'pedido_id' => $request->codigo,
            ]);
      }
    }

    $validar_idioma2 = PedidoIdioma::where('id',$request->cod_idioma2)->get();

    if(count($validar_idioma2) > 0){
      if(isset($request->idioma2) && $request->idioma2 != ""){
        $idioma = PedidoIdioma::where('id', $request->cod_idioma2)
          ->update([
            'idioma_nom' => $request->idioma2,
            'idioma_nivel' => $request->nivel_idioma2,
          ]);
      }

    }else{
      if(isset($request->idioma2) && $request->idioma2 != ""){
        $idioma = PedidoIdioma::create([
            'idioma_nom' => $request->idioma2,
            'idioma_nivel' => $request->nivel_idioma2,
            'pedido_id' => $request->codigo,
        ]);
      }
    }


    $validar_idioma3 = PedidoIdioma::where('id',$request->cod_idioma3)->get();

    if(count($validar_idioma3) > 0){
      if(isset($request->idioma3) && $request->idioma3 != ""){
        $idioma = PedidoIdioma::where('id', $request->cod_idioma3)
        ->update([
            'idioma_nom' => $request->idioma3,
            'idioma_nivel' => $request->nivel_idioma3,
        ]);
      }
    }else{
      if(isset($request->idioma3) && $request->idioma3 != ""){
        $idioma = PedidoIdioma::create([
            'idioma_nom' => $request->idioma3,
            'idioma_nivel' => $request->nivel_idioma3,
            'pedido_id' => $request->codigo,
          ]);
      }
    }
       // echo "dd";
   return redirect()->action('ContactoController@index');
  }

    public function cerrarPedido(Request $request){
      $hoy = date('Y-m-d');
      $idioma = Pedido::where('id', $request->codigo)
              ->update([
                'motivo_cierre' => $request->motivocierre,
                'fecha_cierre' => $hoy,
                'flag_estado' => '0',
             ]);
       return redirect()->action('ContactoController@index');
    }

  public function empresaServicios($entidad){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();

      foreach($entidades as $value){
          $data['enti_logo'] = $value->enti_logo;
          $data['entidad'] = $value->id;
          $data['entidad_color'] = $value->color_entidad;
          $data['btn_buscar'] = $value->btn_buscar;
          $data['login_logo_persona'] = $value->login_logo_persona;
          $data['subcolor_entidad'] = $value->subcolor_entidad;
          $data['submenu_color'] = $value->submenu_color;
          $data['btn_gestion'] = $value->btn_gestion;
      }

      return view('empresa/servicios', $data);

  }

	public function validaNumeroDocumento($num_doc){

		$num = Contacto::select('numero_documento')->where('numero_documento',$num_doc)->get();

		$value = "0";
		if(count($num) > 0){
			$value = "1";
		}

		return json_encode($value,JSON_PRETTY_PRINT);
	}

  public function validaCorreoElectronico($correo){

    $correo = Contacto::select('email')->where('email',$correo)->get();
    $value = "0";
    if(count($correo) > 0){
      $value = "1";
    }
    return json_encode($value,JSON_PRETTY_PRINT);

  }


  public function eliminaContacto($id){

    $deletedRows = Contacto::where('id', $id)->delete();

    return redirect()->action('ContactoController@listaContacto');

  }

  public function editContacto($id){

    $lista_contacto = Contacto::select('tipo_documento','numero_documento','name','email','telefono','cargo')
    ->where('id',$id)
    ->get();
    return json_encode($lista_contacto,JSON_PRETTY_PRINT);

  }

  public function editarFormularioContacto(Request $request){

    $contacto = Contacto::where('id', $request->codigo_contacto)
    ->where('empresa_id',Auth::user()->empresa_id)
    ->update([
      'tipo_documento' => $request->tipo_documento_contacto,
      'numero_documento' => $request->numero_documento,
      'name' => $request->nombre,
      'cargo' => $request->cargo,
      'telefono' => $request->telefono,
    ]);

    return redirect()->action('ContactoController@listaContacto');

  }

}
