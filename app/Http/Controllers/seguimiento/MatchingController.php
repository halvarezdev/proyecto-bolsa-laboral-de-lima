<?php

namespace App\Http\Controllers\seguimiento;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\control\UbigeoLista;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\FormacionSuperior;
use App\PedidoPostulante;
use App\GradoInstruccion;
use App\Http\Requests;
use App\Experiencia;
use App\Notificacion;
use App\EstadoCivil;
use App\Ocupacion;
use App\Profesion;
use App\Maestro;
use App\Persona;
use App\Entidad;
use App\Empresa;
use App\Mail\EnvioCita;
use Mail;
use Carbon\Carbon;

class MatchingController extends Controller{

    protected $redirectTo = 'auth.contacto-login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:contacto');
      //  $this->middleware('auth:entidad');
    }

    public function Inicio($id,Request $request){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();



      foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['btn_buscar'] = $value->btn_buscar;
        $data['login_logo_persona'] = $value->login_logo_persona;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['submenu_color'] = $value->submenu_color;
        $data['icon1'] = $value->icon1;
        $data['icon2'] = $value->icon2;
        $data['icon3'] = $value->icon3;
        $data['icon4'] = $value->icon4;
        $data['icon5'] = $value->icon5;
        $data['btn_apto'] = $value->btn_apto;
        $data['btn_citado'] = $value->btn_citado;
        $data['btn_contratado'] = $value->btn_contratado;
      }

    	$resultado=array();
    	$data['request']=$request->all();
    	$sql1='SELECT DATE_FORMAT(p.fecha_limite,"%d/%m/%Y") AS fecha_limite, ped_InterColcCie, DATE_FORMAT(p.created_at,"%d/%m/%Y") AS fecha , p.num_vacantes, numero_documento, p.id as id_pedido,p.descripcion des_pedido, razon_social,e.descripcion,numero_documento FROM pedidos p INNER JOIN empresas e ON  e.id=p.empresa_id  WHERE p.empresa_id='."'".$request->empresa."' AND p.id=".$id;
    	$Lista1=DB::select($sql1);
    	if (count($Lista1)>0) {
	      foreach ($Lista1 as $key => $value) {
	      	$resultado=$value;
	      }
  		}

      $data['db_response']=$resultado;
  		$data['_id']=$id;
      $data['response_db']=$resultado=$this->getPostulantePedido($id,$request->all());
   		return response()->view('seguimiento/Seguimiento_pedido',$data);
    }

    public function search_postulante($pedido){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();

      foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['btn_buscar'] = $value->btn_buscar;
        $data['login_logo_persona'] = $value->login_logo_persona;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['submenu_color'] = $value->submenu_color;
      }
      $data['menu'] = "1";
      // lista de maestros para el buscador
      $data['lista_pais']=UbigeoLista::getPaisSelectOption();
    	$data['lista_depa']=UbigeoLista::getDepartSelectOption();
    	$data['lista_prov']=UbigeoLista::getProviSelectOption('15');
      $data['lista_dist']=UbigeoLista::getDistritoSelectOption('15','01');
      $data['computacion'] = Maestro::where('MAESP_CODIGO1','=','3')->get();
      $data['idiomas'] = Maestro::where('MAESP_CODIGO1','=','6')->get();
      $data['ocupaciones'] = Ocupacion::where('status', '=', '9')->get();
      $data['profesiones'] = Profesion::where('ggrupo', '!=', 'x')->get();
      $data['grado_intruccion'] = GradoInstruccion::orWhere('grad_flag','1')->orWhere('grad_flag','2')->orWhere('grad_flag','3')->get();

    #/* buesqueda por eloquent */

    $list_query = Persona::select('fichapersona.filecv','personas.pers_edad','fichapersona.foto_post', 'personas.id as id_persona','fichapersona.id as ficha_id','personas.name','personas.email','personas.telefono','personas.sexo','personas.fecha_nacimiento');
    $list_query=$list_query->Join('fichapersona', 'fichapersona.persona_id', '=', 'personas.id');
    $list_query=$list_query->where('fichapersona.fic_estado' , '=', '1')
    ->orderByRaw( "FIELD(personas.cod_entidad, ".(isset(Auth::user()->entidad_id) ? Auth::user()->entidad_id : 1)." ) DESC")
    ->paginate(15);

  $tbl = '';
  if (count($list_query)) {
    foreach ($list_query as $key => $value) {
      $tbl .= '<div class="card" >';
        $tbl .= '<div class="card-body">';
          $tbl .= '<div class="row">';

            $tbl .= '<div class="col-2 col-sm-2 col-md-1" style="max-width: 0;">';
              $tbl .= '<div class="custom-control custom-checkbox">';
                $tbl .= '<input type="checkbox" class="custom-control-input" id="todo'.$value->id_persona.'" name="todo'.$value->id_persona.'" value="1" onclick="return guardarPostulante('.$value->ficha_id.','.$value->id_persona.',this)">';
                $tbl .= '<label class="custom-control-label subtitulo-shearh" for="todo'.$value->id_persona.'"></label>';
              $tbl .= '</div>';
            $tbl .= '</div>';

      $tbl .='<div class="col-10 col-sm-10 col-md-2">';
      $tbl .='<img src="'.asset("/public/storage/".$value->foto_post).'" alt="..."  style="width:100px; height:100px;">';
      $tbl .='</div>';

      $tbl .='<div class="col-12 col-sm-12 col-md-2">';
      $tbl .='<span style="color:'.$data['entidad_color'].'; font-size:14px;">'.$value->name.'</span><br>';
      $tbl .='<p style="font-size:13px;line-height: 140%;">'.$value->ubi_descripcion.'<br>'.($value->sexo == "2" ? "Femenino" : "Masculino")."&nbsp;&nbsp;".$value->pers_edad.' Años<br>'.$value->email.'<br>'.$value->telefono.'<br>';
      $tbl .='</p>';
      $tbl .='</div>';

      $tbl .='<div class="col-12 col-sm-12 col-md-3" >';

      $list_experiencia= Experiencia::select('ocupaciones.nombre as nombre_ocupacion','pers_experiencia.nom_empresa','fech_fin','fech_inicio','pers_experiencia.id')
      ->leftJoin('ocupaciones', 'ocupaciones.ocup_codigo','pers_experiencia.id_ocupacion')
      ->where('pers_experiencia.fichper_id','=',$value->ficha_id)
      ->where('pers_experiencia.pers_id','=',$value->id_persona)
      ->get();

      if(count($list_experiencia)){

          $tbl .= '<div id="carousel_experiencia" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';

        $tbl .= '<div class="carousel-inner" style="height: 70%;">';
          foreach($list_experiencia as $i => $value_exp){
            $total = $i +1;
            $active =  ($i == '0' ? "active" : "");
           $tbl .=' <div class="carousel-item '.$active.'">
            <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_ocupacion.'</span><br>
              <p style="font-size:13px;line-height: 140%;">'.$value_exp->nom_empresa.'<br>
              '.$value_exp->fech_inicio.'-/-'.$value_exp->fech_fin.'<br></p>


            </div>';
          }

        $tbl .='</div>
        <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          <li class="page-item" >
            <a class="page-link" href="#carousel_experiencia" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item" >
            <a class="page-link" href="#carousel_experiencia" role="button" data-slide="next" style="padding: .1rem .75rem;">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>';
         $tbl .= ' </div>';
      }


      $tbl .='</div>';

      $tbl .='<div class="col-12 col-sm-12 col-md-3">';

      $list_eucacion= FormacionSuperior::select('grad_descripcion as nombre_grado','profesionales.nombre as nombre_prefecion','per_superior.otro_centro','fecha_fin','fecha_inicio')
      ->leftJoin('profesionales', 'profesionales.profp_codigo','per_superior.especialidad_id')
      ->leftJoin('gradoinstruccion', 'gradoinstruccion.id','per_superior.nivel_id')
      ->where('per_superior.fichper_id','=',$value->ficha_id)
      ->where('per_superior.pers_id','=',$value->id_persona)
      ->get();

      if(count($list_eucacion)){

          $tbl .= '<div id="carousel_education" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';
          $tbl .= ' <ol class="carousel-indicators">
          <li data-target="#carousel_education" data-slide-to="0" class="active"></li>
          <li data-target="#carousel_education" data-slide-to="1"></li>
          <li data-target="#carousel_education" data-slide-to="2"></li>
        </ol>';
          $tbl .= '<div class="carousel-inner" style="height: 70%;">';
          foreach($list_eucacion as $i => $value_exp){
            $active =  ($i == '0' ? "active" : "");
           $tbl .=' <div class="carousel-item '.$active.'" >
            <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_prefecion.'</span><br>
              <p style="font-size:13px;line-height: 140%;">'.$value_exp->otro_centro.'<br>'.$value_exp->nombre_grado.'<br>
              '.$value_exp->fech_inicio.'-/-'.$value_exp->fecha_fin.'<br></p>
            </div>';
          }

        $tbl .='</div>';
      //   <nav aria-label="Page navigation example">
      //   <ul class="pagination justify-content-end">
      //     <li class="page-item" >
      //       <a class="page-link" href="#carousel_education" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
      //         <span aria-hidden="true">&laquo;</span>
      //         <span class="sr-only">Previous</span>
      //       </a>
      //     </li>
      //     <li class="page-item" onclick="return cambiarmasexp('.$value_exp->id.');">
      //       <a class="page-link" href="#carousel_education" role="button" data-slide="next" style="padding: .1rem .75rem;">
      //         <span aria-hidden="true">&raquo;</span>
      //         <span class="sr-only">Next</span>
      //       </a>
      //     </li>
      //   </ul>
      // </nav>';

      $tbl .='</div>';

      }

      $tbl .='</div>';

      $tbl .='<div class="col-12 col-sm-12 col-md-1" >';
      $tbl .= '<div class="btn-group">
      <a  href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="'.asset("images/icono_proceso.png").'">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a href="#" onclick="DescargarHojavida(this,2,'.$value->ficha_id.','.$value->id_persona.','.$pedido.',2)" class="dropdown-item" style="font-size:14px">Ver CV</a>';
        if(isset($value->filecv) && $value->filecv != "" && $value->filecv != null){
          $tbl .= '<a href="'.route("download.hojavida",["id" => $value->ficha_id]).'" class="dropdown-item" style="font-size:14px"><i class="fas fa-check-square"></i>&nbsp;&nbsp;Descargar CV Adjunto</a>';
        //  $tbl .="<td style='font-size:13px;'><a  href='".route("download.hojavida",["id" => $rst->ficha_id])."' class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
        }else{
          $tbl .= '<a href="#" onclick="return errorCv()" class="dropdown-item" style="font-size:14px; ">Descargar CV Adjunto</a>';
        }

        $tbl .= '<a href="#" onclick="return agregarPostulante('.$value->ficha_id.','.$value->id_persona.')" class="dropdown-item" style="font-size:14px">Vincular Postulante al proceso</a>
        <a href="#" class="dropdown-item" style="font-size:14px">Compartir CV por Email</a>
      </div>
    </div>';
      $tbl .='</div>';

      $tbl .='</div>';

      $tbl .='</div>';
      $tbl .='</div>';

    }
  }

  $tbl .= '  <div class="row justify-content-md-center">
  <div class="col-sm-6">
      '.$list_query->links().'
  </div>
</div>';

  ob_start();
  echo $tbl;
  $initial_content = ob_get_contents();
  ob_end_clean();

      $data['tbl'] = $tbl;
      $data['pedido'] = $pedido;
      $data['contacto'] = Auth::user()->id;
      $data['empresa'] = Auth::user()->empresa_id;

   	return view('seguimiento/Seguimiento_pedidoSearch',$data);
    }

    public function getPostulante(){

      $ocupacion = Input::get('ocupacion');
      $edad_desde = Input::get('edad_desde');
      $edad_hasta = Input::get('edad_hasta');
      $sexo = Input::get('sexo');
      $pais = Input::get('pais');
      $deparme = Input::get('deparme');
      $provi = Input::get('provi');
      $distri = Input::get('distri');
      $titulo_puesto = Input::get('titulo_puesto');
      $empresa = Input::get('empresa');
      $area = Input::get('area');
      $tareas = Input::get('tareas');
      $salario = Input::get('salario');
      $institucion = Input::get('institucion');
      $profesion = Input::get('profesion');
      $gradosAcademicos = Input::get('gradosAcademicos');
      $estado = Input::get('estado');
      $idioma1 = Input::get('idioma1');
      $compu = Input::get('compu');
      $nombre = Input::get('nombre');
      $apellido = Input::get('apellido');
      $documento = Input::get('documento');
      $licenciaConduc = Input::get('licenciaConduc');
      $categoria = Input::get('categoria');
      $port_armas = Input::get('port_armas');
      $arma_propia = Input::get('arma_propia');
      $ultima_act = Input::get('ultima_act');
      $discapcidad = Input::get('discapcidad');
      $cv_visible = Input::get('cv_visible');



      $data['request']=$request->all();
      $data['_id']=$id;
      $data['response_db']=$resultado=$this->RegistroPostulante($request->all());
      return response()->view('seguimiento/Seguimiento_pedidoPostulante',$data);


    }
    public function editar($id,Request $request){
      $arrayResul=array();
      $data['request']=$request->all();
      $data['_id']=$id;
      $where='PP.idPedido='.$request->idPedido.' AND PP.idFicha='.$request->ficha_id.' AND PP.idPostulante='.$request->id_persona.' AND PP.PP_id='.$request->PP_id;
      $sql='SELECT DATE_FORMAT(PP_Fecha,"%d/%m/%Y") AS fecha ,DATE_FORMAT(PP_FechaEnvio,"%d") AS fech_envio  ,DATE_FORMAT(PP_FechaColocacion,"%d") AS dia_coloc,PP.PP_id,p.id as id_persona,FP.id as ficha_id,p.name,p.ape_mat,p.ape_pat,p.numero_documento FROM postulantepedido PP INNER JOIN personas p on p.id=PP.idPostulante INNER JOIN fichapersona FP ON FP.id=PP.idFicha WHERE '.$where;

      $Lista =DB::select($sql);
      if (count($Lista)>0) {
        foreach ($Lista as $key => $value) {
        $arrayResul= $value;
        }
      }
      $data['request_db']=$arrayResul;
      return response()->view('seguimiento/Editar_pedidoPostulante',$data);
    }
    public function video($id,Request $request){
      $data['video']='';
      $sql="SELECT url_cv FROM fichapersona WHERE id='$id'";
      $Lista=DB::select($sql);
      if(count($Lista)>0){
        $data['video']=$Lista[0]->url_cv; 
      }
      return $data;
    }
    public function actualizarColocacion($id,Request $request){
      $arrayResul=array();
      $data['request']=$request->all();
      $data['_id']=$id;
      $fecha=date("Y-m");
       $hora=date("H:i:s");
      $FechaEnvio=$request->FechaEnvio;
      $setup=array();
      $FechaColocacion=$request->FechaColocacion;
      if ($FechaEnvio!='') {
        if ($FechaEnvio<=9) {
        $FechaEnvio='0'.(int)$FechaEnvio;
        }

        $setup['PP_FechaEnvio']=$fecha.'-'.$FechaEnvio.' '.$hora;
      }
      if ($FechaColocacion!='') {
        if ($FechaColocacion<=9) {
        $FechaColocacion='0'.(int)$FechaColocacion;
        }
        $setup['PP_FechaColocacion']=$fecha.'-'.$FechaColocacion.' '.$hora;
      }

      //print_r($setup);
      $where='idPedido='.$request->idPedido.' AND idFicha='.$request->ficha_id.' AND idPostulante='.$request->id_persona.' AND PP_id='.$request->PP_id;

      //$sql='UPDATE postulantepedido SET PP_Estado="1"'.$setup.'  WHERE '.$where;
      DB::table('postulantepedido')
            ->whereRaw($where)
            ->update($setup);
     // DB::select($sql);
      $rtn=array('smg'=>'Información Actualizado');

      echo 'Información Actualizado';
    //  return json_encode($rtn,JSON_PRETTY_PRINT);
      //return response()->json(array('smg'=>'Información Actualizado'));

    }
    public function updatePedido(Request $request){
      $ped=$request->ped;
      $tipos=$request->tipos;
      $where='id='. $ped;
     // $sql='UPDATE pedidos SET ped_InterColcCie="'.$tipos.'"  WHERE '.$where;
      DB::table('pedidos')
            ->whereRaw($where)
            ->update(['ped_InterColcCie' => $tipos]);
     // DB::select($sql);
      $rtn=array('smg'=>'Información Actualizado');
      echo 'Información Actualizado';
      //return json_encode($rtn,JSON_PRETTY_PRINT);
    }
    public function deletePedidPostu($id,Request $request){

      $where='idPedido='.$request->idPedido.' AND idFicha='.$request->ficha_id.' AND idPostulante='.$request->id_persona.' AND PP_id='.$request->PP_id;

      $sql='DELETE FROM  postulantepedido WHERE '.$where;
       // DB::table('users')
       //      ->whereRaw($where)
       //      ->update(['ped_InterColcCie' => $tipos]);
        DB::table('postulantepedido')->whereRaw($where)->delete();
     // DB::select($sql);
      //$rtn=array('smg'=>'Información Actualizado');
      echo 'Información Actualizado';
     // return json_encode($rtn,JSON_PRETTY_PRINT);

    }
    public function RegistroPostulante($request){

      $join_experiencia = "";
      $join_ocupacion = "";
      $join_superior = "";
      $join_idioma = "";
      $where_ubigeo='';
      $where = "";


      //filtrado de sexo
      $where='per_estado="1"';
      if ($request['sexo']!='' && isset($request['sexo'])) {
        $where.=' AND p.sexo="'.$request['sexo'].'"';
      }

      //filtrado por estado de civl
    /*  if ($request['estado_civil']!='' && $request['estado_civil'] != "1" && isset($request['estado_civil'])) {
        $where.=' AND p.estado_civil="'.$request['estado_civil'].'"';
      }
*/
          //IDIOMA QUE HABLA
      if ($request['idioma1']!='' && isset($request['idioma1'])) {
        $where.=' AND ICP.idioma_nom="'.$request['idioma1'].'"';
        $join_idioma=' INNER JOIN pers_idioma ICP ON ICP.fichper_id=FP.id';

        //nivel idioma
          if ($request['nivel_idioma1'] !='' && isset($request['nivel_idioma1'])) {
            $where.=' AND ICP.idioma_nivel="'.$request['nivel_idioma1'].'"';
          }

      }

      //licencia de conducir
      if ($request['licenciaConduc']!='' && isset($request['licenciaConduc'])) {
        $where.=' AND FP.licenciaConduc="'.$request['licenciaConduc'].'"';
      }

      if(isset($request['categoria']) && $request['categoria'] != ""){
        $where.=' AND FP.categoria="'.$request['categoria'].'"';
      }

      if(isset($request['port_armas']) && $request['port_armas'] != ""){
        $where.=' AND FP.porta_arma="'.$request['port_armas'].'"';
      }

      if(isset($request['arma_propia']) && $request['arma_propia'] != ""){
        $where.=' AND FP.arma_propia="'.$request['arma_propia'].'"';
      }
      // grado academico

      if ((isset($request['gradosAcademicos']) && $request['gradosAcademicos'] != "") || ($request['profesion']!='' && $request['profesion'])) {

          $join_superior = " inner join per_superior su on su.fichper_id=FP.id ";

          if(isset($request['gradosAcademicos']) && $request['gradosAcademicos'] != "" && $request['gradosAcademicos'] != "1"){
            $where .= " and su.nivel_id='".$request['gradosAcademicos']."' ";

            if(isset($request['estado']) && $request['estado'] != ""){
              $where .= " and su.estado='".$request['estado']."' ";
            }

          }

          if($request['profesion']!='' && $request['profesion']){
            $where.=' AND su.especialidad_id="'.$request['profesion'].'"';
          }

      }


      //codigo coupacion  experiencia

      if ((isset($request['ocupacion']) && $request['ocupacion']!='') ||
        (isset($request['cod_ocupacion']) && $request['cod_ocupacion'] != "") ||
        (isset($request['tareas']) && $request['tareas'] != "" ) ||
        (isset($request['numExperien']) && $request['numExperien'] != "")) {

        $join_experiencia=' INNER JOIN pers_experiencia EXP ON EXP.fichper_id = FP.id ';

        if(isset($request['ocupacion']) && $request['ocupacion']!=''){
          $where.=' AND EXP.id_ocupacion="'.$request['ocupacion'].'"';
        }

       /* if(isset($request['cod_ocupacion']) && $request['cod_ocupacion']!=''){
          $where.=' AND EXP.id_ocupacion="'.$request['cod_ocupacion'].'"';
        }
*/
        if(isset($request['tareas']) && $request['tareas'] != "" ){
          $where.=' AND EXP.funciones like "%'.$request['tareas'].'%"';
        }

        if(isset($request['numExperien']) && $request['numExperien'] != ""){
        //  $where.=' AND (SUBSTRING_INDEX(EXP.fech_fin, ".", 1) - SUBSTRING_INDEX(EXP.fech_inicio, ".", 1)) = "'.$request['numExperien'].'"';
          if(isset($request['tiempo_exp']) && $request['tiempo_exp'] != ""){
            $where .= ' and TIMESTAMPDIFF(MONTH, EXP.fech_inicio, EXP.fech_fin) = "'.$request['numExperien'].'"';
          }else{
            $where .= ' and TIMESTAMPDIFF(YEAR, EXP.fech_inicio, EXP.fech_fin) = "'.$request['numExperien'].'"';
          }

        }


      }


      //FILTRO DE UBIGEO
      if ($request['pais']!='0') {
        $where.=' AND FP.id_pais="'.$request['pais'].'"';
      }
      //buscamos en el ubigeo los departamentos
      if ($request['deparme']!='0') {
        $where_ubigeo=' AND  LEFT(p.ubigeo,2)="'.$request['deparme'].'"';
      }
      //buscamos los ubigeos provincia y deparmet.
      if ($request['provi']!='0') {
        $where_ubigeo=' AND  LEFT(p.ubigeo,4)="'.$request['deparme'].$request['provi'].'"';
      }
      //ubigeo completo
      if ($request['distri']!='0') {
        $where_ubigeo=' AND p.ubigeo="'.$request['deparme'].$request['provi'].$request['distri'].'"';
      }



      //SI ESTAS BUSCANDO PERSONAS CON DISCAPA
      if (isset($request['radioin'])) {
        if ($request['radioin']!='') {
          $where.=' AND p.discapacidad="'.$request['radioin'].'"';
        }
      }



      //EDAD MINIMA
     if ($request['edad_desde']!='') {
        $where.=' AND TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE())>="'.$request['edad_desde'].'"';
      }
      //EDAD MAXIMA
    if ($request['edad_hasta']!='') {
        $where.=' AND TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE())<="'.$request['edad_hasta'].'"';
      }


      //FILTRO PARA SALARIO MINIMO
    /*  if ($request['salariomini']!='') {
        $where.=' AND FP.fic_salario>="'.$request['salariomini'].'"';
      }
*/
      //FILTRO DE SALARIO MAXIM
     /* if ($request['salariomax']!='') {
        $where.=' AND FP.fic_salario<="'.$request['salariomax'].'"';
      }*/
      //FILTRAR PUESTO
    /*  if ($request['idpuesto']!='') {
        $where.='';
      }*/


      $where.=$where_ubigeo;
      $tbl='';
      $sql='SELECT p.id as id_persona,FP.id as ficha_id,UB.ubi_descripcion,p.name,FP.fic_salario,p.email,p.telefono,p.ape_mat,p.ape_pat,p.numero_documento,sexo,TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad, FP.filecv FROM personas p INNER JOIN fichapersona FP ON FP.persona_id=p.id INNER JOIN ubigeo UB ON UB.ubi_codigo=p.ubigeo '.$join_idioma.$join_experiencia.$join_ocupacion.$join_superior.' WHERE '.$where.' LIMIT 200';
      $Lista=DB::select($sql);
     // print_r($join_IdiaComp);
    //return false;
      // $Lista =DB::table(DB::raw('personas p'))
      //     ->select('p.id as id_persona','FP.id as ficha_id','UB.ubi_descripcion','p.name','FP.fic_salario','p.email','p.telefono','p.ape_mat','p.ape_pat','p.numero_documento','sexo',DB::raw("TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad"))
      //     ->join(DB::raw('fichapersona FP'),'FP.persona_id','=','p.id')
      //     ->join(DB::raw('ubigeo UB'),'UB.ubi_codigo','=','p.ubigeo')
      //     ->join(array(),'','','')
      //     ->whereRaw($where)
      //     ->paginate(50);
      $tbl2='';
      try {
        if (count($Lista)) {
          foreach ($Lista as $key => $rst) {
            $tbl .= "<tr id=Frb-" . $rst->numero_documento. ">" .
                  "<td >
                  <input type=\"checkbox\">
                  <input type=\"hidden\" value=".$rst->ficha_id.">
                  <input type=\"hidden\" value=".$rst->id_persona.">
                  </td>" .
                  "<td >" . $rst->numero_documento . "</td>" .
                  "<td >" . $rst->ape_pat . "</td>" .
                  "<td >" . $rst->ape_mat .  "</td>" .
                  "<td >" . $rst->name .  "</td>" .
                  "<td  >" . $rst->telefono . "</td>";
            $tbl .="<td  >" . $rst->email. "</td>";
            $tbl .="<td  align=\"center\">" . $rst->fic_salario. "</td>";
            $tbl .="<td  >" . $rst->ubi_descripcion. "</td>";
            if(isset($rst->filecv) && $rst->filecv != "" && $rst->filecv != null){
              $tbl .="<td ><a href='".route("download.hojavida",["id" => $rst->ficha_id])."' class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
            }else{
              $tbl .="<td >x</td>";
            }
            $tbl .=  "</tr>";
          }
        }
        $tbl2 .='<tr><td colspan="10">';
        //$tbl2  .=$Lista->setPath('pagin/FactIns');//->links('pagination::bootstrap-4');
        $tbl2 .='</td></tr>';
      } catch (Exception $e) {

      }
      ob_start();
      echo $tbl;
      $initial_content = ob_get_contents();
      ob_end_clean();

      ob_start();
      echo $tbl2;
      $initial_content2 = ob_get_contents();
      ob_end_clean();
      return array('table'=>$initial_content ,'theadPagin'=>$initial_content2);
    }

    public function savePedidoPedido($id='',Request $request){
      $resultado=array();
      $data['request']=$request->all();
      $detalle=$request->detalle;
      $sql1=
      $Lista1=DB::select("SELECT funtion_savepedidopostulante($id,'$detalle')");
      if (count($Lista1)>0) {
        foreach ($Lista1 as $key => $value) {
          $resultado=$value;
        }
      }
      return response()->json(array('resultado'=>$resultado));
    }
    public function getPostulantePedido($id,$request){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();


      foreach($entidades as $value){
        $data['btn_apto'] = $value->btn_apto;
        $data['btn_citado'] = $value->btn_citado;
        $data['btn_contratado'] = $value->btn_contratado;
      }
    $sql="SELECT razon_social FROM pedidos
    INNER JOIN empresas ON empresas.id=empresa_id WHERE pedidos.id='$id' LIMIT 1 ";
    $razon_social='';
    $roq=DB::select($sql);
    if(count($roq)>0){
      $razon_social=$roq[0]->razon_social;
    }
      $where='';
      $join_IdiaComp='';
      $join_specialidad='';
      $where_ubigeo='';
      //filtrado de sexo
      $where='per_estado="1" AND idPedido="'.$id.'"';
      $tbl='';

      $Lista =DB::table(DB::raw('postulantepedido PP'))
          ->select('PP.idPedido','FP.url_cv','PP.PP_id','p.id as id_persona','FP.id as ficha_id','p.name','FP.fic_salario','p.email','p.telefono','p.ape_mat','p.ape_pat','p.numero_documento','sexo',DB::raw("TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) AS edad"), 'FP.filecv', 'PP.PP_Prosedendia', 'PP.PP_Status', 'FP.id as id_ficha','PostPed_contra', 'PostPed_apto', 'PP_Visto')
          ->join(DB::raw('personas p'),'p.id','=','PP.idPostulante')
          ->join(DB::raw('fichapersona FP'),'FP.id','=','PP.idFicha')
          ->whereRaw($where)
          ->paginate(50);

      $tbl2='';
      try {
        if (count($Lista)) {
          foreach ($Lista as $key => $rst) {
            $tbl .= "<tr id=Frb-" . $rst->numero_documento. ">" .
                  "<td style='font-size:13px;'>" . $rst->numero_documento . "</td>" .
                  "<td style='font-size:13px;'>" .$rst->name .' '.$rst->ape_pat .' '.$rst->ape_mat . "</td>" .
                  "<td  style='font-size:13px;'>" . $rst->telefono . "</td>";
            $lista_ext = DB::table('pers_experiencia')->select('ocupaciones.nombre as nombre_ocu')
            ->join(DB::raw('ocupaciones'),'ocupaciones.ocup_codigo','=','pers_experiencia.id_ocupacion')
            ->where('pers_experiencia.fichper_id', $rst->id_ficha)->limit(1)->get();
            if (count($lista_ext)) {
            foreach ($lista_ext as $i => $exp) {
              $tbl .="<td  style='font-size:13px;'>" . $exp->nombre_ocu . "</td>";
            }
             }else{
                $tbl .="<td style='font-size:13px;'>...</td>";
             }
            //$tbl .="<td ><a href=\"#\" class=\"sizeIco\" data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"cargarContenidoPostulante(this,".$rst->PP_id.",".$rst->ficha_id.",".$rst->id_persona.",".$rst->idPedido.")\"><i class=\"far fa-edit\"></i></a></td>";
            if(isset($rst->PP_Visto) && $rst->PP_Visto == "1"){
              $tbl .="<td style='font-size:13px;text-align: center;'><a rel='CV Leido' href=\"#\" class=\"sizeIco\" onclick=\"DescargarHojavida(this,".$rst->PP_id.",".$rst->ficha_id.",".$rst->id_persona.",".$rst->idPedido.",2)\"><i class=\"fas fa-search\" style='color:#007bff'></i></a></td>";

            }else{
              $tbl .="<td style='font-size:13px;text-align: center;'><a rel='Leer CV' href=\"#\" class=\"sizeIco\" onclick=\"DescargarHojavida(this,".$rst->PP_id.",".$rst->ficha_id.",".$rst->id_persona.",".$rst->idPedido.",2)\"><i class=\"fas fa-search\" style='color:red'></i></a></td>";

            }

            
            if(isset($rst->filecv) && $rst->filecv != "" && $rst->filecv != null){
              $tbl .="<td style='font-size:13px;text-align: center;'><a  href='".route("download.hojavida",["id" => $rst->ficha_id])."' class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
            }else{
              $tbl .="<td style='font-size:13px;text-align: center;'>x</td>";
            }
            if($rst->url_cv==''){
              $tbl .="<td style='font-size:13px;text-align: center;'></td>";

            }else{
               $tbl .="<td style='font-size:13px;text-align: center;'><a href='#' onclick='return cambiarAptoVideo(this,".$rst->idPedido.",".$rst->id_persona.",".$rst->ficha_id.", 2)'>Ver</a></td>";

            }
          
           // $tbl .="<td ><a onclick=\"DescargarHojavida(this,".$rst->PP_id.",".$rst->ficha_id.",".$rst->id_persona.",".$rst->idPedido.",1)\" href=\"#\" class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
            if ($rst->PP_Prosedendia == "2") {
              $tbl .="<td style='font-size:13px;text-align: center;'> <b style='color:blue;' title='Postulación en linea'><i class='fas fa-globe'></i></b> </td>";

            }else{
              $tbl .="<td style='font-size:13px;text-align: center;'> <b style='color:#696969;' title='Agregado'> <i class='fas fa-database'></i></b> </td>";
            }

            if($rst->PostPed_apto == '1'){
              $tbl .="<td style='font-size:13px;text-align: center;'></td>";
           
            }else{
              $tbl .="<td style='font-size:13px;text-align: center;'><a  data-toggle=\"modal\" data-target=\"#myModalDelete\" onclick=\"EliminarPedidoPostulante(this,".$rst->PP_id.",".$rst->ficha_id.",".$rst->id_persona.",".$rst->idPedido.")\" href=\"#\" class=\"sizeIco\"><i class=\"far fa-times-circle\"></i></a></td>";
           
            }
            if($rst->PostPed_apto == '1'){
              $tbl .= "<td style='font-size:13px;text-align: center;'><img src='". asset($data['btn_apto']) ."' width='50'></td>";
            }else{
              $tbl .= "<td style='font-size:13px;text-align: center;'><a href='#'
               onclick='return cambiarApto(this,".$rst->idPedido.",".$rst->id_persona.",".$rst->ficha_id.", 2)'><img src='". asset('images/noapto.png') ."' width='50'></a></td>";
            }

            if($rst->PP_Status == '1'){
              $tbl .= "<td style='font-size:13px;text-align: center;'><img src='". asset($data['btn_citado']) ."' width='50'></td>";
            }else{
             $gg= 'onclick="return cambiarCitado(this,'.$rst->idPedido.','.$rst->id_persona.','.$rst->ficha_id.', 1,'."'".$rst->PostPed_apto."'".','. "'".$rst->telefono.'|'.$razon_social.'|'.$rst->name .' '.$rst->ape_pat .' '.$rst->ape_mat."'".')"';
              $tbl .= "<td style='font-size:13px;text-align: center;'><a href='#' $gg  ><img src='". asset('images/nocitado.png') ."' width='50'></a></td>";
            }

            if($rst->PostPed_contra   == '1'){
              $tbl .= "<td style='font-size:13px;text-align: center;'><img src='". asset($data['btn_contratado']) ."' width='50'></td>";
            }else{
              $tbl .= '<td style="font-size:13px;text-align: center;"><a href="#"
               onclick="return cambiarContratado(this,'.$rst->idPedido.','.$rst->id_persona.','.$rst->ficha_id.',3,'."'".$rst->PostPed_apto."','".$rst->PP_Status."'".')">
               <img src="'. asset('images/nocontratado.png') .'" width="50"></a></td>';
            }
          //  $tbl .= "<td></td>";
          //  $tbl .= "<td></td>";
          //  $tbl .="<td ><select class=\"form-control form-control-sm\" onChange=\"cambiarStatus(this,".$rst->idPedido.",".$rst->id_persona.",".$rst->ficha_id.")\"><option>Seleccione</option><option value=\"1\" ".(isset($rst->PP_Status) && $rst->PP_Status != '' && $rst->PP_Status != null && $rst->PP_Status == '1' ? 'selected' : '').">Citado</option><option value=\"2\" ".(isset($rst->PP_Status) && $rst->PP_Status != '' && $rst->PP_Status != null && $rst->PP_Status == '2' ? 'selected' : '').">Apto</option><option value=\"3\" ".(isset($rst->PP_Status) && $rst->PP_Status != '' && $rst->PP_Status != null && $rst->PP_Status == '3' ? 'selected' : '').">No Apto</option><option value=\"4\" ".(isset($rst->PP_Status) && $rst->PP_Status != '' && $rst->PP_Status != null && $rst->PP_Status == '4' ? 'selected' : '').">Proceso Entrevista</option><option value=\"5\" ".(isset($rst->PP_Status) && $rst->PP_Status != '' && $rst->PP_Status != null && $rst->PP_Status == '5' ? 'selected' : '').">Contratado</option></select></td>";

            $tbl .=  "</tr>";
          }
        }
        $tbl2 .='<tr><td colspan="11">';
        $tbl2  .=$Lista->setPath('pagin/FactIns');//->links('pagination::bootstrap-4');
        $tbl2 .='</td></tr>';
      } catch (Exception $e) {

      }
      ob_start();
      echo $tbl;
      $initial_content = ob_get_contents();
      ob_end_clean();

      ob_start();
      echo $tbl2;
      $initial_content2 = ob_get_contents();
      ob_end_clean();
      return array('table'=>$initial_content ,'theadPagin'=>$initial_content2);
    }

     public function cambioEstatus(Request $request){

      $idioma = PedidoPostulante::where('idPedido', $request->codigo_cambio)
              ->where('idFicha',$request->ficha)
              ->where('idPostulante',$request->persona)
              ->update([
                'PP_Status' => $request->selection,
                'PP_Visto' => '1',
             ]);
   
         // return $idioma;
       return back()->withInput();
    }


    public function  editCitado($ficha, $persona, $pedido, $fecha, $hora){

      $now = new \DateTime();

      $pedido_list = PedidoPostulante::where('idPedido', $pedido)
              ->where('idFicha',$ficha)
              ->where('idPostulante',$persona)
              ->update([
                'PP_Status' => '1',
                'PP_Visto' => '1',
                'PP_StatusFecha' => $now->format('Y-m-d H:i:s'),
                'fecha' => $fecha,
                'hora' => $hora,
             ]);
      $datos_pedpost = PedidoPostulante::where('idPedido', $pedido)
      ->where('idFicha',$ficha)
      ->where('idPostulante',$persona)
      ->get();

      $empresa = Empresa::where('id', Auth::user()->empresa_id)->get();

      $persona = Persona::where('id', $persona)->get();

      $name_empresa = $empresa[0]->razon_social;
      $email_persona = $persona[0]->email;
      foreach($datos_pedpost as $value){
          $agregarNotificacion = Notificacion::create([
              'not_estado' => '3',
              'not_mensaje' => 'Usted fue citado',
              'id_contacto' => Auth::guard('contacto')->user()->id,
              'id_users' => $value->idPostulante,
              'id_pedido' => $value->idPedido,
          ]);
      }

      if($fecha != '-'){
        $data['message'] = "La bolsa de trabajo de la Municipalidad de Lima le informa que ha sido citado por la empresa <b>".$name_empresa."</b> el día ".Carbon::parse($fecha)->format('d/m/Y')." a las ".$hora." horas. Buen día.";
      }else {
        $data['message'] = "La bolsa de trabajo de la Municipalidad de Lima le informa que ha sido citado por la empresa <b>".$name_empresa."</b> Esté atento a las comunicaciones que pronto pueda recibir. Buen día.";
      }
      Mail::to($email_persona)->send(new EnvioCita($data));
      $msj="Item modificado";

       return json_encode($msj,JSON_PRETTY_PRINT);

    }


    public function  editApto($ficha, $persona, $pedido){

      $now = new \DateTime();
      $pedido_list = PedidoPostulante::where('idPedido', $pedido)
              ->where('idFicha',$ficha)
              ->where('idPostulante',$persona)
              ->update([
                'PostPed_apto' => '1',
                'PP_Visto' => '1',
                'PostPed_aptoFecha' => $now->format('Y-m-d H:i:s'),
             ]);

      $datos_pedpost = PedidoPostulante::where('idPedido', $pedido)
      ->where('idFicha',$ficha)
      ->where('idPostulante',$persona)
      ->get();

        foreach($datos_pedpost as $value){
            $agregarNotificacion = Notificacion::create([
                'not_estado' => '2',
                'not_mensaje' => 'Usted es Apto',
                'id_contacto' => Auth::guard('contacto')->user()->id,
                'id_users' => $value->idPostulante,
                'id_pedido' => $value->idPedido,
            ]);
        }


      $msj=$datos_pedpost;
         // return $idioma;
      // return back()->withInput();
       return json_encode($msj,JSON_PRETTY_PRINT);

    }

    public function  editContratado($ficha, $persona, $pedido){

      $now = new \DateTime();
      $pedido_list = PedidoPostulante::where('idPedido', $pedido)
              ->where('idFicha',$ficha)
              ->where('idPostulante',$persona)
              ->update([
                'PostPed_contra' => '1',
                'PP_Visto' => '1',
                'PostPed_contraFecha' => $now->format('Y-m-d H:i:s'),
             ]);
      $msj="Item modificado";

      $datos_pedpost = PedidoPostulante::where('idPedido', $pedido)
      ->where('idFicha',$ficha)
      ->where('idPostulante',$persona)
      ->get();

      foreach($datos_pedpost as $value){
          $agregarNotificacion = Notificacion::create([
              'not_estado' => '4',
              'not_mensaje' => 'Usted fue contratado',
              'id_contacto' => Auth::guard('contacto')->user()->id,
              'id_users' => $value->idPostulante,
              'id_pedido' => $value->idPedido,
          ]);
      }

       return json_encode($msj,JSON_PRETTY_PRINT);

    }

  public function agregarpost($ficha, $persona, $idPedido){

    $where[] = ['idPedido',$idPedido];
    $where[] = ['idFicha',$ficha];
    $where[] = ['idPostulante',$persona];

    $formacion = PedidoPostulante::where($where)->get();
    $msj="0";

    if(!count($formacion) > 0){
      $form = PedidoPostulante::create([
        'idPedido' => $idPedido,
        'idFicha' => $ficha,
        'idPostulante' => $persona,
        'PP_Estado' => '1',
        'PP_Prosedendia' => '1',
      ]);
      $msj="1";
    }else{
      $msj="0";
    }

    return json_encode($msj,JSON_PRETTY_PRINT);

  }

  public function deletepost($ficha, $persona, $idPedido){

    $where[] = ['idPedido',$idPedido];
    $where[] = ['idFicha',$ficha];
    $where[] = ['idPostulante',$persona];

    $deletedRows = PedidoPostulante::where($where)->delete();

    $msj="eliminado correctamente";

    return json_encode($msj,JSON_PRETTY_PRINT);

  }

  public function agregarPostAjax(){

      // lista de inputs get
      $ocupacion = Input::get('ocupacion');
      $edad_desde = Input::get('edad_desde');
      $edad_hasta = Input::get('edad_hasta');
      $sexo = Input::get('sexo');
      $id_pais = Input::get('pais');
      $deparme = Input::get('deparme');
      $provi = Input::get('provi');
      $distri = Input::get('distri');
      $titulo_puesto = Input::get('titulo_puesto');
      $empresa = Input::get('empresa');
      $tareas = Input::get('tareas');
      $salario = Input::get('salario');
      $institucion = Input::get('institucion');
      $profesion = Input::get('profesion');
      $gradosAcademicos = Input::get('gradosAcademicos');
      $estado = Input::get('estado');
      $idioma1 = Input::get('idioma1');
      $compu = Input::get('compu');
      $nombre = Input::get('nombre');
      $apellido = Input::get('apellido');
      $documento = Input::get('documento');
      $licenciaConduc = Input::get('licenciaConduc');
      $categoria = Input::get('categoria');
      $port_armas = Input::get('port_armas');
      $arma_propia = Input::get('arma_propia');
      $ultima_act = Input::get('ultima_act');
      $discapcidad = Input::get('discapcidad');
      $cv_visible = Input::get('cv_visible');
      $pedido = Input::get('pedido');
      $porta_arma = Input::get('porta_arma');
      $arma_propia = Input::get('arma_propia');
      $discapacidad = Input::get('discapacidad');
    #/* buesqueda por eloquent */

    $list_query = Persona::select('fichapersona.filecv','personas.pers_edad','fichapersona.foto_post', 'personas.id as id_persona','fichapersona.id as ficha_id','personas.name','personas.email','personas.telefono','personas.sexo','personas.fecha_nacimiento');
    $list_query=$list_query->leftJoin('fichapersona', 'fichapersona.persona_id', '=', 'personas.id');


    # ocupacion
    if ((isset($ocupacion) && $ocupacion !='') || (isset($tareas) && $tareas != "")
    || (isset($titulo_puesto) && $titulo_puesto != "") || (isset($empresa) && $empresa != "")){


      $list_query=$list_query->leftJoin('pers_experiencia', function($join) use ($titulo_puesto){

        $join->on('pers_experiencia.fichper_id', '=', 'fichapersona.id');

       /* if(isset($titulo_puesto) && $titulo_puesto !=''){  # falta
          $join->on('pers_experiencia.id_ocupacion' , '=', DB::raw("'".$titulo_puesto."'"));
       }
*/
      });

      if(isset($ocupacion) && $ocupacion !=''){
        $list_query=$list_query->OrWhere('pers_experiencia.id_ocupacion' , '=', $ocupacion)->OrWhere('fichapersona.ocu_trabajo', '=', $ocupacion);


      }

      if(isset($tareas) && $tareas != "" ){
        $list_query=$list_query->where('pers_experiencia.funciones' , 'like', '%'.$tareas.'%');
      }

      if(isset($titulo_puesto) && $titulo_puesto !=''){  # falta
        $list_query=$list_query->OrWhere('pers_experiencia.id_ocupacion' , '=', $titulo_puesto)->OrWhere('fichapersona.ocu_trabajo', '=', $titulo_puesto);
      }

      if(isset($empresa) && $empresa != ""){
        $list_query=$list_query->where('pers_experiencia.nom_empresa' , 'like', '%'.$empresa.'%');
      }



    }


  //EDAD MINIMA
 if (isset($edad_desde) && $edad_desde!='') {
    $list_query=$list_query->where('personas.pers_edad' , '>=', $edad_desde);
  }
  //EDAD MAXIMA
  if (isset($edad_hasta)  && $edad_hasta!='') {
      $list_query=$list_query->where('personas.pers_edad' , '=<', $edad_hasta);
  }

   //FILTRO DE SEXO

   if (isset($sexo) && $sexo !='') {
     $list_query=$list_query->where('personas.sexo' , '=', $sexo);
   }


  //FILTRO DE UBIGEO
  if (isset($pais) && $pais!='0') {
    $list_query=$list_query->where('fichapersona.id_pais' , '=', $id_pais);
  }
  //buscamos en el ubigeo los departamentos
  if (isset($deparme) && $deparme!='0') {
    $list_query=$list_query->where('personas.ubigeo' , 'like', $deparme.'%');
  }
  //buscamos los ubigeos provincia y deparmet.
  if (isset($provi) && $provi!='0') {
    $list_query=$list_query->where('personas.ubigeo' , 'like', $deparme.$provi.'%');
  }
  //ubigeo completo
  if (isset($distri) && $distri!='0') {
    $list_query=$list_query->where('personas.ubigeo' , '=', $deparme.$provi.$distri);
  }

  // FILTRO DE SALARIO

  if(isset($salario) && $salario != ""){
    $list_query=$list_query->where('fichapersona.fic_salario' , '=', $salario);
  }


  # filtro por educacion
  if ((isset($gradosAcademicos) && $gradosAcademicos != "") || (isset($profesion) && $profesion != "") || (isset($institucion) && $institucion != "")) {

    $list_query=$list_query->leftJoin('per_superior', 'per_superior.fichper_id', '=', 'fichapersona.id');
    if(isset($gradosAcademicos) && $gradosAcademicos != "" && $gradosAcademicos != "1"){
      $list_query=$list_query->where('per_superior.nivel_id' , '=', $gradosAcademicos);
      if(isset($estado) && $estado != ""){
        $list_query=$list_query->where('per_superior.persu_estado' , '=', $estado);

      }

    }

    if(isset($profesion) && $profesion!=''){
      $list_query=$list_query->where('per_superior.especialidad_id' , '=', $profesion);
    }

    if(isset($institucion) && $institucion != ""){
      $list_query=$list_query->where('per_superior.otro_centro' , 'like', '%'.$institucion.'%');
    }


}

# filtro idioma
if (isset($idioma1) && $idioma1 !='') {
  $list_query=$list_query->where('pers_idioma.idioma_nom' , '=', $idioma1);
  $list_query=$list_query->leftJoin('pers_idioma', 'pers_idioma.fichper_id', '=', 'fichapersona.id');
}

#filtro ocupacion
if (isset($compu) && $compu!='') {
  $list_query=$list_query->where('pers_compu.compu_nom' , '=', $compu);
  $list_query=$list_query->leftJoin('pers_compu', 'pers_compu.fichper_id', '=', 'fichapersona.id');
}



  //licencia de conducir
  if (isset($licenciaConduc) && $licenciaConduc!='') {
    $list_query=$list_query->where('fichapersona.licenciaConduc' , '=', $licenciaConduc);
  }

  if(isset($categoria) && $categoria != ""){
    $list_query=$list_query->where('fichapersona.categoria' , '=', $categoria);
  }

  if(isset($port_armas) && $port_armas != ""){
    $list_query=$list_query->where('fichapersona.porta_arma' , '=', $porta_arma);
  }

  if(isset($arma_propia) && $arma_propia != ""){
    $list_query=$list_query->where('fichapersona.arma_propia' , '=', $arma_propia);
  }



  // daots personales

  if(isset($nombre) && $nombre != ""){
    $list_query=$list_query->where('personas.name' , 'like', '%'.$nombre.'%');
  }

  if(isset($apellido) && $apellido != ""){
   // $where.=' AND (p.ape_pat like "%'.$apellido.'%" or p.ape_mat like "%'.$apellido.'%")';
    $list_query=$list_query->where('personas.name' , 'like', '%'.$apellido.'%');
  }

  if(isset($documento) && $documento != ""){
    $list_query=$list_query->where('personas.numero_documento' , 'like', '%'.$documento.'%');
  }

  //SI ESTAS BUSCANDO PERSONAS CON DISCAPA
  if (isset($discapcidad) && $discapcidad!='') {
      $list_query=$list_query->where('personas.discapacidad' , '=', $discapacidad);
  }

  #filtro por cv visible
  if (isset($cv_visible) && $cv_visible!='') {
      $list_query=$list_query->where('fichapersona.foto_post' , '!=', 'null');
  }

  if(isset($ultima_act) && $ultima_act != ""){
    $now = new \DateTime();

   // $where .= ' and TIMESTAMPDIFF(MONTH, FP.updated_at, "'.$now->format('d-m-Y H:i:s').'") = "'.$ultima_act.'" ';
  }

  $list_query=$list_query->where('fichapersona.fic_estado' , '=', '1')
  ->orderByRaw( "FIELD(personas.cod_entidad, ".(isset(Auth::user()->entidad_id) ? Auth::user()->entidad_id : 1).") DESC" )
  ->distinct()
  ->paginate(15);

  $tbl = '';
  if (count($list_query)) {
    foreach ($list_query as $key => $value) {
      $tbl .= '<div class="card" >';
        $tbl .= '<div class="card-body">';
          $tbl .= '<div class="row">';

            $tbl .= '<div class="col-1" style="max-width: 0;">';
              $tbl .= '<div class="custom-control custom-checkbox">';
                $tbl .= '<input type="checkbox" class="custom-control-input" id="todo'.$value->id_persona.'" name="todo'.$value->id_persona.'" value="1" onclick="return guardarPostulante('.$value->ficha_id.','.$value->id_persona.',this)">';
                $tbl .= '<label class="custom-control-label subtitulo-shearh" for="todo'.$value->id_persona.'"></label>';
              $tbl .= '</div>';
            $tbl .= '</div>';

      $tbl .='<div class="col-2">';
      $tbl .='<img src="'.asset("/public/storage/".$value->foto_post).'" alt="..."  style="width:100px; height:100px;">';
      $tbl .='</div>';

      $tbl .='<div class="col-2">';
      $tbl .='<span style="color:#003dc7; font-size:14px;">'.$value->name.'</span><br>';
      $tbl .='<p style="font-size:13px;line-height: 140%;">'.$value->ubi_descripcion.'<br>'.($value->sexo == "2" ? "Femenino" : "Masculino")."&nbsp;&nbsp;".$value->pers_edad.' Años<br>'.$value->email.'<br>'.$value->telefono.'<br>';
      $tbl .='</p>';
      $tbl .='</div>';

      $tbl .='<div class="col-3" style="flex: 0 0 25%; max-width: 25%">';

      $list_experiencia= Experiencia::select('ocupaciones.nombre as nombre_ocupacion','pers_experiencia.nom_empresa','fech_fin','fech_inicio','pers_experiencia.id')
      ->leftJoin('ocupaciones', 'ocupaciones.ocup_codigo','pers_experiencia.id_ocupacion')
      ->where('pers_experiencia.fichper_id','=',$value->ficha_id)
      ->where('pers_experiencia.pers_id','=',$value->id_persona)
      ->get();

      if(count($list_experiencia)){

          $tbl .= '<div id="carousel_experiencia'.$value->ficha_id.'" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';
        /*  $tbl .= ' <ol class="carousel-indicators">
          <li data-target="#carousel_experiencia" data-slide-to="0" class="active"></li>
          <li data-target="#carousel_experiencia" data-slide-to="1"></li>
          <li data-target="#carousel_experiencia" data-slide-to="2"></li>
        </ol>';*/
        $tbl .= '<div class="carousel-inner" style="height: 70%;">';
          foreach($list_experiencia as $i => $value_exp){
            $total = $i +1;
            $active =  ($i == '0' ? "active" : "");
           $tbl .=' <div class="carousel-item '.$active.'">
            <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_ocupacion.'</span><br>
              <p style="font-size:13px;line-height: 140%;">'.$value_exp->nom_empresa.'<br>
              '.$value_exp->fech_inicio.'-/-'.$value_exp->fech_fin.'<br></p>


            </div>';
          }

        $tbl .='</div>
        <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          <li class="page-item" >
            <a class="page-link" href="#carousel_experiencia'.$value->ficha_id.'" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item" >
            <a class="page-link" href="#carousel_experiencia'.$value->ficha_id.'" role="button" data-slide="next" style="padding: .1rem .75rem;">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>';

        /* $tbl .= '<a class="carousel-control-prev" href="#carousel_experiencia" role="button" data-slide="prev" >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel_experiencia" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>' ;*/
         $tbl .= ' </div>';

      }


      $tbl .='</div>';

      $tbl .='<div class="col-3" style="flex: 0 0 30%; max-width: 30%">';

      $list_eucacion= FormacionSuperior::select('grad_descripcion as nombre_grado','profesionales.nombre as nombre_prefecion','per_superior.otro_centro','fecha_fin','fecha_inicio')
      ->leftJoin('profesionales', 'profesionales.profp_codigo','per_superior.especialidad_id')
      ->leftJoin('gradoinstruccion', 'gradoinstruccion.id','per_superior.nivel_id')
      ->where('per_superior.fichper_id','=',$value->ficha_id)
      ->where('per_superior.pers_id','=',$value->id_persona)
      ->get();

      if(count($list_eucacion)){

          $tbl .= '<div id="carousel_education'.$value->ficha_id.'" class="carousel slide" data-ride="carousel" data-interval="false" style="height: 100%;">';
         /* $tbl .= ' <ol class="carousel-indicators">
          <li data-target="#carousel_education'.$value->ficha_id.'" data-slide-to="0" class="active"></li>
          <li data-target="#carousel_education'.$value->ficha_id.'" data-slide-to="1"></li>
          <li data-target="#carousel_education'.$value->ficha_id.'" data-slide-to="2"></li>
        </ol>';*/
          $tbl .= '<div class="carousel-inner" style="height: 70%;">';
          foreach($list_eucacion as $i => $value_exp){
            $active =  ($i == '0' ? "active" : "");
           $tbl .=' <div class="carousel-item '.$active.'" >
            <span style="color:#003dc7; font-size:14px;">'.$value_exp->nombre_prefecion.'</span><br>
              <p style="font-size:13px;line-height: 140%;">'.$value_exp->otro_centro.'<br>'.$value_exp->nombre_grado.'<br>
              '.$value_exp->fech_inicio.'-/-'.$value_exp->fecha_fin.'<br></p>
            </div>';
          }

        $tbl .='</div>
        <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-end">
          <li class="page-item" >
            <a class="page-link" href="#carousel_education'.$value->ficha_id.'" role="button" data-slide="prev" style="padding: .1rem .75rem;" >
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item" onclick="return cambiarmasexp('.$value_exp->id.');">
            <a class="page-link" href="#carousel_education'.$value->ficha_id.'" role="button" data-slide="next" style="padding: .1rem .75rem;">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>';
       /*
      $tbl .= '<a class="carousel-control-prev" href="#carousel_education" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel_education" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>';*/
      $tbl .='</div>';

      }

      $tbl .='</div>';

      $tbl .='<div class="col-1" style="max-width: 1.333333%;">';
      $tbl .= '<div class="btn-group">
      <a  href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="'.asset("images/icono_proceso.png").'">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a href="#" onclick="DescargarHojavida(this,2,'.$value->ficha_id.','.$value->id_persona.','.$pedido.',2)" class="dropdown-item" style="font-size:14px">Ver CV</a>';
        if(isset($value->filecv) && $value->filecv != "" && $value->filecv != null){
          $tbl .= '<a href="'.route("download.hojavida",["id" => $value->ficha_id]).'" class="dropdown-item" style="font-size:14px"><i class="fas fa-check-square"></i>&nbsp;&nbsp;Descargar CV Adjunto</a>';
        //  $tbl .="<td style='font-size:13px;'><a  href='".route("download.hojavida",["id" => $rst->ficha_id])."' class=\"sizeIco\"><i class=\"fas fa-download\"></i></a></td>";
        }else{
          $tbl .= '<a href="#" onclick="return errorCv()" class="dropdown-item" style="font-size:14px; ">Descargar CV Adjunto</a>';
        }

        $tbl .= '<a href="#" onclick="return agregarPostulante('.$value->ficha_id.','.$value->id_persona.')" class="dropdown-item" style="font-size:14px">Vincular Postulante al proceso</a>
        <a href="#" class="dropdown-item" style="font-size:14px">Compartir CV por Email</a>
      </div>
    </div>';
      $tbl .='</div>';

      $tbl .='</div>';

      $tbl .='</div>';
      $tbl .='</div>';

    }
  }

    /*$tbl .= '  <div class="row justify-content-md-center">
      <div class="col-sm-6">
          '.$list_query->links().'
      </div>
    </div>';**/
  ob_start();
  echo $tbl;
  $initial_content = ob_get_contents();
  ob_end_clean();

  return json_encode($tbl,JSON_PRETTY_PRINT);

  }


}
