<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controles;

class Filtros_controller extends Controller{

    function grupoFill(Request $request){
        $report=$request->method;
        $nombreclase = "\Filtros_controller::";
        $arrayinf=array('request'=>$request->all());
        return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit();
    }
    public function serie_documento($request) {
        $response['status']=0;
		$response['message']='';
        try{  
            $serie_empresa=Controles::get_empresasSerie($request['empresa'],$request['tipdcom'],$request['usua']);
              
            $response['serie']=$serie_empresa['response'];
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;        
    }
    public function get_buscardoc($request) {
        $response['status']=0;
		$response['message']='';
        try{  
            $numero=$request['nrodoc'];
            $empresa=$request['empresa'];
            $tipdom=$request['tipodoc'];
            $sql="SELECT empr_id,tpdc_tipodoc,fact_nrodoc, LEFT(fact_nrodoc,3)AS serie,
            RIGHT(fact_nrodoc,7) AS numero ,FORMAT(fact_impventa,2)AS impventa,
            FORMAT(fact_impigv,2)AS impigv,
            FORMAT(fact_impneto,2)AS impneto,fact_tdocguia,fact_nroguia,
            DATE_FORMAT(fact_fecemi,'%d/%m/%Y') AS fecha,
            DATE_FORMAT(fact_fecvenc, '%d/%m/%Y') AS fechave,
                DATE_FORMAT(fact_fecvisua, '%d/%m/%Y')fentrega,
                c.clie_id,c.clie_ruc,c.clie_nomb,c.clie_idalterno,fact_obs,
                v.clie_id AS id_vende,v.clie_nomb AS vendedor 
                
            FROM facturacion_cab f
            INNER JOIN clientes c ON c.clie_id=f.clie_id
            LEFT JOIN clientes v ON v.clie_id=f.clie_vend
            
                WHERE tpdc_tipodoc='$tipdom' AND fact_nrodoc='$numero' AND empr_id='$empresa'";
          
            $row=DB::select($sql);
            if(count($row)<=0){
                throw new \Exception('No se Encontro Resultados');
            }
            $result=$row[0];
            if($result->fact_tdocguia=='PED'){
                $sql2= " SELECT  clie_moto AS encargado, clie_nomb AS enclie_nomb 
                FROM facturacion_cab f JOIN clientes e ON e.clie_id=f.clie_moto
                WHERE fact_tipodocref='$tipdom' 
                AND fact_nrodocref='$numero' AND empr_id='$empresa'";
                $row_en=DB::select($sql2);
                if(count($row_en)>0){
                    $response['encargado']=$row_en[0];
                }
            
                
            }
            $response['response']=$result;
            $detalle=$this->get_detalle_doc($request);
            $response['detalle']=$detalle;
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response; 
    }
    public function get_detalle_doc($request){
        $html='';
        $response['status']=0;
		$response['message']='';
        try{ 
            $sql = "SELECT  kdin_comision,kdin_bonifi, kdin_tipodet,tpdt_nomb AS tipodetalle,  
            FORMAT(krins_preciounit,2)AS lprecio,
            kdin_cantot1 AS cantot1,
            krins_cantidad AS cantot0,
            d.inun_item,krins_cantidad,
            kdin_detallef AS unidadcgr,krins_item, krins_descto, d.insu_id,i.insu_codalterno,
             insu_nomb,
            u.unid_uniconsumo,  FORMAT(krins_preventa, 2) AS precio,krins_imptotal,
            FORMAT(krins_imptotal, 2) AS importe" .
            " FROM kardex_ins d" .
            " INNER JOIN insumos i ON i.insu_id = d.insu_id" .
            " LEFT JOIN unidades u ON u.unid_id = i.unid_id".
            " LEFT JOIN tb_tipo_detalle td ON td.tpdt_tipodoc = d.kdin_tipodet
             WHERE fact_emprid = '".$request['empresa']."' AND
             fact_tipodoc = '".$request['tipodoc']."' AND fact_nrodoc = '".$request['nrodoc']."'".
            "  AND krins_estado=1  
            ORDER BY  i.insu_id, krins_orditem ASC " ;
             
            $default= DB::select($sql);
            if(count($default)>0){
                $iten=1;
                foreach ($default as $key => $value) {
                    $html.='<tr>';
                    $html.='<td>'.$iten.'</td>';
                    $html.='<td>'.$value->insu_codalterno.'</td>';
                    $html.='<td>'.$value->insu_nomb.'</td>';
                    $html.='<td>'.$value->tipodetalle.'</td>';
                    
                    $html.='<td><input type="text" onkeyup="calcTotItemdet_'.$request['type'].'(this,'.$value->kdin_tipodet.')" value="'.$value->cantot1.'"   class="input50">';
                    $html.='<input type="hidden" value="'.$value->unidadcgr.'">
                    <input type="hidden" value="'.$value->cantot0.'">
                    <input type="hidden" value="'.$value->lprecio.'"> 
                    <input type="hidden" value="'.$value->kdin_tipodet.'"> 
                    <input type="hidden" value="'.$value->krins_item.'"> 
                    <input type="hidden" value="'.$value->insu_id.'"> 
                    <input type="hidden" value="'.$value->inun_item.'"> 
                    <input type="hidden" value="'.$value->kdin_bonifi.'">
                    <input type="hidden" value="'.$value->kdin_comision.'">
                    </td>';
                    $html.='<td>';
                    
                    $Lista=$this->unidad_insumo(array('zona'=>$request['zone'],'insumo'=>$value->insu_id),'ARRAY');
                    
                    $exitr=count($Lista['Lista']);
                    $html.='<select onchange="calcular_'.$request['type'].'(this,'."'".$value->kdin_tipodet."'".')" style="width: 120px;">';  
                    $opt='';
                    
                    if($exitr>0){
                         foreach ($Lista['Lista'] as $k => $result) {
                            $opt.='<option value="1" '.($value->inun_item==$result['inun_item']?'selected':''); 
                            $opt.=' data-ingresequivauni="'.$result['inun_equil'].'" data-ingrespreciouni="'.$result['inun_precio'].'"';
                            $opt.=' data-contenido="'.$result['inun_descr'].' X '.$result['inun_equil'].' '.$value->unid_uniconsumo.'">';
                            $opt.=$result['inun_descr'].' X '.$result['inun_equil'].' '.$value->unid_uniconsumo.' S/  '.$result['inun_precio'].'</option>';
                        } 
                        $html.=$opt;
                        
                    }
                    $html.='</select>';  
                    $html.='</td>';
                    $html.='<td><input type="text"   onkeyup="calcTotItemdet_'.$request['type'].'(this,'.$value->kdin_tipodet.')"  value="'.$value->precio.'"   class="input50"> </td>';
                    $html.='<td>'.$value->importe.'</td>';
                    $html.='<td><input type="checkbox" onchange="calimpo_'.$request['type'].'(this.disabled,this,'.$value->kdin_tipodet.')"></td>';
                    $html.='<td> </td>';
                    $html.='<td> </td>';
                  
                    $html.='</tr>';
                    $iten++;
                }
            }
            
            ob_start();
            echo $html;
            $initial_content = ob_get_contents();
            ob_end_clean();
            $response['table']=$initial_content;
            $response['status']=1; 
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;    
    }
    public function unidad_insumo($request,$type='JSON'){
        $zona=$request['zona'];  
        $obj=array();
        $sql_selec='apre_comsion1,apre_comsion2,apre_comsion3,apre_comsion4,apre_comsion5,apre_comsion6';
               
        $Lista=DB::select('SELECT '.$sql_selec.', arti_id,apre_lista,apre_equi1,apre_und1,
        apre_prex3,apre_equi2,apre_und2,apre_prex6,apre_equi3,apre_equi4,apre_equi5,apre_equi6,apre_und3,
        apre_pregrupo,apre_pre5,apre_pre6,apre_und4,apre_und5,apre_und6 FROM articulos_precios 
        WHERE apre_estado=1 AND zone_id='."'".$zona."'".' AND arti_id="'.$request['insumo'].'"');
        $exitr=count($Lista);
        if($exitr>0){
          $result=$Lista[0];
          
          $obj[0]=array('insu_id'=>$result->arti_id,
          'inun_descr'=>$result->apre_und1,
          'inun_equil'=>$result->apre_equi1,
          'inun_estado'=>1,'inun_item'=>1,
          'inun_precio'=>$result->apre_lista,
         'comision'=>$result->apre_comsion1,
        ); 

          $obj[1]=array('insu_id'=>$result->arti_id,
          'inun_descr'=>$result->apre_und2,
          'inun_equil'=>$result->apre_equi2,
          'inun_estado'=>1,'inun_item'=>2,
          'inun_precio'=>$result->apre_prex3,
          'comision'=>$result->apre_comsion2,);
  
          $obj[2]=array('insu_id'=>$result->arti_id,
          'inun_descr'=>$result->apre_und3,
          'inun_equil'=>$result->apre_equi3,
          'inun_estado'=>1,'inun_item'=>3,
          'inun_precio'=>$result->apre_prex6,
          'comision'=>$result->apre_comsion3,); 
          $obj[3]=array('insu_id'=>$result->arti_id,
          'inun_descr'=>$result->apre_und4,
          'inun_equil'=>$result->apre_equi4,
          'inun_estado'=>1,'inun_item'=>4,
          'inun_precio'=>$result->apre_pregrupo,
          'comision'=>$result->apre_comsion4); 
          $obj[4]=array('insu_id'=>$result->arti_id,
          'inun_descr'=>$result->apre_und5,
          'inun_equil'=>$result->apre_equi5,
          'inun_estado'=>1,'inun_item'=>4,
          'inun_precio'=>$result->apre_pre5,
          'comision'=>$result->apre_comsion5);
          $obj[5]=array('insu_id'=>$result->arti_id,
          'inun_descr'=>$result->apre_und6,
          'inun_equil'=>$result->apre_equi6,
          'inun_estado'=>1,'inun_item'=>4,
          'inun_precio'=>$result->apre_pre6,
          'comision'=>$result->apre_comsion6);
          $obj=array('status'=>1,'Lista'=>$obj);
  
        }else{
          $obj=array('status'=>9);
        }
        if($type=='JOSN'){
            return response()->json($obj);
        }else{
            return $obj;
        }
           
    }
    public function fill_cli($request){
        
    }
    public function fill_pedido($request){
        $response['titulo']='Lista de Pedidos a facturar';  
        $response['status']=0;
        $response['request']=$request;
		$response['message']='';
        try{  
            $data=$this->fill_pagin_pedido($request);
            $zone=$request['zone']; 
            $response['data']=$data; 
            $response['fecha']=date('d/m/Y'); 
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
            print_r($response);
        } 
      
        return view('maestro.fill_pedido_index',$response);
    }
    public function fill_pagin_pedido($request){
        $response['status']=0;
		$response['message']='';
        try{ 
            
            $tbl = "";
            $where=" tpdc_tipodoc='PED' AND fact_nrodocref IS NULL AND fact_estado=1 " ;
            $where.=' AND empr_id='."'".$request['empresa']."'";
           // $where.=' AND facturacion_cab.usua_id='."'".$request['usua']."'";
            $fdesde=$request['fdesde'];
            $fhasta=$request['fhasta'];
            
            if($request['cliente']!=''){
                $union='';
                $tex=explode('*', $request['cliente']);
                $cn=count($tex);
                if ($cn>0) {
                    for ($i=0; $i < $cn; $i++) { 
                        $union.=$tex[$i]."%";
                    }
                    $where.=' AND (clientes.clie_nomb LIKE '."'".$union."')"  ;
                }else{
                    $where.=' AND (clientes.clie_nomb LIKE '."'%".$request['cliente']."%')"  ;
                }
                
            }
            $fecha='';
            if($request['nrodoc']!=''){
                $where.='AND fact_nrodoc LIKE "%'.$request['nrodoc'].'%"';
            }else{ 
                $fecha=" AND DATE(fact_fecemi) BETWEEN '$fdesde' AND '$fhasta' ";
                
            }
             
            //$where.=$fecha;
            $zone=$request['zone'];
            $empresa=$request['empresa'];
            
            $paramt=$request['paramt'];
               
            $Lista =DB::table('facturacion_cab')
            ->select(DB::raw('clientes.clie_id,fact_estado,
            cdpg_nomb,tpdc_tipodoc,
            clie_nomb,fact_nrodoc,FORMAT(fact_impneto,2)AS importe,'.
            "DATE_FORMAT(fact_fecemi,'%d/%m/%Y')".' AS fecha'))
            ->Join('condicionespago','condicionespago.cdpg_id','=','facturacion_cab.cdpg_id')
            ->leftJoin('clientes','clientes.clie_id','=',
            DB::raw('facturacion_cab.clie_id'))  
            ->whereRaw($where)
            ->orderByRaw('fact_fecemi DESC, fact_nrodoc DESC')
            ->paginate(40);
               
                    $tbl2=''; 
            if (count($Lista)>0) {
              
                    foreach ($Lista as $key => $rst) {
                       
                            
                            $tbl .= "<div class='col-md-12'>";
                            $tbl .= '<ul class="list-group cursor" onclick="cargar_'.$paramt.'(this,'."'".$rst->fact_nrodoc."'".')'.'">
                            <li class="list-group-item active">'.$rst->clie_nomb.'</li>
                            <li class="list-group-item"> <b>N°:</b> '.$rst->fact_nrodoc.
                            ' <b>Importe:</b>'.$rst->importe.' </li>
                            <li class="list-group-item  "><b>Fecha:</b>'.$rst->fecha.'</li>
                           
                            </ul>'; 
                            $tbl .="</div>";
                        //} 
                    }
                 
            }else{
            $tbl .= "<div class='col-md-12'>No se Encontro Resultado</div>";
            } 
            $tbl2 .='<div class="col-md-12">';
            $tbl2  .=$Lista->setPath('pagin/Insumos'); 
            $tbl2 .='</div>';
            ob_start();
            echo $tbl;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['table']=$initial_content;
		    $response['theadPagin']=$initial_content2;
            $response['status']=1;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
            print_r($response);
        }
        return $response;

    }
    public function fill_producto($request){
        $response['titulo']='Lista de Articulos';  
        $response['status']=0;
        $response['request']=$request;
		$response['message']='';
        try{  
            $data=$this->fill_pagin_producto($request);
            $zone=$request['zone']; 
            $response['data']=$data; 
            $response['fecha']=date('d/m/Y'); 
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
            print_r($response);
        } 
      
        return view('maestro.fill_producto_index',$response);
    }
    public function fill_pagin_producto($request){
        $response['status']=0;
		$response['message']='';
        try{ 
            $stock=$request['stock'];
            $tbl = "";
            $whre='  insumos.insu_id IS NOT NULL ';
            if($stock=='SI'){
                $whre.=' AND kdin_estado=1 ';
            }else{

            }
            if($request['codigo']!=''){
              $whre.=' AND (insu_codalterno LIKE "%'.$request['codigo'].'%" OR insu_codbar LIKE "%'.$request['codigo'].'%" ';
            }
            if($request['decript']!=''){ 
                $union='';
                $tex=explode('*', $request['decript']);
                $cn=count($tex);
                if ($cn>0) {
                    for ($i=0; $i < $cn; $i++) { 
                        $union.=$tex[$i]."%";
                    }
                    $whre.=' AND (insu_nomb LIKE '."'".$union."')"  ;
                }else{
                    $whre.=' AND (insu_nomb LIKE '."'%".$request['decript']."')"  ;
                }
                
            }
            $zone=$request['zone'];
            $empresa=$request['empresa'];
            
            $paramt=$request['paramt'];
            $perf_pcosto=$request['perf_pcosto']=='01'?'enabled-pcosto':'disabled-pcosto';
            $perf_pventa=$request['perf_pventa']=='01'?'enabled-pventa':'disabled-pventa';
            $sql_selec='apre_comsion1,apre_comsion2,apre_comsion3,apre_comsion4,apre_comsion5,apre_comsion6';
                if($stock=='SI'){
                    $whre.=" AND  kcin_emprid='$empresa'";
                    $sql2 ='SELECT IF(empr_fecinvIns IS NULL,DATE(NOW()),DATE(empr_fecinvIns)) as fecha 
                    FROM  empresas WHERE empr_id ="'.$empresa.'"';
                    $fecha=date('Y-m-d');
                    $fechaD=$fecha;
                    $Lista2=DB::select($sql2);
                    if (count($Lista2)>0) {
                        foreach ($Lista2 as $key => $value) {
                            $fechaD=$value->fecha;
                        }
                    }

                    $whre.=" AND  DATE(kdin_fecemi) between '$fechaD' AND '$fecha'";
                    $Lista=DB::table('insumos')
                    ->select('insumos.insu_id',
                    DB::raw($sql_selec.",apre_und6,apre_und5,apre_equi5,apre_equi6,apre_und4,apre_equi4,insu_comision,insu_bonifcacion,SUM(IF (kdin_tipomov = 'I', IF(kdin_cantot IS NULL,0,kdin_cantot),
                    IF(kdin_cantot IS NULL,0,kdin_cantot) * -1)) AS stock,apre_lista AS p1,
                    apre_prex3 as p2,apre_prex6 as p3,apre_pregrupo as p4,apre_pre5 p5,apre_pre6 p6,apre_equi3,apre_equi2,apre_equi1,
                    apre_und3,apre_und2,apre_und1
                    "),'insumos.insu_nomb','insumos.insu_codalterno','insumos.unid_id','unidades.unid_uniconsumo','insumos.grpo_id','insumos.subg_id','insumos.insu_preccosto','insumos.insu_precventa','insumos.insu_stockmin','insumos.insu_estado')
                    ->join('dkardex_ins','dkardex_ins.insu_id','=','insumos.insu_id')
                    ->leftJoin('unidades','unidades.unid_id','=','insumos.unid_id')
                    ->leftJoin('grupos','grupos.grpo_id','=','insumos.grpo_id')
                    ->leftJoin('articulos_precios','articulos_precios.arti_id','=',
                    DB::raw('insumos.insu_id AND zone_id='."'$zone'"))
                    ->whereRaw($whre)
                    ->groupBy('insumos.insu_id')
                    ->havingRaw("SUM(IF (kdin_tipomov = 'I', IF(kdin_cantot IS NULL,0,kdin_cantot),
                    IF(kdin_cantot IS NULL,0,kdin_cantot) * -1))>0")
                    ->orderBy('insumos.insu_nomb','ASC')
                    ->paginate(150);
                }else{ 
                    $Lista=DB::table('insumos')
                    ->select('insumos.insu_id',
                    DB::raw($sql_selec.",apre_und6,apre_und5,apre_equi5,apre_equi6,apre_und4,apre_equi4,insu_bonifcacion,insu_comision,0 AS stock,apre_lista AS p1,
                    apre_prex3 as p2,apre_prex6 as p3,apre_pregrupo as p4,apre_pre5 p5,apre_pre6 p6,apre_equi3,apre_equi2,apre_equi1,
                    apre_und3,apre_und2,apre_und1
                    "),'insumos.insu_nomb','insumos.insu_codalterno','insumos.unid_id','unidades.unid_uniconsumo','insumos.grpo_id','insumos.subg_id','insumos.insu_preccosto','insumos.insu_precventa','insumos.insu_stockmin','insumos.insu_estado')
                    ->leftJoin('unidades','unidades.unid_id','=','insumos.unid_id')
                    ->leftJoin('grupos','grupos.grpo_id','=','insumos.grpo_id')
                    ->leftJoin('articulos_precios','articulos_precios.arti_id','=',
                    DB::raw('insumos.insu_id AND zone_id='."'$zone'"))
                    ->whereRaw($whre)
                    ->groupBy('insumos.insu_id')
                    ->orderBy('insumos.insu_nomb','ASC')
                    ->paginate(150);
                }
                    $tbl2=''; 
            if (count($Lista)>0) {
              
                    foreach ($Lista as $key => $rst) {
                        $p1='<i style="color:red">'.$rst->p1.'</i>|';
                        $p2='<i style="color:#33b5e5">'.$rst->p2.'</i>|';
                        $p3='<i style="color:#0486ff">'.$rst->p3.'</i>|'; 
                        $p4='<i style="color:#0486ff">'.$rst->p4.'</i>|'; 
                        $und=$rst->unid_uniconsumo;
                        
                        //if($rst->stock>0){
                            
                            $tbl .= "<div class='col-md-12'>";
                            $tbl .= '<ul class="list-group">
                            <li class="list-group-item active">'.$rst->insu_nomb.'</li>
                            <li class="list-group-item"> <b>CODIGO:</b> '.$rst->insu_codalterno.
                            ' <b>UND:</b>'.$und.' </li>
                            <li class="list-group-item '.$perf_pventa.'"><b>P.VENTA:</b>'.$p1.$p2.$p3.'</li>
                            <li class="list-group-item"><b>STOCK:</b> '.$rst->stock.' <b> BONIFICACIÓN: '.$rst->insu_bonifcacion.'
                            </b>
                            <div class="input-group mb-3"> 
                            <select class="remove_disabled" onchange="pasarUnid_frmcalculo(this)"  style="width: 15%;">
                            <option value="1"  >UND</option>
                            <option value="2"   >CAJA</option>
 
                            </select>
                            <input data-stock="'.$rst->stock.'" data-stockactual="'.$rst->stock.'"  class="remove_disabled form-control  text-left  form-control-sm" onkeyup="calcTotItem_frmcalculo(this)"   value="1">
                            <select class="remove_disabled" onchange="pasarvalor_frmcalculo(this)"  style="width: 25%;">
                            <option value="1" selected disabled data-comisioninsumo="'.$rst->apre_comsion1.'" data-ingresequivauni="'.$rst->apre_equi1.'" data-ingrespreciouni="'.$rst->p1.'" data-contenido="'.$rst->apre_und1.' X '.((int)$rst->apre_equi1).' '.$und.'">'.$rst->apre_und1.' X '.((int)$rst->apre_equi1).' '.$und.' S/ '.$rst->p1.'</option>
                            <option value="2" disabled data-comisioninsumo="'.$rst->apre_comsion2.'"  data-ingresequivauni="'.$rst->apre_equi2.'" data-ingrespreciouni="'.$rst->p2.'" data-contenido="'.$rst->apre_und2.' X '.((int)$rst->apre_equi2).' '.$und.'">'.$rst->apre_und2.' X '.((int)$rst->apre_equi2).' '.$und.' S/  '.$rst->p2.'</option>
                            <option value="3" disabled  data-comisioninsumo="'.$rst->apre_comsion3.'" data-ingresequivauni="'.$rst->apre_equi3.'" data-ingrespreciouni="'.$rst->p3.'" data-contenido="'.$rst->apre_und3.' X '.((int)$rst->apre_equi3).' '.$und.'">'.$rst->apre_und3.' X '.((int)$rst->apre_equi3).' '.$und.' S/  '.$rst->p3.'</option>
                            <option value="4" disabled data-comisioninsumo="'.$rst->apre_comsion4.'" data-ingresequivauni="'.$rst->apre_equi4.'" data-ingrespreciouni="'.$rst->p4.'" data-contenido="'.$rst->apre_und4.' X '.((int)$rst->apre_equi4).' '.$und.'">'.$rst->apre_und4.' X '.((int)$rst->apre_equi4).' '.$und.' S/  '.$rst->p4.'</option>
                            <option value="5"  disabled data-comisioninsumo="'.$rst->apre_comsion5.'" data-ingresequivauni="'.$rst->apre_equi5.'" data-ingrespreciouni="'.$rst->p5.'" data-contenido="'.$rst->apre_und5.' X '.((int)$rst->apre_equi5).' '.$und.'">'.$rst->apre_und5.' X '.((int)$rst->apre_equi5).' '.$und.' S/  '.$rst->p5.'</option>
                            <option value="6" disabled  data-comisioninsumo="'.$rst->apre_comsion6.'" data-ingresequivauni="'.$rst->apre_equi6.'" data-ingrespreciouni="'.$rst->p6.'" data-contenido="'.$rst->apre_und6.' X '.((int)$rst->apre_equi6).' '.$und.'">'.$rst->apre_und6.' X '.((int)$rst->apre_equi6).' '.$und.' S/  '.$rst->p6.'</option>
                            </select>
                            <select class="remove_disabled" onchange="campre_frmcalculo(this,this.value)"><option value="1">P</option><option value="4">B</option></select>
                            <input disabled class="form-control remove_disabled  text-right  form-control-sm"  onkeyup="calcularimporte_frmcalculo(this)" value="'.$rst->p1.'" onblur="toVDecimal(event)">
                            <input disabled class="form-control   text-right  form-control-sm" value="'.$rst->p1.'">
                            <input disabled type="hidden" value="'.$rst->apre_equi1.'">
                            <input disabled type="hidden" value="'.$rst->apre_und1.' X '.((int)$rst->apre_equi1).' '.$und.'">
                            <input disabled type="hidden" value="1">
                            <input disabled type="hidden" value="'.$rst->insu_bonifcacion.'">
                            <input disabled type="hidden" value="'.$rst->apre_comsion1.'">
                            <input disabled type="hidden" value="'.$rst->insu_nomb.'">
                            <input disabled type="hidden" value="'.$rst->insu_codalterno.'">
                            <input disabled type="hidden" value="'.$und.'">
                            ';
                            $tbl .='<button type="button"     onclick="guardardet_'.$paramt.'(this,'.$rst->stock.",'".$rst->insu_id."'".')" class="btn btn-primary btn-sm btn-outline-primary   ">
                                <i class="spw spw-diskette"></i> </button>
                            </div> 
                            </li>
                            </ul>'; 
                            $tbl .="</div>";
                        //} 
                    }
                 
            }else{
            $tbl .= "<div class='col-md-12'>No se Encontro Resultado</div>";
            } 
            $tbl2 .='<div class="col-md-12">';
            $tbl2  .=$Lista->setPath('pagin/Insumos'); 
            $tbl2 .='</div>';
            ob_start();
            echo $tbl;
            $initial_content = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $tbl2;
            $initial_content2 = ob_get_contents();
            ob_end_clean();
            $response['table']=$initial_content;
		    $response['theadPagin']=$initial_content2;
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;

    }
}