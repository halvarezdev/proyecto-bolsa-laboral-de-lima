<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Facades\Lang;

class PHPMailerController extends Controller
{

    private $portEnv;
    private $hostEnv;
    private $usernameEnv;
    private $passwordEnv;
    private $encryptionEnv;
    private $authEnv;
    private $enviar_correo_log;

    public function __construct(){
        $this->portEnv = env('MAIL_PORT');
        $this->hostEnv = env('MAIL_HOST');
        $this->usernameEnv = env('MAIL_USERNAME');
        $this->passwordEnv = env('MAIL_PASSWORD');
        $this->encryptionEnv = env('MAIL_ENCRYPTION');
        $this->authEnv = env('MAIL_AUTH');
        $this->mailCopy = env('MAIL_COPY');
        $this->enviar_correo_log= env('MAIL_CORREOS_LOG');
    }

     // =============== [ Email ] ===================
     public function email() {
        return view("email");
    }
 
 
    // ========== [ Compose Email ] ================
    public function verifyEmail($to) {
        require base_path("vendor/autoload.php");
        $mail = new PHPMailer(true);     // Passing `true` enables exceptions

        $html = '<h4>Bolsa de Trabajo, su portal de empleo, le da la bienvenida</h4>';
        $html .= '<p>Por favor haga clic en el botón para ingresar a su cuenta</p>';
        $html .= '<a href="'.asset("/").'" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1">Ingrese aqui</a><br><br>';
        $html .= 'Si no creó una cuenta, no se requiere ninguna acción adiciona<br><br>';
        $html .= 'Municipalidad de Lima';
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $this->hostEnv;             //  smtp host
            $mail->SMTPAuth = $this->authEnv;
            $mail->Username = $this->usernameEnv;   //  sender username
            $mail->Password = $this->passwordEnv;       // sender password
            $mail->SMTPSecure = $this->encryptionEnv;   // encryption - ssl/tls
            $mail->Port = $this->portEnv;               // port - 587/465

            $correoRemitente = ($this->usernameEnv.date("YmdHis")."@munlima.gob.pe");
 
            $mail->setFrom($this->usernameEnv, 'Bienvenido a Bolsa de Trabajo');
            $mail->addAddress($to);
            $mail->addBCC($this->mailCopy);

            $mail->isHTML(true);                // Set email content format to HTML
 
            $mail->Subject = Lang::getFromJson('Bienvenido a Bolsa de Trabajo');
            $mail->Body    =  $html;
 
            if( !$mail->send() ) {
                return view('auth.passwords.reset-contacto')->with(
                    ['token' => $token, 'email' => $request->email]
                );
                //return back()->with("failed", "Email not sent.")->withErrors($mail->ErrorInfo);
            }
            
            else {
                 $text='Message has been sent';
                 return $text;
                // return back()->with("success", "Email has been sent.");
            }
 
        } catch (Exception $e) {
            $eturn= 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo; 
            return $eturn;
            //return back()->with('error','Message could not be sent.');
        }
    }
    public function verifyEmailLog($html) {
        require base_path("vendor/autoload.php");
        if($this->enviar_correo_log==''){
            return $this->enviar_correo_log.'Colocar correo para enviar .env MAIL_CORREOS_LOG ';
        }
        $mail = new PHPMailer(true);     // Passing `true` enables exceptions

        
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $this->hostEnv;             //  smtp host
            $mail->SMTPAuth = $this->authEnv;
            $mail->Username = $this->usernameEnv;   //  sender username
            $mail->Password = $this->passwordEnv;       // sender password
            $mail->SMTPSecure = $this->encryptionEnv;   // encryption - ssl/tls
            $mail->Port = $this->portEnv;               // port - 587/465
            $mail->CharSet = 'UTF-8';
            $correoRemitente = ($this->usernameEnv.date("YmdHis")."@munlima.gob.pe");
 
            $mail->setFrom($this->usernameEnv, 'Municipalidad Metropolitana de lima');
            $corLog=explode(',',$this->enviar_correo_log);

            $mail->addAddress($corLog[0]);
            for ($i=1; $i < count($corLog); $i++) { 
                $mail->addBCC($corLog[$i]);
            }
           // $mail->addBCC('abadrichard1996@gmail.com');

            $mail->isHTML(true);                // Set email content format to HTML
 
            $mail->Subject = '¡URGENTE - PROBLEMA EN BOLSA LABORAL!';
            $mail->Body    =  $html;
 
            if( !$mail->send() ) {
                // return view('auth.passwords.reset-contacto')->with(
                //     ['token' => $token, 'email' => $request->email]
                // );
                //return back()->with("failed", "Email not sent.")->withErrors($mail->ErrorInfo);
            }
            
            else {
                 $text='Message has been sent';
                 return $text;
                // return back()->with("success", "Email has been sent.");
            }
 
        } catch (Exception $e) {
            $eturn= 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo; 
            return $eturn;
            //return back()->with('error','Message could not be sent.');
        }
    }
}
