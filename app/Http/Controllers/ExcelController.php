<?php
namespace App\Http\Controllers;
use App\Empresa;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;
use App\User;

class ExcelController extends Controller{

    public function listadoEmpresaExcel(){

        Excel::create('Empresa', function($excel) {
        
            $empresas = Empresa::join('contactos','contactos.empresa_id','=','empresas.id')->get();
            $excel->sheet('Empresa', function($sheet) use($empresas) {
            $sheet->fromArray($empresas);
        
        });
        
        })->export('xlsx');

        echo "entro en todo";
	}

    public function listadoPersonaExcel(){

        Excel::create('Persona', function($excel) {

            $personas = User::join('personas','personas.id','=','users.persona_id')
            ->join('fichapersona','fichapersona.persona_id','=','personas.id')
            ->get();

            $excel->sheet('Empresa', function($sheet) use($personas) {
                $sheet->fromArray($personas);
            });

        })->export('xlsx');

        echo "entro en todo";

    }


}