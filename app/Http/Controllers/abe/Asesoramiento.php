<?php

namespace App\Http\Controllers\abe;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Asesoramiento extends Controller
{
    public function descubriendo(){
     
      return view('abe/descubriendo');
        
    }

    public function herramientas(){
        return view('abe/herramientas');
    }

    public function afrontar(){
        return view('abe/afrontar');
    }
}
