<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controles;

class Home extends Controller{
    public function index(){
        $data['rando']=date('YmdHis').rand();     
        return view('login_view',$data);
    }
    public function inicio($count,$ide,$usert){
      
      
         
		$exste=session('_SESSION_');
         
		if(is_array($exste)){
            if(isset($exste[$count])){
                $user_session=$exste[$count];
                
            }else{
                $user_session=$exste[0];
            }
			
			$programas=$this->get_programas_all($ide,$usert);
			$data['user_session']=$user_session;
            $data['programas']=$programas;
            $data['sessio_all']=$exste; 
            $empresa=Controles::get_empresas($usert);
            
             
            $data['empresa']=$empresa;
			$data['rand']=date('YmdHis').rand();
			return view('plantillas',$data);
		}else{
			$data['rand']=date('YmdHis').rand();
			return view('login_view',$data);
		}
	}
    public function salir(Request $request){
        \Session::flush();
        $obj=array('data'=>'ok');
        return $obj;
    }
    public function ingresar(Request $request){
        $response['status']=0;
        $response['message']='';
        try{ 
        $zone_id='';
         $user=$request->user;
         $pass=$request->pass;
         $company=$request->company;
         $rtn=array();
         $navegador=$this->obtenerNavegadorWeb();
         $dias = array("domingo","lunes","martes","miercoles","jueves","viernes","sabado");
        $sql = " SELECT vend_id,usua_dias,perf_pfpag,perf_pfemi,perf_pcosto,perf_pventa,usua_foto,
        perf_nomb,usua_clave,usua_serie,p.perf_id,usu.usua_id,usua_email,usua_usuario
         FROM usuarios usu INNER JOIN perfiles p ON p.perf_id=usu.perf_id";
        if ($company != null) 
            $sql .= " INNER JOIN administracion adm ON usu.usua_id = adm.usua_id";
            
            $sql .= " WHERE usua_usuario = '" .$user. "' AND usua_clave = '" .$pass. "'";
        if ($company != null) 
            $sql .= " AND adm.empr_id = '". $company . "'";
         
        $existeuser=DB::select($sql) ;
        if (count($existeuser)>0) {
            $row_user=$existeuser[0];
            if($row_user->usua_dias=='')
                throw new \Exception('Solicite Permiso para su cuenta ('.$user.') de las Hora  y Dia');
            
            $dia=$dias[date("w")];
            $data_dia=$this->getSplit($row_user->usua_dias);
            $hora=$data_dia[$dia];
            
            if($hora['dia']=='false')
                throw new \Exception('Su cuenta ('.$h.$user.') No tiene permiso para el dia de hoy '.$dia);
            
            $h=date('H');
            if($hora['desde']>$h)
                throw new \Exception('Su Horario de Acceso al sistema es desde: '.$hora['desde'].' horas');    
            
            if($hora['hasta']<$h)
                throw new \Exception('Su horario de Acceso al sistema es hasta: '.$hora['hasta'].' horas');    

            $sql1="SELECT empr_nomb, zone_id, empr_id, empr_social,empr_ruc,empr_abrev FROM  empresas where empr_id=". $company;

            $det=DB::select($sql1) ; 
            $row_empr=$det[0];
           
            $sesion=session('countusert');
			if($sesion==''){
				$sesion=0;
			}
			$obj=array(
				'vend_id' => $row_user->vend_id,
				'usua_usuario' => $row_user->usua_usuario,
				'usua_foto' => $row_user->usua_foto,
				'usua_clave' => $row_user->usua_clave,
				'perf_id'   => $row_user->perf_id,
				'usua_dias'   => $row_user->usua_dias,
				'usua_id'   => $row_user->usua_id,
				'zone_id'   => $row_empr->zone_id,
				'empr_id'   => $row_empr->empr_id,
				'empr_ruc'   => $row_empr->empr_ruc,
				'empr_abrev' => $row_empr->empr_abrev,
				'perf_nomb' => $row_user->perf_nomb,
				'perf_pfpag' => $row_user->perf_pfpag,
				'perf_pcosto' => $row_user->perf_pcosto,
				'perf_pventa' => $row_user->perf_pventa,
				'perf_pfemi' => $row_user->perf_pfemi,
                'empr_social'=>  $row_empr->empr_social,
                'empr_nomb'=>  $row_empr->empr_nomb, 
            );        
            
            $exste=session('_SESSION_');
			if(is_array($exste)){				 
				$objeto=array();
				$array_session=session('_SESSION_');
				$user_exite=1;
			
				foreach ($array_session as $key => $value) {
					$objeto[]=$value;
					if($value['usua_id']==$row_user->usua_id){
						$user_exite=0;
						break;
					}
				}					
				if($user_exite==1){
					$sesion=$sesion+1;
					$obj['counts']=$sesion; 
					$objeto[]=$obj;
					$array_session['countusert']=$sesion;
					$newdata =array('countusert'=>$sesion,
							'_SESSION_'=>$objeto,);		
					session($newdata);
                    $fecha=date('Y-m-d H:i:S');
                    DB::insert('insert into user_login (uslg_usuario,uslg_agente,uslg_nombre,uslg_remote,uslg_platforma,uslg_fecha)
                     values (?, ?,?, ?,?, ?)', [$user,$navegador['agente'],$navegador['nombre'],$navegador['remote'],$navegador['platforma'],$fecha]);
				}
			}else{
				$obj['counts']=$sesion;
				$obj_sess[]=$obj;
				$newdata =array('countusert'=>$sesion,
							'_SESSION_'=>$obj_sess
						);
				session($newdata);
                $fecha=date('Y-m-d H:i:S');
                    DB::insert('insert into user_login (uslg_usuario,uslg_agente,uslg_nombre,uslg_remote,uslg_platforma,uslg_fecha)
                     values (?, ?,?, ?,?, ?)', [$user,$navegador['agente'],$navegador['nombre'],$navegador['remote'],$navegador['platforma'],$fecha]);
			}				
			$obj['counts']=$sesion;
			$response['status']=1;
			$response['response']=$obj;
			$response['sesion']=$sesion;
			$response['message']='Ingresar al sistema';
        }else{
            $rtn = array( "success"=>FALSE,
                      "smg"=>'Usuario no existe',
            );
        }
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage();
        }
          return json_encode($response,JSON_PRETTY_PRINT);
    }
    public function getEmpresaLogin(Request $request){
        $response['status']=0;
        $response['message']='';
        try{  
            $rtn=array();
            $fic=date('Y-m-d');
            $user=$request->user;
            $existeuser1=array();
            $limit=1;
            $sql1=DB::select('SELECT sist_emplimit  FROM sistemas WHERE sist_activo = 1 LIMIT 1');
            if (count($sql1)>0) {
                $limit= $sql1[0]->sist_emplimit;
            }

            $sql = "SELECT us.usua_usuario,emp.empr_id, empr_nomb, empr_direcc, "
                    ."   empr_ubic, empr_tipo"
                    ." FROM empresas emp"
                    ." INNER JOIN administracion adm ON adm.empr_id = emp.empr_id"
                    ." INNER JOIN usuarios us ON us.usua_id = adm.usua_id "
                    ." WHERE us.usua_usuario =? "
                    ." AND us.usua_estado = 1"
                    ." ORDER BY emp.empr_id LIMIT 0," . $limit;
            $existeuser1=DB::select( $sql,[$user]);
            $empresa=array();
            if (count($existeuser1)>0) {
                foreach ($existeuser1 as $key => $value) {
                    $existeuser[]= $value;
                }
                $response['empresa']=$existeuser;
                $response['status']=1;
            }else{
                $response['message']='Usuario no existe';
            }

            
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage();
        }
        return json_encode($response,JSON_PRETTY_PRINT);
    }
    public function get_change_emp(Request $request){
        $response['status']=0;
        $response['message']='';
        try{ 
            $exste=session('_SESSION_');
            $response['exste']=$exste;
            if(is_array($exste)){   
                $sql1="SELECT empr_nomb, zone_id, empr_id, empr_social,empr_ruc,empr_abrev 
                FROM  empresas where empr_id='". $request->ide."'";
               $det=DB::select($sql1) ; 
               $row_empr=$det[0];
                $exste[$request->count_session]['zone_id']=$row_empr->zone_id;
                $exste[$request->count_session]['empr_id']=$request->ide;
				$exste[$request->count_session]['empr_ruc']=$request->ruc;
				$exste[$request->count_session]['empr_abrev']= $request->abre;
                $exste[$request->count_session]['empr_social']= $request->compania;
                $exste[$request->count_session]['empr_nomb']= $request->namemp; 
                
                $sesion=count($exste);
                $newdata =array('countusert'=>$sesion,
                            '_SESSION_'=>$exste,);		
                session($newdata);
                $response['newdata']=$newdata;
                $response['session']= $exste[$request->count_session];
                $response['status']=1;
            } 
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage();
        }
        return json_encode($response,JSON_PRETTY_PRINT);
    }
    public function get_programas_all($ide,$usert){
		$sql='SELECT prog_manager ,prg.modl_id,mdl.modl_nomb, prg.prog_nomb,prog_icosp,prg.prog_formv2,
			prog_panel,prog_border, prog_lobiPanel,modl_icon,prog_namejs,prg.prog_id
			FROM programas prg 
			INNER JOIN implementacion ipl ON prg.prog_id = ipl.prog_id 
			INNER JOIN sistemas sis ON ipl.sist_id = sis.sist_id 
			INNER JOIN modulos mdl ON prg.modl_id = mdl.modl_id 
			INNER JOIN accesos acc ON prg.prog_id = acc.prog_id  AND acce_estado=1
			INNER JOIN perfiles prf ON acc.perf_id = prf.perf_id
			INNER JOIN usuarios usr ON usr.perf_id = prf.perf_id 
			WHERE prog_tipo="G" AND sis.sist_id =1 AND sis.sist_activo =1  AND usr.usua_id= ?
			ORDER BY  modl_orden, prog_orden';
		$response['status']=0;
        $response['message']='';
        $response['response']=array();
		try{  
			 
			$row_user=DB::select($sql,[$usert]);
			 
			$group_by=array();
			foreach ($row_user as $key => $value) {
				$group_by[$value->modl_id][]=$value;
			}
			$response['status']=1;
			$response['usert']=$usert;
			$response['response']=$group_by;
		}catch (\Exception $exc) {
		 	$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
		}	
		return $response;
	}
    function obtenerNavegadorWeb(){
        $agente = $_SERVER['HTTP_USER_AGENT'];
        $navegador = '';
        $platforma = '';
        $version= "";
        #Obtenemos la Plataforma
        if (preg_match('/linux/i', $agente)) {
        $platforma = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $agente)) {
        $platforma = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $agente)) {
        $platforma = 'windows';
        }
        #Obtener el UserAgente
        if(preg_match('/MSIE/i',$agente) && !preg_match('/Opera/i',$agente))
        {
        $navegador = 'Internet Explorer';
        $navegador_corto = "MSIE";
        }
        elseif(preg_match('/Firefox/i',$agente))
        {
        $navegador = 'Mozilla Firefox';
        $navegador_corto = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$agente))
        {
        $navegador = 'Google Chrome';
        $navegador_corto = "Chrome";
        }
        elseif(preg_match('/Safari/i',$agente))
        {
        $navegador = 'Apple Safari';
        $navegador_corto = "Safari";
        }
        elseif(preg_match('/Opera/i',$agente))
        {
        $navegador = 'Opera';
        $navegador_corto = "Opera";
        }
        elseif(preg_match('/Netscape/i',$agente))
        {
        $navegador = 'Netscape';
        $navegador_corto = "Netscape";
        }
        #Obtenemos la Version
        
       
        /*Resultado final del Navegador Web que Utilizamos*/ 
        $remote=$_SERVER['REMOTE_ADDR'];
            return array(
            'agente' => $agente,
            'nombre'      => $navegador, 
            'remote'  => $remote,
            'platforma'  => $platforma,
            );
    } 
    public function getSplit($dia){
        $objeto=array();
        $dias=explode(';',$dia); 
         
        for ($i=0; $i < count($dias); $i++) {  
            if(isset($dias[$i])){
            $data=explode('|',$dias[$i]);
            $ob=$this->getDias($i);
            $domingo=isset($data[0])?$data[0]:false; 
            $objeto[$ob]=array('dia'=>$domingo,
            'desde'=>isset($data[1])?$data[1]:0,
            'hasta'=>isset($data[2])?$data[2]:0);
            }
        }
      return  $objeto;
    }
    public function getDias($i){
        $dia[0]='lunes';
        $dia[1]='martes';
        $dia[2]='miercoles';
        $dia[3]='jueves';
        $dia[4]='viernes';
        $dia[5]='sabado';
        $dia[6]='domingo';
        return $dia[$i];
    }
}
