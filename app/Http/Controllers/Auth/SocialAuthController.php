<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FichaPersona;
use App\Persona;
use Socialite;
use App\User;
use Auth;

class SocialAuthController extends Controller
{
    // Metodo encargado de la redireccion a Facebook
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    // Metodo encargado de obtener la información del usuario
    public function handleProviderCallback($provider)
    {
        // Obtenemos los datos del usuario
        try {
            $social_user = Socialite::driver($provider)->user();
        } catch (InvalidStateException $e) {
            $social_user = Socialite::driver($provider)->stateless()->user();
        }
        // Comprobamos si el usuario ya existe
        if ($user = User::where('email', $social_user->email)->first()) { 
            return $this->authAndRedirect($user); // Login y redirección
        } else {  
            // En caso de que no exista creamos un nuevo usuario con sus datos.
            $persona = Persona::create([
               // 'tipo_documento' => '1',
               // 'numero_documento' => '00000000',
                'email' => $social_user->email,
                'name' => $social_user->name,
                //'ape_pat' => $data['ape_pat'],
               // 'ape_mat' => $data['ape_mat'],
                'cod_entidad' => '1',
            ]);

            $user = User::create([
                'name' => $social_user->name,
                'email' => $social_user->email,
                'persona_id' => $persona->id,
               // 'avatar' => $social_user->avatar,
            ]);
            
            $ficha = FichaPersona::create([
                'persona_id' => $persona->id,
               // 'ocu_trabajo' => $data['ocu_trabajo'],
               // 'ubi_lugar' => $data['ubi_lugar'],
                'cod_entidad' => '1',
                'tipo_entidad' => '1',
            ]);


            return $this->authAndRedirect($user); // Login y redirección
        }
    }

    // Login y redirección
    public function authAndRedirect($user)
    {
        Auth::login($user);

        return redirect()->to('/home#');
    }

    public function publishToProfile(Request $request){
        try {
            $response = $this->api->post('/me/feed', [
                'message' => $request->message
            ])->getGraphNode()->asArray();
            if($response['id']){
               // post created
            }
        } catch (FacebookSDKException $e) {
            dd($e); // handle exception
        }
    }
    
}
