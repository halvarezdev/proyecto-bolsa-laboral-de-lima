<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Auth\ContactoLoginController;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Entidad;
use App\TipoDocumento;
use App\Contacto;
use App\Persona;
use App\Empresa;
use App\Ubigeo;
use App\Sector;


class ContactoRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/contacto';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:contacto', ['except' => ['logout']]);
    }

    /**
     * Get a validator for an incoming registration $data.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:contactos'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'numero_documento' => ['required', 'unique:empresas'],
           // 'numero_documento' => ['required', 'unique:contactos'],
          //  'numero_documento' => ['required', 'unique:personas'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       /* $persona = Persona::create([
            'tipo_documento' => $data['tipo_documento_contacto'],
            'numero_documento' => $data['numero_documento_contacto'],
            'telefono' => $data['telefono'],
            'email' => $data['email_contacto'],
            'name' => $data['name'],
        ]);
       */
        $empresa = Empresa::create([
            'tipo_documento' => $data['tipo_documento'],
            'numero_documento' => $data['numero_documento'],
            'razon_social' => $data['razon_social'],
            'descripcion' => $data['descripcion'],
            'rubro' => $data['rubro'],
            'sitio_web' => (isset($data['sitio_web']) ? $data['sitio_web'] : ""),
            'direccion' => $data['direccion'],
            'ubigeo' => $data['departamento'].$data['provincia'].$data['distrito'],
            'cod_entidad' => $data['entidad'],
            'tipo_entidad' => '1',
        ]);
        
        $contacto = Contacto::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'cargo' => $data['cargo'],
            'numero_documento' => $data['numero_documento_contacto'],
            'empresa_id' => $empresa->id,
            'telefono' => $data['telefono'],
            'tipo_documento' => $data['tipo_documento_contacto'],
           // 'persona_id' => $persona->id,
            'cod_entidad' => $data['entidad'],
        ]);

        return $contacto;



    }

    function registroFormulario(){

        $entidad = Input::get('e');
        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
        }

        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
        $data['ubigeo'] = Ubigeo::where($where)->get();
        $data['documentos'] = TipoDocumento::orWhere('id', '=', '2')
                ->orWhere('id', '=', '5')
                ->orWhere('id', '=', '6')
                ->orderBy('id', 'desc')
                ->get();
        $data['sector'] = Sector::get();    
        $data['entidad'] = Input::get('e');

        return view('auth/contacto-register', $data);
    }
    function CondicionesFormulario(){

        $entidad = Input::get('e');
        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
        }

        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
        $data['ubigeo'] = Ubigeo::where($where)->get();
        $data['documentos'] = TipoDocumento::orWhere('id', '=', '2')
                ->orWhere('id', '=', '5')
                ->orWhere('id', '=', '6')
                ->orderBy('id', 'desc')
                ->get();
        $data['sector'] = Sector::get();    
        $data['entidad'] = Input::get('e');

        return view('auth/contacto-terminos', $data);
    }
    protected function guard()
    {
        return Auth::guard('contacto');
    }
    
  
}
