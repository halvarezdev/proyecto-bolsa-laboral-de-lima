<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'userLogout']]);
    }

    public function showLoginForm()
    {
        $data['entidad'] = Input::get('e');

        return view('auth.login',$data);
    }


    public function loginJson(Request $request)
    {
        /*$validatedData = $request->validate([
            'g-recaptcha-response' => 'required|recaptchav3:user,0.5'
        ]);*/

        $value = "0";
        // attempt to log the user in
        if (Auth::guard('web')->attempt([ 'email' => $request->email, 'password' => $request->password ], $request->remenber)) {
            // if successful, then to their intened location
            $value = "1";
            //return redirect()->intended(route('contacto.dashboard'));

        }

       // return redirect()->back()->withInput($request->only('email', 'remenber'));
       return json_encode($value);

    }

     public function userLogout($entidad)
    {
        Auth::guard('web')->logout();

       // $request->session()->flush();
       // $request->session()->regenerate();
        return redirect('/'.$entidad);
    }


    /**** SOCIALITE ***/

      public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }



    /**
     *
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
   /* esto es un aprueba total */


}
