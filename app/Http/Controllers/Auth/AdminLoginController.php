<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Entidad;    
use App\Admin;
use Auth;

class AdminLoginController extends Controller
{

    protected $redirectTo = 'auth.admin-login';

    
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logoutAdmin']]);
    }

    public function showLoginForm()
    {
        $data['entidad'] = Input::get('e');
        $entidades = Entidad::where('id','=',$data['entidad'])
        ->where('enti_flag','=','1')
        ->get();

        foreach($entidades as $value){
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['enti_nombre'] = $value->enti_nombre;
        }
        return view('auth.admin-login', $data);
    }

    public function login(Request $request)
    {
        // validate the form data
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        // attempt to log the user in
        if (Auth::guard('admin')->attempt([ 'email' => $request->email, 'password' => $request->password ], $request->remenber)) {
            // if successful, then to their intened location
         // echo "que m";
           // return redirect()->intended(route('admin.dashboard'));
        }
        
       return redirect()->back()->withInput($request->only('email', 'remenber'));

        // if unsuccessful, then redirect back to the login with the form data
    }

    public function logoutAdmin()
    {
        Auth::guard('admin')->logout();
        $entidad = Input::get('e');
        echo $entidad;
      //  $request->session()->flush();
      //  $request->session()->regenerate();
      return redirect()->action('Auth\AdminLoginController@showLoginForm', ['e' => $entidad]);

    }


    protected function guard()
    {
        return Auth::guard('admin');
    }

    protected function redirectTo()
    {
       
        return route('admin.dashboard');

    }

}
