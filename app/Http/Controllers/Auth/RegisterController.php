<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\FichaPersona;
use App\TipoDocumento;
use App\Ocupacion;
use App\Entidad;
use App\Persona;
use App\Ubigeo;
use App\User;
use App\Area;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = array
        (
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'tipo_documento' => ['required', 'string'],
            'numero_documento' => ['required', 'unique:users'],
        );
   

        $messages = array
        (
            'required'   => 'El campo es obligatorio.',
            'different'  => 'El campo ubicacion y traslado deben ser diferente.',
            'unique' => 'Este dato ya se encuentra registrado en nuestras base de datos',
            'date'       => 'La fecha es invalida',
        );
  

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $persona = Persona::create([
            'tipo_documento' => $data['tipo_documento'],
            'numero_documento' => $data['numero_documento'],
            'email' => $data['email'],
            'name' => $data['name'],
            'ape_pat' => $data['ape_pat'],
            'ape_mat' => $data['ape_mat'],
            'cod_entidad' => $data['entidad'],
        ]);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'numero_documento' => $data['numero_documento'],
            'persona_id' => $persona->id,
            'cod_entidad' => $data['entidad'],
        ]);

        $ficha = FichaPersona::create([
            'persona_id' => $persona->id,
            'ocu_trabajo' => $data['ocu_trabajo'],
            'ubi_lugar' => $data['ubi_lugar'],
            'cod_entidad' => $data['entidad'],
            'tipo_entidad' => '1',
        ]);

        // $objeto = new \App\Http\Controllers\PHPMailerController();
        // $myVariable = $objeto->verifyEmail($data['email']);

        return $user;

    }

    public function showRegistrationForm()
    {

        $entidad = Input::get('e');
        $entidades = Entidad::where('id','=',$entidad)
        ->where('enti_flag','=','1')
        ->get();


        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
        }

        $where = ['ubi_codprov' => '00' , 'ubi_coddist' => '00'];
      
        $data['documentos'] = TipoDocumento::get();
        $data['ubigeo'] = Ubigeo::where($where)->get();
        $data['ocupaciones'] = Ocupacion::where('status','=','9')->get();
        $data['areas'] = Area::get();  
        
        return view('auth.register', $data);
    }

    
}
