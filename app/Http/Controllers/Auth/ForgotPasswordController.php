<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    // Comentamos esto que no nos hace falta
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }


    public function showLinkRequestForm()
    {
        $name_entidad = Input::get('e');
        $entidades = Entidad::where('id','=',$name_entidad)
        ->where('enti_flag','=','1')
        ->get();
        $data=[];
        foreach($entidades as $value){
            $data['enti_logo'] = $value->enti_logo;
            $data['entidad'] = $value->id;
            $data['entidad_color'] = $value->color_entidad;
            $data['btn_buscar'] = $value->btn_buscar;
            $data['login_logo_persona'] = $value->login_logo_persona;
            $data['subcolor_entidad'] = $value->subcolor_entidad;
            $data['subsubcolor_entidad'] = $value->subsubcolor_entidad;
            $data['foto_banner1'] = $value->foto_banner1;
        }

        return view('auth.passwords.email', $data);
    }

    // Añadimos las respuestas JSON, ya que el Frontend solo recibe JSON
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
  
        switch ($response) {
            case \Password::INVALID_USER:
                return response()->error($response, 422);
                break;

            case \Password::INVALID_PASSWORD:
                return response()->error($response, 422);
                break;

            case \Password::INVALID_TOKEN:
                return response()->error($response, 422);
                break;
            default: 
                return response()->success($response, 200);
        }
    }
}
