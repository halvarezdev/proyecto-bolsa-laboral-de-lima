<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\EntidadUser;
use App\Entidad;
use Auth;

class EntidadUserLoginController extends Controller
{

    protected $redirectTo = 'auth.entidad-usuario-login';

    
    public function __construct()
    {
        $this->middleware('guest:entidad', ['except' => ['entidadLogout']]);
    }

    public function showLoginForm()
    {   
        $data['entidad'] = Input::get('e');
        $entidades = Entidad::where('id','=',$data['entidad'])
        ->where('enti_flag','=','1')
        ->get();

      foreach($entidades as $value){
          $data['login_logo_persona'] = $value->login_logo_persona;
          $data['enti_nombre'] = $value->enti_nombre;
      }

        return view('auth.entidad-usuario-login',$data);

    }

    public function login(Request $request)
    {
        // validate the form data
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        // attempt to log the user in
        if (Auth::guard('entidad')->attempt([ 'email' => $request->email, 'password' => $request->password ], $request->remenber)) {
            // if successful, then to their intened location
           //echo "exitoso";
           // return redirect()->intended(route('entidad.dashboard'));
        }
        
      return redirect()->back()->withInput($request->only('email', 'remenber'));

        // if unsuccessful, then redirect back to the login with the form data
    }

    public function entidadLogout()
    {
        Auth::guard('entidad')->logout();
        $entidad = Input::get('e');
        echo $entidad;
      //  $request->session()->flush();
      //  $request->session()->regenerate();
      //  return redirect('/');
      return redirect()->action('Auth\EntidadUserLoginController@showLoginForm', ['e' => $entidad]);
    }


    protected function guard()
    {
        return Auth::guard('entidad');
    }


}
