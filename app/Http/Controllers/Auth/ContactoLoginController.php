<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Contacto;
use App\Persona;
use App\Empresa;
use App\TipoDocumento;
use App\Ubigeo;
use App\Sector;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class ContactoLoginController extends Controller
{

    protected $redirectTo = 'auth.contacto-login';

    
    public function __construct()
    {
        $this->middleware('guest:contacto', ['except' => ['logoutContacto']]);
    }

    public function showLoginForm()
    {   
        $data['entidad'] = Input::get('e');
        return view('auth.contacto-login', $data);
    }

    public function login(Request $request)
    {
        // validate the form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        // attempt to log the user in
        if (Auth::guard('contacto')->attempt([ 'email' => $request->email, 'password' => $request->password ], $request->remenber)) {
            // if successful, then to their intened location
            echo "exitoso";
            return redirect()->intended(route('contacto.dashboard'));
        }
        

        return redirect()->back()->withInput($request->only('email', 'remenber'));

        // if unsuccessful, then redirect back to the login with the form data
    }

    public function loginJsonEmpresa(Request $request){

      /*  $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
*/
        $value = "0";
        // attempt to log the user in
        if (Auth::guard('contacto')->attempt([ 'email' => $request->email, 'password' => $request->password ], $request->remenber)) {
            // if successful, then to their intened location
            $value = "1";
           // return redirect()->intended(route('contacto.dashboard'));
        }
        
        return json_encode($value,JSON_PRETTY_PRINT);

    }

    public function logoutContacto(Request $request)
    {
        Auth::guard('contacto')->logout();
      //  $request->session()->flush();
      //  $request->session()->regenerate();
      return redirect('/'.$request->entidad);;
    }

    protected function guard()
    {
        return Auth::guard('contacto');
    }


}
