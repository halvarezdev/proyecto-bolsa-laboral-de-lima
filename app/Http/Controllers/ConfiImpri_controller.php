<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class ConfiImpri_controller extends Controller{

    public function indexfis($ide,$user,$docu,$tipo,Request $request){
        $sql_colum='SELECT tpdc_tipodoc,tpdc_nomb FROM tipos_documento 
            WHERE tpdc_tipodoc='."'".$docu."'";
        
        $row_per=DB::select($sql_colum);
        $response['row_per']=$row_per[0];
        $response['ide']=$ide;
        $response['user']=$user;
        $response['docu']=$docu;
        $response['tipo']=$tipo;
        $response['formato']=$request->formato;
        return view('maestro.configuracionfisica_index',$response);        
    }
    public function imprimirgru(Request $request){
        $report=$request->method;
       $nombreclase = "\ConfiImpri_controller::";
        $arrayinf=array('request'=>$request->all());
       return call_user_func_array(__NAMESPACE__.$nombreclase.$report,$arrayinf);
        exit(); 
    }
    public function mostrarinfo($request){
        $response['status']=0;
        $response['message']=''; 
        try{  
            $columno='';
            $response['request']=$request;
            $sql_colum='SELECT cmc_json FROM tb_compadocumencolum 
            WHERE tpdc_tipodoc='."'".$request['docu']."'";
            $row_per=DB::select($sql_colum);
            $data_cab=array();
            if(count($row_per)>0){
                $columno=$this->selecthtmlGroup($row_per[0]->cmc_json);
                $data_cab=json_decode($row_per[0]->cmc_json);
            }
            $formato=$request['formato'];
            if($request['tipo']=='1'){
                $sql_citem='SELECT cmtem_json AS _json FROM tb_compadocumenitem 
                WHERE    tpdc_tipodoc='."'".$request['docu']."' AND empr_id='".$request['empresa']."' AND cmtem_formato='$formato'";
            }elseif($request['tipo']=='2'){
                $sql_citem='SELECT cmtem_json_tiket AS _json FROM tb_compadocumenitem 
                WHERE tpdc_tipodoc='."'".$request['docu']."' AND empr_id='".$request['empresa']."' AND cmtem_formato='$formato'";
            }else{
                $sql_citem='SELECT cmtem_json AS _json FROM tb_compadocumenitem 
                WHERE tpdc_tipodoc='."'".$request['docu']."' AND empr_id='".$request['empresa']."' AND cmtem_formato='$formato'";
            }
            
            $row_item=DB::select($sql_citem);
            $data_cab1=array();
            if(count($row_item)>0){
              
                $data_cab1=json_decode($row_item[0]->_json);
            }
             
            $response['status']=1;
            $response['formato']=$formato;
            $response['columno']=$columno;
            $response['tipo']=$request['tipo'];
            $response['data_cab']=$data_cab;
            $response['data_cab1']=$data_cab1;
            if($request['tipo']=='1'){
                return view('maestro.configuracionnuevo_index',$response);
            }elseif($request['tipo']=='2'){
                return view('maestro.configuracionnuevotike_index',$response);    
            }else{
                return view('maestro.configuracionnuevo_index',$response); 
            }
           
        }catch (\Exception $exc) {
            $response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        }
        return $response;
    }
    public function selecthtmlGroup($json_dbarray){
        $html="";
          
        $datos_configuracionimpre=json_decode($json_dbarray);
        
        if (count((array)$datos_configuracionimpre)>0) {
            # code...
            $html.='<optgroup label="Cabecera">';
            foreach ($datos_configuracionimpre->cabecera as $key => $value) {
                $html.='<option   value="'.$value->columna.'">'.$value->titulo.'</option>' ;
            }

            $html.='</optgroup>';

            $html.='<optgroup label="Detalle">';
            foreach ($datos_configuracionimpre->detalle as $key => $value) {
                $html.='<option   value="'.$value->columna.'">'.$value->titulo.'</option>' ;
            }

            $html.='</optgroup>';
        }
         
        return $html;
      
    }
    public function update_colum($request){
        $response['titulo']='Registro Pedido POS';  
          
        $response['status']=0;
		$response['message']='';
        try{  
            $affected = DB::table('programas_column')
            ->where('prog_id', $request['prog_id']) 
            ->where('prcm_tipo', $request['prcm_tipo'])
            ->update(['prcm_variable' =>$request['data_import']]);
            $response['status']=1;
        }catch (\Exception $exc) {
			$response['message']=$exc->getMessage().' Line=> '.$exc->getLine();
        } 
        return $response;
    }
    public function guardar($request){
        if($request['typy']=='2'){
            $affected = DB::table('tb_compadocumenitem')
            ->where('empr_id', $request['empresa'])
            ->where('cmtem_formato', $request['formato'])
            ->where('tpdc_tipodoc', $request['docmento'])
            ->update(['cmtem_json_tiket' =>$request['jsonparam']]);
        }else{
            $affected = DB::table('tb_compadocumenitem')
              ->where('empr_id', $request['empresa'])
              ->where('cmtem_formato', $request['formato'])
              ->where('tpdc_tipodoc', $request['docmento'])
              ->update(['cmtem_json' =>$request['jsonparam']]);
        }
        
        return $affected;
    }
}