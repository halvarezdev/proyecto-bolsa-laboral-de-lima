<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Mail\EnvioMasivoPedVac;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Vacante;
use App\Pedido;
use App\User;
use Mail;

class MailsController extends Controller
{
    public function __construct(){
       
       // $this->middleware('auth:contacto');

    }

    public function index(){

        $date = Carbon::now();
        $date = $date->format('Y-m-d');

      // $dinsArray = ['40882010','45685241','42906872','09635534'];
        
       // $dinsArray = ['45685241'];
        
        $data['usuarios'] = User::select('users.email','users.name','ocupaciones.nombre as name_ocupacion','ubigeo.ubi_descripcion',
        'fichapersona.ocu_trabajo','fichapersona.ubi_lugar')
        ->join('personas' ,'personas.id','=','users.persona_id')
        ->join('fichapersona' ,'fichapersona.persona_id','=','personas.id')
        ->join('ubigeo' ,'ubigeo.ubi_codigo','=','fichapersona.ubi_lugar')
        ->join('ocupaciones', 'ocupaciones.ocup_codigo','=','fichapersona.ocu_trabajo')
     //   ->where('users.numero_documento','45685241')
        ->get();

        

        foreach($data['usuarios'] as $ped){

            $porciones = explode(" ", $ped->name_ocupacion);

            $vacantes = Vacante::select('vacantes.procedencia','vacantes.nombre_especialidad as descripcion',
            'institucion.nombre as razon_social','vacantes.created_at','institucion.insti_logo as foto','vacantes.id as codigo')
                ->join('institucion' , 'institucion.id','=','vacantes.id_institucion')
                ->where('fecha_postula', '>', $date.' 23:59:59')
                ->where('vacantes.ubigeo',$ped->ubi_lugar)
                ->where((function($query) use ($porciones){
                foreach($porciones as $value){
                    $query->orWhere('vacantes.formacion_academica', 'LIKE', '%'.$value.'%');
                }
                }));

            $data['pedidos'] = Pedido::select('pedidos.procedencia','pedidos.descripcion','empresas.razon_social',
            'pedidos.created_at','empresas.perfil_foto as foto', 'pedidos.id as codigo')
                ->join('empresas' ,'empresas.id','=','pedidos.empresa_id')
                ->where('pedidos.denominacion',$ped->ocu_trabajo)
                ->where(DB::raw('LEFT(pedidos.ubigeo,2)'),substr($ped->ubi_lugar,0,2))
                ->where('pedidos.fecha_limite','>', $date.' 23:59:59')
                ->where('pedidos.flag_estado','1')
            ->unionAll($vacantes)
            ->orderBy('created_at', 'DESC')
            ->take(50)
            ->get();
            
            $data['cantidad'] = $data['pedidos']->count();
            $data['nombre'] = $ped->name;
            $data['ocu_trabajo'] = $ped->ocu_trabajo;
            $data['ubi_descripcion'] = $ped->ubi_descripcion;
            $data['name_ocupacion'] = $ped->name_ocupacion;
            $data['ubi_lugar'] = $ped->ubi_lugar;
              //  dd($data['cantidad']);
            if($data['cantidad'] > 0){
                Mail::to($ped->email)->send(new EnvioMasivoPedVac($data));
            }
        
        }

        return redirect()->back();

    }


}
