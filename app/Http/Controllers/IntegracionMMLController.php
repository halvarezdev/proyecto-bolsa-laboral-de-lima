<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
 
use App\Contacto;
use App\Persona;
use App\Empresa;
use App\User;
use App\FichaPersona;
class IntegracionMMLController extends Controller{
    function integracionMML(Request $request){
        $httpcode=200;
       
        $_SESSION['USUARIO_SESSION']=$request->code;
      
        $url_sis=env('APP_URL');
     
        $url=env('APP_URL_CALIDAD_EXTRANET').'?code='.$request->code;
        /*
        exit();*/
           
   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
        ///curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == 200) {
            $products = json_decode($response,true);
             
            $usuario=$products['sesion'];
             
            if(count($usuario)>0){
            $usuario_nro_doc=$usuario['usuario_nro_doc'];
            $usuario_tipo_doc=$usuario['usuario_tipo_doc'];
            $usuario_tipo_doc_db=2;
            if($usuario_tipo_doc=='01'){
                $usuario_tipo_doc_db=2;
            }else if($usuario_tipo_doc=='02'){
                $usuario_tipo_doc_db=5;
            }else if($usuario_tipo_doc=='03'){
                $usuario_tipo_doc_db=7;
            }else if($usuario_tipo_doc=='04'){
                $usuario_tipo_doc_db=8;
            }else if($usuario_tipo_doc=='06'){
                $usuario_tipo_doc_db=3;
            }
            if (in_array($usuario_tipo_doc, ['01','02','03','04','06'])) {
                $token=$request->session()->token();
                $default= DB::select("SELECT id,email,numero_documento FROM users where numero_documento= '$usuario_nro_doc'");
          
                if(count($default)<=0){
                    $default= DB::select("SELECT id FROM personas where numero_documento= '$usuario_nro_doc'");
                    $persona_id=0;
                    if(count($default)<=0){
                        $persona = Persona::create([
                            'tipo_documento' => $usuario_tipo_doc,
                            'numero_documento' => $usuario_nro_doc,
                            'email' => $usuario['correo'],
                            'name' => $usuario['nombres'],
                            'ape_pat' => $usuario['ape_paterno'],
                            'ape_mat' => $usuario['ape_materno'],
                            'cod_entidad' => 1,
                        ]);
                        $persona_id=$persona->id;
                        $ficha = FichaPersona::create([
                            'persona_id' => $persona_id,
                            'ocu_trabajo' =>'',
                            'ubi_lugar' =>150000,
                            'cod_entidad' => '',
                            'tipo_entidad' => '1',
                        ]);
                        
                        $defaultV= DB::select("SELECT id,email,numero_documento FROM users where email= '".$usuario['correo']."'");
                        if(count($defaultV)<=0){
                            $user = User::create([
                                'name' => $usuario['nombres'],
                                'email' => $usuario['correo'],
                                'password' => Hash::make($usuario_nro_doc),
                                'numero_documento' => $usuario_nro_doc,
                                'persona_id' => $persona_id,
                                'cod_entidad' => 1,
                            ]);
                        }else{
                            $post=[
                                'name' => $usuario['nombres'],
                                'email' =>$usuario['correo'],
                                'password' => Hash::make($usuario_nro_doc),
                                'numero_documento' => $usuario_nro_doc,
                                'persona_id' => $persona_id,
                                'cod_entidad' => 1
                            ];
                            $row=DB::table('users') 
                            ->where('email',$usuario['correo']) 
                            ->update($post);
                        }
                        
                
                        
                    }else{
                        $persona_id=$default[0]->id;
                        $post=[
                            'tipo_documento' => $usuario_tipo_doc,
                            'numero_documento' => $usuario_nro_doc,
                            'email' => $usuario['correo'],
                            'name' => $usuario['nombres'],
                            'ape_pat' => $usuario['ape_paterno'],
                            'ape_mat' => $usuario['ape_materno'],
                            'cod_entidad' => 1,
                        ];
                        $row=DB::table('personas') 
                        ->where('id',$persona_id) 
                        ->update($post);
                        $default2= DB::select("SELECT id FROM users where persona_id= '$persona_id'");
            
                        if(count($default2)<=0){
                            $default3= DB::select("SELECT id,email,numero_documento FROM users where email= '".$usuario['correo']."'");
                            if(count($default3)<=0){
                            $user = User::create([
                                'name' => $usuario['nombres'],
                                'email' =>$usuario['correo'],
                                'password' => Hash::make($usuario_nro_doc),
                                'numero_documento' => $usuario_nro_doc,
                                'persona_id' => $persona_id,
                                'cod_entidad' => 1,
                            ]);
                        }else{
                            $post=[
                                'name' => $usuario['nombres'],
                                'email' =>$usuario['correo'],
                                'password' => Hash::make($usuario_nro_doc),
                                'numero_documento' => $usuario_nro_doc,
                                'persona_id' => $persona_id,
                                'cod_entidad' => 1
                            ];
                            $row=DB::table('users') 
                            ->where('email',$usuario['correo']) 
                            ->update($post);
                        }

                        }else{

                        
                        $post=[
                            'name' => $usuario['nombres'],
                            'email' =>$usuario['correo'],
                            'password' => Hash::make($usuario_nro_doc),
                            'numero_documento' => $usuario_nro_doc,
                            'persona_id' => $persona_id,
                            'cod_entidad' => 1
                        ];
                        $row=DB::table('users') 
                        ->where('persona_id',$persona_id) 
                        ->update($post);
                    }
                    }
                    $email=$usuario['correo'];
                    $numero_documento= $usuario_nro_doc;
                   
                }else{ 
                    $email=$usuario['correo'];
                    $numero_documento=$usuario_nro_doc;
                    $default= DB::select("SELECT id FROM personas where numero_documento= '$usuario_nro_doc'");
                    $persona_id=0;
                    if(count($default)<=0){
                        $persona = Persona::create([
                            'tipo_documento' => $usuario_tipo_doc,
                            'numero_documento' => $usuario_nro_doc,
                            'email' => $usuario['correo'],
                            'name' => $usuario['nombres'],
                            'ape_pat' => $usuario['ape_paterno'],
                            'ape_mat' => $usuario['ape_materno'],
                            'cod_entidad' => 1,
                        ]);
                        $persona_id=$persona->id;
                        $ficha = FichaPersona::create([
                            'persona_id' => $persona_id,
                            'ocu_trabajo' =>'',
                            'ubi_lugar' =>150000,
                            'cod_entidad' => '',
                            'tipo_entidad' => '1',
                        ]);
                    }else{
                        $persona_id=$default[0]->id;
                        $post=[
                            'tipo_documento' => $usuario_tipo_doc,
                            'numero_documento' => $usuario_nro_doc,
                            'email' => $usuario['correo'],
                            'name' => $usuario['nombres'],
                            'ape_pat' => $usuario['ape_paterno'],
                            'ape_mat' => $usuario['ape_materno'],
                            'cod_entidad' => 1,
                        ];
                        $row=DB::table('personas') 
                        ->where('id',$persona_id) 
                        ->update($post);   
                    }
                       
                    $post=[
                        'name' => $usuario['nombres'],
                        'email' =>$usuario['correo'],
                        'password' => Hash::make($usuario_nro_doc),
                      
                        'persona_id' => $persona_id,
                        'cod_entidad' => 1
                    ];
                    $row=DB::table('users') 
                    ->where('numero_documento',$usuario_nro_doc) 
                    ->update($post);
                    
                    
                } 
           
                if (Auth::guard('web')->attempt([ 'email' =>$email, 'password' => $numero_documento ], '')) {
                    echo "exitoso";
                    return redirect()->intended(route('home'));
                }else{
                    echo "no ingreso";
                }
               /* if (Auth::guard('web')->attempt([ 'email' => 'JEANALVAROCASTILLOJEAN248@GMAIL.COM', 'password' => '70590497' ], '')) {
                    echo "exitoso";
                    return redirect()->intended(route('home'));
                }else{
                    echo "no ingreso";
                }*/
                
            }else{
                $usuario_nro_doc=$usuario['usuario_nro_doc'];
        $default= DB::select("SELECT * FROM empresas where numero_documento= '$usuario_nro_doc'");
  
        if(count($default)<=0){
            $empresa = Empresa::create([
                'tipo_documento' => 6,
                'numero_documento' => $usuario['usuario_nro_doc'],
                'razon_social' =>$usuario['nombres'],
                'descripcion' => '',
                'rubro' => '',
                'sitio_web' => '',
                'direccion' => $usuario['usuario_direccion'],
                'ubigeo' => '150101',
                'cod_entidad' => 1,
                'tipo_entidad' => '1',
            ]);
            $default= DB::select("SELECT * FROM contactos where email= '".$usuario['correo']."'");
            if(count($default)<=0){
                $contacto = Contacto::create([
                    'name' => $usuario['nombres'],
                    'email' => $usuario['correo'],
                    'password' => Hash::make($usuario['usuario_nro_doc']),
                    'cargo' => '',
                    'numero_documento' => $usuario['usuario_nro_doc'],
                    'empresa_id' => $empresa->id,
                    'telefono' => $usuario['usuario_celular'],
                    'tipo_documento' => 6,
                    'cod_entidad' => 1,
                ]);
            }else{
                $post=['empresa_id'=>$empresa->id,'name'=>$usuario['nombres'],
                'email'=>$usuario['correo'],'password'=>Hash::make($usuario['usuario_nro_doc'])];
                $row=DB::table('contactos') 
                ->where('email',$usuario['correo']) 
                ->update($post);
            }
        }else{
            $post=['razon_social'=>$usuario['nombres'],'direccion'=>$usuario['usuario_direccion']];
            $row=DB::table('empresas')
            ->where('id',$default[0]->id) 
            ->update($post);

            $post=['name'=>$usuario['nombres'],'email'=>$usuario['correo'],'password'=>Hash::make($usuario['usuario_nro_doc'])];
            $row=DB::table('contactos')
            ->where('empresa_id',$default[0]->id) 
            ->where('email',$usuario['correo']) 
            ->update($post);
            
        }

        if (Auth::guard('contacto')->attempt([ 'email' => $usuario['correo'], 'password' => $usuario['usuario_nro_doc'] ], '')) {
            echo "exitoso";
            return redirect()->intended(route('contacto.dashboard'));
        }else{
            echo "no ingreso";
        }
                
            }
            }else{
                print_r($products);
            }
        }
    }

}