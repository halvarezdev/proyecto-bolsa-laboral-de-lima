<?php

namespace App\Http\Controllers\cv;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Crear_cv extends Controller{

   public function Inicio(){
   		return view('cv/create_cv');
   }

   public function view_cv(Request $request){
      $datas = $request->all();
      $data['informacionPersonal']=$datas;
      return view('cv/view_cv',$data);
   }
  public function buscar_dni(Request $request){
    $arraiInfo=array();
    $data=array();
    $name='';
    $cv_id='';
    $sql1='SELECT * FROM sxtbd_cvpostulante WHERE CV_DNI='."'".$request->dni."'";
     $Lista1=DB::select($sql1);
     if (count($Lista1)>0) {
      foreach ($Lista1 as $key => $value) {
        $name=$value->CV_FOTO;
        $cv_id=$value->CV_ID;
      }
      if ($name!='') {
       $name= asset('cv_folder/'.$name);
      }
      $data[]=array('imagen'=>$name,'_id'=>$cv_id);
      $arraiInfo=$Lista1;
      $tip=true;

     }else{
      $tip=FALSE;
     }
    return response()->json(array('s'=>$tip,'dataInformacion'=>$arraiInfo,'dataExtraNoDB'=>$data)); 
  }
   public function accion(Request $request){
    $result=array();
  	 	try {
  	 		$idRegistro=0;
  	 	   //optenemos todo los valores que se envia desdes post en json
           $data = $request->all();
           $id_='';
           $image ='';
             if (isset($data['dataExtraNoDB'])) {
              foreach ($data['dataExtraNoDB'] as $key => $value) {
                  $image=$value['imagen'];
                  $id_=$value['_id'];
              }
              }
        if ($id_=='' || $id_==0) {
           if (isset($data['dataInformacion'])) {
              $infoArray=array();
              foreach ($data['dataInformacion'] as $key => $value) {
                $infoArray=$value;
              }
            	$dataInto=$infoArray;
              $sql1='SELECT CV_DNI FROM sxtbd_cvpostulante WHERE CV_DNI='."'".$dataInto['CV_DNI']."'";
             $Lista1=DB::select($sql1);
              if (count($Lista1)>0) {
                 DB::table('sxtbd_cvpostulante')->where('CV_DNI',$dataInto['CV_DNI'])->update($posts); 
            
              }else{
                  $idRegistro= DB::table('sxtbd_cvpostulante')->insertGetId($dataInto);
              }	 
           }
          if ($idRegistro!=0) {
           		
            if ($image!='') {

	            $imageInfo = explode(";base64,", $image);
            
    			    $imgExt = str_replace('data:image/', '', (isset($imageInfo[0])?$imageInfo[0]:''));  
                 
    			    $image = str_replace(' ', '+', (isset($imageInfo[1])?$imageInfo[1]:''));
              if ($image!='') {
    			      $imageName = $idRegistro."-cv-.".$imgExt;
    			      $responsePath = public_path('cv/'.$imageName);
    			      $status = file_put_contents($responsePath,base64_decode($image));
                $posts=['CV_FOTO'=>$imageName];
                DB::table('sxtbd_cvpostulante')->where('CV_ID',$idRegistro)->update($posts); 
              }
            }
          }
        }
       
           $result=array('_id'=>$idRegistro);
     	} catch (Exception $exc) {

            return array(
                'success'=> 0,
                'msgError'=> $exc->getMessage()
            );

        } 
        return response()->json($result);
   }
}

    