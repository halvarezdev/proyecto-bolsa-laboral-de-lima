<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Evento;

class EventoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:entidad');
    }

    public function registroEvento(){
        $data['entidad'] = Auth::user()->entidad_id;
        return view('entidad/registro-evento', $data);
    }

    public function eventoSubmit(Request $request){

      /*  $this->validate($request, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);*/
        $name = "";
        $name1 = "";
        $name2 = "";
        $estado1 = "0";
        $estado2 = "0";
        $estado3 = "0";
        if($request->hasFile('imagen')){
            $file = $request->file('imagen');
            $name = time()."-".$file->getClientOriginalName();
            $file->move(public_path().'/eventos/', $name);
            $estado1 = $request->estado1;
        }

        if($request->hasFile('imagen2')){
            $file1 = $request->file('imagen2');
            $name1 = time()."-".$file1->getClientOriginalName();
            $file1->move(public_path().'/eventos/', $name1);
            $estado2 = $request->estado2;
        }

        if($request->hasFile('imagen3')){
            $file2 = $request->file('imagen3');
            $name2 = time()."-".$file2->getClientOriginalName();
            $file2->move(public_path().'/eventos/', $name2);
            $estado3 = $request->estado3;
        }

        $evento = Evento::create([
            'nombre' => $request->nombre,
            'descripcion' => $request->nombre,
            'ruta_imagen' => $name,
            'ruta_imagen_2' => $name1,
            'ruta_imagen_3' => $name2,
            'id_usario' => Auth::user()->id,
            'fecha_inicio' => $request->fecha_ini,
            'fecha_fin' => $request->fecha_fin,
            'estado' => "1",
            'id_entidad' => $request->entidad,
            'estado1' => $estado1,
            'estado2' => $estado2,
            'estado3' => $estado3,
            'ruta_1' => $request->ruta_1,
            'ruta_2' => $request->ruta_2,
            'ruta_3' => $request->ruta_3,
            /*'tipo1' => $request->tipo_1,
            'tipo2' => $request->tipo_2,
            'tipo3' => $request->tipo_3,*/
        ]);
        return redirect()->action('EventoController@listaEvento');

    }

    public function listaEvento(Request $request){
        $eventos = Evento::where('id_entidad', Auth::user()->entidad_id)
        ->where(function ($query) use ($request) {
            if(isset($request)){
                if(isset($request->fec_inicio) && $request->fec_inicio != ''){
                    $query->whereDate('fecha_inicio','<=', $request->fec_inicio);
                }
                if(isset($request->fec_fin) && $request->fec_fin != ''){
                    $query->whereDate('fecha_fin', '>=', $request->fec_fin);
                }
                if(isset($request->nombre) && $request->nombre != ''){
                    $query->where('nombre', 'like', '%'.$request->nombre.'%');
                }
            }
        })
        ->orderBy('created_at', 'DESC')
        ->paginate(5);
        $data['fec_inicio'] = $request->fec_inicio;
        $data['fec_fin'] = $request->fec_fin;
        $data['nombre'] = $request->nombre;
        $data['entidad'] = Auth::user()->entidad_id;
        $data['lista_eventos'] = $eventos;
        return view('entidad/listado-evento', $data);
    }

    public function updateEvento(Request $request){
        $evento = Evento::where('id_evento', $request->id_evento)->first();
        $name = $evento->ruta_imagen;
        $name1 = $evento->ruta_imagen_2;
        $name2 = $evento->ruta_imagen_3;
        if($request->hasFile('imagen')){
            $file = $request->file('imagen');
            $name = time()."-".$file->getClientOriginalName();
            $file->move(public_path().'/eventos/', $name);
        }

        if($request->hasFile('imagen2')){
            $file1 = $request->file('imagen2');
            $name1 = time()."-".$file1->getClientOriginalName();
            $file1->move(public_path().'/eventos/', $name1);
        }

        if($request->hasFile('imagen3')){
            $file2 = $request->file('imagen3');
            $name2 = time()."-".$file2->getClientOriginalName();
            $file2->move(public_path().'/eventos/', $name2);
        }

        $uardado= Evento::where('id_evento', $request->id_evento)
          ->update([
                'nombre' => $request->nombre,
                'descripcion' => $request->nombre,
                'ruta_imagen' => $request->estado1==0?'':$name,
                'ruta_imagen_2' =>$request->estado2==0?'': $name1,
                'ruta_imagen_3' =>$request->estado3==0?'': $name2,
                'fecha_inicio' => $request->fecha_ini,
                'fecha_fin' => $request->fecha_fin,
                'estado1' => $request->estado1,
                'estado2' => $request->estado2,
                'estado3' => $request->estado3,
                'ruta_1' => $request->ruta_1,
                'ruta_2' => $request->ruta_2,
                'ruta_3' => $request->ruta_3,
                /*'tipo1' => $request->tipo_1,
                'tipo2' => $request->tipo_2,
                'tipo3' => $request->tipo_3,*/
              ]);

        return redirect()->action('EventoController@listaEvento');
    }
}
