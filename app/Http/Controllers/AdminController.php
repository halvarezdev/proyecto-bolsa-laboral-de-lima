<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Empresa;
use App\Entidad;
use App\Pedido;
use App\Ubigeo;
use App\User;


class AdminController extends Controller
{

  protected $redirectTo = 'auth.admin-login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index(){

      $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();

      foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['btn_buscar'] = $value->btn_buscar;
        $data['login_logo_persona'] = $value->login_logo_persona;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['enti_nombre'] = $value->enti_nombre;
      }

        $ruc = Input::get('ruc');
        $whereTotales = ['pedidos.flag_estado' => '2'];
        if(isset($ruc)){
          $whereTotales = ['empresas.numero_documento' => $ruc];
        }

        $pedidos = Pedido::select(DB::raw('(SELECT count(pedidos.id) from pedidos where pedidos.empresa_id = empresas.id) as total_pedidos'),'pedidos.id as pedido_codigo','pedidos.descripcion as descripcion_pedido','empresas.numero_documento as ruc', 'empresas.razon_social', 'contactos.name', 'contactos.telefono', 'contactos.email')
        ->join('empresas', 'empresas.id', '=', 'pedidos.empresa_id')
        ->join('contactos', 'contactos.empresa_id', '=', 'empresas.id')
       // ->join('personas', 'contactos.persona_id', '=', 'personas.id')
        ->where($whereTotales)
        ->orderBy('pedidos.created_at', 'DESC')
        ->paginate(20);

        $data['ruc_busqueda'] = $ruc;
        $data['pedidos'] = $pedidos;
        return view('adminhome', $data);
        
    }

  public function publiarVacante($id){

    $idioma = Pedido::where('id', $id)
      ->update([
          'flag_estado' => "1",
      ]);
    return redirect()->action('AdminController@index');

  }

  public function listadoEmpresa(){

    $ruc = Input::get('ruc');

    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
      ->where('enti_flag','=','1')
      ->get();

    foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['btn_buscar'] = $value->btn_buscar;
        $data['login_logo_persona'] = $value->login_logo_persona;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['enti_nombre'] = $value->enti_nombre;
    }

    $data['list_empresas'] = Empresa::select('empresas.id as cod_empresa','empresas.numero_documento','empresas.razon_social',
    'empresas.descripcion','areas.area_descripcion','contactos.name','contactos.numero_documento as dni','contactos.email')
    ->join('areas','areas.id','=','empresas.rubro')
    ->join('contactos','contactos.empresa_id','=','empresas.id')
    ->where((function($query) use ($ruc){
      if(isset($ruc) && $ruc != ''){
        $query->where('empresas.numero_documento','like','%'.$ruc.'%');
      }
    }))
    ->orderBy('empresas.created_at', 'desc')
    ->paginate(15);

    $data['ruc_empresa'] = $ruc;

    return view('admin.listado_empresa',$data);

  }

  public function listadoPersona(){

    $dni = Input::get('dni');
    $entidades = Entidad::where('id','=',Auth::user()->cod_entidad)
    ->where('enti_flag','=','1')
    ->get();

    foreach($entidades as $value){
        $data['enti_logo'] = $value->enti_logo;
        $data['entidad'] = $value->id;
        $data['entidad_color'] = $value->color_entidad;
        $data['btn_buscar'] = $value->btn_buscar;
        $data['login_logo_persona'] = $value->login_logo_persona;
        $data['subcolor_entidad'] = $value->subcolor_entidad;
        $data['enti_nombre'] = $value->enti_nombre;
    }

    $data['list_personas'] = User::select('users.id AS codigo','personas.ape_pat','personas.ape_mat','personas.name','personas.numero_documento',
    'personas.email','personas.telefono','personas.fecha_nacimiento')
    ->join('personas','personas.id','=','users.persona_id')
    ->join('fichapersona','fichapersona.persona_id','=','personas.id')
    ->where((function($query) use ($dni){
      if(isset($dni) && $dni != ''){
        $query->where('users.numero_documento','like','%'.$dni.'%');
      }
    }))
    ->orderBy('users.created_at', 'desc')
    ->paginate(15);
    
    $data['dni'] = $dni;

    return view('admin.listado_pesona',$data);


  }



}
