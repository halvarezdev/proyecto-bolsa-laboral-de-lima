<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {

        if (! $request->expectsJson()) {

           /* if (Auth::guard('entidad')) {
                return route('entidad.login');
            } 

            if (Auth::guard('admin')) {
                return route('admin.login');
            } 

           if (Auth::guard('contacto')) {
                return route('admin.login');
            } 
    */
          return route('welcome');
        }
    }

}
