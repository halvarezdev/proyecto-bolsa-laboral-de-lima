<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreguntaPersona extends Model
{
    protected $table = 'pregunta_persona';
    /**
     * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'disponibilidad', 'declaracion', 'derechos_laborales', 'pretencion',
        'ficha_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
    
    ];
}
