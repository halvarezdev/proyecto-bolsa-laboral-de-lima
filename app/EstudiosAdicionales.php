<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudiosAdicionales extends Model
{
    protected $table = 'pers_estudiosadi';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estu_tipo', 'estu_nombre','estu_cetro','pers_id', 'fichper_id','fecha_fin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
      //  'password', 'remember_token',
    ];
}

