<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdiomaCompuPersona extends Model
{
    protected $table = 'idioma_compu_persona';
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'compu', 'nivel_compu', 'idioma', 'nivel_idioma','ficha_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      //  'password', 'remember_token',
    ];
}
