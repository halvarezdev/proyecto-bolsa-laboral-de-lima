<?php
 header('Access-Control-Allow-Origin: *'); 
require_once('src/nusoap.php');
 $ruc=$_GET['ruc'];
$client = new nusoap_client('https://ws3.pide.gob.pe/services/SunatConsultaRuc?wsdl', 'wsdl');
 
// Doc/lit parameters get wrapped
$return_msg='<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.consultaruc.registro.servicio2.sunat.gob.pe">
   <soapenv:Header/>
   <soapenv:Body>
      <ser:getDatosPrincipales soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <numruc xsi:type="xsd:string">'.$ruc.'</numruc>
      </ser:getDatosPrincipales>
   </soapenv:Body>
</soapenv:Envelope>';
$result = $client->call('getDatosPrincipales',array(),$return_msg);
if ($client->fault) { 
	$data_re['fault']=$result;	
	$data_re['getDatosPrincipales']=[];	
} else { 
	$data_re['getDatosPrincipales']=$result['getDatosPrincipalesReturn'];
}
 
 $return_msg='<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.consultaruc.registro.servicio2.sunat.gob.pe">
   <soapenv:Header/>
   <soapenv:Body>
      <ser:getDomicilioLegal soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <numruc xsi:type="xsd:string">'.$ruc.'</numruc>
      </ser:getDomicilioLegal>
   </soapenv:Body>
</soapenv:Envelope>
';
$result = $client->call('getDomicilioLegal',array(),$return_msg);

if ($client->fault) { 
	$data_re['fault']=$result;	
	$data_re['getDomicilioLegal']=[];	
} else { 
	$data_re['getDomicilioLegal']=$result['getDomicilioLegalReturn'];
}
  
echo json_encode($data_re); 	
	
?>
 